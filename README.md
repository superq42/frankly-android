**Project.Properties** SHOULD NOT BE TRACKED.

Library projects are in another git. Maintaining a single android support jar file in the libs for various libraries. Please import it as an external jar. 

Working rules for this repo: 


a) For ANY work, be it a feature be it a feature request or a bug fix create a new branch. Branches are for free, don't be stingy. 

b) All local branches are to be tracked remotely as well. This is so that your code is always safe in the case of a natural disaster. This would also allow additional teammates  to collaborate with you on features/bugs that you are working on. 

c) Name all development branches with the convention: **dev-[Name of new feature]** ex: dev-video-filters
    All bug fixes with the convention: **dev-[Bug Descriptor]** ex: dev-video-upload-crash

To merge your code with master follow these steps: 

NEVER PUSH TO MASTER. You can't anyway. Only the admin can. 

On your local: 

a) Update your master: **git checkout master**  and then **git pull master**

b) Switch to your local branch (the last commit should already be on the remote branch): **git checkout dev-video-filters** 

c) Merge with master:  **git merge master**

d) In case of conflicts resolve them and commit: **git commit -m 'Resolved conflicts between master and [Branch Name]'**. 
Follow the message convention as described above.

e) Push the local updated branch to the remote: **git push origin dev-video-filters**

f) Create a Pull request on bitbucket. (option available in the LHS menu) 

All done.