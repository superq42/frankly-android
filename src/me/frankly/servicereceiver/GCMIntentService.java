package me.frankly.servicereceiver;

import java.util.ArrayList;

import me.frankly.config.Constant;
import me.frankly.model.newmodel.NotificationModel;
import me.frankly.util.MyNotificationUtils;
import me.frankly.util.PrefUtils;

import org.json.JSONArray;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMIntentService extends IntentService {
	public static final String GCM_SENDER_ID = "345381322113";
	private String TAG = "GcmIntentService";

	public GCMIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		Log.d("franklygcm", "in on handle of gcm" + extras);
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

				Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
				// Post notification of received message.
				String userTo = extras.getString("user_to");
				Log.d(TAG, "userid saved " + PrefUtils.getUserID()
						+ " userTO: " + userTo);
				String notif_type = extras.getString("type");
				Log.i("gcm", " Type " + notif_type + PrefUtils.getUsername());
				if (notif_type
						.equals(Constant.Notification.TYPE_NOTIFICATION_COUNT)) {
					Log.i("gcm", "1st if");
					String str = extras.getString("unseen_count");
					if (str != null)
						PrefUtils.setNotificationCount(Integer.parseInt(str));
					Log.i("gcm", "  Setting count in pref count  " + str);
				} else if (userTo != null
						&& userTo.equalsIgnoreCase(PrefUtils.getUserID()))
				/* User is Still logged in with same account. */
				{
					Log.d("gcm", "userto!=null");
					NotificationModel notif = parseNotification(extras);
					if (notif != null) {
						Log.i("gcm", "notification not null");
						MyNotificationUtils.CreateBigNotification(
								getApplicationContext(), notif);
					}
				}
				Log.i(TAG, "Received: " + extras.toString());
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GCMBroadcastReciever.completeWakefulIntent(intent);

	}

	private NotificationModel parseNotification(Bundle extras) {

		NotificationModel notif = new NotificationModel();
		String notif_type = extras.getString("type");
		String text = extras.getString("text");
		String image_url = extras.getString("image_url");
		String icon_url = extras.getString("icon_url");
		
		String group_id = extras.getString("group_id");
		String id = extras.getString("id");
		
		int view_type = NotificationModel.TYPE_BIGTEXT;
		if (extras.containsKey("view_type"))
			view_type = Integer.parseInt(extras.getString("view_type"));
		ArrayList<String> stringArray = new ArrayList<String>();
		
		try {
			stringArray = new ArrayList<String>();
			String listJson = extras.getString("lines");
			if (listJson != null) {
				JSONArray array = new JSONArray(listJson);
				for (int i = 0; i < array.length(); i++)
					stringArray.add(array.getString(i));
			}
			notif.setLinesList(stringArray);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Log.d("debug_gcm", view_type + " is the view_type " + stringArray + " "
				+ image_url);

		notif.setText(text);
		notif.setNotif_View_Type(view_type);
		notif.setImage_url(image_url);
		notif.setIcon_url(icon_url);
		notif.setGroup_id(group_id);
		String heading = extras.getString("heading");
		notif.setId(id);
		notif.setHeading(heading);

		if (!notif_type.equals(NotificationModel.TYPE_FOLLOW)) {
			String entity_id = extras.getString("entity_id");
			notif.setEntity_Id(entity_id);
		}
		notif.setDeeplink(extras.getString("deeplink"));

		return notif;
	}
}
