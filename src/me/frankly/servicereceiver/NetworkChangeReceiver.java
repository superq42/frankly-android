package me.frankly.servicereceiver;

import me.frankly.config.AppConfig;
import me.frankly.config.Controller;
import me.frankly.model.newmodel.PendingAnswer;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Receiver to detect network state and start profile/answer upload services
 * accordingly
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

	private static final String LOG_TAG = NetworkChangeReceiver.class
			.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		int networkStatus = MyUtilities.getConnectivityStatus(context);
		Controller.onNetworkStateChaged(networkStatus);

		if (AppConfig.DEBUG) {
			Log.i(LOG_TAG, "Network State: " + networkStatus);
		}

		if (networkStatus == MyUtilities.TYPE_NOT_CONNECTED) {
			// stop service if already running
			stopProfileUploadingService(context);
			PrefUtils.setProfileServiceStatus(MyUtilities.SERVICE_STOPPED);
		} else if (networkStatus != MyUtilities.TYPE_NOT_CONNECTED) {
			// start service to upload profile video or pending answers
			boolean serviceStarted = startProfileUploadService(context);
			if (!serviceStarted) {
				startSingleAnswerUploadService(context);
			}
		}
	}

	/**
	 * @param context
	 * @return true if service started
	 */
	private boolean startProfileUploadService(Context context) {
		boolean serviceStarted = false;
		if (PrefUtils.isVideoSentPending()
				&& PrefUtils.getProfileServiceStatus() == MyUtilities.SERVICE_STOPPED) {
			Intent intent = new Intent(context, ProfileUploadingService.class);
			context.startService(intent);
			serviceStarted = true;
		}
		if (AppConfig.DEBUG) {
			Log.i(LOG_TAG, "Profile Service Started: " + serviceStarted);
		}
		return serviceStarted;
	}

	/**
	 * @param context
	 */
	private void stopProfileUploadingService(Context context) {
		Intent intent = new Intent(context, ProfileUploadingService.class);
		context.stopService(intent);
	}

	/**
	 * @param context
	 * @return true if service started
	 */
	private boolean startSingleAnswerUploadService(Context context) {
		boolean serviceStarted = false;
		PendingAnswer pendingAnswer = PrefUtils
				.getNextPendingAnswer(PendingAnswer.STATE_WAITING);
		if (pendingAnswer == null) {
			pendingAnswer = PrefUtils
					.getNextPendingAnswer(PendingAnswer.STATE_FAILED);
		}
		if (pendingAnswer != null) {
			SingleAnswerUploadService.start(context, pendingAnswer);
			serviceStarted = true;
		}
		if (AppConfig.DEBUG) {
			Log.i(LOG_TAG, "Answer Service Started: " + serviceStarted);
		}
		return serviceStarted;
	}
}
