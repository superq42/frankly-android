package me.frankly.servicereceiver;

import me.frankly.config.AppConfig;
import me.frankly.listener.PendingAnswersProgressListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Receiver that receives progress messages for <B>Pending Answers</B>
 * 
 * @author abhishek_inoxapps
 * 
 */
public class PendingAnswerProgressReceiver extends BroadcastReceiver {

	/**
	 * Default Intent Filter for this Receiver
	 */
	public static final String FILTER_PENDING_ANSWER_PROGRESS = "action_pending_answer_progress";

	/**
	 * Default Keys for Intent of this Receiver
	 */
	public static final String KEY_EXTRA_QUESTION_ID = "key_extra_question_id";
	public static final String KEY_EXTRA_PROGRESS_STATE = "key_extra_progress_state";
	public static final String KEY_EXTRA_PROGRESS_PERCENT = "key_extra_progress_percent";
	public static final String KEY_EXTRA_POST_ID = "key_extra_post_id";
	public static final String KEY_EXTRA_VIDEO_URL = "key_extra_video_url";
	public static final String KEY_EXTRA_THUMBNAIL_URL = "key_extra_thumb_url";
	public static final String KEY_EXTRA_POST_OBJECT = "key_extra_post_object";
	public static final String KEY_EXTRA_ERROR_CODE = "key_extra_error_code";


	private PendingAnswersProgressListener progressListener;

	public PendingAnswerProgressReceiver(
			PendingAnswersProgressListener progressListener) {
		super();
		this.progressListener = progressListener;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (AppConfig.DEBUG) {
			Log.i("PendingAnswerProgressReceiver", "onProgressUpdate() "
					+ (intent == null ? "null" : intent.getExtras()));
		}
		progressListener.onProgressUpdate(context, intent);
	}
}
