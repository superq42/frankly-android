package me.frankly.servicereceiver;

import me.frankly.config.AppConfig;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * this is the common receiver for all install trackers
 */
public class ManyInstallTrackersReceiver extends BroadcastReceiver {

	/**
	 * Log tag
	 */
	private String TAG = ManyInstallTrackersReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		if (AppConfig.DEBUG) {
			Log.i(TAG, "onReceive() " + intent.getAction());
		}

		Intent serviceIntent = new Intent(context,
				ManyInstallTrackersService.class);
		serviceIntent.fillIn(intent, 0);
		context.startService(serviceIntent);
	}
}