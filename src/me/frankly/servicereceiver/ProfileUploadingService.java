package me.frankly.servicereceiver;

import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import me.frankly.config.Controller;
import me.frankly.config.UrlResolver;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class ProfileUploadingService extends Service {
	private EditableProfileObject editableProfileObject;
	private static String TAG="UpLoadProfile";

	// private String compressedVideoUrl = null;

	public ProfileUploadingService() {
		Log.i(TAG, " Media upload  Starts");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.e(TAG, "onStartCommand  ");
		if (PrefUtils.getProfileServiceStatus() == MyUtilities.SERVICE_STOPPED) {
			PrefUtils.setProfileServiceStatus(MyUtilities.SERVICE_STARTED);

			editableProfileObject = PrefUtils.getPendingEditProfileObject();
			if (editableProfileObject == null) {
				stopService(true);
			} else {
				Log.d(TAG, "isUserLoggedIn=" + Controller.isUserLoggedIn());
				if (Controller.isNetworkConnected(getApplicationContext()) && Controller.isUserLoggedIn()) {
					new Thread(new Runnable() {
						@Override
						public void run() {
							if (PrefUtils.getUserID().equals(editableProfileObject.getId())) {
								try {
									uploadFIles();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							stopService(true);
						}
					}).start();
				} else if (!Controller.isUserLoggedIn()) {
					stopService(false);
				}
			}
		} else {
			stopService(true);
		}

		return Service.START_STICKY;
	}

	/**
	 * handing pref and uplaoding data properly
	 * 
	 */
	private void uploadFIles() {
		try {
			HttpResponse postFile = postFile(editableProfileObject);
			Log.i(TAG, " status code" + postFile.getStatusLine().getStatusCode());
			Log.i(TAG, " status reasonprse" + postFile.getStatusLine().getReasonPhrase());
			if (postFile.getStatusLine().getStatusCode() == 200) {
				PrefUtils.setPendingEditProfileObject(null);
				PrefUtils.setIsVideoSentPending(false);
				String userData = MyUtilities.getContent(postFile);
				Log.i(TAG, " Upload media json Responce  " + userData);
				PrefUtils.saveCurrentUserAsJson(userData);
			}
		} catch (Exception e) {
			Log.i(TAG, "Exception " + e);
			e.printStackTrace();
		}
	}

	/**
	 * Makes a network call to upload video and thumb and profile pic( whatever
	 * available)
	 * 
	 * @param profileObject
	 *            :
	 * @return : Response of network call
	 * @throws Exception
	 */
	public HttpResponse postFile(EditableProfileObject profileObject) throws Exception {
		Log.e(TAG, " Media Upload Post File " + "Video Url: " + profileObject.getProfile_video());
		Log.e(TAG, " Media Upload Post File " + "Cover Pic Url: " + profileObject.getCover_picture());
		Log.e(TAG, " Media Upload Post File " + "Profile pic Url: " + profileObject.getProfile_picture());

		String urlString = UrlResolver.BASE_URL+"user/update_profile/" + PrefUtils.getUserID();
		Log.e(TAG, "Url  "+urlString);
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(urlString);
		post.addHeader("X-Token", PrefUtils.getAppAccessToken());
		post.addHeader("X-DeviceId", Controller.getDeviceInfoObject(getApplicationContext()).getDevice_id());
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

		String properVideoForUploading = profileObject.getProperVideoForUploading();
		if (properVideoForUploading != null) {
			File file1 = new File(properVideoForUploading);
			FileBody bin1 = new FileBody(file1);
			builder.addPart("profile_video", bin1);
		}
		if (profileObject.getProfile_picture() != null) {
			File file2 = new File(profileObject.getProfile_picture());
			FileBody bin2 = new FileBody(file2);
			builder.addPart("profile_picture", bin2);
		}
		if (profileObject.getCover_picture() != null) {
			File file3 = new File(profileObject.getCover_picture());
			FileBody bin3 = new FileBody(file3);
			builder.addPart("cover_picture", bin3);
		}

		if (profileObject.getFirst_name() != null) {
			builder.addPart("first_name", new StringBody(profileObject.getFirst_name()));
		}
		if (profileObject.getBio() != null) {
			builder.addPart("bio", new StringBody(profileObject.getBio()));
		}

		final HttpEntity yourEntity = builder.build();

		class ProgressiveEntity implements HttpEntity {
			@Override
			public void consumeContent() throws IOException {
				yourEntity.consumeContent();
			}

			@Override
			public InputStream getContent() throws IOException, IllegalStateException {
				return yourEntity.getContent();
			}

			@Override
			public Header getContentEncoding() {
				return yourEntity.getContentEncoding();
			}

			@Override
			public long getContentLength() {
				return yourEntity.getContentLength();
			}

			@Override
			public Header getContentType() {
				return yourEntity.getContentType();
			}

			@Override
			public boolean isChunked() {
				return yourEntity.isChunked();
			}

			@Override
			public boolean isRepeatable() {
				return yourEntity.isRepeatable();
			}

			@Override
			public boolean isStreaming() {
				return yourEntity.isStreaming();
			} // CONSIDER put a _real_ delegator into here!

			@Override
			public void writeTo(OutputStream outstream) throws IOException {

				class ProxyOutputStream extends FilterOutputStream {
					/**
					 * @author Stephen Colebourne
					 */

					public ProxyOutputStream(OutputStream proxy) {
						super(proxy);
					}

					public void write(int idx) throws IOException {
						out.write(idx);
					}

					public void write(byte[] bts) throws IOException {
						out.write(bts);
					}

					public void write(byte[] bts, int st, int end) throws IOException {

						out.write(bts, st, end);
						// Log.e("upload_proxy", " =st " + st + " --end" + end);
					}

					public void flush() throws IOException {
						out.flush();
					}

					public void close() throws IOException {
						out.close();
					}
				} // CONSIDER import this class (and risk more Jar File Hell)

				class ProgressiveOutputStream extends ProxyOutputStream {
					int sentbytes = 0;
					int prevpercentage = 0;

					public ProgressiveOutputStream(OutputStream proxy) {
						super(proxy);
					}

					public void write(byte[] bts, int st, int end) throws IOException {

						sentbytes += end;
						long contentLength = getContentLength();
						int percentage = (int) ((((float) sentbytes) * 100) / ((float) contentLength));
						if (percentage > prevpercentage) {
							Log.i(TAG, " sent: " + percentage + " --total: " + contentLength);
						}

						out.write(bts, st, end);

					}
				}
				yourEntity.writeTo(new ProgressiveOutputStream(outstream));
			}

		}
		;
		ProgressiveEntity myEntity = new ProgressiveEntity();
		post.setEntity(myEntity);
		HttpResponse response = client.execute(post);
		return response;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// nothing to do here
		return null;
	}
	
	/**
	 * @param pStartNextService
	 */
	private void stopService(boolean pStartNextService) {
		PrefUtils.setProfileServiceStatus(MyUtilities.SERVICE_STOPPED);
		stopSelf();
		if (pStartNextService) {
			SingleAnswerUploadService.start(this, null);
		}
	}
}