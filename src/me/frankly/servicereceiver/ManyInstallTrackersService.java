package me.frankly.servicereceiver;

import me.frankly.config.AppConfig;
import me.frankly.config.Controller;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.Utilities;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.mixpanel.android.mpmetrics.InstallReferrerReceiver;
import com.mobileapptracker.Tracker;

/**
 * this service is called only from {@link ManyInstallTrackersReceiver}
 */
public class ManyInstallTrackersService extends IntentService {
	/**
	 * Log tag
	 */
	private static String TAG = ManyInstallTrackersService.class.getSimpleName();

	/**
	 * no arg constructor and call to super(String) is must
	 */
	public ManyInstallTrackersService() {
		super("ManyInstallTrackersService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (AppConfig.DEBUG) {
			Log.i(TAG, "onHandleIntent() " + intent.getExtras());
		}
		//sendEventToMobileAppTracker(intent);
		Log.d("InstallTracking","Going to send event to mixpanel from mixpanel") ; 
		sendEventToMixPanel(intent);
	}

	/**
	 * @param intent
	 */
	private void sendEventToMobileAppTracker(Intent intent) {
		Tracker tracker = new Tracker();
		tracker.onReceive(this, intent);
	}

	/**
	 * @param intent
	 */
	private void sendEventToMixPanel(Intent intent) {
		try {
			InstallReferrerReceiver mixpanelReferrerTracking = new InstallReferrerReceiver();
			mixpanelReferrerTracking.onReceive(this, intent);
			// Now you can pass the same intent on to other services,
			// or process it yourself

			Bundle extras = intent.getExtras();
			String referrerUsername = null;
			String url = "utm_source=Custom&utm_medium=Custom&utm_term=Custom&utm_content=Custom&utm_campaign=Custom";
			if (extras != null) {
				url = extras.getString("referrer");
				referrerUsername = Utilities.getReferrerParameter(url);
				if (referrerUsername != null) {
					PrefUtils.setRefererUsername(referrerUsername);
				}
			} else {
				MixpanelUtils.setSuperPropertiesForCampains(this);
			}
			if (AppConfig.DEBUG) {
				Log.i(TAG, "sendEventToMixPanel() " + url);
			}
			Log.d("InstallTracking","Just before calling mixpanel") ;
			MixpanelUtils.sendInstallEvent(referrerUsername, url,
					MixpanelUtils.APP_INSTALLED, this);

			Controller.sendInstallReferalData(this, Controller
					.getDeviceInfoObject(this).getDevice_id(), url);

		} catch (Exception e) {
			MixpanelUtils.sendGenericEvent(null, null, null, e.getMessage(),
					"exception in sendEventToMixPanel()", this);
			if (AppConfig.DEBUG) {
				Log.e(TAG, "sendEventToMixPanel()", e);
			}
		}
	}
}
