package me.frankly.servicereceiver;

import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.FranklyCrashlytics;
import me.frankly.config.UrlResolver;
import me.frankly.config.UrlResolver.EndPoints;
import me.frankly.listener.CompressionCompletedListener;
import me.frankly.model.newmodel.PendingAnswer;
import me.frankly.model.newmodel.PostObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.response.SinglePost;
import me.frankly.util.FileUtils;
import me.frankly.util.FileUtils.FileType;
import me.frankly.util.IoUtils;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.VideoCompressionTool;
import me.frankly.util.VideoProcessingUtils;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

/**
 * This service will upload a single answer at a time. <br/>
 * Though it will check for more pending answers and will restart itself
 * accordingly.
 * 
 * @author sachin.gupta
 */
public class SingleAnswerUploadService extends IntentService {

	public static final String LOG_TAG = SingleAnswerUploadService.class
			.getSimpleName();

	private static final String EXTRA_JSON_PENDING_ANSWER = "EXTRA_JSON_PENDING_ANSWER";

	private volatile boolean mIsProcessing;
	private PendingAnswer mPendingAnswer;
	private String mQuestionId;

	/**
	 * no-arg constructor and call to super(name) is must.
	 */
	public SingleAnswerUploadService() {
		super(SingleAnswerUploadService.class.getSimpleName());
	}

	@Override
	public void onHandleIntent(Intent intent) {
		if (intent != null && intent.hasExtra(EXTRA_JSON_PENDING_ANSWER)) {
			String json = intent.getStringExtra(EXTRA_JSON_PENDING_ANSWER);
			mPendingAnswer = (PendingAnswer) JsonUtils.objectify(json,
					PendingAnswer.class);
		} else {
			mPendingAnswer = PrefUtils
					.getNextPendingAnswer(PendingAnswer.STATE_WAITING);
		}

		if (mPendingAnswer == null) {
			if (AppConfig.DEBUG) {
				Log.d(LOG_TAG, "onHandleIntent(): No answer in STATE_WAITING.");
			}
			return;
		}

		try {
			mQuestionId = mPendingAnswer.getQuestionObject().getId();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (TextUtils.isEmpty(mQuestionId)
				|| TextUtils.isEmpty(mPendingAnswer.getClientId())) {
			saveAndBroadcastError(Constant.ERROR_CODES.ANSWER_FILES_MISSING,
					"Question Id or Client Id is null.");
			return;
		}

		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "onHandleIntent(): " + mQuestionId);
		}
		if (!Controller.isUserLoggedIn()) {
			saveAndBroadcastError(Constant.ERROR_CODES.UNAUTHORIZED_ACCESS,
					"User not logged in.");
			return;
		}

		if (!Controller.isNetworkConnected(this)) {
			saveAndBroadcastError(Constant.ERROR_CODES.BAD_INTERNET,
					"No connectivity.");
			return;
		}

		if (!checkFilesAvailability()) {
			saveAndBroadcastError(Constant.ERROR_CODES.ANSWER_FILES_MISSING,
					"Answer files are missing.");
			return;
		}

		if (Build.VERSION.SDK_INT >= 16) {
			if (mPendingAnswer.isCompressionValid()) {
				initPostAnswer();
			} else {
				saveAndBroadcastState(PendingAnswer.STATE_PREPARING);
				compressPendingAnswer();
			}
		} else {
			initPostAnswer();
		}
	}

	/**
	 * 
	 */
	private void compressPendingAnswer() {
		mIsProcessing = true;
		String compressPath = FileUtils.newFile(this,
				FileType.VIDEO_COMPRESSED, mQuestionId).getAbsolutePath();
		mPendingAnswer.setCompressedVideoPath(compressPath);
		updatePendingAnswersList(false);
		new VideoCompressionTool().processOpenVideo(
				mPendingAnswer.getVideoPath(), compressPath, null, null,
				new CompressionCompletedListener() {
					@Override
					public void didWriteData(File file, Context context,
							boolean last, boolean error) {
						if(!last){
							return ;
						}
						if (error) {
							removePendingAnswerFiles(true);
						} else {
							mPendingAnswer.setCompressionValid(true);
							updatePendingAnswersList(false);
						}
						initPostAnswer();
					}
				}, false);
		while (mIsProcessing) {
			SystemClock.sleep(100);
			// wait till compression and upload both completes
		}
	}

	/**
	 * Uploading an answer and handling if any error occurs or files not present
	 * 
	 */
	private void initPostAnswer() {
		String content = "";
		boolean isFilesAvailable = checkFilesAvailability();
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "initPostAnswer() isFilesAvailable: "
					+ isFilesAvailable);
		}
		if (!isFilesAvailable) {
			saveAndBroadcastError(Constant.ERROR_CODES.ANSWER_FILES_MISSING,
					"Files Not Found");
			return;
		}
		int responseCode = 0;
		HttpResponse response = null;
		try {
			response = postFile();
			responseCode = response.getStatusLine().getStatusCode();
			content = IoUtils.getStringFromInputStream(response.getEntity()
					.getContent());
		} catch (Exception e) {
			e.printStackTrace();
			saveAndBroadcastError(Constant.ERROR_CODES.SHIT_HAPPENED,
					"postFile()");
			return;
		}
		if (responseCode == 200 && !TextUtils.isEmpty(content)) {
			SinglePost singlePost = (SinglePost) JsonUtils.objectify(content,
					SinglePost.class);
			PostObject postObject = singlePost.getPost();
			saveAndBroadcastSuccess(postObject);
		} else {
			saveAndBroadcastError(responseCode, "ResponseCode: " + responseCode);
		}
	}

	/**
	 * @return true if both files are present, false if cant recover cover from
	 *         video or video not present
	 */
	private boolean checkFilesAvailability() {
		String videoPath = mPendingAnswer.getVideoPath();
		if (videoPath == null || !new File(videoPath).exists()) {
			return false;
		}
		if (mPendingAnswer.isCompressionValid()) {
			String compressedPath = mPendingAnswer.getCompressedVideoPath();
			if (compressedPath == null || !new File(compressedPath).exists()) {
				mPendingAnswer.setCompressionValid(false);
			}
		} else {
			removePendingAnswerFiles(true);
		}
		String thumbPath = mPendingAnswer.getImagePath();
		if (thumbPath != null && new File(thumbPath).exists()) {
			return true;
		}
		int vidLength = -2;
		try {
			vidLength = VideoProcessingUtils.getVideoLength(
					getApplicationContext(), videoPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Bitmap bmp = VideoProcessingUtils
				.getThumbnail(videoPath, vidLength / 2);
		File thumbFile = FileUtils.newFile(this, FileType.VIDEO_THUMBNAIL,
				mQuestionId);
		thumbPath = thumbFile.getAbsolutePath();
		FileUtils.createImageFile(bmp, thumbPath);
		mPendingAnswer.setImagePath(thumbPath);
		updatePendingAnswersList(false);
		return true;
	}

	/**
	 * remove files related to pending answer
	 * 
	 * @param pOnlyCompressed
	 */

	private void removePendingAnswerFiles(boolean pOnlyCompressed) {
		if (mPendingAnswer.getCompressedVideoPath() != null) {
			if (pOnlyCompressed) {
				int compressedVideoLength = (int) new File(
						mPendingAnswer.getCompressedVideoPath()).length();
				int originalVideoLength = (int) new File(
						mPendingAnswer.getVideoPath()).length();
				MixpanelUtils.sendCompressionFailEvent("Compression_Failed",
						originalVideoLength, compressedVideoLength, this);
			}
			 new File(mPendingAnswer.getCompressedVideoPath()).delete();
		}
		if (pOnlyCompressed) {
			return;
		}
		if (mPendingAnswer.getVideoPath() != null) {
			new File(mPendingAnswer.getVideoPath()).delete();
		}
		if (mPendingAnswer.getImagePath() != null) {
			new File(mPendingAnswer.getImagePath()).delete();
		}
	}

	/**
	 * @param postObject
	 */
	private void saveAndBroadcastSuccess(PostObject postObject) {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "saveAndBroadcastSuccess() " + mQuestionId);
		}
		UniversalUser profile = PrefUtils.getCurrentUserAsObject();
		if (profile != null) {
			profile.setAnswer_count(profile.getAnswer_count() + 1);
			PrefUtils.saveCurrentUserAsJson(JsonUtils.jsonify(profile));
		}
		Controller.removeCurrentUserFromCache();
		Controller.invalidateAllPagesInCache(EndPoints.GET_QUESTIONS);

		Intent i = new Intent(
				PendingAnswerProgressReceiver.FILTER_PENDING_ANSWER_PROGRESS);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_QUESTION_ID,
				mQuestionId);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_POST_ID,
				postObject.getId());
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_THUMBNAIL_URL,
				mPendingAnswer.getImagePath());
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_VIDEO_URL,
				mPendingAnswer.getVideoUrl());

		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_POST_OBJECT,
				JsonUtils.jsonify(postObject));

		LocalBroadcastManager.getInstance(getApplicationContext())
				.sendBroadcast(i);

		updatePendingAnswersList(true);
		mIsProcessing = false;
	}

	/**
	 * @param pRemoveCurrentAndRestart
	 */
	private void updatePendingAnswersList(boolean pRemoveCurrentAndRestart) {
		if (pRemoveCurrentAndRestart) {
			PrefUtils.removePendingAnswer(mPendingAnswer);
			removePendingAnswerFiles(false);
			start(this, null);
		} else {
			PrefUtils.updatePendingAnswer(mPendingAnswer);
		}
	}

	/**
	 * @param uploadPrecent
	 */
	private void saveAndBroadcastPercentage(float uploadPrecent) {
		mPendingAnswer.setCurrentState(PendingAnswer.STATE_UPLOADING);
		mPendingAnswer.setPrecentUploaded(uploadPrecent);
		updatePendingAnswersList(false);

		Intent i = new Intent(
				PendingAnswerProgressReceiver.FILTER_PENDING_ANSWER_PROGRESS);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_PROGRESS_STATE,
				PendingAnswer.STATE_UPLOADING);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_PROGRESS_PERCENT,
				uploadPrecent);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_QUESTION_ID,
				mQuestionId);
		LocalBroadcastManager.getInstance(getApplicationContext())
				.sendBroadcast(i);
	}

	/**
	 * @param errorCode
	 * @param errorMsg
	 */
	private void saveAndBroadcastError(int errorCode, String errorMsg) {
		if (errorCode == Constant.ERROR_CODES.ANSWER_FILES_MISSING
				|| errorCode == Constant.ERROR_CODES.ALREADY_ANSWERED) {
			updatePendingAnswersList(true);
		} else {
			mPendingAnswer.setPrecentUploaded(0);
			mPendingAnswer.setCurrentState(PendingAnswer.STATE_FAILED);
			updatePendingAnswersList(false);
		}

		if (AppConfig.DEBUG) {
			Log.e(LOG_TAG, "saveAndBroadcastError() " + errorCode + ", "
					+ errorMsg);
		}
		Intent i = new Intent(
				PendingAnswerProgressReceiver.FILTER_PENDING_ANSWER_PROGRESS);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_ERROR_CODE,
				errorCode);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_QUESTION_ID,
				mQuestionId);
		LocalBroadcastManager.getInstance(getApplicationContext())
				.sendBroadcast(i);

		mIsProcessing = false;
	}

	/**
	 * @param newState
	 */
	private void saveAndBroadcastState(int newState) {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "saveAndBroadcastState() " + newState);
		}
		mPendingAnswer.setCurrentState(newState);
		updatePendingAnswersList(false);

		Intent i = new Intent(
				PendingAnswerProgressReceiver.FILTER_PENDING_ANSWER_PROGRESS);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_PROGRESS_STATE,
				newState);
		i.putExtra(PendingAnswerProgressReceiver.KEY_EXTRA_QUESTION_ID,
				mQuestionId);
		LocalBroadcastManager.getInstance(getApplicationContext())
				.sendBroadcast(i);
	}

	/**
	 * @param videopath
	 * @param thumb_path
	 * @param question_id
	 * @param client_id
	 * @return RESPONSE OF THE NETWORK CALL
	 * @throws Exception
	 * 
	 *             make a network call to upload answer data and returns the
	 *             response
	 * 
	 */
	@SuppressWarnings("deprecation")
	public HttpResponse postFile() throws Exception {
		String videoPath = mPendingAnswer.getVideoPath();
		if (mPendingAnswer.isCompressionValid()) {
			videoPath = mPendingAnswer.getCompressedVideoPath();
		}
		FranklyCrashlytics.log("VideoPath:" + videoPath + " image_path:"
				+ mPendingAnswer.getImagePath());
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "postFile() video: " + videoPath);
			Log.d(LOG_TAG, "postFile() image: " + mPendingAnswer.getImagePath());
		}
		File file1 = new File(videoPath);
		File file2 = new File(mPendingAnswer.getImagePath());
		String urlString = UrlResolver.BASE_URL + "post/media/add";
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(urlString);
		post.addHeader("X-Token", PrefUtils.getAppAccessToken());
		post.addHeader("X-DeviceId",
				Controller.getDeviceInfoObject(getApplicationContext())
						.getDevice_id());
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		FileBody bin1 = new FileBody(file1);
		FileBody bin2 = new FileBody(file2);
		builder.addPart("video_media", bin1);
		builder.addPart("video_media_thumbnail", bin2);
		builder.addPart("question_id", new StringBody(mQuestionId));
		builder.addPart("answer_type", new StringBody("video"));
		builder.addPart("body", new StringBody(""));
		builder.addPart("tags", new StringBody(""));
		float[] longiLati = PrefUtils.getLocation();
		builder.addPart("lat", new StringBody(longiLati[1] + ""));
		builder.addPart("lon", new StringBody(longiLati[0] + ""));
		builder.addPart("client_id",
				new StringBody(mPendingAnswer.getClientId()));
		final HttpEntity yourEntity = builder.build();

		class ProgressiveEntity implements HttpEntity {
			@Override
			public void consumeContent() throws IOException {
				yourEntity.consumeContent();
			}

			@Override
			public InputStream getContent() throws IOException,
					IllegalStateException {
				return yourEntity.getContent();
			}

			@Override
			public Header getContentEncoding() {
				return yourEntity.getContentEncoding();
			}

			@Override
			public long getContentLength() {
				return yourEntity.getContentLength();
			}

			@Override
			public Header getContentType() {
				return yourEntity.getContentType();
			}

			@Override
			public boolean isChunked() {
				return yourEntity.isChunked();
			}

			@Override
			public boolean isRepeatable() {
				return yourEntity.isRepeatable();
			}

			@Override
			public boolean isStreaming() {
				return yourEntity.isStreaming();
			}

			@Override
			public void writeTo(OutputStream outstream) throws IOException {
				/**
				 * @author Stephen Colebourne
				 */
				class ProxyOutputStream extends FilterOutputStream {
					public ProxyOutputStream(OutputStream proxy) {
						super(proxy);
					}

					public void write(int idx) throws IOException {
						out.write(idx);
					}

					public void write(byte[] bts) throws IOException {
						out.write(bts);
					}

					public void write(byte[] bts, int st, int end)
							throws IOException {

						out.write(bts, st, end);
					}

					public void flush() throws IOException {
						out.flush();
					}

					public void close() throws IOException {
						out.close();
					}
				} // CONSIDER import this class (and risk more Jar File Hell)

				class ProgressiveOutputStream extends ProxyOutputStream {
					int sentbytes = 0;
					long prevBcastTime = 0;

					public ProgressiveOutputStream(OutputStream proxy) {
						super(proxy);
					}

					public void write(byte[] bts, int st, int end)
							throws IOException {

						sentbytes += end;
						long contentLength = getContentLength();
						int percentage = (int) ((((float) sentbytes) / ((float) contentLength)) * 100);

						if (System.currentTimeMillis() - prevBcastTime > 1000) {
							saveAndBroadcastPercentage(percentage);
							prevBcastTime = System.currentTimeMillis();
						}
						out.write(bts, st, end);
					}
				}

				yourEntity.writeTo(new ProgressiveOutputStream(outstream));
			}
		}
		ProgressiveEntity myEntity = new ProgressiveEntity();
		post.setEntity(myEntity);
		HttpResponse response = client.execute(post);
		return response;
	}

	@Override
	public void onDestroy() {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "onDestroy() " + mQuestionId);
		}
		PrefUtils.updateAllPendingAnswers();
		super.onDestroy();
	}

	/**
	 * Starts service either with given pending answer or with
	 * {@link PrefUtils#getNextPendingAnswer()}
	 * 
	 * @param context
	 * @param pendingAnswer
	 *            or null
	 */
	public static void start(Context context, PendingAnswer pendingAnswer) {
		Intent intent = new Intent(context, SingleAnswerUploadService.class);
		if (pendingAnswer != null) {
			intent.putExtra(EXTRA_JSON_PENDING_ANSWER,
					JsonUtils.jsonify(pendingAnswer));
		}
		context.startService(intent);
	}
}
