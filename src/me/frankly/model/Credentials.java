package me.frankly.model;

public class Credentials {
	String auth_token = null;
	String facebook_access_token = null;
	String fb_user_id = null;
	String gcm_id = null;
	String gplus_id;
	String phone_num;
	String fullname;

	String google_plus_access_token = null;
	String gplus_user_id;
	String password = null;
	String twitter_access_token = null;
	String twitter_user_id = null;
	String username = null;
	String twitter_token_secret = null;
	String id = null;
	String email = null;

	public String getGplus_id() {
		return gplus_id;
	}

	public void setGplus_id(String gplus_id) {
		this.gplus_id = gplus_id;
	}

	public String getFullName() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getPhone_num() {
		return phone_num;
	}

	public void setPhone_num(String phone_num) {
		this.phone_num = phone_num;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccess_token() {
		return this.auth_token;
	}

	public String getFacebook_access_token() {
		return this.facebook_access_token;
	}

	public String getFb_user_id() {
		return this.fb_user_id;
	}

	public String getGcm_id() {
		return this.gcm_id;
	}

	public String getGoogle_plus_access_token() {
		return this.google_plus_access_token;
	}

	public String getGplus_user_id() {
		return this.gplus_user_id;
	}

	public String getPassword() {
		return this.password;
	}

	public String getTwitter_access_token() {
		return this.twitter_access_token;
	}

	public String getTwitter_user_id() {
		return this.twitter_user_id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setAccess_token(String paramString) {
		this.auth_token = paramString;
	}

	public void setFacebook_access_token(String paramString) {
		this.facebook_access_token = paramString;
	}

	public void setFb_user_id(String paramString) {
		this.fb_user_id = paramString;
	}

	public void setGcm_id(String paramString) {
		this.gcm_id = paramString;
	}

	public void setGoogle_plus_access_token(String paramString) {
		this.google_plus_access_token = paramString;
	}

	public void setGplus_user_id(String paramString) {
		this.gplus_user_id = paramString;
	}

	public void setPassword(String paramString) {
		this.password = paramString;
	}

	public void setTwitter_access_token(String paramString) {
		this.twitter_access_token = paramString;
	}

	public String getTwitter_token_secret() {
		return twitter_token_secret;
	}

	public void setTwitter_token_secret(String twitter_token_secret) {
		this.twitter_token_secret = twitter_token_secret;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTwitter_user_id(String paramString) {
		this.twitter_user_id = paramString;
	}

	public void setUsername(String paramString) {
		this.username = paramString;
	}
}
