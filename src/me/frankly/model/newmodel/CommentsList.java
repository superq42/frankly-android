package me.frankly.model.newmodel;

import java.util.ArrayList;

public class CommentsList {
	int total_count;
	int next_index;
	ArrayList<CommentObject> comments;
	
	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	

	public ArrayList<CommentObject> getComments() {
		return comments;
	}

	public void setComments(ArrayList<CommentObject> comments) {
		this.comments = comments;
	}
	
	public int getNext_index() {
		return next_index;
	}

	public void setNext_index(int next_index) {
		this.next_index = next_index;
	}
}
