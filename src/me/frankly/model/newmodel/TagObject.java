package me.frankly.model.newmodel;

import java.math.BigInteger;

public class TagObject {
	String name;
	String id;
	int follower_count;
	int post_count;
	String serial_id;
	String cover_picture;
	boolean is_following;
	UniversalUser created_by;

	public TagObject(String name) {
		this.name = name;
	}

	public String getSerial_id() {
		return serial_id;
	}

	public void setSerial_id(BigInteger serial_id) {
		this.serial_id = String.valueOf(serial_id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getFollower_count() {
		return follower_count;
	}

	public void setFollower_count(int follower_count) {
		this.follower_count = follower_count;
	}

	public int getAnswer_count() {
		return post_count;
	}

	public void setAnswer_count(int answer_count) {
		this.post_count = answer_count;
	}

	public int getPost_count() {
		return post_count;
	}

	public void setPost_count(int post_count) {
		this.post_count = post_count;
	}

	public String getCover_picture() {
		return cover_picture;
	}

	public void setCover_picture(String cover_picture) {
		this.cover_picture = cover_picture;
	}

	public boolean isIs_following() {
		return is_following;
	}

	public void setIs_following(boolean is_following) {
		this.is_following = is_following;
	}

	public UniversalUser getCreated_by() {
		return created_by;
	}

	public void setCreated_by(UniversalUser created_by) {
		this.created_by = created_by;
	}

	public void setSerial_id(String serial_id) {
		this.serial_id = serial_id;
	}

}
