package me.frankly.model.newmodel;

public class LocationObject {

	String location_name;
	CoordinatePoint coordinate_point;

	public String getLocation_name() {
		return location_name;
	}

	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}

	public CoordinatePoint getCoordinate_point() {
		return coordinate_point;
	}

	public void setCoordinate_point(CoordinatePoint coordinate_point) {
		this.coordinate_point = coordinate_point;
	}

	public class CoordinatePoint {
		float[] coordinates;
		String type;

		public float[] getCoordinates() {
			return coordinates;
		}

		public void setCoordinates(float[] coordinates) {
			this.coordinates = coordinates;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

	}

}
