package me.frankly.model.newmodel;

import java.util.ArrayList;

public class ChannelIconList {

	public static final String TYPE_ICON_LIST = "icon_list";
	private String type;
	private String name;
	private ArrayList<ChannelIcon> icons;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<ChannelIcon> getIcons() {
		return icons;
	}

	public void setIcons(ArrayList<ChannelIcon> icons) {
		this.icons = icons;
	}

}
