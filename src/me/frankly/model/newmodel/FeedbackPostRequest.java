package me.frankly.model.newmodel;

public class FeedbackPostRequest {

	private String count;
	private String medium;
	private String message;
	
	public void setCount(String str) {
		this.count = str;
	}
	
	public String getCount() {
		return this.count;
	}
	
	public void setMedium(String med) {
		this.medium = med;
	}
	
	public String getMedium() {
		return this.medium;
	}
	
	public void setMessage(String msg) {
		this.message = msg;
	}
	
	public String getMessage() {
		return this.message;
	}
}
