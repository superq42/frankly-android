package me.frankly.model.newmodel;

import java.util.ArrayList;

/**
 * This class holds data for Answers after recording is done.<br/>
 */
public class PendingAnswer {

	public final static int STATE_WAITING = 0;
	public final static int STATE_PREPARING = 1;// Video is being compressed
	public final static int STATE_UPLOADING = 2;// Answer is being uploaded
	public final static int STATE_SUCCEEDED = 3;// Answer upload succeeded
	public final static int STATE_FAILED = 4; // upload failed

	private String videoPath;
	private String imagePath;

	private boolean isCompressionValid;
	private String compressedVideoPath;

	private String videoUrl;
	private String imageUrl;

	private String client_id;
	private ArrayList<String> tagList;
	private QuestionObject questionObject;
	private float precentUploaded;
	private int currentState;

	/**
	 * @return the videoPath
	 */
	public String getVideoPath() {
		return videoPath;
	}

	/**
	 * @param videoPath
	 *            the videoPath to set
	 */
	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the isCompressionValid
	 */
	public boolean isCompressionValid() {
		return isCompressionValid;
	}

	/**
	 * @param isCompressionValid
	 *            the isCompressionValid to set
	 */
	public void setCompressionValid(boolean isCompressionValid) {
		this.isCompressionValid = isCompressionValid;
	}

	/**
	 * @return the compressedVideoPath
	 */
	public String getCompressedVideoPath() {
		return compressedVideoPath;
	}

	/**
	 * @param compressedVideoPath
	 *            the compressedVideoPath to set
	 */
	public void setCompressedVideoPath(String compressedVideoPath) {
		this.compressedVideoPath = compressedVideoPath;
	}

	/**
	 * @return the videoUrl
	 */
	public String getVideoUrl() {
		return videoUrl;
	}

	/**
	 * @param videoUrl
	 *            the videoUrl to set
	 */
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl
	 *            the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the client_id
	 */
	public String getClientId() {
		return client_id;
	}

	/**
	 * @param client_id
	 *            the client_id to set
	 */
	public void setClientId(String client_id) {
		this.client_id = client_id;
	}

	/**
	 * @return the tagList
	 */
	public ArrayList<String> getTagList() {
		return tagList;
	}

	/**
	 * @param tagList
	 *            the tagList to set
	 */
	public void setTagList(ArrayList<String> tagList) {
		this.tagList = tagList;
	}

	/**
	 * @return the questionObject
	 */
	public QuestionObject getQuestionObject() {
		return questionObject;
	}

	/**
	 * @param questionObject
	 *            the questionObject to set
	 */
	public void setQuestionObject(QuestionObject questionObject) {
		this.questionObject = questionObject;
	}

	/**
	 * @return the precentUploaded
	 */
	public float getPrecentUploaded() {
		return precentUploaded;
	}

	/**
	 * @param precentUploaded
	 *            the precentUploaded to set
	 */
	public void setPrecentUploaded(float precentUploaded) {
		this.precentUploaded = precentUploaded;
	}

	/**
	 * @return the currentState
	 */
	public int getCurrentState() {
		return currentState;
	}

	/**
	 * @param currentState
	 *            the currentState to set
	 */
	public void setCurrentState(int currentState) {
		this.currentState = currentState;
	}
}