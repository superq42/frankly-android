package me.frankly.model.newmodel;

public class SingleUniversalUserContainer {
	UniversalUser user;
	boolean available;

	public UniversalUser getUser() {
		return user;
	}

	public void setUser(UniversalUser user) {
		this.user = user;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

}
