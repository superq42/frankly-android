package me.frankly.model.newmodel;

public class QuestionObjectHolder {

	public static final String TYPE_QUESTION = "question";
	public static final String TYPE_INVITE_PROMPT = "invite_prompt";
	public static final String TYPE_SHARE_QUESTION = "share_question";
	public static final String TYPE_CURRENT_QUESTION = "current_user_question"; 
	public static final String TYPE_OTHER_QUESTION = "other_user_question"; 
	public String type;
	public QuestionObject question;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public QuestionObject getQuestion() {
		return question;
	}

	public void setQuestion(QuestionObject question) {
		this.question = question;
	}

}
