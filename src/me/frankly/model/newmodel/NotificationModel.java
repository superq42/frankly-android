package me.frankly.model.newmodel;

import java.util.ArrayList;

public class NotificationModel {

	// fRESH FOR LIST
	public static final String TYPE_FOLLOW = "follow";

	// NOTIFICATION TYPES
	public static final int TYPE_INBOX = 3;
	public static final int TYPE_BIGPICTURE = 2;
	public static final int TYPE_BIGTEXT = 1;
	public static int TYPE_CUSTOM = 4;

	private int view_type = 1;
	private String group_id, image_url, icon_url,  heading,
			styled_text, id, entity_id, text, deeplink;
	private long updated_at;

	private ArrayList<String> lines;

	public String getStyled_text() {
		return styled_text;
	}

	public void setStyled_text(String styled_text) {
		this.styled_text = styled_text;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public long getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(long updated_at) {
		this.updated_at = updated_at;
	}

	public ArrayList<String> getLines() {
		return lines;
	}

	public void setLines(ArrayList<String> lines) {
		this.lines = lines;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getId() {
		return id;
	}


	public String getEntity_Id() {
		return entity_id;
	}

	public void setEntity_Id(String entity_id) {
		this.entity_id = entity_id;
	}

	public String getDeeplink() {
		return deeplink;
	}

	public void setDeeplink(String deeplink) {
		this.deeplink = deeplink;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getNotif_id() {
		return group_id == null ? 0 : group_id.hashCode();
	}

	public int getNotif_View_Type() {
		return view_type;
	}

	public void setNotif_View_Type(int view_type) {
		this.view_type = view_type;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getIcon_url() {
		return icon_url;
	}

	public void setIcon_url(String icon_url) {
		this.icon_url = icon_url;
	}

	public ArrayList<String> getLinesList() {
		return lines;
	}

	public void setLinesList(ArrayList<String> linesList) {
		this.lines = linesList;
	}

}
