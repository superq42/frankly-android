package me.frankly.model.newmodel;

import java.util.ArrayList;

import me.frankly.model.response.SearchableObjectContainer;

public class SearchResults {

	private int count;
	private int next_index;
	private ArrayList<SearchableObjectContainer> results;
	private String q;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public ArrayList<SearchableObjectContainer> getResults() {
		return results;
	}

	public void setResults(ArrayList<SearchableObjectContainer> results) {
		this.results = results;
	}

	public int getNextIndex() {
		return next_index;
	}

	public void setNextIndex(int nextIndex) {
		this.next_index = nextIndex;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}
}
