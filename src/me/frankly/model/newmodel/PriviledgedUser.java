package me.frankly.model.newmodel;

import java.util.ArrayList;
import java.util.HashMap;

public class PriviledgedUser {
	long date_of_birth;
	String email;
	String facebook_id;
	String google_id;
	String instagram_id;
	String twitter_id;
	String interested_in;

	int admin_level;
	int answer_count;
	int following_count;
	String bio;
	int view_count;
	String cover_picture;
	String profile_video;
	String first_name;
	int follower_count;
	String gender;
	String id;
	ArrayList<Interests> interests;
	String last_name;
	int likes_count;
	LocationObject location;
	String profile_picture;
	String username;
	long last_updated;
	String fb_perm;
	int user_type;
	String user_title;
	
	String web_link;
	HashMap<String, String> profile_videos;

	public String getWeb_link() {
		return web_link;
	}

	public void setWeb_link(String web_link) {
		this.web_link = web_link;
	}

	public int getUser_type() {
		return user_type;
	}

	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}

	public String getUser_title() {
		return user_title;
	}

	public void setUser_title(String user_title) {
		this.user_title = user_title;
	}

	public String getFb_perm() {
		return fb_perm;
	}

	public void setFb_perm(String fb_perm) {
		this.fb_perm = fb_perm;
	}

	ArrayList<MediaObject> uploads;

	public long getLast_updated() {
		return last_updated;
	}

	public ArrayList<MediaObject> getUploads() {
		return uploads;
	}

	public void setUploads(ArrayList<MediaObject> uploads) {
		this.uploads = uploads;
	}

	public void setLast_updated(long last_updated) {
		this.last_updated = last_updated;
	}

	public int getAnswer_count() {
		return answer_count;
	}

	public void setAnswer_count(int answer_count) {
		this.answer_count = answer_count;
	}

	public int getAdmin_level() {
		return admin_level;
	}

	public void setAdmin_lount(int admin_level) {
		this.admin_level = admin_level;
	}

	/**
	 * @return the following_count
	 */
	public int getFollowing_count() {
		return following_count;
	}

	/**
	 * @param following_count
	 *            the following_count to set
	 */
	public void setFollowing_count(int following_count) {
		this.following_count = following_count;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getCover_picture() {
		return cover_picture;
	}

	public void setCover_picture(String cover_picture) {
		this.cover_picture = cover_picture;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public int getFollower_count() {
		return follower_count;
	}

	public HashMap<String, String> getProfile_videos() {
		return profile_videos;
	}

	public void setProfile_videos(HashMap<String, String> profile_videos) {
		this.profile_videos = profile_videos;
	}

	public void setFollower_count(int follower_count) {
		this.follower_count = follower_count;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Interests> getInterests() {
		return interests;
	}

	public void setInterests(ArrayList<Interests> interests) {
		this.interests = interests;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public int getLikes_count() {
		return likes_count;
	}

	public void setLikes_count(int likes_count) {
		this.likes_count = likes_count;
	}

	public LocationObject getLocation() {
		return location;
	}

	public void setLocation(LocationObject location) {
		this.location = location;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(long date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebook_id() {
		return facebook_id;
	}

	public void setFacebook_id(String facebook_id) {
		this.facebook_id = facebook_id;
	}

	public String getGoogle_id() {
		return google_id;
	}

	public void setGoogle_id(String google_id) {
		this.google_id = google_id;
	}

	public String getInstagram_id() {
		return instagram_id;
	}

	public void setInstagram_id(String instagram_id) {
		this.instagram_id = instagram_id;
	}

	public String getTwitter_id() {
		return twitter_id;
	}

	public void setTwitter_id(String twitter_id) {
		this.twitter_id = twitter_id;
	}

	public String getInterested_in() {
		return interested_in;
	}

	public void setInterested_in(String interested_in) {
		this.interested_in = interested_in;
	}

	public String getProfile_video() {
		return profile_video;
	}

	public void setProfile_video(String profile_video) {
		this.profile_video = profile_video;
	}

	public void setViewCount(int val) {
		this.view_count = val;
	}
	
	public int getViewCount() {
		return this.view_count;
	}
}
