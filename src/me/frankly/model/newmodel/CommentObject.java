package me.frankly.model.newmodel;

import android.util.Log;

public class CommentObject extends UniqueEntity implements Comparable<CommentObject>{

	String body;
	UniversalUser comment_author;
	long timestamp;
	String serial_id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public UniversalUser getComment_author() {
		return comment_author;
	}

	public void setComment_author(UniversalUser comment_author) {
		this.comment_author = comment_author;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getSerial_id() {
		return serial_id;
	}

	public void setSerial_id(String serial_id) {
		this.serial_id = serial_id;
	}

	@Override
	public int compareTo(CommentObject another) {
		long compareTimestamp = another.getTimestamp();
		if (this.getTimestamp() > compareTimestamp)
            return 1; 
        else if (this.getTimestamp() == compareTimestamp)
            return 0; 
        else  
            return -1; 
	}

}
