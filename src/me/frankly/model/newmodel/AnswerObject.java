package me.frankly.model.newmodel;

import java.util.ArrayList;
import java.util.HashMap;

public class AnswerObject {
	public static final String TYPE_PICTURE = "picture";
	public static final String TYPE_VIDEO = "video";
	public static final String TYPE_TEXT = "text";

	String body;
	MediaObject media;
	
	HashMap<String, String> media_urls;
	

	public HashMap<String, String> getMedia_urls() {
		return media_urls;
	}

	public void setMedia_urls(HashMap<String, String> media_urls) {
		this.media_urls = media_urls;
	}

	String type;
	long timestamp;
	ArrayList<TagObject> tags;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public MediaObject getMedia() {
		return media;
	}

	public void setMedia(MediaObject media) {
		this.media = media;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public ArrayList<TagObject> getTags() {
		return tags;
	}

	public void setTags(ArrayList<TagObject> tags) {
		this.tags = tags;
	}

}
