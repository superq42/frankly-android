package me.frankly.model.newmodel;

import java.util.ArrayList;

public class ChannelObject {
	public static final String TYPE_BANNER = "banner";
	public static final String TYPE_SEARCH = "search";

	private String channel_id;
	private String bg_image;
	private String type;
	private String name;
	private String icon;
	private String description;
	private ArrayList<ChannelIconList> views;

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getBg_image() {
		return bg_image;
	}

	public void setBg_image(String bg_image) {
		this.bg_image = bg_image;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<ChannelIconList> getViews() {
		return views;
	}

	public void setViews(ArrayList<ChannelIconList> views) {
		this.views = views;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
