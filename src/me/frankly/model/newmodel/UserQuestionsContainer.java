package me.frankly.model.newmodel;

import java.util.ArrayList;

public class UserQuestionsContainer {
	private String username;
	private String first_name;
	private String last_name;
	private String profile_picture;
	private String gender;
	private String user_title;
	private boolean is_following;
	private String id;
	private ArrayList<QuestionObject> questions;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirst_name() {
		return first_name;
	}


	public String getFull_name(){
		
	   if( last_name != null && first_name != null)
	   {
		   return (first_name + " " + last_name) ; 
	   }else if(first_name != null){
		   
		   return first_name ; 
	   }else 
		    return "Frankly User" ; 
	   
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getUser_title() {
		return user_title;
	}

	public void setUser_title(String user_title) {
		this.user_title = user_title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<QuestionObject> getQuestions() {
		return questions;
	}

	public void setQuestions(ArrayList<QuestionObject> questions) {
		this.questions = questions;
	}

	public boolean isIs_following() {
		return is_following;
	}

	public void setIs_following(boolean is_following) {
		this.is_following = is_following;
	}

}