package me.frankly.model.newmodel;

import twitter4j.auth.AccessToken;

public class TwitterInfoContainer {

	AccessToken twitterAccessToken;
	EditableProfileObject twitterUser;

	public AccessToken getTwitterAccessToken() {
		return twitterAccessToken;
	}

	public void setTwitterAccessToken(AccessToken twitterAccessToken) {
		this.twitterAccessToken = twitterAccessToken;
	}

	public EditableProfileObject getTwitterUser() {
		return twitterUser;
	}

	public void setTwitterUser(EditableProfileObject twitterUser) {
		this.twitterUser = twitterUser;
	}

}
