package me.frankly.model.newmodel.postrequest;

public class UserIdOnlyRequest {
	String user_id;

	public String getUserId() {
		return user_id;
	}

	public void setUserId(String userId) {
		this.user_id = userId;
	}

}
