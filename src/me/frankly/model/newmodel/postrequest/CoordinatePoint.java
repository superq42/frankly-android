package me.frankly.model.newmodel.postrequest;

public class CoordinatePoint {
	float[] coordinate_point;
	String loc_name;
	String country;
	String country_code;

	public String getLoc_name() {
		return loc_name;
	}

	public void setLoc_name(String loc_name) {
		this.loc_name = loc_name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public float[] getCoordinate_point() {
		return coordinate_point;
	}

	public void setCoordinate_point(float[] coordinate_point) {
		this.coordinate_point = coordinate_point;
	}

}
