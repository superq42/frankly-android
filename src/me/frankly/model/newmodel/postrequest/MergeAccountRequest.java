package me.frankly.model.newmodel.postrequest;

public class MergeAccountRequest {

	String external_access_token;
	String social_user_id;
	String email;

	public String getExternal_access_token() {
		return external_access_token;
	}

	public void setExternal_access_token(String external_access_token) {
		this.external_access_token = external_access_token;
	}

	public String getSocial_user_id() {
		return social_user_id;
	}

	public void setSocial_user_id(String social_user_id) {
		this.social_user_id = social_user_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
