package me.frankly.model.newmodel.postrequest;

public class UsernameOnlyRequest {
	String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
