package me.frankly.model.newmodel.postrequest;

import com.google.gson.annotations.SerializedName;

public class UpdateGCMRequest {
	@SerializedName("X-Deviceid")
	String device_id;
	String push_id;

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public String getGcm_id() {
		return push_id;
	}

	public void setGcm_id(String gcm_id) {
		this.push_id = gcm_id;
	}

}
