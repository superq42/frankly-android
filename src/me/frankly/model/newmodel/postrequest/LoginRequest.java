package me.frankly.model.newmodel.postrequest;

import me.frankly.model.newmodel.DeviceInfo;

public class LoginRequest {
	String type;
	String username;
	String password;
	String email;
	DeviceInfo device_info;
	String external_access_token;
	String push_id;
	String social_user_id;
	String device_id;
	String external_token_secret;

	public LoginRequest(String type, String device_id, DeviceInfo deviceInfo,
			String push_id) {

		this.type = type;
		this.device_info = deviceInfo;
		this.push_id = push_id;
		this.device_id = device_id;
	}

	public LoginRequest(String type, String device_id, DeviceInfo deviceInfo) {

		this.type = type;
		this.device_info = deviceInfo;
		this.device_id = device_id;
	}

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public String getType() {
		return type;
	}

	public String getSocial_user_id() {
		return social_user_id;
	}

	public void setSocial_user_id(String social_user_id) {
		this.social_user_id = social_user_id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DeviceInfo getDevice_info() {
		return device_info;
	}

	public void setDevice_info(DeviceInfo device_info) {
		this.device_info = device_info;
	}

	public String getExternal_access_token() {
		return external_access_token;
	}

	public void setExternal_access_token(String external_access_token) {
		this.external_access_token = external_access_token;
	}

	public String getToken_secret() {
		return external_token_secret;
	}

	public void setToken_secret(String token_secret) {
		this.external_token_secret = token_secret;
	}

	public String getPush_id() {
		return push_id;
	}

	public void setPush_id(String gcm_id) {
		this.push_id = gcm_id;
	}

}
