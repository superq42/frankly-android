package me.frankly.model.newmodel.postrequest;

import java.util.ArrayList;

public class PostQuestionRequest {

	public static final String TYPE_AUDIO = "audio";
	public static final String TYPE_TEXT = "text";

	String request_id;
	String recommended_question_id;
	String question_to;
	String question_type;
	String body;
	String media_url;
	float[] coordinate_point;
	ArrayList<String> tags;
	boolean is_anonymous;

	public String getQuestion_to() {
		return question_to;
	}

	public String getRequest_id() {
		return request_id;
	}

	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}

	public void setRecommended_question_id(String recommended_question_id) {
		this.recommended_question_id = recommended_question_id;
	}

	public String getRecommended_question_id() {
		return recommended_question_id;
	}

	public void setQuestion_to(String question_to) {
		this.question_to = question_to;
	}

	public String getQuestion_type() {
		return question_type;
	}

	public void setQuestion_type(String question_type) {
		this.question_type = question_type;
	}

	public String getMedia_url() {
		return media_url;
	}

	public void setMedia_url(String media_url) {
		this.media_url = media_url;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public float[] getCoordinate_points() {
		return coordinate_point;
	}

	public void setCoordinate_points(float[] coordinate_points) {
		this.coordinate_point = coordinate_points;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public boolean isIs_anonymous() {
		return is_anonymous;
	}

	public void setIs_anonymous(boolean is_anonymous) {
		this.is_anonymous = is_anonymous;
	}

}
