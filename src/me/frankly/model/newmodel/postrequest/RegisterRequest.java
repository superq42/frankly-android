package me.frankly.model.newmodel.postrequest;

import me.frankly.model.newmodel.DeviceInfo;

public class RegisterRequest {

	private String email;
	private String full_name ; 
	private String username;
	private String password;
	private DeviceInfo device_info;
	private String external_access_token;
	private String push_id;
	private String social_user_id;
	String device_id;
	String token_secret;
	String phone_num;

	public String getPhone_num() {
		return phone_num;
	}

	public void setPhone_num(String phone_num) {
		this.phone_num = phone_num;
	}

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public RegisterRequest(String device_id, String fullName,  String email, String password,
			DeviceInfo device_info, String gcm_id) {
		this.email = email;
		this.setFull_name(fullName) ; 
		this.device_info = device_info;
		this.push_id = gcm_id;
		this.password = password;
		this.device_id = device_id;
	}

	public RegisterRequest(String device_id, String fullName, String email,
			String password, DeviceInfo device_info) {
		this.email = email;
		this.full_name = fullName ; 
		this.device_info = device_info;
		this.password = password;
		this.device_id = device_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExternal_access_token() {
		return external_access_token;
	}

	public void setExternal_access_token(String external_access_token) {
		this.external_access_token = external_access_token;
	}

	public String getPush_id() {
		return push_id;
	}

	public void setPush_id(String gcm_id) {
		this.push_id = gcm_id;
	}

	public String getToken_secret() {
		return token_secret;
	}

	public void setToken_secret(String token_secret) {
		this.token_secret = token_secret;
	}

	public String getSocial_user_id() {
		return social_user_id;
	}

	public void setSocial_user_id(String social_user_id) {
		this.social_user_id = social_user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public DeviceInfo getDevice_info() {
		return device_info;
	}

	public void setDevice_info(DeviceInfo device_info) {
		this.device_info = device_info;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

}
