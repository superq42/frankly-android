package me.frankly.model.newmodel.postrequest;

import java.util.ArrayList;

import me.frankly.model.newmodel.TagObject;

public class PostCommentrequest {

	String post_id;
	String body;
	ArrayList<TagObject> tags;
	float[] coordinate_point;

	public String getPost_id() {
		return post_id;
	}

	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public ArrayList<TagObject> getTags() {
		return tags;
	}

	public void setTags(ArrayList<TagObject> tags) {
		this.tags = tags;
	}

	public float[] getCoordinate_point() {
		return coordinate_point;
	}

	public void setCoordinate_point(float[] coordinate_point) {
		this.coordinate_point = coordinate_point;
	}

}
