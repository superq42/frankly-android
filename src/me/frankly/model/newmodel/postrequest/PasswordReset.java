package me.frankly.model.newmodel.postrequest;

public class PasswordReset
{
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
