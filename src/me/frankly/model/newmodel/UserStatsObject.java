package me.frankly.model.newmodel;

public class UserStatsObject {

	private int question_count;
	
	public void setQuestion_count(int count) {
		this.question_count = count;
	}
	
	public int getQuestion_count() {
		return this.question_count;
	}
}
