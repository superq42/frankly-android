package me.frankly.model.newmodel;

import java.util.ArrayList;

public class PostObject extends UniqueEntity {
	UniversalUser question_author;
	UniversalUser answer_author;
	QuestionObject question;
	AnswerObject answer;
	ArrayList<TagObject> tags;
	
	int favorited_count;
	int liked_count;
	int comment_count;
	int view_count;
	String serial_id;
	boolean is_liked;
	String client_id;
	String web_link ; 
	
	int whatsapp_share_count;
	int other_share_count;
	
	public int getWhatsappShareCount()
	{
		return whatsapp_share_count;
	}
    
	public int getOtherShareCount()
	{
		return other_share_count;
	}
	
	public String getWeb_link(){
		return web_link ; 
	}
	public void setWeb_link(String web_link){
		this.web_link = web_link ; 
	}
	public String getSerial_id() {
		return serial_id;
	}

	public void setSerial_id(String serial_id) {
		this.serial_id = serial_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public AnswerObject getAnswer() {
		return answer;
	}

	public void setAnswer(AnswerObject answer) {
		this.answer = answer;
	}

	public int getFavorited_count() {
		return favorited_count;
	}

	public void setFavorited_count(int favorited_count) {
		this.favorited_count = favorited_count;
	}

	public int getLiked_count() {
		return liked_count;
	}

	public void setLiked_count(int liked_count) {
		this.liked_count = liked_count;
	}

	public int getComment_count() {
		return comment_count;
	}

	public void setComment_count(int comment_count) {
		this.comment_count = comment_count;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UniversalUser getQuestion_author() {
		return question_author;
	}

	public boolean isIs_liked() {
		return is_liked;
	}

	public void setIs_liked(boolean is_liked) {
		this.is_liked = is_liked;
	}

	public boolean getIs_liked(){
		return this.is_liked ; 
	}
	public void setQuestion_author(UniversalUser question_author) {
		this.question_author = question_author;
	}

	public UniversalUser getAnswer_author() {
		return answer_author;
	}

	public void setAnswer_author(UniversalUser answer_author) {
		this.answer_author = answer_author;
	}

	public QuestionObject getQuestion() {
		return question;
	}

	public void setQuestion(QuestionObject question) {
		this.question = question;
	}

	public ArrayList<TagObject> getTags() {
		return tags;
	}

	public void setTags(ArrayList<TagObject> tags) {
		this.tags = tags;
	}

	public int getView_count() {
		return view_count;
	}

	public void setView_count(int view_count) {
		this.view_count = view_count;
	}
}

