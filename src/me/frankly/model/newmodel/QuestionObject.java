package me.frankly.model.newmodel;

import java.util.ArrayList;
import java.util.List;

public class QuestionObject {
	public static final String ANONYMOUS_USER_NAME = "anonymous";
	public static final String TYPE_AUDIO = "audio";
	public static final String TYPE_TEXT = "text";

	String type;
	String question_type;
	String body;
	String id;
	LocationObject location;
	UniversalUser question_author;
	UniversalUser question_to;
	String serial_id;
	ArrayList<TagObject> tags;
	MediaObject media;
	int ask_count;
	List<Askers> askers;
	String background_image;
	String web_link;
	String short_id;
	boolean is_anonymous;
	long timestamp;

	boolean is_voted;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean is_voted() {
		return is_voted;
	}

	public void setIs_voted(boolean is_voted) {
		this.is_voted = is_voted;
	}

	public String getBackground_image() {
		if (background_image != null)
			return background_image;
		else
			return "http://quizot.com/placeholder.jpg";

	}

	public void setBackground_image(String background_image) {
		this.background_image = background_image;
	}

	public List<Askers> getAskers() {
		return askers;
	}

	public void setAskers(List<Askers> askers) {
		this.askers = askers;
	}

	public int getAsk_count() {
		return ask_count;
	}

	public void setAsk_count(int ask_count) {
		this.ask_count = ask_count;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof QuestionObject) {
			QuestionObject otherObject = (QuestionObject) o;

			return this.id.equals(otherObject.id);
		} else
			return false;

	}

	@Override
	public int hashCode() {

		byte[] bytes = id.getBytes();
		int sum = 0;
		for (int i = 0; i < bytes.length; i++) {
			sum += bytes[i];
		}

		return sum;
	}

	public String getBody() {
		return body;
	}

	public MediaObject getMediaObject() {

		return media;
	}

	public void setMedia_object(MediaObject mediaobj) {

		media = mediaobj;
	}

	public String getQuestion_type() {
		return question_type;
	}

	public void setQuestion_type(String question_type) {
		this.question_type = question_type;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public LocationObject getLocation() {
		return location;
	}

	public void setLocation(LocationObject location) {
		this.location = location;
	}

	public UniversalUser getQuestion_author() {
		return question_author;
	}

	public void setQuestion_author(UniversalUser question_author) {
		this.question_author = question_author;
	}
	
	public UniversalUser getQuestion_to() {
		return question_to;
	}

	public void setQuestion_to(UniversalUser question_to) {
		this.question_to = question_to;
	}

	public String getSerial_id() {
		return serial_id;
	}

	public void setSerial_id(long serial_id) {
		this.serial_id = String.valueOf(serial_id);
	}

	public ArrayList<TagObject> getTags() {
		return tags;
	}

	public void setTags(ArrayList<TagObject> tags) {
		this.tags = tags;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public boolean is_anonymous() {
		return is_anonymous;
	}

	public void setIs_anonymous(boolean is_anonymous) {
		this.is_anonymous = is_anonymous;
	}
	public String getWeb_link(){
		 
		return web_link ; 
	}
	public void setWeb_link(String web_link){
		this.web_link = web_link ; 
	}
	
	public String getShortId(){
		 
		return web_link ; 
	}
	public void getShortId(String short_id){
		this.short_id = short_id ; 
	}
	
	
}
