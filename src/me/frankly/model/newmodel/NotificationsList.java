package me.frankly.model.newmodel;

import java.util.ArrayList;

public class NotificationsList {

	private ArrayList<NotificationModel> notifications;
	private int count;
	private int next_index;
	private long current_time;

	/**
	 * @return the notifications
	 */
	public ArrayList<NotificationModel> getNotifications() {
		return notifications;
	}

	/**
	 * @param notifications
	 *            the notifications to set
	 */
	public void setNotifications(ArrayList<NotificationModel> notifications) {
		this.notifications = notifications;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return the next_index
	 */
	public int getNext_index() {
		return next_index;
	}

	/**
	 * @param next_index
	 *            the next_index to set
	 */
	public void setNext_index(int next_index) {
		this.next_index = next_index;
	}

	/**
	 * @return the current_time
	 */
	public long getCurrent_time() {
		return current_time;
	}

	/**
	 * @param current_time
	 *            the current_time to set
	 */
	public void setCurrent_time(long current_time) {
		this.current_time = current_time;
	}
}
