package me.frankly.model.newmodel;

import java.util.ArrayList;

public class PendingAnswersContainer {

	private ArrayList<PendingAnswer> pendingUploads;

	public ArrayList<PendingAnswer> getPendingUploads() {
		return pendingUploads;
	}

	public void setPendingUploads(ArrayList<PendingAnswer> pendingUploads) {
		this.pendingUploads = pendingUploads;
	}

	// public void setPendingItem(PendingAnswer pendingUpload) {
	//
	// if (pendingUploads != null) {
	// pendingUploads.add(pendingUpload);
	// } else {
	// pendingUploads = new ArrayList<PendingAnswer>();
	// pendingUploads.add(pendingUpload);
	// }
	// }

	public void addPendingItem(PendingAnswer pendingUpload) {

		if (pendingUploads != null) {
			int index = pendingUploads.indexOf(pendingUpload);
			if (index < 0)
				pendingUploads.add(pendingUpload);
			else
				pendingUploads.set(index, pendingUpload);
		} else {
			pendingUploads = new ArrayList<PendingAnswer>();
			pendingUploads.add(pendingUpload);
		}
	}
}
