package me.frankly.model.newmodel;

import java.util.ArrayList;

public class EditableProfileObject extends UniqueEntity {
	String bio;
	String cover_picture;
	String first_name;
	String profile_picture;
	String profile_video;
	String gender;
	String compressedVideoUrl;
	boolean isCompressionValid;

	public String getProperVideoForUploading() {
		if (isCompressionValid)
			return compressedVideoUrl;
		else
			return profile_video;
	}

	public String getCompressedVideoUrl() {
		return compressedVideoUrl;
	}

	public void setCompressedVideoUrl(String compressedVideoUrl) {
		this.compressedVideoUrl = compressedVideoUrl;
	}

	public boolean isIvCompressionValid() {
		return isCompressionValid;
	}

	public void setIsCompressionValid(boolean ivCompressionValid) {
		this.isCompressionValid = ivCompressionValid;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public LocationObject getPermanent_location() {
		return permanent_location;
	}

	public void setPermanent_location(LocationObject permanent_location) {
		this.permanent_location = permanent_location;
	}

	LocationObject permanent_location;

	public long getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(long date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public ArrayList<Interests> getInterests() {
		return interests;
	}

	public void setInterests(ArrayList<Interests> interests) {
		this.interests = interests;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	long date_of_birth;
	ArrayList<Interests> interests;
	String username;
	String last_name;

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getCover_picture() {
		return cover_picture;
	}

	public void setCover_picture(String cover_picture) {
		this.cover_picture = cover_picture;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getProfile_video() {
		return profile_video;
	}

	public void setProfile_video(String profile_video) {
		this.profile_video = profile_video;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

}
