package me.frankly.model.newmodel;

import java.util.ArrayList;
import java.util.HashMap;

import me.frankly.util.MyUtilities;

public class UniversalUser extends UniqueEntity {

	int answer_count;
	int user_type;
	public final static int TYPE_REGULAR = 1;
	public final static int TYPE_CELEB = 2;
	public int getUser_type() {
		return user_type;
	}

	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}
	
	
	String bio;
	String cover_picture;
	String first_name;
	int follower_count;
	int following_count;
	String gender;
	ArrayList<MediaObject> uploads;
	ArrayList<Interests> interests;
	String last_name;
	int likes_count;
	String created_from;
	int view_count;
	String user_title;
	String web_link ;
	String full_name = "Frankly User";
	String channel_id;
	HashMap<String, String> profile_videos;

	// ProfileVideos profile_videos;

	public HashMap<String, String> getProfile_videos() {
		return profile_videos;
	}

	public void setProfile_videos(HashMap<String, String> profile_videos) {
		this.profile_videos = profile_videos;
	}

	public String getUser_title() {
		return user_title;
	}

	public void setUser_title(String user_title) {
		this.user_title = user_title;
	}

	public int getView_count() {
		return view_count;
	}

	public void setView_count(int view_count) {
		this.view_count = view_count;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}


	boolean active;
	boolean requested;
	LocationObject location;
	String profile_picture;
	String username;
	long last_updated;
	String serial_id;
	boolean is_follower;
	boolean is_following;
	boolean allow_anonymous_question;
	ArrayList<UniversalUser> common_followers;
	int common_followers_count;
	String profile_video;

	public String getProfile_video() {
		return profile_video;
	}

	public void setProfile_video(String profile_video) {
		this.profile_video = profile_video;
	}

	public boolean isRequested() {
		return requested;
	}

	public void setRequested(boolean requested) {
		this.requested = requested;
	}

	public String getCreated_from() {
		return created_from;
	}

	public void setCreated_from(String created_from) {
		this.created_from = created_from;
	}

	public ArrayList<UniversalUser> getCommon_followers() {
		return common_followers;
	}

	public void setCommon_followers(ArrayList<UniversalUser> common_followers) {
		this.common_followers = common_followers;
	}

	public int getCommon_followers_count() {
		return common_followers_count;
	}

	public void setCommon_followers_count(int common_followers_count) {
		this.common_followers_count = common_followers_count;
	}

	public boolean is_follower() {
		return is_follower;
	}

	public ArrayList<MediaObject> getUploads() {
		return uploads;
	}

	/**
	 * @return the following_count
	 */
	public int getFollowing_count() {
		return following_count;
	}

	/**
	 * @return the full name of the user
	 */

	public String getFull_name() {
		if (last_name != null && first_name != null && !first_name.isEmpty()
				&& !last_name.isEmpty()) {
			full_name = MyUtilities.toCamelCase(first_name) + " "
					+ MyUtilities.toCamelCase(last_name);
		} else if (first_name != null && !first_name.isEmpty()) {

			full_name = MyUtilities.toCamelCase(first_name);
		}
		// Checking if the full_name is not an empty string
		if (full_name.isEmpty())
			return "Frankly User";
		else
			return full_name;
	}

	/**
	 * @param following_count
	 *            the following_count to set
	 */
	public void setFollowing_count(int following_count) {
		this.following_count = following_count;
	}

	public void setUploads(ArrayList<MediaObject> uploads) {
		this.uploads = uploads;
	}

	public void setIs_follower(boolean is_follower) {
		this.is_follower = is_follower;
	}

	public boolean is_following() {
		return is_following;
	}

	public void setIs_following(boolean is_following) {
		this.is_following = is_following;
	}

	public String getSerial_id() {
		return serial_id;
	}

	public void setSerial_id(long serial_id) {
		this.serial_id = String.valueOf(serial_id);
	}

	public long getLast_updated() {
		return last_updated;
	}

	public void setLast_updated(long last_updated) {
		this.last_updated = last_updated;
	}

	public int getAnswer_count() {
		return answer_count;
	}

	public void setAnswer_count(int answer_count) {
		this.answer_count = answer_count;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getCover_picture() {
		if (cover_picture != null)
			return cover_picture;
		else
			return null;

	}

	public void setCover_picture(String cover_picture) {
		this.cover_picture = cover_picture;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public int getFollower_count() {
		return follower_count;
	}

	public void setFollower_count(int follower_count) {
		this.follower_count = follower_count;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Interests> getInterests() {
		return interests;
	}

	public void setInterests(ArrayList<Interests> interests) {
		this.interests = interests;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public int getLikes_count() {
		return likes_count;
	}

	public void setLikes_count(int likes_count) {
		this.likes_count = likes_count;
	}

	public LocationObject getLocation() {
		return location;
	}

	public void setLocation(LocationObject location) {
		this.location = location;
	}

	public String getProfile_picture() {
		if (profile_picture != null) {
			return profile_picture;
		} else {

			return null;

		}
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isAllow_anonymous_question() {
		return allow_anonymous_question;
	}

	public void setAllow_anonymous_question(boolean allow_anonymous_question) {
		this.allow_anonymous_question = allow_anonymous_question;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getWeb_link(){
	 
		return web_link ; 
	}
	public void setWeb_link(String web_link){
		this.web_link = web_link ; 
	}

}
