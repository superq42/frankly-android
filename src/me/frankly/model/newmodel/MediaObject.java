package me.frankly.model.newmodel;

public class MediaObject {
	String media_url;
	String thumbnail_url;
	String media_id;
	
	
	
	long uploaded_at;

	public String getMedia_url() {
		return media_url;
	}

	public void setMedia_url(String media_url) {
		this.media_url = media_url;
	}

	public String getThumbnail_url() {
		return thumbnail_url;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public long getUploaded_at() {
		return uploaded_at;
	}

	public void setUploaded_at(long uploaded_at) {
		this.uploaded_at = uploaded_at;
	}

}
