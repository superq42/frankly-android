package me.frankly.model.newmodel;

public class SharePostObject {
	private String platform;
	private String post_id;
	
	public String getPlatform()
	{
		return platform;
	}
	public String getPostId()
	{
		return post_id;
	}
	
	public void setPlatform(String platform)
	{
		this.platform = platform;
	}
	public void setPostId(String post_id)
	{
		this.post_id=post_id;
	}
	
}
