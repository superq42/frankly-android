package me.frankly.model.newmodel;

public class ChannelIcon {
	public static final String TYPE_USER = "icon_user";
	private String type;

	private UniversalUser user;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UniversalUser getUser() {
		return user;
	}

	public void setUser(UniversalUser user) {
		this.user = user;
	}

}
