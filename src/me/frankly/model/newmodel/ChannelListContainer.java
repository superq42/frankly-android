package me.frankly.model.newmodel;

import java.util.ArrayList;

public class ChannelListContainer {
	private ArrayList<ChannelObject> channel_list;

	public ArrayList<ChannelObject> getChannel_list() {
		return channel_list;
	}

	public void setChannel_list(ArrayList<ChannelObject> channel_list) {
		this.channel_list = channel_list;
	}
}
