package me.frankly.model.newmodel;

public class InviteCelebrityRequest {
	private String invitable_id;
	
	public void setInvitableId(String id) {
		this.invitable_id = id;
	}
	
	public String getInvitableId() {
		return this.invitable_id;
	}
}
