package me.frankly.model.newmodel;

import android.content.Context;
import android.provider.Settings.Secure;

public class DeviceInfo {
	private String device_id;
	private String model;
	private String build_version;
	private String API_LEVEL;
	private String gcm_id;

	public DeviceInfo(Context ctx) {
		device_id = Secure.getString(ctx.getContentResolver(),
				Secure.ANDROID_ID);

		build_version = System.getProperty("os.version") + "("
				+ android.os.Build.VERSION.INCREMENTAL + ")";
		API_LEVEL = String.valueOf(android.os.Build.VERSION.SDK_INT);
		model = android.os.Build.MODEL + " (" + android.os.Build.PRODUCT + ")";
	}

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBuild_version() {
		return build_version;
	}

	public void setBuild_version(String build_version) {
		this.build_version = build_version;
	}

	public String getAPI_LEVEL() {
		return API_LEVEL;
	}

	public void setAPI_LEVEL(String API_LEVEL) {
		this.API_LEVEL = API_LEVEL;
	}

	public String getGcm_id() {
		return gcm_id;
	}

	public void setGcm_id(String gcm_id) {
		this.gcm_id = gcm_id;
	}
}
