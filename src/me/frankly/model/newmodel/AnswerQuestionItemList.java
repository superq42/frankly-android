package me.frankly.model.newmodel;

import java.util.ArrayList;

import me.frankly.model.AnswerQuestionDataHolder;

public class AnswerQuestionItemList {

	ArrayList<AnswerQuestionDataHolder> questions;
	int count, total;
	String next_index = "0";

	public ArrayList<AnswerQuestionDataHolder> getItem() {
		// questions.removeAll(Collections.singleton(null)) ;
		
	
		return questions;
	}

	public void setQuestions(ArrayList<AnswerQuestionDataHolder> questions) {
       
		this.questions = questions;

	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getTotal() {
		return total;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCount() {
		return count;
	}

	public void setNext(String next) {
		this.next_index = next;
	}

	public String getNext() {
		return next_index;
	}

	public void removeNulls() {
		 
		ArrayList<AnswerQuestionDataHolder> toRemove = new ArrayList<AnswerQuestionDataHolder>();
	if(this.questions != null){
		for (AnswerQuestionDataHolder ask : this.questions) {
		    if (ask.getQuestion() == null) {
		    	
		        toRemove.add(ask);
		    }
		}
		
		this.questions.removeAll(toRemove);
	}
	}

}
