package me.frankly.model.newmodel;

public class AppUpdateRequest {

	private boolean hard_update;
	private boolean soft_update;

	public void setHard_update(boolean value) {
		this.hard_update = value;
	}

	public void setSoft_update(boolean value) {
		this.soft_update = value;
	}
	
	public boolean getHard_update() {
		return this.hard_update;
	}
	
	public boolean getSoft_update() {
		return this.soft_update;
	}
}
