package me.frankly.model.response;

import java.util.ArrayList;

public class CategoryCelebList {

	private ArrayList<CategoryCeleb> results;

	public ArrayList<CategoryCeleb> getResults() {
		return results;
	}

	public void setResults(ArrayList<CategoryCeleb> results) {
		this.results = results;
	}

}
