package me.frankly.model.response;

import java.util.HashSet;

import me.frankly.model.newmodel.PendingAnswer;
import me.frankly.model.newmodel.PostObject;
import me.frankly.model.newmodel.UniqueEntity;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.newmodel.UserQuestionsContainer;

public class DiscoverableObjectsContainer extends UniqueEntity {
	public static final String TYPE_POST = "post";
	public static final String TYPE_USER = "user";
	public static final String TYPE_QUESTION = "questions";
	public static final String TYPE_UPLOAD = "upload_profile_video";
	public static final String TYPE_PENDINGANSWERUPLOAD = "pending_answer_upload";
	// public static final HashSet<String> ACCEPTED_TYPE = new
	// HashSet<String>();
	public static HashSet<String> ACCEPTED_TYPE = new HashSet<String>();
	static {
		ACCEPTED_TYPE.add(TYPE_POST);
		ACCEPTED_TYPE.add(TYPE_QUESTION);
		ACCEPTED_TYPE.add(TYPE_USER);
		ACCEPTED_TYPE.add(TYPE_UPLOAD);
	}

	private String type;
	private PostObject post;
	private UniversalUser user;
	private UserQuestionsContainer questions;
	private PendingAnswer pendingAnswer;

	public UniversalUser getUser() {
		return user;
	}

	public void setUser(UniversalUser user) {
		this.user = user;
	}

	public PendingAnswer getPendingAnswer() {
		return this.pendingAnswer;
	}

	public void setPendingAnswer(PendingAnswer pendingAnswer) {
		this.pendingAnswer = pendingAnswer;
	}

	public UserQuestionsContainer getQuestions() {
		return questions;
	}

	public void setQuestions(UserQuestionsContainer questions) {
		this.questions = questions;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public PostObject getPost() {
		return post;
	}

	public void setPost(PostObject post) {
		this.post = post;
	}

	@Override
	public String getId() {
		if (id == null) {
			StringBuilder idBuilder = new StringBuilder(type);
			idBuilder.append('#');

			if (type.equals(TYPE_POST))
				idBuilder.append(getPost().getId());
			else if (type.equals(TYPE_QUESTION))
				idBuilder.append(getQuestions().getId());
			else if (type.equals(TYPE_USER))
				idBuilder.append(getUser().getId());
			else if (type.equals(TYPE_PENDINGANSWERUPLOAD))
				idBuilder.append(getPendingAnswer().getQuestionObject().getId());
			id = idBuilder.toString();
		}
		return id;

	}
}
