package me.frankly.model.response;

import java.util.ArrayList;

public class FeedBackList {

	private ArrayList<FeedBackUser> users;
	
	public ArrayList<FeedBackUser> getUsers() {
		return this.users;
	}
	
	public void setUsers(ArrayList<FeedBackUser> res) {
		this.users = res;
	}
}
