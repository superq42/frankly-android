package me.frankly.model.response;

import java.util.ArrayList;

import me.frankly.model.newmodel.QuestionObjectHolder;

public class PaginatedQuestionObjectHolder {
	private ArrayList<QuestionObjectHolder> questions;
	private int count;
	private String next_index = "0";
	private ArrayList<QuestionObjectHolder> current_user_questions;
	
	public ArrayList<QuestionObjectHolder> getCurrentUseQuestions() {
		return current_user_questions;
	}

	public void setCurrentUseQuestions(ArrayList<QuestionObjectHolder> questions) {
		this.questions = current_user_questions;
	} 

	public ArrayList<QuestionObjectHolder> getQuestions() {
		return questions;
	}

	public void setQuestions(ArrayList<QuestionObjectHolder> questions) {
		this.questions = questions;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getNext() {
		return next_index;
	}

	public void setNext(String next) {
		this.next_index = next;
	}

}
