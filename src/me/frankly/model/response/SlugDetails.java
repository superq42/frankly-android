package me.frankly.model.response;

import me.frankly.model.newmodel.PostObject;
import me.frankly.model.newmodel.QuestionObject;

/**
 * This class will either have post or question object
 * 
 * @note response also has a field is_answered, but we will only use null checks
 *       for post and question fields, as these check have to be there anyways
 */
public class SlugDetails {

	private PostObject post;
	private QuestionObject question;

	/**
	 * @return the post
	 */
	public PostObject getPost() {
		return post;
	}

	/**
	 * @param post
	 *            the post to set
	 */
	public void setPost(PostObject post) {
		this.post = post;
	}

	/**
	 * @return the question
	 */
	public QuestionObject getQuestion() {
		return question;
	}

	/**
	 * @param question
	 *            the question to set
	 */
	public void setQuestion(QuestionObject question) {
		this.question = question;
	}
}
