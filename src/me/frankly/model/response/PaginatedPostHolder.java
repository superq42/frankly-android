package me.frankly.model.response;

import java.util.ArrayList;

public class PaginatedPostHolder {
	int next_index;
	ArrayList<DiscoverableObjectsContainer> stream;
	DiscoverableObjectsContainer header;

	public DiscoverableObjectsContainer getHeader() {
		return header;
	}

	public void setHeader(DiscoverableObjectsContainer header) {
		this.header = header;
	}

	public ArrayList<DiscoverableObjectsContainer> getStream() {
		return stream;
	}

	public void setStream(ArrayList<DiscoverableObjectsContainer> stream) {
		this.stream = stream;
	}

	public int getNext_index() {
		return next_index;
	}

	public void setNext_index(int next_index) {
		this.next_index = next_index;
	}
}
