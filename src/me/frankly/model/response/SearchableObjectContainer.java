package me.frankly.model.response;

import java.util.HashSet;

import me.frankly.model.newmodel.UniqueEntity;
import me.frankly.model.newmodel.UniversalUser;

public class SearchableObjectContainer extends UniqueEntity {
	public static final String TYPE_USER = "user";
	public static final String TYPE_INVITABLE_USER = "invitable";
	public static final HashSet<String> ACCEPTED_TYPE = new HashSet<String>();
	static {
		ACCEPTED_TYPE.add(TYPE_INVITABLE_USER);
		ACCEPTED_TYPE.add(TYPE_USER);
	}
	private String type;
	private UniversalUser user;
	private InvitableUser invitable;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UniversalUser getUser() {
		return user;
	}

	public void setUser(UniversalUser user) {
		this.user = user;
	}

	public void setInvitable(InvitableUser invitable) {
		this.invitable = invitable;
	}

	public InvitableUser getInvitable() {
		return invitable;
	}

	@Override
	public String getId() {
		if (id == null) {
			StringBuilder idBuilder = new StringBuilder(type);
			idBuilder.append('#');
			if (type.equals(TYPE_USER))
				idBuilder.append(getUser().getId());
			else if (type.equals(TYPE_INVITABLE_USER))
				idBuilder.append(getInvitable().getId());
			id = idBuilder.toString();
		}
		return id;

	}
}
