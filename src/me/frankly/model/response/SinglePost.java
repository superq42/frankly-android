package me.frankly.model.response;

import me.frankly.model.newmodel.PostObject;

public class SinglePost
{
    PostObject post;

	public PostObject getPost() {
		return post;
	}

	public void setPost(PostObject post) {
		this.post = post;
	}
}
