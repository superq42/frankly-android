package me.frankly.model.response;

public class LoginResponse {
	private boolean success;
	private String access_token;
	private String username;
	private String id;
	private boolean new_user;
	private int user_type;
	
	public boolean isNew_user() {
		return new_user;
	}

	public void setNew_user(boolean new_user) {
		this.new_user = new_user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void setUserType(int c) {
		this.user_type = c;
	}
	
	public int getUserType() {
		return this.user_type;
	}
}
