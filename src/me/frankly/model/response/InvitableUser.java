package me.frankly.model.response;

public class InvitableUser {

	private String id;
	private String first_name;
	private String last_name;
	private String username;
	private String twitter_handle;
	private String email;
	private String twitter_text;
	private String mail_text;
	private String mail_subject;
	private boolean has_invited;
	private int cur_invite_count;
	private int max_invite_count;
	private String profile_picture;
	private String user_title;
	private String bio;

	private String displayName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTwitter_handle() {
		return twitter_handle;
	}

	public void setTwitter_handle(String twitter_handle) {
		this.twitter_handle = twitter_handle;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTwitter_text() {
		return twitter_text;
	}

	public void setTwitter_text(String twitter_text) {
		this.twitter_text = twitter_text;
	}

	public String getMail_text() {
		return mail_text;
	}

	public void setMail_text(String mail_text) {
		this.mail_text = mail_text;
	}

	public boolean isHas_invited() {
		return has_invited;
	}

	public void setHas_invited(boolean has_invited) {
		this.has_invited = has_invited;
	}

	public int getCur_invite_count() {
		return cur_invite_count;
	}

	public void setCur_invite_count(int cur_invite_count) {
		this.cur_invite_count = cur_invite_count;
	}

	public int getMax_invite_count() {
		return max_invite_count;
	}

	public void setMax_invite_count(int max_invite_count) {
		this.max_invite_count = max_invite_count;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	public String getUser_title() {
		return user_title;
	}

	public void setUser_title(String user_title) {
		this.user_title = user_title;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getDisplayName() {
		if (displayName == null) {
			StringBuilder displayNameBuilder = new StringBuilder();
			if (first_name != null && !first_name.isEmpty()) {
				displayNameBuilder.append(first_name);
				if (last_name != null && !last_name.isEmpty())
					displayNameBuilder.append(" ").append(last_name);
			} else
				displayNameBuilder.append(username);
			displayName = displayNameBuilder.toString();
		}
		return displayName;
	}

	public String getMail_subject() {
		return mail_subject;
	}

	public void setMail_subject(String mail_subject) {
		this.mail_subject = mail_subject;
	}
}
