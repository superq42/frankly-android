package me.frankly.model.response;

public class FeedBackUser {

	private String profile_picture;
	
	public void setProfilePicture(String pic) {
		this.profile_picture = pic;
	}
	
	public String getProfilePicture() {
		return this.profile_picture;
	}
}
