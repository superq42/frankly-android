package me.frankly.model.response;

import java.util.ArrayList;

import me.frankly.model.newmodel.UniversalUser;

public class CategoryCeleb {

	private String category_name;
	private ArrayList<UniversalUser> users;

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public ArrayList<UniversalUser> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<UniversalUser> users) {
		this.users = users;
	}
}
