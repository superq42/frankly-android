package me.frankly.model;

import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.UniqueEntity;

public class AnswerQuestionDataHolder extends UniqueEntity {

	/*
	 * This class might contain other object types in the future starting with
	 * just questions
	 */
	public static final String TYPE_QUESTION = "question";
	public String type;
	public String next;
	private QuestionObject question;

	public QuestionObject getQuestion() {
		return question;
	}

	public void setUser(QuestionObject question) {
		this.question = question;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getNext() {
		return this.next;
	}
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return this.question.getId();
	}
}
