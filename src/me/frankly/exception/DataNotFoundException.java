package me.frankly.exception;


public class DataNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String source;

	public DataNotFoundException(String source) {
		super();
		this.source = source;

	}

	public DataNotFoundException(String message, String source) {
		super(message);
		this.source = source;
	}

	public DataNotFoundException(String message, Throwable t, String source) {
		super(message, t);
		this.source = source;
	}

	public DataNotFoundException(Throwable t, String source) {
		super(t);
		this.source = source;
	}

	public String getSource() {
		return source;
	}

}