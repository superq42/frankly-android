package me.frankly.exception;

public enum ErrorCode {
	UNAUTHORISED_ACCESS(403), BAD_REQUEST(400), USER_ALREADY_EXISTS(409), USER_NOT_FOUND(
			404), USERNAME_NOT_AVAILABLE(409), NO_INTERNET_CONNECTION(1011), SHIT_HAPPENED(
			1022), TOKEN_EXPIRED(403), SLOW_INTERNET(502);

	private int error_code;

	private ErrorCode(int error_code) {
		this.error_code = error_code;
	}

	public int getErrorCode() {
		return error_code;
	}
}
