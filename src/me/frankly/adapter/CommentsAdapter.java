package me.frankly.adapter;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.model.newmodel.CommentObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.MyUtilities;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.BaseActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CommentsAdapter extends BaseAdapter implements OnClickListener {
	private BaseActivity mBaseActivity;
	private ArrayList<CommentObject> commentObjects;

	public CommentsAdapter(BaseActivity mBaseActivity,
			ArrayList<CommentObject> commentObjects) {
		this.commentObjects = commentObjects;
		this.mBaseActivity = mBaseActivity;
	}
	
	public void setData(ArrayList<CommentObject> data){
		this.commentObjects = data;
	}

	@Override
	public int getCount() {
		return commentObjects.size();
	}

	@Override
	public Object getItem(int position) {
		return commentObjects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TextView bodyTextView, authorTextView;
		ViewHolder holder;
		CommentObject thisCommentObject = commentObjects.get(position);
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(mBaseActivity);
			convertView = inflater.inflate(R.layout.single_comment_item, null);
			holder = new ViewHolder();
			holder.bodyTextView = (TextView) convertView
					.findViewById(R.id.tv_comment_body);
			holder.picImageView = (CircularImageView) convertView
					.findViewById(R.id.iv_profile_pic);
			holder.timeTextView = (TextView) convertView
					.findViewById(R.id.tv_comment_time);

			holder.bodyTextView = holder.bodyTextView;
			holder.picImageView = holder.picImageView;
			holder.timeTextView = holder.timeTextView;

			holder.picImageView.setOnClickListener(this);
			holder.bodyTextView.setOnClickListener(this);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		

		StringBuilder textBuilder = new StringBuilder("<font color='#ea6153'>")
				.append(thisCommentObject.getComment_author()
						.getFull_name())
				.append("</font> ").append(thisCommentObject.getBody());
		
		holder.bodyTextView.setText(Html.fromHtml(textBuilder.toString()));

		textBuilder = null;
		
		/*holder.bodyTextView.setText(thisCommentObject.getBody());
		holder.authorTextView.setText(thisCommentObject.getComment_author()
				.getFull_name());*/
		holder.picImageView.setImageUrl(CircularImageView
				.getCustomDisplayOptions(), thisCommentObject
				.getComment_author().getProfile_picture());

		if (MyUtilities.getProperTimeString(thisCommentObject.getTimestamp()) != null) {
			Log.e("CommentTime", "Value" + thisCommentObject.getTimestamp());
			holder.timeTextView.setText(MyUtilities
					.getProperTimeString(thisCommentObject.getTimestamp()));
		}
		holder.picImageView.setTag(position);
		holder.bodyTextView.setTag(position);
		View dividerLine = convertView.findViewById(R.id.view_separator);

		/**
		 * making last divider invisible
		 */
		if(position==commentObjects.size()-1){
			dividerLine.setVisibility(View.INVISIBLE);
		}else{
			dividerLine.setVisibility(View.VISIBLE);
		}
		return convertView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_profile_pic:
		case R.id.tv_comment_body:
			UniversalUser user = commentObjects.get((Integer) v.getTag()).getComment_author() ; 
			mBaseActivity.profileActivity(null,user);
			MixpanelUtils.sendProfileNavigationEvent(user.getId(), user.getUsername(),user.getUsername(), 
					Constant.ScreenSpecs.SCREEN_COMMENT,
					MixpanelUtils.NAVIGATION_PROFILE, mBaseActivity); 
			break;
		}

	}

	static class ViewHolder {
		TextView bodyTextView, timeTextView;
		CircularImageView picImageView;
	}

}
