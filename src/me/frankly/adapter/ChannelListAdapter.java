package me.frankly.adapter;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.R.color;
import me.frankly.config.AppConfig;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.ChannelSelectedListener;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.ChannelIcon;
import me.frankly.model.newmodel.ChannelIconList;
import me.frankly.model.newmodel.ChannelObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.newmodel.postrequest.UserIdOnlyRequest;
import me.frankly.util.JsonUtils;
import me.frankly.view.BariolTextView;
import me.frankly.view.CircularImageView;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ChannelListAdapter extends BaseAdapter implements OnClickListener,
		RequestListener {

	private static final int VIEW_TYPE_BANNER = 0;
	private static final int BANNER_ID = 00000;
	private static final int VIEW_TYPE_SEARCH = 1;
	private static final int VIEW_TYPE_CAT_CELEB = 2;
	private Context mCtx;
	private ArrayList<ChannelObject> mCnls;
	private ChannelSelectedListener mChannelSelectedListener;

	private DisplayImageOptions picDisplayOptions;
	private OnFollowedListener onFollowedListener;
	private Handler handler;

	public ChannelListAdapter(Context ctx, ArrayList<ChannelObject> channels) {
		mCtx = ctx;
		mCnls = channels;
		picDisplayOptions = CircularImageView.getCustomDisplayOptions();
		handler = new Handler();
	}

	public void setOnChannelSelectedListener(ChannelSelectedListener newListener) {
		mChannelSelectedListener = newListener;
	}

	@Override
	public int getCount() {

		return calculateCount();
	}

	private int calculateCount() {
		/*
		 * int count = 0; for (ChannelObject o : mCnls) { if
		 * (o.getType().equals(ChannelObject.TYPE_BANNER)) count++; else if
		 * (o.getType().equals(ChannelObject.TYPE_SEARCH)) { // count++; count
		 * += o.getViews().size(); } }
		 */
		if (mCnls.isEmpty())
			return 0;

		return mCnls.get(0).getViews().size();
	}

	@Override
	public int getViewTypeCount() {
		return 3;
	}

	@Override
	public int getItemViewType(int position) {
		if (mCnls.size() > position)
			return mCnls.get(position).getType()
					.equals(ChannelObject.TYPE_BANNER) ? VIEW_TYPE_BANNER
					: VIEW_TYPE_CAT_CELEB;
		else
			return VIEW_TYPE_CAT_CELEB;

	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		/*
		 * if (mCnls.size() > position) return mCnls.get(position).getType()
		 * .equals(ChannelObject.TYPE_BANNER) ? getBannerView( position,
		 * convertView, parent) : getSearchView(position, convertView, parent);
		 * else { ChannelObject searchChannelObject = mCnls.get(mCnls.size() -
		 * 1);
		 */
		return getCatCelebView(convertView,
				mCnls.get(0).getViews().get(position)/*
													 * .getViews() .get(position
													 * - mCnls.size())
													 */);
		// }
		/*
		 * return
		 * mCnls.get(position).getType().equals(ChannelObject.TYPE_BANNER) ?
		 * getBannerView( position, convertView, parent) :
		 * getSearchView(position, convertView, parent);
		 */

	}

	private View getBannerView(int position, View convertView, ViewGroup parent) {
		TextView channelTitleTextView;
		if (convertView == null) {
			convertView = new BariolTextView(mCtx);
			channelTitleTextView = (TextView) convertView;
			channelTitleTextView.setGravity(Gravity.CENTER);
			channelTitleTextView.setTextSize(25);
			channelTitleTextView.setTextColor(mCtx.getResources().getColor(
					color.white_pure));
			channelTitleTextView.setPadding(0, 15, 0, 15);
			LayoutParams p = new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT);
			channelTitleTextView.setLayoutParams(p);
			channelTitleTextView.setOnClickListener(this);
			channelTitleTextView.setId(BANNER_ID);

		} else {
			channelTitleTextView = (TextView) convertView;
		}
		convertView.setTag(mCnls.get(position).getChannel_id());
		channelTitleTextView.setText(mCnls.get(position).getName());
		return convertView;
	}

	private View getSearchView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mCtx).inflate(
					R.layout.search_channel, null);
		}
		View searchView = convertView.findViewById(R.id.rl_search);
		searchView.setOnClickListener(this);

		return convertView;
	}

	private View getCatCelebView(View convertView, ChannelIconList iconList) {

		ViewHolder h;
		if (convertView == null) {
			convertView = LayoutInflater.from(mCtx).inflate(
					R.layout.list_item_category_celebs, null);
			h = new ViewHolder();
			h.seeMoreView = convertView.findViewById(R.id.tv_see_more);
			h.categoryNameTextView = (TextView) convertView
					.findViewById(R.id.tv_category_name);
			h.followOneView = (TextView) convertView
					.findViewById(R.id.tv_ask_one);
			h.followTwoView = (TextView) convertView
					.findViewById(R.id.tv_ask_two);
			h.followThreeView = (TextView) convertView
					.findViewById(R.id.tv_ask_three);
			h.nameOneTextView = (TextView) convertView
					.findViewById(R.id.tv_name_one);
			h.nameTwoTextView = (TextView) convertView
					.findViewById(R.id.tv_name_two);
			h.nameThreeTextView = (TextView) convertView
					.findViewById(R.id.tv_name_three);
			h.profileOneImageView = (ImageView) convertView
					.findViewById(R.id.iv_profile_pic_one);
			h.profileTwoImageView = (ImageView) convertView
					.findViewById(R.id.iv_profile_pic_two);
			h.profileThreeImageView = (ImageView) convertView
					.findViewById(R.id.iv_profile_pic_three);
			h.ivFollowOne = (ImageView) convertView
					.findViewById(R.id.iv_follow_one);
			h.ivFollowTwo = (ImageView) convertView
					.findViewById(R.id.iv_follow_two);
			h.ivFollowThree = (ImageView) convertView
					.findViewById(R.id.iv_follow_three);
			h.titleOneTextView = (TextView) convertView
					.findViewById(R.id.tv_title_one);
			h.titleTwoTextView = (TextView) convertView
					.findViewById(R.id.tv_title_two);
			h.titleThreeTextView = (TextView) convertView
					.findViewById(R.id.tv_title_three);
			h.profileOneImageView.setOnClickListener(this);
			h.profileTwoImageView.setOnClickListener(this);
			h.profileThreeImageView.setOnClickListener(this);
			h.ivFollowOne.setOnClickListener(this);
			h.ivFollowTwo.setOnClickListener(this);
			h.ivFollowThree.setOnClickListener(this);
			h.followOneView.setOnClickListener(this);
			h.followTwoView.setOnClickListener(this);
			h.followThreeView.setOnClickListener(this);
			h.titleOneTextView.setOnClickListener(this);
			h.titleTwoTextView.setOnClickListener(this);
			h.titleThreeTextView.setOnClickListener(this);
			h.nameOneTextView.setOnClickListener(this);
			h.nameTwoTextView.setOnClickListener(this);
			h.nameThreeTextView.setOnClickListener(this);
			h.seeMoreView.setOnClickListener(this);
			h.followOneView.setEms(6);
			h.followTwoView.setEms(6);
			h.followThreeView.setEms(6);
			h.followOneView.setVisibility(View.GONE);
			h.followTwoView.setVisibility(View.GONE);
			h.followThreeView.setVisibility(View.GONE);

			// h.seeMoreView.setVisibility(View.GONE);
			convertView.setOnClickListener(this); // Do nothing -- fix to solved
													// false click on search
													// results
			convertView.setTag(h);
		} else
			h = (ViewHolder) convertView.getTag();
		h.categoryNameTextView.setText(iconList.getName());
		h.seeMoreView.setTag(iconList.getName());

		if (!iconList.getIcons().isEmpty()) {
			h.nameOneTextView.setVisibility(View.VISIBLE);
			h.profileOneImageView.setVisibility(View.VISIBLE);
			h.titleOneTextView.setVisibility(View.VISIBLE);
			h.profileOneImageView.setVisibility(View.VISIBLE);
			// h.followOneView.setVisibility(View.VISIBLE);
			h.nameOneTextView.setText(iconList.getIcons().get(0).getUser()
					.getFull_name());
			ImageLoader.getInstance().displayImage(
					iconList.getIcons().get(0).getUser().getProfile_picture(),
					h.profileOneImageView, picDisplayOptions);
			h.titleOneTextView.setText(iconList.getIcons().get(0).getUser()
					.getUser_title());
			h.profileOneImageView.setTag(iconList.getIcons().get(0).getUser());
			h.followOneView
					.setTag(iconList.getIcons().get(0).getUser().getId());
			h.nameOneTextView.setText(iconList.getIcons().get(0).getUser()
					.getFull_name());
			ImageLoader.getInstance().displayImage(
					iconList.getIcons().get(0).getUser().getProfile_picture(),
					h.profileOneImageView, picDisplayOptions);
			h.titleOneTextView.setText(iconList.getIcons().get(0).getUser()
					.getUser_title());
			h.profileOneImageView.setTag(iconList.getIcons().get(0).getUser());
			h.followOneView.setText(iconList.getIcons().get(0).getUser()
					.is_following() ? "Unfollow" : " Follow ");
			if (iconList.getIcons().get(0).getUser().is_following())
				h.ivFollowOne.setImageResource(R.drawable.ic_followed);
			else
				h.ivFollowOne.setImageResource(R.drawable.ic_add_profilepic);
			h.followOneView.setTag(iconList.getIcons().get(0).getUser());
			h.ivFollowOne.setTag(iconList.getIcons().get(0).getUser());
			h.ivFollowOne.setTag(R.id.tv_ask_one, h.followOneView);
			h.ivFollowOne.setTag(R.id.iv_follow_one, h.ivFollowOne);
			h.followOneView.setTag(R.id.iv_follow_one, h.ivFollowOne);
			h.followOneView.setTag(R.id.tv_ask_one, h.followOneView);
			h.profileOneImageView.setTag(R.id.iv_follow_one, h.ivFollowOne);
			h.profileOneImageView.setTag(R.id.tv_ask_one, h.followOneView);
			h.titleOneTextView.setTag(iconList.getIcons().get(0).getUser());
			h.nameOneTextView.setTag(iconList.getIcons().get(0).getUser());

		} else {
			h.nameOneTextView.setVisibility(View.INVISIBLE);
			h.profileOneImageView.setVisibility(View.INVISIBLE);
			h.titleOneTextView.setVisibility(View.INVISIBLE);
			h.profileOneImageView.setVisibility(View.INVISIBLE);
			h.followOneView.setVisibility(View.INVISIBLE);
			h.ivFollowOne.setVisibility(View.INVISIBLE);
			h.nameOneTextView.setText("");
			h.profileOneImageView.setImageDrawable(null);
			h.titleOneTextView.setText("");
			h.profileOneImageView.setTag(null);
			h.followOneView.setTag(null);
			h.ivFollowOne.setTag(null);
		}
		if (iconList.getIcons().size() >= 2) {
			h.nameTwoTextView.setVisibility(View.VISIBLE);
			h.profileTwoImageView.setVisibility(View.VISIBLE);
			h.titleTwoTextView.setVisibility(View.VISIBLE);
			h.profileTwoImageView.setVisibility(View.VISIBLE);
			// h.followTwoView.setVisibility(View.VISIBLE);
			h.nameTwoTextView.setText(iconList.getIcons().get(1).getUser()
					.getFull_name());
			ImageLoader.getInstance().displayImage(
					iconList.getIcons().get(1).getUser().getProfile_picture(),
					h.profileTwoImageView, picDisplayOptions);
			h.titleTwoTextView.setText(iconList.getIcons().get(1).getUser()
					.getUser_title());
			h.profileTwoImageView.setTag(iconList.getIcons().get(1).getUser());
			h.followTwoView.setText(iconList.getIcons().get(1).getUser()

			.is_following() ? "Unfollow" : " Follow ");
			if (iconList.getIcons().get(1).getUser().is_following())
				h.ivFollowTwo.setImageResource(R.drawable.ic_followed);
			else
				h.ivFollowTwo.setImageResource(R.drawable.ic_add_profilepic);
			h.followTwoView.setTag(iconList.getIcons().get(1).getUser());
			h.ivFollowTwo.setTag(iconList.getIcons().get(1).getUser());
			h.ivFollowTwo.setTag(R.id.tv_ask_two, h.followTwoView);
			h.ivFollowTwo.setTag(R.id.iv_follow_two, h.ivFollowTwo);
			h.followTwoView.setTag(R.id.iv_follow_two, h.ivFollowTwo);
			h.followTwoView.setTag(R.id.tv_ask_two, h.followTwoView);
			// if (mMode == MODE_FOLLOW) {
			h.profileTwoImageView.setTag(R.id.iv_follow_two, h.ivFollowTwo);
			h.profileTwoImageView.setTag(R.id.tv_ask_two, h.followTwoView);
			h.titleTwoTextView.setTag(iconList.getIcons().get(1).getUser());
			h.nameTwoTextView.setTag(iconList.getIcons().get(1).getUser());
			// }
		} else {
			h.nameTwoTextView.setVisibility(View.INVISIBLE);
			h.profileTwoImageView.setVisibility(View.INVISIBLE);
			h.titleTwoTextView.setVisibility(View.INVISIBLE);
			h.profileTwoImageView.setVisibility(View.INVISIBLE);
			h.followTwoView.setVisibility(View.INVISIBLE);
			h.ivFollowTwo.setVisibility(View.INVISIBLE);
			h.nameTwoTextView.setText("");
			h.profileTwoImageView.setImageDrawable(null);
			h.titleTwoTextView.setText("");
			h.profileTwoImageView.setTag(null);
			h.followTwoView.setTag(null);
			h.ivFollowTwo.setTag(null);
		}
		if (iconList.getIcons().size() >= 3) {
			h.nameThreeTextView.setVisibility(View.VISIBLE);
			h.profileThreeImageView.setVisibility(View.VISIBLE);
			h.titleThreeTextView.setVisibility(View.VISIBLE);
			h.profileThreeImageView.setVisibility(View.VISIBLE);
			// h.followThreeView.setVisibility(View.VISIBLE);
			h.nameThreeTextView.setText(iconList.getIcons().get(2).getUser()
					.getFull_name());
			ImageLoader.getInstance().displayImage(
					iconList.getIcons().get(2).getUser().getProfile_picture(),
					h.profileThreeImageView, picDisplayOptions);
			h.titleThreeTextView.setText(iconList.getIcons().get(2).getUser()
					.getUser_title());
			h.profileThreeImageView
					.setTag(iconList.getIcons().get(2).getUser());

			h.followThreeView.setTag(iconList.getIcons().get(2).getUser());

			h.followThreeView.setText(iconList.getIcons().get(2).getUser()
					.is_following() ? "Unfollow" : " Follow ");
			if (iconList.getIcons().get(2).getUser().is_following())
				h.ivFollowThree.setImageResource(R.drawable.ic_followed);
			else
				h.ivFollowThree.setImageResource(R.drawable.ic_add_profilepic);
			h.ivFollowThree.setTag(iconList.getIcons().get(2).getUser());
			h.ivFollowThree.setTag(R.id.tv_ask_three, h.followThreeView);
			h.ivFollowThree.setTag(R.id.iv_follow_three, h.ivFollowThree);
			h.followThreeView.setTag(R.id.iv_follow_three, h.ivFollowThree);
			h.followThreeView.setTag(R.id.tv_ask_three, h.followThreeView);
			// if (mMode == MODE_FOLLOW) {
			h.profileThreeImageView.setTag(R.id.iv_follow_three,
					h.ivFollowThree);
			h.profileThreeImageView
					.setTag(R.id.tv_ask_three, h.followThreeView);
			h.titleThreeTextView.setTag(iconList.getIcons().get(2).getUser());
			h.nameThreeTextView.setTag(iconList.getIcons().get(2).getUser());
			// }
		} else {
			h.nameThreeTextView.setVisibility(View.INVISIBLE);
			h.profileThreeImageView.setVisibility(View.INVISIBLE);
			h.titleThreeTextView.setVisibility(View.INVISIBLE);
			h.profileThreeImageView.setVisibility(View.INVISIBLE);
			h.followThreeView.setVisibility(View.INVISIBLE);
			h.ivFollowThree.setVisibility(View.INVISIBLE);
			h.nameThreeTextView.setText("");
			h.profileThreeImageView.setImageDrawable(null);
			h.titleThreeTextView.setText("");
			h.profileThreeImageView.setTag(null);
			h.followThreeView.setTag(null);
			h.ivFollowThree.setTag(null);
		}
		h.seeMoreView.setTag(iconList.getName());

		return convertView;

	}

	@Override
	public void onClick(View v) {
		UniversalUser user;
		ImageView ivFollow;
		TextView tvFollow;
		switch (v.getId()) {
		case R.id.tv_ask_one:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_one) != null
						&& v.getTag(R.id.tv_ask_one) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_one);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_one);
					followToggle(user, ivFollow, tvFollow);
				}
			}
			break;
		case R.id.tv_ask_two:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_two) != null
						&& v.getTag(R.id.tv_ask_two) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_two);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_two);
					followToggle(user, ivFollow, tvFollow);
				}
			}
			break;
		case R.id.tv_ask_three:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_three) != null
						&& v.getTag(R.id.tv_ask_three) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_three);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_three);
					followToggle(user, ivFollow, tvFollow);
				}
			}
			break;
		case R.id.iv_follow_one:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_one) != null
						&& v.getTag(R.id.tv_ask_one) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_one);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_one);
					followToggle(user, ivFollow, tvFollow);
				}
			}
			break;
		case R.id.iv_follow_two:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_two) != null
						&& v.getTag(R.id.tv_ask_two) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_two);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_two);
					followToggle(user, ivFollow, tvFollow);
				}
			}
			break;
		case R.id.iv_follow_three:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_three) != null
						&& v.getTag(R.id.tv_ask_three) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_three);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_three);
					followToggle(user, ivFollow, tvFollow);
				}
			}
			break;
		case R.id.iv_profile_pic_one:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_one) != null
						&& v.getTag(R.id.tv_ask_one) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_one);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_one);
					followToggle(user, ivFollow, tvFollow);
				}
			}
			break;
		case R.id.iv_profile_pic_two:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_two) != null
						&& v.getTag(R.id.tv_ask_two) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_two);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_two);
					followToggle(user, ivFollow, tvFollow);
				}
			}
			break;
		case R.id.iv_profile_pic_three:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (v.getTag(R.id.iv_follow_three) != null
						&& v.getTag(R.id.tv_ask_three) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_three);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_three);
					followToggle(user, ivFollow, tvFollow);
				}
			}

			break;
		case BANNER_ID:
			if (v.getTag() != null) {
				String channelId = (String) v.getTag();
				mChannelSelectedListener.onChannelSelected(channelId);
			}
			break;
		case R.id.rl_search:
			if(AppConfig.DEBUG) {
				Log.d("SA", "rl_search");
			}
			mChannelSelectedListener.onSearchSelected(null);
			break;
		case R.id.tv_title_one:
			Log.d("checkchannel", "title one clicked");
		case R.id.tv_title_two:
			Log.d("checkchannel", "title two clicked");
		case R.id.tv_title_three:
			Log.d("checkchannel", "title three clicked");
		case R.id.tv_name_one:
			Log.d("checkchannel", "name one clicked");
		case R.id.tv_name_two:
			Log.d("checkchannel", "name two clicked");
		case R.id.tv_name_three:
			Log.d("checkchannel", "name three clicked");
			if (v.getTag() != null) {
				user = (UniversalUser) v.getTag();
				mChannelSelectedListener
						.onChannelSelected(user.getChannel_id());
			}
			break;
		case R.id.tv_see_more:
			if (v.getTag() != null) {
				if(Controller.isNetworkConnectedWithMessage(mCtx)){
					String queryString = (String) v.getTag();
					mChannelSelectedListener.onSearchSelected(queryString);
				}

			}
			break;

		}
		/*
		 * if (mChannelSelectedListener != null) { if (v.getTag() != null) {
		 * String channelId = (String) v.getTag();
		 * mChannelSelectedListener.onChannelSelected(channelId); } else
		 * mChannelSelectedListener.onSearchSelected(); }
		 */

	}

	public void followToggle(UniversalUser user, ImageView ivFollow,
			TextView tvFollow) {
		if (Controller.isNetworkConnectedWithMessage(mCtx)) {
			if (!user.is_following()) {
				tvFollow.setText("Unfollow");
				ivFollow.setImageResource(R.drawable.ic_followed);
				Controller.requestfollow(mCtx, user.getId(), this);
				if (onFollowedListener != null)
					onFollowedListener.onFollowed(user.getId());
			} else {
				tvFollow.setText(" Follow ");
				ivFollow.setImageResource(R.drawable.ic_add_profilepic);
				Controller
						.requestUnfollow(mCtx, user.getId(), unfollowListener);
				if (onFollowedListener != null)
					onFollowedListener.onUnfollowed(user.getId());
			}
		}
	}

	public void followToggle(UniversalUser user, ImageView ivFollow) {
		if (Controller.isNetworkConnectedWithMessage(mCtx)) {
			if (!user.is_following()) {
				ivFollow.setImageResource(R.drawable.ic_followed);
				Controller.requestfollow(mCtx, user.getId(), this);
				if (onFollowedListener != null)
					onFollowedListener.onFollowed(user.getId());
			} else {
				ivFollow.setImageResource(R.drawable.ic_add_profilepic);
				Controller
						.requestUnfollow(mCtx, user.getId(), unfollowListener);
				if (onFollowedListener != null)
					onFollowedListener.onUnfollowed(user.getId());
			}
		}
	}

	public void setOnFollowedListener(OnFollowedListener listener) {
		this.onFollowedListener = listener;
	}

	static class ViewHolder {
		TextView categoryNameTextView, nameOneTextView, nameTwoTextView,
				nameThreeTextView, titleOneTextView, titleTwoTextView,
				titleThreeTextView;
		TextView followOneView, followTwoView, followThreeView;
		ImageView profileOneImageView, profileTwoImageView,
				profileThreeImageView, ivFollowOne, ivFollowTwo, ivFollowThree;
		View seeMoreView;
	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		final UserIdOnlyRequest response = (UserIdOnlyRequest) JsonUtils
				.objectify((String) responseObject, UserIdOnlyRequest.class);
		handler.post(new Runnable() {

			@Override
			public void run() {
				UniversalActionListenerCollection.getInstance()
						.onUserFollowChanged(response.getUserId(), true);
			}
		});
		for (ChannelObject channelObject : mCnls) {
			if (channelObject.getType().equals(ChannelObject.TYPE_SEARCH)) {
				for (ChannelIconList temp : channelObject.getViews()) {
					for (ChannelIcon icon : temp.getIcons())
						if (icon.getUser().getId().equals(response.getUserId())) {
							icon.getUser().setIs_following(true);
							return;
						}
				}
			}
		}

	}

	@Override
	public void onRequestError(int errorCode, String message) {

	}

	private RequestListener unfollowListener = new RequestListener() {

		@Override
		public void onRequestStarted() {

		}

		@Override
		public void onRequestError(int errorCode, String message) {

		}

		@Override
		public void onRequestCompleted(Object responseObject) {

			final UserIdOnlyRequest response = (UserIdOnlyRequest) JsonUtils
					.objectify((String) responseObject, UserIdOnlyRequest.class);
			handler.post(new Runnable() {

				@Override
				public void run() {
					UniversalActionListenerCollection.getInstance()
							.onUserFollowChanged(response.getUserId(), false);
				}
			});

			for (ChannelObject channelObject : mCnls) {
				if (channelObject.getType().equals(ChannelObject.TYPE_SEARCH)) {
					for (ChannelIconList temp : channelObject.getViews()) {
						for (ChannelIcon icon : temp.getIcons())
							if (icon.getUser().getId()
									.equals(response.getUserId())) {
								icon.getUser().setIs_following(false);
								return;
							}
					}
				}
			}
		}
	};

	public static interface OnFollowedListener {
		public void onFollowed(String userId);

		public void onUnfollowed(String userId);
	}

}
