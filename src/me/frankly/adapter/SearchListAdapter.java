package me.frankly.adapter;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.ChannelSelectedListener;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.response.InvitableUser;
import me.frankly.model.response.SearchableObjectContainer;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.ShareUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.BaseActivity;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class SearchListAdapter extends BaseAdapter implements OnClickListener {

	private static final int VIEW_TYPE_COUNT = 2;
	private static final int VIEW_TYPE_USER = 0;
	private static final int VIEW_TYPE_INVITABLE_USER = 1;
	private BaseActivity mBaseActivity;
	private ArrayList<SearchableObjectContainer> mResults;
	private DisplayImageOptions picDisplayOptions;
	private ChannelSelectedListener mChannelSelectedListener;

	public SearchListAdapter(BaseActivity mBaseActivity,
			ArrayList<SearchableObjectContainer> results) {
		this.mBaseActivity = mBaseActivity;
		this.mResults = results;
		picDisplayOptions = CircularImageView.getCustomDisplayOptions();

	}

	public void setData(ArrayList<SearchableObjectContainer> data) {
		mResults = data;
	}

	public void setOnChannleSelectedListener(ChannelSelectedListener newListener) {
		mChannelSelectedListener = newListener;
	}

	@Override
	public int getCount() {
		return mResults.size();
	}

	@Override
	public int getViewTypeCount() {
		return VIEW_TYPE_COUNT;
	}

	@Override
	public int getItemViewType(int position) {
		return mResults.get(position).getType()
				.equals(SearchableObjectContainer.TYPE_USER) ? VIEW_TYPE_USER
				: VIEW_TYPE_INVITABLE_USER;
	}

	@Override
	public Object getItem(int position) {
		return mResults.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return mResults.get(position).getType()
				.equals(SearchableObjectContainer.TYPE_USER) ? getUserView(
				position, convertView, parent) : getInvitableView(position,
				convertView, parent);
	}

	private View getUserView(int position, View convertView, ViewGroup parent) {
		UserViewHolder h;
		UniversalUser thisUser = mResults.get(position).getUser();
		if (convertView == null) {
			convertView = LayoutInflater.from(mBaseActivity).inflate(
					R.layout.list_item_search, null);
			h = new UserViewHolder();
			h.picImageView = (ImageView) convertView.findViewById(R.id.iv_pic);
			h.ivFollow = (ImageView) convertView
					.findViewById(R.id.frag_profile_img_plus_profile);
			h.nameTextView = (TextView) convertView.findViewById(R.id.tv_name);
			h.titleTextView = (TextView) convertView
					.findViewById(R.id.tv_title);
			h.actionView = convertView.findViewById(R.id.iv_ask);
			h.nameContainerView = convertView.findViewById(R.id.ll_name);
			
			h.actionView.setOnClickListener(this);
			h.picImageView.setOnClickListener(this);
			h.ivFollow.setOnClickListener(this);
			h.nameContainerView.setOnClickListener(this);
			convertView.setTag(h);
		} else {
			h = (UserViewHolder) convertView.getTag();
		}
		ImageLoader.getInstance().displayImage(thisUser.getProfile_picture(),
				h.picImageView, picDisplayOptions);
		h.nameTextView.setText(thisUser.getFull_name());
		h.titleTextView.setText(thisUser.getUser_title() != null ? thisUser
				.getUser_title() : "");
		h.actionView.setTag(position);
		h.picImageView.setTag(position);
		h.ivFollow.setTag(position);
		h.ivFollow.setTag(R.id.frag_profile_img_plus_profile, h.ivFollow);
		h.picImageView.setTag(R.id.frag_profile_img_plus_profile, h.ivFollow);
		h.nameContainerView.setTag(position);
		if (thisUser.is_following()) {
			Log.d("followsearch", "following");
			h.ivFollow.setImageResource(R.drawable.ic_followed);
		} else {
			Log.d("followsearch", "not following");
			h.ivFollow.setImageResource(R.drawable.ic_add_profilepic);
		}

		return convertView;
	}

	private View getInvitableView(int position, View convertView,
			ViewGroup parent) {
		InvitableUserViewHolder h;
		InvitableUser thisUser = mResults.get(position).getInvitable();
		if (convertView == null) {
			convertView = LayoutInflater.from(mBaseActivity).inflate(
					R.layout.list_item_search_invite, null);
			h = new InvitableUserViewHolder();
			h.actionView = convertView.findViewById(R.id.tv_invite);
			h.inviteCountProgressBar = (SeekBar) convertView
					.findViewById(R.id.sb_invite_count);
			h.inviteCountLayout = (LinearLayout) convertView
					.findViewById(R.id.tv_invite_layout);
			h.inviteMsgTextView = (TextView) convertView
					.findViewById(R.id.tv_invite_message);
			h.nameTextView = (TextView) convertView.findViewById(R.id.tv_name);
			h.picImageView = (ImageView) convertView
					.findViewById(R.id.iv_profile_pic);
			h.titleTextView = (TextView) convertView
					.findViewById(R.id.tv_title);
			h.inviteMotivationTextView = (TextView) convertView
					.findViewById(R.id.tv_invite_motivation);
			h.inviteCountTextView = (TextView) convertView
					.findViewById(R.id.invite_count);
			h.inviteCountLayout = (LinearLayout) convertView
					.findViewById(R.id.invite_container);
			h.actionView.setOnClickListener(this);
			h.inviteCountProgressBar.setEnabled(false);
			h.inviteCountLayout.setOnClickListener(this);
			// h.inviteCountProgressBar.setProgressDrawable(mCtx.getResources()
			// .getDrawable(R.drawable.invite_progress_drawable));
			convertView.setTag(h);
		} else {
			h = (InvitableUserViewHolder) convertView.getTag();
		}
		h.inviteMsgTextView.setText(thisUser.getTwitter_text());
		h.nameTextView.setText(thisUser.getDisplayName());
		h.actionView.setTag(position);
		h.inviteCountLayout.setTag(position);
		ImageLoader.getInstance().displayImage(thisUser.getProfile_picture(),
				h.picImageView, picDisplayOptions);
		h.titleTextView.setText(thisUser.getUser_title() != null ? thisUser
				.getUser_title() : "");
		h.inviteMotivationTextView.setText(Html
				.fromHtml(getHtmlMotivationString(thisUser)));
		h.inviteCountTextView.setText("" + thisUser.getCur_invite_count());
		((TextView) h.actionView).setText("Invite " + thisUser.getFirst_name());

		h.inviteCountProgressBar.setMax(thisUser.getMax_invite_count());
		h.inviteCountProgressBar.setProgress(thisUser.getCur_invite_count());

		int nonZeroCount = thisUser.getCur_invite_count() <= 0 ? 10 : thisUser
				.getCur_invite_count();
		int margin = MyUtilities.SCREEN_WIDTH * nonZeroCount
				/ thisUser.getMax_invite_count();
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) h.inviteCountLayout
				.getLayoutParams();
		params.setMargins(margin, 0, 0, 0);
		h.inviteCountLayout.setLayoutParams(params);
		return convertView;
	}

	@Override
	public void onClick(View v) {

		int pos = (Integer) v.getTag();

		UniversalUser thisUser = mResults.get(pos).getUser();
		switch (v.getId()) {
		case R.id.iv_ask:
			mBaseActivity.askActivity( thisUser);
			MixpanelUtils.sendGenericEvent(thisUser.getId(),
					thisUser.getUsername(),
					String.valueOf(thisUser.getUser_type()),
					Constant.ScreenSpecs.SCREEN_SEARCH, MixpanelUtils.ASK_ME,
					mBaseActivity);
			break;
		/*
		 * case R.id.iv_pic: RouterActivity.profileActivity(mCtx, null,
		 * thisUser); MixpanelUtils.sendGenericEvent(thisUser.getId(),
		 * thisUser.getUsername(), String.valueOf(thisUser.getUser_type()),
		 * Constant.ScreenSpecs.SCREEN_SEARCH, MixpanelUtils.NAVIGATION_PROFILE,
		 * mCtx); break;
		 */
		case R.id.tv_invite_layout:
			Log.d("Invite", "layout clicked to invite");
		case R.id.tv_invite:
			inviteUser(mResults.get((Integer) v.getTag()).getInvitable());

			Log.d("Invite", "text view clicked to invite");
			InvitableUser invitableUser = mResults.get((Integer) v.getTag())
					.getInvitable();
			final String id = invitableUser.getId();
			inviteUser(invitableUser);
			Controller.inviteCelebrity(mBaseActivity, invitableUser.getId(),
					new RequestListener() {

						@Override
						public void onRequestStarted() {
							Log.d("InviteCelebrity", "starting the request....");
						}

						@Override
						public void onRequestError(int errorCode, String message) {
							Log.d("InviteCelebrity",
									"error in the request.... " + message);
						}

						@Override
						public void onRequestCompleted(Object responseObject) {
							Log.d("SearchList", "responseObject : "
									+ responseObject);
							for (int i = 0; i < mResults.size(); i = i + 1) {
								if (mResults
										.get(i)
										.getType()
										.equals(SearchableObjectContainer.TYPE_INVITABLE_USER)
										&& mResults.get(i).getInvitable()
												.getId().equals(id)) {
									mResults.remove(i);
									notifyDataSetChanged();
									break;
								}
							}
						}
					});
			break;
		case R.id.frag_profile_img_plus_profile:
		case R.id.iv_pic:
			if (Controller.isNetworkConnectedWithMessage(mBaseActivity)) {
				if (!thisUser.is_following()) {
					followUser(thisUser);
					((ImageView) v.getTag(R.id.frag_profile_img_plus_profile))
							.setImageResource(R.drawable.ic_followed);
				} else {
					unFollowUser(thisUser);
					((ImageView) v.getTag(R.id.frag_profile_img_plus_profile))
							.setImageResource(R.drawable.ic_add_profilepic);
				}
				UniversalActionListenerCollection.getInstance()
						.onUserFollowChanged(thisUser.getId(),
								thisUser.is_following());
			}
			break;
		case R.id.ll_name:
			UniversalUser user = mResults.get((Integer) v.getTag()).getUser();
			mChannelSelectedListener.onChannelSelected(user.getChannel_id());
			break;

		}

	}

	private void followUser(UniversalUser mUser) {
		String userName = mUser.getId();
		mUser.setIs_following(true);
		Controller.requestfollow(mBaseActivity, userName, new RequestListener() {

			@Override
			public void onRequestStarted() {
			}

			@Override
			public void onRequestError(int errorCode, final String message) {
			}

			@Override
			public void onRequestCompleted(Object responseObject) {
				Log.d("search follow", "follow");
			}
		});
	}

	private void unFollowUser(UniversalUser mUser) {
		String userId = mUser.getId();
		mUser.setIs_following(false);
		Controller.requestUnfollow(mBaseActivity, userId, new RequestListener() {

			@Override
			public void onRequestStarted() {
			}

			@Override
			public void onRequestError(int errorCode, String message) {
			}

			@Override
			public void onRequestCompleted(Object responseObject) {
				Log.d("search follow", "unfollow");
			}
		});
	}

	protected void runOnUiThread(Runnable runnable) {
		// TODO Auto-generated method stub

	}

	private void inviteUser(InvitableUser invitable) {
		if (ShareUtils.isAppInstalled(ShareUtils.PKG_TWITTER, mBaseActivity)) {
			ShareUtils.shareLinkUsingIntent(invitable.getTwitter_text(), mBaseActivity,
					ShareUtils.PKG_TWITTER);
		} else {
			sendEmail(invitable.getEmail(), invitable.getMail_subject(),
					invitable.getMail_text());
		}

	}

	private void sendEmail(String email, String mail_subject, String mail_text) {
		Uri uri = Uri.parse("mailto:info@yourcompany.com");
		Intent myActivity2 = new Intent(Intent.ACTION_SENDTO, uri);
		myActivity2.putExtra(Intent.EXTRA_SUBJECT,
				"Customer comments/questions").putExtra(Intent.EXTRA_TEXT,
				mail_text);
		mBaseActivity.startActivity(myActivity2);
	}

	static class UserViewHolder {
		ImageView picImageView;
		ImageView ivFollow;
		TextView nameTextView, titleTextView;
		View actionView, nameContainerView;
	}

	static class InvitableUserViewHolder extends UserViewHolder {
		TextView inviteMsgTextView, inviteMotivationTextView,
				inviteCountTextView;
		SeekBar inviteCountProgressBar;
		LinearLayout inviteCountLayout;
	}

	private String getHtmlMotivationString(InvitableUser user) {
		int count = user.getMax_invite_count() - user.getCur_invite_count();
		StringBuilder builder = new StringBuilder("Make your invite count!<br>");
		if (count > 0) {
			builder.append(count).append(" more invites bring <b>");
		} else {
			builder.append(user.getCur_invite_count()).append(" brought <b>");
		}
		return builder.append(user.getDisplayName())
				.append("</b> on frankly.me").toString();
	}
}
