package me.frankly.adapter;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.newmodel.postrequest.UserIdOnlyRequest;
import me.frankly.model.response.CategoryCeleb;
import me.frankly.util.JsonUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.HorizontalListView;
import me.frankly.view.activity.BaseActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class CategoryCelebsAdapterNew extends BaseAdapter implements
		OnClickListener {
	public static final int MODE_ASK = 1;
	public static final int MODE_FOLLOW = 2;
	private BaseActivity mBaseActivity;
	private ArrayList<CategoryCeleb> categoryCelebs;
	private int mMode;
	private OnFollowedListener onFollowedListener;

	public CategoryCelebsAdapterNew(BaseActivity mBaseActivity,
			ArrayList<CategoryCeleb> categoryCelebs, int mode) {
		this.mBaseActivity = mBaseActivity;
		this.categoryCelebs = categoryCelebs;
		this.mMode = mode;

	}

	@Override
	public int getCount() {
		return categoryCelebs.size();
	}

	@Override
	public Object getItem(int position) {
		return categoryCelebs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CategoryCeleb thisCategory = categoryCelebs.get(position);
		CategoryViewHolder h;
		if (convertView == null) {
			convertView = LayoutInflater.from(mBaseActivity).inflate(
					R.layout.list_item_cat_celeb, null);
			h = new CategoryViewHolder();
			h.catListView = (HorizontalListView) convertView
					.findViewById(R.id.hlv_cat_celeb);
			h.catNameTextView = (TextView) convertView
					.findViewById(R.id.tv_cat_name);
			h.celebsAdapter = new CelebsAdapter(mBaseActivity, thisCategory.getUsers(),
					mMode);
			h.catListView.setAdapter(h.celebsAdapter);
			convertView.setTag(h);
			convertView.setOnClickListener(this);
		} else {
			h = (CategoryViewHolder) convertView.getTag();
			h.celebsAdapter.setData(thisCategory.getUsers());
			h.celebsAdapter.notifyDataSetChanged();
		}

		RelativeLayout.LayoutParams p = (LayoutParams) h.catListView
				.getLayoutParams();
		p.leftMargin = thisCategory.getUsers().size() < 4 ? 90 : 0;
		h.catListView.setLayoutParams(p);

		h.catNameTextView.setText(thisCategory.getCategory_name());
		return convertView;
	}

	public void setOnFollowListener(OnFollowedListener listener) {
		this.onFollowedListener = listener;
	}

	public static interface OnFollowedListener {
		public void onFollowed(String userId);

		public void onUnfollowed(String userId);
	}

	static class CategoryViewHolder {
		HorizontalListView catListView;
		TextView catNameTextView;
		CelebsAdapter celebsAdapter;

	}

	class CelebsAdapter extends BaseAdapter implements OnClickListener,
			RequestListener {
		private BaseActivity mBaseActivity;
		private ArrayList<UniversalUser> celebs;
		private DisplayImageOptions picDisplayOptions;
		private int mMode;

		public CelebsAdapter(BaseActivity mBaseActivity, ArrayList<UniversalUser> celebs,
				int mode) {
			this.mBaseActivity = mBaseActivity;
			this.celebs = celebs;
			this.mMode = mode;
			picDisplayOptions = CircularImageView.getCustomDisplayOptions();
		}

		public void setData(ArrayList<UniversalUser> celebs) {
			this.celebs = celebs;
		}

		@Override
		public int getCount() {
			return celebs.size();
		}

		@Override
		public Object getItem(int position) {
			return celebs.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			UniversalUser thisUser = celebs.get(position);
			CelebViewHolder h;
			if (convertView == null) {
				convertView = LayoutInflater.from(mBaseActivity).inflate(
						R.layout.list_item_single_celeb, null);
				h = new CelebViewHolder();
				h.actionTextView = (TextView) convertView
						.findViewById(R.id.tv_ask);
				h.nameTextView = (TextView) convertView
						.findViewById(R.id.tv_name);
				h.profileImageView = (ImageView) convertView
						.findViewById(R.id.iv_profile_pic);
				h.titleTextView = (TextView) convertView
						.findViewById(R.id.tv_title);
				h.actionTextView.setOnClickListener(this);
				h.profileImageView.setOnClickListener(this);
				convertView.setTag(h);
			} else
				h = (CelebViewHolder) convertView.getTag();
			h.nameTextView.setText(thisUser.getFull_name());
			h.titleTextView.setText(thisUser.getUser_title());
			ImageLoader.getInstance().displayImage(
					thisUser.getProfile_picture(), h.profileImageView,
					picDisplayOptions);
			h.actionTextView.setText(mMode == MODE_ASK ? "Ask" : thisUser
					.is_following() ? Html.fromHtml("&#10004;") : "Follow");

			h.actionTextView.setTag(thisUser.getId());
			h.profileImageView.setTag(thisUser.getId());
			return convertView;
		}

		@Override
		public void onClick(View v) {
			String userId;
			switch (v.getId()) {
			case R.id.tv_ask:

				if (v.getTag() != null) {
					userId = (String) v.getTag();
					if (mMode == MODE_ASK)
						mBaseActivity.askActivity( userId);
					else {
						Log.d("check", "setting text following");
						((TextView) v).setText(Html.fromHtml("&#10004;"));
						Controller.requestfollow(mBaseActivity, userId, this);
						if (onFollowedListener != null)
							onFollowedListener.onFollowed(userId);

					}
				}
				break;
			case R.id.iv_profile_pic:

				if (v.getTag() != null) {
					userId = (String) v.getTag();
					mBaseActivity.profileActivity(userId, null);
				}
				break;
			}

		}

		@Override
		public void onRequestStarted() {

		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			UserIdOnlyRequest response = (UserIdOnlyRequest) JsonUtils
					.objectify((String) responseObject, UserIdOnlyRequest.class);
			for (UniversalUser temp : celebs)
				if (temp.getId().equals(response.getUserId()))
					temp.setIs_following(true);

		}

		@Override
		public void onRequestError(int errorCode, String message) {

		}
	}

	@Override
	public void onClick(View v) {

	}

	static class CelebViewHolder {
		TextView nameTextView, titleTextView;
		TextView actionTextView;
		ImageView profileImageView;
	}
}
