package me.frankly.adapter;

import java.util.ArrayList;

import me.frankly.SDKPlayer.IPlayer.IPlayerStateChangedListener;
import me.frankly.config.Constant;
import me.frankly.model.response.DiscoverableObjectsContainer;
import me.frankly.util.JsonUtils;
import me.frankly.util.PrefUtils;
import me.frankly.view.fragment.AtomicPostFragment;
import me.frankly.view.fragment.CelebQuestionsFragment;
import me.frankly.view.fragment.DefaultLoadingFragment;
import me.frankly.view.fragment.EditProfileFragment;
import me.frankly.view.fragment.FeedProfileFragment;
import me.frankly.view.fragment.PendingAnswerFragment;
import me.frankly.view.fragment.ProfileImageFragment;
import me.frankly.view.fragment.ProfileVideoFragment;
import me.frankly.view.fragment.SDKVideoFragment;
import me.frankly.view.fragment.UploadVideoFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.util.SparseArray;

/**
 * A {@link FragmentPagerAdapter} that creates {@link Fragment} for different
 * types of {@link DiscoverableObjectsContainer}
 * 
 * @author abhishek_inoxapps
 * 
 */
public class FeedsPagerAdapter extends FragmentStatePagerAdapter {
	ArrayList<DiscoverableObjectsContainer> mPostObjects;
	private boolean isFirstTime = false;
	private boolean isMoreDataAvailable = true, isEmpty = true;
	public boolean showEmptyScreen = false;
	private String mParentScreen;
	FragmentManager mFragmentManager;
	private IPlayerStateChangedListener playerStateChangedListener;
	private String mUserProfileId = null;
	public int position;
	private SparseArray<Fragment> curDisplayedFragments = new SparseArray<Fragment>(
			3);
	public boolean isVideoUpdated = false;
	private boolean isMyProfile = false;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#skm FPA:";

	public FeedsPagerAdapter(FragmentManager fm,
			ArrayList<DiscoverableObjectsContainer> postObjects, String screen) {
		super(fm);
		fileDebugLog(FILE_DEBUG_TAG + "FeedsPagerAdapter", "constructor Called");
		this.mFragmentManager = fm;
		this.mPostObjects = postObjects;
		this.mParentScreen = screen;
	}

	public FeedsPagerAdapter(FragmentManager fm,
			ArrayList<DiscoverableObjectsContainer> postObjects, String screen,
			String profileId) {
		this(fm, postObjects, screen);
		this.mUserProfileId = profileId;
		isMyProfile = mUserProfileId.equals(PrefUtils.getCurrentUserAsObject()
				.getId());
	}

	@Override
	public Fragment getItem(int arg0) {

		fileDebugLog(FILE_DEBUG_TAG + "getItem", "Called with value:" + arg0);

		Log.d("check", "get item: " + arg0);
		position = arg0;
		Fragment f = null;
		Bundle args = new Bundle();
		Log.d("PIF", "inside getItem");
		// Condition for eof
		if (arg0 == mPostObjects.size()) {
			Log.d("DLF","empty screenadded");
			f = new DefaultLoadingFragment();
			args.putBoolean("isMoreDataAvailable", isMoreDataAvailable);
			args.putBoolean("isEmpty", isEmpty);
			args.putString("mParentScreen", mParentScreen);
			args.putString("profileId", mUserProfileId);

		}// Condition for non-empty list of objects
		else if (!mPostObjects.isEmpty()) {

			String objectType = mPostObjects.get(arg0).getType();
			if (objectType.equals(DiscoverableObjectsContainer.TYPE_POST)) {
				f = new AtomicPostFragment();
				((SDKVideoFragment) f)
						.setParentPlayerListener(playerStateChangedListener);
				((SDKVideoFragment) f).setAdapterAndPos(this, arg0);
				args.putString(Constant.Key.KEY_POST_OBJECT,
						JsonUtils.jsonify(mPostObjects.get(arg0).getPost()));
				if (mUserProfileId != null)
					args.putString(Constant.Key.KEY_USER_ID, mUserProfileId);
				if (isFirstTime && arg0 == 0) {
					isFirstTime = false;
					args.putBoolean(Constant.Key.KEY_IS_AUTOPLAY, true);
				}
			} else if (objectType
					.equals(DiscoverableObjectsContainer.TYPE_QUESTION)) {
				f = new CelebQuestionsFragment();
				args.putString(Constant.Key.KEY_CELEB_QUES_JSON, JsonUtils
						.jsonify(mPostObjects.get(arg0).getQuestions()));

			} else if (objectType
					.equals(DiscoverableObjectsContainer.TYPE_USER)) {
				Log.d("PIF", "user");
				if (mPostObjects.get(arg0) == null
						|| mPostObjects.get(arg0).getUser() == null) {

					return new Fragment();
				}
				if (mParentScreen.equals(Constant.ScreenSpecs.SCREEN_FEEDS)
						|| mParentScreen
								.equals(Constant.ScreenSpecs.SCREEN_DISCOVER)
						|| mParentScreen
								.equals(Constant.ScreenSpecs.SCREEN_CHANNEL)) {
					if (mPostObjects.get(arg0).getUser().getProfile_video() != null) {
						if (playerStateChangedListener != null) {
							f = new FeedProfileFragment();

							((SDKVideoFragment) f)
									.setParentPlayerListener(playerStateChangedListener);
						}
						((SDKVideoFragment) f).setAdapterAndPos(this, arg0);
					} else {
						f = new ProfileImageFragment();
					}

				} else {
					Log.d("PIF", "not screen or feed");
					if (mPostObjects.get(0).getUser().getId()
							.equals(PrefUtils.getUserID())) {
						Log.d("PIF", "calling EditProfileFragment");
						f = new EditProfileFragment();
						if (playerStateChangedListener != null) {

							((SDKVideoFragment) f)
									.setParentPlayerListener(playerStateChangedListener);
						}
					} else if (mPostObjects.get(arg0).getUser()
							.getProfile_video() != null) {
						f = new ProfileVideoFragment();
						if (playerStateChangedListener != null) {

							((SDKVideoFragment) f)
									.setParentPlayerListener(playerStateChangedListener);
						}
						((SDKVideoFragment) f).setAdapterAndPos(this, arg0);
					} else {
						Log.d("PIF", "going to call ProfileImageFragment");
						f = new ProfileImageFragment();
					}
				}
				args.putString(Constant.Key.KEY_USER_OBJECT,
						JsonUtils.jsonify(mPostObjects.get(arg0).getUser()));
			} else if (objectType
					.equals(DiscoverableObjectsContainer.TYPE_UPLOAD)) {

				f = new UploadVideoFragment();

			} else if (objectType
					.equals(DiscoverableObjectsContainer.TYPE_PENDINGANSWERUPLOAD)) {
				f = new PendingAnswerFragment();
				args.putString(Constant.Key.KEY_PENDING_ANSWER, JsonUtils
						.jsonify(mPostObjects.get(arg0).getPendingAnswer()));
			}
			args.putInt("pos", arg0);
			args.putString(Constant.Key.KEY_PARENT_SCREEN_NAME, mParentScreen);
		}

		f.setArguments(args);
		addFragToList(f, arg0);
		return f;
	}

	@Override
	public int getItemPosition(Object object) {
		fileDebugLog(FILE_DEBUG_TAG + "getItemPosition", "Called");
		if (mParentScreen.equals(Constant.ScreenSpecs.SCREEN_PROFILE)
				&& isMyProfile) {
			if (object instanceof EditProfileFragment)
				return POSITION_UNCHANGED;
			else
				return POSITION_NONE;

		} else {
			if (object instanceof DefaultLoadingFragment
					|| object instanceof UploadVideoFragment || isVideoUpdated) {
				Log.d("LastCard", "Spending time here");
				return POSITION_NONE;

			} else
				return POSITION_UNCHANGED;
		}
	}

	@Override
	public int getCount() {
		// EOF or loader to be shown for - first time then when list is not
		// empty
		// Removing this condition will lead to a lag in removing the empty
		// screen
		// when the user tries to reload the screen in case of an API error or
		// internet
		// Connectivity issues
		// if (this.isFirstTime || !this.isEmpty)
		return showEmptyScreen ? mPostObjects.size()
				: (mPostObjects.size() + 1);
		// else
		// return (mPostObjects.size());

	}

	public void setDataBooleans(boolean isMoreDataAvailable, boolean isEmpty) {
		fileDebugLog(FILE_DEBUG_TAG + "setDataBooleans", "Called");
		Log.d("EOF", "Setting bools: isMore:" + isMoreDataAvailable
				+ " isEmpty: " + isEmpty);
		this.isMoreDataAvailable = isMoreDataAvailable;
		this.isEmpty = isEmpty;
	}

	public void setMoreDataAvailable(boolean isAvailable) {
		isMoreDataAvailable = isAvailable;
	}

	/**
	 * Use this method to play video of any of the currently displaying fragment
	 * 
	 * @param position
	 * @return {@link True} if the position is right and successfully started
	 *         playing. Or else, {@link False}
	 */
	public boolean playVideoAtPosition(int position) {
		fileDebugLog(FILE_DEBUG_TAG + "playVideoAtPosition", "position : "
				+ position);
		if (curDisplayedFragments.get((position - 1) % 3) != null
				&& curDisplayedFragments.get((position - 1) % 3) instanceof SDKVideoFragment)
			((SDKVideoFragment) curDisplayedFragments.get((position - 1) % 3))
					.pauseVideo();
		if (curDisplayedFragments.get((position + 1) % 3) != null
				&& curDisplayedFragments.get((position + 1) % 3) instanceof SDKVideoFragment)
			((SDKVideoFragment) curDisplayedFragments.get((position + 1) % 3))
					.pauseVideo();
		return true;
	}

	/**
	 * Use this method to stop playing all videos
	 */
	public void stopIfAny() {
		fileDebugLog(FILE_DEBUG_TAG + "stopIfAny", "Called");
		if (curDisplayedFragments.get((0)) != null
				&& curDisplayedFragments.get(0) instanceof SDKVideoFragment
				&& ((SDKVideoFragment) curDisplayedFragments.get(0))
						.isPlayActive())
			((SDKVideoFragment) curDisplayedFragments.get(0)).stopVideo();
		if (curDisplayedFragments.get(1) != null
				&& curDisplayedFragments.get(1) instanceof SDKVideoFragment
				&& ((SDKVideoFragment) curDisplayedFragments.get(1))
						.isPlayActive())
			((SDKVideoFragment) curDisplayedFragments.get(1)).stopVideo();
		if (curDisplayedFragments.get(2) != null
				&& curDisplayedFragments.get(2) instanceof SDKVideoFragment
				&& ((SDKVideoFragment) curDisplayedFragments.get(2))
						.isPlayActive())
			((SDKVideoFragment) curDisplayedFragments.get(2)).stopVideo();

	}

	/**
	 * Use this method to pause playing all videos
	 */
	public void pauseIfAny() {
		fileDebugLog(FILE_DEBUG_TAG + "pauseIfAny", "Called");
		if (curDisplayedFragments.get((0)) != null
				&& curDisplayedFragments.get(0) instanceof SDKVideoFragment)
			((SDKVideoFragment) curDisplayedFragments.get(0)).pauseVideo();
		if (curDisplayedFragments.get(1) != null
				&& curDisplayedFragments.get(1) instanceof SDKVideoFragment)
			((SDKVideoFragment) curDisplayedFragments.get(1)).pauseVideo();
		if (curDisplayedFragments.get(2) != null
				&& curDisplayedFragments.get(2) instanceof SDKVideoFragment)
			((SDKVideoFragment) curDisplayedFragments.get(2)).pauseVideo();

	}

	public void addFragToList(Fragment frag, int pos) {
		fileDebugLog(FILE_DEBUG_TAG + "addFragToList", "Called");
		curDisplayedFragments.append(pos % 3, frag);

	}

	/**
	 * Instead of {@link #FeedsPagerAdapter.notifyDataSetChanged()}, use this
	 * method when adding the first page of items to the list so that auto-play
	 * can be handled. When adding subsequent pages, use
	 * {@link #notifyDataSetChanged()}
	 */
	public void notifyFirstDataChanged() {
		fileDebugLog(FILE_DEBUG_TAG + "notifyFirstDataChanged", "Called");
		isFirstTime = true;
		Log.d("Kitty", "Value of show empty screen for " + mParentScreen
				+ " is: " + showEmptyScreen);
		boolean notifyChange = false;
		for (int i = 0; i < curDisplayedFragments.size(); i++)
			if (curDisplayedFragments.get(i) instanceof DefaultLoadingFragment)
				notifyChange = true;
		if (notifyChange)
			notifyDataSetChanged();
	}

	public void setPlayerStateChangeListner(IPlayerStateChangedListener listner) {
		fileDebugLog(FILE_DEBUG_TAG + "setPlayerStateChangeListner", "Called");
		playerStateChangedListener = listner;
	}

	public void setData(ArrayList<DiscoverableObjectsContainer> objects) {
		fileDebugLog(FILE_DEBUG_TAG + "setData", "Called");
		mPostObjects = objects;
	}

	@Override
	public void notifyDataSetChanged() {
		fileDebugLog(FILE_DEBUG_TAG + "notifyDataSetChanged", "Called");
		Log.e("check", "data set changed of " + mParentScreen);
		super.notifyDataSetChanged();
	}

	public ArrayList<DiscoverableObjectsContainer> getArrayList() {
		fileDebugLog(FILE_DEBUG_TAG + "getArrayList", "Called");
		return mPostObjects;
	}

	private static void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}
}