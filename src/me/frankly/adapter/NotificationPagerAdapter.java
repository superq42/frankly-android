package me.frankly.adapter;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.view.fragment.NotificationFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viewpagerindicator.IconPagerAdapter;

public class NotificationPagerAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
	private static final String[] CONTENT = new String[] {
		"Recent",
		"Artists", };

	public static final int PAGE_COUNT = 2;

	public NotificationPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment = new NotificationFragment();
		Bundle bundle = new Bundle();
		if (position == 0) {
			bundle.putString(Constant.ScreenSpecs.SCREEN_TYPE, ScreenSpecs.NOTIFICATION_ME);
		} else {
			bundle.putString(Constant.ScreenSpecs.SCREEN_TYPE, ScreenSpecs.NOTIFICATION_NEWS);
		}
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return CONTENT[position];
	}

	@Override
	public int getCount() {
		return PAGE_COUNT;
	}

	@Override
	public int getIconResId(int index) {
		// TODO Auto-generated method stub
		return R.drawable.ic_ask;
	}

}
