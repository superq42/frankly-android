package me.frankly.adapter;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.SDKPlayer.IPlayer;
import me.frankly.SDKPlayer.IPlayerFactory;
import me.frankly.model.newmodel.PostObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.newmodel.UserQuestionsContainer;
import me.frankly.model.response.DiscoverableObjectsContainer;
import me.frankly.util.ListPlayerManager;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class FeedsListAdapter extends BaseAdapter {

	private static final int VIEW_TYPE_POST = 0;
	private static final int VIEW_TYPE_CELEB_QUESTION = 1;
	private static final int VIEW_TYPE_VIDEO_PROFILE = 2;
	private static final int VIEW_TYPE_IMAGE_PROFILE = 3;

	private Context mCtx;
	private ArrayList<DiscoverableObjectsContainer> objects;
	private String mScreen;
	private IPlayer mPlayer;
	private ListPlayerManager mPlayerManager;
	private Handler mHandler;

	public FeedsListAdapter(Context ctx,
			ArrayList<DiscoverableObjectsContainer> objects, String mScreen) {
		this.mCtx = ctx;
		this.objects = objects;
		this.mScreen = mScreen;
		this.mPlayer = IPlayerFactory.getInstance().newPlayer();
		mHandler = new Handler();
		mPlayerManager = new ListPlayerManager(mCtx, mPlayer, mScreen, mHandler);
	}

	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public int getViewTypeCount() {
		return 4;
	}

	@Override
	public int getItemViewType(int position) {
		DiscoverableObjectsContainer mObject = objects.get(position);
		String objType = mObject.getType();
		if (objType.equals(DiscoverableObjectsContainer.TYPE_POST))
			return VIEW_TYPE_POST;
		else if (objType.equals(DiscoverableObjectsContainer.TYPE_QUESTION))
			return VIEW_TYPE_CELEB_QUESTION;
		else {
			if (mObject.getUser().getProfile_video() != null)
				return VIEW_TYPE_VIDEO_PROFILE;
			else
				return VIEW_TYPE_IMAGE_PROFILE;
		}
	}

	@Override
	public Object getItem(int position) {
		return objects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DiscoverableObjectsContainer mObj = objects.get(position);
		String objType = mObj.getType();
		if (objType.equals(DiscoverableObjectsContainer.TYPE_POST))
			return getPostView(mObj.getPost(), position, convertView, parent);
		else if (objType.equals(DiscoverableObjectsContainer.TYPE_QUESTION))
			return getCelebQuestionsView(mObj.getQuestions(), position,
					convertView, parent);
		else {
			if (mObj.getUser().getProfile_video() != null)
				return getVideoProfileView(mObj.getUser(), position,
						convertView, parent);
			else
				return getVideoProfileView(mObj.getUser(), position,
						convertView, parent);
		}
	}

	private View getVideoProfileView(UniversalUser user, int position,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return null;
	}

	private View getCelebQuestionsView(UserQuestionsContainer questions,
			int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null)
			convertView = LayoutInflater.from(mCtx).inflate(
					R.layout.layout_discover_celeb, null);
		return convertView;
	}

	private View getPostView(PostObject post, int position, View convertView,
			ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null)
			convertView = LayoutInflater.from(mCtx).inflate(
					R.layout.fragment_atomic_post, null);
		return convertView;
	}
	
	
	public static class VideoViewHolder{
		public int position;
		public TextureView videoTextureView;
		public View loadingView, initView, pauseView, postCompletionView;
		
	}

}
