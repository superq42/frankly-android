package me.frankly.adapter;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.config.AppConfig;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.newmodel.postrequest.UserIdOnlyRequest;
import me.frankly.model.response.CategoryCeleb;
import me.frankly.util.JsonUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.BaseActivity;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class CategoryCelebsAdapter extends BaseAdapter implements
		OnClickListener, RequestListener {
	public static final int MODE_ASK = 1;
	public static final int MODE_FOLLOW = 2;
	private BaseActivity mBaseActivity;
	private ArrayList<CategoryCeleb> categoryCelebs;
	private DisplayImageOptions picDisplayOptions;
	private OnFollowedListener onFollowedListener;
	private final int mMode;
	private OnMoreClickedListener onMoreClickedListener;
	private Handler handler;

	public CategoryCelebsAdapter(BaseActivity mBaseActivity,
			ArrayList<CategoryCeleb> categoryCelebs, int mode) {
		this.mBaseActivity = mBaseActivity;
		this.categoryCelebs = categoryCelebs;
		picDisplayOptions = CircularImageView.getCustomDisplayOptions();
		mMode = mode;
		handler = new Handler();
	}

	@Override
	public int getCount() {
		return categoryCelebs.size();
	}

	@Override
	public Object getItem(int arg0) {
		return categoryCelebs.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder h;
		CategoryCeleb thisCategoryCeleb = categoryCelebs.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mBaseActivity).inflate(
					R.layout.list_item_category_celebs_def, null);
			h = new ViewHolder();
			h.seeMoreView = convertView.findViewById(R.id.tv_see_more);
			h.categoryNameTextView = (TextView) convertView
					.findViewById(R.id.tv_category_name);
			h.followOneView = (TextView) convertView
					.findViewById(R.id.tv_ask_one);
			h.followTwoView = (TextView) convertView
					.findViewById(R.id.tv_ask_two);
			h.followThreeView = (TextView) convertView
					.findViewById(R.id.tv_ask_three);
			h.nameOneTextView = (TextView) convertView
					.findViewById(R.id.tv_name_one);
			h.nameTwoTextView = (TextView) convertView
					.findViewById(R.id.tv_name_two);
			h.nameThreeTextView = (TextView) convertView
					.findViewById(R.id.tv_name_three);
			h.profileOneImageView = (ImageView) convertView
					.findViewById(R.id.iv_profile_pic_one);
			h.profileTwoImageView = (ImageView) convertView
					.findViewById(R.id.iv_profile_pic_two);
			h.profileThreeImageView = (ImageView) convertView
					.findViewById(R.id.iv_profile_pic_three);
			h.ivFollowOne = (ImageView) convertView
					.findViewById(R.id.iv_follow_one);
			h.ivFollowTwo = (ImageView) convertView
					.findViewById(R.id.iv_follow_two);
			h.ivFollowThree = (ImageView) convertView
					.findViewById(R.id.iv_follow_three);
			h.titleOneTextView = (TextView) convertView
					.findViewById(R.id.tv_title_one);
			h.titleTwoTextView = (TextView) convertView
					.findViewById(R.id.tv_title_two);
			h.titleThreeTextView = (TextView) convertView
					.findViewById(R.id.tv_title_three);
			h.profileOneImageView.setOnClickListener(this);
			h.profileTwoImageView.setOnClickListener(this);
			h.profileThreeImageView.setOnClickListener(this);
			h.ivFollowOne.setOnClickListener(this);
			h.ivFollowTwo.setOnClickListener(this);
			h.ivFollowThree.setOnClickListener(this);
			h.followOneView.setOnClickListener(this);
			h.followTwoView.setOnClickListener(this);
			h.followThreeView.setOnClickListener(this);
			if (mMode == MODE_FOLLOW) {
				h.followOneView.setEms(6);
				h.followTwoView.setEms(6);
				h.followThreeView.setEms(6);
			}
			if (mMode == MODE_ASK)
				h.seeMoreView.setOnClickListener(this);
			else
				h.seeMoreView.setVisibility(View.GONE);
			convertView.setOnClickListener(this); // Do nothing -- fix to solved
													// false click on search
													// results
			convertView.setTag(h);
		} else
			h = (ViewHolder) convertView.getTag();
		h.categoryNameTextView.setText(thisCategoryCeleb.getCategory_name());

		if (!thisCategoryCeleb.getUsers().isEmpty()) {
			h.nameOneTextView.setVisibility(View.VISIBLE);
			h.profileOneImageView.setVisibility(View.VISIBLE);
			h.titleOneTextView.setVisibility(View.VISIBLE);
			h.profileOneImageView.setVisibility(View.VISIBLE);
			h.followOneView.setVisibility(View.VISIBLE);
			h.nameOneTextView.setText(thisCategoryCeleb.getUsers().get(0)
					.getFull_name());
			ImageLoader.getInstance().displayImage(
					thisCategoryCeleb.getUsers().get(0).getProfile_picture(),
					h.profileOneImageView, picDisplayOptions);
			h.titleOneTextView.setText(thisCategoryCeleb.getUsers().get(0)
					.getUser_title());
			h.profileOneImageView.setTag(thisCategoryCeleb.getUsers().get(0));
			h.followOneView.setTag(thisCategoryCeleb.getUsers().get(0).getId());
			h.nameOneTextView.setText(thisCategoryCeleb.getUsers().get(0)
					.getFull_name());
			ImageLoader.getInstance().displayImage(
					thisCategoryCeleb.getUsers().get(0).getProfile_picture(),
					h.profileOneImageView, picDisplayOptions);
			h.titleOneTextView.setText(thisCategoryCeleb.getUsers().get(0)
					.getUser_title());
			h.profileOneImageView.setTag(thisCategoryCeleb.getUsers().get(0));
			h.followOneView
					.setText(mMode == MODE_ASK ? "Ask" : thisCategoryCeleb
							.getUsers().get(0).is_following() ? "Unfollow"
							: " Follow ");
			if (thisCategoryCeleb.getUsers().get(0).is_following())
				h.ivFollowOne.setImageResource(R.drawable.ic_followed);
			else
				h.ivFollowOne.setImageResource(R.drawable.ic_add_profilepic);
			h.followOneView.setTag(thisCategoryCeleb.getUsers().get(0));
			h.ivFollowOne.setTag(thisCategoryCeleb.getUsers().get(0));
			h.ivFollowOne.setTag(R.id.tv_ask_one, h.followOneView);
			h.ivFollowOne.setTag(R.id.iv_follow_one, h.ivFollowOne);
			h.followOneView.setTag(R.id.iv_follow_one, h.ivFollowOne);
			h.followOneView.setTag(R.id.tv_ask_one, h.followOneView);
			if (mMode == MODE_FOLLOW) {
				h.profileOneImageView.setTag(R.id.iv_follow_one, h.ivFollowOne);
				h.profileOneImageView.setTag(R.id.tv_ask_one, h.followOneView);
			}

		} else {
			h.nameOneTextView.setVisibility(View.INVISIBLE);
			h.profileOneImageView.setVisibility(View.INVISIBLE);
			h.titleOneTextView.setVisibility(View.INVISIBLE);
			h.profileOneImageView.setVisibility(View.INVISIBLE);
			h.followOneView.setVisibility(View.INVISIBLE);
			h.ivFollowOne.setVisibility(View.INVISIBLE);
			h.nameOneTextView.setText("");
			h.profileOneImageView.setImageDrawable(null);
			h.titleOneTextView.setText("");
			h.profileOneImageView.setTag(null);
			h.followOneView.setTag(null);
			h.ivFollowOne.setTag(null);
		}
		if (thisCategoryCeleb.getUsers().size() >= 2) {
			h.nameTwoTextView.setVisibility(View.VISIBLE);
			h.profileTwoImageView.setVisibility(View.VISIBLE);
			h.titleTwoTextView.setVisibility(View.VISIBLE);
			h.profileTwoImageView.setVisibility(View.VISIBLE);
			h.followTwoView.setVisibility(View.VISIBLE);
			h.nameTwoTextView.setText(thisCategoryCeleb.getUsers().get(1)
					.getFull_name());
			ImageLoader.getInstance().displayImage(
					thisCategoryCeleb.getUsers().get(1).getProfile_picture(),
					h.profileTwoImageView, picDisplayOptions);
			h.titleTwoTextView.setText(thisCategoryCeleb.getUsers().get(1)
					.getUser_title());
			h.profileTwoImageView.setTag(thisCategoryCeleb.getUsers().get(1));
			h.followTwoView.setText(mMode == MODE_ASK ? "Ask"
					: thisCategoryCeleb.getUsers().get(1)

					.is_following() ? "Unfollow" : " Follow ");
			if (thisCategoryCeleb.getUsers().get(1).is_following())
				h.ivFollowTwo.setImageResource(R.drawable.ic_followed);
			else
				h.ivFollowTwo.setImageResource(R.drawable.ic_add_profilepic);
			h.followTwoView.setTag(thisCategoryCeleb.getUsers().get(1));
			h.ivFollowTwo.setTag(thisCategoryCeleb.getUsers().get(1));
			h.ivFollowTwo.setTag(R.id.tv_ask_two, h.followTwoView);
			h.ivFollowTwo.setTag(R.id.iv_follow_two, h.ivFollowTwo);
			h.followTwoView.setTag(R.id.iv_follow_two, h.ivFollowTwo);
			h.followTwoView.setTag(R.id.tv_ask_two, h.followTwoView);
			if (mMode == MODE_FOLLOW) {
				h.profileTwoImageView.setTag(R.id.iv_follow_two, h.ivFollowTwo);
				h.profileTwoImageView.setTag(R.id.tv_ask_two, h.followTwoView);
			}
		} else {
			h.nameTwoTextView.setVisibility(View.INVISIBLE);
			h.profileTwoImageView.setVisibility(View.INVISIBLE);
			h.titleTwoTextView.setVisibility(View.INVISIBLE);
			h.profileTwoImageView.setVisibility(View.INVISIBLE);
			h.followTwoView.setVisibility(View.INVISIBLE);
			h.ivFollowTwo.setVisibility(View.INVISIBLE);
			h.nameTwoTextView.setText("");
			h.profileTwoImageView.setImageDrawable(null);
			h.titleTwoTextView.setText("");
			h.profileTwoImageView.setTag(null);
			h.followTwoView.setTag(null);
			h.ivFollowTwo.setTag(null);
		}
		if (thisCategoryCeleb.getUsers().size() >= 3) {
			h.nameThreeTextView.setVisibility(View.VISIBLE);
			h.profileThreeImageView.setVisibility(View.VISIBLE);
			h.titleThreeTextView.setVisibility(View.VISIBLE);
			h.profileThreeImageView.setVisibility(View.VISIBLE);
			h.followThreeView.setVisibility(View.VISIBLE);
			h.nameThreeTextView.setText(thisCategoryCeleb.getUsers().get(2)
					.getFull_name());
			ImageLoader.getInstance().displayImage(
					thisCategoryCeleb.getUsers().get(2).getProfile_picture(),
					h.profileThreeImageView, picDisplayOptions);
			h.titleThreeTextView.setText(thisCategoryCeleb.getUsers().get(2)
					.getUser_title());
			h.profileThreeImageView.setTag(thisCategoryCeleb.getUsers().get(2));

			h.followThreeView.setTag(thisCategoryCeleb.getUsers().get(2));

			h.followThreeView
					.setText(mMode == MODE_ASK ? "Ask" : thisCategoryCeleb
							.getUsers().get(2).is_following() ? "Unfollow"
							: " Follow ");
			if (thisCategoryCeleb.getUsers().get(2).is_following())
				h.ivFollowThree.setImageResource(R.drawable.ic_followed);
			else
				h.ivFollowThree.setImageResource(R.drawable.ic_add_profilepic);
			h.ivFollowThree.setTag(thisCategoryCeleb.getUsers().get(2));
			h.ivFollowThree.setTag(R.id.tv_ask_three, h.followThreeView);
			h.ivFollowThree.setTag(R.id.iv_follow_three, h.ivFollowThree);
			h.followThreeView.setTag(R.id.iv_follow_three, h.ivFollowThree);
			h.followThreeView.setTag(R.id.tv_ask_three, h.followThreeView);
			if (mMode == MODE_FOLLOW) {
				h.profileThreeImageView.setTag(R.id.iv_follow_three,
						h.ivFollowThree);
				h.profileThreeImageView.setTag(R.id.tv_ask_three,
						h.followThreeView);
			}
		} else {
			h.nameThreeTextView.setVisibility(View.INVISIBLE);
			h.profileThreeImageView.setVisibility(View.INVISIBLE);
			h.titleThreeTextView.setVisibility(View.INVISIBLE);
			h.profileThreeImageView.setVisibility(View.INVISIBLE);
			h.followThreeView.setVisibility(View.INVISIBLE);
			h.ivFollowThree.setVisibility(View.INVISIBLE);
			h.nameThreeTextView.setText("");
			h.profileThreeImageView.setImageDrawable(null);
			h.titleThreeTextView.setText("");
			h.profileThreeImageView.setTag(null);
			h.followThreeView.setTag(null);
			h.ivFollowThree.setTag(null);
		}
		h.seeMoreView.setTag(thisCategoryCeleb.getCategory_name());

		return convertView;

	}

	@Override
	public void onClick(View v) {
		UniversalUser user;
		ImageView ivFollow;
		TextView tvFollow;
		switch (v.getId()) {
		case R.id.tv_ask_one:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (mMode == MODE_FOLLOW
						&& v.getTag(R.id.iv_follow_one) != null
						&& v.getTag(R.id.tv_ask_one) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_one);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_one);
					followToggle(user, ivFollow, tvFollow);
				} else {
					mBaseActivity.askActivity( user);
				}
			}
			break;
		case R.id.tv_ask_two:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (mMode == MODE_FOLLOW
						&& v.getTag(R.id.iv_follow_two) != null
						&& v.getTag(R.id.tv_ask_two) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_two);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_two);
					followToggle(user, ivFollow, tvFollow);
				} else {
					mBaseActivity.askActivity( user);
				}
			}
			break;
		case R.id.tv_ask_three:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (mMode == MODE_FOLLOW
						&& v.getTag(R.id.iv_follow_three) != null
						&& v.getTag(R.id.tv_ask_three) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_three);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_three);
					followToggle(user, ivFollow, tvFollow);
				} else {
					mBaseActivity.askActivity( user);
				}
			}
			break;
		case R.id.iv_follow_one:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (mMode == MODE_FOLLOW
						&& v.getTag(R.id.iv_follow_one) != null
						&& v.getTag(R.id.tv_ask_one) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_one);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_one);
					followToggle(user, ivFollow, tvFollow);
				} else if (mMode == MODE_ASK
						&& v.getTag(R.id.iv_follow_one) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_one);
					followToggle(user, ivFollow);
				}
			}
			break;
		case R.id.iv_follow_two:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (mMode == MODE_FOLLOW
						&& v.getTag(R.id.iv_follow_two) != null
						&& v.getTag(R.id.tv_ask_two) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_two);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_two);
					followToggle(user, ivFollow, tvFollow);
				} else if (mMode == MODE_ASK
						&& v.getTag(R.id.iv_follow_two) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_two);
					followToggle(user, ivFollow);
				}
			}
			break;
		case R.id.iv_follow_three:
			if (v.getTag() != null) {
				Log.d("check", "setting text following");
				user = (UniversalUser) v.getTag();
				if (mMode == MODE_FOLLOW
						&& v.getTag(R.id.iv_follow_three) != null
						&& v.getTag(R.id.tv_ask_three) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_three);
					tvFollow = (TextView) v.getTag(R.id.tv_ask_three);
					followToggle(user, ivFollow, tvFollow);
				} else if (mMode == MODE_ASK
						&& v.getTag(R.id.iv_follow_three) != null) {
					ivFollow = (ImageView) v.getTag(R.id.iv_follow_three);
					followToggle(user, ivFollow);
				}
			}
			break;
		case R.id.iv_profile_pic_one:
			if (v.getTag() != null && mMode == MODE_ASK) {
				user = (UniversalUser) v.getTag();
				String userId = user.getId();
				mBaseActivity.profileActivity( userId, null);
			} else if (mMode == MODE_FOLLOW
					&& v.getTag(R.id.iv_follow_one) != null
					&& v.getTag(R.id.tv_ask_one) != null) {
				user = (UniversalUser) v.getTag();
				ivFollow = (ImageView) v.getTag(R.id.iv_follow_one);
				tvFollow = (TextView) v.getTag(R.id.tv_ask_one);
				followToggle(user, ivFollow, tvFollow);

			}
			break;
		case R.id.iv_profile_pic_two:
			if (v.getTag() != null && mMode == MODE_ASK) {
				user = (UniversalUser) v.getTag();
				String userId = user.getId();
				mBaseActivity.profileActivity( userId, null);
			} else if (mMode == MODE_FOLLOW
					&& v.getTag(R.id.iv_follow_two) != null
					&& v.getTag(R.id.tv_ask_two) != null) {
				user = (UniversalUser) v.getTag();
				ivFollow = (ImageView) v.getTag(R.id.iv_follow_two);
				tvFollow = (TextView) v.getTag(R.id.tv_ask_two);
				followToggle(user, ivFollow, tvFollow);

			}
			break;
		case R.id.iv_profile_pic_three:
			if (v.getTag() != null && mMode == MODE_ASK) {
				user = (UniversalUser) v.getTag();
				String userId = user.getId();
				mBaseActivity.profileActivity( userId, null);
			} else if (mMode == MODE_FOLLOW
					&& v.getTag(R.id.iv_follow_three) != null
					&& v.getTag(R.id.tv_ask_three) != null) {
				user = (UniversalUser) v.getTag();
				ivFollow = (ImageView) v.getTag(R.id.iv_follow_three);
				tvFollow = (TextView) v.getTag(R.id.tv_ask_three);
				followToggle(user, ivFollow, tvFollow);
			}

			break;
		case R.id.tv_see_more:
			if(AppConfig.DEBUG) {
				Log.d("SA", "see more clicked");
			}
			String catName = (String) v.getTag();
			if (catName != null && onMoreClickedListener != null)
				onMoreClickedListener.onMoreClicked(catName);
			break;
		}

	}

	public void followToggle(UniversalUser user, ImageView ivFollow,
			TextView tvFollow) {
		if (Controller.isNetworkConnectedWithMessage(mBaseActivity)) {
			if (!user.is_following()) {
				tvFollow.setText("Unfollow");
				ivFollow.setImageResource(R.drawable.ic_followed);
				Controller.requestfollow(mBaseActivity, user.getId(), this);
				if (onFollowedListener != null)
					onFollowedListener.onFollowed(user.getId());
			} else {
				tvFollow.setText(" Follow ");
				ivFollow.setImageResource(R.drawable.ic_add_profilepic);
				Controller
						.requestUnfollow(mBaseActivity, user.getId(), unfollowListener);
				if (onFollowedListener != null)
					onFollowedListener.onUnfollowed(user.getId());
			}
		}
	}

	public void followToggle(UniversalUser user, ImageView ivFollow) {
		if (Controller.isNetworkConnectedWithMessage(mBaseActivity)) {
			if (!user.is_following()) {
				ivFollow.setImageResource(R.drawable.ic_followed);
				Controller.requestfollow(mBaseActivity, user.getId(), this);
				if (onFollowedListener != null)
					onFollowedListener.onFollowed(user.getId());
			} else {
				ivFollow.setImageResource(R.drawable.ic_add_profilepic);
				Controller
						.requestUnfollow(mBaseActivity, user.getId(), unfollowListener);
				if (onFollowedListener != null)
					onFollowedListener.onUnfollowed(user.getId());
			}
		}
	}

	public void setOnFollowedListener(OnFollowedListener listener) {
		this.onFollowedListener = listener;
	}

	public void setOnMoreClickedListener(OnMoreClickedListener listener) {
		this.onMoreClickedListener = listener;
	}

	static class ViewHolder {
		TextView categoryNameTextView, nameOneTextView, nameTwoTextView,
				nameThreeTextView, titleOneTextView, titleTwoTextView,
				titleThreeTextView;
		TextView followOneView, followTwoView, followThreeView;
		ImageView profileOneImageView, profileTwoImageView,
				profileThreeImageView, ivFollowOne, ivFollowTwo, ivFollowThree;
		View seeMoreView;
	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		final UserIdOnlyRequest response = (UserIdOnlyRequest) JsonUtils
				.objectify((String) responseObject, UserIdOnlyRequest.class);
		handler.post(new Runnable() {

			@Override
			public void run() {
				UniversalActionListenerCollection.getInstance()
						.onUserFollowChanged(response.getUserId(), true);
			}
		});
		for (CategoryCeleb celebs : categoryCelebs) {
			for (UniversalUser temp : celebs.getUsers())
				if (temp.getId().equals(response.getUserId())) {
					temp.setIs_following(true);
					return;
				}
		}

	}

	@Override
	public void onRequestError(int errorCode, String message) {

	}

	private RequestListener unfollowListener = new RequestListener() {

		@Override
		public void onRequestStarted() {

		}

		@Override
		public void onRequestError(int errorCode, String message) {

		}

		@Override
		public void onRequestCompleted(Object responseObject) {

			final UserIdOnlyRequest response = (UserIdOnlyRequest) JsonUtils
					.objectify((String) responseObject, UserIdOnlyRequest.class);
			handler.post(new Runnable() {

				@Override
				public void run() {
					UniversalActionListenerCollection.getInstance()
							.onUserFollowChanged(response.getUserId(), false);
				}
			});

			for (CategoryCeleb celebs : categoryCelebs) {
				for (UniversalUser temp : celebs.getUsers())
					if (temp.getId().equals(response.getUserId())) {
						temp.setIs_following(false);
						return;
					}
			}
		}
	};

	public static interface OnFollowedListener {
		public void onFollowed(String userId);

		public void onUnfollowed(String userId);
	}

	public static interface OnMoreClickedListener {
		public void onMoreClicked(String categoryName);
	}
}
