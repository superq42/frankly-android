package me.frankly.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import me.frankly.R;
import me.frankly.view.fragment.SlideFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {
	private int[] bg_ids = new int[] {R.drawable.frankly_log, R.drawable.img_tut_2,
			R.drawable.img_tut_4, R.drawable.img_tut_3, R.drawable.img_tut_1 };
	private String[] screenCaptions = new String[] {"",
			"Meet <b>Celebrities</b>", "Ask them <b>Anything</b>",
			"Get Replies in <b>Video Selfies</b>", "Start <b>Conversations</b>" };

	/*
	 * private String[] caption = new String[] {
	 * "Anonymously get secrets out of your /n friends and family",
	 * "Tell the world how awesome you /n truly are!",
	 * "Get liked and popular like /n a celebrity",
	 * "Meet new people. /n Start conversations.",
	 * "How about some personal chats with your favorite celebrity" };
	 */
	public ViewPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		Fragment f = null;

		f = new SlideFragment();
		Bundle args = new Bundle();
		args.putInt(SlideFragment.KEY_BACKGROUND_ID, bg_ids[arg0]);
		args.putString(SlideFragment.KEY_CAPTION, screenCaptions[arg0]);

		f.setArguments(args);

		return f;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return bg_ids.length;
	}

}
