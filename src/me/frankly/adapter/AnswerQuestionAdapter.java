package me.frankly.adapter;

import java.util.ArrayList;
import java.util.Iterator;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.config.Controller;
import me.frankly.config.UrlResolver.EndPoints;
import me.frankly.listener.RequestListener;
import me.frankly.model.AnswerQuestionDataHolder;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.view.activity.AnswerQuestionActivity;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.RecordAnswerActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;

@SuppressLint("ClickableViewAccessibility")
public class AnswerQuestionAdapter extends BaseAdapter implements
		OnClickListener, OnTouchListener {
	/*
	 * private static final int VIEW_TYPE_QUESTION = 1; private static final int
	 * VIEW_TYPE_INVITE = 2;
	 */
	private ArrayList<AnswerQuestionDataHolder> questionsDataHolders;
	private BaseActivity mBaseActivity;
	private static boolean isSessionConsumed = false;
	private ArrayList<ToolTipView> tapAnswerToolTipView = new ArrayList<ToolTipView>();
	private static final int REQUEST_ANSWER_QUESTION = 900;
	@SuppressWarnings("unused")
	private ToolTipRelativeLayout relativeLayout;
	private boolean isDeleteInProgress;

	public AnswerQuestionAdapter(ArrayList<AnswerQuestionDataHolder> qList,
			BaseActivity mBaseActivity) {
		this.questionsDataHolders = qList;
		this.mBaseActivity = mBaseActivity;

	}

	public void setListData(ArrayList<AnswerQuestionDataHolder> qList) {
		this.questionsDataHolders = qList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {

		return questionsDataHolders.size();
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {

		return 1;
	}

	@Override
	public AnswerQuestionDataHolder getItem(int position) {
		return questionsDataHolders.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Log.d("ItemToString", "Inside getView, this is my position: "
				+ position);

		final ViewHolder holder;
		QuestionObject item_object = questionsDataHolders.get(position)
				.getQuestion();

		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(mBaseActivity);
			convertView = inflater.inflate(R.layout.answer_question, null);
			holder = new ViewHolder();

			holder.authorName = (TextView) convertView
					.findViewById(R.id.question_author_name);
			holder.bodyTextView = (TextView) convertView
					.findViewById(R.id.question_text);
			holder.tvAnswer = (TextView) convertView
					.findViewById(R.id.tv_answer);
			holder.toolTipRelativeLayout = (ToolTipRelativeLayout) convertView
					.findViewById(R.id.activity_main_tooltipRelativeLayout);
			holder.deleteMenu = (ImageView) convertView
					.findViewById(R.id.delete_question_menu);
			holder.tvAnswer.setOnClickListener(this);
			holder.deleteMenu.setOnClickListener(this);
			holder.deleteMenu.setTag(position);

				holder.askCountTextView = (TextView) convertView
						.findViewById(R.id.tv_askers_count);
			
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (item_object.getAsk_count() - 1 > 0) {
			// for only one others case
			if (item_object.getAsk_count() == 2) {
				holder.askCountTextView.setText("and "
						+ (item_object.getAsk_count() - 1) + " other");
			} else {
				holder.askCountTextView.setText("and "
						+ (item_object.getAsk_count() - 1) + " others");
			}
		}
			

		/*
		 * holder.collageImageView .setBackgroundResource(R.drawable.human);
		 */

		// If the question has not been asked anonymously then set click
		// listeners
		// Set text underline in case the question is not anonymous
		// Make text white in case of anonymous user
		if (!item_object.is_anonymous()) {
			// Set tag on author name field, so that we can return the id
			// for starting the profile activity
			holder.authorName.setTag(item_object.getQuestion_author().getId());
			holder.authorName.setOnClickListener(this);
			holder.authorName.setEnabled(true);
			// Optimization, check if current color is red and if it is not
			// then set it as red
			if (holder.authorName.getCurrentTextColor() != mBaseActivity
					.getResources().getColor(
							R.color.question_author_profile_link))
				;
			holder.authorName.setTextColor(mBaseActivity.getResources().getColor(
					R.color.question_author_profile_link));

		} else {

			holder.authorName.setTextColor(Color.WHITE);

			holder.authorName.setEnabled(false);

		}

		holder.bodyTextView.setText(item_object.getBody());
		holder.authorName.setText(item_object.getQuestion_author()
				.getFull_name());
		holder.tvAnswer.setTag(position);

		// holder.toolTipRelativeLayout.setOnTouchListener(this);
		if (position == 0) {
			if (!isSessionConsumed
					&& !PrefUtils.getIsFirstAnswerQuestionAnimShown()) {
				holder.toolTipRelativeLayout.setVisibility(View.VISIBLE);
				if (holder.toolTipRelativeLayout.getChildCount() < 1) {
					/*
					 * if (tapAnswerToolTipView != null)
					 * tapAnswerToolTipView.remove();
					 */
					ToolTipView t = Controller.addToolTipView(
							holder.toolTipRelativeLayout, holder.tvAnswer,
							mBaseActivity, "Tap to Answer", false);
					tapAnswerToolTipView.add(t);
					t.setOnTouchListener(this);
					relativeLayout = holder.toolTipRelativeLayout;
				}

			}
		} else {
			holder.toolTipRelativeLayout.removeAllViews();
			// if(position != 0 )
			// tapAnswerToolTipView.remove();
		}

		/*
		 * if(position != 0 && toolTipRelativeLayout.getVisibility() ==
		 * View.VISIBLE) toolTipRelativeLayout.setVisibility(View.GONE);
		 */

		return convertView;
	}

	private static class ViewHolder {
		TextView bodyTextView, authorName, askCountTextView;
		TextView tvAnswer;
		ToolTipRelativeLayout toolTipRelativeLayout;
		ImageView deleteMenu;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		/*
		 * case R.id.leading_text_author: case R.id.tv_askers_count:
		 */
		case R.id.question_author_name:
			String user_id = (String) v.getTag();
			mBaseActivity.profileActivity( user_id, null);
			MixpanelUtils.sendProfileNavigationEvent(user_id, null,
					Constant.USER_ROLE.QUESTION_AUTHOR,
					ScreenSpecs.SCREEN_MY_QUESTIONS,
					MixpanelUtils.NAVIGATION_PROFILE, mBaseActivity);
			break;
		case R.id.tv_answer:
			recordAnswer((Integer) v.getTag());
			MixpanelUtils.sendGenericEvent(
					this.getItemByPosition((Integer) v.getTag()).getId(), null,
					null, ScreenSpecs.SCREEN_MY_QUESTIONS,
					MixpanelUtils.ANSWER_QUESTION, mBaseActivity);
			break;
		case R.id.delete_question_menu:
			if (isDeleteInProgress) {
				Controller.showToast(mBaseActivity,
						"Another request is in progress. Please wait.");
				return;
			}
			if(Controller.isNetworkConnectedWithMessage(mBaseActivity)) {
				int position = (Integer) v.getTag();
				showWarningPopup(position);
			}
			break;
		}

	}

	private void recordAnswer(int i) {
		Intent intent = new Intent(((Activity) mBaseActivity),
				RecordAnswerActivity.class);
		intent.putExtra("question_asked",
				JsonUtils.jsonify(this.getItemByPosition(i)));
		((Activity) mBaseActivity).startActivityForResult(intent,
				REQUEST_ANSWER_QUESTION);
	}

	public QuestionObject getItemByPosition(int position) {
		return questionsDataHolders.get(position).getQuestion();
	}

	public boolean removeItemById(String questionid) {
		boolean found = false;

		for (Iterator<AnswerQuestionDataHolder> iterator = questionsDataHolders
				.iterator(); iterator.hasNext();) {
			AnswerQuestionDataHolder dataHolder = (AnswerQuestionDataHolder) iterator
					.next();
			if (dataHolder.getQuestion().getId().equals(questionid)) {
				iterator.remove();
				found = true;
			}

		}
		notifyDataSetChanged();
		return found;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			hideToolTip();
			
			if (mBaseActivity instanceof AnswerQuestionActivity)
				((AnswerQuestionActivity) mBaseActivity).answerQuestionAtPos(0);
		}
		return false;
	}

	public void hideToolTip() {
		isSessionConsumed = true;
		if (tapAnswerToolTipView != null) {
			Log.d("check", "hide tooltip not null");

			for (ToolTipView toolTipView : tapAnswerToolTipView) {
				toolTipView.remove();
			}
			tapAnswerToolTipView = null;

		} else
			Log.d("check", "hide tooltip null");
		PrefUtils.setIsFirstAnswerQuestionAnimShown(true);
	}

	private void showWarningPopup(final int position) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mBaseActivity);

		alertDialogBuilder.setTitle("Delete Question");
		alertDialogBuilder.setMessage("Do you want to delete this question?");

		alertDialogBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						isDeleteInProgress = true;
						dialog.cancel();
						Controller.ignoreAnswer(mBaseActivity, questionsDataHolders
								.get(position).getQuestion(),
								new RequestListener() {

									@Override
									public void onRequestStarted() {
										Controller.showToast(mBaseActivity,
												"Sending request to delete the question...");
									}

									@Override
									public void onRequestError(int errorCode,
											final String message) {
										isDeleteInProgress = false;
										Controller.showToast(mBaseActivity,
												"Question delete failed.");
									}

									@Override
									public void onRequestCompleted(
											Object responseObject) {
										isDeleteInProgress = false;
										Controller.showToast(mBaseActivity,
												"Question deleted.");
										Controller
												.removeAllPagesInCache(EndPoints.GET_QUESTIONS);
										mBaseActivity
												.runOnUiThread(new Runnable() {
													@Override
													public void run() {
														questionsDataHolders
																.remove(position);
														notifyDataSetChanged();
													}
												});
									}
								});
					}
				});
		alertDialogBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		// show alert
		alertDialog.show();

	}

}