package me.frankly.adapter;

import java.util.List;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.QuestionObjectHolder;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.view.ShareDialog;
import me.frankly.view.activity.BaseActivity;
import android.app.Activity;
import android.graphics.Point;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AskQuestionAdapter extends BaseAdapter implements OnClickListener {

	private BaseActivity mBaseActivity;
	private List<QuestionObjectHolder> listQuestion;
	private boolean showOtherHeader = false;

	public AskQuestionAdapter(BaseActivity mBaseActivity,
			List<QuestionObjectHolder> list) {
		super();
		this.mBaseActivity = mBaseActivity;
		listQuestion = list;
		Display display = ((Activity) mBaseActivity).getWindowManager()
				.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
	}

	@Override
	public int getCount() {
		return listQuestion.size();
	}

	public void addData(List<QuestionObjectHolder> list) {
		listQuestion.addAll(listQuestion);
	}

	@Override
	public Object getItem(int position) {
		return listQuestion.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final QuestionObjectHolder questionObject = listQuestion.get(position);

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mBaseActivity).inflate(
					R.layout.list_item_celeb_question, null);
			viewHolder = new ViewHolder();
			viewHolder.tvQuestion = (TextView) convertView
					.findViewById(R.id.list_item_celb_question);
			viewHolder.tvVoteCounts = (TextView) convertView
					.findViewById(R.id.list_item_celeb_votecounts);
			viewHolder.tvUpvote = (TextView) convertView
					.findViewById(R.id.list_item_celeb_upvote);
			viewHolder.tvQuestionBy = (TextView) convertView
					.findViewById(R.id.list_item__celb_question_by);
			viewHolder.tvQuestionYou = (TextView) convertView
					.findViewById(R.id.list_item_celb_question_you);
			viewHolder.tvQuesCategory = (TextView) convertView
					.findViewById(R.id.list_item_celb_cetegory);
			viewHolder.ivSeperator = (ImageView) convertView
					.findViewById(R.id.view_separator);
			viewHolder.questionContainer = (RelativeLayout) convertView
					.findViewById(R.id.rl_question_one_texts);

			convertView.setTag(viewHolder);
		}
		viewHolder = (ViewHolder) convertView.getTag();

		viewHolder.tvVoteCounts.setText(getAskCountString(questionObject
				.getQuestion().getAsk_count()));
		viewHolder.tvUpvote.setTag(position);
		if (questionObject.getQuestion().getBody() != null)
			viewHolder.tvQuestion.setText(questionObject.getQuestion()
					.getBody());

		if (questionObject.getQuestion().getQuestion_author().getFull_name() != null)
			viewHolder.tvQuestionBy.setText(TextUtils.concat(
					"",
					Html.fromHtml("<b><font color='#a7a7a7'>"
							+ questionObject.getQuestion().getQuestion_author()
									.getFull_name() + "</font></b>")));

		// Setting visibility of upvote based on if the question is asked by
		// the current user and then if it is already upvoted or not
		if (questionObject.getQuestion().getQuestion_author().getId()
				.equals(PrefUtils.getUserID())) {
			viewHolder.tvUpvote
					.setBackgroundResource(R.drawable.bg_rectangle_rednocorner);
			viewHolder.tvUpvote.setText("Share Question");
			viewHolder.tvQuestionYou.setText("");

		} else {
			viewHolder.tvUpvote.setVisibility(View.VISIBLE);
			if (questionObject.getQuestion().is_voted()) {
				viewHolder.tvUpvote
						.setBackgroundResource(R.drawable.bg_rectangle_rednocorner_transparent);
				Log.d("SLA",
						"Setting the text to Answer Request in Ask Question Adapter");
				viewHolder.tvUpvote.setText("Answer Requested");
				viewHolder.tvQuestionYou.setText("You, ");
				viewHolder.tvVoteCounts
						.setText(getAskCountString(questionObject.getQuestion()
								.getAsk_count() - 1));
				Log.i("Upvote", "upvoted hai re");
			} else {
				viewHolder.tvUpvote
						.setBackgroundResource(R.drawable.bg_rectangle_rednocorner);
				Log.d("SLA",
						"Setting the text to Request Answer in Ask Question Adapter");
				viewHolder.tvUpvote.setText("Request Answer");
				viewHolder.tvQuestionYou.setText("");
			}
			viewHolder.tvUpvote.setTag(position);
			viewHolder.tvUpvote.setTag(R.id.list_item_celeb_votecounts,
					viewHolder.tvVoteCounts);
			viewHolder.tvUpvote.setTag(R.id.list_item_celb_question_you,
					viewHolder.tvQuestionYou);
			viewHolder.tvUpvote.setOnClickListener(AskQuestionAdapter.this);
		}

		// Setting onclick listeners and tags for question author link
		viewHolder.tvQuestionBy.setOnClickListener(AskQuestionAdapter.this);
		viewHolder.tvQuestionBy.setTag(position);

		if (listQuestion.get(position).getType() != null
				&& listQuestion.get(position).getType()
						.equals(QuestionObjectHolder.TYPE_SHARE_QUESTION)) {
			viewHolder.tvQuesCategory.setText("Shared Question");
			viewHolder.tvQuesCategory.setVisibility(View.VISIBLE);
			viewHolder.ivSeperator.setVisibility(View.VISIBLE);
			viewHolder.questionContainer
					.setBackgroundResource(R.drawable.bg_rectangle_white_nocorner);
			showOtherHeader = true;
		} else if (listQuestion.get(position).getType() != null
				&& listQuestion.get(position).getType()
						.equals(QuestionObjectHolder.TYPE_CURRENT_QUESTION)) {
			viewHolder.tvQuesCategory.setText("My Question(s)");
			viewHolder.tvQuesCategory.setVisibility(View.VISIBLE);
			viewHolder.ivSeperator.setVisibility(View.VISIBLE);
			showOtherHeader = true;
		} else if (listQuestion.get(position).getType() != null
				&& listQuestion.get(position).getType()
						.equals(QuestionObjectHolder.TYPE_OTHER_QUESTION)
				&& showOtherHeader) {
			viewHolder.tvQuesCategory.setText("Other Question(s)");
			viewHolder.tvQuesCategory.setVisibility(View.VISIBLE);
			viewHolder.ivSeperator.setVisibility(View.VISIBLE);

		} else {
			viewHolder.tvQuesCategory.setVisibility(View.GONE);
			viewHolder.ivSeperator.setVisibility(View.GONE);
			viewHolder.questionContainer
					.setBackgroundResource(R.drawable.bg_rectangle_blacknocorner);
		}
		viewHolder.tvUpvote.setOnClickListener(AskQuestionAdapter.this);
		return convertView;
	}

	static class ViewHolder {
		TextView tvQuestion;
		TextView tvVoteCounts, tvUpvote;
		TextView tvQuestionBy;
		TextView tvQuestionYou;
		LinearLayout linearLayout;
		ImageView ivSeperator;
		TextView tvQuesCategory;
		RelativeLayout questionContainer;
	}

	@Override
	public void onClick(View view) {
		int position = Integer.parseInt(view.getTag().toString());
		Log.d("shareIds", "clicked outside upvote");
		switch (view.getId()) {
		case R.id.list_item_celeb_upvote:
			position = Integer.parseInt(view.getTag().toString());
			Log.d("shareIds",
					""
							+ position
							+ "  "
							+ listQuestion.get(position).getQuestion()
									.getQuestion_author().getId() + "  "
							+ PrefUtils.getUserID());
			if (listQuestion.get(position).getQuestion().getQuestion_author()
					.getId().equals(PrefUtils.getUserID())) {
				Log.d("sharedial", "inside if");
				showCustomDialog(position);
			} else
				vote(position, view);
			break;

		case R.id.list_item__celb_question_by:
			if (!listQuestion.get(position).getQuestion().is_anonymous())
				mBaseActivity.profileActivity(listQuestion.get(position)
						.getQuestion().getQuestion_author().getId(),
						listQuestion.get(position).getQuestion()
								.getQuestion_author());
			break;
		}
	}

	private void vote(int position, View view) {

		QuestionObject questionObject = listQuestion.get(position)
				.getQuestion();
		TextView tvCount = (TextView) view
				.getTag(R.id.list_item_celeb_votecounts);
		TextView tvQuesYou = (TextView) view
				.getTag(R.id.list_item_celb_question_you);
		TextView tvUpvote = (TextView) view;
		int askCount = 0;

		if (!questionObject.is_voted()) {
			tvUpvote.setBackgroundResource(R.drawable.bg_rectangle_rednocorner_transparent);
			tvUpvote.setText("Answer Requested");
			askCount = listQuestion.get(position).getQuestion().getAsk_count() + 1;
			tvQuesYou.setText("You, ");
			listQuestion.get(position).getQuestion().setIs_voted(true);
			Controller.upvoteQuestion(mBaseActivity, questionObject.getId(),
					new RequestListener() {

						@Override
						public void onRequestStarted() {

						}

						@Override
						public void onRequestError(int errorCode, String message) {
							// Controller.showToast(mContext, message);
						}

						@Override
						public void onRequestCompleted(Object responseObject) {
							// Controller.showToast(mContext, "Upvote Success");

						}
					});
			MixpanelUtils.sendGenericEvent(listQuestion.get(position)
					.getQuestion().getId(), null, null,
					Constant.ScreenSpecs.SCREEN_QUESTION_LIST,
					MixpanelUtils.QUESTION_UPVOTED, ((Activity) mBaseActivity));
		} else {
			tvUpvote.setBackgroundResource(R.drawable.bg_rectangle_rednocorner);
			tvUpvote.setText("Request Answer");
			askCount = listQuestion.get(position).getQuestion().getAsk_count() - 1;
			tvQuesYou.setText("");
			listQuestion.get(position).getQuestion().setIs_voted(false);
			Controller.downvoteQuestion(mBaseActivity, questionObject.getId(),
					new RequestListener() {

						@Override
						public void onRequestStarted() {
						}

						@Override
						public void onRequestError(int errorCode, String message) {
							// Controller.showToast(mContext, message);
						}

						@Override
						public void onRequestCompleted(Object responseObject) {
							// Controller.showToast(mContext, "Devote Success");
						}
					});
			MixpanelUtils.sendGenericEvent(listQuestion.get(position)
					.getQuestion().getId(), null, null,
					Constant.ScreenSpecs.SCREEN_QUESTION_LIST,
					MixpanelUtils.QUESTION_DOWNVOTED,
					((Activity) mBaseActivity));
		}
		listQuestion.get(position).getQuestion().setAsk_count(askCount);

		// tvCount.setText(getAskCountString(questionObject.getAsk_count())) ;
	}

	private String getAskCountString(int askCount) {

		if (askCount > 1) {
			if (askCount == 2)
				return " and "
						+ (askCount-1)
						+ " "
						+ mBaseActivity.getResources().getString(
								R.string.question_upvote_singular);
			else
				return " and "
						+ (askCount-1)
						+ " "
						+ mBaseActivity.getResources().getString(
								R.string.question_upvote_plural);
		} else
			return "";
	}

	public void showCustomDialog(int position) {
		ShareDialog cdd = new ShareDialog(mBaseActivity,
				R.layout.share_dialog_post_layout, listQuestion.get(position)
						.getQuestion(), 3, listQuestion.get(position)
						.getQuestion().getQuestion_to().getId()
						.equals(PrefUtils.getUserID()), "ASKCELEB", null);
		cdd.show();

	}
}
