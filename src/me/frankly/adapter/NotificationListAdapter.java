package me.frankly.adapter;

import java.util.List;

import me.frankly.R;
import me.frankly.model.newmodel.NotificationModel;
import me.frankly.util.ImageUtil;
import me.frankly.util.MyUtilities;
import me.frankly.util.SLog;
import me.frankly.view.CircularImageView;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.gms.internal.bo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class NotificationListAdapter extends BaseAdapter {

	private Context mContext;
	private List<NotificationModel> mListNotificationObjects;
	private long mCurrentTime;

	public NotificationListAdapter(Context context,
			List<NotificationModel> list) {
		mContext = context;
		mListNotificationObjects = list;

	}

	@Override
	public int getCount() {
		return mListNotificationObjects.size();
	}

	public void add(List<NotificationModel> listNotification, long currentTime) {
		mListNotificationObjects.addAll(listNotification);
		mCurrentTime = currentTime;
		notifyDataSetChanged();
	}

	@Override
	public Object getItem(int position) {
		return mListNotificationObjects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		SLog.i("ricky", "Notification adapter " + position);

		NotificationModel notificationObject = mListNotificationObjects
				.get(position);

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.list_item_nofification, null);
			viewHolder = new ViewHolder();
			viewHolder.tvTime = (TextView) convertView
					.findViewById(R.id.list_item_notifi_time);
			viewHolder.tvname = (TextView) convertView
					.findViewById(R.id.list_item_notifi_name);

			viewHolder.circularImageView = (CircularImageView) convertView
					.findViewById(R.id.list_item_notifi_pic);

			convertView.setTag(viewHolder);
		}
		viewHolder = (ViewHolder) convertView.getTag();
		if (notificationObject.getText() != null
				&& notificationObject.getStyled_text() != null) {
			final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(
					255, 255, 255));

			// Span to set text color to some RGB value
			final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

			String notificationText = notificationObject.getStyled_text();
			int boldTextStart = notificationText.indexOf("<b>");
			int boldTextEnd = notificationText.indexOf("</b>");
			if (boldTextStart != -1 && boldTextEnd != -1) {

				SpannableStringBuilder sb = new SpannableStringBuilder(
						notificationText);

				sb.setSpan(fcs, boldTextStart + 3, boldTextEnd,
						Spannable.SPAN_INCLUSIVE_INCLUSIVE);

				sb.setSpan(bss, boldTextStart + 3, boldTextEnd,
						Spannable.SPAN_INCLUSIVE_INCLUSIVE);
				sb.replace(boldTextStart, boldTextStart + 3, "");
				sb.replace(boldTextEnd - 3, boldTextEnd + 1, "");

				viewHolder.tvname.setText(sb);
			} else {
				String str = notificationObject.getText();

				SpannableStringBuilder sb = new SpannableStringBuilder(str);
				// Span to make text bold
				sb.setSpan(fcs, 0, str.indexOf(' '),
						Spannable.SPAN_INCLUSIVE_INCLUSIVE);

				// Set the text color
				sb.setSpan(bss, 0, str.indexOf(' '),
						Spannable.SPAN_INCLUSIVE_INCLUSIVE);
				viewHolder.tvname.setText(sb);

			}
		}
		
		
		viewHolder.tvTime.setText(MyUtilities.getProperTimeString(mCurrentTime,
				notificationObject.getUpdated_at()));
		
		if (notificationObject.getIcon_url() != null) {

			DisplayImageOptions options = CircularImageView
					.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
			ImageLoader.getInstance().displayImage(
					ImageUtil.getThumbUrl(notificationObject.getIcon_url()),
					viewHolder.circularImageView, options);
		} else {
			viewHolder.circularImageView.setImageResource(R.drawable.icon);
		}
		return convertView;
	}

	static class ViewHolder {
		TextView tvname;
		TextView tvTime;
		CircularImageView circularImageView;
	}

}
