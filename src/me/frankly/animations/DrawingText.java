package me.frankly.animations;

import me.frankly.util.MyUtilities;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class DrawingText extends View{
	
	float x, y;
	boolean check = true;
	Context context;
	String text = "";

	// CONSTRUCTOR
	public DrawingText(Context context, float x, float y, String text) {
		super(context);
		this.x = x;
		this.y = y;
		this.context = context;
		this.text = text;
		setFocusable(true);
	}

	@Override
	protected void onDraw(final Canvas canvas) {

		Paint paint = new Paint(); 
		paint.setColor(Color.WHITE); 
		paint.setTextSize(52);
		paint.setStrokeWidth(4f);
		paint.setTypeface(MyUtilities.getTypeface(context, "bariol"));
		
		if(text.contains("\n"))
		{
			for(String line: text.split("\n")){
			      canvas.drawText(line, x, y, paint);
			      y+= -paint.ascent() + paint.descent();
			}
		}
		else
		{
			canvas.drawText(text, x, y, paint);
		}
		
	}

}
