package me.frankly.animations;

import me.frankly.util.MyUtilities;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class BallMoveDown extends View{
	
	private Paint maskPaint;
	float x, y;
	boolean check = true;
	Context context;

	// CONSTRUCTOR
	public BallMoveDown(Context context, float x, float y, boolean check) {
		super(context);
		this.context = context;
		this.x = x;
		this.y = y;
		this.check = check;
		setFocusable(true);
	}

	@Override
	protected void onDraw(final Canvas canvas) {

		maskPaint = new Paint();
		maskPaint.setColor(Color.WHITE);
		maskPaint.setStyle(Paint.Style.FILL);
		if(check)
		{
			/*maskPaint.setStrokeWidth(4f);
			canvas.drawCircle(x, y, 15, maskPaint);*/
			Paint paint = new Paint(); 
			paint.setColor(Color.WHITE); 
			paint.setTextSize(20); 
			paint.setTypeface(MyUtilities.getTypeface(context, "bariol"));
			canvas.drawText("Open Profile", 0, y, paint);
		}
		else
		{
			maskPaint.setStrokeWidth(2f);
			canvas.drawLine(x, y, x, y+10 , maskPaint);
		}
		
		 
		
		
	}


}
