package me.frankly.animations;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

public class FocusIn extends View{
	
	private Paint maskPaint;
	float x, y;
	int radius1, radius2;
	boolean check = true;

	// CONSTRUCTOR
	public FocusIn(Context context, float x, float y, int radius1, int radius2) {
		super(context);
		this.x = x;
		this.y = y;
		this.radius1 = radius1;
		this.radius2 = radius2;
		setFocusable(true);
	}

	@Override
	protected void onDraw(final Canvas canvas) {

		maskPaint = new Paint();
		maskPaint.setColor(Color.parseColor("#66FFFFFF"));
		/*maskPaint
				.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));*/
		maskPaint.setStyle(Paint.Style.STROKE);
		maskPaint.setStrokeWidth(10f);
		canvas.drawCircle(x, y, radius1, maskPaint);
		canvas.drawCircle(x, y, radius2, maskPaint);
	}

	
}
