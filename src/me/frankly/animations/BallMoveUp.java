package me.frankly.animations;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class BallMoveUp extends View{
	private Paint maskPaint;
	float x, y;
	boolean check = true;

	// CONSTRUCTOR
	public BallMoveUp(Context context, float x, float y, boolean check) {
		super(context);
		this.x = x;
		this.y = y;
		this.check = check;
		setFocusable(true);
	}

	@Override
	protected void onDraw(final Canvas canvas) {

		maskPaint = new Paint();
		maskPaint.setColor(Color.WHITE);
		maskPaint.setStyle(Paint.Style.FILL);
		if(check)
		{
			/*maskPaint.setStrokeWidth(10f);
			canvas.drawCircle(x, y, 20, maskPaint);*/
		}
		else
		{
			maskPaint.setStrokeWidth(2f);
			canvas.drawLine(x, y, x, y-10 , maskPaint);
		}
		
	}

}
