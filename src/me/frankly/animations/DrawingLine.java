package me.frankly.animations;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

public class DrawingLine extends View {

	int framesPerSecond = 60;
	long animationDuration = 2000; // 10 seconds

	Matrix matrix = new Matrix(); // transformation matrix

	Paint paint = new Paint(); // your paint

	long startTime;

	float x, y;
	int length;
	int segLength;
	private int count = 1;

	public DrawingLine(Context context, float centreX, float centreY,
			int length, int direction) {
		super(context);

		// start the animation:
		x = centreX;
		y = centreY;
		this.length = length;
		paint.setStrokeWidth(2);
		paint.setColor(Color.WHITE);
		this.startTime = System.currentTimeMillis();
		segLength = (int) (length / 100);
		this.postInvalidate();
		
		Log.e("ashu", "length=" + length + "seglength=" + segLength);
	}

	@Override
	protected void onDraw(Canvas canvas) {

		long elapsedTime = System.currentTimeMillis() - startTime;

		canvas.drawLine(x, y, x, y - (segLength * count), paint);
		Log.e("ashu", "count=" + count);
		count++;
		if (elapsedTime < animationDuration)
			this.postInvalidateDelayed(1000 / framesPerSecond);
	}

}