package me.frankly.view.croputil;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

public class ImageProcess {

	/*
	 * This method gets the image inside the template area and returns that
	 * bitmap. Process of how this method works: 1) Combine img and
	 * templateImage together with templateImage being on the top. 2) Create a
	 * new blank bitmap using the given width and height, which is the size of
	 * the template on the screen. 3) Starting in the center go through the
	 * image quadrant by quadrant copying the pixel value onto the new blank
	 * bitmap. 4) Once it hits the pixel colour values of the template lines,
	 * then set the pixel values from that point on transparent. 5) Return the
	 * cropped bitmap.
	 */
	@SuppressLint("NewApi")
	public static Bitmap cropImage(Bitmap img, Bitmap templateImage, int width,
			int height) {
		// Merge two images together.

		width = 400;
		height = 400;

		Log.i("Crop", "CropImage  Img  Width " + img.getWidth());
		Log.i("Crop", "CropImage  Img ight He" + img.getHeight());
		Log.i("Crop", "CropImage  Template  width " + templateImage.getWidth());
		Log.i("Crop", "CropImage  template height " + templateImage.getHeight());
		Log.i("Crop", "CropImage  width " + width);
		Log.i("Crop", "CropImage  height " + height);
		
		if(Build.VERSION.SDK_INT>=19)
		{
			img.setConfig(Bitmap.Config.ARGB_8888);
		}
		Canvas combineImg = new Canvas(img);
		combineImg.drawBitmap(img, 0f, 0f, null);
		combineImg.drawBitmap(templateImage, 0f, 0f, null);

		// Create new blank ARGB bitmap.
		Bitmap finalBm = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);

		// Get the coordinates for the middle of combineImg.
		int hMid = img.getHeight() / 2;
		int wMid = img.getWidth() / 2;
		int hfMid = finalBm.getHeight() / 2;
		int wfMid = finalBm.getWidth() / 2;

		int y2 = hfMid;
		int x2 = wfMid;

		int widthstart = (img.getWidth() - width) / 2;
		int widthend = img.getWidth() - widthstart;
		int heightstart = (img.getHeight() - height) / 2;
		int heightend = heightstart + height;

		Log.i("Crop", "CropImage  widthstart " + widthstart);
		Log.i("Crop", "CropImage  widthend " + widthend);
		Log.i("Crop", "CropImage  heightstart " + heightstart);
		Log.i("Crop", "CropImage  heightend " + heightend);

		for (int i = widthstart, ii = 0; i < widthend; i++, ii++) {
			for (int j = heightstart, jj = 0; j < heightend; j++, jj++) {
				int px = img.getPixel(i, j);
				finalBm.setPixel(ii, jj, px);
			}
		}

		/*
		 * for (int y = hMid; y >= 0; y--) { boolean template = false; // Check
		 * Upper-left section of combineImg. for (int x = wMid; x >= 0; x--) {
		 * if (x2 < 0) { break; }
		 * 
		 * int px = bm.getPixel(x, y); if (Color.red(px) == 234 &&
		 * Color.green(px) == 157 && Color.blue(px) == 33) { template = true;
		 * finalBm.setPixel(x2, y2, Color.TRANSPARENT); } else if (template) {
		 * finalBm.setPixel(x2, y2, Color.TRANSPARENT); } else {
		 * finalBm.setPixel(x2, y2, px); } x2--; } // Check upper-right section
		 * of combineImage. x2 = wfMid; template = false; for (int x = wMid; x <
		 * bm.getWidth(); x++) { if (x2 >= finalBm.getWidth()) { break; }
		 * 
		 * int px = bm.getPixel(x, y); if (Color.red(px) == 234 &&
		 * Color.green(px) == 157 && Color.blue(px) == 33) { template = true;
		 * finalBm.setPixel(x2, y2, Color.TRANSPARENT); } else if (template) {
		 * finalBm.setPixel(x2, y2, Color.TRANSPARENT); } else {
		 * finalBm.setPixel(x2, y2, px); } x2++; }
		 * 
		 * // Once we reach the top-most part on the template line, set pixel
		 * value transparent // from that point on. int px = bm.getPixel(wMid,
		 * y); if (Color.red(px) == 234 && Color.green(px) == 157 &&
		 * Color.blue(px) == 33) { for (int y3 = y2; y3 >= 0; y3--) { for (int
		 * x3 = 0; x3 < finalBm.getWidth(); x3++) { finalBm.setPixel(x3, y3,
		 * Color.TRANSPARENT); } } break; }
		 * 
		 * x2 = wfMid; y2--; }
		 * 
		 * x2 = wfMid; y2 = hfMid; for (int y = hMid; y <= bm.getHeight(); y++)
		 * { boolean template = false; // Check bottom-left section of
		 * combineImage. for (int x = wMid; x >= 0; x--) { if (x2 < 0) { break;
		 * }
		 * 
		 * int px = bm.getPixel(x, y); if (Color.red(px) == 234 &&
		 * Color.green(px) == 157 && Color.blue(px) == 33) { template = true;
		 * finalBm.setPixel(x2, y2, Color.TRANSPARENT); } else if (template) {
		 * finalBm.setPixel(x2, y2, Color.TRANSPARENT); } else {
		 * finalBm.setPixel(x2, y2, px); } x2--; }
		 * 
		 * // Check bottom-right section of combineImage. x2 = wfMid; template =
		 * false; for (int x = wMid; x < bm.getWidth(); x++) { if (x2 >=
		 * finalBm.getWidth()) { break; }
		 * 
		 * int px = bm.getPixel(x, y); if (Color.red(px) == 234 &&
		 * Color.green(px) == 157 && Color.blue(px) == 33) { template = true;
		 * finalBm.setPixel(x2, y2, Color.TRANSPARENT); } else if (template) {
		 * finalBm.setPixel(x2, y2, Color.TRANSPARENT); } else {
		 * finalBm.setPixel(x2, y2, px); } x2++; }
		 * 
		 * // Once we reach the bottom-most part on the template line, set pixel
		 * value transparent // from that point on. int px = bm.getPixel(wMid,
		 * y); if (Color.red(px) == 234 && Color.green(px) == 157 &&
		 * Color.blue(px) == 33) { for (int y3 = y2; y3 < finalBm.getHeight();
		 * y3++) { for (int x3 = 0; x3 < finalBm.getWidth(); x3++) {
		 * finalBm.setPixel(x3, y3, Color.TRANSPARENT); } } break; }
		 * 
		 * x2 = wfMid; y2++; }
		 */

		// Get rid of images that we finished with to save memory.
		img.recycle();
		templateImage.recycle();
//		bm.recycle();
		return finalBm;
	}

	/*
	 * This method gets the image inside the template area and returns that
	 * bitmap. Process of how this method works: 1) Combine img and
	 * templateImage together with templateImage being on the top. 2) Create a
	 * new blank bitmap using the given width and height, which is the size of
	 * the template on the screen. 3) Starting in the center go through the
	 * image quadrant by quadrant copying the pixel value onto the new blank
	 * bitmap. 4) Once it hits the pixel colour values of the template lines
	 * exit out of the loop. (Because there is no point in setting transparent
	 * pixel values to pixels that are already transparent.) 5) Return the
	 * cropped bitmap.
	 */
	public static Bitmap cropImageVer2(Bitmap img, Bitmap templateImage,
			int width, int height) {
		// Merge two images together.
		Bitmap bm = Bitmap.createBitmap(img.getWidth(), img.getHeight(),
				Bitmap.Config.ARGB_8888);
		Canvas combineImg = new Canvas(bm);
		combineImg.drawBitmap(img, 0f, 0f, null);
		combineImg.drawBitmap(templateImage, 0f, 0f, null);

		// Create new blank ARGB bitmap.
		Bitmap finalBm = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);

		// Get the coordinates for the middle of bm.
		int hMid = bm.getHeight() / 2;
		int wMid = bm.getWidth() / 2;
		int hfMid = finalBm.getHeight() / 2;
		int wfMid = finalBm.getWidth() / 2;

		int y2 = hfMid;
		int x2 = wfMid;

		for (int y = hMid; y >= 0; y--) {
			// Check Upper-left section of combineImg.
			for (int x = wMid; x >= 0; x--) {
				if (x2 < 0) {
					break;
				}

				int px = bm.getPixel(x, y);
				// Get out of loop once it hits the left side of the template.
				if (Color.red(px) == 234 && Color.green(px) == 157
						&& Color.blue(px) == 33) {
					break;
				} else {
					finalBm.setPixel(x2, y2, px);
				}
				x2--;
			}

			// Check upper-right section of combineImage.
			x2 = wfMid;
			for (int x = wMid; x < bm.getWidth(); x++) {
				if (x2 >= finalBm.getWidth()) {
					break;
				}

				int px = bm.getPixel(x, y);
				// Get out of loop once it hits the right side of the template.
				if (Color.red(px) == 234 && Color.green(px) == 157
						&& Color.blue(px) == 33) {
					break;
				} else {
					finalBm.setPixel(x2, y2, px);
				}
				x2++;
			}

			// Get out of loop once it hits top most part of the template.
			int px = bm.getPixel(wMid, y);
			if (Color.red(px) == 234 && Color.green(px) == 157
					&& Color.blue(px) == 33) {
				break;
			}
			x2 = wfMid;
			y2--;
		}

		x2 = wfMid;
		y2 = hfMid;
		for (int y = hMid; y <= bm.getHeight(); y++) {
			// Check bottom-left section of combineImage.
			for (int x = wMid; x >= 0; x--) {
				if (x2 < 0) {
					break;
				}

				int px = bm.getPixel(x, y);
				// Get out of loop once it hits the left side of the template.
				if (Color.red(px) == 234 && Color.green(px) == 157
						&& Color.blue(px) == 33) {
					break;
				} else {
					finalBm.setPixel(x2, y2, px);
				}
				x2--;
			}

			// Check bottom-right section of combineImage.
			x2 = wfMid;
			for (int x = wMid; x < bm.getWidth(); x++) {
				if (x2 >= finalBm.getWidth()) {
					break;
				}

				int px = bm.getPixel(x, y);
				// Get out of loop once it hits the right side of the template.
				if (Color.red(px) == 234 && Color.green(px) == 157
						&& Color.blue(px) == 33) {
					break;
				} else {
					finalBm.setPixel(x2, y2, px);
				}
				x2++;
			}

			// Get out of loop once it hits bottom most part of the template.
			int px = bm.getPixel(wMid, y);
			if (Color.red(px) == 234 && Color.green(px) == 157
					&& Color.blue(px) == 33) {
				break;
			}
			x2 = wfMid;
			y2++;
		}

		// Get rid of images that we finished with to save memory.
		img.recycle();
		templateImage.recycle();
		bm.recycle();
		return finalBm;
	}
}
