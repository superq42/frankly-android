package me.frankly.view.activity;

import java.util.ArrayList;
import java.util.HashSet;

import me.frankly.R;
import me.frankly.adapter.CategoryCelebsAdapter;
import me.frankly.adapter.CategoryCelebsAdapter.OnFollowedListener;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.response.CategoryCeleb;
import me.frankly.model.response.CategoryCelebList;
import me.frankly.util.JsonUtils;
import me.frankly.util.PrefUtils;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FollowCelebsActivity extends BaseActivity implements
		RequestListener, OnFollowedListener, OnClickListener {
	private TextView followStatusTextView;
	private ListView celebsListView;
	private View listHeaderView;
	private CategoryCelebsAdapter peopleAdapter;
	private ArrayList<CategoryCeleb> categoryCelebs = new ArrayList<CategoryCeleb>();
	private HashSet<String> followedUserIds = new HashSet<String>();
	private ImageView loaderView;
	private AnimationDrawable loaderDrawable;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#Sahil FoCelebAc:";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_follow_celebs);

		fileDebugLog(FILE_DEBUG_TAG + " onCreate", "Called");

		celebsListView = (ListView) findViewById(R.id.lv_cat_celebs);
		listHeaderView = LayoutInflater.from(this).inflate(
				R.layout.list_item_follow_header, null);
		celebsListView.addHeaderView(listHeaderView);

		followStatusTextView = (TextView) findViewById(R.id.tv_done_state);
		followStatusTextView.setText("Follow Atleast 3 people.");
		followStatusTextView.setEnabled(false);
		followStatusTextView.setOnClickListener(this);

		peopleAdapter = new CategoryCelebsAdapter(this, categoryCelebs,
				CategoryCelebsAdapter.MODE_FOLLOW);
		peopleAdapter.setOnFollowedListener(this);
		loaderView = new ImageView(this);
		loaderDrawable = (AnimationDrawable) getResources().getDrawable(
				R.drawable.loader_general);
		loaderView.setImageDrawable(loaderDrawable);
		loaderDrawable.start();
		celebsListView.addFooterView(loaderView);
		celebsListView.setAdapter(peopleAdapter);
		getPeopleToFollow();
	}

	private void getPeopleToFollow() {
		fileDebugLog(FILE_DEBUG_TAG + " getPeopleToFollow", "Called");

		Controller.getPeopleToFollow(this, this);
	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		fileDebugLog(FILE_DEBUG_TAG + " onRequestCompleted", "Called");
		Log.d("FCA-search", "cat celeb response: " + (String) responseObject);
		final CategoryCelebList celebList = (CategoryCelebList) JsonUtils
				.objectify((String) responseObject, CategoryCelebList.class);
		if (!isFinishing()) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (celebList != null && celebList.getResults() != null) {

						hideLoaderView();
						categoryCelebs.addAll(celebList.getResults());
						peopleAdapter.notifyDataSetChanged();
					}
				}
			});
		}
	}

	@Override
	public void onRequestError(int errorCode, String message) {

	}

	private void updateValue() {
		fileDebugLog(FILE_DEBUG_TAG + " updateValue", "Called");

		if (followedUserIds.size() < 3) {
			followStatusTextView.setText("Follow "
					+ (3 - followedUserIds.size()) + " more people");
			followStatusTextView.setBackgroundDrawable(null);
			followStatusTextView.setEnabled(false);
		} else {
			followStatusTextView.setPadding(20, 20, 20, 20);
			followStatusTextView.setText(Html
					.fromHtml("&#09; &#10004; Done, Continue &#09;"));
			followStatusTextView
					.setBackgroundResource(R.drawable.bg_rectangle_rednocorner);
			followStatusTextView.setEnabled(true);
		}

	}

	@Override
	public void onClick(View v) {
		fileDebugLog(FILE_DEBUG_TAG + " onClick", "Called");

		PrefUtils.setPeopleFollowed();
		navigateToMainActivity();

	}

	private void navigateToMainActivity() {
		fileDebugLog(FILE_DEBUG_TAG + " navigateToMainActivity", "Called");
		Log.d("Navigate", "navigateToMainActivity");
		Intent i = new Intent(this, FranklyHomeActivity.class);
		i.putExtra(LauncherActivity.KEY_IS_NEW_USER, true);
		i.setData(getIntent().getData());
		finish();
		startActivity(i);
	}

	@Override
	public void onFollowed(String userId) {
		fileDebugLog(FILE_DEBUG_TAG + " onFollowed", "Called");

		if (followedUserIds.add(userId))
			updateValue();
	}

	@Override
	public void onUnfollowed(String userId) {
		fileDebugLog(FILE_DEBUG_TAG + " onUnfollowed", "Called");

		if (followedUserIds.remove(userId))
			updateValue();
	}

	@Override
	public void onResume() {
		fileDebugLog(FILE_DEBUG_TAG + " onResume", "Called");

		super.onResume();
	}

	private void hideLoaderView() {
		fileDebugLog(FILE_DEBUG_TAG + " hideLoaderView", "Called");

		if (loaderView.getVisibility() == View.VISIBLE) {
			loaderView.setVisibility(View.GONE);
			loaderDrawable.stop();
		}
	}

	private void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}
}
