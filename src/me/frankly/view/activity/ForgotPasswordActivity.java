package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.util.MyUtilities;
import me.frankly.util.RegexUtils;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ForgotPasswordActivity extends Activity implements OnClickListener {

	private EditText emailEditText;
	private View forgotLinLay, forgotTVMessage;
	private ImageView iv_loader;
	private AnimationDrawable loaderAnimation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forget_password_screen);
		
		Controller.registerGAEvent("ForgetPasswordActivity");
		
		MyUtilities.showKeyboard(this);
		emailEditText = (EditText) findViewById(R.id.forgot_et_email);
		forgotLinLay = findViewById(R.id.forgot_password_ll);
		View forgotBtnDone = findViewById(R.id.forgot_done);
		View btn_back = findViewById(R.id.action_bar_back_btn);
		forgotTVMessage = findViewById(R.id.forgot_tv_message);
		TextView title_text = (TextView) findViewById(R.id.action_bar_title_text);
		title_text.setText("Forgot Password");

		iv_loader = (ImageView) findViewById(R.id.iv_loader);
		iv_loader.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) iv_loader.getDrawable();

		forgotBtnDone.setOnClickListener(this);
		btn_back.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.forgot_done:
			if(Controller.isNetworkConnectedWithMessage(this))
				sendForgotPassword();
			break;
		case R.id.action_bar_back_btn:
			hideKeyboard();
			onBackPressed();
			break;
		}
	}

	private void sendForgotPassword() {
		/**
		 * send here the email address to the server and on its completion 
		 * set the visibility of the layout gone.Also set the textview visible.
		 **/
		
		MyUtilities.hideKeyboard(emailEditText, this);

		String str = emailEditText.getText().toString().trim();
		if (RegexUtils.isValidEmail(str)) {
			Controller.forgotPassword(this,str, forgotPasswordListener, false);
			iv_loader.setVisibility(View.VISIBLE);
			loaderAnimation.start();
		} else if (RegexUtils.isValidUsername(str)) {
			Controller.forgotPassword(this,str, forgotPasswordListener, true);
			iv_loader.setVisibility(View.VISIBLE);
			loaderAnimation.start();
		} else {
			Controller.showToast(this, "Invalid Email or Username Format!");
			return;
		}

	}
	
	

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(emailEditText.getWindowToken(), 0);
	}

	private RequestListener forgotPasswordListener = new RequestListener() {

		@Override
		public void onRequestStarted() {
		}

		@Override
		public void onRequestError(int errorCode, final String message) {
			loaderAnimation.stop();
			iv_loader.setVisibility(View.GONE);
			Controller.showToast(ForgotPasswordActivity.this,
					"Some error occurred!");

		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			Controller.showToast(ForgotPasswordActivity.this, "Request Sent");
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					forgotLinLay.setVisibility(View.GONE);
					forgotTVMessage.setVisibility(View.VISIBLE);
					loaderAnimation.stop();
					iv_loader.setVisibility(View.GONE);
				}
			});

		}
	};

}