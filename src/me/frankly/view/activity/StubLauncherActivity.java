package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.AppConfig;
import me.frankly.config.Controller;
import me.frankly.config.FranklyApplication;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.AppUpdateRequest;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This class checks and show options for force update
 */
public class StubLauncherActivity extends BaseActivity implements
		RequestListener, OnClickListener {

	private static final String LOG_TAG = StubLauncherActivity.class
			.getSimpleName();

	private ImageView fImgView, fUpdateBtnImgView, fUpdateImgView;
	private TextView fUpdateTxtView;
	private Animation fadeInAnim, anim;
	private long currentTime;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stub);
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG,
					"onCreate() Saved App Open Time : "
							+ PrefUtils.getLastUpdateCheckTime());
		}

		currentTime = System.currentTimeMillis();

		if (currentTime - PrefUtils.getLastUpdateCheckTime() > (10 * 60 * 60 * 1000)) {
			Log.d("SLA", "calling app update info");
			Controller.getAppUpdateInfo(this, FranklyApplication.mVersionCode,
					this);
		} else {
			launcherActivity();
		}
	}

	@Override
	public void onRequestStarted() {
		// nothing to do here
	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "onRequestCompleted(): " + responseObject);
		}
		PrefUtils.setLastUpdateCheckTime(currentTime);

		AppUpdateRequest appUpdateReq = (AppUpdateRequest) JsonUtils.objectify(
				responseObject.toString(), AppUpdateRequest.class);
		if (appUpdateReq == null) {
			launcherActivity();
		} else if (appUpdateReq.getHard_update()) {
			showHardUpdate();
		} else if (appUpdateReq.getSoft_update()) {
			showSoftUpdate();
		} else {
			launcherActivity();
		}
	}

	@Override
	public void onRequestError(int errorCode, String message) {
		if (AppConfig.DEBUG) {
			Log.e(LOG_TAG, "onRequestError(): " + message + ", " + errorCode);
		}
		launcherActivity();
	}

	private void showHardUpdate() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				fImgView = (ImageView) findViewById(R.id.frankly_img);
				fUpdateBtnImgView = (ImageView) findViewById(R.id.frankly_update_btn_img);
				fUpdateImgView = (ImageView) findViewById(R.id.frankly_update_need_img);
				fUpdateTxtView = (TextView) findViewById(R.id.frankly_update_txt);
				fUpdateBtnImgView.setOnClickListener(StubLauncherActivity.this);
				anim = AnimationUtils.loadAnimation(StubLauncherActivity.this,
						R.anim.anim_slide_and_fade);
				fadeInAnim = AnimationUtils.loadAnimation(
						StubLauncherActivity.this, R.anim.anim_fade_in);
				anim.setFillAfter(true);
				fadeInAnim.setFillAfter(true);
				fImgView.startAnimation(anim);
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						fUpdateBtnImgView.setVisibility(View.VISIBLE);
						fUpdateImgView.setVisibility(View.VISIBLE);
						fUpdateTxtView.setVisibility(View.VISIBLE);
						fUpdateBtnImgView.startAnimation(fadeInAnim);
						fUpdateImgView.startAnimation(fadeInAnim);
						fUpdateTxtView.startAnimation(fadeInAnim);
					}
				}, 1500);
			}
		});
	}

	private void showSoftUpdate() {
		PrefUtils.setSoftUpdate(true);
		launcherActivity();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.frankly_update_btn_img: {
			MyUtilities.openPlayStore(this);
			finish();
			break;
		}
		}
	}
}
