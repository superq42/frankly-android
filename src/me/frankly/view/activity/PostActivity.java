package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Constant.Key;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.config.Controller;
import me.frankly.view.fragment.AtomicPostFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class PostActivity extends BaseActivity implements OnClickListener {

	public static final int MODE_DEFAULT = 0;
	public static final int MODE_SHARE = 1;
	public static final int MODE_COMMENTS = 2;

	@Override
	protected void onCreate(Bundle arg0) {

		super.onCreate(arg0);
		setContentView(R.layout.layout_post_activity);

		Controller.registerGAEvent("PostActivity");

		ImageView backBtn = (ImageView) findViewById(R.id.action_bar_back_btn);
		backBtn.setOnClickListener(this);

		Intent i = getIntent();
		int screenMode = i.getIntExtra(Key.SCREEN_MODE, MODE_DEFAULT);
		String postObjectJson = i.getStringExtra(Constant.Key.KEY_POST_OBJECT);

		Bundle args = new Bundle();
		args.putString(Key.KEY_POST_OBJECT, postObjectJson);
		args.putBoolean(Key.KEY_IS_AUTOPLAY, true);
		args.putString(Key.KEY_PARENT_SCREEN_NAME, ScreenSpecs.SCREEN_POST);
		args.putInt(Key.SCREEN_MODE, screenMode);

		Fragment fragment = new AtomicPostFragment();
		fragment.setArguments(args);
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, fragment).commit();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_bar_back_btn: {
			finish();
			break;
		}
		}
	}
}
