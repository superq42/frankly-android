package me.frankly.view.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.appsflyer.BuildConfig;

import me.frankly.R;
import me.frankly.SDKPlayer.IPlayer.IPlayerStateChangedListener;
import me.frankly.adapter.FeedsPagerAdapter;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.PendingAnswersProgressListener;
import me.frankly.listener.RequestListener;
import me.frankly.listener.SwipeAnimationDoneListener;
import me.frankly.listener.universal.action.listener.CommentListener;
import me.frankly.listener.universal.action.listener.PostLikeListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.newmodel.NotificationApiResponse;
import me.frankly.model.newmodel.PendingAnswer;
import me.frankly.model.newmodel.PendingAnswersContainer;
import me.frankly.model.newmodel.PostObject;
import me.frankly.model.newmodel.SingleUniversalUserContainer;
import me.frankly.model.newmodel.UniqueEntity;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.response.DiscoverableObjectsContainer;
import me.frankly.model.response.PaginatedPostHolder;
import me.frankly.servicereceiver.PendingAnswerProgressReceiver;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.util.ShareUtils;
import me.frankly.util.Utilities;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.DirectionalViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class ProfileActivity extends BaseActivity implements
		OnPageChangeListener, OnClickListener, IPlayerStateChangedListener,
		OnTouchListener, UserFollowListener, CommentListener, PostLikeListener,
		PendingAnswersProgressListener {

	private final static String TAG = ProfileActivity.class.getSimpleName();

	private boolean isMoreDataAvailable = true, isLoading = false;
	private DirectionalViewPager mViewPager;
	public FeedsPagerAdapter mPagerAdapter;
	public ArrayList<DiscoverableObjectsContainer> objectsContainers;
	private int index = 0;
	private String userId;
	private boolean isPlayerInstanstiated = false;
	private AnimationDrawable loaderAnimation;
	private ImageView comment_iv_loader;
	private TextView tvNotifiactionCount;
	private ImageView imgMenu, shareProfileImageView, defShareImageView;
	private View answerQuestionsView;
	private boolean isMyProfile;
	private UniversalUser mUser;
	private View mActionBar, animView;
	private boolean isScreenEmpty = false;
	private String userName = null;

	// In this hashmap string will be the question ID
	private HashMap<String, PendingAnswersProgressListener> pendingAnsProgressListeners;

	public static final long SWIPE_TIMEOUT = 5000;

	private int mCurrentPage;
	private Animation animation1, animation2, animation3;
	private ImageView ivSwipe1, ivSwipe2, ivSwipe3;
	float swipePosY;
	private int pos;
	private ImageView imgNotification;
	PendingAnswerProgressReceiver pendingAnswerProgressReceiver;
	private ArrayList<SwipeAnimationDoneListener> swipeAnimationDoneListeners;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.profile_list_fragment);
		mActionBar = findViewById(R.id.frag_profile_actionbar);

		log("onCreate");
		
		Controller.registerGAEvent("ProfileActivity");

		objectsContainers = new ArrayList<DiscoverableObjectsContainer>();
		String userString = getIntent().getStringExtra(
				Constant.Key.KEY_USER_OBJECT);

		if (userString != null) {
			mUser = (UniversalUser) JsonUtils.objectify(userString,
					UniversalUser.class);
			Log.d("gcm", "" + mUser.getFirst_name());
			userId = mUser.getId();
		} else if (getIntent().hasExtra(Constant.Key.KEY_USER_ID)) {

			userId = getIntent().getStringExtra(Constant.Key.KEY_USER_ID);
		} else if (getIntent().hasExtra(Constant.Key.KEY_USER_NAME)) {
			userName = getIntent().getStringExtra(Constant.Key.KEY_USER_NAME);
			userId = getIntent().getStringExtra(Constant.Key.KEY_USER_NAME);
		}
		if (userName != null) {
			Log.d("ismyprofile", "username not null");
			isMyProfile = PrefUtils.getCurrentUserAsObject().getUsername()
					.equals(userName);
			if (isMyProfile)
				mUser = PrefUtils.getCurrentUserAsObject();
		} else if (userId != null) {
			Log.d("ismyprofile", "userid not null");
			isMyProfile = PrefUtils.getCurrentUserAsObject().getId()
					.equals(userId);
		}
		mViewPager = (DirectionalViewPager) findViewById(R.id.vp_fragment);

		mViewPager.setOrientation(DirectionalViewPager.VERTICAL);
		mViewPager.setOnPageChangeListener(this);

		mPagerAdapter = new FeedsPagerAdapter(getSupportFragmentManager(),
				objectsContainers, Constant.ScreenSpecs.SCREEN_PROFILE, userId);

		mViewPager.setAdapter(mPagerAdapter);
		mPagerAdapter.setPlayerStateChangeListner(this);
		Log.d("PIF", "set the feed pager adapter");

		animView = findViewById(R.id.rl_swipe_animation);
		animView.setOnTouchListener(this);
		ivSwipe1 = (ImageView) findViewById(R.id.iv_swipe_1);
		ivSwipe2 = (ImageView) findViewById(R.id.iv_swipe_2);
		ivSwipe3 = (ImageView) findViewById(R.id.iv_swipe_3);

		setProfileState();
		comment_iv_loader = (ImageView) findViewById(R.id.profile_iv_loader);
		comment_iv_loader.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) comment_iv_loader.getDrawable();
		shareProfileImageView = (ImageView) findViewById(R.id.iv_share_profile);
		defShareImageView = (ImageView) findViewById(R.id.iv_share_profile_default);
		RelativeLayout.LayoutParams p = (LayoutParams) shareProfileImageView
				.getLayoutParams();

		if (ShareUtils.isAppInstalled(ShareUtils.PKG_WHATSAPP, this)) {
			shareProfileImageView
					.setImageResource(R.drawable.ic_profile_whatsapp);
			shareProfileImageView.setTag(ShareUtils.PKG_WHATSAPP);
			shareProfileImageView.setOnClickListener(this);
		} else
			shareProfileImageView.setVisibility(View.GONE);

		showLoading(true);
		mViewPager.setOnPageChangeListener(this);
		UniversalActionListenerCollection.getInstance()
				.addGeneralCommentListener(this);
		UniversalActionListenerCollection.getInstance()
				.addGeneralFollowListener(this);
		UniversalActionListenerCollection.getInstance().addGeneralLikeListener(
				this);
		pendingAnswerProgressReceiver = new PendingAnswerProgressReceiver(this);
		LocalBroadcastManager
				.getInstance(this)
				.registerReceiver(
						pendingAnswerProgressReceiver,
						new IntentFilter(
								PendingAnswerProgressReceiver.FILTER_PENDING_ANSWER_PROGRESS));

		if (isMyProfile) {
			p.addRule(RelativeLayout.LEFT_OF, R.id.action_bar_btn_answer);
			p.rightMargin = 30;
			defShareImageView.setVisibility(View.GONE);
		} else {
			p.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			p.rightMargin = 30;
			defShareImageView.setVisibility(View.VISIBLE);
			defShareImageView.setOnClickListener(this);
		}

		pendingAnsProgressListeners = new HashMap<String, PendingAnswersProgressListener>();

		swipeAnimationDoneListeners = new ArrayList<SwipeAnimationDoneListener>();

		if (userId != null || userName != null) {
			if (isMyProfile) {
				updateUserProfileIndex(PrefUtils.getCurrentUserAsObject());
			} else {
				fetchUser();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mUser != null) {
			deletePendingAnswersFromList(objectsContainers);
			updatePendingAnswers(mUser);
			mPagerAdapter.notifyDataSetChanged();
		}

		if (PrefUtils.getIsFirstProfileSwipeAnimShown() == false
				&& mUser != null && objectsContainers.size() > 1
				&& !mUser.getId().equals(PrefUtils.getUserID()))
			resetSwipeTimer();
	}

	@Override
	protected void onStop() {
		Log.d("#skm profile activity", "onstop called");
		stopSwipeTimer();
		super.onStop();
	}

	private void deletePendingAnswersFromList(
			ArrayList<DiscoverableObjectsContainer> objectsContainersList) {

		if (objectsContainersList != null) {
			for (int i = 0; i < objectsContainersList.size(); i++) {
				DiscoverableObjectsContainer tempObjectsContainer = objectsContainersList
						.get(i);

				if (tempObjectsContainer.getType().equals(
						DiscoverableObjectsContainer.TYPE_PENDINGANSWERUPLOAD)) {
					objectsContainersList.remove(i);
					Log.d("#skm", "Deleting pending answer");
				}
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		/*
		 * if (userId != null || userName != null) { fetchUser();
		 * 
		 * if (isMyProfile)
		 * updateUserProfileIndex(PrefUtils.getCurrentUserAsObject()); else {
		 * fetchUser(); } }
		 */
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
	}

	private void fetchUser() {
		log("fetch user called");
		Log.i(TAG, "fetch user isempty" + isScreenEmpty + " Loading "
				+ isLoading);
		if (Controller.isNetworkConnected(this)) {
			if (!isLoading) {
				isLoading = true;
				if (userId != null)
					Controller.requestProfile(this, userId, fetchUserListener);
				else if (userName != null)
					Controller.requestProfileByName(userName, this,
							fetchUserListener);
			}
		} else {
			mPagerAdapter.setDataBooleans(false, true);
			showErrorView(Constant.PLACEHOLDER_SCREENS.NO_INTERNET);
		}

	}

	@Override
	protected void onPause() {
		super.onPause();

		if (mPagerAdapter != null)
			mPagerAdapter.stopIfAny();
		stopSwipeTimer();
	}

	private void fetchUserPost() {
		Log.i(TAG, "fetchUserPost");

		if (!isLoading && isMoreDataAvailable) {
			isLoading = true;
			Log.d("GCM", "User id being sent is: " + userId);
			Log.d("GCM", "index is: " + index);

			Controller.requestTypedAnswersByUser(this, userId,
					String.valueOf(index), userPostListener);
		}
	}

	public void updatePostOfPendingQuestion(PostObject postObject,
			String questionId) {
		int i = 0;
		for (Iterator<DiscoverableObjectsContainer> iterator = objectsContainers
				.iterator(); iterator.hasNext(); i++) {
			DiscoverableObjectsContainer object = (DiscoverableObjectsContainer) iterator
					.next();
			if (object.getType().equals(
					DiscoverableObjectsContainer.TYPE_PENDINGANSWERUPLOAD)) {
				PendingAnswer pendingAnswer = object.getPendingAnswer();
				if (pendingAnswer.getQuestionObject().getId()
						.equals(questionId)) {
					DiscoverableObjectsContainer tempObject = new DiscoverableObjectsContainer();
					tempObject.setType(DiscoverableObjectsContainer.TYPE_POST);
					tempObject.setPost(postObject);
					objectsContainers.remove(i);
					objectsContainers.add(i, tempObject);
					mPagerAdapter.notifyDataSetChanged();
					break;
				}
			}
		}
	}

	private void updatePendingAnswers(UniversalUser user) {
		if (!isFinishing()) {
			PendingAnswersContainer pendingUploadsContainer = PrefUtils
					.getPendingUploads();
			ArrayList<PendingAnswer> pendingUploadsList = new ArrayList<PendingAnswer>();
			if (pendingUploadsContainer != null) {
				pendingUploadsList = pendingUploadsContainer
						.getPendingUploads();
			}

			if (pendingUploadsList.size() > 0) {
				/**
				 * because of some bug in Answer uploading service, there are
				 * many duplicate entries on pending upload list. first remove
				 * them.
				 */
				ArrayList<PendingAnswer> tempPendingUploadList = new ArrayList<PendingAnswer>();

				for (Iterator<PendingAnswer> iterator = pendingUploadsList
						.iterator(); iterator.hasNext();) {
					PendingAnswer pendingAnswer = (PendingAnswer) iterator
							.next();
					boolean isDuplicate = false;
					for (Iterator<PendingAnswer> innerIterator = tempPendingUploadList
							.iterator(); innerIterator.hasNext();) {
						PendingAnswer innerPendingAnswer = (PendingAnswer) innerIterator
								.next();
						if (innerPendingAnswer
								.getQuestionObject()
								.getId()
								.equals(pendingAnswer.getQuestionObject()
										.getId())) {
							isDuplicate = true;
						}
					}
					if (isDuplicate == false) {
						tempPendingUploadList.add(pendingAnswer);
					}

				}

				// Update prefUtils as well
				pendingUploadsList = tempPendingUploadList;
				pendingUploadsContainer.setPendingUploads(pendingUploadsList);
				PrefUtils.savePendingUploads(pendingUploadsContainer);

				for (Iterator<PendingAnswer> iterator = pendingUploadsList
						.iterator(); iterator.hasNext();) {
					PendingAnswer pendingAnswer = (PendingAnswer) iterator
							.next();
					UniversalUser answerAuthor = pendingAnswer
							.getQuestionObject().getQuestion_to();
					Log.d("#skm1", pendingAnswer.getQuestionObject().getBody());
					if (user.getId().equals(answerAuthor.getId())) {
						DiscoverableObjectsContainer proifleContainer = new DiscoverableObjectsContainer();
						proifleContainer
								.setType(DiscoverableObjectsContainer.TYPE_PENDINGANSWERUPLOAD);
						proifleContainer.setUser(user);
						proifleContainer.setPendingAnswer(pendingAnswer);
						if (objectsContainers.size() > 0) {
							objectsContainers.add(1, proifleContainer);
						} else {
							objectsContainers.add(0, proifleContainer);
						}
						mPagerAdapter.notifyDataSetChanged();
					}
				}
			}
		}
	}

	private void updateUserProfileIndex(UniversalUser user) {

		if (!isFinishing()) {
			if (objectsContainers.size() == 0) {
				mUser = user;
				DiscoverableObjectsContainer proifleContainer = new DiscoverableObjectsContainer();
				proifleContainer
						.setType(DiscoverableObjectsContainer.TYPE_USER);
				proifleContainer.setUser(user);
				objectsContainers.add(0, proifleContainer);
				deletePendingAnswersFromList(objectsContainers);
				updatePendingAnswers(user);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (isScreenEmpty) {
							isScreenEmpty = false;
							/*
							 * Utilities.removeEmptyScreen(getWindow()
							 * .getDecorView().getRootView());
							 */
						}
						if (isPlayerInstanstiated)
							mPagerAdapter.notifyDataSetChanged();
						else {
							isPlayerInstanstiated = true;
							mPagerAdapter.notifyFirstDataChanged();
						}

					}
				});
			}
			showLoading(false);
			isLoading = false;

			fetchUserPost();
		}
	}

	RequestListener fetchUserListener = new RequestListener() {

		@Override
		public void onRequestStarted() {
		}

		@Override
		public void onRequestError(int errorCode, String message) {
			isLoading = false;
			showLoading(false);
			Log.i(TAG, "onRequestError fetch user " + message + isScreenEmpty);
			if (isScreenEmpty)
				Controller.showToast(ProfileActivity.this,
						" Please Try again after sometime");
			showErrorView(Constant.PLACEHOLDER_SCREENS.API_ERROR);

		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			log("request to fetch user completed");
			Log.i(TAG, " Fetch User onRequestCompleted   user "
					+ responseObject);
			SingleUniversalUserContainer user = (SingleUniversalUserContainer) JsonUtils
					.objectify((String) responseObject,
							SingleUniversalUserContainer.class);
			if (user != null) {

				updateUserProfileIndex(user.getUser());

				Log.d("GCM", "Username is: " + user.getUser().getUsername());
				Log.d("GCM", "User id is: " + userId);
				if (userId.equals(user.getUser().getUsername())) {
					userId = user.getUser().getId();
					Log.d("GCM", "The user id now is: " + userId);
					isMoreDataAvailable = true;
					isLoading = false;
					// Set the profile state again because it might be
					// user's own profile
					setProfileState();
					// Fetch the user post with the updated id
					fetchUserPost();
				}
			}
		}
	};

	RequestListener userPostListener = new RequestListener() {

		@Override
		public void onRequestStarted() {
			Log.i(TAG, "onRequestStarted");
		}

		@Override
		public void onRequestError(int errorCode, String message) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "userPostListener onRequestError " + message);
			}
			isLoading = false;
			if (!isFinishing() && !objectsContainers.isEmpty())
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mPagerAdapter.setDataBooleans(false, true);
						mPagerAdapter.notifyDataSetChanged();
					}
				});
			else {
				showErrorView(Constant.PLACEHOLDER_SCREENS.NO_INTERNET);
			}
		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			PaginatedPostHolder paginatedPostHolder = (PaginatedPostHolder) JsonUtils
					.objectify((String) responseObject,
							PaginatedPostHolder.class);
			index = paginatedPostHolder.getNext_index();
			isMoreDataAvailable = index > 0;
			updatePostPages(removeUnknowns(paginatedPostHolder.getStream()));
			isLoading = false;
		}

	};

	private void updatePostPages(
			final ArrayList<DiscoverableObjectsContainer> list) {
		if (!isFinishing())
			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					mPagerAdapter.setDataBooleans(isMoreDataAvailable, false);
					Log.d("CommentCount", "Give me");
					objectsContainers = MyUtilities.appendUniqueItems(
							objectsContainers, list,
							MyUtilities.MODE_DELETE_PREVIOUS);
					mPagerAdapter.setData(objectsContainers);
					mPagerAdapter.notifyDataSetChanged();
					if (PrefUtils.getIsFirstProfileSwipeAnimShown() == false
							&& mUser != null && objectsContainers.size() > 1
							&& !mUser.getId().equals(PrefUtils.getUserID()))
						resetSwipeTimer();
				}
			});
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int pos) {
		this.pos = pos;
		log("onPageSelected pos : " + pos);
		Log.d("profile1", "profile page selected: " + pos);
		if (!PrefUtils.getIsFirstProfileSwipeAnimShown() && mUser != null
				&& objectsContainers.size() > 1
				&& !mUser.getId().equals(PrefUtils.getUserID())) {
			stopSwipeTimer();
			PrefUtils.setIsFirstProfileSwipeAnimShown(true);
		}
		mPagerAdapter.playVideoAtPosition(pos);
		mCurrentPage = pos; // Used in OnBackPressed
		if (isNearEnd(pos) && !isLoading && isMoreDataAvailable) {
			fetchUserPost();
		}
		if (pos == 0 && isMyProfile) {
			shareProfileImageView.setVisibility(View.VISIBLE);
			imgNotification.setVisibility(View.VISIBLE);

		} else if (pos == 0 && !isMyProfile) {
			shareProfileImageView.setVisibility(View.VISIBLE);
			defShareImageView.setVisibility(View.VISIBLE);
		} else if (isMyProfile) {
			shareProfileImageView.setVisibility(View.GONE);
			imgNotification.setVisibility(View.GONE);
			// defShareImageView.setVisibility(View.GONE);
		} else {
			shareProfileImageView.setVisibility(View.GONE);
			defShareImageView.setVisibility(View.GONE);
		}
		Controller.registerGAEvent(MixpanelUtils.CARD_FLIPPED);
	}

	private boolean isNearEnd(int pos) {
		return objectsContainers.size() == pos + 1;
	}

	private void setProfileState() {
		if (userId != null) {
			isMyProfile = userId.equals(PrefUtils.getUserID());
			setProfile();
		}
		answerQuestionsView = findViewById(R.id.action_bar_btn_answer);
		UniversalUser mUser = PrefUtils.getCurrentUserAsObject();
		if (isMyProfile) {
			if ((mUser != null && mUser.getUser_type() != UniversalUser.TYPE_CELEB)
					|| mUser == null) {
				// answerQuestionsView.setVisibility(View.VISIBLE);
			}
		}
		answerQuestionsView.setOnClickListener(this);
	}

	private void setProfile() {
		tvNotifiactionCount = (TextView) findViewById(R.id.action_bar_my_profile_notification_count);
		ImageView imgBack = (ImageView) findViewById(R.id.action_bar_my_profile_back);
		imgBack.setOnClickListener(this);

		if (isMyProfile) {
			getNotificationCount();
			imgMenu = (ImageView) findViewById(R.id.action_bar_my_profile_menu);
			imgMenu.setOnClickListener(this);
			imgMenu.setVisibility(View.VISIBLE);

			imgNotification = (ImageView) findViewById(R.id.action_bar_my_profile_notification);
			imgNotification.setOnClickListener(this);
			imgNotification.setVisibility(View.VISIBLE);

		} else {

			imgMenu = (ImageView) findViewById(R.id.action_bar_my_profile_menu);
			imgMenu.setVisibility(View.GONE);
			imgNotification = (ImageView) findViewById(R.id.action_bar_my_profile_notification);
			imgNotification.setVisibility(View.GONE);

		}
	}

	@Override
	public void onClick(View v) {
		Log.d("FF", "Yoddling in Profile Activity");
		switch (v.getId()) {
		case R.id.action_bar_my_profile_menu:
			showSettings(v);
			break;
		case R.id.action_bar_my_profile_back:
			onBackPressed();
			break;
		case R.id.action_bar_my_profile_notification:
			notificationActivity();
			PrefUtils.setNotificationCount(0);
			tvNotifiactionCount.setVisibility(View.GONE);
			registerMixpanelEvent(MixpanelUtils.NAVIGATION_NOTIFICATIONS);
			break;
		case R.id.action_bar_btn_answer:
			openAnswerQuestionActivity();
			registerMixpanelEvent(MixpanelUtils.NAVIGATION_ANSWER_QUESTIONS);
			break;
		case R.id.iv_share_profile:
			String shareText = getShareTextForThisUser();
			ShareUtils.shareLinkUsingIntent(shareText, this,
					(String) v.getTag());
			break;
		case R.id.iv_share_profile_default:
			defaultShare();
			break;
		}

	}

	private String getShareTextForThisUser() {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		if (mUser.getId().equals(PrefUtils.getCurrentUserAsObject().getId())) {
			builder.append(
					"Hey, I am answering questions through video selfies on Frankly.me. Checkout ")
					.append(mUser.getWeb_link()).append(" - ")
					.append(mUser.getFirst_name());
		} else {
			builder.append(mUser.getFull_name())
					.append(" just joined Frankly.me. You can ask questions and get frank video replies. Checkout ")
					.append(mUser.getWeb_link());
		}
		return builder.toString();
	}

	@SuppressWarnings("deprecation")
	private void showSettings(View mainView) {
		int[] location = new int[2];
		Log.d("check", "showing settings o");
		mainView.getLocationOnScreen(location);

		View view = getLayoutInflater().inflate(R.layout.popup_menu, null);
		final PopupWindow popupWindow = new PopupWindow(this);
		popupWindow.setContentView(view);
		popupWindow.setWidth(MyUtilities.SCREEN_WIDTH / 2);
		popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);

		popupWindow.setFocusable(true);

		ImageView settingsImgView = (ImageView) view
				.findViewById(R.id.img_settings);

		settingsImgView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				settingActivity();
				registerMixpanelEvent(MixpanelUtils.NAVIGATION_SETTINGS);
				popupWindow.dismiss();
			}
		});

		ImageView shareImgView = (ImageView) view.findViewById(R.id.img_share);
		shareImgView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				defaultShare();
				popupWindow.dismiss();
			}
		});

		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		popupWindow.showAtLocation(mainView, Gravity.NO_GRAVITY, location[0]
				- popupWindow.getWidth() + mainView.getWidth() / 2,
				mainView.getHeight() - 5);
		Log.d("PU", "width : " + popupWindow.getWidth() + " location[0] : "
				+ location[0]);

		// PopupMenu popup = new PopupMenu(this, imgMenu);
		// popup.getMenuInflater().inflate(R.menu.profile_menu,
		// popup.getMenu());
		// popup.setOnMenuItemClickListener(new
		// PopupMenu.OnMenuItemClickListener() {
		// public boolean onMenuItemClick(MenuItem item) {
		// switch (item.getItemId()) {
		// case R.id.menu_profile_settings:
		// RouterActivity.settingActivity(ProfileActivity.this);
		// registerMixpanelEvent(MixpanelUtils.NAVIGATION_SETTINGS);
		//
		// return false;
		// case R.id.menu_profile_share:
		// defaultShare();
		// return false;
		// }
		// return false;
		// }
		// });
		//
		// popup.show();

	}

	private void getNotificationCount() {
		Controller.getNotificationsCount(this, new RequestListener() {

			@Override
			public void onRequestStarted() {
			}

			@Override
			public void onRequestError(int errorCode, String message) {
				displayNotificationCount(PrefUtils.getNotificationCount());
			}

			@Override
			public void onRequestCompleted(Object responseObject) {
				NotificationApiResponse notification = (NotificationApiResponse) JsonUtils.objectify(
						(String) responseObject, NotificationApiResponse.class);
				if (notification != null) {
					PrefUtils.setNotificationCount(notification.getCount());
					displayNotificationCount(notification.getCount());
				}
			}
		});
	}

	private void displayNotificationCount(final int count) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (isMyProfile) {
					if (count > 0) {
						tvNotifiactionCount.setText(String.valueOf(count));
						tvNotifiactionCount.setVisibility(View.VISIBLE);
					} else
						tvNotifiactionCount.setVisibility(View.GONE);
				}
			}
		});
	}

	@Override
	public void onPlayerPrepared() {
		mActionBar.setVisibility(View.INVISIBLE);
		if (!PrefUtils.getIsFirstProfileSwipeAnimShown())
			stopSwipeTimer();
	}

	@Override
	public void onPlayerCompleted() {
		mActionBar.setVisibility(View.VISIBLE);
		if (!PrefUtils.getIsFirstProfileSwipeAnimShown() && mUser != null
				&& objectsContainers.size() > 1
				&& !mUser.getId().equals(PrefUtils.getUserID()))
			resetSwipeTimer();
	}

	@Override
	public void onPlayerPaused() {
		mActionBar.setVisibility(View.VISIBLE);
		if (!PrefUtils.getIsFirstProfileSwipeAnimShown() && mUser != null
				&& objectsContainers.size() > 1
				&& !mUser.getId().equals(PrefUtils.getUserID()))
			resetSwipeTimer();

	}

	@Override
	public void onPlayerResumed() {
		mActionBar.setVisibility(View.INVISIBLE);
		if (!PrefUtils.getIsFirstProfileSwipeAnimShown() && mUser != null
				&& objectsContainers.size() > 1
				&& !mUser.getId().equals(PrefUtils.getUserID()))
			stopSwipeTimer();

	}

	private void showLoading(final boolean show) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (show) {
					comment_iv_loader.setVisibility(View.VISIBLE);
					loaderAnimation.start();

				} else {
					comment_iv_loader.setVisibility(View.INVISIBLE);
					loaderAnimation.stop();
				}

			}
		});
	}

	@Override
	public void onPlayerPausedToBuffer() {

		stopSwipeTimer();
	}

	@Override
	public void onPlayerResumedAfterBuffer() {

		stopSwipeTimer();
	}

	@Override
	public void onPlayerError() {
		if (!PrefUtils.getIsFirstProfileSwipeAnimShown() && mUser != null
				&& objectsContainers.size() > 1
				&& !mUser.getId().equals(PrefUtils.getUserID())) {
			resetSwipeTimer();

		}
	}

	private void showErrorView(int viewType) {
		isScreenEmpty = true;
		Utilities.showEmptyScreen(getWindow().getDecorView().getRootView(),
				errorListener, viewType);
		comment_iv_loader.setVisibility(View.INVISIBLE);
		loaderAnimation.stop();
	}

	private OnClickListener errorListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Log.d("Answer", "in api error listener");
			if (Controller.isNetworkConnectedWithMessage(ProfileActivity.this)) {
				fetchUser();
			}
		}
	};

	public void registerMixpanelEvent(String event_name) {

		MixpanelUtils.sendGenericEvent(mUser.getId(), mUser.getUsername(),
				String.valueOf(mUser.getUser_type()),
				ScreenSpecs.SCREEN_PROFILE, event_name, this);
	}

	private ArrayList<DiscoverableObjectsContainer> removeUnknowns(
			ArrayList<DiscoverableObjectsContainer> stream) {
		Iterator<DiscoverableObjectsContainer> i = stream.iterator();
		while (i.hasNext()) {
			DiscoverableObjectsContainer temp = i.next();
			if (!DiscoverableObjectsContainer.ACCEPTED_TYPE.contains(temp
					.getType()))
				i.remove();
		}
		return stream;
	}

	public void showTimeline() {
		if (mViewPager != null && objectsContainers.size() > 1)
			mViewPager.setCurrentItem(1);
	}

	@Override
	public void onBackPressed() {
		if (isMyProfile && imgNotification != null
				&& imgNotification.getVisibility() != View.VISIBLE)
			imgNotification.setVisibility(View.VISIBLE);
		if (mCurrentPage > 0)
			mViewPager.setCurrentItem(0);
		else {
			super.onBackPressed();
			overridePendingTransition(0, R.drawable.push_out_to_bottom);
		}
	}

	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		if (!isFinishing() && userId.equals(this.userId)) {
			for (DiscoverableObjectsContainer thisObjectsContainer : objectsContainers) {
				if (thisObjectsContainer.getType().equals(
						DiscoverableObjectsContainer.TYPE_USER))
					thisObjectsContainer.getUser().setIs_following(isFollowed);
				else if (thisObjectsContainer.getType().equals(
						DiscoverableObjectsContainer.TYPE_POST)
						&& thisObjectsContainer.getPost().getAnswer_author()
								.getId().equals(userId))
					thisObjectsContainer.getPost().getAnswer_author()
							.setIs_following(isFollowed);
			}

		}

	}

	@Override
	public void onCommentDone(String postId, String newCommentId,
			int newCommentCount) {
		if (!isFinishing()) {
			for (DiscoverableObjectsContainer thisObjectsContainer : objectsContainers) {
				if (thisObjectsContainer.getType().equals(
						DiscoverableObjectsContainer.TYPE_POST)
						&& thisObjectsContainer.getPost().getId()
								.equals(postId)) {
					thisObjectsContainer.getPost().setComment_count(
							newCommentCount);
				}

			}
		}

	}

	@Override
	protected void onDestroy() {
		UniversalActionListenerCollection.getInstance()
				.removeGeneralCommentListener(this);
		UniversalActionListenerCollection.getInstance()
				.removeGeneralFollowListener(this);
		UniversalActionListenerCollection.getInstance()
				.removeGeneralLikeListener(this);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				pendingAnswerProgressReceiver);
		super.onDestroy();
	}

	@Override
	public void onPostLikeChanged(String postId, boolean isLiked,
			int newLikeCount) {
		// TODO Auto-generated method stub
		if (!isFinishing())
			for (DiscoverableObjectsContainer thisObjectsContainer : objectsContainers) {
				if (thisObjectsContainer.getType().equals(
						DiscoverableObjectsContainer.TYPE_POST)
						&& thisObjectsContainer.getPost().getId()
								.equals(postId)) {
					thisObjectsContainer.getPost().setIs_liked(isLiked);
					thisObjectsContainer.getPost().setLiked_count(newLikeCount);
				}
			}

	}

	@SuppressLint("HandlerLeak")
	private Handler swipeShowHandler = new Handler() {
		public void handleMessage(Message msg) {
		}
	};

	private Runnable swipeShowCallback = new Runnable() {
		@Override
		public void run() {
			// myButton.setVisibility(View.GONE);
			if (!PrefUtils.getIsFirstProfileSwipeAnimShown()) {
				stopSwipeTimer();
				findViewById(R.id.rl_swipe_background).setVisibility(
						View.VISIBLE);
				AlphaAnimation fade_in = new AlphaAnimation(0.0f, 1.0f);
				fade_in.setDuration(100);
				fade_in.setAnimationListener(new AnimationListener() {
					public void onAnimationStart(Animation arg0) {
						Log.d("animation", "started");
						animView.setVisibility(View.VISIBLE);
						new Thread(new Runnable() {
							@Override
							public void run() {
								Log.d("swipe profile", "called");
								Log.d("animation", "run");
								swipeAnimation();
							}
						}).start();
					}

					public void onAnimationRepeat(Animation arg0) {
						Log.d("animation", "repeated");
					}

					public void onAnimationEnd(Animation arg0) {
						Log.d("animation", "end");

					}
				});
				animView.startAnimation(fade_in);

			}

		}
	};

	public void resetSwipeTimer() {
		swipeShowHandler.removeCallbacks(swipeShowCallback);
		swipeShowHandler.postDelayed(swipeShowCallback, SWIPE_TIMEOUT);
	}

	public void stopSwipeTimer() {
		swipeShowHandler.removeCallbacks(swipeShowCallback);
	}

	@Override
	public void onUserInteraction() {
		Log.d("Log ", "onUserInteraction called");
		if (PrefUtils.getIsFirstProfileSwipeAnimShown() == false
				&& mUser != null && objectsContainers.size() > 1
				&& !mUser.getId().equals(PrefUtils.getUserID()))
			resetSwipeTimer();
	}

	public void swipeAnimation() {
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				animation3 = new AlphaAnimation(1.0f, 0.0f);
				animation3.setDuration(1000);
				// animation3.setStartOffset(1000);
				animation3.setRepeatCount(Animation.INFINITE);
				ivSwipe3.startAnimation(animation3);
			}
		});
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				animation2 = new AlphaAnimation(1.0f, 0.0f);
				animation2.setDuration(1000);
				// animation2.setStartOffset(500);
				animation2.setRepeatCount(Animation.INFINITE);
				ivSwipe2.startAnimation(animation2);
			}
		});
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				animation1 = new AlphaAnimation(1.0f, 0.0f);
				animation1.setDuration(1000);
				// animation1.setStartOffset(1000);
				animation1.setRepeatCount(Animation.INFINITE);
				ivSwipe1.startAnimation(animation1);
			}
		});

	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (v.getId()) {

		case R.id.rl_swipe_animation:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				swipePosY = event.getY();
				return true;
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {

				float currSwipePosY = event.getY();
				if (currSwipePosY - swipePosY <= -20) {
					v.setVisibility(View.GONE);
					PrefUtils.setIsFirstProfileSwipeAnimShown(true);
					findViewById(R.id.rl_swipe_background).setVisibility(
							View.GONE);

					for (SwipeAnimationDoneListener l : swipeAnimationDoneListeners)
						l.onSwipeDone();

					mViewPager.setCurrentItem(pos + 1);

					return true;
				} else if (currSwipePosY - swipePosY >= 20) {
					PrefUtils.setIsFirstProfileSwipeAnimShown(true);
					v.setVisibility(View.GONE);
					findViewById(R.id.rl_swipe_background).setVisibility(
							View.GONE);
					for (SwipeAnimationDoneListener l : swipeAnimationDoneListeners)
						l.onSwipeDone();
					return true;
				}
			}
		case R.id.action_bar_btn_profile:

			break;
		}
		return false;

	}

	private void defaultShare() {
		String shareTextX = getShareTextForThisUser();
		Intent defShareIntent = new Intent(Intent.ACTION_SEND);
		defShareIntent.setType("text/plain");
		defShareIntent.putExtra(Intent.EXTRA_TEXT, shareTextX);
		startActivity(defShareIntent);
	}

	@Override
	public void onProgressUpdate(Context ctx, Intent data) {
		if (AppConfig.DEBUG) {
			Log.i(TAG,
					"onProgressUpdate() "
							+ (data == null ? "null" : data.getExtras()));
		}
		if (!data.hasExtra(PendingAnswerProgressReceiver.KEY_EXTRA_QUESTION_ID)) {
			return;// Error. No question id with progress update.
		}

		String questionId = data
				.getStringExtra(PendingAnswerProgressReceiver.KEY_EXTRA_QUESTION_ID);

		if (data.hasExtra(PendingAnswerProgressReceiver.KEY_EXTRA_POST_OBJECT)) {
			PostObject postObject = (PostObject) JsonUtils
					.objectify(
							data.getStringExtra(PendingAnswerProgressReceiver.KEY_EXTRA_POST_OBJECT),
							PostObject.class);
			updatePostOfPendingQuestion(postObject, questionId);
			return;
		}

		// send event to pending answer fragment with same question id
		PendingAnswersProgressListener listener = (PendingAnswersProgressListener) pendingAnsProgressListeners
				.get(questionId);
		if (listener != null) {
			listener.onProgressUpdate(ctx, data);
		}
	}

	public boolean addPendingAnsProgressListener(String questionId,
			PendingAnswersProgressListener listener) {
		if (questionId == null || listener == null) {
			return false;
		}
		pendingAnsProgressListeners.put(questionId, listener);
		return true;
	}

	public void delPendingAnsProgressListener(String questionId) {
		if (pendingAnsProgressListeners.containsKey(questionId)) {
			pendingAnsProgressListeners.remove(questionId);
		}
	}

	public void addSwipeAnimationDoneListener(
			SwipeAnimationDoneListener listener) {
		swipeAnimationDoneListeners.add(listener);
	}

	public void removeSwipeAnimationDoneListner(
			SwipeAnimationDoneListener listener) {
		swipeAnimationDoneListeners.remove(listener);
	}

	/**
	 * this method removes deleted answer from user's answer list locally and
	 * redraws the adapter
	 * 
	 * @param id
	 */
	public void deleteAnswerFromAdapter(String id) {
		for (int i = 0; i < objectsContainers.size(); i++) {
			if (objectsContainers.get(i).getId().split("#")[1].equals(id)) {
				objectsContainers.remove(i);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mPagerAdapter.notifyDataSetChanged();
						Toast.makeText(ProfileActivity.this, "Answer Deleted",
								Toast.LENGTH_SHORT).show();
					}
				});
				return;
			}
		}
	}

	private void log(String message) {
		if (AppConfig.DEBUG) {
			Log.d("ProfileActivity", message);
		}
	}
}
