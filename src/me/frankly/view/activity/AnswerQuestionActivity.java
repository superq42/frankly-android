package me.frankly.view.activity;

import java.util.ArrayList;
import java.util.Iterator;

import me.frankly.R;
import me.frankly.adapter.AnswerQuestionAdapter;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.Constant.Key;
import me.frankly.config.UrlResolver.EndPoints;
import me.frankly.listener.RequestListener;
import me.frankly.model.AnswerQuestionDataHolder;
import me.frankly.model.newmodel.AnswerQuestionItemList;
import me.frankly.model.newmodel.PendingAnswer;
import me.frankly.model.newmodel.PendingAnswersContainer;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.ImageUtil;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.util.ShareUtils;
import me.frankly.util.Utilities;
import me.frankly.view.ShareDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class AnswerQuestionActivity extends BaseActivity implements
		OnScrollListener, RequestListener, OnItemClickListener, OnClickListener {

	public ArrayList<AnswerQuestionDataHolder> items = new ArrayList<AnswerQuestionDataHolder>();
	private AnswerQuestionAdapter adapter;
	private View mRootView, emptyContainerView;
	private boolean makeRequest = true;
	private boolean isRequested = false;
	private boolean apiFail = false;
	private String since = "0";
	public static final int REQUEST_ANSWER_QUESTION = 900;
	private AnimationDrawable loaderAnimation;
	private ImageView iv_loader, imgView;
	private ListView listview;
	private UniversalUser user = null;
	private boolean isShareShown = false;
	private PendingAnswersContainer pendingAnswers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mRootView = LayoutInflater.from(this).inflate(
				R.layout.activity_answer_question, null);

		setContentView(mRootView);

		Controller.registerGAEvent("AnswerQuestionActivity");
		if (Controller.isNetworkConnected(this))
			Controller.removeAllPagesInCache(EndPoints.GET_QUESTIONS);
		user = PrefUtils.getCurrentUserAsObject();
		pendingAnswers = PrefUtils.getPendingUploads();

		adapter = new AnswerQuestionAdapter(items, this);
		View view = findViewById(R.id.answer_question_action_bar);
		view.bringToFront();

		imgView = (ImageView) findViewById(R.id.act_ans_bg);

		// Initializing questions list object to fetch data
		listview = (ListView) findViewById(R.id.asked_question_list);
		emptyContainerView = mRootView.findViewById(R.id.rl_empty_screen);
		// Loader Animation
		iv_loader = new ImageView(this);
		iv_loader.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) iv_loader.getDrawable();
		listview.addFooterView(iv_loader);
		iv_loader.setVisibility(View.VISIBLE);
		loaderAnimation.start();

		// We need to set Adapter after adding footer view
		listview.setAdapter(adapter);
		
		if(getIntent().hasExtra(Constant.Key.KEY_ANSWER_QUESTION))
		{
			String questionString = getIntent().getStringExtra(Constant.Key.KEY_ANSWER_QUESTION);
			QuestionObject deeplinkQuestion = (QuestionObject) JsonUtils.objectify(questionString, QuestionObject.class);
			AnswerQuestionDataHolder deeplinkQuestionHolder = new AnswerQuestionDataHolder();
			deeplinkQuestionHolder.setUser(deeplinkQuestion);
			items.add(0, deeplinkQuestionHolder);
			adapter.notifyDataSetChanged();
		}

		TextView title_text = (TextView) findViewById(R.id.action_bar_title_text);
		title_text.setText("Answer Questions");
		ImageView btn_back = (ImageView) findViewById(R.id.action_bar_back_btn);
		btn_back.setImageResource(R.drawable.btn_back_header);

		// Get New data
		getNextPage();

		// Setting scroll listener for the list view
		listview.setOnScrollListener(this);

		// Setting click listener for question item
		// listview.setOnItemClickListener(this);

		// Setting click listener for back button
		btn_back.setOnClickListener(this);

		displayBackground();

	}

	private void displayBackground() {
		if (user != null && user.getCover_picture() != null) {
			ImageLoader.getInstance().loadImage(user.getCover_picture(),
					new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String arg0, View arg1) {
							//  nothing to do here
						}

						@Override
						public void onLoadingFailed(String arg0, View arg1,
								FailReason arg2) {
							// nothing to do here
						}

						@Override
						public void onLoadingComplete(String arg0, View arg1,
								Bitmap bitmap) {
							bitmap = MyUtilities.fastblur(bitmap, 10);
							imgView.setImageBitmap(bitmap);
						}

						@Override
						public void onLoadingCancelled(String arg0, View arg1) {
						}
					});
		}

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

		if (totalItemCount != 0
				&& totalItemCount - 2 - firstVisibleItem <= visibleItemCount) {
			if (Controller.isNetworkConnected(this))
				getNextPage();
		}

	}

	private void getNextPage() {
		if (makeRequest && !isRequested) {

			Controller.getAskedQuestions(this, this.since,
					Controller.DEFAULT_PAGE_SIZE, this);
			isRequested = true;
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	public void onRequestStarted() {

		runOnUiThread(new Runnable() {

			@Override
			public void run() {

				iv_loader.setVisibility(View.VISIBLE);
				loaderAnimation.start();
			}
		});
	}

	@Override
	public void onRequestCompleted(final Object responseObject) {

		// Running parsing of JSON in UI thread
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {

				AnswerQuestionItemList new_items = (AnswerQuestionItemList) JsonUtils
						.objectify((String) responseObject,
								AnswerQuestionItemList.class);

				new_items.removeNulls();
				if (pendingAnswers != null
						&& !pendingAnswers.getPendingUploads().isEmpty())
					removePendings(new_items);

				// Setting up makeRequest variable to figure out if we need
				// another call to the server
				if (new_items.getNext().equals("-1"))
					makeRequest = false;

				Log.d("Answer", responseObject.toString());
				Log.d("Answer", "Next: " + new_items.getNext() + " Count: "
						+ new_items.getCount() + " makeRequest: " + makeRequest);

				if (new_items.getCount() == 0 && !makeRequest && !isShareShown
						&& items.size() == 0) {
					isShareShown = true;
					Log.d("AQF", "opeing");
					String text = ShareUtils.getTextToShare(user);
					shareActivity(null, text, null, null, user.getUser_type());
					finish();
				} else
					Utilities.removeEmptyScreen(emptyContainerView, mRootView);

				since = new_items.getNext();
				isRequested = false;
				displayData(new_items);
				loaderAnimation.stop();
				iv_loader.setVisibility(View.INVISIBLE);
				listview.removeFooterView(iv_loader);

			}
		});

	}

	protected void removePendings(AnswerQuestionItemList new_items) {
		Iterator<AnswerQuestionDataHolder> i = new_items.getItem().iterator();
		while (i.hasNext()) {
			AnswerQuestionDataHolder temp = i.next();
			for (PendingAnswer thisPending : pendingAnswers.getPendingUploads()) {
				if (temp.getQuestion().getId()
						.equals(thisPending.getQuestionObject().getId())) {
					try {
						i.remove();
					} catch (IllegalStateException exception) {
						Log.e("AnswerQuestionActivity:",
								"removePendings IllegalStateException"
										+ exception.getStackTrace().toString());
					}
					continue;
				}
			}
		}
	}

	@Override
	public void onRequestError(int errorCode, String message) {

		loaderAnimation.stop();
		iv_loader.setVisibility(View.INVISIBLE);
		makeRequest = true;
		isRequested = false;
		if (apiFail)
			Controller.showToast(this, "Please try again after some time.");
		else
			this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (!Controller
							.isNetworkConnected(AnswerQuestionActivity.this)&&items.isEmpty()) {
						Utilities.showEmptyScreen(mRootView, apiErrorListener,
								Constant.PLACEHOLDER_SCREENS.NO_INTERNET);
						loaderAnimation.stop();
						iv_loader.setVisibility(View.INVISIBLE);

					} else if(items.isEmpty()){
						Utilities.showEmptyScreen(mRootView, apiErrorListener,
								Constant.PLACEHOLDER_SCREENS.API_ERROR);
					}

				}
			});

	}

	private OnClickListener inviteFriendsListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Log.d("Answers", "Here in onclick");
			showInviteFriendsDialog();
		}
	};

	private OnClickListener apiErrorListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Log.d("Answer", "in api error listener");
			if (Controller.isNetworkConnected(AnswerQuestionActivity.this)) {
				apiFail = true;
				getNextPage();
			} else
				Controller.showToast(AnswerQuestionActivity.this,
						"There seems to be a problem "
								+ "with your internet connection");
		}
	};

	protected void showInviteFriendsDialog() {
		Log.d("AQA", "called show Invite Friends Dialog");
		ShareDialog cdd = new ShareDialog(this,
				R.layout.share_dialog_post_layout, null, 1, false, "post", null);
		cdd.show();

	}

	private void displayData(AnswerQuestionItemList new_items) {
		// Need to update the adapter item list because items references is
		// updated but not the
		// reference for qList in Adapter
		items = MyUtilities.appendUniqueItems(items, new_items.getItem(),
				MyUtilities.MODE_UPDATE_PREVIOUS);
		adapter.setListData(items);
		
		String questionId = getIntent().getStringExtra(Key.KEY_SHORT_QUESTION);
		if (questionId != null) {
			for (int i = 0; i < items.size(); i++) {
				AnswerQuestionDataHolder aqdh = items.get(i);
				if (questionId.equals(aqdh.getQuestion().getId())) {
					listview.setSelection(i);
					break;
				}
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		adapter.hideToolTip();
		answerQuestionAtPos(position);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_bar_back_btn:
			onBackPressed();
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.e("onActivityResult", "reqCode " + requestCode + " result:"
				+ resultCode + " thread" + Thread.currentThread().getName());

		if (requestCode == REQUEST_ANSWER_QUESTION) {
			if (resultCode == RESULT_OK) {
				if (data != null) {

					final String questionId = data
							.getStringExtra("question_obj");
					Log.e("onActivityResult", "question_id___" + questionId);

					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							boolean removeItemById = adapter
									.removeItemById(questionId);
							if (adapter.getCount() == 0)
								Utilities
										.showEmptyScreen(
												mRootView,
												inviteFriendsListener,
												Constant.PLACEHOLDER_SCREENS.EMPTY_QUESTION_LIST);
							Log.e("onActivityResult", "removed__"
									+ removeItemById);

						}
					});

				}

			}
		}
	}

	public void answerQuestionAtPos(int i) {

		Intent intent = new Intent(AnswerQuestionActivity.this,
				RecordAnswerActivity.class);
		intent.putExtra("question_asked",
				JsonUtils.jsonify(adapter.getItemByPosition(i)));
		startActivityForResult(intent, REQUEST_ANSWER_QUESTION);
	}

	private String getShareText() {
		return (new StringBuilder()
				.append("Hey, I am answering questions through video selfies on Frankly.me. Checkout ")
				.append(user.getWeb_link())
				.append(" just a notification away - ").append(user
				.getFirst_name())).toString();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.drawable.push_to_right);

	}

}
