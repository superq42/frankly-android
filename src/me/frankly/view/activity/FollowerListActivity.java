package me.frankly.view.activity;

import me.frankly.R;

import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class FollowerListActivity extends BaseActivity {
	ListView followList;
	FollowListAdapter followListAdapter;
	JSONObject followerlist;

	@Override
	protected void onCreate(Bundle args) {
		super.onCreate(args);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.followerlist);
		if (args != null) {

			followList = (ListView) findViewById(R.id.followView);
			followListAdapter = new FollowListAdapter();
			followList.setAdapter(followListAdapter);
			RequestQueue queue = Volley.newRequestQueue(this);
			String url = "https://www.googleapis.com/customsearch/v1?";
			JsonObjectRequest jsObjRequest = new JsonObjectRequest(
					Request.Method.GET, url, null,
					new Response.Listener<JSONObject>() {

						@Override
						public void onResponse(JSONObject response) {
							followerlist = response;

						}

					}, null);
			queue.add(jsObjRequest);

		}
	}

	public class FollowListAdapter extends BaseAdapter {

		FollowListAdapter() {

		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) FollowerListActivity.this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.followlistitem, parent);
			}

			return convertView;
		}

		@Override
		public int getCount() {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

	}

}
