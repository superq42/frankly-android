package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

public class RecordFeedBack extends Fragment implements OnClickListener,
		RequestListener {

	private EditText contentEditText;
	private ImageView postFeedbackImgView;
	private String content;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.layout_feed_back_popup_second, container, false);
		contentEditText = (EditText) rootView.findViewById(R.id.user_feedback);
		contentEditText.setHintTextColor(Color.parseColor("#50ffffff"));
		postFeedbackImgView = (ImageView) rootView
				.findViewById(R.id.post_feedback);
		postFeedbackImgView.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();

		switch (id) {
		case R.id.post_feedback:
			content = contentEditText.getText().toString();
			Controller.sendFeedBackText(getActivity(), content, this);
			getActivity().finish();
			break;
		}
	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {

	}

	@Override
	public void onRequestError(int errorCode, String message) {
		// TODO Auto-generated method stub

	}

}
