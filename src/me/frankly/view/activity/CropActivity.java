package me.frankly.view.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import me.frankly.R;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class CropActivity extends FragmentActivity implements OnClickListener {

	public static final String FAILURE_REASON = "failure_reason";
	public static final String IMAGE_PATH = "image_path";
	private static final int REASON_UNKNOWN = 255;
	public static final int REASON_USER_CANCELLED_IT = 257;
	public static final int REASON_CROPPED_BITMAP_NULL = 256;
	public static final int X_COVER = 720;
	public static final int Y_COVER = 450;
	public static final int X_PROFILE = 720;
	public static final int Y_PROFILE = 720;
	public static final int X_GALLERY = 720;
	public static final int Y_GALLERY = 720;
	public static final String FILE_PATH = "file_path";
	public static final String REQUEST_CODE = "request_code";
	private View btn_crop;
	private View btn_cancel;
	private ImageView cropview;
	private String orientation;
	private int request_code;
	private Bitmap preparedBitmap;
	public static final String TAG = "debug_CROP_IMAGE";
	private RelativeLayout rl_container;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*
		 * Tracker t = MyApplication.getDefTracker();
		 * t.setScreenName("CropActivity"); t.send(new
		 * HitBuilders.AppViewBuilder().build());
		 */
		setContentView(R.layout.activity_crop);
		rl_container = (RelativeLayout) findViewById(R.id.container);
		btn_crop = findViewById(R.id.btn_crop_crop);
		btn_cancel = findViewById(R.id.btn_crop_cancel);
		btn_crop.setOnClickListener(this);
		btn_cancel.setOnClickListener(this);
		cropview = (ImageView) findViewById(R.id.cropimageview);
		request_code = getIntent().getIntExtra(REQUEST_CODE, 0);
		String file_path = getIntent().getStringExtra(FILE_PATH);
		Log.d(TAG, "onCreate() ");
		Log.d(TAG, "intent data requestcode :" + request_code + " filepath "
				+ file_path);
		if (file_path != null) {
			preparedBitmap = getPreparedBitmapForCropView(file_path);
			Log.d(TAG, "onCreate() preparedBitmap:" + preparedBitmap);
			if (preparedBitmap != null) {
				cropview.setImageBitmap(preparedBitmap);
				/*
				 * if (request_code ==
				 * EditProfileActivity.REQUEST_UPDATE_PROFILE_PIC) {
				 * cropview.setFixedAspectRatio(true);
				 * cropview.setAspectRatio(X_PROFILE, Y_PROFILE); } else if
				 * (request_code ==
				 * EditProfileActivity.REQUEST_UPDATE_COVER_PIC) {
				 * cropview.setFixedAspectRatio(true);
				 * cropview.setAspectRatio(X_COVER, Y_COVER); } else {
				 * cropview.setFixedAspectRatio(true);
				 * cropview.setAspectRatio(X_GALLERY, Y_GALLERY); }
				 */
				// decodeFile.recycle();

			} else {
				Log.e(TAG, "prepared bitmap returned null ");
				finishWithResult(RESULT_CANCELED, REASON_UNKNOWN, null);
			}
		} else {
			finishWithResult(RESULT_CANCELED, REASON_UNKNOWN, null);
		}

	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (preparedBitmap != null && !preparedBitmap.isRecycled())
			preparedBitmap.recycle();
		preparedBitmap = null;
		unbindDrawables(rl_container);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		/*
		 * case R.id.btn_crop_crop:
		 * 
		 * // RectF actualCropRect = cropview.getActualCropRect(); // Log.i(TAG
		 * + "_Rect", actualCropRect.left + " " + // actualCropRect.top + " " +
		 * actualCropRect.right + " " + // actualCropRect.bottom); try { Bitmap
		 * croppedImage = cropview.getCroppedImage(); if (croppedImage != null)
		 * { String finalImage_path = saveCroppedImageonDisk(croppedImage); if
		 * (finalImage_path == null) { finishWithResult(RESULT_CANCELED,
		 * REASON_UNKNOWN, null); } else { finishWithResult(RESULT_OK, 0,
		 * finalImage_path); }
		 * 
		 * } else {
		 * 
		 * Log.e(TAG, "cropped Bitmap found null");
		 * finishWithResult(RESULT_CANCELED, REASON_CROPPED_BITMAP_NULL, null);
		 * 
		 * } } catch (IllegalArgumentException e) { e.printStackTrace();
		 * Toast.makeText(this,
		 * "Not a valid Crop,  Please try Again with Larger Values ",
		 * Toast.LENGTH_SHORT).show(); } break;
		 */
		/*
		 * case R.id.btn_crop_cancel: finishWithResult(RESULT_CANCELED,
		 * REASON_USER_CANCELLED_IT, null); break;
		 */

		default:
			break;
		}
	}

	private String saveCroppedImageonDisk(Bitmap croppedImage) {

		File root = new File(Environment.getExternalStorageDirectory()
				+ File.separator + "MyDir" + File.separator);
		root.mkdirs();
		String fname = "cropped_" + System.currentTimeMillis() + ".jpg";
		Bitmap finalBmp = getLowResolutionFromBitmap(croppedImage);

		if (finalBmp != null) {
			File fileAfterCropping = new File(root, fname);

			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			finalBmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			try {
				FileOutputStream fo = new FileOutputStream(fileAfterCropping);
				fo.write(bytes.toByteArray());
				fo.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				croppedImage.recycle();
				finalBmp.recycle();

			}
			sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
					Uri.fromFile(fileAfterCropping)));
			String absolutePath = fileAfterCropping.getAbsolutePath();
			Log.d(TAG, "returning path " + absolutePath);
			return absolutePath;
		} else {
			return null;
		}

	}

	private Bitmap getLowResolutionFromBitmap(Bitmap croppedImage) {
		Bitmap finalBmp = null;
		try {
			int[] requiredDimentions = getRequiredDimentions(request_code);
			int reqWidth = requiredDimentions[1];
			int reqHeight = requiredDimentions[0];
			Log.i(TAG, "reqwid:" + reqWidth);
			Log.i(TAG, "reqheight:" + reqHeight);
			int insampleSize = 1;
			int original_height = croppedImage.getHeight();
			int original_width = croppedImage.getWidth();
			if (original_height > reqHeight || original_width > reqWidth) {
				insampleSize = calculateInSampleSize(reqWidth, reqHeight,
						original_height, original_width);
			}
			int newheight = original_height / insampleSize;
			int newWidth = original_width / insampleSize;
			Log.i(TAG, "size old " + original_height + "x" + original_width
					+ " new " + newheight + "x" + newWidth);
			finalBmp = Bitmap.createScaledBitmap(croppedImage, newWidth,
					newheight, false);
			Log.i(TAG, "finalbmp wid:" + finalBmp.getWidth() + "height: "
					+ finalBmp.getHeight());

		} catch (Exception e) {
			Log.i("exception", "exception in decode bitmap");
			e.printStackTrace();
		}
		return finalBmp;
	}

	private static int calculateInSampleSize(int reqWidth, int reqHeight,
			int height, int width) {
		// Raw height and width of image
		int inSampleSize;
		Log.i(TAG, "inside calcinsamplesize");

		if (width < height) {
			inSampleSize = Math.round((float) height / (float) reqHeight);
		} else {
			inSampleSize = Math.round((float) width / (float) reqWidth);
		}

		return inSampleSize;
	}

	private void finishWithResult(int resultCode, int reason_code,
			String file_path) {
		if (resultCode == RESULT_OK) {
			Bundle conData = new Bundle();
			conData.putString(IMAGE_PATH, file_path);
			Intent intent = new Intent();
			intent.putExtras(conData);
			setResult(RESULT_OK, intent);
			finish();
		} else {
			Intent intent = new Intent();
			Bundle conData = new Bundle();
			conData.putInt(FAILURE_REASON, reason_code);
			setResult(RESULT_CANCELED, intent);
			finish();
		}
	}

	public Bitmap getPreparedBitmapForCropView(String filePath) {
		// Log.d(TAG, "getPreparedBitmapForCropView() ");
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);
		int[] properDimentions = getProperDimentions(request_code,
				options.outHeight, options.outWidth);
		int reqHeight = properDimentions[0];
		int reqWidth = properDimentions[1];
		Log.d(TAG, "getPreparedBitmapForCropView() 1 req hxw " + reqHeight
				+ "x" + reqWidth);
		Bitmap createScaledBitmap = lessResolution(filePath, reqWidth,
				reqHeight);
		Log.d(TAG,
				"getPreparedBitmapForCropView() 1 bitmap dimensions= "
						+ createScaledBitmap.getHeight() + "x"
						+ createScaledBitmap.getWidth());
		if (createScaledBitmap != null) {

			ExifInterface exif = null;
			try {
				exif = new ExifInterface(filePath);
			} catch (IOException e) {
				e.printStackTrace();
			}
			reqHeight = createScaledBitmap.getHeight();
			reqWidth = createScaledBitmap.getWidth();
			Log.d(TAG, "getPreparedBitmapForCropView() 2");

			orientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
			if (orientation != null && !orientation.equals("0")) {
				int imageExifOrientation = Integer.valueOf(orientation);
				int rotationAmount = 0;
				if (imageExifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
					// Need to do some rotating here...
					rotationAmount = 270;
				}
				if (imageExifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
					// Need to do some rotating here...
					rotationAmount = 90;
				}
				if (imageExifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
					// Need to do some rotating here...
					rotationAmount = 180;
				}
				Log.d(TAG, "getPreparedBitmapForCropView() 3");
				Matrix rotateMatrix = new Matrix();
				rotateMatrix.setRotate(rotationAmount);
				Bitmap bmp = Bitmap.createBitmap(createScaledBitmap, 0, 0,
						reqWidth, reqHeight, rotateMatrix, false);
				if (createScaledBitmap != bmp)
					createScaledBitmap.recycle();
				Log.d(TAG + " final dimention orientation", bmp.getHeight()
						+ " " + bmp.getWidth());
				return bmp;
			}
			Log.d(TAG, "getPreparedBitmapForCropView() 4");
			Log.d(TAG + " final dimention", createScaledBitmap.getHeight()
					+ " " + createScaledBitmap.getWidth());
			return createScaledBitmap;
		}
		return null;

	}

	private int[] getProperDimentions(int request_code, int height, int width) {
		int[] point = new int[] { 0, 0 };
		float min_h = 0;
		float min_w = 0;
		float ratio = 1;
		/*
		 * switch (request_code) { case
		 * EditProfileActivity.REQUEST_UPDATE_COVER_PIC: // original min_w =
		 * X_COVER * 1.5f; min_h = Y_COVER * 1.5f; ratio = 0;
		 * 
		 * if (height > width) { ratio = (float) height / (float) width;
		 * Log.d(TAG + "_ratio", height + "x" + width + " ratio" + ratio + "");
		 * point[0] = (int) (ratio * min_w); point[1] = Math.round(min_w);
		 * return point;
		 * 
		 * } else { ratio = (float) width / (float) height; Log.d(TAG +
		 * "_ratio", height + "x" + width + " ratio" + ratio + ""); point[0] =
		 * (int) min_h; point[1] = Math.round(min_h * ratio);
		 * 
		 * return point; } case EditProfileActivity.REQUEST_UPDATE_PROFILE_PIC:
		 * // original min_w = X_PROFILE * 1.5f; min_h = Y_PROFILE * 1.5f; ratio
		 * = 0;
		 * 
		 * if (height > width) { ratio = (float) height / (float) width;
		 * Log.d(TAG + "_ratio", height + "x" + width + " ratio" + ratio + "");
		 * point[0] = (int) (ratio * min_w); point[1] = Math.round(min_w);
		 * return point;
		 * 
		 * } else { ratio = (float) width / (float) height; Log.d(TAG +
		 * "_ratio", height + "x" + width + " ratio" + ratio + ""); point[0] =
		 * (int) min_h; point[1] = Math.round(min_h * ratio);
		 * 
		 * return point; }
		 * 
		 * case EditProfileActivity.REQUEST_CODE_PICK_IMAGE: min_w = X_GALLERY;
		 * min_h = Y_GALLERY; ratio = 0;
		 * 
		 * if (height > width) { ratio = (float) height / (float) width;
		 * Log.d(TAG + "_ratio", height + "x" + width + " ratio" + ratio + "");
		 * point[0] = (int) (ratio * min_w); point[1] = Math.round(min_w);
		 * return point;
		 * 
		 * } else { ratio = (float) width / (float) height; Log.d(TAG +
		 * "_ratio", height + "x" + width + " ratio" + ratio + ""); point[0] =
		 * (int) min_h; point[1] = Math.round(min_h * ratio);
		 * 
		 * return point; }
		 * 
		 * default: break; }
		 */
		return new int[] { 1000, 1000 };
	}

	public int[] getRequiredDimentions(int request_code) {
		int[] point = new int[] { 0, 0 };
		/*
		 * switch (request_code) { case
		 * EditProfileActivity.REQUEST_CODE_PICK_IMAGE: return new int[] {
		 * Y_GALLERY, X_GALLERY }; case
		 * EditProfileActivity.REQUEST_UPDATE_COVER_PIC: return new int[] {
		 * Y_COVER, X_COVER }; case
		 * EditProfileActivity.REQUEST_UPDATE_PROFILE_PIC: return new int[] {
		 * Y_PROFILE, X_PROFILE };
		 * 
		 * default: break; }
		 */
		return point;

	}

	@Override
	protected void onStart() {
		super.onStart();
		// PrefUtils.setApplicationForegroundStatus(true);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// PrefUtils.setApplicationForegroundStatus(false);

	}

	private void unbindDrawables(View view) {
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}
		if (view instanceof ViewGroup) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				unbindDrawables(((ViewGroup) view).getChildAt(i));
			}
			((ViewGroup) view).removeAllViews();
		}
	}

	public static Bitmap lessResolution(String filePath, int width, int height) {
		int reqHeight = width;
		int reqWidth = height;
		BitmapFactory.Options options = new BitmapFactory.Options();

		// First decode with inJustDecodeBounds=true to check dimensions
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		return BitmapFactory.decodeFile(filePath, options);
	}

	private static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {

		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}

}
