package me.frankly.view.activity;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.SDKPlayer.IPlayer.IPlayerStateChangedListener;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.VideoplayInteruptionBroadcastReciever;
import me.frankly.listener.SwipeAnimationDoneListener;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.util.ShareUtils;
import me.frankly.util.Utilities;
import me.frankly.view.BariolTextView;
import me.frankly.view.fragment.ChannelFragment;
import me.frankly.view.fragment.ChannelListFragment;
import me.frankly.view.fragment.SoftUpdateFragment;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

public class FranklyHomeActivity extends BaseActivity implements
		OnClickListener, IPlayerStateChangedListener, OnTouchListener {

	private static final String TAG = FranklyHomeActivity.class.getSimpleName();

	private static final int DISPLAY_MODE_CHANNEL = 1;
	private static final int DISPLAY_MODE_REMOTE_CONTROL = 2;
	public static  String DEF_CHANNEL_ID = Constant.CHANNEL.FEED;
	private static final String NULL_CHANNEL_ID = null;

	private int mDisplayMode;
	private String mCurChannelId = NULL_CHANNEL_ID;
	private View channelsView, profileView, answerView;
	private ChannelListFragment remoteControlFragment;
	private ChannelFragment curChannelFragment;

	// walkthrough variables

	private BariolTextView tvSwipe1, tvSwipe2;
	private Animation animation1, animation2, animation3;
	private ImageView ivSwipe1, ivSwipe2, ivSwipe3;
	private ArrayList<SwipeAnimationDoneListener> swipeAnimationDoneListeners;
	public static final long SWIPE_TIMEOUT = 30000;
	float swipePosY;
	public boolean wellDoneSwipe;
	private View animView;

	// walkthrough variables-end

	// public static ArrayList<SharePackage> packageList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_home);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		ShareUtils.CreateHashMap();
		registerVideoPauseReciever();
		MyUtilities.calculateScreenSize(this);
		/* packageList = ShareUtils.getSharingPackages(this, "post"); */
		channelsView = findViewById(R.id.iv_channels);
		profileView = findViewById(R.id.action_bar_btn_profile);
		answerView = findViewById(R.id.action_bar_btn_answer);
		channelsView.setOnClickListener(this);
		profileView.setOnClickListener(this);
		answerView.setOnClickListener(this);
		mDisplayMode = DISPLAY_MODE_CHANNEL;
		showDiscoverIfFirstTimeUser();
		showChannel(DEF_CHANNEL_ID);
		if (PrefUtils.getSoftUpdate()) {
			SoftUpdateFragment frag = new SoftUpdateFragment();
			frag.show(getSupportFragmentManager(), "tag");

			PrefUtils.setSoftUpdate(false);
		}
		initialiseSwipeAnim();
		handleDeeplink();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		handleDeeplink();
	}

	private void handleDeeplink() {
		Uri uri = getIntent().getData();
		Log.d("gcm", "inHomeactivity");
		if (uri != null) {
			Log.d("gcm", "uri not null : " + uri);
			handleDeepLink(uri, true);
		} else {
			Log.d("gcm", "URI null");
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.iv_channels:
			if(AppConfig.DEBUG) {
				Log.d("SA", "toggle remote");
			}
			toggleRemoteControl();
			break;
		case R.id.action_bar_btn_profile:

			if (Controller.isNetworkConnectedWithMessage(this))
				openMyProfile();
			break;
		case R.id.action_bar_btn_answer:
				openAnswerQuestionActivity();
				overridePendingTransition(R.drawable.pull_from_right,
						R.drawable.hold);
			break;
		case R.id.iv_search:
			if (Controller.isNetworkConnectedWithMessage(this))
				searchActivity();
			break;
		}

	}

	private void toggleRemoteControl() {
		if (mDisplayMode == DISPLAY_MODE_CHANNEL)
			showRemoteControl();
		else
			hideRemoteControl();
	}

	@Override
	protected void onPause() {
		if (curChannelFragment != null)
			curChannelFragment.onActivityPaused();
		super.onPause();
		stopSwipeTimer();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (PrefUtils.getIsFirstSwipeAnimShown() == false)
			resetSwipeTimer();
	}

	@Override
	protected void onStop() {
		super.onStop();
		stopSwipeTimer();
	}

	private void openMyProfile() {
		String userId = PrefUtils.getUserID();
		profileActivity(userId, null);

	}

	private void hideRemoteControl() {
//		if (!PrefUtils.getIsFirstSwipeAnimShown())
//			resetSwipeTimer();
		Log.d(TAG, "hiding remote control");
		if (remoteControlFragment != null
				&& mDisplayMode == DISPLAY_MODE_REMOTE_CONTROL) {
			mDisplayMode = DISPLAY_MODE_CHANNEL;
			MyUtilities.hideKeyboard(getWindow().getCurrentFocus(), this);
			getSupportFragmentManager().beginTransaction()
					.setCustomAnimations(0, R.drawable.push_to_top)
					.hide(remoteControlFragment).commit();
			if (PrefUtils.getIsFirstSwipeAnimShown() == false)
				resetSwipeTimer();
		}
	}

	public void showRemoteControl() {
		Log.d("SA", "showing remote control");
		stopSwipeTimer();
		if (curChannelFragment != null)
			curChannelFragment.setInFocus(false);
		if (animView.getVisibility() == View.VISIBLE) {
			animView.setVisibility(View.GONE);
			findViewById(R.id.rl_swipe_background).setVisibility(View.GONE);
			PrefUtils.setIsFirstSwipeAnimShown(true);
		}
		mDisplayMode = DISPLAY_MODE_REMOTE_CONTROL;
		if (remoteControlFragment == null) {
			remoteControlFragment = new ChannelListFragment();
			getSupportFragmentManager().beginTransaction()
					.setCustomAnimations(R.drawable.pull_from_top, 0)
					.add(R.id.fl_remote_control, remoteControlFragment)
					.commit();
		}
		getSupportFragmentManager().beginTransaction()
				.setCustomAnimations(R.drawable.pull_from_top, 0)
				.show(remoteControlFragment).commit();

	}
	public void showChannel(String newChannelId) {
		hideRemoteControl();
		if (newChannelId == null)
			Log.d("checkchannel", "new channel id is null");
		if (newChannelId.equals(mCurChannelId))
			Log.d("checkchannel", "same old channel id old is: "
					+ mCurChannelId + " new is: " + newChannelId);
		if (newChannelId != NULL_CHANNEL_ID
				&& (mCurChannelId == NULL_CHANNEL_ID || !mCurChannelId
						.equals(newChannelId))) {
			curChannelFragment = new ChannelFragment();
			Bundle channelArgs = new Bundle();
			channelArgs.putString(Constant.Key.KEY_CHANNEL_ID, newChannelId);
			curChannelFragment.setArguments(channelArgs);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.fl_main, curChannelFragment).commit();
			mCurChannelId = newChannelId;
		}
		if (PrefUtils.getIsFirstSwipeAnimShown() == false) {
			resetSwipeTimer();
		}
	}

	@Override
	public void onBackPressed() {
		if (remoteControlFragment != null
				&& remoteControlFragment.resetState(this)) {
			// do-nothing
		} else if (mDisplayMode == DISPLAY_MODE_REMOTE_CONTROL)
			hideRemoteControl();
		else
			super.onBackPressed();
	}

	private void hideActionBar() {
		channelsView.setVisibility(View.GONE);
		profileView.setVisibility(View.GONE);
		answerView.setVisibility(View.GONE);
	}

	private void showActionBar() {
		channelsView.setVisibility(View.VISIBLE);
		profileView.setVisibility(View.VISIBLE);
		answerView.setVisibility(View.VISIBLE);
	}

	@Override
	public void onPlayerPrepared() {
		stopSwipeTimer();
		hideActionBar();
	}

	@Override
	public void onPlayerCompleted() {

		showActionBar();
	}

	@Override
	public void onPlayerPaused() {
		showActionBar();
		if (PrefUtils.getIsFirstSwipeAnimShown() == false)
			resetSwipeTimer();
	}

	@Override
	public void onPlayerResumed() {
		hideActionBar();
		stopSwipeTimer();

	}

	@Override
	public void onPlayerPausedToBuffer() {
		stopSwipeTimer();
	}

	@Override
	public void onPlayerResumedAfterBuffer() {
		stopSwipeTimer();
	}

	@Override
	public void onPlayerError() {

	}

	@SuppressLint("HandlerLeak")
	private Handler swipeShowHandler = new Handler() {
		public void handleMessage(Message msg) {
		}
	};

	private Runnable swipeShowCallback = new Runnable() {
		@Override
		public void run() {
			wellDoneSwipe = false;
			// myButton.setVisibility(View.GONE);
			if (!PrefUtils.getIsFirstSwipeAnimShown()) {
				stopSwipeTimer();
				findViewById(R.id.rl_swipe_background).setVisibility(
						View.VISIBLE);
				// animView.setAnimation(animFadeIn);
				AlphaAnimation fade_in = new AlphaAnimation(0.0f, 1.0f);
				fade_in.setDuration(100);
				fade_in.setAnimationListener(new AnimationListener() {
					public void onAnimationStart(Animation arg0) {
						animView.setVisibility(View.VISIBLE);
					}

					public void onAnimationRepeat(Animation arg0) {
					}

					public void onAnimationEnd(Animation arg0) {
						new Thread(new Runnable() {
							@Override
							public void run() {
								swipeAnimation();
							}
						}).start();
					}
				});
				animView.startAnimation(fade_in);
				for(SwipeAnimationDoneListener l : swipeAnimationDoneListeners){
					l.onSwipeStarted();
				}
			}

		}
	};

	private VideoplayInteruptionBroadcastReciever mVideoplayInterruptionBroadcastReciever;

	public void resetSwipeTimer() {
		Log.d("swipe", "resetSwipeTimer");
		swipeShowHandler.removeCallbacks(swipeShowCallback);
		swipeShowHandler.postDelayed(swipeShowCallback, SWIPE_TIMEOUT);
	}

	public void stopSwipeTimer() {
		Log.d("swipe", "stopSwipeTimer");
		swipeShowHandler.removeCallbacks(swipeShowCallback);
	}

	@Override
	public void onUserInteraction() {
		//TODO: Reason for reseting the timer ?
		Log.d("Log ", "onUserInteraction called");
//		if (PrefUtils.getIsFirstSwipeAnimShown() == false)
//			resetSwipeTimer();
	}

	public void addSwipeAnimationDoneListener(
			SwipeAnimationDoneListener listener) {
		swipeAnimationDoneListeners.add(listener);
	}

	public void removeSwipeAnimationDoneListner(
			SwipeAnimationDoneListener listener) {
		swipeAnimationDoneListeners.remove(listener);
	}

	public void wellDoneSwipe() {
		if (!PrefUtils.getIsFirstSwipeAnimShown()) {
			Log.d("RESET", "going to call welldoneswipe");
			wellDoneSwipe = true;
			stopSwipeTimer();
			findViewById(R.id.rl_swipe_background).setVisibility(View.VISIBLE);
			AlphaAnimation fade_in = new AlphaAnimation(0.0f, 1.0f);
			fade_in.setDuration(100);
			fade_in.setAnimationListener(new AnimationListener() {
				public void onAnimationStart(Animation arg0) {
					animView.setVisibility(View.VISIBLE);
					new Thread(new Runnable() {
						@Override
						public void run() {
							swipeAnimation();
						}
					}).start();
				}

				public void onAnimationRepeat(Animation arg0) {
				}

				public void onAnimationEnd(Animation arg0) {
				}
			});
			animView.startAnimation(fade_in);
			for(SwipeAnimationDoneListener swipeAnimationDoneListener : swipeAnimationDoneListeners)
			{
				swipeAnimationDoneListener.onSwipeStarted();
			}
		}
	}

	public void swipeAnimation() {

		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				stopSwipeTimer();
				if (wellDoneSwipe) {
					tvSwipe1.setText("Well Done!");
					tvSwipe2.setText("keep swiping to Discover more");
				} else {
					tvSwipe1.setText("Swipe Up");
					tvSwipe2.setText("to Discover more");
				}
				animation3 = new AlphaAnimation(1.0f, 0.0f);
				animation3.setDuration(1000);
				// animation3.setStartOffset(1000);
				animation3.setRepeatCount(Animation.INFINITE);
				ivSwipe3.startAnimation(animation3);
			}
		});
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				animation2 = new AlphaAnimation(1.0f, 0.0f);
				animation2.setDuration(1000);
				// animation2.setStartOffset(500);
				animation2.setRepeatCount(Animation.INFINITE);
				ivSwipe2.startAnimation(animation2);
			}
		});
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				animation1 = new AlphaAnimation(1.0f, 0.0f);
				animation1.setDuration(1000);
				// animation1.setStartOffset(1000);
				animation1.setRepeatCount(Animation.INFINITE);
				ivSwipe1.startAnimation(animation1);
			}
		});

	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (v.getId()) {

		case R.id.rl_swipe_animation:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				swipePosY = event.getY();
				return true;
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				float currSwipePosY = event.getY();
				if (currSwipePosY - swipePosY <= -20) {
					v.setVisibility(View.GONE);
					PrefUtils.setIsFirstSwipeAnimShown(true);
					findViewById(R.id.rl_swipe_background).setVisibility(
							View.GONE);
					for (SwipeAnimationDoneListener l : swipeAnimationDoneListeners)
						{
							l.onSwipeDone();
						}
					curChannelFragment
							.setPagerCurChildPos(curChannelFragment.currPagePosition + 1);
					return true;
				} else if (currSwipePosY - swipePosY >= 20) {
					PrefUtils.setIsFirstSwipeAnimShown(true);
					v.setVisibility(View.GONE);
					findViewById(R.id.rl_swipe_background).setVisibility(
							View.GONE);
					for (SwipeAnimationDoneListener l : swipeAnimationDoneListeners)
						l.onSwipeDone();
					if (curChannelFragment.currPagePosition > 0)
						curChannelFragment
								.setPagerCurChildPos(curChannelFragment.currPagePosition - 1);
					return true;
				}
			}
		}
		return false;

	}

	private void initialiseSwipeAnim() {
		swipeAnimationDoneListeners = new ArrayList<SwipeAnimationDoneListener>();
		ivSwipe1 = (ImageView) findViewById(R.id.iv_swipe_1);
		ivSwipe2 = (ImageView) findViewById(R.id.iv_swipe_2);
		ivSwipe3 = (ImageView) findViewById(R.id.iv_swipe_3);
		animView = findViewById(R.id.rl_swipe_animation);
		animView.setOnTouchListener(this);
		tvSwipe1 = (BariolTextView) findViewById(R.id.tv_swipe_1);
		tvSwipe2 = (BariolTextView) findViewById(R.id.tv_swipe_2);
	}

	void registerVideoPauseReciever() {
		mVideoplayInterruptionBroadcastReciever = new VideoplayInteruptionBroadcastReciever();
		IntentFilter iFilter = new IntentFilter();
		iFilter.addAction(Intent.ACTION_HEADSET_PLUG);
		iFilter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
		iFilter.addAction("android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION");
		registerReceiver(mVideoplayInterruptionBroadcastReciever, iFilter);
	}

	@Override
	protected void onDestroy() {
		unRegisterVideoPauseReciever();
		super.onDestroy();
	}

	void unRegisterVideoPauseReciever() {
		if (mVideoplayInterruptionBroadcastReciever != null) {
			unregisterReceiver(mVideoplayInterruptionBroadcastReciever);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
	}
	
	/**
	 * shows discover channel instead of feed channel 
	 * for sign up case
	 */
	void showDiscoverIfFirstTimeUser()
	{
		if(getIntent().getBooleanExtra(LauncherActivity.KEY_IS_NEW_USER, false)){
			DEF_CHANNEL_ID = Constant.CHANNEL.DISCOVER;
		}
	}
}
