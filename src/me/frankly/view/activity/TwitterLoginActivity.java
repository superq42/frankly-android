package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.LocationObject;
import me.frankly.model.newmodel.TwitterInfoContainer;
import me.frankly.util.JsonUtils;
import me.frankly.util.PrefUtils;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

public class TwitterLoginActivity extends Activity {

	// Constants
	/**
	 * Register your here app https://dev.twitter.com/apps/new and get your
	 * consumer key and secret
	 * */
	static String TWITTER_CONSUMER_KEY = "aaNDJcxdHadQTxBW8P7B42yoy";
	static String TWITTER_CONSUMER_SECRET = "AAOwvDBHlci4WmJANTmgOLJg28v3HSx0SogBEfQY9TGamsF9CS";

	// Preference Constants

	public static final int REQUEST_CODE_TWITTER_LOGIN = 123;

	static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";

	// Twitter oauth urls
	static final String URL_TWITTER_AUTH = "auth_url";
	static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";

	String appOnly_authentication = "https://api.twitter.com/oauth2/token",
			Request_token_URL = "https://api.twitter.com/oauth/request_token",
			Authorize_URL = "https://api.twitter.com/oauth/authorize",
			Access_token_URL = "https://api.twitter.com/oauth/access_token";

	// Twitter
	private static Twitter twitter;
	private static RequestToken requestToken;
	private WebView twitterWebView;

	// Shared Preferences

	// Internet Connection detector

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_login);

		Controller.registerGAEvent("TwitterLoginActivity");

		// All UI elements
		twitterWebView = (WebView) findViewById(R.id.wv_twitter);
		// Shared Preferences

		/**
		 * Twitter login button click event will call loginToTwitter() function
		 * */

		/**
		 * Button click event to Update Status, will call updateTwitterStatus()
		 * function
		 * */

		/**
		 * Button click event for logout from twitter
		 * */

		/**
		 * This if conditions is tested once is redirected from twitter page.
		 * Parse the uri to get oAuth Verifier
		 * */

		loginToTwitter();

	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.d("twitter", "in on new intent");
		if (!isTwitterLoggedInAlready()) {
			Uri uri = intent.getData();
			if (uri != null && uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
				// oAuth verifier
				String verifier = uri
						.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
				new GetAccessToken(verifier).execute();
				return;

			}
		}
	}

	/**
	 * Function to login twitter
	 * */
	private void loginToTwitter() {
		// Check if already logged in
		if (!isTwitterLoggedInAlready()) {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);

			Configuration configuration = builder.build();

			TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();
			new GetRequestToken().execute();

		} else {
			// user already logged into twitter
			Toast.makeText(getApplicationContext(),
					"Already Logged into twitter", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Function to update status
	 * */

	/**
	 * Function to logout from twitter It will just clear the application shared
	 * preferences
	 * */

	/**
	 * Check user already logged in your application using twitter Login flag is
	 * fetched from Shared Preferences
	 * */
	private boolean isTwitterLoggedInAlready() {
		// return twitter login status from Shared Preferences
		return !PrefUtils.getTwitterAccessToken().equals("");
	}

	protected void onResume() {
		super.onResume();
	}

	class GetRequestToken extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			try {
				requestToken = twitter
						.getOAuthRequestToken(TWITTER_CALLBACK_URL);

			} catch (TwitterException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// TwitterLoginActivity.this.startActivity(new
			// Intent(Intent.ACTION_VIEW,
			// Uri.parse(requestToken.getAuthenticationURL())));

			if (requestToken != null) {
				Log.d("twitter",
						"authentication url: "
								+ requestToken.getAuthenticationURL());
				TwitterLoginActivity.this.twitterWebView.loadUrl(
						requestToken.getAuthenticationURL(), null);
				// final String twitterId = getTwitterIdFromAccounts();
				// if (twitterId != null) {
			}
			twitterWebView.getSettings().setJavaScriptEnabled(true);
			/*
			 * twitterWebView.setWebViewClient(new WebViewClient() {
			 * 
			 * @Override public void onPageFinished(WebView view, String url) {
			 * super.onPageFinished(view,
			 * url); view.loadUrl("javascript:(function() { " +
			 * "document.getElementsByClassName('concerned notice')[0].style.display = 'none'; "
			 * + "})()"); // view.loadUrl(
			 * "javascript:document.getElementById('username_or_email').value = '"
			 * + twitterId + "';"); } });
			 */
			// }

		}
	}

	class GetAccessToken extends AsyncTask<Void, Void, AccessToken> {

		private String verifier;

		public GetAccessToken(String verifier) {
			this.verifier = verifier;
		}

		@Override
		protected AccessToken doInBackground(Void... params) {
			AccessToken accessToken = null;
			try {
				// Get the access token
				accessToken = twitter.getOAuthAccessToken(requestToken,
						verifier);

				// Shared Preferences

				Log.e("Twitter OAuth Token",
						" success > " + accessToken.getToken());

				// Hide login button

			} catch (Exception e) {
				// Check log for login errors
				e.printStackTrace();
				Log.e("Twitter Login Error", "exception > " + e.getMessage());
			}
			return accessToken;

		}

		@Override
		protected void onPostExecute(final AccessToken accessToken) {
			super.onPostExecute(accessToken);

			// Getting user details from twitter
			// For now i am getting his name only
			if (accessToken != null) {
				final long userID = accessToken.getUserId();

				new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							final User user = twitter.showUser(userID);

							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									Intent intent = getIntent();
									TwitterInfoContainer infoContainer = new TwitterInfoContainer();
									infoContainer
											.setTwitterAccessToken(accessToken);
									infoContainer
											.setTwitterUser(setProfileFromTwitter(user));
									intent.putExtra("twitter_info",
											JsonUtils.jsonify(infoContainer));

									setResult(Activity.RESULT_OK, intent);
									finish();

								}
							});

						} catch (TwitterException e) {
							e.printStackTrace();
						}
					}
				}).start();
			} else
				Toast.makeText(TwitterLoginActivity.this,
						"Error fetching Twitter ID. Try some other source",
						Toast.LENGTH_SHORT).show();

			// Displaying in xml ui

		}
	}

	private EditableProfileObject setProfileFromTwitter(User user) {

		EditableProfileObject tempProfileObject = new EditableProfileObject();
		if (user.getDescription() != null)
			tempProfileObject.setBio(Controller.getValidAboutMe(user
					.getDescription()));
		if (user.getProfileBannerURL() != null)
			tempProfileObject.setCover_picture(user.getProfileBannerURL());
		if (user.getName() != null) {
			String[] strs = user.getName().split(" ");
			tempProfileObject.setFirst_name(strs[0]);
			if (strs.length >= 2)
				tempProfileObject.setLast_name(strs[1]);
			else
				tempProfileObject.setLast_name("");
		}
		if (user.getLocation() != null) {
			LocationObject loc = new LocationObject();
			loc.setLocation_name(user.getLocation());
			tempProfileObject.setPermanent_location(loc);
		}
		if (user.getBiggerProfileImageURL() != null)
			tempProfileObject.setProfile_picture(user
					.getBiggerProfileImageURL());

		return tempProfileObject;
	}

	private String getTwitterIdFromAccounts() {
		AccountManager am = AccountManager.get(getApplicationContext());
		Account[] accts = am
				.getAccountsByType("com.twitter.android.auth.login");
		if (accts.length > 0)
			return accts[0].name;
		return null;

	}
}
