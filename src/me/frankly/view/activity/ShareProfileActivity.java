package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.AnimUtils;
import me.frankly.util.ImageUtil;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.util.ShareUtils;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * This activity can be launched in many different modes
 */
public class ShareProfileActivity extends BaseActivity implements
		OnClickListener {

	private final String LOG_TAG = ShareProfileActivity.class.getSimpleName();

	public static final int MODE_NO_QUESTIONS = 0;
	public static final int MODE_GET_ASKED = 1;
	public static final int MODE_ANSWER_POSTED = 2;
	public static final int MODE_FIRST_QUESTION = 3;

	private int mScreenMode;
	private String mTextToShare;
	private boolean mShowingOneTimeLayout;
	private TextView mTvNotifCount;

	// mCelebName, mUserType and mBgPic used only in MODE_FIRST_QUESTION
	private String mCelebName;
	private String mBgPic;
	private int mUserType;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		mScreenMode = intent.getIntExtra(Constant.Key.SCREEN_MODE, 0);
		mTextToShare = intent.getStringExtra(Constant.Key.TEXT_TO_SHARE);

		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "Text to share: " + mTextToShare);
		}
		setContentView(R.layout.layout_share_profile);

		if ((mScreenMode == MODE_ANSWER_POSTED || mScreenMode == MODE_FIRST_QUESTION)
				&& !PrefUtils.isShareScreenShown(mScreenMode)) {
			mShowingOneTimeLayout = true;
			setUpOneTimeLayout(intent);
		} else {
			setUpShareLayout(false);
		}

	}

	/**
	 * sets first time share layout as per
	 * {@link ShareProfileActivity#mScreenMode}
	 * 
	 * @param intent
	 */
	private void setUpOneTimeLayout(Intent intent) {
	
		findViewById(R.id.rl_first_ques).setVisibility(View.VISIBLE);
		findViewById(R.id.rl_root_share_text_and_buttons).setVisibility(View.GONE);
		
		TextView tvTitle = (TextView) findViewById(R.id.tv_title_yay);
		StringBuilder text = new StringBuilder();

		switch (mScreenMode) {
		case MODE_FIRST_QUESTION: {
			tvTitle.setText("Successfully Asked");

			mCelebName = intent.getStringExtra(Constant.Key.KEY_USER_NAME);
			mBgPic = intent.getStringExtra(Constant.Key.KEY_USER_BG_PIC);
			mUserType = intent.getIntExtra(Constant.Key.KEY_USER_TYPE, -1);
			text.append(
					"Your question was sucessfully asked to <b><font color='#ea6153'>")
					.append(mCelebName).append("</font></b>");
			setScreenBackground();
			break;
		}
		case MODE_ANSWER_POSTED: {
			tvTitle.setText("Successfully Replied");
			text.append("Your reply was sucessfully posted to your channel.");
			break;
		}
		}
		TextView tvSuccess = (TextView) findViewById(R.id.encouragement_txt_2);
		tvSuccess.setText(Html.fromHtml(text.toString()));

		findViewById(R.id.frag_profile_what_next_btn).setOnClickListener(this);
	}

	/**
	 * sets final share layout as per {@link ShareProfileActivity#mScreenMode}
	 * 
	 * @param pAnimate
	 */
	private void setUpShareLayout(boolean pAnimate) {

		findViewById(R.id.rl_first_ques).setVisibility(View.GONE);
		findViewById(R.id.rl_root_share_text_and_buttons).setVisibility(View.VISIBLE);
		
		
		if (mScreenMode != MODE_ANSWER_POSTED) {
			setScreenBackground();
		}

		setActionBarButtons();

		if (pAnimate) {
			Animation inFromRight = AnimUtils.inFromRightAnimation();
			AnimUtils.showSlideAnimation(
					findViewById(R.id.rl_root_share_text_and_buttons), inFromRight,
					false, null);
		}

		ImageView ivEncourage = (ImageView) findViewById(R.id.img_encourage);

		switch (mScreenMode) {
		case MODE_GET_ASKED: {
			ivEncourage.setImageResource(R.drawable.ic_encourage_get_asked);
			break;
		}
		case MODE_ANSWER_POSTED: {
			if (mShowingOneTimeLayout) {
				mShowingOneTimeLayout = false;
				PrefUtils.setShareScreenShown(mScreenMode, true);
			}
			ivEncourage.setImageResource(R.drawable.ic_encourage_share_answer);
			break;
		}
		case MODE_FIRST_QUESTION: {
			if (mShowingOneTimeLayout) {
				mShowingOneTimeLayout = false;
				PrefUtils.setShareScreenShown(mScreenMode, true);
			}
			findViewById(R.id.kitty_view_ll).setVisibility(View.VISIBLE);

			ivEncourage.setVisibility(View.GONE);

			TextView txvEncourage = (TextView) findViewById(R.id.encouragement_txt);
			StringBuilder textBuilder = new StringBuilder(
					"Invite your friends to Upvote and follow your question to ")
					.append("<b><font color='#ea6153'>")
					.append(mCelebName)
					.append("</font></b>. Questions with More votes and followers get answered earlier than the rest!");

			txvEncourage.setText(Html.fromHtml(textBuilder.toString()));
			break;
		}
		case MODE_NO_QUESTIONS:
		default: {
			ivEncourage.setImageResource(R.drawable.ic_encourage_no_questions);
			break;
		}
		}

		findViewById(R.id.twitter_share_img).setOnClickListener(this);
		findViewById(R.id.whatsapp_share_img).setOnClickListener(this);
		findViewById(R.id.fb_share_img).setOnClickListener(this);
		findViewById(R.id.default_share_img).setOnClickListener(this);
	}

	/**
	 * set views on screen header as per
	 * {@link ShareProfileActivity#mScreenMode}
	 */
	private void setActionBarButtons() {
		ImageView imgBack = (ImageView) findViewById(R.id.action_bar_my_profile_back);
		imgBack.setOnClickListener(this);
		imgBack.setImageResource(R.drawable.btn_back_header);

		if (mScreenMode == MODE_FIRST_QUESTION
				|| mScreenMode == MODE_ANSWER_POSTED) {
			return;
		}

		View ivMenu3Dots = findViewById(R.id.action_bar_my_profile_menu);
		ivMenu3Dots.setVisibility(View.VISIBLE);
		ivMenu3Dots.setOnClickListener(this);

		// getNotificationCount();
		//
		// mTvNotifCount = (TextView)
		// findViewById(R.id.action_bar_my_profile_notification_count);
		//
		// View ivNotifIcon =
		// findViewById(R.id.action_bar_my_profile_notification);
		// ivNotifIcon.setVisibility(View.VISIBLE);
		// ivNotifIcon.setOnClickListener(this);
	}

	/**
	 * set views on screen header when screen is not launched in
	 * {@link ShareProfileActivity#MODE_ANSWER_POSTED}
	 */
	private void setScreenBackground() {
		final ImageView ivCoverPic = (ImageView) findViewById(R.id.cover_pic);
		ivCoverPic.setImageResource(R.drawable.backdrop);

		String bgImgUrl = null;

		if (mScreenMode == MODE_FIRST_QUESTION) {
			bgImgUrl = mBgPic;
		} else {
			UniversalUser user = PrefUtils.getCurrentUserAsObject();
			bgImgUrl = user.getCover_picture();
			if (bgImgUrl == null && user.getProfile_picture() != null) {
				bgImgUrl = ImageUtil.getThumbUrl(user.getProfile_picture());
			}
		}
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "pic : " + bgImgUrl);
		}
		if (bgImgUrl == null) {
			return;
		}

		ImageLoader.getInstance().loadImage(bgImgUrl,
				new ImageLoadingListener() {

					@Override
					public void onLoadingStarted(String arg0, View arg1) {
						// nothing to do here
					}

					@Override
					public void onLoadingFailed(String arg0, View arg1,
							FailReason arg2) {
						// nothing to do here
					}

					@Override
					public void onLoadingComplete(String arg0, View arg1,
							final Bitmap arg2) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Bitmap bitmap = arg2;
								if (mScreenMode == MODE_GET_ASKED
										|| mScreenMode == MODE_NO_QUESTIONS) {
									bitmap = MyUtilities.fastblur(arg2, 27);
								} else if (mScreenMode == MODE_FIRST_QUESTION
										&& mUserType == UniversalUser.TYPE_CELEB) {
									bitmap = MyUtilities.fastblur(arg2, 10);
								}
								ivCoverPic.setImageBitmap(bitmap);
							}
						});
					}

					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
						// nothing to do here
					}
				});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.frag_profile_what_next_btn: {
			setUpShareLayout(true);
			break;
		}
		case R.id.twitter_share_img: {
			if (ShareUtils.isAppInstalled(ShareUtils.PKG_TWITTER, this)) {
				shareProfile(ShareUtils.PKG_TWITTER);
			} else {
				Toast.makeText(this,
						"Twitter app is not installed on your mobile.",
						Toast.LENGTH_SHORT).show();
			}
			break;
		}
		case R.id.fb_share_img: {
			if (ShareUtils.isAppInstalled(ShareUtils.PKG_FACEBOOK, this)) {
				shareProfile(ShareUtils.PKG_FACEBOOK);
			} else {
				Toast.makeText(this,
						"Facebook app is not installed on your mobile.",
						Toast.LENGTH_SHORT).show();
			}
			break;
		}
		case R.id.whatsapp_share_img: {
			if (ShareUtils.isAppInstalled(ShareUtils.PKG_WHATSAPP, this)) {
				shareProfile(ShareUtils.PKG_WHATSAPP);
			} else {
				Toast.makeText(this,
						"What's App is not installed on your mobile.",
						Toast.LENGTH_SHORT).show();
			}
			break;
		}
		case R.id.default_share_img: {
			shareProfile(Intent.ACTION_SEND);
			break;
		}
		case R.id.action_bar_my_profile_menu: {
			showSettings(v);
			break;
		}
		case R.id.action_bar_my_profile_back: {
			onBackPressed();
			break;
		}
		case R.id.action_bar_my_profile_notification: {
			notificationActivity();
			PrefUtils.setNotificationCount(0);
			mTvNotifCount.setVisibility(View.GONE);
			registerMixpanelEvent(ScreenSpecs.SCREEN_SHARE,
					MixpanelUtils.NAVIGATION_NOTIFICATIONS);
			break;
		}
		}
	}

	public void shareProfile(String packageName) {
		if (packageName.equalsIgnoreCase(Intent.ACTION_SEND)) {
			Intent intent = new Intent(packageName);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, mTextToShare);
			startActivity(intent);
			return;
		}
		MixpanelUtils.sendShareJewelEvent(packageName, null, null,
				Constant.ScreenSpecs.SCREEN_SHARE, this);

		ShareUtils.shareLinkUsingIntent(mTextToShare, this, packageName);
	}

	private void showSettings(View mainView) {
		int[] location = new int[2];
		mainView.getLocationOnScreen(location);

		View view = getLayoutInflater().inflate(R.layout.popup_menu, null);
		final PopupWindow popupWindow = new PopupWindow(this);
		popupWindow.setContentView(view);
		popupWindow.setWidth(MyUtilities.SCREEN_WIDTH / 2);
		popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);

		popupWindow.setFocusable(true);

		ImageView settingsImgView = (ImageView) view
				.findViewById(R.id.img_settings);

		settingsImgView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				settingActivity();
				popupWindow.dismiss();
			}
		});

		ImageView shareImgView = (ImageView) view.findViewById(R.id.img_share);
		shareImgView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				shareProfile(Intent.ACTION_SEND);
				popupWindow.dismiss();
			}
		});
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.showAtLocation(mainView, Gravity.NO_GRAVITY, location[0]
				- popupWindow.getWidth() + mainView.getWidth() / 2,
				mainView.getHeight() - 5);
		Log.d("PU", "width : " + popupWindow.getWidth() + " location[0] : "
				+ location[0]);
	}

	@Override
	public void onBackPressed() {
		if (mScreenMode == MODE_ANSWER_POSTED && mShowingOneTimeLayout) {
			registerMixpanelEvent(ScreenSpecs.SCREEN_SHARE,
					MixpanelUtils.BACK_PRESSED_SHARE_RECORD_ANSWER);
			// here we can also set result for RecordAnswerFragment
		}
		super.onBackPressed();
		if (mScreenMode != MODE_ANSWER_POSTED) {
			overridePendingTransition(0, R.drawable.push_to_right);
		}
	}
}
