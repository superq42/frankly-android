package me.frankly.view.activity;

import java.util.ArrayList;
import java.util.List;

import me.frankly.R;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Constant.DeepLinks;
import me.frankly.config.Constant.Key;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.PostObject;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.QuestionObjectHolder;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.response.SinglePost;
import me.frankly.model.response.SlugDetails;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.view.fragment.AskCelebrityFragment;
import me.frankly.view.fragment.EditProfileFragment;
import me.frankly.view.fragment.RecordAnswerFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

/**
 * This class is to be used as a super class for all activities in this
 * application
 */
public abstract class BaseActivity extends FragmentActivity {

	private static final String LOG_TAG = BaseActivity.class.getSimpleName();

	private ProgressDialog mProgressDialog;
	private boolean isFromNotification;

	/**
	 * 
	 */
	public void launcherActivity() {
		Intent intent = new Intent(this, LauncherActivity.class);
		startActivity(intent);
		finish();
	}

	/**
	 * 
	 * @param pPostObject
	 * @param pParentScreen
	 * @param pRequestCode
	 * @param pFragment
	 */
	public void openComments(PostObject pPostObject, String pParentScreen,
			int pRequestCode, Fragment pFragment) {
		Intent intent = new Intent(this, CommentActivity.class);
		intent.putExtra(Constant.Key.KEY_POST_OBJECT,
				JsonUtils.jsonify(pPostObject));
		Log.i(LOG_TAG, "opencomments(): " + pPostObject.getId());
		if (pParentScreen.equals(ScreenSpecs.SCREEN_PROFILE)) {
			startActivityForResult(intent, pRequestCode);
		} else {
			if (pFragment.getParentFragment() != null) {
				pFragment.getParentFragment().startActivityForResult(intent,
						pRequestCode);
			} else {
				startActivityForResult(intent, pRequestCode);
			}
		}
		overridePendingTransition(R.drawable.pull_up_from_bottom,
				R.drawable.hold);
	}

	/**
	 * 
	 * @param callingFragment
	 * @param mUser
	 * @param requestCodeUpdateVideo
	 */
	public void dispatchTakeVideoIntent(Fragment callingFragment,
			UniversalUser mUser, int requestCodeUpdateVideo) {
		Intent takeVideoIntent = new Intent(this, RecordAnswerActivity.class);
		Bundle args = new Bundle();
		args.putString(RecordAnswerActivity.KEY_EDIT_PROFILE, mUser.getId());
		takeVideoIntent.putExtras(args);
		callingFragment.getParentFragment().startActivityForResult(
				takeVideoIntent, requestCodeUpdateVideo);
	}

	/**
	 * just delegate to parameterized method
	 */
	public void openAnswerQuestionActivity() {
		openAnswerQuestionActivity(null);
	}

	/**
	 * @param pQuestionId
	 */
	public void openAnswerQuestionActivity(QuestionObject pQuestion) {
		Intent i = new Intent(this, AnswerQuestionActivity.class);
		if (pQuestion != null) {
			i.putExtra(Key.KEY_ANSWER_QUESTION, JsonUtils.jsonify(pQuestion));
		}
		if (isFromNotification)
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
		overridePendingTransition(R.drawable.pull_up_from_bottom,
				R.drawable.hold);
	}

	public void openShareAskActivity(QuestionObject shareQuestion) {
		Intent intent = new Intent(this, AskActivity.class);
		intent.putExtra(Constant.Key.KEY_QUESTION_OBJECT,
				JsonUtils.jsonify(shareQuestion));
		intent.putExtra(Constant.Key.KEY_FROM_DEEPLINK, true);
		startActivity(intent);
	}

	/**
	 * 
	 * @param user
	 */
	public void askActivity(UniversalUser user) {
		Intent intent = new Intent(this, AskActivity.class);

		String userJson = JsonUtils.jsonify(user);
		intent.putExtra(Constant.Key.KEY_USER_OBJECT, userJson);
		if (isFromNotification)
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.drawable.pull_from_right, R.drawable.hold);
	}

	/**
	 * 
	 * @param userId
	 */
	public void askActivity(String userId) {
		Intent intent = new Intent(this, AskActivity.class);
		intent.putExtra(Constant.Key.KEY_USER_ID, userId);
		if (isFromNotification)
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		overridePendingTransition(R.drawable.pull_from_right, R.drawable.hold);
	}

	public void askActivityByName(String userName) {
		Intent intent = new Intent(this, AskActivity.class);
		intent.putExtra(Constant.Key.KEY_USER_NAME, userName);
		if (isFromNotification)
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	public void profileActivity(String userId, UniversalUser user) {
		Intent intent = new Intent(this, ProfileActivity.class);
		if (user != null || userId != null) {
			intent.putExtra(Constant.Key.KEY_USER_ID, userId);
			if (user != null) {
				intent.putExtra(Constant.Key.KEY_USER_OBJECT,
						JsonUtils.jsonify(user));
			}
			if (isFromNotification)
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			overridePendingTransition(R.drawable.pull_up_from_bottom,
					R.drawable.hold);

		}

	}

	public void profileActivityByName(String userName, UniversalUser user) {
		Intent intent = new Intent(this, ProfileActivity.class);
		if (userName != null
				&& userName.equals(PrefUtils.getCurrentUserAsObject()
						.getUsername()))
			user = PrefUtils.getCurrentUserAsObject();
		if (user != null || userName != null) {
			intent.putExtra(Constant.Key.KEY_USER_NAME, userName);
			if (user != null) {
				intent.putExtra(Constant.Key.KEY_USER_OBJECT,
						JsonUtils.jsonify(user));
			}
			if (isFromNotification)
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
	}

	public void notificationActivity() {
		Intent intent = new Intent(this, NotificationActivity.class);
		if (isFromNotification)
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	public void settingActivity() {
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivity(intent);
	}

	public void searchActivity() {
		Intent intent = new Intent(this, SearchActivity.class);
		startActivity(intent);
	}

	public void handleDeepLink(Uri uri, boolean pIsFromNotification) {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "handleDeepLink() : " + uri);
		}
		List<String> pathSegments = uri.getPathSegments();

		String pathIndex0 = null;
		if (DeepLinks.URL_SCHEME_FRANKLY.equals(uri.getScheme())) {
			pathIndex0 = uri.getHost();
			if (pathSegments == null) {
				pathSegments = new ArrayList<String>();
			}
			pathSegments.add(0, pathIndex0);
		} else if (DeepLinks.URL_SCHEME_HTTP.equals(uri.getScheme())) {
			if (pathSegments != null && !pathSegments.isEmpty()) {
				pathIndex0 = pathSegments.get(0);
			}
		}
		if (pathIndex0 == null) {
			if (!pIsFromNotification) {
				Controller.showToast(this, R.string.api_failure);
			}
			return;
		}
		isFromNotification = pIsFromNotification;

		if (pathIndex0.equals(DeepLinks.DISCOVER)) {
			// TODO
		} else if (pathIndex0.equals(DeepLinks.FEED)) {
			// TODO
		} else if (pathIndex0.equals(DeepLinks.ANSWER_QUESTIONS)) {
			openAnswerQuestionActivity();
		} else if (pathIndex0.equals(DeepLinks.POST_ID)) {
			if (pathSegments.size() > 1) {
				String postId = pathSegments.get(1);
				int screenMode = PostActivity.MODE_DEFAULT;
				if (pathSegments.size() > 2) {
					String pathIndex2 = pathSegments.get(2);
					if (pathIndex2.equals(DeepLinks.SHARE)) {
						screenMode = PostActivity.MODE_SHARE;
					} else if (pathIndex2.equals(DeepLinks.COMMENTS)
							|| pathIndex2.equals(DeepLinks.COMMENT)) {
						screenMode = PostActivity.MODE_COMMENTS;
					}
				}
				sendGetSinglePostApiRequest(postId, screenMode);
			}
		} else if (pathIndex0.equals(DeepLinks.USER_ID)) {
			if (pathSegments.size() > 1) {
				String userId = pathSegments.get(1);
				profileActivity(userId, null);
			}
		} else if (pathIndex0.equals(DeepLinks.QUESTION_ID)
				|| pathIndex0.equals(DeepLinks.QUESTION)) {
			if (pathSegments.size() > 1) {
				String questionId = pathSegments.get(1);
				sendQuestionDetailsApiRequest(questionId);
			}
		} else if (pathIndex0.equals(DeepLinks.NOTIFICATIONS)) {
			// TODO - remove this if block?
			notificationActivity();
		} else if (pathIndex0.equals(DeepLinks.UPDATE)) {
			// TODO - remove this if block?
			String url = "https://play.google.com/store/apps/details?id=me.frankly";
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
		} else if (pathIndex0.equals(DeepLinks.PROFILE_ID)) {
			// TODO - remove this if block?
			if (pathSegments.size() > 1) {
				String userId = pathSegments.get(1);
				profileActivity(userId, null);
			}
		} else if (pathSegments.size() > 1) {
			// pathIndex0 is username
			String pathIndex1 = pathSegments.get(1);
			if (pathIndex1.equals(DeepLinks.ASK)) {
				askActivityByName(pathIndex0);
			} else {
				// pathIndex1 is SLUG
				sendSlugDetailsApiRequest(pathIndex0, pathIndex1);
			}
		} else {
			// pathIndex0 is username
			profileActivityByName(pathIndex0, null);
		}

		isFromNotification = false;
	}

	/**
	 * @param questionId
	 */
	private void sendQuestionDetailsApiRequest(String questionId) {
		RequestListener listener = new RequestListener() {
			@Override
			public void onRequestStarted() {
				showProgressDialog();
			}

			@Override
			public void onRequestError(int errorCode, String message) {
				removeProgressDialog();
				Controller.showToast(BaseActivity.this, R.string.api_failure);
			}

			@Override
			public void onRequestCompleted(Object responseObject) {
				removeProgressDialog();
				openAppropriateQuestionsListScreen(responseObject);
			}

		};
		Controller.requestQuestionByShortId(questionId, this, listener);
	}

	/**
	 * @flow {@link BaseActivity#handleDeepLink(Uri, boolean)}
	 * @flow {@link BaseActivity#sendQuestionDetailsApiRequest(String)}
	 * @flow {@link RequestListener#onRequestCompleted(Object)}
	 * 
	 * @param responseObject
	 */
	private void openAppropriateQuestionsListScreen(Object responseObject) {
		QuestionObjectHolder response = (QuestionObjectHolder) JsonUtils
				.objectify((String) responseObject, QuestionObjectHolder.class);
		if (response == null || response.getQuestion() == null
				|| response.getQuestion().getQuestion_to() == null) {
			Controller.showToast(BaseActivity.this, R.string.api_failure);
			return;
		}
		QuestionObject question = response.getQuestion();
		UniversalUser questionTo = question.getQuestion_to();
		UniversalUser currentUser = PrefUtils.getCurrentUserAsObject();
		if (questionTo.getId().equals(currentUser.getId())) {
			openAnswerQuestionActivity(question);
		} else {
			openShareAskActivity(question);
		}
	}

	/**
	 * @param pUserName
	 * @param pSlug
	 */
	private void sendSlugDetailsApiRequest(String pUserName, String pSlug) {
		RequestListener listener = new RequestListener() {
			@Override
			public void onRequestStarted() {
				showProgressDialog();
			}

			@Override
			public void onRequestError(int errorCode, String message) {
				removeProgressDialog();
				Controller.showToast(BaseActivity.this, R.string.api_failure);
			}

			@Override
			public void onRequestCompleted(Object responseObject) {
				removeProgressDialog();
				openAppropriateScreenForSlug(responseObject);
			}
		};
		Controller.requestSlugDetails(this, pUserName, pSlug, listener);
	}

	/**
	 * @flow {@link BaseActivity#handleDeepLink(Uri, boolean)}
	 * @flow {@link BaseActivity#sendSlugDetailsApiRequest(String, String)}
	 * @flow {@link RequestListener#onRequestCompleted(Object)}
	 * 
	 * @param responseObject
	 */
	private void openAppropriateScreenForSlug(Object responseObject) {
		SlugDetails response = (SlugDetails) JsonUtils.objectify(
				(String) responseObject, SlugDetails.class);
		if (response == null
				|| (response.getQuestion() == null && response.getPost() == null)) {
			Controller.showToast(BaseActivity.this, R.string.api_failure);
			return;
		}
		if (response.getPost() != null) {
			openPostActivity(response.getPost(), PostActivity.MODE_DEFAULT);
		} else {
			QuestionObject question = response.getQuestion();
			UniversalUser questionTo = question.getQuestion_to();
			UniversalUser currentUser = PrefUtils.getCurrentUserAsObject();
			if (questionTo.getId().equals(currentUser.getId())) {
				openAnswerQuestionActivity(question);
			} else {
				openShareAskActivity(question);
			}
		}
	}

	/**
	 * @param postId
	 * @param pScreenMode
	 */
	public void sendGetSinglePostApiRequest(String postId, final int pScreenMode) {
		RequestListener listener = new RequestListener() {
			@Override
			public void onRequestStarted() {
				showProgressDialog();
			}

			@Override
			public void onRequestError(int errorCode, String message) {
				removeProgressDialog();
				Controller.showToast(BaseActivity.this, R.string.api_failure);
			}

			@Override
			public void onRequestCompleted(Object responseObject) {
				removeProgressDialog();
				SinglePost singlePost = (SinglePost) JsonUtils.objectify(
						(String) responseObject, SinglePost.class);
				if (singlePost != null && singlePost.getPost() != null) {
					openPostActivity(singlePost.getPost(), pScreenMode);
				} else {
					Controller.showToast(BaseActivity.this,
							R.string.api_failure);
				}
			}
		};
		Controller.getSinglePost(this, postId, listener);
	}

	/**
	 * @param pPostObject
	 * @param pScreenMode
	 */
	public void openPostActivity(PostObject pPostObject, int pScreenMode) {
		Intent intent = new Intent(this, PostActivity.class);
		String postObjectJson = JsonUtils.jsonify(pPostObject);
		intent.putExtra(Key.KEY_POST_OBJECT, postObjectJson);
		intent.putExtra(Key.SCREEN_MODE, pScreenMode);
		if (isFromNotification)
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	/**
	 * @param pFragment
	 *            or null if launched from any activity
	 * @param pTextToShare
	 * @param pCelebName
	 * @param pCelebPic
	 * @param pType
	 */
	public void shareActivity(Fragment pFragment, String pTextToShare,
			String pCelebName, String pCelebPic, int pType) {
		Intent i = new Intent(this, ShareProfileActivity.class);
		i.putExtra(Key.TEXT_TO_SHARE, pTextToShare);

		if (pFragment instanceof RecordAnswerFragment) {
			i.putExtra(Key.SCREEN_MODE, ShareProfileActivity.MODE_ANSWER_POSTED);
			pFragment.startActivityForResult(i,
					RecordAnswerFragment.REQUEST_SHARE_ACTIVITY);
		} else if (pFragment instanceof EditProfileFragment) {
			i.putExtra(Key.SCREEN_MODE, ShareProfileActivity.MODE_GET_ASKED);
			pFragment.startActivity(i);
		} else if (pFragment instanceof AskCelebrityFragment) {
			i.putExtra(Key.SCREEN_MODE,
					ShareProfileActivity.MODE_FIRST_QUESTION);
			i.putExtra(Key.KEY_USER_NAME, pCelebName);
			i.putExtra(Key.KEY_USER_BG_PIC, pCelebPic);
			i.putExtra(Key.KEY_USER_TYPE, pType);
			pFragment.startActivity(i);
		} else {
			startActivity(i);
		}
		overridePendingTransition(R.drawable.pull_from_right, R.drawable.hold);
	}

	/**
	 * @param pSource
	 * @param pEvent
	 */
	public void registerMixpanelEvent(String pSource, String pEvent) {
		MixpanelUtils.sendGenericEvent(null, null, null, pSource, pEvent, this);
	}

	/**
	 * all sub-classes can use these two methods
	 * 
	 * @see BaseActivity#removeProgressDialog()
	 */
	public void showProgressDialog() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mProgressDialog == null) {
					mProgressDialog = new ProgressDialog(BaseActivity.this,
							ProgressDialog.THEME_HOLO_LIGHT);
					mProgressDialog.setMessage("Please wait...");
					mProgressDialog.setCancelable(false);
				}
				mProgressDialog.show();
			}
		});
	}

	/**
	 * all sub-classes can use these two methods
	 * 
	 * @see BaseActivity#showProgressDialog()
	 */
	public void removeProgressDialog() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mProgressDialog != null) {
					mProgressDialog.hide();
				}
			}
		});
	}
}
