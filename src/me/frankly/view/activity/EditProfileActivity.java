package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.JsonUtils;
import me.frankly.util.PrefUtils;
import me.frankly.view.fragment.EditProfileFragment;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class EditProfileActivity extends BaseActivity {

	private static final String THUMB_PATH = "thumb_path";
	private static final String VIDEO_PATH = "video_path";
//	private ImageView imgUserPic;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_editprofile);
		
		Log.d("FF", "Yoddling in EditProfileActivity");
		
		Controller.registerGAEvent("EditProfileActivity");
		
		UniversalUser universalUser = PrefUtils.getCurrentUserAsObject();
	//	ImageLoader.getInstance().displayImage(picUrl, imgUserPic);

		EditProfileFragment fragment = new EditProfileFragment();
		Bundle bundle = new Bundle();

		bundle.putString(Constant.Key.KEY_USER_OBJECT, JsonUtils.jsonify(universalUser));
		fragment.setArguments(bundle);

		getSupportFragmentManager().beginTransaction().add(R.id.act_editprofile_frame_l, fragment).commit();

	}

	public void finishActivityWithResult(boolean success, String videoPath, String thumbPath) {
		if (success) {
			Intent i = getIntent();
			i.putExtra(VIDEO_PATH, videoPath);
			i.putExtra(THUMB_PATH, thumbPath);
			setResult(Activity.RESULT_OK, i);
		} else {
			setResult(Activity.RESULT_CANCELED, null);
		}

		this.finish();

	}

}
