package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.util.ShareUtils;
import me.frankly.view.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends BaseActivity implements OnClickListener {

	private static int CHANGE_USERNAME = 0;
	private static int CHANGE_USERPASS = 1;
	private ProgressBar spinner;
	private ProgressDialog mConnectionProgressDialog;
	private ImageView action_bar_back_btn;
	private TextView setting_whatsapp_invite_btn;
	private boolean whatsappAvailable = true;
	private UniversalUser mUser;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
	
		Controller.registerGAEvent("SettingsActivity");
		
		TextView title_text = (TextView) findViewById(R.id.action_bar_title_text);
		title_text.setText("Settings");
		this.whatsappAvailable = MyUtilities.checkForPackage(this,
				ShareUtils.PKG_WHATSAPP);
		findAllViews();
		registerListeners();
		mUser = PrefUtils.getCurrentUserAsObject();
	}

	private void registerListeners() {

		action_bar_back_btn.setOnClickListener(this);
		if (whatsappAvailable)
			setting_whatsapp_invite_btn.setOnClickListener(this);

	}

	private void findAllViews()  {

		action_bar_back_btn = (ImageView) findViewById(R.id.action_bar_back_btn);

		findViewById(R.id.settings_rl_invite).setOnClickListener(this);
		findViewById(R.id.settings_rl_review).setOnClickListener(this);
		findViewById(R.id.settings_rl_change_username).setOnClickListener(this);
		findViewById(R.id.settings_rl_change_password).setOnClickListener(this);
		findViewById(R.id.settings_rl_logout).setOnClickListener(this);
		findViewById(R.id.settings_rl_feedback).setOnClickListener(this);
		findViewById(R.id.tv_terms).setOnClickListener(this) ; 
		findViewById(R.id.tv_policy).setOnClickListener(this) ; 

		if (whatsappAvailable) {
			setting_whatsapp_invite_btn = (TextView) findViewById(R.id.setting_whatsapp_invite_btn);
			setting_whatsapp_invite_btn.setVisibility(View.VISIBLE);
		}
		PackageInfo pInfo;
		String version = "1.2" ; 
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			version = pInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		
		TextView tv_version_name = (TextView) findViewById(R.id.version_name) ; 
		tv_version_name.setText("Version: " +version) ; 
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.action_bar_back_btn:
			this.onBackPressed();
			break;
		case R.id.settings_rl_invite:
			showCustomDialog();
			
			break;
		case R.id.settings_rl_review:
			openPlayStore();
			break;
		case R.id.settings_rl_change_username:
			getMultiPurposeEditTextDialog("Change User Name", CHANGE_USERNAME);
			break;
		case R.id.settings_rl_change_password:
			getMultiPurposeEditTextDialog("Change Password", CHANGE_USERPASS);
			break;
		case R.id.settings_rl_feedback:
			/*Intent intent = new Intent(this,
					FeedBackActivity.class);
			startActivity(intent);
			Controller.recordResponse(getActivity(), "n", MEDIUM, "",
					responseRequestListener);*/
			MyUtilities.sendEmail(this);
			break;

		case R.id.settings_rl_logout:
		 if (Controller.isNetworkConnected(this)) {
				if (PrefUtils.getPendingUploads() != null
				 && PrefUtils.getPendingUploads().getPendingUploads() != null
				 && PrefUtils.getPendingUploads().getPendingUploads().size() != 0) 
				    showDiscardDialog("Are you sure?","Your answers are still being uploaded. Logging out will " +
				    		"delete them forever.");
			     else
				    showDiscardDialog("Don't leave us yet!","Logging out will make you miss out on" +
				    		" some awesome stories. Are you sure?") ; 
			}else {
				Controller
				.showToast(this,
						"There seems to be a network issue. Can't log you out at the moment.");
	           }
		
		    break;
		case R.id.setting_whatsapp_invite_btn:
			if (whatsappAvailable)
				MyUtilities.shareLinkOnFacebookUsingIntent(getResources()
						.getString(R.string.default_invite_text), this,
						ShareUtils.PKG_WHATSAPP);
			break;
		case R.id.tv_terms:
			Intent terms = new Intent(Intent.ACTION_VIEW) ;
			terms.setData(Uri.parse("http://frankly.me/tos")) ; 
			startActivity(terms) ; 
			break ; 
		case R.id.tv_policy:
			Intent policy = new Intent(Intent.ACTION_VIEW) ;
			policy.setData(Uri.parse("http://frankly.me/privacy")) ; 
			startActivity(policy) ; 
			break ; 
		default:
			break;
		}

	}

	private void showCustomDialog() {
		ShareDialog cdd = new ShareDialog(this,
				R.layout.share_dialog_post_layout, null, 1, false,Constant.ScreenSpecs.SCREEN_PROFILE,null);
		cdd.show();
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
	
		if (resultCode == RESULT_OK)
		Controller.showToast(this, "Invitation sent!") ;
	}

	private void logoutAnyway() {
     
		mConnectionProgressDialog = new ProgressDialog(this,
				ProgressDialog.THEME_HOLO_LIGHT);
		mConnectionProgressDialog
				.setMessage("Securely logging you out of Frankly");
		if (!mConnectionProgressDialog.isShowing())
			mConnectionProgressDialog.show();
		Controller.logoutUserExplicit(this);
     
     
	}

	public void getMultiPurposeEditTextDialog(String title, final int type) {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alert);
		final EditText etEdit = (EditText) dialog.findViewById(R.id.editText1);

		if (type == CHANGE_USERNAME) {
			etEdit.setHint(mUser.getUsername());
		} else {
			etEdit.setHint("Password");
		}

		TextView txtTitle = (TextView) dialog.findViewById(R.id.tv_title);
		txtTitle.setText(title);
		if (type == CHANGE_USERNAME) {
			Log.d("Settings", PrefUtils.getCurrentUserAsObject().getUsername());
			if (PrefUtils.getCurrentUserAsObject() != null)
				etEdit.setText(PrefUtils.getCurrentUserAsObject().getUsername());

		} else {
			etEdit.setHint("Min. 6 characters long.");
		}

		Button BtnOk = (Button) dialog.findViewById(R.id.buttonOk);
		Button BtnCancel = (Button) dialog.findViewById(R.id.buttonCancel);
		getKeyboardForcefully(etEdit);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();

		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		@SuppressWarnings("deprecation")
		int width = display.getWidth();
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = width;
		dialog.getWindow().setAttributes(lp);

		BtnCancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				removeKeyboardForcefully(etEdit);

				dialog.cancel();
			}
		});

		BtnOk.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (type == CHANGE_USERNAME) {

					changeUserName(etEdit.getText().toString(), dialog);
				} else {
					changePassword(etEdit.getText().toString(), dialog);

				}

			}
		});

	}

	private void openPlayStore() {
		try {
			Intent viewIntent = new Intent("android.intent.action.VIEW",
					Uri.parse("https://play.google.com/store/apps/details?id="
							+ getPackageName()));
			startActivity(viewIntent);
		} catch (Exception e) {
			Toast.makeText(
					getApplicationContext(),
					"Unable to connect to the playstore, please try again after some time.",
					Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	public void removeKeyboardForcefully(View et) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(et.getWindowToken(), 0);

	}

	public void getKeyboardForcefully(final View et) {
		et.requestFocus();
		et.postDelayed(new Runnable() {
			public void run() {
				InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(et, 10);
			}
		}, 50);
	}

	private void changeUserName(String username, final Dialog dialog) {
		final String newusername = username;

		if (username == null || username.length() < 6) {

			Controller.showToast(SettingsActivity.this,
					"Username should be at least 6 characters long!");

		} else if (PrefUtils.getCurrentUserAsObject() != null
				&& PrefUtils.getCurrentUserAsObject().getUsername()
						.equalsIgnoreCase(username)) {
			Controller.showToast(SettingsActivity.this,
					"This is your current username. Please select a new one.");
		} else {
			Controller.changeUserName(this, username, new RequestListener() {

				@Override
				public void onRequestStarted() {
					showSpinner();
				}

				@Override
				public void onRequestError(int errorCode, String message) {
					errorMessage(message, "Failed to change your username.");
				}

				@Override
				public void onRequestCompleted(Object responseObject) {

					hideSpinner();
					removeKeyboardForcefully(action_bar_back_btn);
					Controller.showToast(SettingsActivity.this,
							"Your username has been changed successfully!");
					mUser.setUsername(newusername);
					PrefUtils.saveCurrentUserAsJson(JsonUtils.jsonify(mUser));

				}
			});
			dialog.cancel();
		}
	}

	private void changePassword(String password, final Dialog dialog) {
		if (password != null && password.length() >= 6) {
			Controller.changePassword(this, password, new RequestListener() {

				@Override
				public void onRequestStarted() {
					showSpinner();
				}

				@Override
				public void onRequestError(int errorCode, String message) {
					errorMessage(message, "Failed to change your password.");
				}

				@Override
				public void onRequestCompleted(Object responseObject) {
					hideSpinner();
					removeKeyboardForcefully(action_bar_back_btn);

					Controller.showToast(SettingsActivity.this,
							"Your password has been changed successfully!");
				}
			});
			dialog.cancel();
		} else {
			Controller.showToast(SettingsActivity.this,
					"Password should be at least 6 characters long!");
		}

	}

	private void errorMessage(String message, String errorText) {
		hideSpinner();

		try {
			JSONObject errorObject = new JSONObject(message);
			String errormessage = errorObject.getString("message");
			Controller.showToast(SettingsActivity.this, errormessage);
		} catch (JSONException e) {

			if (!Controller.isNetworkConnected(this))
				errorText = errorText
						+ " Please check your network connection.";
			Controller.showToast(SettingsActivity.this, errorText);
			e.printStackTrace();
		}
	}

	private void showSpinner() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				spinner = (ProgressBar) findViewById(R.id.progressBar1);
				spinner.setVisibility(View.VISIBLE);

			}
		});

	}

	private void hideSpinner() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				spinner.setVisibility(View.GONE);
			}
		});
	}

	public void showDiscardDialog(String title,String body) {
		 AlertDialog.Builder alertDialogBuilder  = new AlertDialog.Builder(this);

		alertDialogBuilder.setTitle(title);
		alertDialogBuilder
				.setMessage(body);
		alertDialogBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						logoutAnyway();

					}
				});
		alertDialogBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// cancel the alert box and put a Toast to the user
						dialog.cancel();

					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		// show alert
		alertDialog.show();
	}

	
	


}
