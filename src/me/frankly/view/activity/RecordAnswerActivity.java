package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.listener.SubmissionStatusListener;
import me.frankly.util.PrefUtils;
import me.frankly.view.fragment.RecordAnswerFragment;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

public class RecordAnswerActivity extends BaseActivity implements SubmissionStatusListener {

	public static final String KEY_EDIT_PROFILE = "editable_profile";
	// private RelativeLayout rl_topcontent;
	public static final String KEY_QUESTION_ASKED = "question_asked";
	public static final String KEY_QUESTION_OBJECT = "record_ans";
	private String questionObject;
	private String editableProfile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record_answer);
	
		Controller.registerGAEvent("RecordAnswerActivity");
		
		// rl_topcontent = (RelativeLayout) findViewById(R.id.rl_topcontent);
		// questionObject =
		// " {                 \"body\": \"What is one thing that you want to change about yourself?\",                 \"ask_count\": 0,                 \"tags\": [],                 \"timestamp\": 1413883000,                 \"id\": \"546ef175df3103663844da5f\",                 \"is_anonymous\": true,                 \"media\": null,                 \"serial_id\": \"26081537867251321202020382986\",                 \"question_type\": \"text\",                 \"question_author\": {                     \"username\": \"Anonymous\",                     \"first_name\": \"Anonymous\",                     \"last_name\": \"\",                     \"gender\": \"F\",                     \"profile_picture\": null,                     \"id\": \"53aecf5bdf310364b912edc1\"                 },                 \"askers\": [],                 \"location\": {                     \"coordinate_point\": {                         \"type\": \"Point\",                         \"coordinates\": [                             0,                             0                         ]                     }                 }             }         ";
		questionObject = getIntent().getStringExtra(KEY_QUESTION_ASKED);
		editableProfile = getIntent().getStringExtra(KEY_EDIT_PROFILE);
		// Log.e("recansactivity", stringExtra);
		RecordAnswerFragment fragment = new RecordAnswerFragment();
		fragment.setSubmissionResultListener(this);
		Bundle args = new Bundle();

		if (questionObject != null)
			args.putString(KEY_QUESTION_ASKED, questionObject);
		else if (editableProfile != null)
			args.putString(KEY_EDIT_PROFILE, editableProfile);

		// EditableProfileObject profileObject = new EditableProfileObject();
		// profileObject.setId("bharat_verma");
		// args.putString(KEY_EDIT_PROFILE, JsonUtils.jsonify(profileObject));
		fragment.setArguments(args);
		showThisFragment(R.id.rl_topcontent, fragment, 0);
		Log.d("token", PrefUtils.getAppAccessToken() + " usename:" + PrefUtils.getUsername());

	}

	public void showThisFragment(int content, Fragment fragment, int tag) {

		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(content, fragment);
		transaction.addToBackStack(String.valueOf(tag));
		transaction.commitAllowingStateLoss();

	}

	@Override
	protected void onResume() {
		super.onResume();
		// networkChangeReceiver = new NetworkChangeReceiver(this);
		// registerReceiver(networkChangeReceiver, new
		// IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
	}

	@Override
	public void onSubmitted(String postID, boolean success) {
		Log.i("TAG_RAVINDER", "post id: " + postID);
		if (success) {
			Intent i = getIntent();
			i.putExtra("question_obj", postID);
			RecordAnswerActivity.this.setResult(Activity.RESULT_OK, i);
			RecordAnswerActivity.this.finish();

		}
	}

	@Override
	public void onSubmissionCanceled(Bundle data, int type, boolean success) {

		Intent i = new Intent();
		String canceled = data.getString("submission_canceled");
		i.putExtra("submission_canceled", canceled);
		RecordAnswerActivity.this.setResult(Activity.RESULT_CANCELED, i);
		RecordAnswerActivity.this.finish();
	}

	@Override
	public void onSubmitted(boolean success, String video_path, String thumb_path) {
		Log.i("debug301_onSubmitted", "video_path " + video_path + " thumb_path:" + thumb_path);
		if (success) {
			Intent i = getIntent();
			i.putExtra("video_path", video_path);
			i.putExtra("thumb_path", thumb_path);
			RecordAnswerActivity.this.setResult(Activity.RESULT_OK, i);
			RecordAnswerActivity.this.finish();
		}
	}

}
