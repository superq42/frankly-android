package me.frankly.view.activity;

import java.util.ArrayList;
import java.util.Iterator;

import me.frankly.R;
import me.frankly.adapter.CategoryCelebsAdapter;
import me.frankly.adapter.CategoryCelebsAdapter.OnMoreClickedListener;
import me.frankly.adapter.SearchListAdapter;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.RequestListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.newmodel.SearchResults;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.response.CategoryCeleb;
import me.frankly.model.response.CategoryCelebList;
import me.frankly.model.response.SearchableObjectContainer;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.ShareUtils;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SearchActivity extends BaseActivity implements TextWatcher,
		RequestListener, OnScrollListener, OnClickListener,
		OnItemClickListener, OnMoreClickedListener, UserFollowListener {

	private static final int LIST_MODE_SEARCH = 1;
	private static final int LIST_MODE_CAT_CELEB = 2;
	private static final int LIST_MODE_NONE = -1;
	private static final int ERROR_MODE_EMPTY_RESULT = 1;
	private static final int ERROR_MODE_NO_INTERNET = 2;
	private static final int ERROR_MODE_NONE = -1;

	private EditText searchEditText;
	private ListView searchListView;
	private ArrayList<SearchableObjectContainer> searchResult = new ArrayList<SearchableObjectContainer>();
	private SearchListAdapter searchAdapter;
	private BaseAdapter categoryCelebAdapter;
	private ArrayList<CategoryCeleb> categoryCelebs = new ArrayList<CategoryCeleb>();
	private boolean isPaused = true;

	private View clearView, errorContainerView;
	private TextView errorMessageTextView;
	private ImageView loaderView, errorIconImageView;
	private AnimationDrawable loaderDrawable;
	private int nextIndex = 0;
	private boolean isLoading = false;
	private volatile String mCurQueryString;
	private int mCurListMode = LIST_MODE_NONE, mCurErrorMode = ERROR_MODE_NONE;
	private boolean isTwitterInstalled;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#Ninja.SearchAc:";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		fileDebugLog(FILE_DEBUG_TAG + "onCreate", "Called");

		searchEditText = (EditText) findViewById(R.id.et_search);
		searchListView = (ListView) findViewById(R.id.lv_search);
//		backView = findViewById(R.id.iv_back);
		isTwitterInstalled = ShareUtils.isAppInstalled(ShareUtils.PKG_TWITTER,
				this);
		if(AppConfig.DEBUG) {
			Log.d("SA", "Search Activity on Create");
		}
		Log.d("search", "istwitterinstalled " + isTwitterInstalled);
		searchEditText.addTextChangedListener(this);
		searchAdapter = new SearchListAdapter(this, searchResult);
		categoryCelebAdapter = new CategoryCelebsAdapter(this, categoryCelebs,
				CategoryCelebsAdapter.MODE_ASK);
		((CategoryCelebsAdapter) categoryCelebAdapter)
				.setOnMoreClickedListener(this);
		loaderView = new ImageView(this);
		loaderDrawable = (AnimationDrawable) getResources().getDrawable(
				R.drawable.loader_general);
		loaderView.setImageDrawable(loaderDrawable);
		searchListView.addFooterView(loaderView);
		hideLoaderView();
		searchListView.setOnScrollListener(this);
		clearView = findViewById(R.id.iv_search_clear);
		clearView.setOnClickListener(this);
//		backView.setOnClickListener(this);
		searchListView.setOnItemClickListener(this);
		findErrorViews();
		getCategoryCelebs();
		displayCategoryCelebs();

	}

	public void addUserFollowListners() {
		for (CategoryCeleb cc : categoryCelebs) {
			for (UniversalUser user : cc.getUsers()) {
				UniversalActionListenerCollection.getInstance()
						.addUserFollowListener(this, user.getId());
			}
		}
	}

	private void findErrorViews() {
		fileDebugLog(FILE_DEBUG_TAG + "findErrorViews", "Called");

		errorContainerView = findViewById(R.id.rl_error);
		errorIconImageView = (ImageView) findViewById(R.id.iv_err_icon);
		errorMessageTextView = (TextView) findViewById(R.id.tv_err_msg);
	}

	@Override
	protected void onResume() {
		super.onResume();
		fileDebugLog(FILE_DEBUG_TAG + "onResume", "Called");

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				MyUtilities.hideKeyboard(searchEditText, SearchActivity.this);

			}
		}, 100);
		isPaused = false;
	}

	private void displaySearch() {
		fileDebugLog(FILE_DEBUG_TAG + "displaySearch", "Called");

		if (mCurListMode != LIST_MODE_SEARCH) {
			searchListView.setAdapter(searchAdapter);
			mCurListMode = LIST_MODE_SEARCH;
		}
	}

	private void displayCategoryCelebs() {
		fileDebugLog(FILE_DEBUG_TAG + "displayCategoryCelebs", "Called");

		if (Controller.isNetworkConnected(this))
			hideError();
		if (mCurListMode != LIST_MODE_CAT_CELEB) {
			searchListView.setAdapter(categoryCelebAdapter);
			mCurListMode = LIST_MODE_CAT_CELEB;
		}
	}

	private void getCategoryCelebs() {
		fileDebugLog(FILE_DEBUG_TAG + "getCategoryCelebs", "Called");

		if (Controller.isNetworkConnected(this)) {
			showLoaderView();
			Controller.getCategorisedCelebs(this, catCelebListener);
		} else
			showNoInternetError();

	}

	@Override
	public void afterTextChanged(Editable arg0) {
		fileDebugLog(FILE_DEBUG_TAG + "afterTextChanged", "Called");

		if (arg0.toString().trim().isEmpty())
			displayCategoryCelebs();
		else {
			displaySearch();
			initSearch(arg0);
		}

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	private void initSearch(Editable arg0) {
		fileDebugLog(FILE_DEBUG_TAG + "initSearch", "Called");

		if (Controller.isNetworkConnected(this)) {
			if (!searchResult.isEmpty()) {
				searchResult.clear();
				searchAdapter.notifyDataSetChanged();
			}
			isLoading = false;
			nextIndex = 0;
			mCurQueryString = arg0.toString().trim();
			search();
			hideError();
		} else
			showNoInternetError();
	}

	private void search() {
		fileDebugLog(FILE_DEBUG_TAG + "search()", "Called");

		if (!isLoading && isMoreDataAvailable() && mCurQueryString != null
				&& !mCurQueryString.isEmpty()) {
			isLoading = true;
			showLoaderView();
			Controller.searchElastic(this, mCurQueryString,
					Controller.DEFAULT_PAGE_SIZE, nextIndex, this);
		} else if (mCurQueryString != null && mCurQueryString.isEmpty())
			hideLoaderView();

	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		fileDebugLog(FILE_DEBUG_TAG + "onRequestCompleted-search result:", ""
				+ (String) responseObject);
		final SearchResults results = (SearchResults) JsonUtils.objectify(
				(String) responseObject, SearchResults.class);
		if (results.getQ().equals(mCurQueryString)) {
			nextIndex = results.getNextIndex();
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					hideLoaderView();
					isLoading = false;
					if (!isTwitterInstalled)
						results.setResults(removeInvitablesWithNoEmails(results
								.getResults()));
					searchResult = MyUtilities.appendUniqueItems(searchResult,
							removeUnknowns(results.getResults()),
							MyUtilities.MODE_DELETE_PREVIOUS);
					if (!searchResult.isEmpty()) {
						hideError();
						for (int i = 0; i < searchResult.size(); i = i + 1) {
							if (searchResult
									.get(i)
									.getType()
									.equals(SearchableObjectContainer.TYPE_INVITABLE_USER)
									&& searchResult.get(i).getInvitable()
											.isHas_invited()) {
								searchResult.remove(i);
							}
						}
						searchAdapter.setData(searchResult);
						searchAdapter.notifyDataSetChanged();
					} else
						showEmptyResultError();

				}
			});
		}

	}

	@Override
	public void onRequestError(int errorCode, String message) {

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (isNearEnd(firstVisibleItem + visibleItemCount, totalItemCount))
			search();

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		fileDebugLog(FILE_DEBUG_TAG + "onScrollStateChanged", "Called");

		MyUtilities.hideKeyboard(searchEditText, this);

	}

	private boolean isNearEnd(int i, int totalItemCount) {
		// fileDebugLog(FILE_DEBUG_TAG + "isNearEnd",
		// "(totalItemCount - i < 3)? "
		// + (totalItemCount - i < 3));

		return totalItemCount - i < 3;
	}

	private void showLoaderView() {
		fileDebugLog(FILE_DEBUG_TAG + "showLoaderView", "Called");

		if (loaderView.getVisibility() != View.VISIBLE) {
			loaderView.setVisibility(View.VISIBLE);
			loaderDrawable.start();
		}

	}

	private void hideLoaderView() {
		fileDebugLog(FILE_DEBUG_TAG + "hideLoaderView", "Called");

		if (loaderView.getVisibility() == View.VISIBLE) {
			loaderView.setVisibility(View.GONE);
			loaderDrawable.stop();
		}

	}

	private boolean isMoreDataAvailable() {
		fileDebugLog(FILE_DEBUG_TAG + "isMoreDataAvailable",
				"(nextIndex >= 0)? " + (nextIndex >= 0));

		return nextIndex >= 0;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_search_clear:
			fileDebugLog(FILE_DEBUG_TAG + "onClick-clearView", "Called");

			clearSearch();
			break;
		/*case R.id.iv_back:
			fileDebugLog(FILE_DEBUG_TAG + "onClick-backView", "Called");

			onBackPressed();*/
		}

	}

	private void clearSearch() {
		fileDebugLog(FILE_DEBUG_TAG + "clearSearch", "Called");

		if (!searchEditText.getText().toString().isEmpty()) {
			mCurQueryString = "";
			nextIndex = 0;
			searchEditText.setText("");
			searchResult.clear();
			searchAdapter.notifyDataSetChanged();
			hideLoaderView();
			displayCategoryCelebs();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		fileDebugLog(FILE_DEBUG_TAG + "onItemClick", "Called");

		try {
			UniversalUser thisUser = searchResult.get(arg2).getUser();
			profileActivity( null, thisUser);
			MixpanelUtils.sendGenericEvent(thisUser.getId(),
					thisUser.getUsername(),
					String.valueOf(thisUser.getUser_type()),
					Constant.ScreenSpecs.SCREEN_SEARCH,
					MixpanelUtils.NAVIGATION_PROFILE, this);
		} catch (Exception e) {
		}
	}

	private RequestListener catCelebListener = new RequestListener() {

		@Override
		public void onRequestStarted() {

		}

		@Override
		public void onRequestError(int errorCode, String message) {
			Log.d("search error", "" + errorCode + message);

		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			fileDebugLog(FILE_DEBUG_TAG
					+ "onRequestCompleted-cat celeb response:", ""
					+ (String) responseObject);
			Log.d("search error", "inside response");
			final CategoryCelebList celebList = (CategoryCelebList) JsonUtils
					.objectify((String) responseObject, CategoryCelebList.class);

			if (!isFinishing()) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						Log.d("search error", "inside run");
						if (celebList != null && celebList.getResults() != null) {
							categoryCelebs.addAll(celebList.getResults());
							categoryCelebAdapter.notifyDataSetChanged();
							addUserFollowListners();
							if (mCurListMode == LIST_MODE_CAT_CELEB)
								hideLoaderView();
						} else {
							Log.d("search error", "null");
						}
					}
				});
			}

		}
	};

	private ArrayList<SearchableObjectContainer> removeUnknowns(
			ArrayList<SearchableObjectContainer> stream) {
		Iterator<SearchableObjectContainer> i = stream.iterator();
		while (i.hasNext()) {
			SearchableObjectContainer temp = i.next();
			if (!SearchableObjectContainer.ACCEPTED_TYPE.contains(temp
					.getType()))
				i.remove();
		}
		return stream;

	}

	public ArrayList<SearchableObjectContainer> removeInvitablesWithNoEmails(
			ArrayList<SearchableObjectContainer> stream) {
		fileDebugLog(FILE_DEBUG_TAG + "removing invitables with no email",
				"!!!");
		Iterator<SearchableObjectContainer> i = stream.listIterator(0);
		fileDebugLog(FILE_DEBUG_TAG + "iterator size:", "" + stream.size());
		while (i.hasNext()) {
			SearchableObjectContainer temp = i.next();

			if (temp.getType().equals(
					SearchableObjectContainer.TYPE_INVITABLE_USER)
					&& (temp.getInvitable().getEmail() == null || temp
							.getInvitable().getEmail().isEmpty())) {
				fileDebugLog(FILE_DEBUG_TAG + "removing invitable:", ""
						+ temp.getInvitable().getFirst_name());
				i.remove();
			}
		}
		return stream;

	}

	private void showNoInternetError() {
		fileDebugLog(FILE_DEBUG_TAG + "showNoInternetError", "Called");

		if (mCurErrorMode != ERROR_MODE_NO_INTERNET) {
			if (searchListView.getVisibility() == View.VISIBLE)
				searchListView.setVisibility(View.GONE);
			if (errorContainerView.getVisibility() != View.VISIBLE)
				errorContainerView.setVisibility(View.VISIBLE);
			errorIconImageView.setImageResource(R.drawable.search_no_internet);
			errorMessageTextView
					.setText(Html
							.fromHtml("<B>Couldn't connect to the internet!</B><Br>Please check your internet & try again"));

			mCurErrorMode = ERROR_MODE_NO_INTERNET;
		}

	}

	private void showEmptyResultError() {
		fileDebugLog(FILE_DEBUG_TAG + "showEmptyResultError", "Called");
		if (mCurErrorMode != ERROR_MODE_EMPTY_RESULT) {
			if (searchListView.getVisibility() == View.VISIBLE)
				searchListView.setVisibility(View.GONE);
			if (errorContainerView.getVisibility() != View.VISIBLE)
				errorContainerView.setVisibility(View.VISIBLE);
			errorIconImageView.setImageResource(R.drawable.search_empty_result);
			errorMessageTextView.setText(Html.fromHtml("No results found!"));

			mCurErrorMode = ERROR_MODE_EMPTY_RESULT;
		}

	}

	private void hideError() {
		if (mCurErrorMode != ERROR_MODE_NONE) {
			if (searchListView.getVisibility() != View.VISIBLE)
				searchListView.setVisibility(View.VISIBLE);
			errorContainerView.setVisibility(View.GONE);
			mCurErrorMode = ERROR_MODE_NONE;
		}
	}

	@Override
	public void onMoreClicked(String categoryName) {
		fileDebugLog(FILE_DEBUG_TAG + "onMoreClicked", "Called");
		if(AppConfig.DEBUG) {
			Log.d("SA", "see more clicked");
		}
		searchEditText.setText(categoryName);
		searchEditText.setSelection(categoryName.length());
	}

	@Override
	public void onBackPressed() {
		fileDebugLog(FILE_DEBUG_TAG + "onBackPressed", "Called");
		if (searchEditText.getText().toString().isEmpty())
			super.onBackPressed();
		else
			clearSearch();

	}

	private void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}

	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		// TODO Auto-generated method stub
		Log.d("search follow111", userId);
		for (CategoryCeleb cc : categoryCelebs) {
			for (UniversalUser user : cc.getUsers()) {
				if (user.getId().equals(userId)) {
					user.setIs_following(isFollowed);
					if (isPaused)
						categoryCelebAdapter.notifyDataSetChanged();
					Log.d("search follow111", user.getFirst_name());
					break;
				}
			}

		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		isPaused = true;
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		for (CategoryCeleb cc : categoryCelebs) {
			for (UniversalUser user : cc.getUsers()) {
				UniversalActionListenerCollection.getInstance()
						.addUserFollowListener(this, user.getId());
			}
		}

	}
}
