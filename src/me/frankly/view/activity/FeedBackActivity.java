package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.view.fragment.FeedBackQuestionFragment;
import me.frankly.view.fragment.FeedbackFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FeedBackActivity extends BaseActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.feedback_frame, new FeedBackQuestionFragment()).commit();
		/*FeedBackQuestionFragment mFeedBackDialogFrag = new FeedBackQuestionFragment();
		mFeedBackDialogFrag.show(getSupportFragmentManager(), "random tag");*/
	}
}
