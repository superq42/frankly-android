package me.frankly.view.activity;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import me.frankly.R;
import me.frankly.adapter.ViewPagerAdapter;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.FranklyApplication;
import me.frankly.exception.DataNotFoundException;
import me.frankly.listener.RequestListener;
import me.frankly.model.Credentials;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.LocationObject;
import me.frankly.model.newmodel.NotificationModel;
import me.frankly.model.newmodel.PriviledgedUser;
import me.frankly.model.newmodel.SinglePriviledgedUserContainer;
import me.frankly.model.newmodel.TwitterInfoContainer;
import me.frankly.model.response.LoginResponse;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PermissionUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.ShareUtils;
import me.frankly.util.TrackingHelper;
import me.frankly.view.fragment.SoftUpdateFragment;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.facebook.Request;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.viewpagerindicator.CirclePageIndicator;

public class LauncherActivity extends BaseActivity implements
		GraphUserCallback, OnClickListener, RequestListener,
		ConnectionCallbacks, OnConnectionFailedListener,
		OnBackStackChangedListener, OnPageChangeListener {
	// /////////////////////////////////////////////////////////
	private ViewPager _mViewPager;
	private ViewPagerAdapter _adapter;
	private CirclePageIndicator mIndicator;
	// ////////////////////////////////////////////////////////////
	private static final int REQ_CODE_REGISTER_USER = 111;
	private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
	public static final String KEY_IS_NEW_USER = "is_new_user";
	private String mAuthSource;
	private LoginButton fbLoginButton;
	private SignInButton gPlusSignInButton;
	private LinearLayout llTwitterPresent, llTwitterAbsent;
	private View twitterConnectView, signUpView, tvLogin;
	private UiLifecycleHelper fbUiLifecycleHelper;
	private Session.StatusCallback fbStatusCallback = new StatusCallback() {

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onFacebookSessionStateChanged(session, state, exception);

		}
	};
	private Credentials authCredentials;
	private GraphUser userFacebookData = null;
	private ProgressDialog mConnectionProgressDialog;
	private EditableProfileObject tempProfileObject;

	private GoogleApiClient googleApiClient;
	private ConnectionResult mConnectionResult;
	// private SigninFragment.LoginFailListener mLoginFailListener = null;
	private boolean isNewUser = false;
	private GraphUser mFBUser;

	private boolean mIsInstallTracked;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#Ninja LA:";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		fileDebugLog(FILE_DEBUG_TAG + " onCreateView", "Called");

		Controller.getLocaleFromGPS(this);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_login);

		mIsInstallTracked = PrefUtils.isInstallTracked();
		if (!mIsInstallTracked) {
			TrackingHelper.onActivityCreate(this);
		}

		Controller.registerGAEvent("NewLoginActivity");

		if (Controller.isUserLoggedIn()) {

			Controller.getInitProfile(this);
			isNewUser = !PrefUtils.isPeopleFollowed();
			navigateToMainActivity();
		}
		findAllViews();
		configureLoginButtons();
		setListeners();
		mConnectionProgressDialog = new ProgressDialog(this,
				ProgressDialog.THEME_HOLO_LIGHT);
		mConnectionProgressDialog.setMessage("Please wait...");
		this.fbUiLifecycleHelper = new UiLifecycleHelper(this,
				this.fbStatusCallback);
		fbUiLifecycleHelper.onCreate(savedInstanceState);

		this.googleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, Plus.PlusOptions.builder().build())
				.addScope(Plus.SCOPE_PLUS_LOGIN)
				.addScope(new Scope("profile"))
				.addScope(new Scope("https://www.googleapis.com/auth/plus.me"))
				.addScope(
						new Scope(
								"https://www.googleapis.com/auth/plus.profile.emails.read"))
				.build();
		// ////////////////////////////////////////////////////////////
		_adapter = new ViewPagerAdapter(getSupportFragmentManager());

		_mViewPager = (ViewPager) findViewById(R.id.pager);
		_mViewPager.setAdapter(_adapter);
		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(_mViewPager);
		mIndicator.setOnPageChangeListener(this);
		// //////////////////////////////////////////////////////////////////

		// Register App launch event from launcher icon
		MixpanelUtils.sendGenericEvent(null, null, null,
				Constant.ScreenSpecs.SCREEN_SPLASH, MixpanelUtils.APP_LAUNCHED,
				this);

		try {
			PackageInfo info = getPackageManager().getPackageInfo("me.frankly",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();

		}

		/*
		 * Apps Flyer install tracker "probably" works by setting a shared pref
		 * after the first broadcast of an install Thereafter based on the share
		 * pref value it does not register a install even if
		 * registerInstallAppsFlyer is called.
		 * 
		 * It takes into account that install broadcast event will happen only
		 * once even if the pref utils are cleared by the user through phone
		 * settings or otherwise through in-app logout option
		 * 
		 * Setting an additional condition to not call the event in case the
		 * user is already registered
		 */
		if (PrefUtils.getCurrentUserAsObject() == null)
			registerInstallAppsFlyer();
	}

	private void registerInstallAppsFlyer() {
		AppsFlyerLib.setAppsFlyerKey(FranklyApplication.APPS_FLYER_KEY);
		AppsFlyerLib.setUseHTTPFalback(true);
		AppsFlyerLib.sendTracking(getApplicationContext());

	}

	private void checkTwitterPresence() {

		if (ShareUtils.findAppByPackageName(ShareUtils.PKG_TWITTER, this) != null) {
			llTwitterPresent.setVisibility(View.VISIBLE);
			llTwitterAbsent.setVisibility(View.GONE);
			gPlusSignInButton = (SignInButton) findViewById(R.id.iv_login_gplus);
			fbLoginButton = (LoginButton) findViewById(R.id.iv_login_fb);
			findViewById(R.id.iv_login_fb_stub).setVisibility(View.VISIBLE);
		} else {
			llTwitterAbsent.setVisibility(View.VISIBLE);
			llTwitterPresent.setVisibility(View.GONE);
			fbLoginButton = (LoginButton) findViewById(R.id.iv_login_fb_new);
			gPlusSignInButton = (SignInButton) findViewById(R.id.iv_login_gplus_new);
			findViewById(R.id.iv_login_fb_stub).setVisibility(View.GONE);
		}
	}

	private void findAllViews() {
		twitterConnectView = findViewById(R.id.iv_login_twitter);

		signUpView = findViewById(R.id.tv_signup);
		tvLogin = findViewById(R.id.tv_login);
		llTwitterPresent = (LinearLayout) findViewById(R.id.ll_non_fb_login);
		llTwitterAbsent = (LinearLayout) findViewById(R.id.ll_fb_gplus_login);
		checkTwitterPresence();
	}

	private void configureLoginButtons() {
		configureFacebookLoginButton();
	}

	private void configureFacebookLoginButton() {
		this.fbLoginButton.setReadPermissions(PermissionUtils
				.getFacebookPermission(0));
		fbLoginButton.setBackgroundResource(R.drawable.btn_connect_with_fb);
		fbLoginButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

	}

	private void setListeners() {
		twitterConnectView.setOnClickListener(this);
		gPlusSignInButton.setOnClickListener(this);

		signUpView.setOnClickListener(this);
		tvLogin.setOnClickListener(this);
		getSupportFragmentManager().addOnBackStackChangedListener(this);
		findViewById(R.id.iv_login_fb_stub).setOnClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d("login", "newloginactivity started");

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		this.fbUiLifecycleHelper.onDestroy();
		Log.d("login", "newloginactivity destroyed");
	}

	@Override
	public void onPause() {
		super.onPause();
		this.fbUiLifecycleHelper.onPause();
		Log.d("login", "newloginactivity paused");
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!mIsInstallTracked) {
			TrackingHelper.onActivityResume(this);
			mIsInstallTracked = true;
			PrefUtils.setInstallTracked(true);
		}
		this.fbUiLifecycleHelper.onResume();
		Log.d("login", "newloginactivity resumd");
	}

	@Override
	public void onSaveInstanceState(Bundle paramBundle) {
		super.onSaveInstanceState(paramBundle);
		Log.d("login", "newloginactivity onSaveInstanceState");
		this.fbUiLifecycleHelper.onSaveInstanceState(paramBundle);
	}

	@Override
	public void onStop() {
		super.onStop();
		if (this.googleApiClient.isConnected())
			this.googleApiClient.disconnect();
		Log.d("login", "newloginactivity resumd");

	}

	private void doSignInToGoogle() {

		mAuthSource = Constant.AuthSource.SOURCE_GOOGLE;

		Log.d("google", "g+ connecting");
		if (!googleApiClient.isConnected()) {
			if (mConnectionResult == null) {
				mConnectionProgressDialog.show();
				googleApiClient.connect();
			} else {
				try {

					mConnectionResult.startResolutionForResult(this,
							REQUEST_CODE_RESOLVE_ERR);
				} catch (SendIntentException e) {
					mConnectionResult = null;
					googleApiClient.connect();
				}
			}
		}
	}

	private void onFacebookSessionStateChanged(Session paramSession,
			SessionState paramSessionState, Exception paramException) {
		if (paramSessionState.isOpened()) {
			this.mAuthSource = Constant.AuthSource.SOURCE_FACEBOOK;

			requestUserFacebookData();

		}

		if (paramException != null)
			Log.e("check", "fb signin error" + paramException.getMessage());
	}

	private void requestUserFacebookData() {
		Request.executeBatchAsync(new Request[] { Request.newMeRequest(
				Session.getActiveSession(), this) });
	}

	@Override
	public void onCompleted(GraphUser user, Response response) {

		if (user != null) {
			mFBUser = user;
			do_loginWithFacebook(user);
		}

	}

	private void do_loginWithFacebook(GraphUser paramGraphUser) {
		Log.d("fbGraph", "User data: "
				+ paramGraphUser.getInnerJSONObject().toString());

		if (this.authCredentials == null)
			this.authCredentials = new Credentials();
		Log.d("fb", Session.getActiveSession().getAccessToken());
		this.authCredentials.setFacebook_access_token(Session
				.getActiveSession().getAccessToken());

		this.authCredentials.setFb_user_id(paramGraphUser.getId());
		this.mAuthSource = Constant.AuthSource.SOURCE_FACEBOOK;
		this.userFacebookData = paramGraphUser;
		setProfileFromFacebook();
		fbLoginButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mFBUser != null)
					do_loginWithFacebook(mFBUser);
				else
					Toast.makeText(
							LauncherActivity.this,
							"Oops, there was error logging you in.Talk to us at letstalk@frankly.me if this happens repeatedly.",
							Toast.LENGTH_LONG).show();
			}
		});

		mayBeLoginExistingUser();
	}

	@SuppressLint("SimpleDateFormat")
	private void setProfileFromFacebook() {
		if (!isFinishing())
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					mConnectionProgressDialog.show();
				}
			});

		if (userFacebookData != null) {
			try {
				if (tempProfileObject == null)
					tempProfileObject = new EditableProfileObject();

				tempProfileObject
						.setProfile_picture(Controller.getFbProfilePicUrl(userFacebookData
								.getUsername() != null ? userFacebookData
								.getUsername() : userFacebookData.getId()));
				tempProfileObject.setUsername(authCredentials.getUsername());
				if (userFacebookData.getFirstName() != null)
					tempProfileObject.setFirst_name(userFacebookData
							.getFirstName());
				if (userFacebookData.getLastName() != null)
					tempProfileObject.setLast_name(userFacebookData
							.getLastName());
				if (userFacebookData.getBirthday() != null) {
					String birthdayString = userFacebookData.getBirthday();
					SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");

					Date d = f.parse(birthdayString);
					long sec = d.getTime() / 1000L;
					tempProfileObject.setDate_of_birth(sec);
				}
				if (userFacebookData.asMap().get("bio") != null)

					tempProfileObject.setBio(Controller
							.getValidAboutMe(userFacebookData.asMap()
									.get("bio").toString()));
				if (userFacebookData.asMap().get("gender") != null) {
					String fbGender = userFacebookData.asMap().get("gender")
							.toString();
					if (fbGender.equalsIgnoreCase("male"))
						tempProfileObject.setGender("M");
					else if (fbGender.equalsIgnoreCase("female"))
						tempProfileObject.setGender("F");
				}
				try {
					if (userFacebookData.getInnerJSONObject()
							.getJSONObject("location").getString("name") != null) {

						LocationObject l = new LocationObject();

						l.setLocation_name(userFacebookData
								.getInnerJSONObject().getJSONObject("location")
								.getString("name"));
						tempProfileObject.setPermanent_location(l);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

				final RequestListener fbCoverGetListener = new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode, String message) {
					}

					@Override
					public void onRequestCompleted(Object responseObject) {
						tempProfileObject
								.setCover_picture((String) responseObject);
						Log.e("fb",
								"fb cover url:"
										+ tempProfileObject.getCover_picture());

					}
				};

				Controller
						.getFBCoverUrl(
								this,
								userFacebookData.getUsername() != null ? userFacebookData
										.getUsername() : userFacebookData
										.getId(), fbCoverGetListener);

			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

	private void mayBeLoginExistingUser() {

		try {

			Controller.requestLogin(this, this.authCredentials,
					this.mAuthSource, this);
			if (!isFinishing())
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mConnectionProgressDialog
								.setMessage("Signing into Frankly.me");
						if (!mConnectionProgressDialog.isShowing())
							mConnectionProgressDialog.show();
					}
				});
			return;
		} catch (DataNotFoundException localDataNotFoundException) {
			localDataNotFoundException.printStackTrace();
			return;
		} catch (IllegalStateException localIllegalStateException) {
			localIllegalStateException.printStackTrace();
		}
	}

	@Override
	public void onRequestStarted() {
	}

	@Override
	public void onRequestCompleted(Object paramObject) {
		if (!isFinishing())
			runOnUiThread(new Runnable() {
				public void run() {
					mConnectionProgressDialog.hide();
				}
			});
		Log.d("check", "response in login activity" + (String) paramObject);

		LoginResponse localLoginResponse = (LoginResponse) JsonUtils.objectify(
				(String) paramObject, LoginResponse.class);
		this.authCredentials.setAccess_token(localLoginResponse
				.getAccess_token());
		this.authCredentials.setId(localLoginResponse.getId());
		this.authCredentials.setUsername(localLoginResponse.getUsername());
		isNewUser = localLoginResponse.isNew_user();

		/*
		 * Author : Tanay A temporary solution to the celebirty profile views
		 */

		PrefUtils.setUserCelebrityStatus(localLoginResponse.getUserType());

		if (Controller.saveCredentials(this, this.authCredentials)) {
			MixpanelUtils.sendLoginSuccessEvent(mAuthSource, isNewUser,
					authCredentials.getId(), this);
			navigateToMainActivity();
		}
	}

	@Override
	public void onRequestError(final int errorCode, final String message) {
		Log.e("Ashish", "hide progress");
		if (!isFinishing())
			runOnUiThread(new Runnable() {
				public void run() {
					mConnectionProgressDialog.hide();
					JSONObject object;
					String errorText = "Some unknown error has occured. Please try later";
					try {
						object = new JSONObject(message);
						errorText = object.getString("message");
					} catch (JSONException e) {

						e.printStackTrace();
					}
					/*
					 * if (errorCode == ErrorCode.USER_NOT_FOUND.getErrorCode()
					 * && !(mAuthSource == Constant.AuthSource.SOURCE_EMAIL ||
					 * mAuthSource == Constant.AuthSource.SOURCE_USERNAME)) {
					 * registerUser(); } else
					 */
					Toast.makeText(LauncherActivity.this, errorText,
							Toast.LENGTH_SHORT).show();
				}
			});
	}

	protected void registerUser(int type) {

		Intent registerIntent = new Intent(this, RegisterUserActivity.class);
		if (type == 1)
			registerIntent.putExtra(RegisterUserActivity.KEY_AUTH_SOURCE,
					"signup");
		else if (type == 2)
			registerIntent.putExtra(RegisterUserActivity.KEY_AUTH_SOURCE,
					"login");
		Log.e("mAuthSource", "--" + mAuthSource);
		// registerIntent.putExtra(RegisterUserActivity.KEY_AUTH_CREDENTIALS,
		// JsonUtils.jsonify(authCredentials)); // *
		// registerIntent.putExtra(RegisterUserActivity.KEY_SOCIAL_PROFILE,
		// JsonUtils.jsonify(tempProfileObject));
		startActivityForResult(registerIntent, REQ_CODE_REGISTER_USER);
		overridePendingTransition(R.drawable.pull_up_from_bottom,
				R.drawable.hold);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_login_fb_stub:
		case R.id.iv_login_fb_new:
			if (Controller.isNetworkConnected(this)) {
				fbLoginButton.performClick();
				MixpanelUtils.sendLoginAttemptEvent(
						Constant.AuthSource.SOURCE_FACEBOOK, this);
			} else
				Toast.makeText(this, "No internet connection!",
						Toast.LENGTH_SHORT).show();

			break;
		case R.id.iv_login_gplus_new:
		case R.id.iv_login_gplus:
			if (Controller.isNetworkConnected(this)) {
				doSignInToGoogle();
				MixpanelUtils.sendLoginAttemptEvent(
						Constant.AuthSource.SOURCE_GOOGLE, this);
			} else
				Toast.makeText(this, "No internet connection!",
						Toast.LENGTH_SHORT).show();

			break;
		case R.id.iv_login_twitter:
			if (Controller.isNetworkConnected(this)) {
				MixpanelUtils.sendLoginAttemptEvent(
						Constant.AuthSource.SOURCE_TWITTER, this);
				doSignInToTwitter();
			} else
				Toast.makeText(this, "No internet connection!",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.tv_signup:
			registerUser(1);
			MixpanelUtils.sendGenericEvent(null, null, null,
					Constant.ScreenSpecs.SCREEN_SPLASH,
					MixpanelUtils.NAVIGATION_SIGNUP_SCREEN, this);
			break;
		case R.id.tv_login:
			registerUser(2);
			MixpanelUtils.sendGenericEvent(null, null, null,
					Constant.ScreenSpecs.SCREEN_SPLASH,
					MixpanelUtils.NAVIGATION_LOGIN_SCREEN, this);
			break;
		}
	}

	private void doSignInToTwitter() {

		Intent twitterLoginIntent = new Intent(this, TwitterLoginActivity.class);
		startActivityForResult(twitterLoginIntent,
				TwitterLoginActivity.REQUEST_CODE_TWITTER_LOGIN);
	}

	@Override
	protected void onActivityResult(int reqCode, int resCode, Intent data) {
		Log.e("Ashish", "Back to New Login1");
		if (reqCode == REQ_CODE_REGISTER_USER) {
			Log.e("Ashish", "Back to New Login2");
			if (resCode == RESULT_OK) {
				if (data.getBooleanExtra(Constant.Key.LOGIN_FACEBOOK_FRANKLY,
						false)) {
					fbLoginButton.performClick();
					MixpanelUtils.sendLoginAttemptEvent(
							Constant.AuthSource.SOURCE_FACEBOOK, this);
				} else if (data.getBooleanExtra(
						Constant.Key.LOGIN_GOOGLE_PLUS_FRANKLY, false)) {
					doSignInToGoogle();
					MixpanelUtils.sendLoginAttemptEvent(
							Constant.AuthSource.SOURCE_GOOGLE, this);
				} else if (data.getBooleanExtra(
						Constant.Key.LOGIN_TWITTER_FRANKLY, false)) {
					doSignInToTwitter();
					MixpanelUtils.sendLoginAttemptEvent(
							Constant.AuthSource.SOURCE_TWITTER, this);
				} else if (data.getBooleanExtra(
						Constant.Key.LOGIN_EMAIL_FRANKLY, false)) {
					onUsernameAndPasswordSelected(
							data.getStringExtra("username"),
							data.getStringExtra("password"));
					MixpanelUtils.sendLoginAttemptEvent(
							Constant.AuthSource.SOURCE_EMAIL, this);
				} else {

					isNewUser = true;
					MixpanelUtils.sendLoginSuccessEvent(
							Constant.AuthSource.SOURCE_EMAIL, isNewUser,
							PrefUtils.getUserID(), this);
					fetchProfileAndNavigateToMainActivity();
				}
			}

		} else if (reqCode == REQUEST_CODE_RESOLVE_ERR
				&& resCode == Activity.RESULT_OK) {
			mConnectionResult = null;
			googleApiClient.connect();
		} else if (reqCode == TwitterLoginActivity.REQUEST_CODE_TWITTER_LOGIN
				&& resCode == Activity.RESULT_OK) {
			String twitterInfo = data.getExtras().getString("twitter_info");
			if (twitterInfo != null) {
				Log.d("check", "TwitterInfo in fragment:" + twitterInfo);
				doLoginWithTwitter(twitterInfo);
			}
		}
		super.onActivityResult(reqCode, resCode, data);
		fbUiLifecycleHelper.onActivityResult(reqCode, resCode, data);
	}

	private void fetchProfileAndNavigateToMainActivity() {
		Log.e("Ashish", "Back to New Login");
		Controller.requestProfile(this, PrefUtils.getUserID(),
				initProfileListener);
	}

	private RequestListener initProfileListener = new RequestListener() {

		@Override
		public void onRequestStarted() {
		}

		@Override
		public void onRequestError(int errorCode, final String message) {
			if (!isFinishing())
				LauncherActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						Toast.makeText(LauncherActivity.this, message,
								Toast.LENGTH_SHORT).show();
						navigateToMainActivity();

					}
				});
		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			Log.e("Ashish", "Back to New Login onRequestCompleted");
			Log.d("slidingmenu", "Jingle Bells Jingle Bells "
					+ (String) responseObject);

			final PriviledgedUser mResponse = ((SinglePriviledgedUserContainer) JsonUtils
					.objectify((String) responseObject,
							SinglePriviledgedUserContainer.class)).getUser();
			if (!isFinishing())
				LauncherActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						PrefUtils.saveCurrentUserAsJson(JsonUtils
								.jsonify(mResponse));
						MixpanelUtils
								.setMixpanelPeopleDetails(getApplicationContext());
						navigateToMainActivity();

					}
				});
		}
	};

	private void navigateToMainActivity() {
		fileDebugLog(FILE_DEBUG_TAG + " navigateToMainActivity", "Called");

		if (!isFinishing())
			runOnUiThread(new Runnable() {
				public void run() {

					Uri data = null;
					if (mConnectionProgressDialog != null
							&& mConnectionProgressDialog.isShowing()) {
						mConnectionProgressDialog.dismiss();
					}
					String action = getIntent().getAction();
					Log.i("sumit", " Action " + action);
					if (action != null
							&& action
									.equals(Constant.Notification.ACTION_FROM_NOTIFICATION)) {
						String strObj = getIntent().getStringExtra(
								Constant.Notification.KEY_NOTIF_OBJECT);
						// Log.i("sumit", " Obj   " + strObj);
						if (strObj != null) {
							NotificationModel notification = (NotificationModel) JsonUtils
									.objectify(strObj, NotificationModel.class);
							data = Uri.parse(notification.getDeeplink());

						}
					} else {
						data = getIntent().getData();
						Log.d("LA", "" + data);
					}
					Intent intent;
					if (isNewUser) {
						intent = new Intent(LauncherActivity.this,
								FollowCelebsActivity.class);
						fileDebugLog(FILE_DEBUG_TAG
								+ "Opening FollowCelebsActivity", "");
					} else {
						intent = new Intent(LauncherActivity.this,
								FranklyHomeActivity.class);
						PrefUtils.setPeopleFollowed();
					}

					if (isNewUser) {
						intent.putExtra(KEY_IS_NEW_USER, isNewUser);
						PrefUtils.setisNewUserVisitedProfile(true);
					}
					if (data != null) {
						intent.setData(data);
						Log.d("LA", "data set");
					}
					finish();
					if (action != null
							&& (action
									.equals(Constant.Notification.ACTION_FROM_NOTIFICATION) || action
									.equals("android.intent.action.VIEW"))) {
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						Log.d("LA","flag set");
					}

					Log.d("Feedback",
							"app open count : " + PrefUtils.getAppOpenCount());
					PrefUtils.setAppOpenCount(PrefUtils.getAppOpenCount() + 1);
					startActivity(intent);
				}

			});

	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void onConnected(Bundle connectionHint) {
		Log.d("google", "g+ connected");
		Person currentUser;
		if (Plus.PeopleApi.getCurrentPerson(this.googleApiClient) != null) {
			currentUser = Plus.PeopleApi.getCurrentPerson(this.googleApiClient);
			final String email = Plus.AccountApi
					.getAccountName(this.googleApiClient);
			final String gplus_id = currentUser.getId();
			Person.Name localName = currentUser.getName();
			currentUser.getImage().getUrl();
			new Thread(new Runnable() {
				public void run() {
					try {

						String at = GoogleAuthUtil
								.getToken(
										LauncherActivity.this,
										email,
										"oauth2:https://www.googleapis.com/auth/plus.me"
												+ " https://www.googleapis.com/auth/plus.profile.emails.read");
						Log.d("gplus", "Gplus X_Token: " + at);
						do_loginWithGoogle(gplus_id, email, at);
						return;
					} catch (UserRecoverableAuthException localUserRecoverableAuthException) {
						localUserRecoverableAuthException.printStackTrace();
						return;
					} catch (IOException localIOException) {
						localIOException.printStackTrace();
						return;
					} catch (GoogleAuthException localGoogleAuthException) {
						localGoogleAuthException.printStackTrace();
					}
				}
			}).start();
			Toast.makeText(this, email + " is connected.", Toast.LENGTH_SHORT)
					.show();
			if (this.tempProfileObject == null)
				this.tempProfileObject = new EditableProfileObject();
			if (currentUser.hasName()) {
				this.tempProfileObject.setFirst_name(localName.getGivenName());
				this.tempProfileObject.setLast_name(localName.getFamilyName());
			}
			if (currentUser.hasAboutMe())
				this.tempProfileObject.setBio(Controller
						.getValidAboutMe(currentUser.getAboutMe()));
			if (currentUser.hasGender()) {
				int i = currentUser.getGender();
				if (i == Person.Gender.MALE)
					tempProfileObject.setGender("M");
				else
					tempProfileObject.setGender("M");
			}

			if (currentUser.hasCover())
				tempProfileObject.setCover_picture(currentUser.getCover()
						.getCoverPhoto().getUrl());
			if (currentUser.hasImage())
				tempProfileObject.setProfile_picture(currentUser.getImage()
						.getUrl());
			String str3;
			SimpleDateFormat localSimpleDateFormat;
			if (currentUser.hasBirthday()) {
				str3 = currentUser.getBirthday();
				localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-DD");
				try {
					long l = localSimpleDateFormat.parse(str3).getTime() / 1000L;
					this.tempProfileObject.setDate_of_birth(l);
					LocationObject locationObject = new LocationObject();
					locationObject.setLocation_name(currentUser
							.getCurrentLocation());
					this.tempProfileObject
							.setPermanent_location(locationObject);

				} catch (ParseException localParseException) {
					while (true)
						localParseException.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onConnectionSuspended(int cause) {
		googleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.d("google", "g+ connection failed");
		if ((this.mConnectionProgressDialog.isShowing())
				&& (result.hasResolution())) {
			try {
				result.startResolutionForResult(this, 9000);
				this.mConnectionResult = result;
				return;
			} catch (IntentSender.SendIntentException localSendIntentException) {
				while (true)
					this.googleApiClient.connect();
			}
		}
	}

	private void do_loginWithGoogle(String paramString1, String paramString2,
			String paramString3) {
		if (this.authCredentials == null)
			this.authCredentials = new Credentials();
		this.authCredentials.setGoogle_plus_access_token(paramString3);
		this.authCredentials.setGplus_user_id(paramString1);
		this.mAuthSource = Constant.AuthSource.SOURCE_GOOGLE;
		mayBeLoginExistingUser();
	}

	private void doLoginWithTwitter(String twitterInfo) {

		TwitterInfoContainer twitterInfoContainer = (TwitterInfoContainer) JsonUtils
				.objectify(twitterInfo, TwitterInfoContainer.class);
		this.mAuthSource = Constant.AuthSource.SOURCE_TWITTER;
		if (this.authCredentials == null)
			authCredentials = new Credentials();
		authCredentials.setTwitter_access_token(twitterInfoContainer
				.getTwitterAccessToken().getToken());
		authCredentials.setTwitter_token_secret(twitterInfoContainer
				.getTwitterAccessToken().getTokenSecret());
		authCredentials.setTwitter_user_id(String.valueOf(twitterInfoContainer
				.getTwitterAccessToken().getUserId()));
		tempProfileObject = twitterInfoContainer.getTwitterUser();
		if (twitterInfoContainer.getTwitterUser().getCover_picture() != null)
			tempProfileObject.setCover_picture(twitterInfoContainer
					.getTwitterUser().getCover_picture());
		if (twitterInfoContainer.getTwitterUser().getProfile_picture() != null)
			tempProfileObject.setProfile_picture(twitterInfoContainer
					.getTwitterUser().getProfile_picture());
		// setProfileFromTwitter(twitterInfoContainer.getTwitterUser());
		mayBeLoginExistingUser();
	}

	public void onEmailAndPasswordSelected(String nowEmail, String nowPassword) {
		if (authCredentials == null)
			authCredentials = new Credentials();
		mAuthSource = Constant.AuthSource.SOURCE_EMAIL;
		authCredentials.setEmail(nowEmail);
		authCredentials.setPassword(nowPassword);
		mayBeLoginExistingUser();
	}

	public void onUsernameAndPasswordSelected(String nowUsername,
			String nowPassword) {
		if (authCredentials == null)
			authCredentials = new Credentials();
		mAuthSource = Constant.AuthSource.SOURCE_USERNAME;
		authCredentials.setUsername(nowUsername);
		authCredentials.setPassword(nowPassword);
		mayBeLoginExistingUser();
	}

	@Override
	public void onBackStackChanged() {

	}

	/*
	 * public void setLoginFailListener( SigninFragment.LoginFailListener
	 * loginFailListener) { this.mLoginFailListener = loginFailListener; }
	 * 
	 * public void removeLoginFailListener() { this.mLoginFailListener = null; }
	 */

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {
		Log.i("auto_swipe", "current position: " + arg0);
		MixpanelUtils.sendSlideshowEvent(arg0, this);
	}

	public void registerMixpanelEvent(String event_name, String source) {
		MixpanelUtils.sendGenericEvent(null, null, null, source, event_name,
				this);
	}

	private void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}
}
