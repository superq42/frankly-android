package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.Credentials;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.response.LoginResponse;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.view.fragment.LogInFragment;
import me.frankly.view.fragment.SignUpFragment;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

/**
 * Activity to register a new user onto the system.. Accepts the Authorisation
 * source and optional social data in intent.
 * 
 * @author abhishek_inoxapps
 * 
 */
public class RegisterUserActivity extends BaseActivity {

	/**
	 * Keys to PUT/GET data in/from intent.
	 */
	public static final String KEY_AUTH_SOURCE = "auth_source";
	public static final String KEY_AUTH_CREDENTIALS = "auth_credentials";
	public static final String KEY_SOCIAL_PROFILE = "social_profile";
	@SuppressWarnings("unused")
	private LogInFragment.LoginFailListener mLoginFailListener = null;

	private String mAuthSource;
	/* Used for analytics */
	public String mAuthSourceString;
	private Credentials nowCredentials;
	private EditableProfileObject nowProfileObject;
	private FragmentManager fm;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_user);
		Controller.registerGAEvent("RegisterUserActivity");
		
		Intent i = getIntent();
		fm = getSupportFragmentManager();
		setUpEnvironment(i);
		beginRegistration();

	}

	/**
	 * Extracts the data from input intent and sets up the registration
	 * environment accordingly.
	 * 
	 * @param intent
	 */
	private void setUpEnvironment(Intent intent) {
		mAuthSource = intent.getStringExtra(KEY_AUTH_SOURCE);
		if(mAuthSource.equalsIgnoreCase("signup"))
			mAuthSource = "email";
		else if (mAuthSource.equalsIgnoreCase("login"))
			mAuthSource = "login";
		// if (mAuthSource != Controller.SOURCE_EMAIL && mAuthSource !=
		// Controller.SOURCE_USERNAME) {
		String credString = intent.getStringExtra(KEY_AUTH_CREDENTIALS);
		String profString = intent.getStringExtra(KEY_SOCIAL_PROFILE);
		if (credString != null)
			nowCredentials = (Credentials) JsonUtils.objectify(credString,
					Credentials.class);
		else
			nowCredentials = new Credentials();
		if (profString != null)

			nowProfileObject = (EditableProfileObject) JsonUtils.objectify(
					profString, EditableProfileObject.class);
		else
			nowProfileObject = new EditableProfileObject();

		if (mAuthSource.equalsIgnoreCase(Constant.AuthSource.SOURCE_EMAIL)
				|| mAuthSource
						.equalsIgnoreCase(Constant.AuthSource.SOURCE_USERNAME)) {
			mAuthSourceString = "email";
		} else if (mAuthSource
				.equalsIgnoreCase(Constant.AuthSource.SOURCE_FACEBOOK)) {
			mAuthSourceString = "facebook";
		} else if (mAuthSource
				.equalsIgnoreCase(Constant.AuthSource.SOURCE_GOOGLE)) {
			mAuthSourceString = "google plus";
		} else if (mAuthSource
				.equalsIgnoreCase(Constant.AuthSource.SOURCE_TWITTER)) {
			mAuthSourceString = "twitter";
		}

	}

	/**
	 * initiates the registration process
	 */
	private void beginRegistration() {
		showFragment(mayBeGetNextRegistrationFragment(), false);

	}

	/**
	 * Shows the given fragment onto the Frame-Layout of the activity
	 * 
	 * @param f
	 * @param addToBackStack
	 */
	private void showFragment(Fragment f, boolean addToBackStack) {

		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.realtabcontent, f);
		if (addToBackStack)
			ft.addToBackStack(null);
		ft.commit();

	}

	/**
	 * Called internally to get the sequential fragments for registration
	 * process
	 * 
	 * @return the next fragment to be shown depending on the Authorisation
	 *         Source and available data.. <b>null</b> if all data are available
	 */
	private Fragment mayBeGetNextRegistrationFragment() {
		Fragment f = null;
		Bundle args;
		if (nowCredentials.getEmail() == null) {
			if (mAuthSource == Constant.AuthSource.SOURCE_EMAIL
					|| mAuthSource == Constant.AuthSource.SOURCE_USERNAME) {
				f = new SignUpFragment();
				args = new Bundle();
				args.putBoolean(SignUpFragment.KEY_SHOW_PASSWORD_FEILD,
						true);
				f.setArguments(args);
			}
			else if (mAuthSource.equalsIgnoreCase("login")){
				f = new LogInFragment();
			}
		}
		return f;
	}



	/**
	 * 
	 * 
	 * @param email
	 * @param pwd
	 */
	public void onEmailAndPasswordSelected(String email, String pwd,
			String phone_no, String fullname) {
		nowCredentials.setEmail(email);
		nowCredentials.setPassword(pwd);
		nowCredentials.setPhone_num(phone_no);
		nowCredentials.setFullname(fullname);
		Log.d("Signup", "Set the fields");
		proceedRegistration();
	}

	public void clearEmailAndPassword() {
		nowCredentials.setEmail(null);
		nowCredentials.setPassword(null);
		nowCredentials.setPhone_num(null);
		nowCredentials.setFullname(null);
	}

	/**
	 * 
	 * 
	 * @param username
	 */
	public void onUsernameSelected(String username) {
		nowProfileObject.setUsername(username);
		nowCredentials.setUsername(username);
		proceedRegistration();
	}

	public void clearUsername() {
		nowProfileObject.setUsername(null);
		nowCredentials.setUsername(null);

	}

	/**
	 * 
	 * 
	 * @param picPath
	 * @param bio
	 */
	public void onPicAndBioSelected(String picPath, String bio) {
		nowProfileObject.setProfile_picture(picPath);
		nowProfileObject.setBio(bio);
		proceedRegistration();
	}

	public void clearPicAndBio() {
		nowProfileObject.setProfile_picture(null);
		nowProfileObject.setBio(null);
	}

	/**
	 * performs the next registration step
	 */
	private void proceedRegistration() {
		Fragment nextFragment = mayBeGetNextRegistrationFragment();
		if (nextFragment != null)
			showFragment(nextFragment, true);
		else
			finalizeRegistration();

	}

	/**
	 * Called when all the data are available to register the user on System
	 */
	private void finalizeRegistration() {
		Log.d("Signup", "In finalize");
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				showProgressDialog("Please Wait...");
			}
		});
		
		if (mAuthSource.equalsIgnoreCase("login"))
			mAuthSource = "email";
		Controller.requestRegistration(this, mAuthSource, nowCredentials,
				registrationRequestListener);

	}

	RequestListener registrationRequestListener = new RequestListener() {

		@Override
		public void onRequestStarted() {
		}

		@Override
		public void onRequestError(int errorCode, final String message) {

			hideProgressDialog();

			if (!isFinishing()) {
				try {
					JSONObject object = new JSONObject(message);
					String errors = "";
					if (object.has("email"))
						errors = (String) object.get("email");
					if (object.has("phone_number"))
						errors += " " + (String) object.get("phone_number");
					if (object.has("message"))
						errors += " " + (String) object.get("message");

					final String errorMessage = errors;
					runOnUiThread(new Runnable() {

						@Override
						public void run() {

							Toast.makeText(RegisterUserActivity.this,
									errorMessage, Toast.LENGTH_SHORT).show();
						}
					});
				} catch (JSONException e1) {
					e1.printStackTrace();
				}

			}

		}

		@Override
		public void onRequestCompleted(Object responseObject) {

			String str;
			LoginResponse localLoginResponse;
			str = (String) responseObject;
			Log.d("check", "response in activity: " + (String) responseObject);
			localLoginResponse = (LoginResponse) JsonUtils.objectify(str,
					LoginResponse.class);
			nowCredentials
					.setAccess_token(localLoginResponse.getAccess_token());

			if (mAuthSource.equalsIgnoreCase(Constant.AuthSource.SOURCE_EMAIL)) {
				mAuthSourceString = "email";
			} else if (mAuthSource
					.equalsIgnoreCase(Constant.AuthSource.SOURCE_FACEBOOK)) {
				mAuthSourceString = "facebook";
			} else if (mAuthSource
					.equalsIgnoreCase(Constant.AuthSource.SOURCE_GOOGLE)) {
				mAuthSourceString = "google plus";
			} else if (mAuthSource
					.equalsIgnoreCase(Constant.AuthSource.SOURCE_TWITTER)) {
				mAuthSourceString = "twitter";
			} else if (mAuthSource
					.equalsIgnoreCase(Constant.AuthSource.SOURCE_USERNAME)) {
				mAuthSourceString = "twitter";
			}

			nowCredentials.setId(localLoginResponse.getId());
			nowProfileObject.setId(localLoginResponse.getId());
			if (Controller.saveCredentials(
					RegisterUserActivity.this, nowCredentials)
					&& !isFinishing()) {

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						hideProgressDialog();
						Intent resultIntent = new Intent();
						resultIntent.putExtra(KEY_AUTH_CREDENTIALS,
								JsonUtils.jsonify(nowCredentials));
						resultIntent.putExtra(KEY_SOCIAL_PROFILE,
								JsonUtils.jsonify(nowProfileObject));

						setResult(RESULT_OK, resultIntent);
						finish();
					}
				});

			}
		}
	};

	public void onBackPressed() {
		finish();
		overridePendingTransition(R.drawable.hold,
				R.drawable.push_out_to_bottom);
	};

	private void showProgressDialog(String message) {
		if (progressDialog == null) {
			progressDialog = new ProgressDialog(this,
					ProgressDialog.THEME_HOLO_LIGHT);
			progressDialog.setCancelable(false);
		}
		progressDialog.setMessage(message);
		if (!progressDialog.isShowing())
			progressDialog.show();
	}

	private void hideProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
	}
	
	public void setLoginFailListener(
			LogInFragment.LoginFailListener loginFailListener) {
		this.mLoginFailListener = loginFailListener;
	}

	public void removeLoginFailListener() {
		this.mLoginFailListener = null;
	}
	
	
	public void registerMixpanelEvent(String event_name, String source){
		MixpanelUtils.sendGenericEvent(null,null,null,source,
				event_name, this) ;
	}
}