package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.model.newmodel.PostObject;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.view.fragment.CommentFragment;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class CommentActivity extends BaseActivity implements OnClickListener {

	private String postJson = null;
	private CommentFragment firstFragment;
	private ImageView iv_background;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

		setContentView(R.layout.activity_comment);
		iv_background = (ImageView)findViewById(R.id.act_comment_bg);
		
		Controller.registerGAEvent("CommentActivity");

		Intent intent = getIntent();

		postJson = intent.getStringExtra(Constant.Key.KEY_POST_OBJECT);
		if (postJson != null) {

			firstFragment = new CommentFragment();
			Bundle args = new Bundle();
			args.putString(Constant.Key.KEY_POST_OBJECT, postJson);
			firstFragment.setArguments(args);
			getPostThumbnail();
			// Add the fragment to the 'fragment_container' FrameLayout
			getSupportFragmentManager().beginTransaction()
					.add(R.id.fragment_container, firstFragment).commit();
		} else {
			Toast.makeText(this, "Didn't get the post id in intent",
					Toast.LENGTH_SHORT).show();
		}
		findViewById(R.id.action_bar_back_btn).setOnClickListener(this);
	}

	public void onactivityResult(Intent intent) {
		setResult(Activity.RESULT_OK, intent);
		finish();
	}

	public void updateResult() {
		Intent i = getIntent();
		PostObject mPostObject = (PostObject) JsonUtils.objectify(postJson,
				PostObject.class);
		int totalComments = firstFragment.newCommentCount;
		if (postJson != null) {
			Log.d("CommentCount",
					"initialComments" + mPostObject.getComment_count());
			totalComments = totalComments
					+ ((PostObject) JsonUtils.objectify(postJson,
							PostObject.class)).getComment_count();
		}
		Log.d("CommentCount", "totalComments : " + totalComments);
		i.putExtra(Constant.Key.COMMENT_COUNT, totalComments);
		i.putExtra(Constant.Key.COMMENT_ID, mPostObject.getClient_id());
		setResult(RESULT_OK, i);
		Log.d("CommentCount", "is i null : " + (i == null));
	}

	@Override
	public void onBackPressed() {
		Log.d("CommentCount", "back pressed event");
		updateResult();
		super.onBackPressed();
		overridePendingTransition(R.drawable.hold,
				R.drawable.push_out_to_bottom);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_bar_back_btn:
			onBackPressed();
			break;
		}

	}
	
	/**
	 * Loads the Thumbnail of the current post as background
	 */
	private void getPostThumbnail(){
		ImageLoader.getInstance().loadImage(((PostObject)JsonUtils.objectify(postJson, PostObject.class)).getAnswer().getMedia().getThumbnail_url(),
				new ImageLoadingListener() {

					@Override
					public void onLoadingStarted(String arg0, View arg1) {
					}

					@Override
					public void onLoadingFailed(String arg0, View arg1,
							FailReason arg2) {
					}

					@Override
					public void onLoadingComplete(String arg0, View arg1,
							final Bitmap bitmap) {
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
									Bitmap mBm = bitmap;
									mBm = MyUtilities.fastblur(bitmap, 10);
									iv_background.setImageBitmap(mBm);
							}
						});

					}

					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
					}
				});
	
	}
	
}
