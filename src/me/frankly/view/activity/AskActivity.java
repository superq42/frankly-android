package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.QuestionObjectHolder;
import me.frankly.model.newmodel.SingleUniversalUserContainer;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.view.fragment.AskCelebrityFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class AskActivity extends BaseActivity implements RequestListener {

	private UniversalUser user = null;
	private String userId = null;
	private String userName = null;
	private String shortId = null;
	private ImageView imageView;
	public boolean shareDeeplink = false;
	private QuestionObjectHolder shareQuestionHolder;
	public Fragment fragment;
	private QuestionObject deeplinkQuestion;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#sahil Ask.Act:";

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		fileDebugLog(FILE_DEBUG_TAG + " onCreateView", "Called");

		setContentView(R.layout.activity_ask);
		imageView = (ImageView) findViewById(R.id.act_ask_bg);

		Controller.registerGAEvent("AskActivity");

		Log.e("hemant", "oncreate called ask act");
		Intent i = getIntent();
		String questionString = getIntent().getStringExtra(
				Constant.Key.KEY_QUESTION_OBJECT);
		if (questionString != null) {
			deeplinkQuestion = (QuestionObject) JsonUtils.objectify(
					questionString, QuestionObject.class);
			user = deeplinkQuestion.getQuestion_to();
		} else if (i.hasExtra(Constant.Key.KEY_USER_OBJECT)) {
			user = (UniversalUser) JsonUtils.objectify(
					i.getStringExtra(Constant.Key.KEY_USER_OBJECT),
					UniversalUser.class);
			userId = user.getId();
		} else if (i.hasExtra(Constant.Key.KEY_USER_ID))
			userId = i.getStringExtra(Constant.Key.KEY_USER_ID);

		else if (i.hasExtra(Constant.Key.KEY_USER_NAME))
			userName = i.getStringExtra(Constant.Key.KEY_USER_NAME);
		if (i.hasExtra(Constant.Key.KEY_FROM_DEEPLINK)) {
			shareDeeplink = i.getBooleanExtra(Constant.Key.KEY_FROM_DEEPLINK,
					false);
		}
		if (user == null) {
			fetchUserInfo();
		} else
			displayUserInfo();

	}

	private void displayUserInfo() {
		userId = user.getId();
		if (user.getProfile_picture() != null) {
			ImageLoader.getInstance().loadImage(user.getProfile_picture(),
					new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String arg0, View arg1) {
						}

						@Override
						public void onLoadingFailed(String arg0, View arg1,
								FailReason arg2) {
						}

						@Override
						public void onLoadingComplete(String arg0, View arg1,
								final Bitmap bitmap) {
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									Bitmap mBm = bitmap;
									if (user.getUser_type() == UniversalUser.TYPE_CELEB) {
										mBm = MyUtilities.fastblur(bitmap, 10);
									}
									imageView.setImageBitmap(mBm);
								}
							});

						}

						@Override
						public void onLoadingCancelled(String arg0, View arg1) {
						}
					});	
		}
		Log.d("followsearch", "" + user.getUser_type());
		// if (user.getUser_type() == Constant.USER.CELEBRITY) {
		Log.e("hemant", "askceleb opened");
		fragment = new AskCelebrityFragment();
		Bundle bundle = new Bundle();
		bundle.putString(Constant.Key.KEY_USER_ID, userId);
		bundle.putString(Constant.Key.KEY_USER_OBJECT, JsonUtils.jsonify(user));
		bundle.putInt(Constant.Key.KEY_USER_TYPE, Constant.USER.CELEBRITY);
		bundle.putBoolean(Constant.Key.KEY_FROM_DEEPLINK, shareDeeplink);
		if (deeplinkQuestion != null)
			bundle.putString(Constant.Key.KEY_QUESTION_OBJECT,
					JsonUtils.jsonify(deeplinkQuestion));
		fragment.setArguments(bundle);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.activity_ask_framell, fragment).commit();

		/*
		 * } else { fragment = new AskFragment(); Bundle bundle = new Bundle();
		 * bundle.putString(Constant.Key.KEY_USER_ID, userId);
		 * bundle.putString(Constant.Key.KEY_USER_NAME, user.getFull_name());
		 * bundle.putInt(Constant.Key.KEY_USER_TYPE, Constant.USER.USER);
		 * fragment.setArguments(bundle);
		 * getSupportFragmentManager().beginTransaction()
		 * .replace(R.id.activity_ask_framell, fragment).commit(); }
		 */

	}

	
	
	private void fetchUserInfo() {
		if (userId != null)
			Controller.requestProfile(this, userId, this);
		else if (userName != null)
			Controller.requestProfileByName(userName, this, this);
//		else if (shortId != null)
//			Controller.requestQuestionByShortId(shortId, this,
//					new RequestListener() {
//
//						@Override
//						public void onRequestStarted() {
//						}
//
//						@Override
//						public void onRequestError(int errorCode, String message) {
//						}
//
//						@Override
//						public void onRequestCompleted(Object responseObject) {
//							shareQuestionHolder = (QuestionObjectHolder) JsonUtils
//									.objectify((String) responseObject,
//											QuestionObjectHolder.class);
//							shareQuestionHolder
//									.setType(QuestionObjectHolder.TYPE_SHARE_QUESTION);
//							user = shareQuestionHolder.getQuestion()
//									.getQuestion_to();
//							if (user != null)
//								displayUserInfo();
//						}
//					});

	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		SingleUniversalUserContainer container = (SingleUniversalUserContainer) JsonUtils
				.objectify((String) responseObject,
						SingleUniversalUserContainer.class);
		if (this.user == null) {
			this.user = container.getUser();
			if (!isFinishing())
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						displayUserInfo();
					}
				});
		}
	}

	@Override
	public void onRequestError(int errorCode, String message) {
	}

	public QuestionObjectHolder getShareQuestion() {
		return shareQuestionHolder;
	}

	private void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.drawable.push_to_right);
	}
}
