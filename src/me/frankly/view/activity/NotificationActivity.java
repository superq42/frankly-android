package me.frankly.view.activity;

import me.frankly.R;
import me.frankly.adapter.NotificationPagerAdapter;
import me.frankly.config.Controller;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class NotificationActivity extends BaseActivity implements
		OnPageChangeListener, OnClickListener {

	private String TAG = "NotificationActivity";
	private ViewPager mViewPager;
	private NotificationPagerAdapter mNotificationPagerAdapter;
	private int currentPage = 0;
	private TextView tv_title_left;
	private TextView tv_title_right;
	private View view_lineLeft;
	private View view_right;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_notification);
		
		Controller.registerGAEvent("NotificationActivity");
		
		mViewPager = (ViewPager) findViewById(R.id.act_notification_viewpager1);

		TextView title_text = (TextView) findViewById(R.id.action_bar_title_text);
		title_text.setText("Notifications");

		mNotificationPagerAdapter = new NotificationPagerAdapter(
				getSupportFragmentManager());
		mViewPager.setAdapter(mNotificationPagerAdapter);

		tv_title_left = (TextView) findViewById(R.id.tv_notification_left);
		tv_title_right = (TextView) findViewById(R.id.tv_notification_right);
		tv_title_left.setOnClickListener(this);
		tv_title_right.setOnClickListener(this);
		view_lineLeft = findViewById(R.id.view_notification_left);
		view_right = findViewById(R.id.view_notification_right);

		mViewPager.setOnPageChangeListener(this);
		ImageView imgBack = (ImageView) findViewById(R.id.action_bar_back_btn);
		imgBack.setOnClickListener(this);

	}

	@Override
	public void onPageScrollStateChanged(int position) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int position) {
		Log.i(TAG, "onPageSelected " + position);
		currentPage = position;
		if (currentPage == 0) {
			tv_title_left.setTextColor(Color.parseColor("#ea6153"));
			view_lineLeft.setVisibility(View.VISIBLE);
			tv_title_right.setTextColor(Color.parseColor("#ffffff"));
			view_right.setVisibility(View.INVISIBLE);
		} else if (currentPage == 1) {
			tv_title_right.setTextColor(Color.parseColor("#ea6153"));
			view_right.setVisibility(View.VISIBLE);
			tv_title_left.setTextColor(Color.parseColor("#ffffff"));
			view_lineLeft.setVisibility(View.INVISIBLE);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_bar_back_btn:
			finish();
			break;
		case R.id.tv_notification_left:
			mViewPager.setCurrentItem(0, true);
			break;
		case R.id.tv_notification_right:
			mViewPager.setCurrentItem(1, true);
			break;
		default:
			break;
		}

	}

}
