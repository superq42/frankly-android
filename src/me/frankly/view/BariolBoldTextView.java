package me.frankly.view;

import me.frankly.util.MyUtilities;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class BariolBoldTextView extends TextView {

	public BariolBoldTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public BariolBoldTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public BariolBoldTextView(Context context) {
		super(context);
		init(context);
	}

	private void init(Context pContext) {
		if (isInEditMode()) {
			return;
		}
		this.setTypeface(MyUtilities.getTypeface(pContext, "bariol_bold_webfont"));
	}
}
