package me.frankly.view;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.model.newmodel.PostObject;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.MyUtilities;
import me.frankly.util.SharePackage;
import me.frankly.util.ShareUtils;
import me.frankly.view.activity.BaseActivity;
import android.app.Dialog;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class ShareDialog extends Dialog implements
		android.view.View.OnClickListener {

	public BaseActivity mBaseActivity;
	public Dialog mDialog;
	public ImageView btnFirst, btnSecond, btnThird, btnMore, profilePic,
			btnShareClose;
	private TextView tvQuestion, tvName, tvTitle;
	private ArrayList<SharePackage> mSharePackageList;
	private int layoutResId;
	private String questionText, nameText, titleText, thumbnailUrl, shareUrl;
	private UniversalUser mUser;
	private int type;
	private String key;
	private int numberofApps;
	private int flag = 0;
	private boolean isMyPost;

	public ShareDialog(BaseActivity mBaseActivity, int layoutResId, Object mObject,
			int type, boolean isMyPost, String key,
			ArrayList<SharePackage> packageList) {

		super(mBaseActivity);
		this.mBaseActivity = mBaseActivity;
		this.layoutResId = layoutResId;
		this.key = key;
		this.type = type;
		this.isMyPost = isMyPost;
		this.mSharePackageList = packageList;
		if (packageList == null || packageList.isEmpty())
			numberofApps = 0;
		else if (packageList.size() >= 1)
			numberofApps = 1;
		/*else
			numberofApps = 2;*/
		if (mObject != null) {
			if (type == 0) {
				PostObject post = (PostObject) mObject;
				this.questionText = post.getQuestion().getBody();
				this.nameText = post.getAnswer_author().getFull_name();
				this.thumbnailUrl = post.getAnswer().getMedia()
						.getThumbnail_url();
				this.mUser = post.getAnswer_author();
				this.shareUrl = post.getWeb_link();
			} else if (type == 2) {
				UniversalUser user = (UniversalUser) mObject;

				this.nameText = user.getFull_name();
				this.titleText = user.getUser_title();
				this.thumbnailUrl = user.getProfile_picture();
				this.shareUrl = user.getWeb_link();
				this.mUser = user;
			} else if (type == 3) {
				QuestionObject shareQuestion = (QuestionObject) mObject;

				this.nameText = shareQuestion.getQuestion_author().getFirst_name();
				this.titleText = shareQuestion.getQuestion_to().getUser_title();
				this.thumbnailUrl = shareQuestion.getQuestion_to()
						.getProfile_picture();
				this.shareUrl = shareQuestion.getWeb_link();
				this.mUser = shareQuestion.getQuestion_to();
				this.questionText = shareQuestion.getBody();
			}
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(layoutResId);
		this.getWindow().setBackgroundDrawableResource(R.color.white_pure);
		if (mSharePackageList == null || mSharePackageList.size() == 0)
			mSharePackageList = ShareUtils.getSharingPackages(getContext(),
					this.key);

		tvTitle = (TextView) findViewById(R.id.share_tv_share);
		if (type == 0 || type == 2 || type == 3) {
			tvTitle.setText("Share");
		} else if (type == 1)
			tvTitle.setText("Invite your friends");
		tvQuestion = (TextView) findViewById(R.id.share_tv_question);
		tvName = (TextView) findViewById(R.id.share_tv_author);

		// For post
		if (type == 0) {

			tvQuestion.setVisibility(View.VISIBLE);
			tvQuestion.setText(questionText);
			tvName.setVisibility(View.VISIBLE);
			tvName.setText("by " + nameText);
			tvName.setOnClickListener(this);

		} else if (type == 2) {
			// For profile
			tvQuestion.setVisibility(View.VISIBLE);
			tvQuestion.setText(nameText);
			tvQuestion.setOnClickListener(this);
			tvName.setVisibility(View.VISIBLE);
			tvName.setText(titleText);

		}
		
		else if (type == 3)
		{
			tvQuestion.setVisibility(View.VISIBLE);
			tvQuestion.setText(questionText);
			tvQuestion.setOnClickListener(this);
			tvName.setVisibility(View.VISIBLE);
			tvName.setText(nameText);
		}

		if (thumbnailUrl != null) {

			profilePic = (ImageView) findViewById(R.id.share_iv_profile);
			profilePic.setVisibility(View.VISIBLE);
			ImageLoader.getInstance().displayImage(thumbnailUrl, profilePic);

		}

		btnShareClose = (ImageView) findViewById(R.id.share_close);
		btnFirst = (ImageView) findViewById(R.id.dialog_first_share);
		btnSecond = (ImageView) findViewById(R.id.dialog_second_share);
		btnThird = (ImageView) findViewById(R.id.dialog_third_share);
		btnMore = (ImageView) findViewById(R.id.dialog_more_share);
		btnShareClose.setOnClickListener(this);
		btnFirst.setOnClickListener(this);
		btnSecond.setOnClickListener(this);
		btnThird.setOnClickListener(this);
		btnMore.setOnClickListener(this);
		try {
			manageSharingThings();
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

	}

	public void createDialogView() {

	}

	private void manageSharingThings() throws NameNotFoundException {
		ImageView[] shareImages = new ImageView[] { btnFirst, btnSecond,
				btnThird };
		if (mSharePackageList != null && mSharePackageList.size() > 0) {
			int length = 3;
			length = Math.min(length, mSharePackageList.size() - numberofApps);
			Log.d("share", "" + length);
			int i = 0;
			for (; i < length; i++) {
				SharePackage sharePackage;
				if (mSharePackageList.size() > (i + numberofApps)) {
					shareImages[i].setVisibility(View.VISIBLE);
					sharePackage = mSharePackageList.get(i + numberofApps);
					Drawable icon = getContext().getPackageManager()
							.getApplicationIcon(sharePackage.packageName);
					shareImages[i].setImageDrawable(icon);
					Log.e("share_setPackagename", sharePackage.packageName + "");
					shareImages[i].setTag(sharePackage.packageName);
				}
			}
			if (i <= 3) {
				for (; i < shareImages.length; i++) {
					shareImages[i].setVisibility(View.GONE);
				}
			}
		} else {
			for (int i = 0; i < shareImages.length; i++) {
				shareImages[i].setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialog_first_share:

			mSharePackageList.get(numberofApps).viewCount++;
			flag = 1;
		case R.id.dialog_second_share:
			if (flag == 0)
				mSharePackageList.get(numberofApps + 1).viewCount++;
			flag = 1;
		case R.id.dialog_third_share:
			if (flag == 0)
				mSharePackageList.get(numberofApps + 2).viewCount++;
			if (type == 0 || type == 2) {

				if (v.getTag() != null) {
					ShareUtils.shareLinkUsingIntent(makeShareText(v.getTag()
							.toString()), mBaseActivity, (String) v.getTag(), 1);
				}

			} else if (type == 1) {
				ShareUtils.shareLinkUsingIntent(mBaseActivity.getResources()
						.getString(R.string.default_invite_text), mBaseActivity,
						(String) v.getTag(), 1);
			} else if (type == 3) {
				String postInviteText = mBaseActivity.getResources().getString(
						R.string.post_invite_text, mUser.getFull_name());
				postInviteText = postInviteText + " " + shareUrl;
				ShareUtils.shareLinkUsingIntent(postInviteText, mBaseActivity,
						(String) v.getTag(), 1);
			}
			break;
		case R.id.dialog_more_share:
			if (type == 0 || type == 2)
				MyUtilities.shareText(makeShareText(""), null, mBaseActivity);
			else if (type == 1)
				MyUtilities.shareText(
						mBaseActivity.getResources().getString(
								R.string.default_invite_text), null, mBaseActivity);
			else if (type == 3) {
				String postInviteText = mBaseActivity.getResources().getString(
						R.string.post_invite_text, mUser.getFull_name());
				postInviteText = postInviteText + " " + shareUrl;
				MyUtilities.shareText(postInviteText, null, mBaseActivity);
			}

			break;
		case R.id.share_tv_author:
			mBaseActivity.profileActivity( null, mUser);

			break;
		case R.id.share_iv_profile:
			mBaseActivity.profileActivity(null, mUser);
			break;

		case R.id.share_close:
			dismiss();
			break;
		default:
			break;
		}
		dismiss();
	}

	public String makeShareText(String packageName) {
		String postShareText = "", question_text = "";
		question_text = questionText;
		if (type == 0) {
			if (packageName.equals(ShareUtils.PKG_TWITTER)) {
				int length = 38;
				length += nameText.length() + 3;
				length += shareUrl.length();
				int question_length = 140 - length;
				if (question_length < questionText.length())
					question_text = questionText.substring(0, question_length)
							+ "...";
			}

			if (!isMyPost)
				postShareText = "Watch " + nameText + "'s video answer to \""
						+ question_text.trim() + "\" at " + shareUrl;
			else
				postShareText = "Watch my video answer to \""
						+ question_text.trim() + "\" at " + shareUrl;
		} else if (type == 2) {
			if (isMyPost)
				postShareText = "Ask me anything on frankly.me and I"
						+ " will reply frankly on: " + shareUrl;
			else
				postShareText = "Check out " + nameText
						+ "'s frank video answers" + " on frankly.me: "
						+ shareUrl;

		} else if (type == 3) {
			postShareText = "Check out my question to" + nameText + "on "
					+ shareUrl;
		}
		return postShareText.trim();
	}
}
