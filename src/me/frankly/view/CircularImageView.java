package me.frankly.view;

import me.frankly.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class CircularImageView extends ImageView {
	private int borderWidth = 2;
	private int viewWidth;
	private int viewHeight;
	private Bitmap image;
	private Paint paint;
	private Paint paintBorder;
	private BitmapShader shader;

	private Context context;

	public CircularImageView(Context context) {
		super(context);
		this.context = context;
		setup();
	}

	public CircularImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		setup();
	}

	public CircularImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		setup();
	}

	private void setup() {
		// init paint
		paint = new Paint();
		paint.setAntiAlias(true);

		paintBorder = new Paint();
		setBorderColor("#6a6968");
		paintBorder.setAntiAlias(true);
		// this.setLayerType(LAYER_TYPE_SOFTWARE, paintBorder);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			this.setLayerType(LAYER_TYPE_SOFTWARE, null);
		}
		// paintBorder.setShadowLayer(4.0f, 0.0f, 2.0f, Color.BLACK);
	}

	public void setBorderWidth(int borderWidth) {
		this.borderWidth = borderWidth;
		this.invalidate();
	}

	public void setBorderColor(String borderColor) {
		if (paintBorder != null)
			paintBorder.setColor(Color.parseColor(borderColor));

		this.invalidate();
	}

	public void setBorderColor(int borderColor) {
		if (paintBorder != null)
			paintBorder.setColor(borderColor);

		this.invalidate();
	}

	private void loadBitmap() {
		BitmapDrawable bitmapDrawable = (BitmapDrawable) this.getDrawable();

		if (bitmapDrawable != null)
			image = bitmapDrawable.getBitmap();
	}

	@SuppressLint("DrawAllocation")
	@Override
	public void onDraw(Canvas canvas) {
		// load the bitmap
		loadBitmap();

		// init shader
		if (image != null) {
			shader = new BitmapShader(Bitmap.createScaledBitmap(image, canvas.getWidth(), canvas.getHeight(), false), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
			paint.setShader(shader);
			int circleCenter = viewWidth / 2;

			// circleCenter is the x or y of the view's center
			// radius is the radius in pixels of the cirle to be drawn
			// paint contains the shader that will texture the shape
			canvas.drawCircle(circleCenter + borderWidth, circleCenter + borderWidth, circleCenter + borderWidth, paintBorder);
			canvas.drawCircle(circleCenter + borderWidth, circleCenter + borderWidth, circleCenter, paint);
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = measureWidth(widthMeasureSpec);
		int height = measureHeight(heightMeasureSpec, widthMeasureSpec);

		viewWidth = width - (borderWidth * 2);
		viewHeight = height - (borderWidth * 2);

		setMeasuredDimension(width, height);
	}

	private int measureWidth(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else {
			// Measure the text
			result = viewWidth;
		}

		return result;
	}

	private int measureHeight(int measureSpecHeight, int measureSpecWidth) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpecHeight);
		int specSize = MeasureSpec.getSize(measureSpecHeight);

		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else {
			// Measure the text (beware: ascent is a negative number)
			result = viewHeight;
		}

		return (result + 2);
	}

	public void setImageUrl(String url, int size) {

		ImageLoader imageloader = ImageLoader.getInstance();
		initUIL(imageloader);
		imageloader.displayImage(url, this, getCustomDisplayOptions());
	}

	public void setImageUrl(DisplayImageOptions options, String url) {

		ImageLoader.getInstance().displayImage(url, this, options);
	}

	// private void setImageUrl(String url) {
	// ImageLoader.getInstance().displayImage(url, this,
	// getCustomDisplayOptions());
	//
	// }

	public void setImageUrl(String url, String gender) {
		ImageLoader.getInstance().displayImage(url, this, getGenderBasedDisplayOptions(gender));
	}

	private void initUIL(ImageLoader loader) {
		if (!ImageLoader.getInstance().isInited()) {
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).tasksProcessingOrder(QueueProcessingType.LIFO).denyCacheImageMultipleSizesInMemory()
					.defaultDisplayImageOptions(getCustomDisplayOptions()).writeDebugLogs().build();
			loader.init(config);
		}

	}

	public void setDefaultImageResId(int tempImage) {

	}

	public static DisplayImageOptions getCustomDisplayOptions() {
		DisplayImageOptions options;
		options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.placeholder_profile_person).showImageForEmptyUri(R.drawable.placeholder_profile_person)
				.showImageOnFail(R.drawable.placeholder_profile_person).cacheInMemory(true).cacheOnDisc(true).imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		return options;
	}

	public static DisplayImageOptions getDefaultDisplayOptions(int res_id) {
		DisplayImageOptions options;
		options = new DisplayImageOptions.Builder().showImageOnLoading(res_id).showImageForEmptyUri(res_id).showImageOnFail(res_id).cacheInMemory(true).cacheOnDisc(true)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2).bitmapConfig(Bitmap.Config.RGB_565).build();
		return options;
	}
	
	public static DisplayImageOptions getDefaultDisplayOptions1(int res_id) {
		DisplayImageOptions options;
		options = new DisplayImageOptions.Builder().showImageOnLoading(res_id).showImageForEmptyUri(res_id).showImageOnFail(res_id).cacheInMemory(true).cacheOnDisc(true)
				.imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565).build();
		return options;
	}

	public static DisplayImageOptions getGenderBasedDisplayOptions(String gender) {
		int res_id = R.drawable.placeholder_profile_person;
		if (gender != null && gender.equalsIgnoreCase("M"))
			res_id = R.drawable.placeholder_profile_person;


		DisplayImageOptions options;
		options = new DisplayImageOptions.Builder().showImageOnLoading(res_id).showImageForEmptyUri(res_id).showImageOnFail(res_id).cacheInMemory(true).cacheOnDisc(true)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2).bitmapConfig(Bitmap.Config.RGB_565).build();
		return options;
	}
}