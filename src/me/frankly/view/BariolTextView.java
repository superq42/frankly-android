package me.frankly.view;

import me.frankly.util.MyUtilities;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class BariolTextView extends TextView {

	public BariolTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public BariolTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public BariolTextView(Context context) {
		super(context);
		init(context);
	}

	private void init(Context pContext) {
		if (isInEditMode()) {
			return;
		}
		this.setTypeface(MyUtilities.getTypeface(pContext, "bariol"));
	}
}
