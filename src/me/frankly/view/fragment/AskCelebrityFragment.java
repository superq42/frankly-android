package me.frankly.view.fragment;

import java.util.ArrayList;
import java.util.List;

import me.frankly.R;
import me.frankly.adapter.AskQuestionAdapter;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.QuestionPostCompletionListener;
import me.frankly.listener.RequestListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.QuestionObjectHolder;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.model.response.PaginatedQuestionObjectHolder;
import me.frankly.util.ImageUtil;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.SharePackage;
import me.frankly.util.ShareUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.ShareDialog;
import me.frankly.view.activity.AskActivity;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.ShareProfileActivity;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class AskCelebrityFragment extends Fragment implements OnClickListener,
		OnScrollListener, QuestionPostCompletionListener, UserFollowListener {

	private static String TAG = "AskScreen";
	private UniversalUser mUniversalUser;
	private BaseActivity mBaseActivity;
	private ListView mListView;
	private AskQuestionAdapter mAskQuestionAdapter;
	private List<QuestionObjectHolder> mListQuestionObjects;
	private String userId;
	private String since = "0";
	private boolean loading = false;
	private boolean moreData = true;
	private ImageView iv_loader, ivFollow, ivNoQuestions;
	private AnimationDrawable loaderAnimation;
	private TextView txtAskQuestion, tv_Name, tvRank;
	private ToolTipRelativeLayout toolTipRelativeLayout;
	private ToolTipView askToolTipView;
	private Dialog dialog;
	public static ArrayList<SharePackage> packageList;
	private CircularImageView cvProfilepic;
	private QuestionObjectHolder stubQuestionObjectHolder;
	private boolean isFirstRequest = true;
	private String questionShareText;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#Ninja AskCelebF:";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + "onCreate", "Called");

		super.onCreate(savedInstanceState);
		if (getActivity() instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) getActivity();
		}
		userId = getArguments().getString(Constant.Key.KEY_USER_ID);

		mListQuestionObjects = new ArrayList<QuestionObjectHolder>();

		Log.d("ACF", "inside onCreate");
		String questionString = null;
		boolean isFromDeeplink = false;
		mUniversalUser = (UniversalUser) JsonUtils.objectify(getArguments()
				.getString(Constant.Key.KEY_USER_OBJECT), UniversalUser.class);
		if (getArguments().containsKey(Constant.Key.KEY_QUESTION_OBJECT))
			questionString = getArguments().getString(
					Constant.Key.KEY_QUESTION_OBJECT);
		if (questionString != null) {
			QuestionObjectHolder shareQuestionHolder = new QuestionObjectHolder();
			shareQuestionHolder.setQuestion((QuestionObject) JsonUtils
					.objectify(questionString, QuestionObject.class));
			if (getArguments().containsKey(Constant.Key.KEY_FROM_DEEPLINK))
				isFromDeeplink = getArguments().getBoolean(
						Constant.Key.KEY_FROM_DEEPLINK);
			if (isFromDeeplink)
				shareQuestionHolder
						.setType(QuestionObjectHolder.TYPE_SHARE_QUESTION);
			mListQuestionObjects.add(0, shareQuestionHolder);
		}

		packageList = ShareUtils.getSharingPackages(getActivity(), "post");
		UniversalActionListenerCollection.getInstance().addUserFollowListener(
				this, mUniversalUser.getId());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + "onCreateView", "Called");

		View view = inflater.inflate(R.layout.fragment_celeb_ask_question,
				container, false);
		mListView = (ListView) view.findViewById(R.id.frag_ask_list_question);
		iv_loader = new ImageView(mBaseActivity);
		iv_loader.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) iv_loader.getDrawable();
		showLoading(false);
		toolTipRelativeLayout = (ToolTipRelativeLayout) view
				.findViewById(R.id.activity_main_tooltipRelativeLayout);
		ivNoQuestions = (ImageView) view.findViewById(R.id.iv_no_questions);
		mAskQuestionAdapter = new AskQuestionAdapter(mBaseActivity,
				mListQuestionObjects);
		mListView.setAdapter(mAskQuestionAdapter);
		mListView.setOnScrollListener(this);
		mListView.addFooterView(iv_loader);

		if (!mListQuestionObjects.isEmpty())
			iv_loader.setVisibility(View.INVISIBLE);

		ImageView imgBack = (ImageView) view
				.findViewById(R.id.action_bar_back_btn);
		imgBack.setOnClickListener(this);

		txtAskQuestion = (TextView) view
				.findViewById(R.id.frag_ask_celeb_btn_ask);
		txtAskQuestion.setOnClickListener(this);
		txtAskQuestion.setBackgroundColor(getResources().getColor(
				R.color.app_theme_color));
		tv_Name = (TextView) view.findViewById(R.id.frag_proilfe_name);
		tvRank = (TextView) view.findViewById(R.id.frag_proilfe_rank);
		cvProfilepic = (CircularImageView) view
				.findViewById(R.id.frag_profile_img);
		ivFollow = (ImageView) view
				.findViewById(R.id.frag_profile_img_plus_profile);
		tv_Name.setText(mUniversalUser.getFull_name());
		tvRank.setText(mUniversalUser.getUser_title());
		if (mUniversalUser.getProfile_picture() != null) {
			DisplayImageOptions options = CircularImageView
					.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
			cvProfilepic.setImageUrl(options,
					mUniversalUser.getProfile_picture());
		}
		if (mUniversalUser.is_following()) {
			ivFollow.setImageResource(R.drawable.ic_followed);
			ivFollow.setTag("following");
			cvProfilepic.setTag("following");
		} else {
			ivFollow.setImageResource(R.drawable.ic_add_profilepic);
			ivFollow.setTag("follow");
			cvProfilepic.setTag("follow");
		}
		fetchCelebrityQuestions();

		Controller.registerGAEvent("AskCelebrityFragment");
		tv_Name.setOnClickListener(this);
		ivFollow.setOnClickListener(this);
		cvProfilepic.setOnClickListener(this);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		fileDebugLog(FILE_DEBUG_TAG + "onResume", "Called");

		if (!PrefUtils.getIsFirstAskAnimShown()) {
			if (askToolTipView != null)
				askToolTipView.remove();
			askToolTipView = Controller.addToolTipView(toolTipRelativeLayout,
					txtAskQuestion, mBaseActivity, "Tap to Ask", false);
		}
	}

	@Override
	public void onClick(View view) {
		fileDebugLog(FILE_DEBUG_TAG + "onClick", "Called");
		switch (view.getId()) {
		case R.id.frag_ask_celeb_btn_ask:
			if (!PrefUtils.getIsFirstAskAnimShown()) {
				askToolTipView.remove();
				PrefUtils.setIsFirstAskAnimShown(true);
			}
			openAskQuestionFragment();
			registerMixpanelEvent(MixpanelUtils.ASK_ME);
			break;

		case R.id.action_bar_back_btn:
			getActivity().onBackPressed();
			break;
		case R.id.iv_invite_friends:
			showCustomDialog(0);
			if (dialog != null)
				dialog.dismiss();
			break;
		case R.id.iv_no_thanks:
			if (dialog != null)
				dialog.dismiss();
			break;
		case R.id.frag_proilfe_name:
			mBaseActivity.profileActivity(null, mUniversalUser);
			break;
		case R.id.frag_profile_img_plus_profile:
		case R.id.frag_profile_img:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				if (view.getTag().equals("follow")) {
					mUniversalUser.setIs_following(true);
					followUser();
					cvProfilepic.setTag("following");
					ivFollow.setImageResource(R.drawable.ic_followed);
				} else if (view.getTag().equals("following")) {
					unFollowUser();
					mUniversalUser.setIs_following(false);
					cvProfilepic.setTag("follow");
					ivFollow.setImageResource(R.drawable.ic_add_profilepic);
					ivFollow.setTag("follow");
				}
				UniversalActionListenerCollection.getInstance()
						.onUserFollowChanged(mUniversalUser.getId(),
								mUniversalUser.is_following());
			}
			break;
		case R.id.fb_share_img:
			shareProfile(ShareUtils.PKG_FACEBOOK);
			/*
			 * if (firstShareDialog != null) firstShareDialog.dismiss();
			 */
			break;
		case R.id.whatsapp_share_img:
			shareProfile(ShareUtils.PKG_WHATSAPP);
			/*
			 * if (firstShareDialog != null) firstShareDialog.dismiss();
			 */
			break;
		case R.id.twitter_share_img:
			shareProfile(ShareUtils.PKG_TWITTER);
			/*
			 * if (firstShareDialog != null) firstShareDialog.dismiss();
			 */
			break;
		case R.id.default_share_img:
			shareProfile(Intent.ACTION_SEND);
			/*
			 * if (firstShareDialog != null) firstShareDialog.dismiss();
			 */
			break;
		}
	}

	private void followUser() {
		fileDebugLog(FILE_DEBUG_TAG + "followUser", "Called");
		String userName = mUniversalUser.getId();
		Controller.requestfollow(getActivity(), userName,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode,
							final String message) {
					}

					@Override
					public void onRequestCompleted(Object responseObject) {
					}
				});
	}

	private void unFollowUser() {
		fileDebugLog(FILE_DEBUG_TAG + "unFollowUser", "Called");
		String userId = mUniversalUser.getId();
		Controller.requestUnfollow(getActivity(), userId,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode, String message) {
						fileDebugLog(FILE_DEBUG_TAG + "onRequesterror", ""
								+ message);
					}

					@Override
					public void onRequestCompleted(Object responseObject) {
					}
				});
	}

	// Opens AskFragment.
	private void openAskQuestionFragment() {
		Log.d("ACF", "opening ask fragment");
		fileDebugLog(FILE_DEBUG_TAG + "openAskQuestionFragment", "Called");
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();

		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.setCustomAnimations(R.drawable.enter_up,
				R.drawable.exit_up, R.drawable.pop_enter_up,
				R.drawable.pop_exit_up);

		AskFragment fragment = new AskFragment();
		fragment.setQuestionCompletionListener(this);

		Bundle bundle = new Bundle();
		bundle.putString(Constant.Key.KEY_USER_ID, userId);
		bundle.putString(Constant.Key.KEY_USER_NAME,
				mUniversalUser.getFull_name());
		fragment.setArguments(bundle);

		transaction.replace(R.id.activity_ask_framell, fragment);
		transaction.addToBackStack(null);
		transaction.commit();

	}

	private void fetchCelebrityQuestions() {
		fileDebugLog("FDR", "fetchCelebrityQuestions Called");
		Log.i(TAG, "fetchCelebrityQuestions " + loading);

		if (!loading && moreData) {

			Controller.getCelebrityQuestions(getActivity(), userId, since,
					celebQuestionListner);
		}

	}

	RequestListener celebQuestionListner = new RequestListener() {
		@Override
		public void onRequestStarted() {
			fileDebugLog(FILE_DEBUG_TAG + "onRequestStarted", "Called");
			Log.d("ACF", "request started");
			loading = true;
			showLoading(true);
		}

		@Override
		public void onRequestError(int errorCode, String message) {
			fileDebugLog(FILE_DEBUG_TAG + "onRequestError", "" + message);
			Log.d("ACF", "request error");
			loading = false;
			showLoading(false);
		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			fileDebugLog("FDR", "fetches Lots JSON data " + responseObject);
			Log.d("ACF", "request completed");

			PaginatedQuestionObjectHolder questionsList = (PaginatedQuestionObjectHolder) JsonUtils
					.objectify((String) responseObject,
							PaginatedQuestionObjectHolder.class);
			if (questionsList.getNext().equals("-1"))
				moreData = false;
			Log.d("ACF", "request completed" + questionsList.getCount());
			since = questionsList.getNext();
			if (questionsList.getCount() != 0
					|| questionsList.getCurrentUseQuestions().size() != 0) {
				Log.d("ACF", "cout not zero");
				if (questionsList.getCurrentUseQuestions().size() > 0
						&& isFirstRequest) {
					if (mListQuestionObjects.size() > 0
							&& mListQuestionObjects
									.get(0)
									.getType()
									.equals(QuestionObjectHolder.TYPE_SHARE_QUESTION)
							&& mListQuestionObjects.get(0).getQuestion()
									.getQuestion_author().getId() == PrefUtils
									.getUserID()
							&& questionsList
									.getCurrentUseQuestions()
									.get(0)
									.getQuestion()
									.getId()
									.equals(mListQuestionObjects.get(0)
											.getQuestion().getId())
							&& questionsList.getCurrentUseQuestions().size() > 1)
						questionsList
								.getCurrentUseQuestions()
								.get(1)
								.setType(
										QuestionObjectHolder.TYPE_CURRENT_QUESTION);
					else
						questionsList
								.getCurrentUseQuestions()
								.get(0)
								.setType(
										QuestionObjectHolder.TYPE_CURRENT_QUESTION);
					displayCelebrityQuestions(questionsList
							.getCurrentUseQuestions());
				}
				if (questionsList.getQuestions().size() > 0 && isFirstRequest) {
					questionsList.getQuestions().get(0)
							.setType(QuestionObjectHolder.TYPE_OTHER_QUESTION);

				}
				displayCelebrityQuestions(questionsList.getQuestions());
			} else if (mListQuestionObjects.isEmpty() && !moreData) {
				// In case Celebrity has been asked Zero questions:
				fileDebugLog(FILE_DEBUG_TAG + "onRequestCompleted-empty",
						"mListQuestionObjects.isEmpty()");
				displayEmptyView();
			}
			isFirstRequest = false;
			loading = false;
			showLoading(false);
		}

	};

	private void displayCelebrityQuestions(final List<QuestionObjectHolder> list) {
		fileDebugLog(FILE_DEBUG_TAG + "displayCelebrityQuestions", "Called");
		if (isAdded())
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (mListView.getVisibility() != View.VISIBLE)
						mListView.setVisibility(View.VISIBLE);
					if (ivNoQuestions.getVisibility() == View.VISIBLE)
						ivNoQuestions.setVisibility(View.GONE);
					mListQuestionObjects.addAll(list);
					removeRepeated();
					mAskQuestionAdapter.notifyDataSetChanged();

				}
			});

	}

	protected void displayEmptyView() {
		fileDebugLog(FILE_DEBUG_TAG + "displayEmptyView", "Called");

		if (isAdded())
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					fileDebugLog(FILE_DEBUG_TAG + "runOnUiThread-run()",
							"Called");
					mListView.setVisibility(View.GONE);
					ivNoQuestions.setVisibility(View.VISIBLE);
				}
			});
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// fileDebugLog(FILE_DEBUG_TAG + "onScroll", "Called");
		if (!loading && moreData) {
			int currentpos = firstVisibleItem + visibleItemCount;
			if (currentpos >= totalItemCount - 2) {
				fetchCelebrityQuestions();
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		fileDebugLog(FILE_DEBUG_TAG + "onScrollStateChanged", "Called");
	}

	private void showLoading(final boolean status) {
		fileDebugLog(FILE_DEBUG_TAG + "showLoading-status=", "" + status);
		if (isAdded())
			((Activity) mBaseActivity).runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (status) {
						if (iv_loader.getVisibility() != View.VISIBLE)
							iv_loader.setVisibility(View.VISIBLE);
						loaderAnimation.start();
					} else {
						loaderAnimation.stop();
						if (iv_loader.getVisibility() == View.VISIBLE)
							iv_loader.setVisibility(View.INVISIBLE);
					}
				}
			});

	}

	@Override
	public void onQuestionPosted(String questionBody, String quesId) {
		fileDebugLog(FILE_DEBUG_TAG + "onQuestionPosted", "Called");
		stubQuestionObjectHolder = new QuestionObjectHolder();
		Controller.requestQuestionByShortId(quesId, mBaseActivity,
				new RequestListener() {
					@Override
					public void onRequestStarted() {
						fileDebugLog(FILE_DEBUG_TAG + "sharequestion",
								"started");
					}

					@Override
					public void onRequestError(int errorCode, String message) {
						fileDebugLog(FILE_DEBUG_TAG + "sharequestion",
								"error: " + message);
						showLoading(false);
					}

					@Override
					public void onRequestCompleted(Object responseObject) {
						fileDebugLog(FILE_DEBUG_TAG
								+ "onRequestCompleted-sharequestion2",
								"completed" + responseObject.toString());
						stubQuestionObjectHolder = (QuestionObjectHolder) JsonUtils
								.objectify((String) responseObject,
										QuestionObjectHolder.class);

						/*
						 * for (int i = 0; i < mListQuestionObjects.size(); i++)
						 * { if (mListQuestionObjects .get(i) .getType()
						 * .equals(QuestionObjectHolder.TYPE_CURRENT_QUESTION))
						 * { mListQuestionObjects.get(i).setType(
						 * QuestionObjectHolder.TYPE_QUESTION); break; } }
						 */
						if (!mListQuestionObjects.isEmpty()
								&& mListQuestionObjects
										.get(0)
										.getType()
										.equals(QuestionObjectHolder.TYPE_CURRENT_QUESTION))
							mListQuestionObjects.get(0).setType(
									QuestionObjectHolder.TYPE_QUESTION);
						stubQuestionObjectHolder
								.setType(QuestionObjectHolder.TYPE_CURRENT_QUESTION);
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								mListQuestionObjects.add(0,
										stubQuestionObjectHolder);
								mAskQuestionAdapter.notifyDataSetChanged();
								if (!PrefUtils
										.isShareScreenShown(ShareProfileActivity.MODE_FIRST_QUESTION)) {
									String shareTextStr = ShareUtils
											.getTextToShare(stubQuestionObjectHolder
													.getQuestion());
									String bgImgUrl = mUniversalUser
											.getCover_picture();
									if (bgImgUrl == null
											&& mUniversalUser
													.getProfile_picture() != null) {
										bgImgUrl = ImageUtil
												.getThumbUrl(mUniversalUser
														.getProfile_picture());
									}
									mBaseActivity.shareActivity(
											AskCelebrityFragment.this,
											shareTextStr,
											mUniversalUser.getFirst_name(),
											bgImgUrl,
											mUniversalUser.getUser_type());
								} else {
									showPostDialog();
								}
								if (ivNoQuestions.getVisibility() == View.VISIBLE)
									ivNoQuestions.setVisibility(View.GONE);
								showLoading(false);
							}
						});

					}
				});
	}

	private void showPostDialog() {
		fileDebugLog(FILE_DEBUG_TAG + "showPostDialog", "Called");
		// custom dialog
		if (dialog == null) {
			dialog = new Dialog(mBaseActivity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setContentView(R.layout.share_dialog_post_question);

			Window window = dialog.getWindow();
			WindowManager.LayoutParams wlp = window.getAttributes();

			wlp.gravity = Gravity.BOTTOM;
			wlp.y = 100;
			window.setAttributes(wlp);

			// set the custom dialog components - text, image and button
			TextView text1 = (TextView) dialog
					.findViewById(R.id.tv_queued_text_1);

			/*
			 * CharSequence str = Html.fromHtml("<b><font color='#ea6153'>" +
			 * mUniversalUser.getFull_name() + "</font></b>");
			 */

			StringBuilder textBuilder = new StringBuilder(
					"Invite your friends to Upvote and follow your question to ")
					.append("<b><font color='#ea6153'>")
					.append(mUniversalUser.getFull_name())
					.append("</font></b>. Questions with More votes and followers get answered earlier than the rest!");

			text1.setText(Html.fromHtml(textBuilder.toString()));

			textBuilder = null;

			TextView tvInvite = (TextView) dialog
					.findViewById(R.id.iv_invite_friends);
			TextView tvNoThanks = (TextView) dialog
					.findViewById(R.id.iv_no_thanks);
			tvInvite.setOnClickListener(this);
			tvNoThanks.setOnClickListener(this);
		}

		dialog.show();
	}

	public void showCustomDialog(int position) {
		fileDebugLog(FILE_DEBUG_TAG + "showCustomDialog", "Called+Position:"
				+ position);
		ShareDialog cdd = new ShareDialog(mBaseActivity,
				R.layout.share_dialog_post_layout, mListQuestionObjects.get(
						position).getQuestion(), 3, mUniversalUser.getId()
						.equals(PrefUtils.getUserID()), "ASKCELEB", null);
		cdd.show();

	}

	public void registerMixpanelEvent(String event_name) {
		fileDebugLog(FILE_DEBUG_TAG + "registerMixpanelEvent", "Called");
		if (isAdded())
			MixpanelUtils.sendGenericEvent(mUniversalUser.getId(),
					mUniversalUser.getUsername(),
					String.valueOf(mUniversalUser.getUser_type()),
					Constant.ScreenSpecs.SCREEN_QUESTION_LIST, event_name,
					getActivity());
	}

	public void removeRepeated() {
		fileDebugLog(FILE_DEBUG_TAG + "removeRepeated", "Called");
		for (int i = 0; i < mListQuestionObjects.size() - 1; i++) {
			// fileDebugLog(FILE_DEBUG_TAG + "mListQuestionObjects =", "i=" + i
			// + "Lots of JSON again");
			for (int j = i + 1; j < mListQuestionObjects.size(); j++) {
				// fileDebugLog(FILE_DEBUG_TAG + "2_mListQuestionObjects =",
				// "j="
				// + j + "Hell of Lot JSON again");
				if (mListQuestionObjects
						.get(i)
						.getQuestion()
						.getId()
						.equals(mListQuestionObjects.get(j).getQuestion()
								.getId())) {
					mListQuestionObjects.remove(j);
					fileDebugLog(FILE_DEBUG_TAG
							+ "Removed-mListQuestionObjects", ""
							+ mListQuestionObjects);
				}
			}
		}
	}

	private void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}

	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		if (isAdded() && mUniversalUser.getId().equals(userId)) {
			mUniversalUser.setIs_following(isFollowed);

			if (isFollowed) {
				cvProfilepic.setTag("following");
				ivFollow.setTag("following");
				ivFollow.setImageResource(R.drawable.ic_followed);
			} else {
				cvProfilepic.setTag("follow");
				ivFollow.setImageResource(R.drawable.ic_add_profilepic);
				ivFollow.setTag("follow");
			}

		}

	}

	public void shareProfile(String packageName) {
		if (packageName.equalsIgnoreCase(Intent.ACTION_SEND)) {
			Intent intent = new Intent(packageName);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, questionShareText);
			startActivity(intent);
			return;
		}
		ShareUtils.shareLinkUsingIntent(questionShareText, mBaseActivity,
				packageName);
	}

	@Override
	public void onDestroy() {
		UniversalActionListenerCollection.getInstance()
				.removeUserFollowListener(this, mUniversalUser.getId());
		super.onDestroy();
	}
}
