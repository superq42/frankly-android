package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.UserQuestionsContainer;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.BaseActivity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.nhaarman.supertooltips.ToolTipView.OnToolTipViewClickedListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class CelebQuestionsFragment extends Fragment implements
		OnClickListener, RequestListener, OnTouchListener,
		OnToolTipViewClickedListener {
	private static final String FOLLOW = "FOLLOW";
	private static final String FOLLOWING = "FOLLOWING";
	private String mParentScreen;

	private BaseActivity mBaseActivity;
	private ImageView imgFollow;
	private RelativeLayout rlSecondQuesView;
	private ImageView ivBackground;
	private UserQuestionsContainer objectContainer;
	private CircularImageView picCircularImageView;
	private TextView nameTextView, titleTextView, quesOneTextView,
			quesTwoTextView, quesOneAskerTextView, quesTwoAskerTextView,
			quesOneUpvoteCountTextView, quesTwoUpvoteCountTextView,
			upvoteOneView, upvoteTwoView, ivAsk, tvQuesOneYou, tvQuesTwoYou;
	ToolTipRelativeLayout toolTipRelativeLayout;
	ToolTipView upvoteTipView, askToolTipView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) getActivity();
		}
		Bundle args = getArguments();
		if (args != null) {
			if (args.containsKey(Constant.Key.KEY_CELEB_QUES_JSON)) {
				objectContainer = (UserQuestionsContainer) JsonUtils.objectify(
						args.getString(Constant.Key.KEY_CELEB_QUES_JSON),
						UserQuestionsContainer.class);
				mParentScreen = getArguments().getString(
						Constant.Key.KEY_PARENT_SCREEN_NAME);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		final View mRootView = inflater.inflate(R.layout.layout_discover_celeb,
				null);
		ivBackground = (ImageView) mRootView.findViewById(R.id.iv_background);

		findViews(mRootView);
		setListeners();
		toolTipRelativeLayout = (ToolTipRelativeLayout) mRootView
				.findViewById(R.id.activity_main_tooltipRelativeLayout);
		toolTipRelativeLayout.setOnTouchListener(this);
		if (objectContainer.getProfile_picture() != null) {
			ImageLoader.getInstance().loadImage(
					MyUtilities.createThumbUrl(objectContainer
							.getProfile_picture()), new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String arg0, View arg1) {
						}

						@Override
						public void onLoadingFailed(String arg0, View arg1,
								FailReason arg2) {
						}

						@Override
						public void onLoadingComplete(String arg0, View arg1,
								Bitmap bitmap) {
							bitmap = MyUtilities.fastblur(bitmap, 1);
							ivBackground.setImageBitmap(bitmap);
						}

						@Override
						public void onLoadingCancelled(String arg0, View arg1) {
						}
					});
		}

		if (objectContainer != null) {

			nameTextView.setText(objectContainer.getFull_name());
			titleTextView.setText(objectContainer.getUser_title());
			picCircularImageView.setImageUrl(
					objectContainer.getProfile_picture(),
					objectContainer.getGender());
			if (!objectContainer.getQuestions().isEmpty()) {
				setQuestionDetails(0, quesOneTextView,
						quesOneUpvoteCountTextView, quesOneAskerTextView,
						upvoteOneView, tvQuesOneYou);

				if (objectContainer.getQuestions().size() >= 2) {
					rlSecondQuesView.setVisibility(View.VISIBLE);
					setQuestionDetails(1, quesTwoTextView,
							quesTwoUpvoteCountTextView, quesTwoAskerTextView,
							upvoteTwoView, tvQuesTwoYou);

				}
			}
			Log.d("follow_problem",
					"Follow status shashank "
							+ objectContainer.isIs_following());
			if (objectContainer.isIs_following()
					&& (mParentScreen.equals(Constant.ScreenSpecs.SCREEN_FEEDS) || mParentScreen
							.equals(Constant.ScreenSpecs.SCREEN_DISCOVER))) {
				Log.d("follow_problem", "settingTag " + FOLLOWING);

				toggleFollowButton(FOLLOWING, R.drawable.ic_followed);
				imgFollow.setVisibility(View.GONE);
			} else {
				Log.d("follow_problem", "settingTag " + FOLLOW);
				toggleFollowButton(FOLLOW, R.drawable.ic_add_profilepic);
			}

		}

		Controller.registerGAEvent("CelebQuestionsFragment");
		return mRootView;
	}

	private void findViews(View mRootView) {

		quesOneTextView = (TextView) mRootView.findViewById(R.id.tv_ques_one);
		quesTwoTextView = (TextView) mRootView.findViewById(R.id.tv_ques_two);
		upvoteOneView = (TextView) mRootView
				.findViewById(R.id.iv_ques_one_upvote);
		upvoteTwoView = (TextView) mRootView
				.findViewById(R.id.iv_ques_two_upvote);
		imgFollow = (ImageView) mRootView.findViewById(R.id.iv_follow);
		nameTextView = (TextView) mRootView.findViewById(R.id.tv_name);
		titleTextView = (TextView) mRootView.findViewById(R.id.tv_title);
		picCircularImageView = (CircularImageView) mRootView
				.findViewById(R.id.iv_profile_pic);
		quesOneAskerTextView = (TextView) mRootView
				.findViewById(R.id.tv_ques_one_asker);
		quesTwoAskerTextView = (TextView) mRootView
				.findViewById(R.id.tv_ques_two_asker);
		quesOneUpvoteCountTextView = (TextView) mRootView
				.findViewById(R.id.tv_ques_one_upvote_count);
		quesTwoUpvoteCountTextView = (TextView) mRootView
				.findViewById(R.id.tv_ques_two_upvote_count);
		rlSecondQuesView = (RelativeLayout) mRootView
				.findViewById(R.id.rl_question_two);
		ivAsk = (TextView) mRootView.findViewById(R.id.iv_ask);
		tvQuesOneYou = (TextView) mRootView.findViewById(R.id.tv_ques_one_you);
		tvQuesTwoYou = (TextView) mRootView.findViewById(R.id.tv_ques_two_you);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (getArguments().getInt("pos") == 0) {
			// updateWalkthrough();
		}
		updateWalkthrough();
	}

	private void addToolTip() {
		if (upvoteTipView != null)
			upvoteTipView.remove();
		if (objectContainer.getQuestions().size() >= 2
				&& upvoteTwoView.getVisibility() == View.VISIBLE) {
			upvoteTipView = Controller.addToolTipView(toolTipRelativeLayout,
					upvoteTwoView, getActivity(),
					"Questions with more requests\nget answered early.", false);
			upvoteTipView.setOnToolTipViewClickedListener(this);
		} else if (upvoteOneView.getVisibility() == View.VISIBLE) {
			upvoteTipView = Controller.addToolTipView(toolTipRelativeLayout,
					upvoteOneView, getActivity(),
					"Questions with more requests\nget answered early.", true);
			upvoteTipView.setOnToolTipViewClickedListener(this);
		}

		((ViewGroup) getView()).findViewById(R.id.iv_overlay).setVisibility(
				View.VISIBLE);

	}

	private void addAskToolTip() {

		if (!PrefUtils.getIsFirstAskAnimShown()
				&& objectContainer.getQuestions().size() < 2) {
			if (askToolTipView != null)
				askToolTipView.remove();
			askToolTipView = Controller.addToolTipView(toolTipRelativeLayout,
					ivAsk, getActivity(), "Ask your question to "
							+ objectContainer.getFirst_name(), true);
			askToolTipView.setOnToolTipViewClickedListener(this);
			if ((((ViewGroup) getView()).findViewById(R.id.iv_overlay))
					.getVisibility() != View.VISIBLE)
				((ViewGroup) getView()).findViewById(R.id.iv_overlay)
						.setVisibility(View.VISIBLE);
		}

	}

	private void setListeners() {
		upvoteOneView.setOnClickListener(this);
		upvoteTwoView.setOnClickListener(this);
		imgFollow.setOnClickListener(this);
		picCircularImageView.setOnClickListener(this);
		nameTextView.setOnClickListener(this);
		quesOneAskerTextView.setOnClickListener(this);
		quesTwoAskerTextView.setOnClickListener(this);
		ivAsk.setOnClickListener(this);
	}

	private void setQuestionDetails(int objectIndex,
			TextView questionBodyTextView, TextView questionUpvoteTextView,
			TextView questionAskerTextView, TextView upvoteView,
			TextView tvQuesYou) {

		QuestionObject question = objectContainer.getQuestions().get(
				objectIndex);
		Log.d("question is:", ""
				+ question.getQuestion_author().getFirst_name().toString());
		questionBodyTextView.setText(question.getBody());

		int askCount = question.getAsk_count();

		questionUpvoteTextView.setText(getAskCountString(askCount));

		questionAskerTextView.setText(TextUtils.concat(
				" ",
				Html.fromHtml("<b><font color='#a7a7a7'>"
						+ question.getQuestion_author().getFull_name()
						+ "</font></b>")));
		Log.e("upvoted", "Value0: "
				+ objectContainer.getQuestions().get(0).is_voted());
		// Check if the question is from the current user
		if (question.getQuestion_author().getId().equals(PrefUtils.getUserID()))
			upvoteView.setVisibility(View.GONE);
		else {
			upvoteView.setVisibility(View.VISIBLE);
			if (question.is_voted()) {
				upvoteView
						.setBackgroundResource(R.drawable.bg_rectangle_rednocorner);
				tvQuesYou.setText("You, ");
				questionUpvoteTextView.setText(getAskCountString(askCount - 1));
			}
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_ques_one_upvote:
			voting(0, upvoteOneView, quesOneUpvoteCountTextView, tvQuesOneYou);
			break;
		case R.id.iv_ques_two_upvote:
			voting(1, upvoteTwoView, quesTwoUpvoteCountTextView, tvQuesTwoYou);
			break;
		case R.id.iv_profile_pic:
		case R.id.iv_follow:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				if (imgFollow.getTag().equals(FOLLOW)) {
					followUser();
				} else if (imgFollow.getTag().equals(FOLLOWING)) {
					unfollowUser();
				}
			}
			break;
		case R.id.tv_name:
			/*
			 * if (Controller.isNetworkConnected(getActivity()))
			 * MpUtils.sendGenericEvent(objectContainer.getId(), null,
			 * mParentScreen, MpUtils.CELEB_PROFILE_TEXT, getActivity()
			 * .getApplicationContext());
			 */
			mBaseActivity.profileActivity(objectContainer.getId(), null);
			
			break;
		case R.id.tv_ques_one_asker:
			/*
			 * if (Controller.isNetworkConnected(getActivity()))
			 * MpUtils.sendGenericEvent(objectContainer
			 * .getQuestions().get(0).getQuestion_author().getId(), null,
			 * mParentScreen, MpUtils.PROFILE_QUESTION_1_ASKER, getActivity()
			 * .getApplicationContext());
			 */
mBaseActivity.profileActivity( objectContainer
					.getQuestions().get(0).getQuestion_author().getId(),
					objectContainer.getQuestions().get(0).getQuestion_author());
			
			break;
		case R.id.tv_ques_two_asker:
			/*
			 * if (Controller.isNetworkConnected(getActivity()))
			 * MpUtils.sendGenericEvent(objectContainer
			 * .getQuestions().get(1).getQuestion_author().getId(), null,
			 * mParentScreen, MpUtils.PROFILE_QUESTION_2_ASKER, getActivity()
			 * .getApplicationContext());
			 */
			mBaseActivity.profileActivity( objectContainer
					.getQuestions().get(1).getQuestion_author().getId(),
					objectContainer.getQuestions().get(1).getQuestion_author());
			
			break;
		case R.id.iv_ask:
			mBaseActivity.askActivity(objectContainer.getId());
			if (!PrefUtils.getIsFirstAskAnimShown())
				PrefUtils.setIsFirstAskAnimShown(true);
			break;
		}

	}

	private void voting(int itemIndex, TextView upvoteView,
			TextView questionUpvoteTextView, TextView tvQuestionYouText) {
		if (upvoteTipView != null)
			PrefUtils.setIsFirstUpvoteAnimShown(true);

		QuestionObject question = objectContainer.getQuestions().get(itemIndex);
		int askCount = question.getAsk_count();

		if (Controller.isNetworkConnectedWithMessage(getActivity())) {

			if (!question.is_voted()) {

				askCount++;
				upvote(question.getId());
				upvoteView
						.setBackgroundResource(R.drawable.bg_rectangle_rednocorner_transparent);
				upvoteView.setText("Answer Requested");
				tvQuestionYouText.setText("You, ");
				question.setIs_voted(true);
				// questionUpvoteTextView.setText(getAskCountString(askCount));

				MixpanelUtils.sendGenericEvent(objectContainer.getQuestions()
						.get(itemIndex).getId(), objectContainer.getUsername(),
						"1", mParentScreen, MixpanelUtils.QUESTION_UPVOTED,
						getActivity().getApplicationContext());
			} else {
				MixpanelUtils.sendGenericEvent(objectContainer.getQuestions()
						.get(itemIndex).getId(), objectContainer.getUsername(),
						"1", mParentScreen, MixpanelUtils.QUESTION_DOWNVOTED,
						getActivity().getApplicationContext());
				askCount--;
				downvote(question.getId());
				upvoteView
						.setBackgroundResource(R.drawable.bg_rectangle_rednocorner);
				upvoteView.setText("Request Answer");
				tvQuestionYouText.setText("");
				question.setIs_voted(false);
				// questionUpvoteTextView.setText(getAskCountString(askCount));
			}
			question.setAsk_count(askCount);

		}

		if (upvoteTipView != null) {
			upvoteTipView.remove();
			((ViewGroup) getView()).findViewById(R.id.iv_overlay)
					.setVisibility(View.GONE);
		}
	}

	private void upvote(String quesid) {
		Controller.upvoteQuestion(getActivity(), quesid, this);
	}

	private String getAskCountString(int askCount) {

		if (askCount > 0) {
			if (askCount == 1)
				return " and " + askCount + " "
						+ getString(R.string.question_upvote_singular);
			else
				return " and " + askCount + " "
						+ getString(R.string.question_upvote_plural);
		} else
			return "";
	}

	private void downvote(String quesid) {
		Controller.downvoteQuestion(getActivity(), quesid,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode, String message) {
					}

					@Override
					public void onRequestCompleted(Object responseObject) {

					}
				});
	}

	@Override
	public void onRequestStarted() {
	}

	@Override
	public void onRequestCompleted(Object responseObject) {
	}

	@Override
	public void onRequestError(int errorCode, String message) {
	}

	private void followUser() {
		String userName = objectContainer.getId();
		Controller.requestfollow(getActivity(), userName,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
						toggleFollowButton(FOLLOWING, R.drawable.ic_followed);

					}

					@Override
					public void onRequestError(int errorCode,
							final String message) {
						if (isAdded())
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									Controller
											.showToast(getActivity(),
													"Sorry, your request failed. Please try again after some time.");
									toggleFollowButton(FOLLOW,
											R.drawable.ic_add_profilepic);
								}
							});

					}

					@Override
					public void onRequestCompleted(Object responseObject) {
						/*
						 * MpUtils.sendGenericEvent(objectContainer.getId(),
						 * null, mParentScreen, MpUtils.CELEB_FOLLOWED_QUES,
						 * getActivity() .getApplicationContext());
						 */
					}
				});
	}

	private void unfollowUser() {
		String userId = objectContainer.getId();
		Controller.requestUnfollow(getActivity(), userId,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
						toggleFollowButton(FOLLOW, R.drawable.ic_add_profilepic);
					}

					@Override
					public void onRequestError(int errorCode, String message) {
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Controller
										.showToast(getActivity(),
												"Sorry, your request failed. Please try again after some time.");
								toggleFollowButton(FOLLOWING,
										R.drawable.ic_followed);
							}
						});
					}

					@Override
					public void onRequestCompleted(Object responseObject) {

					}
				});
	}

	private void toggleFollowButton(String tag, int drawable) {
		imgFollow.setTag(tag);
		imgFollow.setImageResource(drawable);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (upvoteTipView != null) {
			upvoteTipView.remove();
			((ViewGroup) getView()).findViewById(R.id.iv_overlay)
					.setVisibility(View.GONE);
		}
		if (askToolTipView != null) {
			askToolTipView.remove();
			((ViewGroup) getView()).findViewById(R.id.iv_overlay)
					.setVisibility(View.GONE);
		}
		return false;
	}

	@Override
	public void onToolTipViewClicked(ToolTipView toolTipView) {
		toolTipView.remove();
		((ViewGroup) getView()).findViewById(R.id.iv_overlay).setVisibility(
				View.GONE);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (upvoteTipView != null) {
			upvoteTipView.remove();
			((ViewGroup) getView()).findViewById(R.id.iv_overlay)
					.setVisibility(View.GONE);
		}
		if (askToolTipView != null) {
			askToolTipView.remove();
			((ViewGroup) getView()).findViewById(R.id.iv_overlay)
					.setVisibility(View.GONE);
		}
	}

	public void updateWalkthrough() {
		if (!PrefUtils.getIsFirstUpvoteAnimShown())
			addToolTip();
		if (!PrefUtils.getIsFirstAskAnimShown())
			addAskToolTip();
	}
}