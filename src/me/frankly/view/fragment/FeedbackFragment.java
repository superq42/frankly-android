package me.frankly.view.fragment;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.response.FeedBackList;
import me.frankly.model.response.FeedBackUser;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.RecordFeedBack;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FeedbackFragment extends DialogFragment implements
		RequestListener, OnClickListener {

	private ProgressBar mProgressBar;
	private ImageView user1ImgView, user2ImgView, user3ImgView,
			feedbackYesImgView, feedbackNoImgView;
	private LinearLayout imgContainerLayout;
	private final static String MEDIUM = "android-feedback";

	private RequestListener responseRequestListener = new RequestListener() {

		@Override
		public void onRequestStarted() {

		}

		@Override
		public void onRequestError(int errorCode, String message) {
			Log.d("FBQ", "error : " + message);
		}

		@Override
		public void onRequestCompleted(Object responseObject) {

			Log.d("FBQ", "response : " + responseObject);
		}
	};

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = new Dialog(getActivity(), R.style.Dialog_No_Border);

		// request a window without the title
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.feedback_dialog, container,
				false);
		setCancelable(false);
		mProgressBar = (ProgressBar) rootView.findViewById(R.id.prog_bar);
		imgContainerLayout = (LinearLayout) rootView
				.findViewById(R.id.img_container);
		user1ImgView = (ImageView) rootView.findViewById(R.id.feedback_img1);
		user2ImgView = (ImageView) rootView.findViewById(R.id.feedback_img2);
		user3ImgView = (ImageView) rootView.findViewById(R.id.feedback_img3);
		feedbackYesImgView = (ImageView) rootView
				.findViewById(R.id.feedback_btn_y);
		feedbackNoImgView = (ImageView) rootView
				.findViewById(R.id.feedback_btn_n);

		feedbackNoImgView.setOnClickListener(this);
		feedbackYesImgView.setOnClickListener(this);

		Controller.getPeoplePictures(getActivity(), 3, this);
		return rootView;
	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		Log.d("FB", "responseObject : " + responseObject);
		FeedBackList list = (FeedBackList) JsonUtils.objectify(
				responseObject.toString(), FeedBackList.class);
		final ArrayList<FeedBackUser> users = list.getUsers();
		Log.d("FB", "" + users.size());

		String[] profilePictures = {
				"https://s3.amazonaws.com/franklyapp/1135fba2a768429cafe0dde5fd1787b9/photos/2f161ebe98c811e48dac22000b5119ba.jpeg",
				"https://s3.amazonaws.com/franklyapp/cab4132c546c5c71df31033c71cd7175/photos/524764be8c1c11e48bcb22000b4c02a7.jpeg",
				"https://s3.amazonaws.com/franklyapp/3e68661c7ca54c529f245724c8595ee0/photos/33d4789ea08211e49c2322000b5119ba.jpeg" };

		for (int i = 0; i < 3; i = i + 1) {
			if (users.get(i).getProfilePicture() != null)
				profilePictures[i] = users.get(i).getProfilePicture();
		}

		final String[] pics = profilePictures;

		if (isAdded()) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {

					DisplayImageOptions dio = CircularImageView
							.getCustomDisplayOptions();
					ImageLoader.getInstance().displayImage(pics[0],
							user1ImgView, dio);
					ImageLoader.getInstance().displayImage(pics[1],
							user2ImgView, dio);
					ImageLoader.getInstance().displayImage(pics[2],
							user3ImgView, dio);
					imgContainerLayout.setVisibility(View.VISIBLE);
					mProgressBar.setVisibility(View.GONE);
				}
			});
		}

	}

	@Override
	public void onRequestError(int errorCode, String message) {
		Log.d("FBF", "errorCode  : " + errorCode + " message : " + message);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		switch (id) {
		case R.id.feedback_btn_y:
			Controller.recordResponse(getActivity(), "y", MEDIUM, "",
					responseRequestListener);
			dismiss();
			getActivity().finish();
			// PrefUtils.setFeedbackShown(true);
			if (isAdded())
				MyUtilities.openPlayStore(getActivity());
			break;
		case R.id.feedback_btn_n:
			Controller.recordResponse(getActivity(), "n", MEDIUM, "",
					responseRequestListener);
			getActivity().getSupportFragmentManager().beginTransaction()
					.add(R.id.feedback_frame, new RecordFeedBack()).commit();
			dismiss();
			// PrefUtils.setFeedbackShown(true);
			break;
		}
	}
}
