package me.frankly.view.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.view.activity.CropActivity;
import me.frankly.view.activity.RegisterUserActivity;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class PicAndBioFragment extends Fragment implements OnClickListener,
		OnEditorActionListener {

	public static final String KEY_USERNAME = "username";
	private static final int PIC_CROP = 202;

	private ImageView picImageView;
	private ImageView picFrameView;
	private EditText bioEditText;

	private String nowPicPath = null;
	private String nowBio = null;
	private Uri outputFileUri;
	private String TAG = "register";
	private View mRootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mayBeSetPicFromContacts();
	}

	private void mayBeSetPicFromContacts() {
		Cursor c = getActivity().getContentResolver().query(
				ContactsContract.Profile.CONTENT_URI, null, null, null, null);
		int count = c.getCount();
		String[] columnNames = c.getColumnNames();
		c.moveToFirst();
		int position = c.getPosition();
		if (count == 1 && position == 0) {
			for (int j = 0; j < columnNames.length; j++) {
				String columnName = columnNames[j];
				String columnValue = c.getString(c.getColumnIndex(columnName));
				Log.d("register", "column name: " + columnName + " cval: "
						+ columnValue);
				if (columnName.equalsIgnoreCase("photo_uri")
						&& columnValue != null) {

					Uri tUri = Uri.parse(columnValue);
					try {
						InputStream is = getActivity().getContentResolver()
								.openInputStream(tUri);
						File f = Controller.createTempFile(is,
								String.valueOf(System.currentTimeMillis())
										+ ".jpeg");
						if (f != null)
							nowPicPath = f.getAbsolutePath();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

				}

				// consume the values here
			}
		}
		c.close();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*
		 * Tracker t = MyApplication.getDefTracker();
		 * t.setScreenName("Register Pic and bio screen: " +
		 * ((RegisterUserActivity) getActivity()).mAuthSourceString); t.send(new
		 * HitBuilders.AppViewBuilder().build());
		 */
		mRootView = inflater.inflate(R.layout.add_info_signup, null);
		// TextView messageTextView = (TextView)
		// mRootView.findViewById(R.id.tv_message);
		picImageView = (ImageView) mRootView.findViewById(R.id.iv_add_info_pic);
		picFrameView = (ImageView) mRootView.findViewById(R.id.iv_pic_frame);
		bioEditText = (EditText) mRootView.findViewById(R.id.et_fullname);
		// messageTextView.setText("Hey! " + paramUsername);
		if (nowPicPath != null) {
			Uri tempUri = Uri.parse(nowPicPath);
			picImageView.setImageURI(Uri.parse(nowPicPath));
			Log.d("register", "pic path from uri: " + tempUri.getPath()
					+ " encoded path: " + tempUri.getEncodedPath() + " toStr: "
					+ tempUri.toString());

		}
		if (nowBio != null)
			bioEditText.setText(nowBio);
		bioEditText.setOnEditorActionListener(this);
		picImageView.setOnClickListener(this);
		mRootView.findViewById(R.id.iv_pic_frame).setOnClickListener(this);
		// mRootView.findViewById(R.id.iv_back).setOnClickListener(this);
		mRootView.findViewById(R.id.tv_next).setOnClickListener(this);
		((RegisterUserActivity) getActivity()).clearPicAndBio();
		bioEditText.requestFocus();
		mRootView.addOnLayoutChangeListener(new OnLayoutChangeListener() {

			@Override
			public void onLayoutChange(View v, int left, int top, int right,
					int bottom, int oldLeft, int oldTop, int oldRight,
					int oldBottom) {

			}
		});
		mRootView.getViewTreeObserver().addOnGlobalLayoutListener(
				keyboardLayoutListener);
		return mRootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// case R.id.iv_pic_:
		case R.id.iv_pic_frame:
			updatePic();
			break;
		/*
		 * case R.id.iv_back: getActivity().onBackPressed(); break;
		 */
		case R.id.tv_next:
			mayBeGetAndSetFeilds();
			break;
		}
	}

	private void mayBeGetAndSetFeilds() {
		nowBio = bioEditText.getText().toString();
		/*
		 * if (nowPicPath == null) Toast.makeText(getActivity(),
		 * "Select a profile pic", Toast.LENGTH_SHORT).show(); else if (nowBio
		 * == null || nowBio.length() < 4) Toast.makeText(getActivity(),
		 * "Enter your bio", Toast.LENGTH_SHORT).show(); else
		 */
		((RegisterUserActivity) getActivity()).onPicAndBioSelected(nowPicPath,
				nowBio);

	}

	private void updatePic() {

		final File root = new File(Environment.getExternalStorageDirectory()
				+ File.separator + "MyDir" + File.separator);
		root.mkdirs();
		final String fname = System.currentTimeMillis() + ".jpg";
		final File sdImageMainDirectory = new File(root, fname);
		outputFileUri = Uri.fromFile(sdImageMainDirectory);

		// Camera.
		final List<Intent> cameraIntents = new ArrayList<Intent>();
		final Intent captureIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		final PackageManager packageManager = getActivity().getPackageManager();
		final List<ResolveInfo> listCam = packageManager.queryIntentActivities(
				captureIntent, 0);
		for (ResolveInfo res : listCam) {
			Log.d(TAG, "Inside update pic");
			final String packageName = res.activityInfo.packageName;
			final Intent intent = new Intent(captureIntent);
			intent.setComponent(new ComponentName(res.activityInfo.packageName,
					res.activityInfo.name));
			intent.setPackage(packageName);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
			cameraIntents.add(intent);
		}

		// Filesystem.
		final Intent galleryIntent = new Intent();
		galleryIntent.setType("image/*");
		galleryIntent.setAction(Intent.ACTION_PICK);

		// Chooser of filesystem options.
		final Intent chooserIntent = Intent.createChooser(galleryIntent,
				"Select Source");

		// Add the camera options.
		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
				cameraIntents.toArray(new Parcelable[] {}));

		// startActivityForResult(chooserIntent,
		// EditProfileActivity.REQUEST_UPDATE_PROFILE_PIC);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.e(TAG, " onActivityResult result  request:" + requestCode
				+ " result :" + resultCode);
		if (requestCode == PIC_CROP) {
			Log.d(TAG, " Returning from crop ");
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					Bundle extras = data.getExtras();
					String image_path = extras
							.getString(CropActivity.IMAGE_PATH);
					Log.d(TAG + " path", image_path + "");
					File file = new File(image_path);
					nowPicPath = file.getPath();
					picImageView.setImageURI(Uri.fromFile(file));

				}

			} else {

			}

		}

	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_DONE)
			mayBeGetAndSetFeilds();
		return false;
	}

	OnGlobalLayoutListener keyboardLayoutListener = new OnGlobalLayoutListener() {

		@Override
		public void onGlobalLayout() {
			if (isAdded()) {
				int heightDiff = getActivity().getWindowManager()
						.getDefaultDisplay().getHeight()
						- mRootView.getHeight();
				if (heightDiff > 100) {
					if (picImageView.getVisibility() == View.VISIBLE) {
						picImageView.setVisibility(View.GONE);
						picFrameView.setVisibility(View.GONE);
					}

				} else {
					if (picImageView.getVisibility() != View.VISIBLE) {
						picImageView.setVisibility(View.VISIBLE);
						picFrameView.setVisibility(View.VISIBLE);
					}
				}

			}
		}
	};
}
