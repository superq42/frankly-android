package me.frankly.view.fragment;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.response.FeedBackList;
import me.frankly.model.response.FeedBackUser;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.view.CircularImageView;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FeedBackQuestionFragment extends Fragment implements
		RequestListener {

	private DisplayImageOptions dio;
	
	private ImageView powerUserImgView1, powerUserImgView2, powerUserImgView3,
			powerUserImgView4, powerUserImgView5;
	private final static String MEDIUM = "android-feedback";

	private RequestListener responseRequestListener = new RequestListener() {

		@Override
		public void onRequestStarted() {

		}

		@Override
		public void onRequestError(int errorCode, String message) {
			Log.d("FBQ", "error : " + message);
		}

		@Override
		public void onRequestCompleted(Object responseObject) {

			Log.d("FBQ", "response : " + responseObject);
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View rootView = inflater.inflate(R.layout.layout_feed_back_popup_first,
				container, false);
		Log.d("Feedback", "onCreateView");
		
		dio = CircularImageView.getCustomDisplayOptions();			
		
		powerUserImgView1 = (ImageView) rootView.findViewById(R.id.power_user1);
		powerUserImgView2 = (ImageView) rootView.findViewById(R.id.power_user2);
		powerUserImgView3 = (ImageView) rootView.findViewById(R.id.power_user3);
		powerUserImgView4 = (ImageView) rootView.findViewById(R.id.power_user4);
		powerUserImgView5 = (ImageView) rootView.findViewById(R.id.power_user5);
		ImageView feedbackYes = (ImageView) rootView
				.findViewById(R.id.iv_btn_good);
		ImageView feedbackNo = (ImageView) rootView
				.findViewById(R.id.iv_btn_bad);
		feedbackNo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				MyUtilities.sendEmail(getActivity());
				PrefUtils.setFeedbackShown(true);
				getActivity().finish();
				Controller.recordResponse(getActivity(), "n", MEDIUM, "",
						responseRequestListener);
			}
		});

		feedbackYes.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				MyUtilities.openPlayStore(getActivity());
				PrefUtils.setFeedbackShown(true);
				getActivity().finish();
				Controller.recordResponse(getActivity(), "y", MEDIUM, "",
						responseRequestListener);
			}
		});

		Controller.getPeoplePictures(getActivity(), 5, this);

		return rootView;
	}

	@Override
	public void onRequestStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		Log.d("FB", "responseObject : " + responseObject);
		FeedBackList list = (FeedBackList) JsonUtils.objectify(
				responseObject.toString(), FeedBackList.class);
		final ArrayList<FeedBackUser> users = list.getUsers();
		Log.d("FB", "" + users.size());
		if (isAdded()) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (users.size() > 4
							&& users.get(4).getProfilePicture() != null) {
						ImageLoader.getInstance().displayImage(
								users.get(4).getProfilePicture(),
								powerUserImgView5, dio);
					} else {
						manageError(4);
					}

					if (users.size() > 1
							&& users.get(1).getProfilePicture() != null) {
						ImageLoader.getInstance().displayImage(
								users.get(1).getProfilePicture(),
								powerUserImgView2, dio);
					} else {
						manageError(1);
					}

					if (users.size() > 2
							&& users.get(2).getProfilePicture() != null) {
						ImageLoader.getInstance().displayImage(
								users.get(2).getProfilePicture(),
								powerUserImgView3, dio);
					} else {
						manageError(2);
					}

					if (users.size() > 3
							&& users.get(3).getProfilePicture() == null) {
						ImageLoader.getInstance().displayImage(
								users.get(3).getProfilePicture(),
								powerUserImgView4, dio);
					} else {
						manageError(3);
					}

					if (users.size() > 0
							&& users.get(0).getProfilePicture() == null) {
						ImageLoader.getInstance().displayImage(
								users.get(0).getProfilePicture()/* null */,
								powerUserImgView1, dio);
					} else {
						manageError(0);
					}
				}
			});
		}
	}

	@Override
	public void onRequestError(int errorCode, String message) {
		if (isAdded()) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					manageError(4);
					manageError(1);
					manageError(2);
					manageError(3);
					manageError(0);
				}
			});
		}
	}

	/**
	 * 
	 * @param index
	 */
	private void manageError(int index) {
		switch (index) {
		case 0: {
			ImageLoader
					.getInstance()
					.displayImage(
							"https://s3.amazonaws.com/franklyapp/1ae70c083cc94c0984629250e91d9c6b/photos/8b1230f0963311e480cf22000b5119ba.jpeg"/* null */,
							powerUserImgView1, dio);
			break;
		}
		case 1: {
			ImageLoader
					.getInstance()
					.displayImage(
							"https://s3.amazonaws.com/franklyapp/3e68661c7ca54c529f245724c8595ee0/photos/33d4789ea08211e49c2322000b5119ba.jpeg",
							powerUserImgView2, dio);
			break;
		}
		case 2: {
			ImageLoader
					.getInstance()
					.displayImage(
							"https://s3.amazonaws.com/franklyapp/cab4132c546c5c71df31033c71cd7175/photos/524764be8c1c11e48bcb22000b4c02a7.jpeg",
							powerUserImgView3, dio);
			break;
		}
		case 3: {
			ImageLoader
					.getInstance()
					.displayImage(
							"https://s3.amazonaws.com/franklyapp/1135fba2a768429cafe0dde5fd1787b9/photos/2f161ebe98c811e48dac22000b5119ba.jpeg",
							powerUserImgView4, dio);
			break;
		}
		case 4: {
			ImageLoader
					.getInstance()
					.displayImage(
							"https://s3.amazonaws.com/franklyapp/cab4132c5447879cdf31033c4c029efd/photos/e735b1048c1511e4af7622000b4c02a7.jpeg",
							powerUserImgView5, dio);
			break;
		}
		}
	}
}
