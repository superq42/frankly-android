package me.frankly.view.fragment;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.SDKPlayer.IPlayer.IPlayerStateChangedListener;
import me.frankly.config.Constant;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.RequestListener;
import me.frankly.listener.SwipeAnimationDoneListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.ImageUtil;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.SharePackage;
import me.frankly.util.ShareUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.ShareDialog;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.FranklyHomeActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.nhaarman.supertooltips.ToolTipView.OnToolTipViewClickedListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FeedProfileFragment extends SDKVideoFragment implements
		SwipeAnimationDoneListener, IPlayerStateChangedListener,
		OnToolTipViewClickedListener, UserFollowListener {

	private String TAG = "FeedProfileFragment";
	private UniversalUser mUser;
	private ImageView imgCoverImage;
	private TextureView mTextureView;
	private View pauseView;
	private ImageView loadingView;
	private AnimationDrawable loaderAnimation;
	private CircularImageView cvProfilepic;
	private boolean isLoaderAnimating = false;
	private View playView;
	private LinearLayout mShareLayout;
	private ImageView ivShareSpecific1;
	private ImageView ivShareSpecific2;
	private ImageView ivFollow;

	private TextView tv_tap_play, tv_ask, tv_Name, tv_Bio, tvRank;
	private String mParentScreen;
	private ToolTipView askToolTipView, visitProfileToolTipView;
	private ToolTipRelativeLayout toolTipRelativeLayout;
	private BaseActivity mBaseActivity;
	private RelativeLayout rl_top_bar, rl_below_top_bar;
	private final static int REQUEST_CODE_SHARE = 1;
	private boolean shouldShowTapToPlay = true;

	// public static ArrayList<SharePackage> packageList =
	// FranklyHomeActivity.packageList;
	private ArrayList<SharePackage> packageList;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_feed_profile, container,
				false);
		if (getActivity() instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) getActivity();
		}
		Log.d("Network",
				"This is my user: "
						+ getArguments()
								.getString(Constant.Key.KEY_USER_OBJECT));
		if (getArguments() != null) {
			mUser = (UniversalUser) JsonUtils.objectify(getArguments()
					.getString(Constant.Key.KEY_USER_OBJECT),
					UniversalUser.class);
			mParentScreen = getArguments().getString(
					Constant.Key.KEY_PARENT_SCREEN_NAME);
		}

		Log.d(TAG, mUser.getFull_name() + " onCreate");

		loadingView = (ImageView) view.findViewById(R.id.iv_loader);
		loadingView.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) loadingView.getDrawable();
		mShareLayout = (LinearLayout) view.findViewById(R.id.share_ll);
		toolTipRelativeLayout = (ToolTipRelativeLayout) view
				.findViewById(R.id.activity_main_tooltipRelativeLayout);

		pauseView = view.findViewById(R.id.frag_profile_pauseview);
		playView = view.findViewById(R.id.iv_play);

		imgCoverImage = (ImageView) view
				.findViewById(R.id.frag_profile_coverpic);

		cvProfilepic = (CircularImageView) view
				.findViewById(R.id.frag_profile_img);

		mTextureView = (TextureView) view
				.findViewById(R.id.atomic_post_textureview);
		setupShareView(view);
		setMyProfile(view);
		showCoverPhoto();
		setVideoViews();

		tv_tap_play = (TextView) view.findViewById(R.id.tv_tap_text);
		rl_top_bar = (RelativeLayout) view
				.findViewById(R.id.rl_top_title_gradient);
		rl_below_top_bar = (RelativeLayout) view
				.findViewById(R.id.rl_below_action_bar);
		UniversalActionListenerCollection.getInstance().addUserFollowListener(
				this, mUser.getId());
		if (getActivity() instanceof FranklyHomeActivity)
			((FranklyHomeActivity) getActivity())
					.addSwipeAnimationDoneListener(this);
		return view;
	}

	private void setupShareView(View view) {
		Log.d(TAG, TAG + " setupShareView");
		packageList = ShareUtils.getSharingPackages(getActivity(), Constant.ScreenSpecs.SCREEN_PROFILE);
		ImageView ivShare = (ImageView) view.findViewById(R.id.share_widget1);
		ivShareSpecific1 = (ImageView) view.findViewById(R.id.share_widget2);
		ivShareSpecific2 = (ImageView) view.findViewById(R.id.share_widget3);
		ivShare.setImageResource(R.drawable.btn_share_upper);
		ivShare.setOnClickListener(this);
		if (packageList != null && packageList.size() > 0) {
			for (int i = 0; i < 2; i++) {
				if (i == 0)
					ShareUtils.setupSharePackage(packageList.get(i),
							ivShareSpecific1, ((OnClickListener) this));
				else if (i == 1 && packageList.size() > 1)
					ShareUtils.setupSharePackage(packageList.get(i),
							ivShareSpecific2, ((OnClickListener) this));
				else
					ivShareSpecific2.setVisibility(View.GONE);
			}
		} else {
			ivShare.setVisibility(View.VISIBLE);
			ivShare.setOnClickListener(this);
			ivShareSpecific1.setVisibility(View.GONE);
			ivShareSpecific2.setVisibility(View.GONE);
		}

	}

	@Override
	public void onResume() {
		Log.d(TAG, mUser.getFull_name() + " onResume");
		super.onResume();
		// setVideoViews();

		if (getArguments().getInt("pos") == 0) {
			updateWalkthrough();
		}
		addFollowToolTip();
		addAskToolTip();
	}

	public void updateWalkthrough() {
		Log.d("asktooltip", "inside updatewalkthrough");
		if (!PrefUtils.getIsFirstTapAnimShown()
				&& mBaseActivity instanceof FranklyHomeActivity
				&& ((FranklyHomeActivity) mBaseActivity).findViewById(
						R.id.rl_swipe_animation).getVisibility() == View.GONE && shouldShowTapToPlay )
			tapAnimation();
		else if (tv_tap_play.getVisibility() == View.VISIBLE) {
			tv_tap_play.clearAnimation();
			tv_tap_play.setVisibility(View.GONE);
		}
		addFollowToolTip();
		addAskToolTip();
	}

	@Override
	public void onPause() {
		Log.e("check1", "inside onPause");
		if (getActivity() instanceof FranklyHomeActivity)
			((FranklyHomeActivity) getActivity())
					.removeSwipeAnimationDoneListner(this);
		super.onPause();
	}

	private void showCoverPhoto() {
		if (mUser.getCover_picture() != null) {
			ImageLoader.getInstance().displayImage(mUser.getCover_picture(),
					imgCoverImage);
		}
		if (mUser.getProfile_picture() != null) {
			DisplayImageOptions options = CircularImageView
					.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
			cvProfilepic.setImageUrl(options, mUser.getProfile_picture());
		}
	}

	private void setVideoViews() {
		if (mUser.getProfile_video() != null) {
			setAutoPlay(false);

			String video_url = ImageUtil.getNetworkBasedVideoUrl(mUser
					.getProfile_videos());
			if (video_url == null)
				video_url = mUser.getProfile_video();
			setVideoUrl(video_url);

			setInitialView(pauseView);
			setPauseView(pauseView);
			setPostPlayView(pauseView);
			setVideoTextureView(mTextureView);

			mTextureView.setOnClickListener(this);
			pauseView.setOnClickListener(this);
		} else {
			mTextureView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onPlayerPaused() {
		super.onPlayerPaused();
		pauseView.setBackgroundColor(Color.parseColor("#40151515"));
		Log.i(TAG, "OnPLayerPaused" + mShareLayout.getVisibility());
		if (mShareLayout.getVisibility() != View.VISIBLE)
			mShareLayout.setVisibility(View.VISIBLE);
		addFollowToolTip();
		addAskToolTip();
		topGradientVisible();
	}

	@Override
	public void onPlayerCompleted() {
		super.onPlayerCompleted();
		if (mShareLayout.getVisibility() != View.VISIBLE)
			mShareLayout.setVisibility(View.VISIBLE);
		addFollowToolTip();
		addAskToolTip();
		topGradientVisible();
		if(!PrefUtils.getIsFirstSwipeAnimShown() && getActivity() instanceof FranklyHomeActivity)
		{
			((FranklyHomeActivity)getActivity()).resetSwipeTimer();
		}
	}

	@Override
	public void onPlayerPrepared() {
		super.onPlayerPrepared();
		topGradientInvisible();
	}

	@Override
	public void onPlayerResumed() {
		super.onPlayerResumed();
		topGradientInvisible();
	}

	// TODO Fix profile navigation mixpanel - superq42

	private void topGradientInvisible() {
		rl_top_bar.setVisibility(View.GONE);
		rl_below_top_bar.setVisibility(View.GONE);
	}

	private void topGradientVisible() {
		rl_top_bar.setVisibility(View.VISIBLE);
		rl_below_top_bar.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// Click listener for Ask Button
		case R.id.frag_feed_profile_ask_btn:
			if (askToolTipView != null) {
				askToolTipView.remove();
				getPauseView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				getInitialView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				askToolTipView = null;
				PrefUtils.setIsFirstAskProfileAnimShown(true);
			}
			mBaseActivity.askActivity(mUser);
			registerMixpanelEvent(MixpanelUtils.ASK_ME, 0, "");
			break;
		// Opens profile of user
		case R.id.frag_proilfe_name:
			if (visitProfileToolTipView != null) {
				visitProfileToolTipView.remove();
				getPauseView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				getInitialView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				visitProfileToolTipView = null;
				PrefUtils.setIsFirstFollowAnimShown(true);
			}

			mBaseActivity.profileActivity(null, mUser);

			registerMixpanelEvent(MixpanelUtils.NAVIGATION_PROFILE, 0, "");
			break;
		// For following user
		case R.id.frag_profile_img_plus:
		case R.id.frag_profile_img:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				toggleFollow();
			}
			break;

		case R.id.frag_profile_pauseview:
			// ((HomeActivity) getActivity()).stopSwipeTimer();
			tv_tap_play.setVisibility(View.GONE);
			playView.setVisibility(View.GONE);
			if (visitProfileToolTipView != null) {
				visitProfileToolTipView.remove();
				getPauseView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				getInitialView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				visitProfileToolTipView = null;

			}
			if (askToolTipView != null) {
				askToolTipView.remove();
				getPauseView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				getInitialView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				askToolTipView = null;
			}
			if(!PrefUtils.getIsFirstSwipeAnimShown() && getActivity() instanceof FranklyHomeActivity)
			{
				((FranklyHomeActivity)getActivity()).stopSwipeTimer();
			}
			togglePlayState();
			break;
		case R.id.share_widget1:
			showCustomDialog();
			registerMixpanelEvent(MixpanelUtils.SHARE_DIALOG, 0, "");
			break;
		// Sharing on specific networks
		case R.id.share_widget2:
			// packageList.get(0).viewCount++;
			shareSpecific(v);
			break;
		case R.id.share_widget3:
			// packageList.get(1).viewCount++;
			shareSpecific(v);
			registerMixpanelEvent(MixpanelUtils.SHARE_JEWEL, 0, "");
			break;

		}
		super.onClick(v);
	}

	private void setMyProfile(View view) {

		tv_Name = (TextView) view.findViewById(R.id.frag_proilfe_name);
		tv_Bio = (TextView) view.findViewById(R.id.frag_proilfe_info);
		tvRank = (TextView) view.findViewById(R.id.frag_proilfe_rank);
		tv_Bio.setText(mUser.getBio());
		tv_Name.setText(mUser.getFull_name());
		tv_Name.setOnClickListener(this);
		tv_Bio.setOnClickListener(this);
		tvRank.setText(mUser.getUser_title());
		tv_ask = (TextView) view.findViewById(R.id.frag_feed_profile_ask_btn);
		tv_ask.setOnClickListener(this);
		ivFollow = (ImageView) view.findViewById(R.id.frag_profile_img_plus);
		ivFollow.setOnClickListener(this);
		cvProfilepic.setOnClickListener(this);
		if (mUser.is_following()) {
			ivFollow.setImageResource(R.drawable.ic_followed);
		}
	}

	@Override
	protected View getLoadingView(boolean show) {
		if (loaderAnimation != null) {
			if (!show && isLoaderAnimating) {
				loaderAnimation.stop();
				isLoaderAnimating = false;
			} else if (show && !isLoaderAnimating) {
				loaderAnimation.start();
				isLoaderAnimating = true;
			}
		}
		return loadingView;
	}

	private void toggleFollow() {
		if (!mUser.is_following()) {
			mUser.setIs_following(true);
			followRequest();
			ivFollow.setImageResource(R.drawable.ic_followed);
			registerMixpanelEvent(MixpanelUtils.USER_FOLLOWED, 0, "");
		} else {
			unfollowRequest();
			ivFollow.setImageResource(R.drawable.ic_add_profilepic);
			registerMixpanelEvent(MixpanelUtils.USER_UNFOLLOWED, 0, "");
			mUser.setIs_following(false);
		}
		UniversalActionListenerCollection.getInstance().onUserFollowChanged(
				mUser.getId(), mUser.is_following());
	}

	@Override
	protected void hideAllViewsExceptCoverImage() {

		Log.d(TAG, TAG + "hideAllViewsExceptCoverImage");
		playView.setVisibility(View.GONE);
		if (!PrefUtils.getIsFirstTapAnimShown()
				&& mParentScreen
						.equalsIgnoreCase(Constant.ScreenSpecs.SCREEN_CHANNEL)) {
			tv_tap_play.clearAnimation();
			tv_tap_play.setVisibility(View.GONE);
			PrefUtils.setIsFirstTapAnimShown(true);

		} else if (PrefUtils.getIsFirstTapAnimShown()
				&& tv_tap_play.getVisibility() == View.VISIBLE) {
			Log.d(TAG, "tap to play is visible");
			tv_tap_play.clearAnimation();
			tv_tap_play.setVisibility(View.GONE);
		}
		// getLoadingView(false);
	}

	@Override
	public View getDefaultCoverImage() {
		return imgCoverImage;
	}

	@Override
	public String[] getUserDetails() {
		return new String[] { mUser.getUsername(),
				String.valueOf(mUser.getUser_type()) };

	}

	private void showCustomDialog() {

		ShareDialog cdd = new ShareDialog(mBaseActivity,

		R.layout.share_dialog_post_layout, mUser, 2, mUser.getId().equals(
				PrefUtils.getUserID()), "FEED", packageList);

		cdd.show();
	}

	private void shareSpecific(View v) {
		String packageName = null;
		Log.d("postdeep", mUser.getWeb_link());
		String shareText = getString(R.string.share_text, mUser.getFull_name());
		shareText = shareText + " " + mUser.getWeb_link();
		Log.d("postdeep", shareText);
		String shareSubject = getString(R.string.share_subject,
				mUser.getFirst_name());

		if (v.getTag() != null) {
			packageName = (String) v.getTag();
			MixpanelUtils.sendShareJewelEvent(packageName, mUser.getUsername(),
					String.valueOf(mUser.getUser_type()),
					ScreenSpecs.SCREEN_DISCOVER_PROFILE, getActivity());
			ShareUtils.shareLinkUsingIntent(shareText, getActivity(),
					packageName, REQUEST_CODE_SHARE);
		} else {
			Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
					+ shareSubject));
			intentsms.putExtra("sms_body", shareText);
			startActivity(intentsms);
		}
	}

	private void followRequest() {
		Controller.requestfollow(getActivity(), mUser.getId(),
				new RequestListener() {

					@Override
					public void onRequestStarted() {

					}

					@Override
					public void onRequestError(int errorCode, String message) {
						if (isAdded())
							getActivity().runOnUiThread(new Runnable() {

								@Override
								public void run() {
									Controller
											.showToast(
													getActivity(),
													"Sorry, your request couldn't be completed at this time. \n"
															+ "This might be due to a bad internet connection.");

								}
							});

					}

					@Override
					public void onRequestCompleted(Object responseObject) {

					}
				});
	}

	private void unfollowRequest() {
		Controller.requestUnfollow(getActivity(), mUser.getId(),
				new RequestListener() {

					@Override
					public void onRequestStarted() {

					}

					@Override
					public void onRequestError(int errorCode, String message) {
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Controller
										.showToast(
												getActivity(),
												"Sorry, your request couldn't be completed at this time. \n"
														+ "This might be due to a bad internet connection.");

							}
						});
					}

					@Override
					public void onRequestCompleted(Object responseObject) {

					}
				});
	}

	private void tapAnimation() {
		Log.d("swipe", "show tap");
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);
		fadeIn.setRepeatMode(Animation.REVERSE);
		fadeIn.setRepeatCount(Animation.INFINITE); // made the animation repeat
													// infinte times
		tv_tap_play.setVisibility(View.VISIBLE);
		tv_tap_play.setShadowLayer(1, 1, 2, Color.parseColor("#CC000000"));
		tv_tap_play.setAnimation(fadeIn);
		Animation playViewFadeIn = new AlphaAnimation(0, 1);
		playViewFadeIn.setInterpolator(new DecelerateInterpolator());
		playViewFadeIn.setDuration(1000);
		playViewFadeIn.setRepeatCount(0);
		playView.setAnimation(playViewFadeIn);
	}

	private void addFollowToolTip() {
		Log.e("check1", "feed profile inside addtooltip");
		if (!PrefUtils.getIsFirstFollowAnimShown()
				&& PrefUtils.getIsFirstSwipeAnimShown()
				&& PrefUtils.getIsFirstTapAnimShown()
				&& PrefUtils.getVideoPlayCount() >= 2
				&& visitProfileToolTipView == null) {
			Log.e("check1", "feed profile inside 3");
			visitProfileToolTipView = Controller.addToolTipView(
					toolTipRelativeLayout, tv_Name, mBaseActivity,
					"Tap to visit Profile", false);
			pauseView.findViewById(R.id.iv_overlay).setVisibility(View.VISIBLE);
			visitProfileToolTipView.setOnToolTipViewClickedListener(this);
		} else if (PrefUtils.getIsFirstFollowAnimShown()
				&& visitProfileToolTipView != null) {
			visitProfileToolTipView.remove();
			getPauseView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getInitialView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			visitProfileToolTipView = null;
		}
	}

	private void addAskToolTip() {
		Log.d("asktooltip", "outside");
		Log.e("check1", "feed profile inside addtooltip");
		if (PrefUtils.getIsFirstFollowAnimShown()
				&& PrefUtils.getIsFirstFollowProfileAnimShown()
				&& !PrefUtils.getIsFirstAskProfileAnimShown()
				&& PrefUtils.getVideoPlayCount() >= 5 && askToolTipView == null) {
			Log.d("asktooltip", "inside");
			askToolTipView = Controller.addToolTipView(toolTipRelativeLayout,
					tv_ask, mBaseActivity, "Tap to Ask", false);
			pauseView.findViewById(R.id.iv_overlay).setVisibility(View.VISIBLE);
			askToolTipView.setOnToolTipViewClickedListener(this);
		} else if (PrefUtils.getIsFirstAskProfileAnimShown()
				&& askToolTipView != null) {
			askToolTipView.remove();
			getPauseView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getInitialView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			askToolTipView = null;
		}
	}


	@Override
	public void onToolTipViewClicked(ToolTipView toolTipView) {
		if (visitProfileToolTipView != null) {
			visitProfileToolTipView.remove();
			visitProfileToolTipView = null;
		}
		if (askToolTipView != null) {
			askToolTipView.remove();
			askToolTipView = null;
		}
		pauseView.findViewById(R.id.iv_overlay).setVisibility(View.GONE);
	}


	public void registerMixpanelEvent(String event_name, int share_type,
			String profile_role) {
		if (isAdded())
			MixpanelUtils.sendGenericEvent(mUser.getId(), mUser.getUsername(),
					String.valueOf(mUser.getUser_type()),
					Constant.ScreenSpecs.SCREEN_DISCOVER_PROFILE, event_name,
					getActivity());
	}

	@Override
	public void onSwipeStarted() {
		shouldShowTapToPlay = false;
		Log.d("swipe", "hidden");
		tv_tap_play.clearAnimation();
		tv_tap_play.setVisibility(View.GONE);

	}

	@Override
	public void onSwipeDone() {
		shouldShowTapToPlay = true;
	}
	
	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		if (mUser.getId().equals(userId)) {
			mUser.setIs_following(isFollowed);
			if (isAdded()) {
				if (isFollowed)
					ivFollow.setImageResource(R.drawable.ic_followed);
				else
					ivFollow.setImageResource(R.drawable.ic_add_profilepic);

			}
		}

	}

	@Override
	public void onDestroy() {
		UniversalActionListenerCollection.getInstance()
				.removeUserFollowListener(this, mUser.getId());
		super.onDestroy();
	}
}
