package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.util.FileUtils;
import me.frankly.util.FileUtils.FileType;
import me.frankly.util.PrefUtils;
import me.frankly.view.croputil.ImageProcess;
import me.frankly.view.croputil.MoveGestureDetector;
import me.frankly.view.croputil.RotateGestureDetector;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImageCropFragment extends Fragment implements OnTouchListener,
		OnClickListener {

	private String mImagePath;
	private ImageView ivProfilePic;
	private ImageView ivTemplate;
	private int mScreenWidth;
	private int mScreenHeight;

	private Matrix mMatrix = new Matrix();
	private float mScaleFactor = 1f;
	private float mRotationDegrees = 0.f;
	private float mFocusX = 0.f;
	private float mFocusY = 0.f;
	private int mImageHeight, mImageWidth;
	private ScaleGestureDetector mScaleDetector;
	private RotateGestureDetector mRotateDetector;
	private MoveGestureDetector mMoveDetector;
	private ProgressDialog mProgressDialog;

	private int mTemplateWidth;
	private int mTemplateHeight;

	private final static int IMG_MAX_SIZE_MDPI = 400;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null
				&& getArguments().containsKey(Constant.Key.CROP_IMAGE_URL))
			mImagePath = getArguments().getString(Constant.Key.CROP_IMAGE_URL);
		mScreenWidth = Constant.SCREEN.WIDTH;
		mScreenHeight = Constant.SCREEN.HEIGHT;
		Log.i("CropFragment", "PAth  " + mImagePath);

	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_crop_image,
				container, false);
		ivProfilePic = (ImageView) rootView.findViewById(R.id.frag_crop_img);
		ivTemplate = (ImageView) rootView.findViewById(R.id.frag_crop_template);
		ivProfilePic.setOnTouchListener(this);
		rootView.findViewById(R.id.frag_crop_btn).setOnClickListener(this);

		mTemplateWidth = 500;
		mTemplateHeight = 500;
		Bitmap faceTemplate = Bitmap.createBitmap(mScreenWidth, mScreenHeight,
				Bitmap.Config.ARGB_4444);
		Canvas c = new Canvas(faceTemplate);
		drawCircle(c, 500, 500);

		// img.setScaleType(ScaleType.FIT_CENTER);

		ivTemplate.setImageBitmap(faceTemplate);

		setSelectedImage(mImagePath);

		// View is scaled by matrix, so scale initially
		// mMatrix.postScale(mScaleFactor, mScaleFactor);
		ivProfilePic.setImageMatrix(mMatrix);

		// Setup Gesture Detectors
		mScaleDetector = new ScaleGestureDetector(getActivity(),
				new ScaleListener());
		mRotateDetector = new RotateGestureDetector(getActivity(),
				new RotateListener());
		mMoveDetector = new MoveGestureDetector(getActivity(),
				new MoveListener());
		rootView.findViewById(R.id.frag_crop_back_btn).setOnClickListener(this);

		return rootView;
	}

	public void cropImage() {

		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setMessage("Cropping Image\nPlease Wait.....");
		mProgressDialog.show();

		ivProfilePic.buildDrawingCache(true);
		ivProfilePic.setDrawingCacheEnabled(true);
		ivTemplate.buildDrawingCache(true);
		ivTemplate.setDrawingCacheEnabled(true);

		// Create new thread to crop.
		new Thread(new Runnable() {

			@Override
			public void run() {
				// Crop image using the correct template size.
				Bitmap croppedImg = null;
				croppedImg = ImageProcess.cropImage(
						ivProfilePic.getDrawingCache(true),
						ivTemplate.getDrawingCache(true), mTemplateWidth,
						mTemplateHeight);
				Log.e("null check", croppedImg + "");
				ivProfilePic.setDrawingCacheEnabled(false);
				ivTemplate.setDrawingCacheEnabled(false);

				// Send a message to the Handler indicating the Thread has
				// finished.
				String newImagePath = FileUtils.newFile(getActivity(),
						FileType.PROFILE_IMAGE, PrefUtils.getUserID())
						.getAbsolutePath();
				FileUtils.createImageFile(croppedImg, newImagePath);
				showData(newImagePath);
			}
		}).start();
	}

	private void setSelectedImage(String path) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		if (mScreenWidth == 320 && mScreenHeight == 480) {
			options.inSampleSize = calculateImageSize(options,
					IMG_MAX_SIZE_MDPI);
		} else {
			options.inSampleSize = calculateImageSize(options,
					Constant.SCREEN.HEIGHT);
		}

		options.inJustDecodeBounds = false;
		Bitmap photoImg = BitmapFactory.decodeFile(path, options);
		if (photoImg == null) {
			Controller.showToast(getActivity(), "Please choose Correct Pic");
			getFragmentManager().popBackStack();
		} else {
			/*
			 * else { Bitmap newBitmap = null; Log.d("CropImage", "photo " +
			 * photoImg); try { ExifInterface exif = new ExifInterface(path);
			 * int rotation = exif.getAttributeInt(
			 * ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
			 * int rotationInDegrees = exifToDegrees(rotation); Matrix matrix =
			 * new Matrix(); if (rotation != 0f) {
			 * matrix.preRotate(rotationInDegrees); newBitmap =
			 * Bitmap.createBitmap(photoImg, 0, 0, photoImg.getWidth(),
			 * photoImg.getHeight(), matrix, true); }
			 * 
			 * } catch (IOException e) { e.printStackTrace(); Log.e("CropImage",
			 * "io Exception " + e); }
			 */

			/*
			 * if (newBitmap != null) { mImageHeight = newBitmap.getHeight();
			 * mImageWidth = newBitmap.getWidth();
			 * ivProfilePic.setImageBitmap(newBitmap);
			 * 
			 * photoImg.recycle(); } else
			 */{
				mImageHeight = photoImg.getHeight();
				mImageWidth = photoImg.getWidth();
				ivProfilePic.setImageBitmap(photoImg);
			}

			Log.i("Touch ", "mScreenHeight " + mScreenHeight);
			Log.i("Touch ", "mScreenWidth " + mScreenWidth);
			Log.i("Touch ", "mImageHeight " + mImageHeight);
			Log.i("Touch ", "mImageWidth " + mImageWidth);

			float scaledImageCenterX = (mImageWidth * mScaleFactor) / 2;
			float scaledImageCenterY = (mImageHeight * mScaleFactor) / 2;

			int diffx = (mScreenWidth - mImageWidth) / 2;
			int diffy = (mScreenHeight - mImageHeight) / 2;
			Log.i("Touch ", "diffx " + diffx);
			Log.i("Touch ", "diffy " + diffy);

			mFocusX = scaledImageCenterX + diffx;
			mFocusY = scaledImageCenterY + diffy;

			mMatrix.reset();
			mMatrix.postScale(mScaleFactor, mScaleFactor);
			mMatrix.postRotate(mRotationDegrees, scaledImageCenterX,
					scaledImageCenterY);
			mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY
					- scaledImageCenterY);
			ivProfilePic.setImageMatrix(mMatrix);

			Log.i("Touch ", "mScaleFactor " + mScaleFactor);
			Log.i("Touch ", "mFocusX " + mFocusX);
			Log.i("Touch ", "mFocusY " + mFocusY);

			Log.i("Touch ", "scaledImageCenterX " + scaledImageCenterX);
			Log.i("Touch ", "scaledImageCenterY " + scaledImageCenterY);
			// }
		}
	}

	private int exifToDegrees(int exifOrientation) {
		if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
			return 90;
		} else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
			return 180;
		} else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
			return 270;
		}
		return 0;
	}

	private int calculateImageSize(BitmapFactory.Options opts, int threshold) {
		int scaleFactor = 1;
		final int height = opts.outHeight;
		final int width = opts.outWidth;

		if (width >= height) {
			scaleFactor = Math.round((float) width / threshold);
		} else {
			scaleFactor = Math.round((float) height / threshold);
		}
		return scaleFactor;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		Log.i("Touch ", "mScaleFactor " + mScaleFactor);
		Log.i("Touch ", "mRotationDegrees " + mRotationDegrees);

		mScaleDetector.onTouchEvent(event);
		mRotateDetector.onTouchEvent(event);
		mMoveDetector.onTouchEvent(event);

		float scaledImageCenterX = (mImageWidth * mScaleFactor) / 2;
		float scaledImageCenterY = (mImageHeight * mScaleFactor) / 2;

		mMatrix.reset();
		mMatrix.postScale(mScaleFactor, mScaleFactor);
		mMatrix.postRotate(mRotationDegrees, scaledImageCenterX,
				scaledImageCenterY);
		mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY
				- scaledImageCenterY);

		ImageView view = (ImageView) v;
		view.setImageMatrix(mMatrix);
		return true;
	}

	private class ScaleListener extends
			ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			mScaleFactor *= detector.getScaleFactor();
			mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));

			return true;
		}
	}

	private class RotateListener extends
			RotateGestureDetector.SimpleOnRotateGestureListener {
		@Override
		public boolean onRotate(RotateGestureDetector detector) {
			mRotationDegrees -= detector.getRotationDegreesDelta();
			return true;
		}
	}

	private class MoveListener extends
			MoveGestureDetector.SimpleOnMoveGestureListener {
		@Override
		public boolean onMove(MoveGestureDetector detector) {
			PointF d = detector.getFocusDelta();
			mFocusX += d.x;
			mFocusY += d.y;

			return true;
		}
	}

	private void showData(final String url) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				mProgressDialog.dismiss();
				onBackresult(url);
			}
		});
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.frag_crop_btn:
			cropImage();
			break;

		case R.id.frag_crop_back_btn:
			showWarningPopup();
			break;
		}
	}

	private void drawCircle(Canvas c, int width, int height) {
		Paint paintRect = new Paint();
		paintRect.setColor(Color.parseColor("#111111"));
		paintRect.setAlpha(90);

		Paint paintCircle = new Paint();

		paintCircle = new Paint();
		paintCircle.setColor(getResources().getColor(
				android.R.color.transparent));
		paintCircle.setXfermode(new PorterDuffXfermode(Mode.CLEAR));

		c.drawRect(0, 0, c.getWidth(), c.getHeight(), paintRect);
		c.drawCircle((mScreenWidth / 2), (mScreenHeight / 2), 500 / 2,
				paintCircle);

		Paint paintCircle1 = new Paint();
		paintCircle1.setStyle(Paint.Style.STROKE);
		paintCircle1.setStrokeWidth(3);
		paintCircle1.setColor(Color.WHITE);
		c.drawCircle((mScreenWidth / 2), (mScreenHeight / 2), 500 / 2,
				paintCircle1);
	}

	private void onBackresult(String url) {

		Intent intent = new Intent();
		intent.putExtra(Constant.Key.CROP_IMAGE_URL, url.toString());
		getTargetFragment().onActivityResult(getTargetRequestCode(),
				Activity.RESULT_OK, intent);
		getFragmentManager().popBackStack();

	}

	private void showWarningPopup() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		alertDialogBuilder.setTitle("Unsaved Changes");
		alertDialogBuilder
				.setMessage("Do you want to save your changes before exiting?");

		alertDialogBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						cropImage();
						dialog.cancel();

					}
				});
		alertDialogBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						getFragmentManager().popBackStack();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		// show alert
		alertDialog.show();

	}

}
