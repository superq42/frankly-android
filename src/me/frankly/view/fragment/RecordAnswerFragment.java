package me.frankly.view.fragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import me.frankly.R;
import me.frankly.config.AppConfig;
import me.frankly.config.Controller;
import me.frankly.listener.SubmissionStatusListener;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.PendingAnswer;
import me.frankly.model.newmodel.PendingAnswersContainer;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.servicereceiver.SingleAnswerUploadService;
import me.frankly.util.AnimUtils;
import me.frankly.util.CamcoderUtils;
import me.frankly.util.CamcoderUtils.CameraType;
import me.frankly.util.CamcoderUtils.FlashMode;
import me.frankly.util.FileUtils;
import me.frankly.util.FileUtils.FileType;
import me.frankly.util.JsonUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.ShareUtils;
import me.frankly.util.VideoProcessingUtils;
import me.frankly.video.TextureRenderer;
import me.frankly.video.VideoTextureRenderer;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.RecordAnswerActivity;
import nl.flotsam.xeger.Xeger;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;

/**
 * @author Bharat Verma Three screens for capture video is just a state of mind.
 *         This is actually a single Fragment that is created with beautiful and
 *         well managed transitions to give a feel of 3 screens
 * 
 *         This is not an independent Fragment. For better code management, code
 *         handling camera and recording is transfered smartly to
 *         VideoProcessingUtils
 * 
 */
public class RecordAnswerFragment extends Fragment implements OnClickListener,
		OnTouchListener, StopRecordingListener, SurfaceTextureListener {

	private static final String TAG = RecordAnswerFragment.class
			.getSimpleName();

	private static final int MIN_VIDEO_LENGTH = 5000;
	public static final int MAX_VIDEO_LENGTH = 90000;

	private static final float DEF_THRESHOLD_DISPLACEMENT = 50;
	private static final int FILTERS_COUNT = 8;

	private ArrayList<String> videoSegmentNames;
	private ArrayList<Integer> videoSegmentLength;
	private boolean maxLengthReached;
	private QuestionObject questionAnswering;
	private View mRootView;
	private ProgressBar btnStart;
	private Button btnCancel;
	private Button btnFlash;
	private Button btnGallery;
	private Button btnCamType;
	private Button btnDone;
	private Button btnRemoveSegment;
	private volatile int currentAction;
	private volatile int mediaRecorderState;
	private View rl_bottom_buttons;
	private TextureView surfaceview_main;

	private View btn_tick;
	private ProgressBar pbVideoProgress;
	private RelativeLayout ll_buttonrecord;
	private boolean hasFlashLight;
	private ImageView image_thumb;
	private TextView tv_swipe;
	private volatile MyCountDownTimer myCountDownTimer;
	private int motive = 0;
	public static final int MOTIVE_ANSWER = 100;
	public static final int MOTIVE_PROFILE_INTRO = 200;

	private static final int REQUEST_FROM_GALLERY = 101;
	public static final int REQUEST_SHARE_ACTIVITY = 102;

	private static final int ACTION_UP = 0;
	private static final int ACTION_DOWN = 1;
	private static final int MR_STATE_UNPREPARED = 0;
	private static final int MR_STATE_PREPARING = 1;
	private static final int MR_STATE_PREPARED = 2;
	private static final int MR_STATE_RECORDING = 3;
	private static final int MR_STATE_TEARINGDOWN = 4;

	private static final String SELECT_SEGMENT = "select_segment";
	private static final String DELETE_SEGMENT = "delete_segment";

	private ToolTipRelativeLayout toolTipRelativeLayout;
	private Bitmap bmp;
	private boolean answerPosted;
	private TextView tv_question_text;
	private TextView questionAuthorTextView;
	private RelativeLayout rl_headerstrip_1;
	private RelativeLayout rl_headerstrip_2;
	private String mediaPath;
	private String videoOnSurfacePath;
	private String thumbnailPath;
	private TextView tv_question_heading;
	private int current_state;
	private Button btnCancel_1;
	private ImageView btn_selectFilters;
	private ImageView btn_selectThumb;
	private String client_id;
	private String TAG_UPLOAD = "debug101_upload";
	private String TAG_ACTION_DOWN = "ACTION_DOWN";
	private String TAG_ACTION_UP = "ACTION_UP";
	private MediaPlayer mPlayer;
	private CamcoderUtils camUtils;
	private static final int STATE_RECORDING = 901;
	private static final int STATE_FILTERS = 902;
	private static final int STATE_COVERPIC = 903;
	private static final int STATE_SHARE = 904;
	private static final int STATE_THUMB_PROCESSING = 905;
	private static final int STATE_VIDEO_STOPPED = 212;
	private static final int STATE_VIDEO_PLAYING = 211;
	private static final int STATE_VIDEO_PAUSED = 210;
	private static final int GALLERY_FILE_COPY_START = 0;
	private static final int GALLERY_FILE_COPY_DONE = 1;

	private SubmissionStatusListener submissionStatusListener;
	private boolean answerCreatedOnce;
	private String concerned_id;
	private EditableProfileObject editableProfile;
	private ProgressDialog progressDialog;
	private int video_player_state = STATE_VIDEO_STOPPED;

	private Context mContext;
	private Animation scaleUp, scaleDown;

	ToolTipView recordToolTipView, removeSegTipView, doneTipView,
			cameraTypeTipView;
	private VideoTextureRenderer renderer;
	private int fragmentShader = 0;
	private SurfaceTexture vidTex;

	private static final boolean SHOW_FILE_DEBUG_LOGS = true;
	private static final String FILE_DEBUG_TAG = "#skm RAF:";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + "onCreate", "Called");
		super.onCreate(savedInstanceState);
		/**
		 * Two array lists : videoSegmentNames,for saving segment names and
		 * videoSegmentLength, for saving segment lengths
		 */
		videoSegmentNames = new ArrayList<String>();
		videoSegmentLength = new ArrayList<Integer>();

		/**
		 * Reseting videoSegmentLength,video length ,max length and timer
		 */
		resetData();
		getDataFromAttrs();

		camUtils = new CamcoderUtils();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + "onCreateView", "Called");
		mRootView = inflater.inflate(R.layout.layout_fragment_recans, null);

		mContext = getActivity();

		findAllViewsById(mRootView);
		Log.e(TAG, "onCreateView called");
		camUtils.init(surfaceview_main, getActivity());
		camUtils.initMediaRecorder();
		btnRemoveSegment.setTag(SELECT_SEGMENT);
		mediaRecorderState = MR_STATE_UNPREPARED;

		setListeners();
		prepareInitOptions();
		prepareVideoMode();
		initCameraSettingsAndButtons();
		btnCancel.setVisibility(View.VISIBLE);
		if (motive == MOTIVE_ANSWER) {
			tv_question_text.setText(questionAnswering.getBody());
			String askerName = questionAnswering.getQuestion_author()
					.getFull_name();
			if (askerName.length() > 13) {
				String temp = askerName.substring(0, 12);
				askerName = temp + "...";
			}
			questionAuthorTextView.setText(Html.fromHtml("Reply to "
					+ askerName));

		} else {
			tv_question_text.setText("Hold To Record Video");
			questionAuthorTextView.setVisibility(View.GONE);
		}
		current_state = STATE_RECORDING;

		Controller.registerGAEvent("RecordAnswerFragment");

		if (!PrefUtils.getIsFirstRecordButtonAnimShown()) {
			recordButtonAnimation(ll_buttonrecord);
		}

		return mRootView;

	}

	@Override
	public void onResume() {
		fileDebugLog(FILE_DEBUG_TAG + "onResume", "Called");
		super.onResume();
		printCurrentRAFStates();
		getView().setFocusableInTouchMode(true);
		getView().requestFocus();
		getView().setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {

				if (event.getAction() == KeyEvent.ACTION_UP
						&& keyCode == KeyEvent.KEYCODE_BACK) {
					printCurrentRAFStates();
					fileDebugLog(TAG + "onKey", "Key presses");
					switch (current_state) {
					case STATE_RECORDING:
						removeThisFragment();
						break;
					case STATE_FILTERS:
						backToRecordingState();
						break;
					case STATE_THUMB_PROCESSING:
						// removeThisFragment();
						break;
					default:
						Log.e("debug_backpressed", "default case executed ");
						removeThisFragment();
						break;
					}
					return true;
				}
				return false;
			}
		});
		if (current_state == STATE_FILTERS || current_state == STATE_SHARE
				&& surfaceview_main != null) {
			if (surfaceview_main.getSurfaceTexture() == null)
				surfaceview_main.setSurfaceTextureListener(this);
			else if (current_state == STATE_FILTERS && mPlayer == null)
				playVideo();
		}

		if (camUtils.mCamera == null && current_state == STATE_RECORDING) {
			camUtils.setCamDefaults();
			camUtils.prepareCamera();
		}

		if (!PrefUtils.getIsFirstRecordButtonAnimShown()
				&& recordToolTipView == null)
			recordToolTipView = Controller.addToolTipView(
					toolTipRelativeLayout, btnStart, mContext, "Tap to Record",
					false);
	}
	
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("current_state", STATE_SHARE);
	}

	@Override
	public void onStop() {
		fileDebugLog(FILE_DEBUG_TAG + "onStop", "Called");
		super.onStop();
		if (current_state == STATE_FILTERS) {
			if (mPlayer != null) {
				if (mPlayer.isPlaying())
					mPlayer.stop();
				mPlayer.reset();
				mPlayer.release();
				mPlayer = null;
			}
			if (renderer != null)
				renderer.stopRendering();
			video_player_state = STATE_VIDEO_STOPPED;
		}
		if (current_state == STATE_SHARE) {
			image_thumb.setVisibility(View.VISIBLE);
		}
		if (camUtils.mCamera != null) {
			camUtils.releaseCamera();
		}
	}

	private void setListeners() {

		fileDebugLog(FILE_DEBUG_TAG + "setListeners", "Called");

		surfaceview_main.setOnClickListener(this);
		ll_buttonrecord.setOnTouchListener(this);
		btnStart.setOnTouchListener(this);
		btnCancel.setOnClickListener(this);
		btnCancel_1.setOnClickListener(this);
		btnDone.setOnClickListener(this);
		btnFlash.setOnClickListener(this);
		btnGallery.setOnClickListener(this);
		btnCamType.setOnClickListener(this);
		btnRemoveSegment.setOnClickListener(this);
		btn_tick.setOnClickListener(this);
		btn_selectFilters.setOnClickListener(this);
		btn_selectThumb.setOnClickListener(this);

	}

	/**
	 * set default camera settings and bottom buttons
	 */
	private void initCameraSettingsAndButtons() {
		hasFlashLight = getActivity().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA_FLASH);

		camUtils.setFlashMode(FlashMode.OFF);

		if (camUtils.hasFrontCamera()) {
			btnCamType.setVisibility(View.VISIBLE);
			camUtils.setCameraType(CameraType.FRONT);
		} else {
			btnCamType.setVisibility(View.INVISIBLE);
			camUtils.setCameraType(CameraType.BACK);

			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) btnFlash
					.getLayoutParams();
			lp.addRule(RelativeLayout.RIGHT_OF, 0);
			lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			btnFlash.setLayoutParams(lp);
		}
		setFlashButtonVisibility();
	}

	private void prepareInitOptions() {
		fileDebugLog(FILE_DEBUG_TAG + "prepareInitOptions", "Called");
		if (videoSegmentLength.size() > 0) {
			btnCancel.setVisibility(View.VISIBLE);
			btnDone.setVisibility(View.VISIBLE);
		} else {
			btnDone.setVisibility(View.INVISIBLE);
		}
	}

	private void prepareVideoMode() {
		fileDebugLog(FILE_DEBUG_TAG + "prepareVideoMode", "Called");
		pbVideoProgress.setVisibility(View.VISIBLE);
		if (videoSegmentNames.size() <= 0 || videoSegmentNames == null) {
			btnRemoveSegment
					.setBackgroundResource(R.drawable.btn_remove_segment);
			btnRemoveSegment.setTag(SELECT_SEGMENT);
			btnRemoveSegment.setVisibility(View.INVISIBLE);
		}
	}

	private void findAllViewsById(View view) {
		fileDebugLog(FILE_DEBUG_TAG + "findAllViewsById", "Called");
		btnStart = (ProgressBar) view.findViewById(R.id.pb_video_rec);
		pbVideoProgress = btnStart;
		pbVideoProgress.setProgress(0);
		pbVideoProgress.setSecondaryProgress(0);
		btnCancel = (Button) view.findViewById(R.id.btn_close);
		btnCancel_1 = (Button) view.findViewById(R.id.btn_close_1);
		btnDone = (Button) view.findViewById(R.id.btn_done);
		btnFlash = (Button) view.findViewById(R.id.btn_flash);
		btnGallery = (Button) view.findViewById(R.id.btn_gallery);
		btnCamType = (Button) view.findViewById(R.id.btn_cam_type);
		btnRemoveSegment = (Button) view.findViewById(R.id.btn_remove_seg);
		rl_bottom_buttons = view.findViewById(R.id.rl_recans_bottom_buttons);
		surfaceview_main = (TextureView) view
				.findViewById(R.id.tv_recans_recordview);

		btn_tick = view.findViewById(R.id.btn_done_1);
		ll_buttonrecord = (RelativeLayout) view
				.findViewById(R.id.ll_btn_start_container);
		tv_question_text = (TextView) view
				.findViewById(R.id.tv_recans_questiontext);
		tv_question_heading = (TextView) view
				.findViewById(R.id.tv_recans_filter);
		rl_headerstrip_1 = (RelativeLayout) view
				.findViewById(R.id.rl_recans_topbutttons);
		rl_headerstrip_2 = (RelativeLayout) view
				.findViewById(R.id.rl_recans_preview);
		rl_headerstrip_2.setVisibility(View.GONE);
		image_thumb = (ImageView) view.findViewById(R.id.image_recans_thumb);
		tv_swipe = (TextView) view.findViewById(R.id.tv_recans_swipe);
		tv_swipe.setVisibility(View.GONE);
		btn_selectFilters = (ImageView) view
				.findViewById(R.id.btn_recans_filterwand);
		btn_selectThumb = (ImageView) view
				.findViewById(R.id.btn_recans_filterthumb);

		toolTipRelativeLayout = (ToolTipRelativeLayout) view
				.findViewById(R.id.activity_main_tooltipRelativeLayout);
		questionAuthorTextView = (TextView) view
				.findViewById(R.id.tv_asker_name);
	}

	private Handler mediaRecorderHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			printCurrentRAFStates();
			fileDebugLog(FILE_DEBUG_TAG + "mediaRecorderHandler",
					(currentAction == 1) ? "DOWN" : "UP");
			switch (currentAction) {
			case ACTION_DOWN: {
				if (mediaRecorderState == MR_STATE_UNPREPARED) {
					mediaRecorderState = MR_STATE_PREPARING;
					final String lastFile = FileUtils.newFile(getActivity(),
							FileType.VIDEO_SEGMENT,
							videoSegmentNames.size() + "_" + concerned_id)
							.toString();
					videoSegmentNames.add(lastFile);

					new Thread(new Runnable() {

						@Override
						public void run() {
							if (camUtils.prepareMediaRecorder(lastFile) == true) {
								mediaRecorderState = MR_STATE_PREPARED;
								if (currentAction == ACTION_DOWN) {
									try {
										camUtils.mMediaRecorder.start();
										getActivity().runOnUiThread(
												new Runnable() {
													@Override
													public void run() {
														showScaleAnimation(
																ll_buttonrecord,
																R.anim.anim_scale,
																null);
													}
												});
										/**
										 * A new thread is being started in
										 * update progress bar.
										 * mediaRecorderState will be set as
										 * recording once it completes.
										 */
										updateProgressBar();
									} catch (RuntimeException e) {
										manageMediaRecorderError();
										mediaRecorderHandler
												.sendEmptyMessage(0);
									}
								} else {
									manageMediaRecorderError();
								}
							} else {
								camUtils.mCamera.lock();
								manageMediaRecorderError();
								mediaRecorderHandler.sendEmptyMessage(0);
							}
						}
					}).start();
				}
			}// end case
				break;
			case ACTION_UP:
				if (mediaRecorderState == MR_STATE_RECORDING) {

					new Thread(new Runnable() {
						@Override
						public void run() {
							mediaRecorderState = MR_STATE_TEARINGDOWN;
							if (myCountDownTimer != null) {
								Log.i(TAG_ACTION_UP + " ",
										"myCountDownTimer thread cancelled");
								myCountDownTimer.cancel();
							} else {
								Log.i(TAG_ACTION_UP + " ", ""
										+ myCountDownTimer);
							}
							stopVideoRecording();
							mediaRecorderState = MR_STATE_UNPREPARED;
						}
					}).start();

					showScaleAnimation(ll_buttonrecord, R.anim.anim_scale_down,
							null);

				} else if (mediaRecorderState != MR_STATE_UNPREPARED) {
					/**
					 * Wait for media recorder to get into the recording state
					 * and then release it if the button is still in UP state.
					 */
					this.sendEmptyMessageDelayed(0, 100);
				}
				break;
			}
		}
	};

	/**
	 * if {@link CamcoderUtils#prepareMediaRecorder(String) OR
	 * this#startRecording() has been called and any error occurs, we need to
	 * call this method.
	 * 
	 * It removes the last segment and revert bottom buttons or progress to
	 * previous states as per count of segments
	 */
	private void manageMediaRecorderError() {
		camUtils.releaseMediaRecorder();
		mediaRecorderState = MR_STATE_UNPREPARED;

		if (videoSegmentNames.isEmpty()) {
			return;
		}
		int lastIndex = videoSegmentNames.size() - 1;
		FileUtils.delete(videoSegmentNames.remove(lastIndex));

		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (videoSegmentNames.isEmpty()) {
					if (removeSegTipView != null) {
						removeSegTipView.remove();
					}
					if (doneTipView != null) {
						doneTipView.remove();
					}
					pbVideoProgress.setProgress(0);
					videoSegmentLength.clear();
					MyCountDownTimer.timeFinished = 0;

					btnDone.setVisibility(View.INVISIBLE);
					btnRemoveSegment.setVisibility(View.INVISIBLE);
					btnCancel.setVisibility(View.VISIBLE);
					btnCamType.setVisibility(View.VISIBLE);
					btnGallery.setVisibility(View.VISIBLE);
					setFlashButtonVisibility();
				} else if (videoSegmentLength.size() > videoSegmentNames.size()) {
					int lastIndex = videoSegmentLength.size() - 1;
					videoSegmentLength.remove(lastIndex);
					int actualTime = videoSegmentLength.get(--lastIndex);
					pbVideoProgress.setProgress(actualTime);
					MyCountDownTimer.timeFinished = actualTime;
				}
			}
		});
	}

	@Override
	public boolean onTouch(final View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			printCurrentRAFStates();
			currentAction = ACTION_DOWN;
			fileDebugLog(FILE_DEBUG_TAG + "onTouch", "MotionEvent.ACTION_DOWN:");
			if (!PrefUtils.getIsFirstRecordButtonAnimShown()) {
				v.clearAnimation();
				if (recordToolTipView != null) {
					recordToolTipView.remove();
				}

			}

			if (PrefUtils.getIsFirstRecordUpDropAnimShown()
					&& videoSegmentNames.size() == 1) {
				if (removeSegTipView != null && doneTipView != null) {
					removeSegTipView.remove();
					doneTipView.remove();
				}
			}

			tv_question_text.setVisibility(View.GONE);
			Log.i(TAG_ACTION_DOWN, " isMaxLengthReached: " + maxLengthReached
					+ " vidSegmentLength:" + videoSegmentLength.size()
					+ " vidSegmentNames:" + videoSegmentNames.size());
			if (btnRemoveSegment.getTag().equals(DELETE_SEGMENT)) {
				btnRemoveSegment.setTag(SELECT_SEGMENT);
				btnRemoveSegment
						.setBackgroundResource(R.drawable.btn_remove_segment);
				pbVideoProgress.setProgress(MyCountDownTimer.timeFinished);
			}
			if (!maxLengthReached) {
				Log.i(TAG_ACTION_DOWN, "inside if condition to start recording");
				btnRemoveSegment.setVisibility(View.GONE);
				btnGallery.setVisibility(View.GONE);
				btnCamType.setVisibility(View.GONE);
				btnFlash.setVisibility(View.GONE);
				btnCancel.setVisibility(View.GONE);
				btnDone.setVisibility(View.GONE);
				Log.i(TAG_ACTION_DOWN, "just before startrecordingthread ");
				if (mediaRecorderState == MR_STATE_UNPREPARED) {
					mediaRecorderHandler.sendEmptyMessage(0);
				}
			} else {
				Toast toast = Toast.makeText(getActivity(),
						"Max Length Reached... ", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}

			break;
		case MotionEvent.ACTION_UP:
			printCurrentRAFStates();
			currentAction = ACTION_UP;
			fileDebugLog(FILE_DEBUG_TAG + "onTouch", "MotionEvent.ACTION_UP:");
			if (!PrefUtils.getIsFirstRecordButtonAnimShown()) {
				PrefUtils.setIsFirstRecordButtonAnimShown(true);
			}
			if (!maxLengthReached) {
				if (videoSegmentNames.isEmpty()) {
					btnRemoveSegment.setVisibility(View.INVISIBLE);
					btnCancel.setVisibility(View.VISIBLE);
					btnGallery.setVisibility(View.VISIBLE);
					btnCamType.setVisibility(View.VISIBLE);
					setFlashButtonVisibility();
					btnDone.setVisibility(View.INVISIBLE);
					pbVideoProgress.setProgress(0);
				} else {
					btnRemoveSegment.setVisibility(View.VISIBLE);
					if (!PrefUtils.getIsFirstRecordUpDropAnimShown()
							&& videoSegmentNames.size() == 1) {
						removeSegTipView = Controller.addToolTipView(
								toolTipRelativeLayout, btnRemoveSegment,
								mContext, "Tap to Remove Segment", false);
						doneTipView = Controller.addToolTipView(
								toolTipRelativeLayout, btnDone, mContext,
								"Tap to Proceed Further", false);
						PrefUtils.setIsFirstRecordUpDropAnimShown(true);
					}
				}

				tv_question_text.setVisibility(View.VISIBLE);

				Log.i(TAG_ACTION_UP + " " + Thread.currentThread().getName(),
						"before while loop");
				Log.i(TAG_ACTION_UP + " ", "done with waiting "
						+ myCountDownTimer);

				mediaRecorderHandler.sendEmptyMessage(0);
			}
			//
			if (!videoSegmentNames.isEmpty()) {
				tv_question_text.setVisibility(View.VISIBLE);
				btnGallery.setVisibility(View.GONE);
				btnCamType.setVisibility(View.GONE);
				btnCancel.setVisibility(View.VISIBLE);
				btnDone.setVisibility(View.VISIBLE);
				btnRemoveSegment.setVisibility(View.VISIBLE);
				if (!PrefUtils.getIsFirstRecordUpDropAnimShown()
						&& videoSegmentNames.size() == 1) {
					removeSegTipView = Controller.addToolTipView(
							toolTipRelativeLayout, btnRemoveSegment, mContext,
							"Tap to Remove Segment", false);
					doneTipView = Controller.addToolTipView(
							toolTipRelativeLayout, btnDone, mContext,
							"Tap to Proceed Further", false);
					PrefUtils.setIsFirstRecordUpDropAnimShown(true);
				}
			}
			break;
		}

		return true;
	}

	@Override
	public void onClick(View v) {
		printCurrentRAFStates();
		switch (v.getId()) {
		case R.id.btn_done_1:
			fileDebugLog(FILE_DEBUG_TAG + "onClick",
					"btn_done_1:Done button on share and preview screen.");

			if (renderer != null) {
				renderer.stopRendering();
				TextureRenderer.selectedFragmentShader = renderer.getmShader();
			}
			if (mPlayer != null) {
				if (mPlayer.isPlaying())
					mPlayer.stop();
				mPlayer.reset();
				mPlayer.release();
				mPlayer = null;
			}
			video_player_state = STATE_VIDEO_STOPPED;
			if (current_state == STATE_FILTERS) {
				tv_swipe.setVisibility(View.GONE);
				removeVideoSegments(true);
				if (motive == MOTIVE_ANSWER) {
					current_state = STATE_SHARE;
					userWantsToPost(v);

					String text = ShareUtils.getTextToShare(client_id);
					((BaseActivity) getActivity()).shareActivity(this, text,
							null, null, 0);

					tv_question_heading.setVisibility(View.GONE);
					btnCancel_1.setVisibility(View.GONE);
					surfaceview_main.setClickable(false);
					btn_tick.setVisibility(View.GONE);
				} else if (motive == MOTIVE_PROFILE_INTRO) {
					removeFragmentWithSuccess();
				}
			}
			break;
		case R.id.btn_close_1:
			fileDebugLog(FILE_DEBUG_TAG + "onClick",
					"btn_close_1: Cancel button on preview screen.");
			backToRecordingState();
			break;
		case R.id.btn_flash:
			switchFlashMode(false);
			camUtils.releaseCamera();
			camUtils.showPreview();
			break;
		case R.id.btn_gallery:
			pickFromGallery();
			break;
		case R.id.btn_cam_type:
			if (videoSegmentLength.isEmpty()) {
				if (camUtils.getCameraType() == CameraType.FRONT) {
					camUtils.setCameraType(CameraType.BACK);
					switchFlashMode(true);
				} else {
					camUtils.setCameraType(CameraType.FRONT);
				}
				setFlashButtonVisibility();
				camUtils.releaseCamera();
				camUtils.showPreview();
			} else {
				Toast.makeText(getActivity(),
						"Can't switch Camera at this moment",
						Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.btn_close:
			fileDebugLog(FILE_DEBUG_TAG + "onClick",
					"btn_close: Close button on recording screen.");
			// topLeftButtonPressed();
			// showDiscardDialog();
			Log.e("debug_backpressed", "btn_close ");
			removeThisFragment();
			break;

		case R.id.btn_done:
			fileDebugLog(FILE_DEBUG_TAG + "onClick",
					"btn_done: Done button on recording screen.");
			if (removeSegTipView != null && doneTipView != null) {
				removeSegTipView.remove();
				doneTipView.remove();
			}

			// check total length
			if (videoSegmentLength != null
					&& !videoSegmentLength.isEmpty()
					&& videoSegmentLength.get(videoSegmentLength.size() - 1) < MIN_VIDEO_LENGTH) {
				Toast.makeText(getActivity(),
						"A little more! Video should be atleast 5 seconds.",
						Toast.LENGTH_LONG).show();
				return;
			}

			Animation outToLeftAnimation1 = AnimUtils.outToLeftAnimation();
			AnimUtils.showSlideAnimation(rl_headerstrip_1, outToLeftAnimation1,
					true, null);
			Animation inFromRightAnimation1 = AnimUtils.inFromRightAnimation();
			AnimUtils.showSlideAnimation(rl_headerstrip_2,
					inFromRightAnimation1, false, new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							rl_bottom_buttons.setVisibility(View.GONE);
							tv_question_text.setVisibility(View.GONE);
							current_state = STATE_THUMB_PROCESSING;
							showProgressDialog();

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							Log.d("debug_PREVIEWSTATE+"
									+ Thread.currentThread().getName(),
									" animation played ,--"
											+ System.currentTimeMillis());
							if (videoSegmentNames != null
									&& videoSegmentNames.size() > 0) {
								Log.d("debug_PREVIEWSTATE",
										"btn_done clicked before merging= "
												+ System.currentTimeMillis());

								camUtils.releaseMediaRecorder();
								camUtils.releaseCamera();

								bmp = VideoProcessingUtils.getThumbnail(
										videoSegmentNames.get(0), -1);
								image_thumb.setVisibility(View.VISIBLE);
								image_thumb.setImageBitmap(bmp);

								thumbnailPath = FileUtils.newFile(
										getActivity(),
										FileType.VIDEO_THUMBNAIL, concerned_id)
										.getAbsolutePath();
								FileUtils.createImageFile(bmp, thumbnailPath);
								Log.d("debug_PREVIEWSTATE",
										" before thread start,--"
												+ System.currentTimeMillis());
								moveToPreviewPhase();
							} else {
								hideProgressBar();
								if (mediaPath != null)
									setVideoOnSurfaceView(mediaPath);
							}

						}
					});
			// AnimUtils.showSlideAnimation(rl_bottombuttons,
			// AnimUtils.inFromRightAnimation(), false, null);
			Log.d("debug_PREVIEWSTATE",
					" animation played ,--" + System.currentTimeMillis());
			// moveToPreviewPhase();

			break;
		case R.id.btn_remove_seg:
			fileDebugLog(FILE_DEBUG_TAG + "onClick", "btn_remove_seg");
			Log.e(TAG, "btn_removeSegmentClicked");
			if (videoSegmentLength != null) {

				if (removeSegTipView != null && doneTipView != null) {
					removeSegTipView.remove();
					doneTipView.remove();
				}

				Log.i("answer",
						"video segmentsize: " + videoSegmentLength.size());

				if (btnRemoveSegment.getTag().equals(SELECT_SEGMENT)) {
					if (videoSegmentLength.size() > 1) {
						pbVideoProgress
								.setSecondaryProgress(MyCountDownTimer.timeFinished);
						pbVideoProgress.setProgress(videoSegmentLength
								.get(videoSegmentLength.size() - 2));
					} else {
						pbVideoProgress
								.setSecondaryProgress(MyCountDownTimer.timeFinished);
						pbVideoProgress.setProgress(0);
					}

					btnRemoveSegment
							.setBackgroundResource(R.drawable.btn_remove_part);
					btnRemoveSegment.setTag(DELETE_SEGMENT);
				} else {
					// videoSegmentNames.size() > 0 added
					if (videoSegmentLength.size() > 0
							&& videoSegmentNames.size() > 0) {
						Log.i(TAG,
								"Segment deleted " + videoSegmentLength.size());

						Log.i(TAG,
								"Segment deleted length:"
										+ videoSegmentLength
												.get(videoSegmentLength.size() - 1));

						new File(videoSegmentNames.get(videoSegmentLength
								.size() - 1)).delete();
						videoSegmentLength
								.remove(videoSegmentLength.size() - 1);
						videoSegmentNames.remove(videoSegmentNames.size() - 1);

						if (videoSegmentLength.size() == 0) {
							pbVideoProgress.setProgress(0);
							pbVideoProgress.setSecondaryProgress(0);
							MyCountDownTimer.setTimeFinished(0);
						} else {
							int previousSegment = videoSegmentLength
									.get(videoSegmentLength.size() - 1);
							pbVideoProgress.setSecondaryProgress(0);
							pbVideoProgress.setProgress(previousSegment);
							MyCountDownTimer.setTimeFinished(previousSegment);
						}
						btnRemoveSegment
								.setBackgroundResource(R.drawable.btn_remove_segment);

						btnRemoveSegment.setTag(SELECT_SEGMENT);
						maxLengthReached = false;
					}
					if (videoSegmentLength.size() == 0) {
						Log.e(TAG, "Size is zero!!");
						btnGallery.setVisibility(View.VISIBLE);
						btnCamType.setVisibility(View.VISIBLE);
						setFlashButtonVisibility();
						btnDone.setVisibility(View.INVISIBLE);
						btnRemoveSegment.setVisibility(View.INVISIBLE);
						pbVideoProgress.setSecondaryProgress(0);
						pbVideoProgress.setProgress(0);
					}
				}
			}
			break;
		default:
			break;
		}

	}

	/**
	 * User has created video and now he wants to go back state to edit the
	 * video. Lets reset everything like camcoder,camera and take user to the
	 * state where he left recording
	 * 
	 */
	private void backToRecordingState() {
		fileDebugLog(FILE_DEBUG_TAG + "backToRecordingState", "Called");
		if (mPlayer != null) {
			fileDebugLog(FILE_DEBUG_TAG + "backToRecordingState",
					"MediaPlayer not null");
			if (mPlayer.isPlaying())
				mPlayer.stop();
			mPlayer.reset();
			mPlayer.release();
			mPlayer = null;
		}
		if (renderer != null) {
			renderer.setmShader(0);
			renderer.stopRendering();
		}
		video_player_state = STATE_VIDEO_STOPPED;
		image_thumb.setVisibility(View.GONE);
		surfaceview_main.setVisibility(View.GONE);
		surfaceview_main.setClickable(false);
		
		current_state = STATE_RECORDING;

		/**
		 * Delete the temporary preview video and image from Frankly cache.
		 * 
		 * @note can't delete in case of single segment, as that is also the
		 *       final video
		 */
		if (videoSegmentNames != null && videoSegmentNames.size() > 1) {
			new File(videoOnSurfacePath).delete();
		}
		new File(thumbnailPath).delete();

		/**
		 * If media player was started on previous screen, it has tampered with
		 * the texture view. Take a new texture view. Also set the LayoutParams
		 * of texture view again for use.
		 */
		TextureView oldTexture = surfaceview_main;
		ViewGroup textureParent = (ViewGroup) oldTexture.getParent();
		int index = textureParent.indexOfChild(oldTexture);
		textureParent.removeView(oldTexture);
		surfaceview_main = new TextureView(getActivity());
		surfaceview_main.setId(R.id.tv_recans_recordview);
		textureParent.addView(surfaceview_main, index);
		surfaceview_main.setClickable(false);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);
		surfaceview_main.setLayoutParams(lp);
		camUtils.setTextureView(surfaceview_main);
		camUtils.perceptionFix(getActivity());
		camUtils.releaseCamera();
		camUtils.showPreview();
		camUtils.setSurfaceView(surfaceview_main);

		surfaceview_main.setVisibility(View.VISIBLE);
		tv_swipe.setVisibility(View.GONE);
		btnCamType.setVisibility(View.VISIBLE);
		// rl_play_btn.setVisibility(View.GONE);

		if (videoSegmentNames.size() > 0) {
			btnDone.setVisibility(View.VISIBLE);
			btnCamType.setVisibility(View.GONE);
		}

		Animation outToRightAnimation = AnimUtils.outToRightAnimation();
		AnimUtils.showSlideAnimation(rl_headerstrip_2, outToRightAnimation,
				true, null);
		// AnimUtils.showSlideAnimation(rl_filternthumb,
		// AnimUtils.outToRightAnimation(), true, null);
		Animation inFromLeftAnimation = AnimUtils.inFromLeftAnimation();
		AnimUtils.showSlideAnimation(rl_headerstrip_1, inFromLeftAnimation,
				false, null);
		AnimUtils.showSlideAnimation(rl_bottom_buttons,
				AnimUtils.inFromLeftAnimation(), false,
				new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {

					}

					@Override
					public void onAnimationRepeat(Animation animation) {

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						tv_question_text.setVisibility(View.VISIBLE);
					}
				});
	}

	/**
	 * checks camera type and flash feature in device and set visibility
	 * accordingly
	 */
	private void setFlashButtonVisibility() {
		if (hasFlashLight && camUtils.getCameraType() == CameraType.BACK) {
			btnFlash.setVisibility(View.VISIBLE);
		} else {
			btnFlash.setVisibility(View.GONE);
		}
	}

	/**
	 * @param pForceOff
	 */
	private void switchFlashMode(boolean pForceOff) {
		if (pForceOff || camUtils.getFlashMode() == FlashMode.ON) {
			camUtils.setFlashMode(FlashMode.OFF);
			btnFlash.setBackgroundResource(R.drawable.btn_flashoff);
		} else {
			camUtils.setFlashMode(FlashMode.ON);
			btnFlash.setBackgroundResource(R.drawable.btn_flash_on);
		}
	}

	/**
	 * This method is used to start progressbar and updating it with every
	 * second. we need to stop timer on ACTION_UP event. Refer to onTouch for
	 * that
	 * 
	 */
	public void updateProgressBar() {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				myCountDownTimer = new MyCountDownTimer(
						(/* 90000 */MAX_VIDEO_LENGTH - (MyCountDownTimer.timeFinished)),
						1, pbVideoProgress);
				myCountDownTimer
						.setStopRecordingListener(RecordAnswerFragment.this);
				myCountDownTimer.start();
				pbVideoProgress.setSecondaryProgress(0);
				mediaRecorderState = MR_STATE_RECORDING;
			}
		});
	}

	/**
	 * @return recordingStatus This method is used to stop camcoder from
	 *         recording. Also, this saves the details of segment created in
	 *         Arraylists videoSegmentNames, videoSegmentLength. This method
	 *         also handles UI changes accordingly
	 */
	public void stopVideoRecording() {
		if (AppConfig.DEBUG) {
			Log.i(TAG, "stopVideoRecording()");
		}
		try {
			camUtils.mMediaRecorder.stop();
			saveSegmentLength();
		} catch (Exception e) {
			Log.e(TAG, "stopVideoRecording()", e);
			manageMediaRecorderError();
		}
	}

	private void saveSegmentLength() {
		fileDebugLog(FILE_DEBUG_TAG + "saveSegmentLength", "Called");
		if (videoSegmentLength != null) {
			if (videoSegmentLength.size() > 0) {
				Log.i("video_seg",
						"video segment size" + videoSegmentLength.size());
				if (MyCountDownTimer.getTimeFinished()
						- videoSegmentLength.get(videoSegmentLength.size() - 1) > 0) {
					videoSegmentLength.add(MyCountDownTimer.getTimeFinished());
					Log.i(TAG + "saveSegmentLength() yoo",
							"last segment time saved: "
									+ videoSegmentLength.get(videoSegmentLength
											.size() - 1));
				}
			} else if (MyCountDownTimer.getTimeFinished() > 1) {
				videoSegmentLength.add(MyCountDownTimer.getTimeFinished());
				Log.i(TAG + "saveSegmentLength()", "last segment time saved: "
						+ videoSegmentLength.get(videoSegmentLength.size() - 1));
			} else {
				// if(videoSegmentLength)

			}
			Log.i(TAG + "saveSegmentLength()", "last segment time fetched:"
					+ MyCountDownTimer.getTimeFinished() / 1000);
		}

	}

	void resetData() {
		fileDebugLog(FILE_DEBUG_TAG + "resetData", "Called");
		videoSegmentNames = new ArrayList<String>();
		maxLengthReached = false;
		videoSegmentLength = new ArrayList<Integer>();
		MyCountDownTimer.setTimeFinished(0);
	}

	/**
	 * parse QuestionData From Question object json recieved in args from
	 * RecordAnswerActivity
	 * 
	 */
	private void getDataFromAttrs() {
		fileDebugLog(FILE_DEBUG_TAG + "getDataFromAttrs", "Called");
		Bundle args = getArguments();
		String recordAnsJson = args
				.getString(RecordAnswerActivity.KEY_QUESTION_ASKED);
		String editprofile = args
				.getString(RecordAnswerActivity.KEY_EDIT_PROFILE);
		// Log.e("questionObjectRecieved", recordAnsJson);
		if (recordAnsJson != null) {
			motive = MOTIVE_ANSWER;
			questionAnswering = (QuestionObject) JsonUtils.objectify(
					recordAnsJson, QuestionObject.class);
			concerned_id = questionAnswering.getId();
		} else if (editprofile != null) {
			motive = MOTIVE_PROFILE_INTRO;
			Log.d("debug301_getData", "" + editprofile);
			// editableProfile = (EditableProfileObject)
			// JsonUtils.objectify(editprofile, EditableProfileObject.class);
			Log.d("debug301_getData", "" + editableProfile);
			// concerned_id = editableProfile.getId();
			concerned_id = editprofile;
		}
		// Log.e("onActivityResult", "question_id on Fragment__" +
		// questionAnswering.getId());
	}

	@Override
	public void onTimeFinished() {
		fileDebugLog(FILE_DEBUG_TAG + "onTimeFinished", "Called");
		maxLengthReached = true;
		Log.i("answer", "countdown timer finished");
		if (mediaRecorderState == MR_STATE_RECORDING) {
			stopVideoRecording();
			Log.i("answer", "recording stopped");
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast toast = Toast.makeText(getActivity(),
							"Max Length Reached... ", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					btnRemoveSegment.setVisibility(View.VISIBLE);
					if (!videoSegmentNames.isEmpty()) {
						tv_question_text.setVisibility(View.VISIBLE);
						btnGallery.setVisibility(View.GONE);
						btnCamType.setVisibility(View.GONE);
						btnCancel.setVisibility(View.VISIBLE);
						btnDone.setVisibility(View.VISIBLE);
						btnRemoveSegment.setVisibility(View.VISIBLE);
					}
					showScaleAnimation(rl_bottom_buttons,
							R.anim.anim_scale_down, null);

				}
			});
		}

	}

	/**
	 * This method allows user to pick mediaFile from gallery, After picking
	 * video from gallery, you will get callback on onActivityResult method
	 */
	public void pickFromGallery() {
		fileDebugLog(FILE_DEBUG_TAG + "pickFromGallery", "Called");
		Intent imgPicker = new Intent();
		imgPicker.setType("video/*");
		imgPicker.setAction(Intent.ACTION_PICK);
		startActivityForResult(imgPicker, REQUEST_FROM_GALLERY);
	}

	/**
	 * This method is the EXIT door from this fragment. If the user has created
	 * video and posted it, we send success to parent activity before exit, if
	 * answer once created and not posted, it shows warning dialog, else is
	 * silently stops
	 * 
	 */
	private void removeThisFragment() {
		fileDebugLog(FILE_DEBUG_TAG + "removeThisFragment", "Called");
		Log.e("debug_backpressed", "removeThisFragment() ");
		if (motive == MOTIVE_ANSWER) {
			Log.e("debug_backpressed", "MotiveAnswer ");
			if (answerPosted) {
				Log.e("debug_backpressed", "aNSWERpOSTED ");
				removeFragmentWithSuccess();
			} else {
				Log.e("debug_backpressed",
						"aNSWER not Posted yet  answerCreatedOnce="
								+ answerCreatedOnce + " current_state:"
								+ current_state);
				if (current_state == STATE_RECORDING) {

					if (videoSegmentLength.size() > 0) {
						showDiscardDialog();
					} else {
						removeFragmentAnyway();
					}
				} else {
					removeFragmentAnyway();
				}
			}
		} else if (motive == MOTIVE_PROFILE_INTRO) {
			if (videoSegmentLength.size() > 0) {
				showDiscardDialog();
			} else {
				removeFragmentAnyway();
			}

		}
	}

	/**
	 * remove fragments and give callback to parent activity with result as
	 * RESULT_CANCEL
	 * 
	 */
	private void removeFragmentAnyway() {
		fileDebugLog(FILE_DEBUG_TAG + "removeFragmentAnyway", "Called");
		removeVideoSegments(false);
		camUtils.releaseCamera();
		resetData();
		if (bmp != null) {
			bmp.recycle();
			bmp = null;
		}
		FragmentManager fm = getActivity().getSupportFragmentManager();
		fm.popBackStack();
		Bundle data = new Bundle();
		data.putString("submission_canceled", "submission_canceled");
		submissionStatusListener.onSubmissionCanceled(data, 0, true);
		submissionStatusListener = null;
	}

	/**
	 * Removing fragment and indicating to activity that the answer has been
	 * added in queue for processing. This is important so that you can remove
	 * 
	 */
	public void removeFragmentWithSuccess() {
		fileDebugLog(FILE_DEBUG_TAG + "removeFragmentWithSuccess", "Called");
		camUtils.releaseCamera();
		resetData();
		if (bmp != null) {
			bmp.recycle();
			bmp = null;
		}
		if (motive == MOTIVE_ANSWER) {
			submissionStatusListener.onSubmitted(questionAnswering.getId(),
					true);
		} else if (motive == MOTIVE_PROFILE_INTRO) {
			submissionStatusListener
					.onSubmitted(true, mediaPath, thumbnailPath);
		}
	}

	/**
	 * Removing all individual segments created.
	 * 
	 * @note It should be called only when use can not navigate back to edit or
	 *       add segments
	 * @note In case of single segment, that is also the final video
	 * 
	 * @param pSkipSingle
	 */
	private void removeVideoSegments(boolean pSkipSingle) {
		int segCount = videoSegmentNames == null ? 0 : videoSegmentNames.size();
		if (AppConfig.DEBUG) {
			Log.d(TAG, "removeVideoSegments() " + segCount + "," + pSkipSingle);
		}
		if (segCount == 0 || (pSkipSingle && segCount == 1)) {
			return;
		}
		for (String segmentPath : videoSegmentNames) {
			new File(segmentPath).delete();
		}
	}

	/**
	 * User has recorded the video and now and pressed top-right button. Merge
	 * all segments in a video, release video segments etc.
	 * 
	 */
	private void moveToPreviewPhase() {
		fileDebugLog(FILE_DEBUG_TAG + "moveToPreviewPhase", "Called");
		Log.d("debug_PREVIEWSTATE", "btn_done clicked");

		if (videoSegmentNames != null && videoSegmentNames.size() > 0) {

			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						if (videoSegmentNames.size() > 1) {
							mediaPath = FileUtils.newFile(getActivity(),
									FileType.VIDEO_FINAL, concerned_id)
									.getAbsolutePath();
							VideoProcessingUtils
									.mergeVideoSegments(getActivity(),
											videoSegmentNames, mediaPath);
						} else {
							mediaPath = videoSegmentNames.get(0);
						}
						videoOnSurfacePath = mediaPath;
					} catch (IOException e) {
						e.printStackTrace();
					}
					Log.d("debug_PREVIEWSTATE",
							" merge done,--" + System.currentTimeMillis());
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							Log.d("debug_PREVIEWSTATE", "after merging= "
									+ System.currentTimeMillis());
							current_state = STATE_FILTERS;
							switchFlashMode(true);
							setVideoOnSurfaceView(mediaPath);
							answerCreatedOnce = true;
							hideProgressBar();
						}
					});

				}
			}).start();

		} else
			Toast.makeText(getActivity(),
					"Please record your answer to proceed", Toast.LENGTH_LONG)
					.show();
	}

	/**
	 * @param mediaPath
	 *            : path of video you need to set on surfaceview for
	 *            preview/filter
	 * 
	 *            This method is responsible for setting surfaceview and Iplayer
	 *            for playing recorded
	 */
	public void setVideoOnSurfaceView(String mediaPath) {
		fileDebugLog(FILE_DEBUG_TAG + "setVideoOnSurfaceView", "Called");
		Log.e(TAG, "setVideoOnSurface");
		if (mediaPath == null) {
			return;
		}
		videoOnSurfacePath = mediaPath;
		// surfaceview_main.setVisibility(View.GONE);

		image_thumb.setImageBitmap(bmp);
		image_thumb.setVisibility(View.VISIBLE);

		TextureView oldTexture = surfaceview_main;
		ViewGroup textureParent = (ViewGroup) oldTexture.getParent();
		int index = textureParent.indexOfChild(oldTexture);
		textureParent.removeView(oldTexture);
		surfaceview_main = new TextureView(getActivity());
		surfaceview_main.setId(R.id.tv_recans_recordview);
		textureParent.addView(surfaceview_main, index);

		// camUtils.textureView = surfaceview_main;
		surfaceview_main.setClickable(true);
		surfaceview_main.setOnClickListener(this);
		surfaceview_main.setVisibility(View.GONE);
		surfaceview_main.setVisibility(View.VISIBLE);
		surfaceview_main.setSurfaceTextureListener(this);

		// surfaceview_main.setVisibility(View.VISIBLE);

		if (Build.VERSION.SDK_INT >= 18) {
			surfaceview_main.setOnTouchListener(new OnTouchListener() {
				private float initX;
				private float finalX;

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					Log.d(FILE_DEBUG_TAG, "ontouch called");
					switch (event.getAction()) {
					case MotionEvent.ACTION_UP:
						finalX = event.getX();
						float displacement = finalX - initX;
						if (Math.abs(displacement) >= DEF_THRESHOLD_DISPLACEMENT) {
							if (displacement < 0) {
								fragmentShader--;
								if (fragmentShader < 0)
									fragmentShader += FILTERS_COUNT;
							} else {
								fragmentShader++;
								if (fragmentShader >= FILTERS_COUNT)
									fragmentShader -= FILTERS_COUNT;
							}
						}
						if (renderer != null && vidTex != null) {
							Log.d(FILE_DEBUG_TAG, "surface is not null");
							renderer.setmShader(fragmentShader);
						}
						break;
					case MotionEvent.ACTION_DOWN:
						initX = event.getX();
						break;
					}
					return true;
				}
			});
		}

	}

	/**
	 * playvideo preview on surface
	 * 
	 */
	private void playVideo() {
		fileDebugLog(FILE_DEBUG_TAG + "playVideo", "Called");
		AlphaAnimation tempAlphaAnim = new AlphaAnimation(1.0f, 0.0f);
		tempAlphaAnim.setDuration(500);
		image_thumb.startAnimation(tempAlphaAnim);
		tempAlphaAnim.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				image_thumb.setVisibility(View.GONE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		});

		try {

			mPlayer = new MediaPlayer();
			if (Build.VERSION.SDK_INT >= 18) {
//				tv_swipe.setVisibility(View.VISIBLE);
				
				TextureRenderer.selectedFragmentShader = 0;
				
				renderer = new VideoTextureRenderer(getActivity(),
						surfaceview_main.getSurfaceTexture(),
						surfaceview_main.getWidth(),
						surfaceview_main.getHeight());
				vidTex = renderer.getVideoTexture();
				int mTryCount = 0;
				while (vidTex == null && mTryCount < 60) {
					mTryCount++;
					Log.d(FILE_DEBUG_TAG, "Trying to get surface");
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					vidTex = renderer.getVideoTexture();
				}
				if (vidTex != null) {
					tv_swipe.setVisibility(View.VISIBLE);
				} else {
					renderer.stopRendering();
				}
			}
			if (!mPlayer.isPlaying()) {
				Log.e("playvideo", "playVideo() prepareURL ");

				mPlayer.setDataSource(videoOnSurfacePath);
				if (Build.VERSION.SDK_INT < 18 || vidTex == null) {
					Log.d(FILE_DEBUG_TAG, " < 18surface is null");
					Thread.sleep(20);
					mPlayer.setSurface(new Surface(surfaceview_main
							.getSurfaceTexture()));
				} else {
					mPlayer.setSurface(new Surface(vidTex));
				}
				if (Build.VERSION.SDK_INT >= 18) {
					renderer.setVideoSize(surfaceview_main.getWidth(),
							surfaceview_main.getHeight());
				}
				mPlayer.setLooping(true);
				mPlayer.prepare();
				mPlayer.start();
			}
		} catch (Exception e) {
			Log.e(FILE_DEBUG_TAG, "exception in playlocalvideo" + e);
			e.printStackTrace();
		}
	}

	Handler galleryFileCopyHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			switch (msg.what) {
			case GALLERY_FILE_COPY_START:
				Bundle galleryFilePathBndl = msg.getData();
				if (galleryFilePathBndl != null) {
					final String galleryMediaPath = galleryFilePathBndl
							.getString("galleryFilePathName");
					showProgressDialog();
					new Thread(new Runnable() {

						@Override
						public void run() {
							mediaPath = FileUtils.newFile(getActivity(),
									FileType.VIDEO_FINAL, concerned_id)
									.getAbsolutePath();
							FileUtils.copyFile(galleryMediaPath, mediaPath);
							Message msg = Message.obtain();
							msg.what = GALLERY_FILE_COPY_DONE;
							sendMessage(msg);
						}
					}).start();
				}
				break;

			case GALLERY_FILE_COPY_DONE:
				bmp = VideoProcessingUtils.getThumbnail(mediaPath, -1);
				thumbnailPath = FileUtils.newFile(getActivity(),
						FileType.VIDEO_THUMBNAIL, concerned_id)
						.getAbsolutePath();
				FileUtils.createImageFile(bmp, thumbnailPath);
				current_state = STATE_FILTERS;
				setVideoOnSurfaceView(mediaPath);
				hideProgressBar();
				break;
			}

		}
	};

	/*
	 * onActivityResult is @Overrided here so that we get set video from gallery
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		fileDebugLog(FILE_DEBUG_TAG + "onActivityResult", "Called");
		Log.e("onActivity  rec_ans", " requ " + requestCode + " " + resultCode);
		if (requestCode == REQUEST_SHARE_ACTIVITY) {
			removeThisFragment();
		} else if (requestCode == REQUEST_FROM_GALLERY) {
			if (resultCode == Activity.RESULT_OK) {
				Uri mediaURI = intent.getData();
				if (mediaURI != null
						&& VideoProcessingUtils.isAValidVideo(mediaURI,
								getActivity())) {
					camUtils.removeSurfaceHolderCallback();
					String mediaPath_gallery = VideoProcessingUtils
							.getVideoPathFromURI(mediaURI, getActivity());

					Message msgToGalleryFileCopyHandler = Message.obtain();
					Bundle galleryFilePathNameBndl = new Bundle();
					galleryFilePathNameBndl.putString("galleryFilePathName",
							mediaPath_gallery);
					msgToGalleryFileCopyHandler.what = GALLERY_FILE_COPY_START;
					msgToGalleryFileCopyHandler
							.setData(galleryFilePathNameBndl);
					galleryFileCopyHandler
							.sendMessage(msgToGalleryFileCopyHandler);

					if (recordToolTipView != null) {
						recordToolTipView.remove();
					}
					btnCancel.setVisibility(View.VISIBLE);
					btnFlash.setVisibility(View.GONE);
					btnCamType.setVisibility(View.GONE);
					// current_state = STATE_FILTERS;

					Animation outToLeftAnimation1 = AnimUtils
							.outToLeftAnimation();
					AnimUtils.showSlideAnimation(rl_headerstrip_1,
							outToLeftAnimation1, true, null);
					Animation inFromRightAnimation1 = AnimUtils
							.inFromRightAnimation();
					AnimUtils.showSlideAnimation(rl_headerstrip_2,
							inFromRightAnimation1, false, null);

					rl_bottom_buttons.setVisibility(View.GONE);
					tv_question_text.setVisibility(View.GONE);
					// AnimUtils.showSlideAnimation(rl_filternthumb,
					// AnimUtils.inFromRightAnimation(), false, null);
				} else {
					camUtils.showPreview();
					camUtils.setSurfaceView(surfaceview_main);
					Controller.showToast(mContext,
							"Invalid video! Please select a valid one.");
				}
			}
		} else if (requestCode == 504) {
			image_thumb.setVisibility(View.VISIBLE);
			// image_thumb.setImageBitmap(bmp);
		}

		super.onActivityResult(requestCode, resultCode, intent);
	}

	private void showScaleAnimation(View v, int anim, AnimationListener listener) {
		fileDebugLog(FILE_DEBUG_TAG + "showScaleAnimation", "Called");
		Animation animation = AnimationUtils.loadAnimation(getActivity(), anim);
		// animation.setInterpolator(new BounceInterpolator());
		if (listener != null)
			animation.setAnimationListener(listener);
		ll_buttonrecord.startAnimation(animation);

	}

	private String getRandomClientPostId() {
		fileDebugLog(FILE_DEBUG_TAG + "getRandomClientPostId", "Called");
		Xeger xeger = new Xeger("[a-zA-Z0-9]{7}");
		return "a" + xeger.generate();
	}

	/**
	 * @param view
	 *            : view thats clicked :P
	 * 
	 *            User have done all possible process on video and now he wants
	 *            to post.. Huh!.. finally.
	 */
	public void userWantsToPost(View view) {
		fileDebugLog(FILE_DEBUG_TAG + "userWantsToPost", "Called");
		if (!answerPosted) {
			resetData();
			if (current_state == STATE_FILTERS) {
				if (renderer != null)
					renderer.stopRendering();
				if (mPlayer != null) {
					if (mPlayer.isPlaying())
						mPlayer.stop();
					mPlayer.reset();
					mPlayer.release();
					mPlayer = null;
				}
				video_player_state = STATE_VIDEO_STOPPED;
				// image_thumb.setVisibility(View.VISIBLE);
			}
			// mplayer.resetSelf();
			publishAnswer(mediaPath, thumbnailPath);
			answerPosted = true;
		}
	}

	/**
	 * @param videoPath
	 *            : path of final video to be uplaoded after compression
	 * @param imagePath
	 *            : path of video thumbnail
	 * 
	 *            This method saves current answer to queue(the pendingquestion
	 *            list) and starts the service if in stopped mode
	 */
	private void publishAnswer(String videoPath, String imagePath) {
		fileDebugLog(FILE_DEBUG_TAG + "publishAnswer", "Called");
		Log.i(TAG_UPLOAD, "publish Answer called");

		PendingAnswersContainer pendingAnswerContainer = PrefUtils
				.getPendingUploads();

		if (pendingAnswerContainer == null) {
			Log.i(TAG_UPLOAD, "mPendingAnswer container "
					+ pendingAnswerContainer);
			pendingAnswerContainer = new PendingAnswersContainer();
		}

		PendingAnswer pendingAnswer = new PendingAnswer();
		pendingAnswer.setQuestionObject(questionAnswering);
		client_id = getRandomClientPostId();
		pendingAnswer.setClientId(client_id);
		pendingAnswer.setVideoPath(videoPath);
		pendingAnswer.setImagePath(imagePath);

		pendingAnswerContainer.addPendingItem(pendingAnswer);

		Log.i(TAG_UPLOAD, "list size after adding new item: "
				+ pendingAnswerContainer.getPendingUploads().size());
		PrefUtils.savePendingUploads(pendingAnswerContainer);

		// start service to upload this pending answer
		SingleAnswerUploadService.start(getActivity(), pendingAnswer);
	}

	public void setSubmissionResultListener(SubmissionStatusListener listener) {
		fileDebugLog(FILE_DEBUG_TAG + "setSubmissionResultListener", "Called");
		this.submissionStatusListener = listener;

	}

	/**
	 * This is warning Dialog to show when user press back button and hasn't
	 * posted yet
	 * 
	 */
	public void showDiscardDialog() {
		fileDebugLog(FILE_DEBUG_TAG + "showDiscardDialog", "Called");
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		alertDialogBuilder.setTitle("Unsaved Changes");
		alertDialogBuilder.setMessage("Recorded video will be discarded.");
		alertDialogBuilder.setPositiveButton("Continue",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						removeFragmentAnyway();
					}
				});
		alertDialogBuilder.setNegativeButton("Back",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// cancel the alert box and put a Toast to the user
						dialog.cancel();

					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		// show alert
		alertDialog.show();
	}


	private void showProgressDialog() {
		fileDebugLog(FILE_DEBUG_TAG + "showProgressDialog", "Called");
		progressDialog = new ProgressDialog(getActivity(),
				android.R.style.Theme_Holo_Light_Dialog_MinWidth);
		progressDialog.setMessage("Please wait");
		progressDialog.setCancelable(false);
		progressDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.R.color.transparent));
		progressDialog.show();

	}

	private void hideProgressBar() {
		fileDebugLog(FILE_DEBUG_TAG + "hideProgressBar", "Called");
		if (progressDialog != null && progressDialog.isShowing())
			progressDialog.cancel();
	}

	private void recordButtonAnimation(final View v) {
		fileDebugLog(FILE_DEBUG_TAG + "recordButtonAnimation", "Called");
		scaleUp = AnimationUtils
				.loadAnimation(getActivity(), R.anim.anim_scale);
		scaleUp.setDuration(1000);
		scaleUp.setStartOffset(3000);

		// animation1 AnimationListener
		scaleUp.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation arg0) {
				// start animation2 when animation1 ends (continue)
				v.startAnimation(scaleDown);
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
			}

			@Override
			public void onAnimationStart(Animation arg0) {
			}

		});

		scaleDown = AnimationUtils.loadAnimation(getActivity(),
				R.anim.anim_scale_down);
		scaleDown.setDuration(1000);

		// animation2 AnimationListener
		scaleDown.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation arg0) {
				// start animation1 when animation2 ends (repeat)
				v.startAnimation(scaleUp);
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
			}

			@Override
			public void onAnimationStart(Animation arg0) {
			}

		});
		v.startAnimation(scaleUp);
	}

	/**
	 * printCurrentRAFStates() Prints the current values of state variables in
	 * RecordAnswerFragment. video_player_state, current_state,
	 * mediaRecorderState
	 */
	private void printCurrentRAFStates() {

		String strVideoPlayerState = "INVALID";
		String strCurrentState = "INVALID";
		String strMediaRecorderState = "INVALID";

		switch (video_player_state) {
		case STATE_VIDEO_STOPPED: {
			strVideoPlayerState = "VIDEO_STOPPED";
		}
			break;
		case STATE_VIDEO_PLAYING: {
			strVideoPlayerState = "VIDEO_PLAYING";
		}
			break;
		case STATE_VIDEO_PAUSED: {
			strVideoPlayerState = "VIDEO_PAUSED";
		}
			break;
		}

		switch (mediaRecorderState) {
		case MR_STATE_UNPREPARED: {
			strMediaRecorderState = "MR_UNPREPARED";
		}
			break;
		case MR_STATE_PREPARING: {
			strMediaRecorderState = "MR_PREPARING";
		}
			break;
		case MR_STATE_PREPARED: {
			strMediaRecorderState = "MR_PREPARED";
		}
			break;
		case MR_STATE_RECORDING: {
			strMediaRecorderState = "MR_RECORDING";
		}
			break;
		case MR_STATE_TEARINGDOWN: {
			strMediaRecorderState = "MR_TEARINGDOWN";
		}
			break;
		}

		switch (current_state) {
		case STATE_RECORDING: {
			strCurrentState = "OVERALL_STATE_RECORDING";
		}
			break;
		case STATE_FILTERS: {
			strCurrentState = "OVERALL_STATE_FILTERS";
		}
			break;
		case STATE_COVERPIC: {
			strCurrentState = "OVERALL_STATE_COVERPIC";
		}
			break;
		case STATE_THUMB_PROCESSING: {
			strCurrentState = "OVERALL_STATE_THUMB_PROCESSING";
		}
			break;
		}

		fileDebugLog(FILE_DEBUG_TAG + "states:", strCurrentState + " "
				+ strMediaRecorderState + " " + strVideoPlayerState);

	}

	private static void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
			int height) {
		fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureAvailable", "Called");
		if (current_state == STATE_FILTERS && mPlayer == null)
			playVideo();
	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture arg0) {
		return false;
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture arg0, int arg1,
			int arg2) {

	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture arg0) {

	}
}
