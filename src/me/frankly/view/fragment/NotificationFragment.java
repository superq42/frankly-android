package me.frankly.view.fragment;

import java.util.ArrayList;
import java.util.List;

import me.frankly.R;
import me.frankly.adapter.NotificationListAdapter;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.NotificationModel;
import me.frankly.model.newmodel.NotificationsList;
import me.frankly.util.JsonUtils;
import me.frankly.util.SLog;
import me.frankly.util.Utilities;
import me.frankly.view.ShareDialog;
import me.frankly.view.activity.BaseActivity;
import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

public class NotificationFragment extends Fragment implements RequestListener,
		OnScrollListener, OnItemClickListener {

	private BaseActivity mBaseActivity;

	private ListView mListView;
	private List<NotificationModel> mListNotification;
	private NotificationListAdapter mNotificationListAdapter;
	private boolean loading = false;
	private ImageView iv_loader;
	private String screenType;
	private int next_index = 0;
	private long current_time;
	private boolean apiFail = false;
	public static final int PAGE_SIZE = 10;
	private AnimationDrawable loaderAnimation;
	private View mRootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		screenType = getArguments().getString(Constant.ScreenSpecs.SCREEN_TYPE);
		if (getActivity() instanceof BaseActivity)
			mBaseActivity = (BaseActivity) getActivity();
		iv_loader = new ImageView(mBaseActivity);
		iv_loader.setImageResource(R.drawable.loader_general);
		iv_loader.setVisibility(View.INVISIBLE);
		loaderAnimation = (AnimationDrawable) iv_loader.getDrawable();

	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		mRootView = inflater.inflate(R.layout.fragment_notifications, null);
		mListView = (ListView) mRootView
				.findViewById(R.id.frag_notification_listview);
		mListNotification = new ArrayList<NotificationModel>();
		mNotificationListAdapter = new NotificationListAdapter(getActivity(),
				mListNotification);
		
		mListView.setOnScrollListener(this);
		
		// addFooterView must be called before setAdapter for below kitkat devices
		mListView.addFooterView(iv_loader);
		mListView.setAdapter(mNotificationListAdapter);
		
		mListView.setOnItemClickListener(this);

		fetchNotifications();

		Controller.registerGAEvent("NotificationFragment");

		return mRootView;
	}

	public void fetchNotifications() {
		if (Controller.isNetworkConnected(getActivity())) {

			if (!loading) {
				Log.d("Notification", "Going to fetch notifications");
				Controller.getNotification(getActivity(), next_index, screenType,
						PAGE_SIZE, this);
			}

		} else {

			Utilities.showEmptyScreen(mRootView, apiErrorListener,
					Constant.PLACEHOLDER_SCREENS.NO_INTERNET);
		}
	}

	public void displayNotifications(final List<NotificationModel> list) {
		((Activity) mBaseActivity).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				SLog.i("sumit", "DIsplay List Size " + list.size());
				mNotificationListAdapter.add(list, current_time);
			}
		});

	}

	@Override
	public void onRequestStarted() {
		loading = true;
		toggleLoading(true);
	}

	@Override
	public void onRequestCompleted(Object responseObject) {

		SLog.i("sumit ", "Reso " + responseObject.toString());
		if (responseObject != null) {
			NotificationsList notificationList = (NotificationsList) JsonUtils
					.objectify((String) responseObject, NotificationsList.class);
			if (notificationList != null
					&& notificationList.getNotifications() != null
					&& notificationList.getNotifications().size() > 0) {
				next_index = notificationList.getNext_index();
				current_time = notificationList.getCurrent_time();
				displayNotifications(notificationList.getNotifications());
			} else if(mListNotification.isEmpty()){
				((Activity) mBaseActivity).runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Utilities
								.showEmptyScreen(
										mRootView,
										inviteFriendsListener,
										Constant.PLACEHOLDER_SCREENS.EMPTY_NOTIFICATIONS);
					}
				});
			}
		}
		toggleLoading(false);
		loading = false;
	}

	@Override
	public void onRequestError(int errorCode, String message) {
		toggleLoading(false);
		loading = false;
		if (!apiFail) {
			((Activity) mBaseActivity).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Utilities.showEmptyScreen(mRootView, apiErrorListener,
							Constant.PLACEHOLDER_SCREENS.API_ERROR);
				}
			});

		} else if (apiFail) {
			Controller.showToast(getActivity(),
					"Please try again after some time.");
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (next_index < 0) {
			mListView.removeFooterView(iv_loader);
			return;
		}
		if (!loading) {
			int currentpos = firstVisibleItem + visibleItemCount;
			if (currentpos >= totalItemCount - 2) {
				fetchNotifications();
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

	private void toggleLoading(final boolean status) {
		((Activity) mBaseActivity).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (status) {
					Log.d("Notification", "Going to show loader now");
					iv_loader.setVisibility(View.VISIBLE);
					loaderAnimation.start();
					// mListView.addFooterView(mProgressBar);
					// mProgressBar.setVisibility(View.VISIBLE);
				}

				else {
					loaderAnimation.stop();
					iv_loader.setVisibility(View.INVISIBLE);
					// mListView.removeFooterView(mProgressBar);
					// mProgressBar.setVisibility(View.INVISIBLE);
				}
			}
		});

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		NotificationModel notificationObject = mListNotification.get(position);
		Log.i("sumit", "OnItemClick " + notificationObject.getDeeplink());
		// String deepLink = "frankly://post/53d83177df310306987ea97e";
		if (notificationObject.getDeeplink() != null) {
			mBaseActivity.handleDeepLink(
					Uri.parse(notificationObject.getDeeplink()), false);
		} else {
			Controller.showToast(mBaseActivity, "Some error occurred.");
		}
	}

	private OnClickListener inviteFriendsListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			showInviteFriendsDialog();
		}
	};

	private OnClickListener apiErrorListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Log.d("Answer", "in api error listener");
			if (Controller.isNetworkConnected(getActivity())) {
				apiFail = true;
				fetchNotifications();
			} else
				Controller.showToast(getActivity(),
						"There seems to be a problem "
								+ "with your internet connection");
		}
	};

	protected void showInviteFriendsDialog() {

		ShareDialog cdd = new ShareDialog(mBaseActivity,
				R.layout.share_dialog_post_layout, null, 1, false,
				Constant.ScreenSpecs.SCREEN_FEEDS, null);
		cdd.show();
	}

}
