package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.util.MyUtilities;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

public class SoftUpdateFragment extends DialogFragment implements
		OnClickListener {

	private ImageView crossImgView, updateImgView;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = new Dialog(getActivity(), R.style.Dialog_No_Border);

		// request a window without the title
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		View rootView = getActivity().getLayoutInflater().inflate(
				R.layout.frag_soft_update, null, false);
		crossImgView = (ImageView) rootView.findViewById(R.id.soft_cross);
		updateImgView = (ImageView) rootView.findViewById(R.id.update_now);
		crossImgView.setOnClickListener(this);
		updateImgView.setOnClickListener(this);

		dialog.setCancelable(true);
		
		dialog.setContentView(rootView);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#A1282828")));
		dialog.show();
		return dialog;
	}

	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup parent,
	// Bundle savedInstanceState) {
	// View rootView = inflater.inflate(R.layout.frag_soft_update, parent,
	// false);
	// crossImgView = (ImageView) rootView.findViewById(R.id.soft_cross);
	// updateImgView = (ImageView) rootView.findViewById(R.id.update_now);
	// crossImgView.setOnClickListener(this);
	// updateImgView.setOnClickListener(this);
	// return rootView;
	// }

	@Override
	public void onClick(View v) {

		int id = v.getId();

		switch (id) {
		case R.id.soft_cross:
			dismiss();
			break;
		case R.id.update_now:
			MyUtilities.openPlayStore(getActivity());
			dismiss();
			break;
		}
	}
}
