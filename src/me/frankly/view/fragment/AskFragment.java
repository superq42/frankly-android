package me.frankly.view.fragment;

import java.lang.ref.WeakReference;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.QuestionPostCompletionListener;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.IdOnlyResponse;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AskFragment extends Fragment implements OnClickListener,
		RequestListener {

	private EditText edtQuestion;
	private TextView txtAskQuestion;
	private String userId;
	private String userName;
	private int userType;
	private String submittedQuestionBody;
	private ImageView iv_loader, btn_anonymous;
	private AnimationDrawable loaderAnimation;
	private WeakReference<QuestionPostCompletionListener> questionPostCompletionListener;
	private Context mContext;
	private boolean isAnonymous;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#Ninja Ask.Frag:";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + "onCreateView", "Called");
		View view = inflater.inflate(R.layout.fragment_ask_question, container,
				false);

		ImageView imgBack = (ImageView) view.findViewById(R.id.ab_bb__back_btn);
		imgBack.setOnClickListener(this);

		edtQuestion = (EditText) view.findViewById(R.id.frag_ask_edt_question);
		iv_loader = (ImageView) view.findViewById(R.id.frag_ask_loader);
		iv_loader.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) iv_loader.getDrawable();

		btn_anonymous = (ImageView) view.findViewById(R.id.btn_ask_anonymous);

		txtAskQuestion = (TextView) view.findViewById(R.id.btn_ask_question);
		txtAskQuestion.setVisibility(View.VISIBLE);
		txtAskQuestion.setBackgroundColor(getResources().getColor(
				R.color.app_theme_color));
		txtAskQuestion.setOnClickListener(this);

		btn_anonymous.setOnClickListener(this);

		TextView txtTitle = (TextView) view.findViewById(R.id.ab_bb_title);
		txtTitle.setText(userName);
		getKeyboardForcefully(edtQuestion);

		isAnonymous = false;

		Controller.registerGAEvent("AskFragment");

		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		fileDebugLog(FILE_DEBUG_TAG + "onCreate", "Called");
		mContext = getActivity();
		userId = getArguments().getString(Constant.Key.KEY_USER_ID);
		userName = getArguments().getString(Constant.Key.KEY_USER_NAME);
		userType = getArguments().getInt(Constant.Key.KEY_USER_TYPE);

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {

		case R.id.btn_ask_question:
			fileDebugLog(FILE_DEBUG_TAG + "onClick-txtAskQuestion", "Called");
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				if (checkEnterCharacters()) {
					removeKeyboardForcefully(edtQuestion);
					askQuestion();
					registerMixpanelEvent(MixpanelUtils.ADD_QUESTION);

				}
			}
			break;
		case R.id.ab_bb__back_btn:
			fileDebugLog(FILE_DEBUG_TAG + "onClick-imgBack", "Called");
			removeKeyboardForcefully(edtQuestion);
			getActivity().onBackPressed();
			break;

		case R.id.btn_ask_anonymous:
			if (isAnonymous) {
				isAnonymous = false;
				btn_anonymous
						.setImageResource(R.drawable.btn_ask_not_ananymous);
			} else {
				isAnonymous = true;
				btn_anonymous
						.setImageResource(R.drawable.btn_ask_ananymous);
			}
			break;
		}
	}

	private boolean checkEnterCharacters() {
		fileDebugLog(FILE_DEBUG_TAG + "checkEnterCharacters", "Called");
		String question = edtQuestion.getText().toString().trim();

		if (question.length() > 10) {
			if (question.length() > 300) {
				Controller.showToast(getActivity(),
						"Question text can contain only 300 characters. "
								+ "Please shorten your question.");
				return false;
			} else
				return true;
		} else {
			Controller.showToast(getActivity(),
					getActivity().getString(R.string.question_check));
			return false;
		}
	}

	private void askQuestion() {
		fileDebugLog(FILE_DEBUG_TAG + "askQuestion", "Called");

		submittedQuestionBody = edtQuestion.getText().toString().trim();
		if (isAnonymous)
			Controller.postAQuestion(getActivity(), QuestionObject.TYPE_TEXT,
					null, submittedQuestionBody, true, userId, null, null,
					this, null);
		else
			Controller.postAQuestion(getActivity(), QuestionObject.TYPE_TEXT,
					null, submittedQuestionBody, false, userId, null, null,
					this, null);
		iv_loader.setVisibility(View.VISIBLE);
		loaderAnimation.start();
		txtAskQuestion.setVisibility(View.GONE);
		btn_anonymous.setVisibility(View.GONE);
	}

	public void removeKeyboardForcefully(View et) {
		fileDebugLog(FILE_DEBUG_TAG + "removeKeyboardForcefully", "Called");

		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
	}

	@Override
	public void onRequestStarted() {
		Log.d("question ask", "started");
	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		fileDebugLog(FILE_DEBUG_TAG + "onRequestCompleted", "Called");
		final IdOnlyResponse newPage = (IdOnlyResponse) JsonUtils.objectify(
				(String) responseObject, IdOnlyResponse.class);
		fileDebugLog(FILE_DEBUG_TAG + "onRequestCompleted-newPage",
				"onposted: " + responseObject);

		if (isAdded())
			((Activity) mContext).runOnUiThread(new Runnable() {

				@Override
				public void run() {
					loaderAnimation.stop();
					iv_loader.setVisibility(View.INVISIBLE);
					onQuestionAdded(newPage.getId());
					if (userType == UniversalUser.TYPE_REGULAR)
						Controller.showToast(mContext, "Question asked");
					getActivity().onBackPressed();
					Log.d("question ask", "completed");
				}
			});
	}

	@Override
	public void onRequestError(int errorCode, String message) {

		if (isAdded())
			((Activity) mContext).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Controller.showToast(((Activity) mContext),
							"Sorry your request couldn't be completed at this time. \n"
									+ "Please try again later.");
					loaderAnimation.stop();
					iv_loader.setVisibility(View.INVISIBLE);
					txtAskQuestion.setVisibility(View.VISIBLE);
				}
			});
	}

	protected void onQuestionAdded(String question_id) {
		fileDebugLog(FILE_DEBUG_TAG + "onQuestionAdded", "Called");
		Log.i("ask", "onQuestionAdded" + question_id);
		if (questionPostCompletionListener != null)
			questionPostCompletionListener.get().onQuestionPosted(
					submittedQuestionBody, question_id);
	}

	public void getKeyboardForcefully(final View et) {
		fileDebugLog(FILE_DEBUG_TAG + "getKeyboardForcefully", "Called");
		et.requestFocus();
		et.postDelayed(new Runnable() {
			public void run() {
				InputMethodManager keyboard = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(et, 10);
			}
		}, 50);
	}

	public void setQuestionCompletionListener(
			QuestionPostCompletionListener newListener) {
		fileDebugLog(FILE_DEBUG_TAG + "setQuestionCompletionListener", "Called");
		this.questionPostCompletionListener = new WeakReference<QuestionPostCompletionListener>(
				newListener);
	}

	public void registerMixpanelEvent(String event_name) {
		fileDebugLog(FILE_DEBUG_TAG + "registerMixpanelEvent", "Called");
		if (isAdded())
			MixpanelUtils.sendGenericEvent(userId, userName,
					String.valueOf(userType), Constant.ScreenSpecs.SCREEN_ASK,
					event_name, getActivity());
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		removeKeyboardForcefully(edtQuestion);
	}

	private void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}
}
