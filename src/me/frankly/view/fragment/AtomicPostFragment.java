package me.frankly.view.fragment;

import java.util.ArrayList;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Constant.Key;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.RequestListener;
import me.frankly.listener.SwipeAnimationDoneListener;
import me.frankly.listener.universal.action.listener.CommentListener;
import me.frankly.listener.universal.action.listener.PostLikeListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.newmodel.PostObject;
import me.frankly.util.ImageUtil;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.SharePackage;
import me.frankly.util.ShareUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.ShareDialog;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.FranklyHomeActivity;
import me.frankly.view.activity.PostActivity;
import me.frankly.view.activity.ProfileActivity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.nhaarman.supertooltips.ToolTipView.OnToolTipViewClickedListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class AtomicPostFragment extends SDKVideoFragment implements
		AnimationListener, CommentListener, UserFollowListener,
		PostLikeListener, OnToolTipViewClickedListener,
		SwipeAnimationDoneListener {
	private static final int REQUEST_CODE_COMMENT = 1, REQUEST_CODE_SHARE = 2;

	private ImageView mVidCoverImageViewInit;
	private View playView, pauseView;
	private PostObject mPostObject;
	private String mPostId;
	private TextView tvComment, tvLikeCount;
	private ImageView loadingView, iv_like_animate, ivLike, iv_delete;
	private String mParentScreen;

	private AnimationDrawable loaderAnimation;
	private ImageView ivShareSpecific1, ivFollow;
	private CircularImageView cvProfilepic;

	private ToolTipView askToolTipView, visitProfileToolTipView;
	private ToolTipRelativeLayout toolTipRelativeLayout;
	private TextView tv_Name, tvRank, tvAsk, tv_tap_play, tv_whatsapp_text,
			tv_share_text;

	private float dist = 1.5f;
	private int duration = 1000; // 2 sec
	private int fadeDuration = 400; // 1 sec
	private int rType = Animation.RELATIVE_TO_SELF;
	private float rotationAngle = 35;
	private int tType = Animation.RELATIVE_TO_SELF; // Type of translation
	private boolean isLoaderAnimating = false;
	private Animation fadeOut;
	private LinearLayout mShareLayout;
	private RelativeLayout mProfileLayout;
	private String userProfileId;
	private ArrayList<SharePackage> packageList;
	private BaseActivity mBaseActivity;
	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#skm APF:";

	private int whatsappCount;
	private int shareCount;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + "onCreateView", "Called");
		View mRootView = inflater.inflate(R.layout.fragment_atomic_post_new,
				container, false);
		if (getActivity() instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) getActivity();
		}
		Bundle args = getArguments();
		if (args != null && args.containsKey(Constant.Key.KEY_POST_OBJECT)) {
			mPostObject = (PostObject) JsonUtils.objectify(
					args.getString(Constant.Key.KEY_POST_OBJECT),
					PostObject.class);
			mParentScreen = getArguments().getString(
					Constant.Key.KEY_PARENT_SCREEN_NAME);
			setmPostId(mPostObject.getId());
		}
		if (args != null && args.containsKey(Constant.Key.KEY_IS_AUTOPLAY))
			setAutoPlay(args.getBoolean(Constant.Key.KEY_IS_AUTOPLAY));
		userProfileId = getArguments().getString(Constant.Key.KEY_USER_ID);

		loadingView = (ImageView) mRootView.findViewById(R.id.iv_loader);
		loadingView.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) loadingView.getDrawable();
		mVidCoverImageViewInit = (ImageView) mRootView
				.findViewById(R.id.atomic_post_coverview_init);
		ImageLoader.getInstance().displayImage(
				mPostObject.getAnswer().getMedia().getThumbnail_url(),
				mVidCoverImageViewInit);
		mShareLayout = (LinearLayout) mRootView.findViewById(R.id.share_ll);
		playView = mRootView.findViewById(R.id.iv_play);
		pauseView = mRootView.findViewById(R.id.rl_pause_view);
		mProfileLayout = (RelativeLayout) mRootView
				.findViewById(R.id.atomic_profile_ll);
		toolTipRelativeLayout = (ToolTipRelativeLayout) mRootView
				.findViewById(R.id.activity_main_tooltipRelativeLayout);
		tv_tap_play = (TextView) mRootView.findViewById(R.id.tv_tap_text);

		if (!PrefUtils.getIsFirstSwipeAnimShown()
				&& getActivity() instanceof FranklyHomeActivity
				&& (mParentScreen.equals(ScreenSpecs.SCREEN_CHANNEL)
						|| mParentScreen.equals(ScreenSpecs.SCREEN_FEEDS) || mParentScreen
							.equals(ScreenSpecs.SCREEN_DISCOVER))) {
			((FranklyHomeActivity) mBaseActivity)
					.addSwipeAnimationDoneListener(this);
			Log.d("AA", "Howdy Partner");
		} else {
			Log.d("AA", PrefUtils.getIsFirstSwipeAnimShown() + " "
					+ mParentScreen + " "
					+ (getActivity() instanceof FranklyHomeActivity));
		}
		iv_delete = (ImageView) mRootView.findViewById(R.id.delete_widget);

		setProfile(mRootView);
		setVideoView(mRootView);
		// setFooterView(mRootView);
		setPostInfoView(mRootView);
		setupShareView(mRootView);
		setQuestionView(mRootView);
		Controller.registerGAEvent(mParentScreen);
		UniversalActionListenerCollection.getInstance().addPostCommentListener(
				this, mPostId);
		UniversalActionListenerCollection.getInstance().addUserFollowListener(
				this, mPostObject.getAnswer_author().getId());
		UniversalActionListenerCollection.getInstance().addPostLikeListener(
				this, mPostId);
		
		if(args != null ) {
			int screenMode = args.getInt(Key.SCREEN_MODE);
			switch (screenMode) {
			case PostActivity.MODE_SHARE: {
				showCustomDialog();
				break;
			}
			case PostActivity.MODE_COMMENTS: {
				((BaseActivity) getActivity()).openComments(mPostObject,
						mParentScreen, REQUEST_CODE_COMMENT, this);
				break;
			}
			}
		}
		
		return mRootView;
	}

	@Override
	public void onStart() {
		fileDebugLog(FILE_DEBUG_TAG + "onStart", "Called");
		Bundle args = getArguments();
		if (args != null && args.containsKey(Constant.Key.KEY_IS_AUTOPLAY))
			setAutoPlay(args.getBoolean(Constant.Key.KEY_IS_AUTOPLAY));
		super.onStart();
	}

	@Override
	public void onResume() {
		fileDebugLog(FILE_DEBUG_TAG + "onResume", "Called");
		super.onResume();
		Log.i("Comment", "OnResume  post ID" + mPostObject.getId()
				+ " comments " + mPostObject.getComment_count());
		if (isAdded()) {
			tvComment.setText(String.valueOf(mPostObject.getComment_count()));
			if (mProfileLayout.getVisibility() != View.GONE) {
				updateWalkthrough();
			}
		}
	}

	private void setVideoView(View view) {
		fileDebugLog(FILE_DEBUG_TAG + "setVideoView", "Called");
		Log.d("NetworkSpeed", "Going to ask for video url");
		String video_url = ImageUtil.getNetworkBasedVideoUrl(mPostObject
				.getAnswer().getMedia_urls());
		if (video_url == null)
			video_url = mPostObject.getAnswer().getMedia().getMedia_url();
		Log.i("NewAtom", "Selected url" + video_url);

		setVideoUrl(video_url);

		setInitialView(pauseView);
		setPauseView(pauseView);
		setPostPlayView(pauseView);
		TextureView textureView = (TextureView) view
				.findViewById(R.id.atomic_post_textureview);
		setVideoTextureView(textureView);
		textureView.setOnClickListener(this);
		playView.setOnClickListener(this);
		pauseView.setOnClickListener(this);
	}

	private void setProfile(View view) {

		fileDebugLog(FILE_DEBUG_TAG + "setProfile", "Called");
		tv_Name = (TextView) view.findViewById(R.id.frag_proilfe_name);
		tvRank = (TextView) view.findViewById(R.id.frag_proilfe_rank);
		tv_Name.setText(mPostObject.getAnswer_author().getFull_name());
		tvRank.setText(mPostObject.getAnswer_author().getUser_title());
		tvAsk = (TextView) view.findViewById(R.id.frag_feed_profile_ask_btn);
		ivFollow = (ImageView) view.findViewById(R.id.frag_profile_img_plus);
		if (mPostObject.getAnswer_author().getUser_title() == null
				|| mPostObject.getAnswer_author().getUser_title().isEmpty()) {
			tvRank.setVisibility(View.GONE);
		} else
			tvRank.setText(mPostObject.getAnswer_author().getUser_title());
		cvProfilepic = (CircularImageView) view
				.findViewById(R.id.frag_profile_img);
		if (mPostObject.getAnswer().getMedia().getThumbnail_url() != null) {
			DisplayImageOptions options = CircularImageView
					.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
			cvProfilepic.setImageUrl(options, mPostObject.getAnswer_author()
					.getProfile_picture());
		}
		if (mParentScreen.equals(Constant.ScreenSpecs.SCREEN_PROFILE)
				&& userProfileId.equals(PrefUtils.getCurrentUserAsObject()
						.getId())) {
			iv_delete.setVisibility(View.VISIBLE);
			iv_delete.setOnClickListener(this);
			mProfileLayout.setVisibility(View.VISIBLE);
			tvAsk.setVisibility(View.GONE);
			ivFollow.setVisibility(View.GONE);
		} else {
			tvAsk.setOnClickListener(this);
			ivFollow.setOnClickListener(this);
			cvProfilepic.setOnClickListener(this);
			if (mPostObject.getAnswer_author().is_following()) {
				ivFollow.setImageResource(R.drawable.ic_followed);
			}
			if (mParentScreen.equals(ScreenSpecs.SCREEN_CHANNEL)
					|| mParentScreen.equals(ScreenSpecs.SCREEN_POST)) {
				tv_Name.setOnClickListener(this);
			}
		}
	}

	/*
	 * private void setFooterView(View view) { fileDebugLog(FILE_DEBUG_TAG +
	 * "setFooterView", "Called"); tvLikeCount = (TextView) view
	 * .findViewById(R.id.atomic_post_like_count_text); tvComment = (TextView)
	 * view .findViewById(R.id.atomic_post_comment_count_text); TextView
	 * tvViewCount = (TextView) view
	 * .findViewById(R.id.atomic_post_view_count_text);
	 * 
	 * iv_like_animate = (ImageView) view.findViewById(R.id.iv_like_animate);
	 * tvViewCount.setText(String.valueOf(mPostObject.getView_count()));
	 * tvLikeCount.setText(String.valueOf(mPostObject.getLiked_count()));
	 * tvComment.setText(String.valueOf(mPostObject.getComment_count())); if
	 * (mPostObject.isIs_liked()) {
	 * tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(
	 * R.drawable.btn_liked_footer, 0, 0, 0); }
	 * view.findViewById(R.id.atomic_post_comment_count).setOnClickListener(
	 * this);
	 * view.findViewById(R.id.atomic_post_like_count).setOnClickListener(this);
	 * //
	 * view.findViewById(R.id.atomic_post_view_count).setOnClickListener(this);
	 * 
	 * }
	 */

	private void setPostInfoView(View view) {
		whatsappCount = mPostObject.getWhatsappShareCount();
		shareCount = mPostObject.getOtherShareCount();
		tvLikeCount = (TextView) view
				.findViewById(R.id.atomic_post_like_count_text);
		tvComment = (TextView) view
				.findViewById(R.id.atomic_post_comment_count_text);
		ivLike = (ImageView) view.findViewById(R.id.like_widget);
		tv_whatsapp_text = (TextView) view.findViewById(R.id.tv_whatsapp_text);
		tv_share_text = (TextView) view.findViewById(R.id.tv_share_text);
		tvLikeCount.setText(String.valueOf(mPostObject.getLiked_count()));
		tvComment.setText(String.valueOf(mPostObject.getComment_count()));
		tv_whatsapp_text.setText("" + whatsappCount);
		tv_share_text.setText("" + shareCount);
		// tv_whatsapp_text.setVisibility(View.GONE);
		// tv_share_text.setVisibility(View.GONE);
		if (mPostObject.isIs_liked()) {
			ivLike.setImageResource(R.drawable.btn_liked_footer);
		}

		view.findViewById(R.id.like_widget).setOnClickListener(this);
		view.findViewById(R.id.comment_widget).setOnClickListener(this);
	}

	private void setQuestionView(View view) {
		fileDebugLog(FILE_DEBUG_TAG + "setQuestionView", "Called");
		TextView tvQuestion = (TextView) view
				.findViewById(R.id.info_display_paused_video_question_text);
		TextView tvQuestionAuthor = (TextView) view
				.findViewById(R.id.info_display_paused_video_question_author);
		if (!mPostObject.getQuestion().is_anonymous()) {
			tvQuestionAuthor.setOnClickListener(this);
		}
		tvQuestion.setText(mPostObject.getQuestion().getBody());
		tvQuestionAuthor.setText(mPostObject.getQuestion_author()
				.getFirst_name());

	}

	private void setupShareView(View view) {
		fileDebugLog(FILE_DEBUG_TAG + "setupShareView", "Called");
		Log.d("APF", "list");
		packageList = ShareUtils.getSharingPackages(getActivity(), "post");
		Log.d("APF", "list completed");
		ImageView ivShare = (ImageView) view.findViewById(R.id.share_widget1);
		ivShareSpecific1 = (ImageView) view.findViewById(R.id.share_widget2);
		// ivShareSpecific2 = (ImageView) view.findViewById(R.id.share_widget3);
		ivShare.setImageResource(R.drawable.btn_share_upper_new);
		ivShare.setOnClickListener(this);
		if (packageList != null && packageList.size() > 0) {
			for (int i = 0; i < 2; i++) {
				if (i == 0)
					ShareUtils.setupSharePackage(packageList.get(i),
							ivShareSpecific1, ((OnClickListener) this));
				/*
				 * else if (i == 1 && packageList.size() > 1)
				 * ShareUtils.setupSharePackage(packageList.get(i),
				 * ivShareSpecific2, ((OnClickListener) this)); else
				 * ivShareSpecific2.setVisibility(View.GONE);
				 */
			}
		} else {
			ivShare.setVisibility(View.VISIBLE);
			ivShare.setOnClickListener(this);
			ivShareSpecific1.setVisibility(View.GONE);
			// ivShareSpecific2.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		fileDebugLog(FILE_DEBUG_TAG + "onClick", "Called");
		switch (v.getId()) {
		case R.id.rl_pause_view:
		case R.id.atomic_post_textureview:
		case R.id.iv_play:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				playView.setVisibility(View.GONE);
				if (!PrefUtils.getIsFirstTapAnimShown()) {
					// tv_tap_play.setVisibility(View.GONE);
					PrefUtils.setIsFirstTapAnimShown(true);
				}
				if (visitProfileToolTipView != null) {
					visitProfileToolTipView.remove();
					getPauseView().findViewById(R.id.iv_overlay).setVisibility(
							View.GONE);
					getInitialView().findViewById(R.id.iv_overlay)
							.setVisibility(View.GONE);
					visitProfileToolTipView = null;

				}
				if (askToolTipView != null) {
					askToolTipView.remove();
					getPauseView().findViewById(R.id.iv_overlay).setVisibility(
							View.GONE);
					getInitialView().findViewById(R.id.iv_overlay)
							.setVisibility(View.GONE);
					askToolTipView = null;
				}
				togglePlayState();
			}
			break;
		case R.id.delete_widget:
			showDeletePopup();
			break;
		case R.id.atomic_post_like_count:
		case R.id.like_widget:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				if (!mPostObject.getIs_liked()) {
					likePost();
					mPostObject.setIs_liked(true);
					registerMixpanelEvent(MixpanelUtils.POST_LIKED, 0, null);
				} else {
					unLikePost();
					mPostObject.setIs_liked(false);
					registerMixpanelEvent(MixpanelUtils.POST_UNLIKED, 0, null);
				}
				UniversalActionListenerCollection.getInstance()
						.onPostLikeChanged(mPostId, mPostObject.getIs_liked(),
								mPostObject.getLiked_count());
			}
			break;
		case R.id.atomic_post_comment_count:
		case R.id.comment_widget:
			((BaseActivity) getActivity()).openComments(mPostObject,
					mParentScreen, REQUEST_CODE_COMMENT, this);
			registerMixpanelEvent(MixpanelUtils.NAVIGATION_COMMENTS, 0, null);
			break;

		case R.id.atomic_post_profile_ask_me_btn:
			mBaseActivity.askActivity(mPostObject.getAnswer_author());
			registerMixpanelEvent(MixpanelUtils.ASK_ME, 0, null);
			break;
		case R.id.info_display_paused_video_question_author:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				mBaseActivity.profileActivity(mPostObject.getQuestion_author()
						.getId(), mPostObject.getQuestion_author());

				registerMixpanelEvent(MixpanelUtils.NAVIGATION_PROFILE, 0,
						Constant.USER_ROLE.QUESTION_AUTHOR);
			}
			break;
		case R.id.frag_proilfe_name:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				if (visitProfileToolTipView != null) {
					visitProfileToolTipView.remove();
					getPauseView().findViewById(R.id.iv_overlay).setVisibility(
							View.GONE);
					getInitialView().findViewById(R.id.iv_overlay)
							.setVisibility(View.GONE);
					visitProfileToolTipView = null;
					PrefUtils.setIsFirstFollowAnimShown(true);
				}

				mBaseActivity.profileActivity(mPostObject.getAnswer_author()
						.getId(), mPostObject.getAnswer_author());

				registerMixpanelEvent(MixpanelUtils.NAVIGATION_PROFILE, 0,
						Constant.USER_ROLE.ANSWER_AUTHOR);
			}
			break;
		case R.id.frag_feed_profile_ask_btn:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				if (askToolTipView != null) {
					askToolTipView.remove();
					getPauseView().findViewById(R.id.iv_overlay).setVisibility(
							View.GONE);
					getInitialView().findViewById(R.id.iv_overlay)
							.setVisibility(View.GONE);
					askToolTipView = null;
					PrefUtils.setIsFirstAskProfileAnimShown(true);
				}
				mBaseActivity.askActivity(mPostObject.getAnswer_author());
				registerMixpanelEvent(MixpanelUtils.ASK_ME, 0, "");
			}
			break;
		case R.id.share_widget1:
			showCustomDialog();
			registerMixpanelEvent(MixpanelUtils.SHARE_DIALOG, 0, "");
			break;
		case R.id.share_widget2:
			shareSpecific(v, 0);
			whatsappCount++;
			shareCount++;
			tv_share_text.setText("" + shareCount);
			tv_whatsapp_text.setText("" + whatsappCount);
			Controller.platformSharePost(getActivity(), mPostObject.getId(),
					"whatsapp", mPlatformShareListener);
			break;
		case R.id.share_widget3:
			shareSpecific(v, 1);
			break;
		case R.id.frag_profile_img_plus:
		case R.id.frag_profile_img:
			if (Controller.isNetworkConnectedWithMessage(getActivity()))
				toggleFollow();
			break;

		}
	}

	@Override
	public void onPlayerPaused() {
		fileDebugLog(FILE_DEBUG_TAG + "onPlayerPaused", "Called");
		super.onPlayerPaused();
		updateWalkthrough();
		if (mShareLayout.getVisibility() != View.VISIBLE)
			mShareLayout.setVisibility(View.VISIBLE);

	}

	@Override
	public void onPlayerCompleted() {
		fileDebugLog(FILE_DEBUG_TAG + "onPlayerCompleted", "Called");
		super.onPlayerCompleted();
		updateWalkthrough();
		if (mShareLayout.getVisibility() != View.VISIBLE)
			mShareLayout.setVisibility(View.VISIBLE);

	}

	private boolean isMyProile(String userId) {
		fileDebugLog(FILE_DEBUG_TAG + "isMyProile", "Called");
		return userId.equals(PrefUtils.getUserID());
	}

	private void showCustomDialog() {
		fileDebugLog(FILE_DEBUG_TAG + "showCustomDialog", "Called");
		ShareDialog cdd = new ShareDialog(mBaseActivity,
				R.layout.share_dialog_post_layout, mPostObject, 0,
				isMyProile(mPostObject.getAnswer_author().getId()), "post",
				packageList);
		cdd.show();
		shareCount++;
		tv_share_text.setText("" + shareCount);
		Controller.platformSharePost(getActivity(), mPostObject.getId(),
				"other", mPlatformShareListener);
	}

	private void shareSpecific(View v, int i) {
		fileDebugLog(FILE_DEBUG_TAG + "shareSpecific", "Called");
		String packageName = null;
		String shareText = "";

		if (isMyProile(mPostObject.getAnswer_author().getId()))
			shareText = "Watch my video answer to \""
					+ mPostObject.getQuestion().getBody().trim() + "\" at "
					+ mPostObject.getWeb_link();
		else
			shareText = "Watch "
					+ mPostObject.getAnswer_author().getFull_name()
					+ "'s video answer to \""
					+ mPostObject.getQuestion().getBody().trim() + "\" at "
					+ mPostObject.getWeb_link();

		String shareSubject = getString(R.string.share_subject, mPostObject
				.getAnswer_author().getFirst_name());

		if (v.getTag() != null) {
			packageName = (String) v.getTag();

			MixpanelUtils.sendShareJewelEvent(packageName, mPostObject
					.getAnswer_author().getUsername(), String
					.valueOf(mPostObject.getAnswer_author().getUser_type()),
					ScreenSpecs.SCREEN_FEEDS, getActivity());
			ShareUtils.shareLinkUsingIntent(shareText, getActivity(),
					packageName, REQUEST_CODE_SHARE);

		} else {
			Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
					+ shareSubject));
			intentsms.putExtra("sms_body", shareText);
			startActivity(intentsms);
		}
		packageList.get(i).viewCount++;
	}

	private void likePost() {
		fileDebugLog(FILE_DEBUG_TAG + "likePost", "Called");
		/*
		 * iv_like_animate.setVisibility(View.VISIBLE);
		 * iv_like_animate.setImageResource(R.drawable.btn_liked_footer);
		 * iv_like_animate.startAnimation(initializeLikedAnimation());
		 * 
		 * tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(
		 * R.drawable.btn_liked_footer, 0, 0, 0);
		 */

		int count = mPostObject.getLiked_count() + 1;
		mPostObject.setLiked_count(count);
		tvLikeCount.setText(String.valueOf(count));
		ivLike.setImageResource(R.drawable.btn_liked_footer);
		Controller.likeThisPost(getActivity(), mPostObject.getId(),
				likeListener);
	}

	private void unLikePost() {
		fileDebugLog(FILE_DEBUG_TAG + "unLikePost", "Called");
		/*
		 * tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(
		 * R.drawable.btn_likes_footer, 0, 0, 0);
		 */
		ivLike.setImageResource(R.drawable.btn_like);
		int count = mPostObject.getLiked_count() - 1;
		mPostObject.setLiked_count(count);
		tvLikeCount.setText(String.valueOf(count));
	}

	private RequestListener likeListener = new RequestListener() {

		@Override
		public void onRequestStarted() {
			fileDebugLog(FILE_DEBUG_TAG + "onRequestStarted", "Called");
		}

		@Override
		public void onRequestError(int errorCode, final String message) {
			fileDebugLog(FILE_DEBUG_TAG + "onRequestError", "Called");
			if (isAdded())
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
					}
				});

		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			fileDebugLog(FILE_DEBUG_TAG + "onRequestCompleted", "Called");
			if (isAdded())
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
					}
				});
		}
	};

	private RequestListener mPlatformShareListener = new RequestListener() {

		@Override
		public void onRequestStarted() {
		}

		@Override
		public void onRequestError(int errorCode, final String message) {
		}

		@Override
		public void onRequestCompleted(Object responseObject) {

		}
	};

	private boolean mShouldShowTaptoPlay = true;

	@Override
	protected View getLoadingView(boolean show) {
		fileDebugLog(FILE_DEBUG_TAG + "getLoadingView", "Called");
		if (loaderAnimation != null) {
			if (!show && isLoaderAnimating) {
				loaderAnimation.stop();
				isLoaderAnimating = true;
			} else if (show && !isLoaderAnimating) {
				loaderAnimation.start();
				isLoaderAnimating = false;
			}
		}
		return loadingView;
	}

	@Override
	protected void hideAllViewsExceptCoverImage() {
		fileDebugLog(FILE_DEBUG_TAG + "hideAllViewsExceptCoverImage", "Called");
		if (tv_tap_play.getVisibility() == View.VISIBLE) {
			tv_tap_play.clearAnimation();
			tv_tap_play.setVisibility(View.GONE);
		}
		if (playView != null && playView.getVisibility() == View.VISIBLE) {

			mVidCoverImageViewInit.setVisibility(View.VISIBLE);
			playView.setVisibility(View.GONE);

		}
	}

	private AnimationSet initializeLikedAnimation() {
		fileDebugLog(FILE_DEBUG_TAG + "initializeLikedAnimation", "Called");
		AnimationSet likedAnimation;
		likedAnimation = new AnimationSet(true);
		likedAnimation.setFillAfter(false);

		RotateAnimation ra1 = new RotateAnimation(0, -rotationAngle, rType,
				0.5f, rType, 0.5f);
		ra1.setDuration(duration / 2);
		likedAnimation.addAnimation(ra1);

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(fadeDuration);
		likedAnimation.addAnimation(fadeIn);

		fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(fadeDuration);

		fadeOut.setDuration(fadeDuration);

		RotateAnimation ra2 = new RotateAnimation(0, 1.5f * rotationAngle,
				rType, 0.5f, rType, 0.5f);
		ra2.setDuration(duration);
		likedAnimation.addAnimation(ra2);
		likedAnimation.addAnimation(fadeOut);

		// Move to X: distance , Y: distance
		TranslateAnimation ta1 = new TranslateAnimation(tType, 0, tType, 0,
				tType, -0.5f, tType, -dist - 0.5f);

		ta1.setDuration(duration);
		// Add Translation to the set
		likedAnimation.addAnimation(ta1);
		likedAnimation.setAnimationListener(this);
		return likedAnimation;
	}

	@Override
	public void onAnimationStart(Animation animation) {

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		iv_like_animate.setVisibility(View.INVISIBLE);

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		fileDebugLog(FILE_DEBUG_TAG + "onActivityResult", "Called");
		Log.d("CommentCount", "CC" + (requestCode == REQUEST_CODE_COMMENT)
				+ " " + (data == null));
		if (requestCode == REQUEST_CODE_COMMENT && data != null) {

			Log.i("CommentCount",
					"Comment count atomic post frag="
							+ data.getIntExtra(Key.COMMENT_COUNT,
									mPostObject.getComment_count()));
			String id = data.getStringExtra(Constant.Key.COMMENT_ID);
			if (id.equals(mPostObject.getClient_id())) {

				int count = data.getIntExtra(Key.COMMENT_COUNT,
						mPostObject.getComment_count());
				mPostObject.setComment_count(count);
			}
		} else {
			Log.i("count", "Data null");
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPlayerPrepared() {
		fileDebugLog(FILE_DEBUG_TAG + "onPlayerPrepared", "Called");
		super.onPlayerPrepared();
	}

	@Override
	public View getDefaultCoverImage() {
		fileDebugLog(FILE_DEBUG_TAG + "getDefaultCoverImage", "Called");
		return mVidCoverImageViewInit;
	}

	@Override
	protected String[] getUserDetails() {
		fileDebugLog(FILE_DEBUG_TAG + "getUserDetails", "Called");
		return new String[] { mPostObject.getAnswer_author().getUsername(),
				String.valueOf(mPostObject.getAnswer_author().getUser_type()) };

	}

	private void toggleFollow() {
		fileDebugLog(FILE_DEBUG_TAG + "toggleFollow", "Called");
		if (!mPostObject.getAnswer_author().is_following()) {
			followRequest();
			ivFollow.setImageResource(R.drawable.ic_followed);
			mPostObject.getAnswer_author().setIs_following(true);
			registerMixpanelEvent(MixpanelUtils.USER_FOLLOWED, 0, "");
		} else {
			unfollowRequest();
			mPostObject.getAnswer_author().setIs_following(false);
			ivFollow.setImageResource(R.drawable.ic_add_profilepic);
			registerMixpanelEvent(MixpanelUtils.USER_UNFOLLOWED, 0, "");
		}
		UniversalActionListenerCollection.getInstance().onUserFollowChanged(
				mPostObject.getAnswer_author().getId(),
				mPostObject.getAnswer_author().is_following());
	}

	private void followRequest() {
		fileDebugLog(FILE_DEBUG_TAG + "followRequest", "Called");
		Controller.requestfollow(getActivity(), mPostObject.getAnswer_author()
				.getId(), new RequestListener() {

			@Override
			public void onRequestStarted() {

			}

			@Override
			public void onRequestError(int errorCode, String message) {

			}

			@Override
			public void onRequestCompleted(Object responseObject) {

			}
		});
	}

	private void unfollowRequest() {
		fileDebugLog(FILE_DEBUG_TAG + "unfollowRequest", "Called");
		Controller.requestUnfollow(getActivity(), mPostObject
				.getAnswer_author().getId(), new RequestListener() {

			@Override
			public void onRequestStarted() {

			}

			@Override
			public void onRequestError(int errorCode, String message) {

			}

			@Override
			public void onRequestCompleted(Object responseObject) {

			}
		});
	}

	public void registerMixpanelEvent(String event_name, int share_type,
			String profile_role) {
		fileDebugLog(FILE_DEBUG_TAG + "registerMixpanelEvent", "Called");
		if (isAdded()) {
			if (share_type == 0)
				MixpanelUtils.sendGenericEvent(mPostObject.getId(), mPostObject
						.getAnswer_author().getUsername(),
						String.valueOf(mPostObject.getAnswer_author()
								.getUser_type()), mParentScreen, event_name,
						getActivity());
			else
				MixpanelUtils.sendProfileNavigationEvent(mPostObject.getId(),
						mPostObject.getAnswer_author().getUsername(),
						profile_role, mParentScreen, event_name, getActivity());
		}
	}

	public String getmPostId() {
		fileDebugLog(FILE_DEBUG_TAG + "getmPostId", "Called");
		return mPostId;
	}

	public void setmPostId(String mPostId) {
		fileDebugLog(FILE_DEBUG_TAG + "setmPostId", "Called");
		this.mPostId = mPostId;
	}

	private static void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}

	private void addFollowToolTip() {
		if (!PrefUtils.getIsFirstFollowAnimShown()
				&& PrefUtils.getIsFirstSwipeAnimShown()
				&& PrefUtils.getIsFirstTapAnimShown()
				&& PrefUtils.getVideoPlayCount() >= 2
				&& visitProfileToolTipView == null && isAdded()
				&& mProfileLayout.getVisibility() == View.VISIBLE
				&& (mParentScreen.equals(ScreenSpecs.SCREEN_CHANNEL) || mParentScreen
						.equals(ScreenSpecs.SCREEN_POST))) {
			Log.e("check1", "feed profile inside 3");
			visitProfileToolTipView = Controller.addToolTipView(
					toolTipRelativeLayout, tv_Name, getActivity(),
					"Tap to visit Profile", false);
			pauseView.findViewById(R.id.iv_overlay).setVisibility(View.VISIBLE);
			visitProfileToolTipView.setOnToolTipViewClickedListener(this);
		} else if (PrefUtils.getIsFirstFollowAnimShown()
				&& visitProfileToolTipView != null) {
			visitProfileToolTipView.remove();
			getPauseView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getInitialView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			visitProfileToolTipView = null;
		}
	}

	@Override
	public void onToolTipViewClicked(ToolTipView toolTipView) {
		if (visitProfileToolTipView != null) {
			visitProfileToolTipView.remove();
			visitProfileToolTipView = null;
		}
		if (askToolTipView != null) {
			askToolTipView.remove();
			askToolTipView = null;
		}
		pauseView.findViewById(R.id.iv_overlay).setVisibility(View.GONE);
	}

	public void updateWalkthrough() {
		Log.d("asktooltip", "inside updatewalkthrough");
		if (isAdded()
				&& getActivity() instanceof FranklyHomeActivity
				&& !PrefUtils.getIsFirstTapAnimShown()
				&& (mParentScreen.equals(ScreenSpecs.SCREEN_DISCOVER)
						|| mParentScreen.equals(ScreenSpecs.SCREEN_FEEDS) || mParentScreen
							.equals(ScreenSpecs.SCREEN_CHANNEL))
				&& ((FranklyHomeActivity) getActivity()).findViewById(
						R.id.rl_swipe_animation).getVisibility() == View.GONE && mShouldShowTaptoPlay)
			tapAnimation();
		else if (tv_tap_play.getVisibility() == View.VISIBLE) {
			tv_tap_play.clearAnimation();
			tv_tap_play.setVisibility(View.GONE);
		}
		addFollowToolTip();
		addAskToolTip();
	}

	private void tapAnimation() {
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);
		fadeIn.setRepeatMode(Animation.REVERSE);
		fadeIn.setRepeatCount(Animation.INFINITE); // made the animation repeat
		tv_tap_play.setVisibility(View.VISIBLE);
		tv_tap_play.setShadowLayer(1, 1, 2, Color.parseColor("#CC000000"));
		tv_tap_play.setAnimation(fadeIn);
		Animation playViewFadeIn = new AlphaAnimation(0, 1);
		playViewFadeIn.setInterpolator(new DecelerateInterpolator());
		playViewFadeIn.setDuration(1000);
		playViewFadeIn.setRepeatCount(0);
		playView.setAnimation(playViewFadeIn);

	}

	private void addAskToolTip() {
		Log.d("asktooltip", "outside");
		Log.e("check1", "feed profile inside addtooltip");
		if (PrefUtils.getIsFirstFollowAnimShown()
				&& PrefUtils.getIsFirstFollowProfileAnimShown()
				&& !PrefUtils.getIsFirstAskProfileAnimShown()
				&& PrefUtils.getVideoPlayCount() >= 5 && askToolTipView == null) {
			Log.d("asktooltip", "inside");
			askToolTipView = Controller.addToolTipView(toolTipRelativeLayout,
					tvAsk, getActivity(), "Tap to Ask", false);
			pauseView.findViewById(R.id.iv_overlay).setVisibility(View.VISIBLE);
			askToolTipView.setOnToolTipViewClickedListener(this);
		} else if (PrefUtils.getIsFirstAskProfileAnimShown()
				&& askToolTipView != null) {
			askToolTipView.remove();
			getPauseView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getInitialView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			askToolTipView = null;
		}
	}

	@Override
	public void onDestroy() {
		UniversalActionListenerCollection.getInstance()
				.removePostCommentListener(this, mPostId);
		UniversalActionListenerCollection.getInstance()
				.removeUserFollowListener(this,
						mPostObject.getAnswer_author().getId());
		UniversalActionListenerCollection.getInstance().removePostLikeListener(
				this, mPostId);
		super.onDestroy();
	}

	@Override
	public void onCommentDone(String postId, String newCommentId,
			int newCommentCount) {
		if (isAdded()) {
			Log.d("APF", "comment count is " + newCommentCount);
			mPostObject.setComment_count(newCommentCount);
			tvComment.setText(String.valueOf(newCommentCount));
		}
	}

	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		if (mPostObject.getAnswer_author().getId().equals(userId)) {
			mPostObject.getAnswer_author().setIs_following(isFollowed);
			if (isAdded()) {
				if (isFollowed)
					ivFollow.setImageResource(R.drawable.ic_followed);
				else
					ivFollow.setImageResource(R.drawable.ic_add_profilepic);

			}
		}

	}

	@Override
	public void onPostLikeChanged(String postId, boolean isLiked,
			int newLikeCount) {
		if (isAdded() && mPostId.equals(postId) && mPostObject != null) {
			mPostObject.setIs_liked(isLiked);
			mPostObject.setLiked_count(newLikeCount);
			/*
			 * if (isLiked) tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(
			 * R.drawable.btn_liked_footer, 0, 0, 0); else
			 * tvLikeCount.setCompoundDrawablesWithIntrinsicBounds(
			 * R.drawable.btn_likes_footer, 0, 0, 0);
			 */
			tvLikeCount.setText(String.valueOf(newLikeCount));
		}

	}

	@Override
	public void onSwipeDone() {
		mShouldShowTaptoPlay = true;
	}

	@Override
	public void onSwipeStarted() {
		mShouldShowTaptoPlay = false;
		if (tv_tap_play.getVisibility() == View.VISIBLE) {
			tv_tap_play.clearAnimation();
			tv_tap_play.setVisibility(View.GONE);
		}
	}
	private void showDeletePopup() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		alertDialogBuilder.setTitle("Delete Answer");
		alertDialogBuilder.setMessage("Do you want to delete this answer?");

		alertDialogBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						deleteAnswer();
					}
				});
		alertDialogBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		// show alert
		alertDialog.show();

	}

	private void deleteAnswer() {
		getLoadingView(true).setVisibility(View.VISIBLE);
		Controller.deleteAnswer(getActivity(), mPostObject.getId(),
				new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(final int errorCode, final String message) {
						if (isAdded())
							getActivity().runOnUiThread(new Runnable() {

								@Override
								public void run() {
									getLoadingView(false).setVisibility(
											View.GONE);
									switch (errorCode) {
									case Constant.ERROR_CODES.NETWORK_NOT_AVAILABLE:
										Controller
										.showToast(
												getActivity(),
												"No internet! Please check your network connection.");
										break;
									case Constant.ERROR_CODES.SHIT_HAPPENED:
										Controller
										.showToast(
												getActivity(),
												"Sorry an error occurred while deleting your answer. \n"
														+ "Please try again after some time.");
										break;
									case Constant.ERROR_CODES.BAD_INTERNET:
										Controller
										.showToast(
												getActivity(),
												"Slow internet! Please check your network connection.");
										break;
									default:
										Controller
										.showToast(
												getActivity(),
												"Sorry an error occurred while deleting your answer. \n"
														+ "Please try again after some time.");
										break;
									}
								}
							});

					}

					@Override
					public void onRequestCompleted(Object responseObject) {
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								getLoadingView(false).setVisibility(View.GONE);
							}
						});
						try {
							JSONObject response = new JSONObject(
									(String) responseObject);
							if (response.getBoolean("success")) {
								/*
								 * server response : answer deleted
								 */

								if (getActivity() instanceof ProfileActivity) {
									((ProfileActivity) getActivity())
											.deleteAnswerFromAdapter(response
													.getString("id"));
								}
							} else {
								/*
								 * server response : answer deletion failed
								 */
								getActivity().runOnUiThread(new Runnable() {
									@Override
									public void run() {
										Controller
												.showToast(
														getActivity(),
														"Sorry an error occurred while deleting your answer. \n"
																+ "Please try again after some time.");

									}
								});
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});
	}
}