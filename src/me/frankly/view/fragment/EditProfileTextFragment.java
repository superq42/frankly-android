package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class EditProfileTextFragment extends Fragment implements
		OnClickListener, RequestListener {

	private EditText edtUserName;
	private EditText edtUserInfo;
	private String userName;
	private String userBio;
	//private ImageView ivBackground;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userName = (String) getArguments().get(Constant.Key.KEY_USER_NAME);
		userBio = (String) getArguments().get(Constant.Key.KEY_USER_BIO);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_editprofiletext,
				container, false);

		edtUserInfo = (EditText) view
				.findViewById(R.id.frag_editproftxt_userinfo);
		edtUserName = (EditText) view
				.findViewById(R.id.frag_editproftxt_username);

		edtUserInfo.setText(userBio);
		edtUserName.setText(userName);
		edtUserName.setFocusable(true);

		ImageView imgBack = (ImageView) view.findViewById(R.id.ab_bb__back_btn);
		imgBack.setOnClickListener(this);

		ImageView imgSubmit = (ImageView) view.findViewById(R.id.ab_bb_submit);
		imgSubmit.setVisibility(View.VISIBLE);
		imgSubmit.setOnClickListener(this);

		TextView tvTitle = (TextView) view.findViewById(R.id.ab_bb_title);
		tvTitle.setText(R.string.edit_details);
		getKeyboardForcefully(edtUserName);
		
		Controller.registerGAEvent("EditProfileTextFragment");

		return view;
	}

	@Override
	public void onClick(View v) {
		removeKeyboardForcefully(edtUserName);
		switch (v.getId()) {
		case R.id.ab_bb__back_btn:
			MyUtilities.hideKeyboard(edtUserName, getActivity());
			onBackresult();
			break;
		case R.id.ab_bb_submit:

			MyUtilities.hideKeyboard(edtUserName, getActivity());
			if (isInputValid() && Controller.isNetworkConnected(getActivity())) {
				UniversalUser user = PrefUtils.getCurrentUserAsObject();
				user.setFirst_name(edtUserName.getText().toString());
				user.setBio(edtUserInfo.getText().toString());
				PrefUtils.saveCurrentUserAsJson(JsonUtils.jsonify(user));
				uploadInfo();
				onBackresult();
			}
			break;

		}
	}

	private void onBackresult() {
		Intent intent = new Intent();

		intent.putExtra(Constant.Key.KEY_USER_NAME, edtUserName.getText()
				.toString());
		intent.putExtra(Constant.Key.KEY_USER_BIO, edtUserInfo.getText()
				.toString());
		getTargetFragment().onActivityResult(getTargetRequestCode(),
				Activity.RESULT_OK, intent);
		getFragmentManager().popBackStack();
	}

	private void uploadInfo() {
		final EditableProfileObject profile = new EditableProfileObject();
		profile.setBio(edtUserInfo.getText().toString());
		profile.setFirst_name(edtUserName.getText().toString());
		Controller.requestUpdateProfile(getActivity(),profile, this);
	}

	private boolean isInputValid() {
		int nameLength = edtUserName.getText().toString().length();
		int bioLength = edtUserInfo.getText().toString().length();
		boolean result = true;
		if (nameLength < 3) {
			Controller.showToast(getActivity(),
					getActivity().getString(R.string.name_length_error));
			result = false;
		} else if (nameLength > 30) {
			Controller.showToast(getActivity(),
					getActivity().getString(R.string.name_length_max_error));
			result = false;
		}
		if (bioLength > 140) {
			Controller.showToast(getActivity(),
					getActivity().getString(R.string.bio_length_max_error));
			result = false;
		}

		return result;
	}

	public void removeKeyboardForcefully(View et) {
		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(et.getWindowToken(), 0);

	}

	public void getKeyboardForcefully(final View et) {
		et.requestFocus();
		et.postDelayed(new Runnable() {
			public void run() {
				InputMethodManager keyboard = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(et, 10);
			}
		}, 50);
	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
	}

	@Override
	public void onRequestError(int errorCode, String message) {
	}
}
