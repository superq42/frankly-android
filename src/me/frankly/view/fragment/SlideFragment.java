package me.frankly.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import me.frankly.R;

public class SlideFragment extends Fragment implements OnClickListener {

	public static final String KEY_BACKGROUND_ID = "bg_id";
	public static final String KEY_CAPTION = "caption";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle args = getArguments();
		int bgId = args.getInt(KEY_BACKGROUND_ID);
		String caption = args.getString(KEY_CAPTION);
		View mRootView = inflater.inflate(R.layout.layout_one, null);
		ImageView iv = (ImageView) mRootView.findViewById(R.id.iv_walkthrough);
		TextView captionTextView = (TextView) mRootView
				.findViewById(R.id.tv_caption);
		captionTextView.setText(Html.fromHtml(caption));
		iv.setImageResource(bgId);
		// View singUpView = mRootView.findViewById(R.id.tv_join_now);
		// singUpView.setVisibility(args.getBoolean(KEY_SHOW_BTN) ? View.VISIBLE
		// : View.GONE);
		// singUpView.setOnClickListener(this);
		return mRootView;
	}

	@Override
	public void onClick(View v) {
	}
}
