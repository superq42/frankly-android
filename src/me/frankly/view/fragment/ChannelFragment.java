package me.frankly.view.fragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import me.frankly.R;
import me.frankly.SDKPlayer.IPlayer.IPlayerStateChangedListener;
import me.frankly.adapter.FeedsPagerAdapter;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.RequestListener;
import me.frankly.listener.universal.action.listener.CommentListener;
import me.frankly.listener.universal.action.listener.PostLikeListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.response.DiscoverableObjectsContainer;
import me.frankly.model.response.PaginatedPostHolder;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.util.Utilities;
import me.frankly.view.activity.FeedBackActivity;
import me.frankly.view.activity.FranklyHomeActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.DirectionalViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class ChannelFragment extends Fragment implements RequestListener,
		OnPageChangeListener, CommentListener, UserFollowListener,
		PostLikeListener {
	private String mChannelId;
	private int mNextIndex = 0;

	private DirectionalViewPager mViewPager;
	private Handler handle;
	public FeedsPagerAdapter mPagerAdapter;
	private ArrayList<DiscoverableObjectsContainer> mPostObjects;
	private boolean isMoreDataAvailable = true, isLoading = false;
	private boolean isRetry;
	private IPlayerStateChangedListener parentPlayerListener;
	private ProgressDialog mRetryDialog;
	private View emptyContainerView;
	private View mRootView;
	public int currPagePosition = 0;

	private static int counter;
	private static boolean showFeedBackPopUp = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null && args.containsKey(Constant.Key.KEY_CHANNEL_ID))
			mChannelId = args.getString(Constant.Key.KEY_CHANNEL_ID);
		else
			mChannelId = FranklyHomeActivity.DEF_CHANNEL_ID;
		UniversalActionListenerCollection.getInstance()
				.addGeneralCommentListener(this);
		UniversalActionListenerCollection.getInstance()
				.addGeneralFollowListener(this);
		UniversalActionListenerCollection.getInstance().addGeneralLikeListener(
				this);
		mPostObjects = new ArrayList<DiscoverableObjectsContainer>();
		mPagerAdapter = new FeedsPagerAdapter(getChildFragmentManager(),
				mPostObjects, Constant.ScreenSpecs.SCREEN_CHANNEL);
		handle = new Handler();
		if (getActivity() instanceof FranklyHomeActivity)
			parentPlayerListener = (IPlayerStateChangedListener) getActivity();
		getChannelStream();
		showFeedBackPopUp = PrefUtils.getAppOpenCount() % 3 == 0
				&& !PrefUtils.isFeedbackShown();
		counter = 1;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.layout_list_fragment, null);
		mViewPager = (DirectionalViewPager) mRootView
				.findViewById(R.id.vp_fragment);
		mViewPager.setOrientation(DirectionalViewPager.VERTICAL);
		mPagerAdapter.setPlayerStateChangeListner(parentPlayerListener);
		mViewPager.setAdapter(mPagerAdapter);
		mViewPager.setOnPageChangeListener(this);
		mRetryDialog = new ProgressDialog(getActivity(),
				ProgressDialog.THEME_HOLO_LIGHT);

		emptyContainerView = mRootView.findViewById(R.id.rl_empty_screen);

		showFeedBackPopUp = PrefUtils.getAppOpenCount() % 3 == 0
				&& !PrefUtils.isFeedbackShown();

		Log.d("FBQ", "showFeedback : " + showFeedBackPopUp
				+ " isFeedbackShown : " + PrefUtils.isFeedbackShown());
		counter = 1;

		return mRootView;
	}

	private void getChannelStream() {
		isLoading = true;
		Controller.getChannelDown(mChannelId, String.valueOf(mNextIndex), 10,
				this, getActivity());

	}

	private OnClickListener apiErrorListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (Controller.isNetworkConnected(getActivity())) {

				isRetry = true;
				mViewPager.setVisibility(View.VISIBLE);
				getChannelStream();

			} else
				Controller.showToast(getActivity(),
						"There seems to be a problem "
								+ "with your internet connection");
		}
	};

	@Override
	public void onRequestStarted() {
		if (isRetry)
			handle.post(new Runnable() {
				@Override
				public void run() {
					try {
						if (mPostObjects.isEmpty()) {

							mRetryDialog.setMessage("Trying again...");
							mRetryDialog.show();
						}
					} catch (Exception e) {
						Controller.showToast(getActivity(), "Trying again...");
					}

				}
			});

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		final PaginatedPostHolder newPage = (PaginatedPostHolder) JsonUtils
				.objectify((String) responseObject, PaginatedPostHolder.class);
		mNextIndex = newPage.getNext_index();

		isMoreDataAvailable = mNextIndex > 0;
		// toggleRetryDialog(false) ;
		isLoading = false;
		isRetry = false;

		if (isAdded())
			handle.post(new Runnable() {

				@Override
				public void run() {

					if (!newPage.getStream().isEmpty()
							|| newPage.getHeader() != null) {

						Utilities.removeEmptyScreen(emptyContainerView,
								mRootView);
						mPagerAdapter.setMoreDataAvailable(isMoreDataAvailable);

						if (mRetryDialog.isShowing())
							mRetryDialog.hide();

						mPostObjects = MyUtilities.appendUniqueItems(
								removeUnknowns(newPage.getStream()),
								mPostObjects, MyUtilities.MODE_PULL_TO_REFRESH);
						if (newPage.getHeader() != null) {
							if (!mPostObjects.isEmpty()
									&& !newPage
											.getHeader()
											.getId()
											.equals(mPostObjects.get(0).getId()))
								mPostObjects.add(0, newPage.getHeader());
							else if (mPostObjects.isEmpty())
								mPostObjects.add(0, newPage.getHeader());
						}

						for (int i = 0; i < mPostObjects.size(); i++)

							if (PrefUtils.getCurrentUserAsObject() == null
									|| mPostObjects
											.get(i)
											.getType()
											.equals(DiscoverableObjectsContainer.TYPE_UPLOAD)
									&& PrefUtils.getCurrentUserAsObject()
											.getProfile_video() != null)
								mPostObjects.remove(i);
						mPagerAdapter.setData(mPostObjects);

						// mPostObjects.addAll(newPage.getStream());
						Log.d("LastCard",
								"Going to notify that data set has changed");

						mPagerAdapter.notifyDataSetChanged();
						if (!isMoreDataAvailable)
						{
							mPagerAdapter.setDataBooleans(false, false);
						}
					} else if (isMoreDataAvailable)
						getChannelStream();
					else if (!mPostObjects.isEmpty()) {
						mPagerAdapter.setDataBooleans(false, false);
						mPagerAdapter.notifyDataSetChanged();
					}
					else {
						mPagerAdapter.setDataBooleans(false, true);
						mPagerAdapter.notifyDataSetChanged();
						((FranklyHomeActivity) getActivity()).stopSwipeTimer();
						Utilities.showEmptyScreen(mRootView,
								new OnClickListener() {

									@Override
									public void onClick(View v) {
										Log.d("blankfeedfix", "click");
										if (getActivity() instanceof FranklyHomeActivity) {
											((FranklyHomeActivity) getActivity())
													.showChannel(Constant.CHANNEL.DISCOVER);
											// ((FranklyHomeActivity)getActivity()).showRemoteControl();
										}
									}
								}, Constant.PLACEHOLDER_SCREENS.NO_FEED);
						
					}
				}
			});

	}

	@Override
	public void onRequestError(int errorCode, String message) {
		if (isAdded())
			handle.post(new Runnable() {
				@Override
				public void run() {
					if (mRetryDialog.isShowing())
						mRetryDialog.hide();
				}
			});
		mPagerAdapter.setDataBooleans(false, true);
		mPagerAdapter.notifyDataSetChanged();
		/*
		 * The api error or bad internet screen should not be shown when there
		 * is a retry
		 */
		if (mPostObjects != null && mPostObjects.isEmpty() && !isRetry) {
			final int error_code = errorCode;
			if (isAdded()){
				handle.post(new Runnable() {

					@Override
					public void run() {
						Log.d("kitty", "switch (error_code) " + error_code);
						switch (error_code) {
						case Constant.ERROR_CODES.SHIT_HAPPENED:
							Utilities.showEmptyScreen(mRootView,
									apiErrorListener,
									Constant.PLACEHOLDER_SCREENS.NO_INTERNET);
							break;
						case Constant.ERROR_CODES.BAD_INTERNET:
							Utilities.showEmptyScreen(mRootView,
									apiErrorListener,
									Constant.PLACEHOLDER_SCREENS.BAD_INTERNET);
							break;
						default:
							Utilities.showEmptyScreen(mRootView,
									apiErrorListener,
									Constant.PLACEHOLDER_SCREENS.API_ERROR);
							break;
						}

					}
				});
			}
		} else if (isRetry && isAdded())
			Controller.showToast(getActivity(),
					"Please try again after some time.");
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {
		currPagePosition = arg0;
		if (!PrefUtils.getIsFirstSwipeAnimShown() && arg0 == 1) {
			((FranklyHomeActivity) getActivity()).stopSwipeTimer();
			((FranklyHomeActivity) getActivity()).wellDoneSwipe();
		}
		updateUI();

		if (showFeedBackPopUp) {
			if (++counter % 3 == 0) {
				Intent intent = new Intent(getActivity(),
						FeedBackActivity.class);
				getActivity().startActivity(intent);
				showFeedBackPopUp = false;
				PrefUtils.setFeedbackShown(true);
			}
		}

		playAtPos(arg0);
		if (isNearEnd(arg0) && !isLoading && isMoreDataAvailable)
		{
			getChannelStream();
		}
			
	}

	public void updateUI() {
		List<Fragment> fragments = getChildFragmentManager().getFragments();
		Log.i("fragment", "" + fragments.size());
		for (Fragment fr : fragments) {
			if (fr != null
					&& fr.isVisible()
					&& (!PrefUtils.getIsFirstFollowAnimShown() || !PrefUtils
							.getIsFirstTapAnimShown())
					|| !PrefUtils.getIsFirstAskAnimShown()) {
				if (fr instanceof FeedProfileFragment)
					((FeedProfileFragment) fr).updateWalkthrough();
				else if (fr instanceof CelebQuestionsFragment) {
					((CelebQuestionsFragment) fr).updateWalkthrough();
				} else if (fr instanceof AtomicPostFragment)
					((AtomicPostFragment) fr).updateWalkthrough();
			}
		}
	}

	private boolean isNearEnd(int pos) {
		return mPostObjects.size() - pos <= 3;
	}

	private void playAtPos(int i) {
		mPagerAdapter.playVideoAtPosition(i);
	}

	private ArrayList<DiscoverableObjectsContainer> removeUnknowns(
			ArrayList<DiscoverableObjectsContainer> stream) {
		Iterator<DiscoverableObjectsContainer> i = stream.iterator();
		while (i.hasNext()) {
			DiscoverableObjectsContainer temp = i.next();
			if (!DiscoverableObjectsContainer.ACCEPTED_TYPE.contains(temp
					.getType()))
				i.remove();
		}
		return stream;

	}

	public void setInFocus(boolean inFocus) {
		if (mPagerAdapter != null && !inFocus) {
			mPagerAdapter.pauseIfAny();

		}
	}

	public void onActivityPaused() {

		if (mPagerAdapter != null && mViewPager != null)
			mPagerAdapter.stopIfAny();
	}

	public void setPagerCurChildPos(int pos) {
		mViewPager.setCurrentItem(pos);
	}

	@Override
	public void onCommentDone(String postId, String newCommentId,
			int newCommentCount) {
		for (DiscoverableObjectsContainer objectsContainer : mPostObjects)
			if (objectsContainer != null
					&& objectsContainer.getType().equals(
							DiscoverableObjectsContainer.TYPE_POST)
					&& objectsContainer.getPost() != null
					&& objectsContainer.getPost().getId().equals(postId)) {
				objectsContainer.getPost().setComment_count(newCommentCount);
				break;
			}

	}

	@Override
	public void onDestroy() {
		UniversalActionListenerCollection.getInstance()
				.removeGeneralCommentListener(this);
		UniversalActionListenerCollection.getInstance()
				.removeGeneralFollowListener(this);
		UniversalActionListenerCollection.getInstance()
				.removeGeneralLikeListener(this);
		super.onDestroy();
	}

	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		for (DiscoverableObjectsContainer objectsContainer : mPostObjects) {
			if (objectsContainer.getType().equals(
					DiscoverableObjectsContainer.TYPE_POST)
					&& objectsContainer.getPost().getAnswer_author().getId()
							.equals(userId)) {
				objectsContainer.getPost().getAnswer_author()
						.setIs_following(isFollowed);
			} else if (objectsContainer.getType().equals(
					DiscoverableObjectsContainer.TYPE_USER)
					&& objectsContainer.getUser().getId().equals(userId)) {
				objectsContainer.getUser().setIs_following(isFollowed);
			}
		}

	}

	@Override
	public void onPostLikeChanged(String postId, boolean isLiked,
			int newLikeCount) {

		if (isAdded())
			for (DiscoverableObjectsContainer thisObjectsContainer : mPostObjects) {
				if (thisObjectsContainer.getType().equals(
						DiscoverableObjectsContainer.TYPE_POST)
						&& thisObjectsContainer.getPost().getId()
								.equals(postId)) {
					thisObjectsContainer.getPost().setIs_liked(isLiked);
					thisObjectsContainer.getPost().setLiked_count(newLikeCount);
					break;

				}
			}
	}

	public void removeCard() {

		for (int i = 0; i < mPagerAdapter.getArrayList().size(); i++)
			if (mPagerAdapter.getArrayList().get(i).getType()
					.equals(DiscoverableObjectsContainer.TYPE_UPLOAD))
				mPagerAdapter.getArrayList().remove(i);
		mPagerAdapter.isVideoUpdated = true;
		mPagerAdapter.notifyDataSetChanged();

	}
}
