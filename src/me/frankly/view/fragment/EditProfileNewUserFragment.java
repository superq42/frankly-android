package me.frankly.view.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UrlResolver;
import me.frankly.listener.CompressionCompletedListener;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.servicereceiver.ProfileUploadingService;
import me.frankly.util.FileUtils;
import me.frankly.util.FileUtils.FileType;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.util.Utilities;
import me.frankly.util.VideoCompressionTool;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.RecordAnswerActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class EditProfileNewUserFragment extends Fragment implements
		OnClickListener, RequestListener {

	private EditText edtUserName;
	private EditText edtUserInfo;
	private String userName;
	private String userBio;
	private CircularImageView cvProfileImg;
	private ImageView ivCoverPic;
	private UniversalUser mUser;
	private ImageView ivProfilePicOverlay;
	private Uri profilePicUri = null;
	private static final int REQUEST_CODE_PROFILE_PIC = 0;
	private static final int REQUEST_CODE_UPDATE_VIDEO = 2;
	private static final int REQUEST_CODE_CROP_IMAGE = 3;
	public boolean isDirty;
	private String video_path;
	private String thumb_path = null;
	private String profile_path;
	private EditableProfileObject editprofileObject;
	private TextView uploadVidTextView;
	private String name, bio;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userName = (String) getArguments().get(Constant.Key.KEY_USER_NAME);
		userBio = (String) getArguments().get(Constant.Key.KEY_USER_BIO);
		mUser = PrefUtils.getCurrentUserAsObject();
		name = mUser.getFull_name();
		bio = mUser.getBio();
		editprofileObject = new EditableProfileObject();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.edit_profile_new_user, container,
				false);

		edtUserInfo = (EditText) view
				.findViewById(R.id.frag_editproftxt_userinfo);
		edtUserName = (EditText) view
				.findViewById(R.id.frag_editproftxt_username);
		cvProfileImg = (CircularImageView) view
				.findViewById(R.id.edit_profile_img);
		ivCoverPic = (ImageView) view.findViewById(R.id.iv_cover_image);
		uploadVidTextView = (TextView) view
				.findViewById(R.id.profile_uploadvideo);
		ivProfilePicOverlay = (ImageView) view
				.findViewById(R.id.profile_overlay);
		edtUserInfo.setText(userBio);
		edtUserName.setText(userName);
		edtUserName.setFocusable(true);

		ImageView imgBack = (ImageView) view.findViewById(R.id.ab_bb__back_btn);
		imgBack.setOnClickListener(this);

		TextView tvSubmit = (TextView) view.findViewById(R.id.ab_bb_submit);
		tvSubmit.setVisibility(View.VISIBLE);
		tvSubmit.setOnClickListener(this);

		TextView tvTitle = (TextView) view.findViewById(R.id.ab_bb_title);
		tvTitle.setText(R.string.edit_details_new_user);
		cvProfileImg.setOnClickListener(this);
		if ((mUser.getProfile_videos() == null || mUser.getProfile_videos()
				.isEmpty()) && mUser.getProfile_video() == null) {
			uploadVidTextView.setText("+ Add Intro Movie");
			uploadVidTextView
					.setBackgroundResource(R.drawable.bg_rectangle_rednocorner_transparent);

		} else {
			uploadVidTextView.setText("Update Intro Movie");
			uploadVidTextView
					.setBackgroundResource(R.drawable.bg_rectangle_blackback_nocorner);

		}
		uploadVidTextView.setPadding(30, 20, 30, 20);
		uploadVidTextView.setOnClickListener(this);
		ivProfilePicOverlay.setOnClickListener(this);
		updateProfile();
		edtUserInfo.clearFocus();
		edtUserName.clearFocus();

		edtUserName.setFocusableInTouchMode(true);
		edtUserInfo.setFocusableInTouchMode(true);
		
		OnKeyListener okl = new OnKeyListener() {
			@Override
			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
				Log.d("onKey", "KeyCode : " + arg1 + ", " + arg2.getKeyCode());
				if (arg2.getAction() == KeyEvent.ACTION_DOWN
						&& arg2.getKeyCode() == KeyEvent.KEYCODE_BACK) {
					MyUtilities.hideKeyboard(edtUserName, getActivity());
					MyUtilities.hideKeyboard(edtUserInfo, getActivity());
					if (!edtUserInfo.getText().toString().equals(userBio)
							|| !edtUserName.getText().toString()
									.equals(userName)) {
						showWarningPopup();
						return true;
					}
				}
				return false;
			}
		};
		
		edtUserName.setOnKeyListener(okl);
		edtUserInfo.setOnKeyListener(okl);
		
		Controller.registerGAEvent("EditProfileTextFragment");
		return view;
	}

	private void updateProfile() {
		Log.d("img", "updatinf profile pic");
		String cover_picture = null;
		String profile_picture = null;

		if (thumb_path != null)
			cover_picture = thumb_path;
		else
			cover_picture = mUser.getCover_picture();

		profile_picture = getAbsoluteUrl(mUser.getProfile_picture());

		if (cover_picture != null) {
			ImageLoader.getInstance().loadImage(mUser.getCover_picture(),
					new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String arg0, View arg1) {
						}

						@Override
						public void onLoadingFailed(String arg0, View arg1,
								FailReason arg2) {
						}

						@Override
						public void onLoadingComplete(String arg0, View arg1,
								final Bitmap bitmap) {
							if (isAdded())
								getActivity().runOnUiThread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										Bitmap mBm = bitmap;
										mBm = MyUtilities.fastblur(bitmap, 10);
										ivCoverPic.setImageBitmap(mBm);
									}
								});

						}

						@Override
						public void onLoadingCancelled(String arg0, View arg1) {
						}
					});
		}
		if (profile_path != null) {
			profile_picture = getAbsoluteUrl(profile_path);
		}
		DisplayImageOptions option = CircularImageView
				.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
		ImageLoader.getInstance().displayImage(getAbsoluteUrl(profile_picture),
				cvProfileImg, option);

	}

	private String getAbsoluteUrl(String url) {
		if (url != null && !url.startsWith("file") && !url.startsWith("http"))
			return "file://" + url;
		return url;
	}

	@Override
	public void onClick(View v) {
		removeKeyboardForcefully(edtUserName);
		switch (v.getId()) {
		case R.id.ab_bb__back_btn:
			MyUtilities.hideKeyboard(edtUserName, getActivity());
			if (!edtUserInfo.getText().toString().equals(userBio)
					|| !edtUserName.getText().toString().equals(userName))
				showWarningPopup();
			else
				onBackresult();
			break;
		case R.id.ab_bb_submit:
			MyUtilities.hideKeyboard(edtUserName, getActivity());
			if (isInputValid() && Controller.isNetworkConnected(getActivity())) {
				UniversalUser user = PrefUtils.getCurrentUserAsObject();
				user.setFirst_name(edtUserName.getText().toString());
				user.setBio(edtUserInfo.getText().toString());
				PrefUtils.saveCurrentUserAsJson(JsonUtils.jsonify(user));
				uploadInfo();
				onBackresult();
			}
			break;
		case R.id.edit_profile_img:
		case R.id.profile_overlay:
			openImageIntent();
			break;
		case R.id.profile_uploadvideo:
			dispatchTakeVideoIntent();
			break;

		}
	}

	private void uploadInfo() {
		final EditableProfileObject profile = new EditableProfileObject();
		profile.setBio(edtUserInfo.getText().toString());
		profile.setFirst_name(edtUserName.getText().toString());
		Controller.requestUpdateProfile(getActivity(), profile, this);
	}

	private void onBackresult() {
		Intent intent = new Intent();
		intent.putExtra(Constant.Key.KEY_USER_NAME, edtUserName.getText()
				.toString());
		intent.putExtra("video_path", video_path);
		intent.putExtra("thumb_path", thumb_path);
		getTargetFragment().onActivityResult(getTargetRequestCode(),
				Activity.RESULT_OK, intent);
		getFragmentManager().popBackStack();
	}

	private boolean isInputValid() {
		int nameLength = edtUserName.getText().toString().trim().length();
		int bioLength = edtUserInfo.getText().toString().trim().length();
		boolean result = true;
		if (nameLength < 3) {
			Controller.showToast(getActivity(),
					getActivity().getString(R.string.name_length_error));
			result = false;
		} else if (nameLength > 30) {
			Controller.showToast(getActivity(),
					getActivity().getString(R.string.name_length_max_error));
			result = false;
		}
		if (bioLength > 140) {
			Controller.showToast(getActivity(),
					getActivity().getString(R.string.bio_length_max_error));
			result = false;
		}
		return result;
	}

	public void removeKeyboardForcefully(View et) {
		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
	}

	public void getKeyboardForcefully(final View et) {
		et.requestFocus();
		et.postDelayed(new Runnable() {
			public void run() {
				InputMethodManager keyboard = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(et, 10);
			}
		}, 50);
	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
	}

	@Override
	public void onRequestError(int errorCode, String message) {
	}

	private void openImageIntent() {
		File newImageFile = FileUtils.newFile(getActivity(),
				FileType.PROFILE_IMAGE, PrefUtils.getUserID());
		profilePicUri = Uri.fromFile(newImageFile);

		// Camera.
		final List<Intent> cameraIntents = new ArrayList<Intent>();
		final Intent captureIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		final PackageManager packageManager = getActivity().getPackageManager();
		final List<ResolveInfo> listCam = packageManager.queryIntentActivities(
				captureIntent, 0);
		for (ResolveInfo res : listCam) {
			final String packageName = res.activityInfo.packageName;
			final Intent intent = new Intent(captureIntent);
			intent.setComponent(new ComponentName(res.activityInfo.packageName,
					res.activityInfo.name));
			intent.setPackage(packageName);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, profilePicUri);
			cameraIntents.add(intent);
		}

		final Intent galleryIntent = new Intent();
		galleryIntent.setType("image/*");
		galleryIntent.setAction(Intent.ACTION_PICK);

		final Intent chooserIntent = Intent.createChooser(galleryIntent,
				"Select Source");

		// Add the camera options.
		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
				cameraIntents.toArray(new Parcelable[] {}));

		startActivityForResult(chooserIntent, REQUEST_CODE_PROFILE_PIC);
	}

	private void dispatchTakeVideoIntent() {
		Intent takeVideoIntent = new Intent(getActivity(),
				RecordAnswerActivity.class);
		Bundle args = new Bundle();
		args.putString(RecordAnswerActivity.KEY_EDIT_PROFILE, mUser.getId());
		takeVideoIntent.putExtras(args);
		startActivityForResult(takeVideoIntent, REQUEST_CODE_UPDATE_VIDEO);

	}

	private void openCropImageFragment() {

		ImageCropFragment fragment = new ImageCropFragment();
		Bundle bundle = new Bundle();

		bundle.putString(Constant.Key.CROP_IMAGE_URL,
				Utilities.getPath(profilePicUri, getActivity()));
		fragment.setArguments(bundle);
		fragment.setTargetFragment(this, REQUEST_CODE_CROP_IMAGE);

		getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.act_editprofile_frame_l, fragment)
				.addToBackStack(null).commit();

	}

	private void upLoadProfile() {
		Controller.removeCurrentUserFromCache();
		String profilepicpath = profile_path;
		if (profilepicpath == null)
			profilepicpath = Utilities.getPath(profilePicUri, getActivity());
		editprofileObject.setId(PrefUtils.getUserID());
		editprofileObject.setProfile_picture(profilepicpath);
		editprofileObject.setProfile_video(video_path);
		editprofileObject.setCover_picture(thumb_path);
		editprofileObject.setFirst_name(name);
		editprofileObject.setBio(bio);
		PrefUtils.setPendingEditProfileObject(editprofileObject);
		PrefUtils.setIsVideoSentPending(true);
		
		File compressedVideoFile = FileUtils.newFile(getActivity(),
				FileType.VIDEO_COMPRESSED, PrefUtils.getUserID());
		final String compressedVideoPath = compressedVideoFile
				.getAbsolutePath();

		if (Build.VERSION.SDK_INT >= 16 && video_path != null) {
			new VideoCompressionTool().processOpenVideo(video_path,
					compressedVideoPath, null, getActivity(),
					new CompressionCompletedListener() {

						@Override
						public void didWriteData(File file, Context context,
								boolean last, boolean error) {
							if (error) {
								FileUtils.delete(compressedVideoPath);
							} else {
								editprofileObject
										.setCompressedVideoUrl(compressedVideoPath);
							}
							editprofileObject.setIsCompressionValid(!error);
							PrefUtils
									.setPendingEditProfileObject(editprofileObject);
							PrefUtils.setIsVideoSentPending(true);
							Intent intent = new Intent(context,
									ProfileUploadingService.class);
							context.startService(intent);
						}
					}, false);

		} else if (video_path != null || profilePicUri != null) {
			Log.e("debug_201didwriteData", "uploadeing without compression ");
			editprofileObject.setIsCompressionValid(false);
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				Intent intent = new Intent(getActivity(),
						ProfileUploadingService.class);
				getActivity().startService(intent);
			}
		}
		UniversalUser currentUserAsObject = PrefUtils.getCurrentUserAsObject();
		if (video_path != null && thumb_path != null) {
			currentUserAsObject.setProfile_video(video_path);
			currentUserAsObject.setCover_picture(getAbsoluteUrl(thumb_path));
		}
		if (profilePicUri != null)
			currentUserAsObject
					.setProfile_picture(getAbsoluteUrl(profilepicpath));
		PrefUtils.saveCurrentUserAsJson(JsonUtils.jsonify(currentUserAsObject));
		// Controller.removeAllPagesInCache();
		Controller.removeCache(UrlResolver.EndPoints.PROFILE_GET_BY_ID + "/"
				+ PrefUtils.getUserID());

		// ((EditProfileActivity)
		// getActivity()).finishActivityWithResult(video_path, thumb_path);

	}

	@SuppressLint("NewApi")
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_PROFILE_PIC) {
			if (resultCode == Activity.RESULT_OK) {
				isDirty = true;
				if (data != null && data.getData() != null) {
					profilePicUri = data.getData();
					Log.d("img",
							"profilepic uri is " + profilePicUri.toString());
				}
				if (cvProfileImg != null) {
					Log.d("img",
							"profilepic uri is " + profilePicUri.toString());
					Drawable cvPicDrawable = null;
					try {
						InputStream is = getActivity().getContentResolver()
								.openInputStream(profilePicUri);
						cvPicDrawable = Drawable.createFromStream(is,
								profilePicUri.toString());
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (cvPicDrawable != null)
						cvProfileImg.setImageDrawable(cvPicDrawable);
				}
				openCropImageFragment();
			} else
				profilePicUri = null;

		} else if (requestCode == REQUEST_CODE_UPDATE_VIDEO) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					Log.d("save111", "asdsada");
					isDirty = true;
					video_path = data.getStringExtra("video_path");
					// video_path= getAbsoluteUrl(video_path);
					thumb_path = data.getStringExtra("thumb_path");
					Controller.showToast(getActivity(), "Video Uploading");
					Log.d("vidupload", video_path + thumb_path);
					uploadVidTextView.setText("Update Intro Movie");
					uploadVidTextView
							.setBackgroundResource(R.drawable.bg_rectangle_blackback_nocorner);
					uploadVidTextView.setPadding(30, 20, 30, 20);
					upLoadProfile();
					mUser = PrefUtils.getCurrentUserAsObject();
					updateProfile();
				}
			}
		} else if (requestCode == REQUEST_CODE_CROP_IMAGE) {
			Log.d("save11", "" + resultCode);
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					isDirty = true;
					Log.d("save", data.toString());
					profile_path = data
							.getStringExtra(Constant.Key.CROP_IMAGE_URL);
					profilePicUri = Uri.parse(profile_path);
					updateProfile();
					upLoadProfile();
				}
			}
		}

	}

	private void showWarningPopup() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		alertDialogBuilder.setTitle("Unsaved Changes");
		alertDialogBuilder
				.setMessage("Do you want to save your changes before exiting?");

		alertDialogBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						if (isInputValid()
								&& Controller.isNetworkConnected(getActivity())) {
							UniversalUser user = PrefUtils
									.getCurrentUserAsObject();
							user.setFirst_name(edtUserName.getText().toString());
							user.setBio(edtUserInfo.getText().toString());
							PrefUtils.saveCurrentUserAsJson(JsonUtils
									.jsonify(user));
							uploadInfo();
							onBackresult();
						}
					}
				});
		alertDialogBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						onBackresult();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		// show alert
		alertDialog.show();

	}
}
