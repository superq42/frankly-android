package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.JsonUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.BaseActivity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ProfileImageFragment extends Fragment implements OnClickListener {

	private String TAG = "ProfileFragment";
	private UniversalUser mUser;
	private ImageView imgCoverImage;
	private String mParentScreen;
	private ImageView imgFOllow;
	private ImageView ivFollow;
	private static final String FOLLOW = "FOLLOW";
	private static final String FOLLOWING = "FOLLOWING";
	private View askView;
	private CircularImageView cvProfilepic;
	private boolean isInitFollowing;
	private TextView tvFollow;
	private BaseActivity mBaseActivity;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// setting up screen and user parameters
		if (getActivity() instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) getActivity();
		}
		if (getArguments() != null) {
			mUser = (UniversalUser) JsonUtils.objectify(getArguments()
					.getString(Constant.Key.KEY_USER_OBJECT),
					UniversalUser.class);
			mParentScreen = getArguments().getString(
					Constant.Key.KEY_PARENT_SCREEN_NAME);
		}
	}

	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_profile_image,
				container, false);
		imgCoverImage = (ImageView) view
				.findViewById(R.id.frag_profile_coverpic);
		cvProfilepic = (CircularImageView) view
				.findViewById(R.id.frag_profile_img);
		TextView tv_Name = (TextView) view.findViewById(R.id.frag_proilfe_name);
		TextView tv_Bio = (TextView) view.findViewById(R.id.frag_proilfe_info);
		TextView tvRank = (TextView) view.findViewById(R.id.frag_proilfe_rank);
		TextView tvAnsCount = (TextView) view
				.findViewById(R.id.infobar_answercount);
		TextView tvViews = (TextView) view.findViewById(R.id.infobar_view);
		tvFollow = (TextView) view.findViewById(R.id.infobar_followers);

		Log.d("PIF", "onCreateView Called");

		// setting values of views from user object

		tv_Bio.setText(mUser.getBio());
		tv_Name.setText(mUser.getFull_name());
		if (mUser.getUser_title() == null || mUser.getUser_title().isEmpty()) {
			tvRank.setVisibility(View.GONE);
		} else
			tvRank.setText(mUser.getUser_title());
		askView = view.findViewById(R.id.frag_profile_ask_btn);
		tvAnsCount.setText(String.valueOf(mUser.getAnswer_count()));
		tvViews.setText(String.valueOf(mUser.getView_count()));
		tvFollow.setText(String.valueOf(mUser.getFollower_count()));
		cvProfilepic.setOnClickListener(this);
		// setting up follow and ask buttons

		setFollowAskView(view);

		// fetching and setting cover image and profile image from url
		cvProfilepic.setOnClickListener(this);
		setCoverImage();
		Controller.registerGAEvent("ProfileImageFragment");
		return view;
	}

	private void setCoverImage() {
		if (mUser.getCover_picture() != null) {
			ImageLoader.getInstance().displayImage(mUser.getCover_picture(),
					imgCoverImage);
		}
		if (mUser.getProfile_picture() != null) {
			DisplayImageOptions options = CircularImageView
					.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
			cvProfilepic.setImageUrl(options, mUser.getProfile_picture());
		}
	}

	private void setFollowAskView(View rootView) {
		imgFOllow = (ImageView) rootView
				.findViewById(R.id.frag_profile_btn_follow);
		ivFollow = (ImageView) rootView
				.findViewById(R.id.frag_profile_img_plus_profile);
		askView.setVisibility(View.VISIBLE);
		askView.setOnClickListener(this);
		if (mUser.is_following()) {
			isInitFollowing = true;
			imgFOllow.setTag(FOLLOWING);
			imgFOllow.setBackgroundResource(R.drawable.btn_following_profile);
			ivFollow.setTag(FOLLOWING);
			cvProfilepic.setTag(FOLLOWING);
			ivFollow.setImageResource(R.drawable.ic_followed);
		} else {
			isInitFollowing = false;
			imgFOllow.setTag(FOLLOW);
			imgFOllow.setBackgroundResource(R.drawable.btn_follow_profile);
			ivFollow.setTag(FOLLOW);
			cvProfilepic.setTag(FOLLOW);
			ivFollow.setImageResource(R.drawable.ic_add_profilepic);
		}
		imgFOllow.setOnClickListener(this);
		ivFollow.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.frag_profile_feed_ask_btn:
		case R.id.frag_profile_ask_btn:
			mBaseActivity.askActivity(mUser);
			break;
		case R.id.frag_profile_btn_follow:
		case R.id.frag_profile_img_plus_profile:
		case R.id.frag_profile_img:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				if (v.getTag().equals(FOLLOW)) {
					followUser();
					imgFOllow.setTag(FOLLOWING);
					imgFOllow
							.setBackgroundResource(R.drawable.btn_following_profile);
					cvProfilepic.setTag(FOLLOWING);
					ivFollow.setTag(FOLLOWING);
					cvProfilepic.setTag(FOLLOWING);
					ivFollow.setImageResource(R.drawable.ic_followed);
					if (isInitFollowing)
						tvFollow.setText(String.valueOf(mUser
								.getFollower_count()));
					else
						tvFollow.setText(String.valueOf(mUser
								.getFollower_count() + 1));
				} else if (v.getTag().equals(FOLLOWING)) {
					unFollowUser();
					imgFOllow.setTag(FOLLOW);
					imgFOllow
							.setBackgroundResource(R.drawable.btn_follow_profile);
					cvProfilepic.setTag(FOLLOW);
					ivFollow.setTag(FOLLOW);
					cvProfilepic.setTag(FOLLOW);
					ivFollow.setImageResource(R.drawable.ic_add_profilepic);
					if (isInitFollowing)
						tvFollow.setText(String.valueOf(mUser
								.getFollower_count() - 1));
					else
						tvFollow.setText(String.valueOf(mUser
								.getFollower_count()));
				}
			}
			break;
		case R.id.frag_proilfe_name:
		case R.id.frag_proilfe_rank:
			if (mParentScreen.equals(Constant.ScreenSpecs.SCREEN_CHANNEL)) {
				if (getActivity() instanceof BaseActivity) {
					((BaseActivity) getActivity()).profileActivity(
							mUser.getId(), mUser);
				} else {
					Log.e(TAG,
							"parent activity not an instance of BaseActivity "
									+ getActivity().getClass().getSimpleName());
				}

			}
			break;
		}
	}

	private void followUser() {
		String userName = mUser.getId();
		Controller.requestfollow(getActivity(), userName,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode,
							final String message) {
						// resetFollowButton();

					}

					@Override
					public void onRequestCompleted(Object responseObject) {
					}
				});
	}

	private void unFollowUser() {
		String userId = mUser.getId();
		Controller.requestUnfollow(getActivity(), userId,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode, String message) {
						Log.i(TAG, "onRequesterror " + message);
						// setFollowButton();
					}

					@Override
					public void onRequestCompleted(Object responseObject) {
					}
				});
	}
}
