package me.frankly.view.fragment;

import android.os.CountDownTimer;
import android.util.Log;
import android.widget.ProgressBar;

public class MyCountDownTimer extends CountDownTimer {

	public static int timeFinished = 0;
	// private RecordAnsFragment context;
	private StopRecordingListener stopRecordingListener;
	private ProgressBar pbVideoProgress;

	public MyCountDownTimer(long millisInFuture, long countDownInterval, ProgressBar pbVideoProgress) {
		super(millisInFuture, countDownInterval);
		// this.context = recordAnsFragmentNew;
		this.pbVideoProgress = pbVideoProgress;
	}

	public void setStopRecordingListener(StopRecordingListener listener) {
		stopRecordingListener = listener;
	}

	@Override
	public void onFinish() {
		Log.i("TAGCAM", "time khatam bhai Press done button");
		pbVideoProgress.setProgress(/* 90000 */RecordAnswerFragment.MAX_VIDEO_LENGTH);
		stopRecordingListener.onTimeFinished();

	}

	@Override
	public void onTick(long millisUntilFinished) {
		//Log.i("TAGCAM", "ticking=" + millisUntilFinished);
		/*
		 * if (millisUntilFinished % 1000 != 0) { millisUntilFinished =
		 * millisUntilFinished + (1000 - (millisUntilFinished % 1000)); }
		 */
		timeFinished = (int) (/* 90000 */RecordAnswerFragment.MAX_VIDEO_LENGTH - (millisUntilFinished));
		// Log.i("TAGCAM" + " onTick()", "TimeLeft in mills: " +
		// millisUntilFinished);
		// Log.i("TAGCAM" + " onTick()", "TimeLeft: " +
		// MyCountDownTimer.timeFinished);
		//Log.i("TAGCAM", "t time finished" + timeFinished);
		pbVideoProgress.setProgress(timeFinished);

		// context.timerTextView.setText(String.valueOf(millisUntilFinished /
		// 1000));
	}

	public static int getTimeFinished() {
		return timeFinished;
	}

	public static void setTimeFinished(int timeFinished) {
		MyCountDownTimer.timeFinished = timeFinished;
	}
}
