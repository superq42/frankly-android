package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.RequestListener;
import me.frankly.listener.SwipeAnimationDoneListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.util.ImageUtil;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.ProfileActivity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.nhaarman.supertooltips.ToolTipView.OnToolTipViewClickedListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ProfileVideoFragment extends SDKVideoFragment implements
		OnToolTipViewClickedListener, UserFollowListener,
		SwipeAnimationDoneListener {

	private String TAG = "ProfileVideoFragment";
	private UniversalUser mUser;
	private ImageView imgCoverImage;
	private TextureView mTextureView;
	private View askImageView;
	private View pauseView;
	private ImageView imgFollow;
	private ImageView ivFollow;

	private TextView tv_tap_play, tvFollow;
	private String mParentScreen;
	private ImageView loadingView;
	private AnimationDrawable loaderAnimation;
	private Context mContext;
	private ToolTipRelativeLayout toolTipRelativeLayout;
	private ToolTipView followToolTipView, askToolTipView;

	private CircularImageView cvProfilepic;
	private boolean isLoaderAnimating = false;
	private View playView;
	private boolean isInitFollowing;
	private BaseActivity mBaseActivity;
	private boolean shouldShowTapToPlay = true; 
	
	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#Ninja PVF";

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + " onCreateView", "Called");
		Log.e(TAG, "onCreatView" + System.currentTimeMillis());
		View view = inflater.inflate(R.layout.fragment_profile_video,
				container, false);
		if (getActivity() instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) getActivity();
		}
		// setting up screen and user parameters

		if (getArguments() != null) {
			mUser = (UniversalUser) JsonUtils.objectify(getArguments()
					.getString(Constant.Key.KEY_USER_OBJECT),
					UniversalUser.class);
			mParentScreen = getArguments().getString(
					Constant.Key.KEY_PARENT_SCREEN_NAME);
		}
		mContext = getActivity();
		toolTipRelativeLayout = (ToolTipRelativeLayout) view
				.findViewById(R.id.activity_main_tooltipRelativeLayout);
		loadingView = (ImageView) view.findViewById(R.id.iv_loader);
		loadingView.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) loadingView.getDrawable();
		pauseView = view.findViewById(R.id.frag_profile_pauseview);
		playView = view.findViewById(R.id.iv_play);
		ivFollow = (ImageView) view
				.findViewById(R.id.frag_profile_img_plus_profile);
		imgCoverImage = (ImageView) view
				.findViewById(R.id.frag_profile_coverpic);
		cvProfilepic = (CircularImageView) view
				.findViewById(R.id.frag_profile_img);
		askImageView = view.findViewById(R.id.frag_profile_ask_btn);
		cvProfilepic.setOnClickListener(this);

		// setting follow and ask buttons
		setFollowAskView(view);
		TextView tv_Name = (TextView) view.findViewById(R.id.frag_proilfe_name);
		TextView tv_Bio = (TextView) view.findViewById(R.id.frag_proilfe_info);
		TextView tvRank = (TextView) view.findViewById(R.id.frag_proilfe_rank);
		fileDebugLog(FILE_DEBUG_TAG + " tvAnsCount", "initialized");
		TextView tvAnsCount = (TextView) view
				.findViewById(R.id.infobar_answercount);
		tvAnsCount.setOnClickListener(this);
		TextView tvViews = (TextView) view.findViewById(R.id.infobar_view);
		tvFollow = (TextView) view.findViewById(R.id.infobar_followers);

		// initializing views with values from mUser object
		tv_Bio.setText(mUser.getBio());
		tv_Name.setText(mUser.getFull_name());
		if (mUser.getUser_title() == null || mUser.getUser_title().isEmpty())
			tvRank.setVisibility(View.GONE);
		else
			tvRank.setText(mUser.getUser_title());
		tvAnsCount.setText(String.valueOf(mUser.getAnswer_count()));
		tvViews.setText(String.valueOf(mUser.getView_count()));
		tvFollow.setText(String.valueOf(mUser.getFollower_count()));
		mTextureView = (TextureView) view
				.findViewById(R.id.atomic_post_textureview);

		// fetching and setting cover image and profile image from url
		showCoverPhoto();

		if (!PrefUtils.getIsFirstProfileSwipeAnimShown()
				&& mContext instanceof ProfileActivity)
			((ProfileActivity) mContext).addSwipeAnimationDoneListener(this);
		// setting url,textureSurface,play,pause,post-play view for
		// SDKVideoFragment
		setVideoViews();

		LinearLayout bottom_bar = (LinearLayout) view
				.findViewById(R.id.frag_bottom_bar);
		bottom_bar.setVisibility(View.VISIBLE);
		Controller.registerGAEvent("ProfileVideoFragment");
		tv_tap_play = (TextView) view.findViewById(R.id.tv_tap_text);
		tv_tap_play.setVisibility(View.GONE);
		Log.i(TAG + " anim", "oncreateview");
		cvProfilepic.setOnClickListener(this);
		// Walkthrough animations

		// showFollowToolTip();
		// showAskToolTip();
		UniversalActionListenerCollection.getInstance().addUserFollowListener(
				this, mUser.getId());
		return view;
	}

	private void showFollowToolTip() {
		fileDebugLog(FILE_DEBUG_TAG + " showFollowToolTip", "Called");

		if (!mUser.is_following())
			if (PrefUtils.getIsFirstProfileSwipeAnimShown()
					&& !PrefUtils.getIsFirstFollowProfileAnimShown())
				addFollowToolTip();
	}

	private void addFollowToolTip() {
		fileDebugLog(FILE_DEBUG_TAG + " addFollowToolTip", "Called");

		if (followToolTipView != null)
			followToolTipView.remove();
		followToolTipView = Controller.addToolTipView(toolTipRelativeLayout,
				imgFollow, mContext, "Tap to follow", false);
		pauseView.findViewById(R.id.iv_overlay).setVisibility(View.VISIBLE);
	}

	private void showAskToolTip() {
		fileDebugLog(FILE_DEBUG_TAG + " showAskToolTip", "Called");

		if (PrefUtils.getIsFirstProfileSwipeAnimShown()
				&& PrefUtils.getIsFirstFollowProfileAnimShown()
				&& !PrefUtils.getIsFirstAskProfileAnimShown()
				&& (mUser.is_following()))
			addAskToolTip();
	}

	private void addAskToolTip() {
		if (askToolTipView != null)
			askToolTipView.remove();
		askToolTipView = Controller.addToolTipView(toolTipRelativeLayout,
				askImageView, mContext, "Tap to Ask", false);
		pauseView.findViewById(R.id.iv_overlay).setVisibility(View.VISIBLE);
	}

	private void showCoverPhoto() {
		if (mUser.getCover_picture() != null) {
			ImageLoader.getInstance().displayImage(mUser.getCover_picture(),
					imgCoverImage);
		}
		if (mUser.getProfile_picture() != null) {
			DisplayImageOptions options = CircularImageView
					.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
			cvProfilepic.setImageUrl(options, mUser.getProfile_picture());
		}
	}

	private void setVideoViews() {
		if (mUser.getProfile_video() != null) {
			setAutoPlay(false);
			String video_url = ImageUtil.getNetworkBasedVideoUrl(mUser
					.getProfile_videos());
			if (video_url == null)
				video_url = mUser.getProfile_video();
			setVideoUrl(video_url);
			setInitialView(pauseView);
			setPauseView(pauseView);
			setPostPlayView(pauseView);
			setVideoTextureView(mTextureView);
			mTextureView.setOnClickListener(this);
			pauseView.setOnClickListener(this);
		} else {
			mTextureView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		fileDebugLog(FILE_DEBUG_TAG + " onCLick", "Called");

		switch (v.getId()) {
		case R.id.frag_profile_feed_ask_btn:
		case R.id.frag_profile_ask_btn:
			if (askToolTipView != null) {
				askToolTipView.remove();
				PrefUtils.setIsFirstAskProfileAnimShown(true);
				getPauseView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
				getInitialView().findViewById(R.id.iv_overlay).setVisibility(
						View.GONE);
			}
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				mBaseActivity.askActivity( mUser);
				registerMixpanelEvent(MixpanelUtils.ASK_ME, 0, "");
			}
			break;
		case R.id.frag_profile_btn_follow:
		case R.id.frag_profile_img_plus_profile:
		case R.id.frag_profile_img:
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				if (followToolTipView != null) {
					followToolTipView.remove();
					PrefUtils.setIsFirstFollowProfileAnimShown(true);
					getPauseView().findViewById(R.id.iv_overlay).setVisibility(
							View.GONE);
					getPostPlayView().findViewById(R.id.iv_overlay)
							.setVisibility(View.GONE);
					getInitialView().findViewById(R.id.iv_overlay)
							.setVisibility(View.GONE);
					if (!isLoaderAnimating)
						showAskToolTip();
				}
				if (!mUser.is_following()) {
					followUser();
					imgFollow
							.setBackgroundResource(R.drawable.btn_following_profile);
					ivFollow.setImageResource(R.drawable.ic_followed);
					if (isInitFollowing)
						tvFollow.setText(String.valueOf(mUser
								.getFollower_count()));
					else
						tvFollow.setText(String.valueOf(mUser
								.getFollower_count() + 1));
					registerMixpanelEvent(MixpanelUtils.USER_FOLLOWED, 0, "");

				} else {
					unFollowUser();
					imgFollow
							.setBackgroundResource(R.drawable.btn_follow_profile);
					ivFollow.setImageResource(R.drawable.ic_add_profilepic);
					if (isInitFollowing)
						tvFollow.setText(String.valueOf(mUser
								.getFollower_count() - 1));
					else
						tvFollow.setText(String.valueOf(mUser
								.getFollower_count()));
					registerMixpanelEvent(MixpanelUtils.USER_UNFOLLOWED, 0, "");
				}
				UniversalActionListenerCollection.getInstance()
						.onUserFollowChanged(mUser.getId(),
								mUser.is_following());
			}
			break;
		case R.id.frag_profile_pauseview:
			if (askToolTipView != null)
				askToolTipView.remove();
			tv_tap_play.setVisibility(View.GONE);
			togglePlayState();
			if (getActivity() instanceof ProfileActivity) {
				((ProfileActivity) getActivity()).stopSwipeTimer();
			}
			break;
		case R.id.rl_img_text:
			break;
		case R.id.infobar_answercount:
			if (mParentScreen.equals(Constant.ScreenSpecs.SCREEN_PROFILE))
				((ProfileActivity) getActivity()).showTimeline();
			break;
		}
		super.onClick(v);
	}

	private void setFollowAskView(View rootView) {
		imgFollow = (ImageView) rootView
				.findViewById(R.id.frag_profile_btn_follow);
		ivFollow = (ImageView) rootView
				.findViewById(R.id.frag_profile_img_plus_profile);
		askImageView.setVisibility(View.VISIBLE);
		askImageView.setOnClickListener(this);
		if (mUser.is_following()) {
			imgFollow.setBackgroundResource(R.drawable.btn_following_profile);

			ivFollow.setImageResource(R.drawable.ic_followed);
			isInitFollowing = true;

		} else {
			imgFollow.setBackgroundResource(R.drawable.btn_follow_profile);
			ivFollow.setImageResource(R.drawable.ic_add_profilepic);
			isInitFollowing = false;
		}
		imgFollow.setOnClickListener(this);
		ivFollow.setOnClickListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		fileDebugLog(FILE_DEBUG_TAG + " onResume", "Called");

		// resetting all views needed by SDKVideoFragment in onResume()
		setVideoViews();

		showFollowToolTip();
		showAskToolTip();
	}

	private void followUser() {
		fileDebugLog(FILE_DEBUG_TAG + " followUser", "Called");

		String userName = mUser.getId();
		mUser.setIs_following(true);
		Controller.requestfollow(getActivity(), userName,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode,
							final String message) {
					}

					@Override
					public void onRequestCompleted(Object responseObject) {
					}
				});
	}

	private void unFollowUser() {
		fileDebugLog(FILE_DEBUG_TAG + " unFollowUser", "Called");

		String userId = mUser.getId();
		mUser.setIs_following(false);
		Controller.requestUnfollow(getActivity(), userId,
				new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode, String message) {
						Log.i(TAG + " sumit", "onRequesterror " + message);
					}

					@Override
					public void onRequestCompleted(Object responseObject) {
					}
				});
	}

	// called by SDKVideoFragment when video is buffering or has buffered to
	// toggle loading animation
	@Override
	protected View getLoadingView(boolean show) {
		fileDebugLog(FILE_DEBUG_TAG + " getLoadingView", "Called");

		if (loaderAnimation != null) {
			if (!show && isLoaderAnimating) {
				loaderAnimation.stop();
				isLoaderAnimating = false;
			} else if (show && !isLoaderAnimating) {
				loaderAnimation.start();
				isLoaderAnimating = true;
			}
		}
		return loadingView;
	}

	// called by SDKVideoFragment when player starts playing video to hide all
	// views
	@Override
	protected void hideAllViewsExceptCoverImage() {
		fileDebugLog(FILE_DEBUG_TAG + " hideAllViewsExceptCoverImage", "Called");

		playView.setVisibility(View.GONE);
		if (!PrefUtils.getIsFirstTapAnimShown()
				&& mParentScreen
						.equalsIgnoreCase(Constant.ScreenSpecs.SCREEN_DISCOVER)) {
			tv_tap_play.setVisibility(View.GONE);
			PrefUtils.setIsFirstTapAnimShown(true);
		}
		if (followToolTipView != null) {
			followToolTipView.remove();
			getPauseView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getInitialView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
		}

		if (askToolTipView != null) {
			askToolTipView.remove();
			getPauseView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getInitialView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
		}
		// getLoadingView(false);
	}

	@Override
	public View getDefaultCoverImage() {
		fileDebugLog(FILE_DEBUG_TAG + " getDefaultCoverImage", "Called");

		return imgCoverImage;
	}

	@Override
	protected String[] getUserDetails() {
		fileDebugLog(FILE_DEBUG_TAG + " getUserDetails", "Called");

		return new String[] { mUser.getUsername(),
				String.valueOf(mUser.getUser_type()) };

	}

	@Override
	public void onPlayerResumed() {
		fileDebugLog(FILE_DEBUG_TAG + " onPlayerResumed", "Called");

		super.onPlayerResumed();
		if (followToolTipView != null) {
			followToolTipView.remove();
			getPauseView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getInitialView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
		}

		else if (askToolTipView != null) {
			askToolTipView.remove();
			getPauseView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getPostPlayView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
			getInitialView().findViewById(R.id.iv_overlay).setVisibility(
					View.GONE);
		}
	}

	@Override
	public void onPlayerPaused() {
		fileDebugLog(FILE_DEBUG_TAG + " onPlayerPaused", "Called");

		super.onPlayerPaused();
		showFollowToolTip();
		showAskToolTip();
	}

	@Override
	public void onPlayerCompleted() {
		fileDebugLog(FILE_DEBUG_TAG + " onPlayerCompleted", "Called");

		super.onPlayerCompleted();
		showFollowToolTip();
		showAskToolTip();
	}

	public void registerMixpanelEvent(String event_name, int share_type,
			String profile_role) {
		fileDebugLog(FILE_DEBUG_TAG + " registerMixpanelEvent", "Called");

		if (isAdded())
			MixpanelUtils.sendGenericEvent(mUser.getId(), mUser.getUsername(),
					String.valueOf(mUser.getUser_type()),
					Constant.ScreenSpecs.SCREEN_PROFILE, event_name,
					getActivity());
	}

	@Override
	public void onToolTipViewClicked(ToolTipView toolTipView) {
		fileDebugLog(FILE_DEBUG_TAG + " onToolTipViewClicked", "Called");

		toolTipView.remove();
		getPauseView().findViewById(R.id.iv_overlay).setVisibility(View.GONE);
		getInitialView().findViewById(R.id.iv_overlay).setVisibility(View.GONE);
		getPostPlayView().findViewById(R.id.iv_overlay)
				.setVisibility(View.GONE);
	}

	private void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}

	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		if (mUser.getId().equals(userId)) {
			mUser.setIs_following(isFollowed);
			if (isFollowed) {
				imgFollow
						.setBackgroundResource(R.drawable.btn_following_profile);
				ivFollow.setImageResource(R.drawable.ic_followed);
			} else {
				imgFollow.setBackgroundResource(R.drawable.btn_follow_profile);
				ivFollow.setImageResource(R.drawable.ic_add_profilepic);
			}
		}

	}

	@Override
	public void onDestroy() {
		UniversalActionListenerCollection.getInstance()
				.removeUserFollowListener(this, mUser.getId());
		super.onDestroy();
	}

	@Override
	public void onSwipeDone() {
		shouldShowTapToPlay = false;
		showFollowToolTip();
		showAskToolTip();

	}

	@Override
	public void onSwipeStarted() {
		tv_tap_play.clearAnimation();
		tv_tap_play.setVisibility(View.GONE);
	}

}
