package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.SDKPlayer.IPlayer;
import me.frankly.SDKPlayer.IPlayer.IPlayerStateChangedListener;
import me.frankly.SDKPlayer.IPlayerFactory;
import me.frankly.adapter.FeedsPagerAdapter;
import me.frankly.config.Constant;
import me.frankly.config.VideoplayInteruptionBroadcastReciever;
import me.frankly.listener.ExternalInterruptListener;
import me.frankly.util.ImageUtil;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public abstract class SDKVideoFragment extends Fragment implements
		IPlayerStateChangedListener, OnClickListener, ExternalInterruptListener {

	private TextureView mVidTextureView;
	private String mVideoUrl, mParentScreen;
	private IPlayer mPlayer;
	private boolean isAutoPlay = false;
	private boolean isPlayActive = false, isPlaying = false;
	private View initialView, pauseView, postPlayView;
	@SuppressWarnings("unused")
	private FeedsPagerAdapter mParentAdapter;
	@SuppressWarnings("unused")
	private int mPos;
	private Animation initFadeAnimation;
	private long initTime;
	protected IPlayerStateChangedListener mParentPlayerStateChangedListener;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null
				&& getArguments().containsKey(
						Constant.Key.KEY_PARENT_SCREEN_NAME))
			mParentScreen = getArguments().getString(
					Constant.Key.KEY_PARENT_SCREEN_NAME);
		initFadeAnimation = new AlphaAnimation(1, 0);
		initFadeAnimation.setInterpolator(new AccelerateInterpolator()); // and
																			// this
		initFadeAnimation.setDuration(100);
		initFadeAnimation.setAnimationListener(fadeAnimListener);
		isAutoPlay = false;
		isPlaying = false;
		isPlayActive = false;

	}

	public View getInitialView() {
		return initialView;
	}

	/**
	 * Use this method to set View before play started
	 * 
	 * @param initialView
	 */
	public void setInitialView(View initialView) {
		this.initialView = initialView;
	}

	public View getPauseView() {
		return pauseView;
	}

	/**
	 * Use this method to set View when video is paused
	 * 
	 * @param pauseView
	 */
	public void setPauseView(View pauseView) {
		this.pauseView = pauseView;
	}

	public View getPostPlayView() {
		return postPlayView;
	}

	/**
	 * Use this method to set View when Video completed
	 * 
	 * @param postPlayView
	 */
	public void setPostPlayView(View postPlayView) {
		this.postPlayView = postPlayView;
	}

	public TextureView getmVidTextureView() {
		return mVidTextureView;
	}

	private void initializePlayer() {
		if (mPlayer != null)
			mPlayer.resetSelf();
		mPlayer = IPlayerFactory.getInstance().newPlayer();
		isAutoPlay = false;
		isPlaying = false;
		isPlayActive = false;
	}

	public boolean isPlayActive() {
		return isPlayActive;
	}

	public void setPlayActive(boolean isPlayActive) {
		this.isPlayActive = isPlayActive;
	}

	/**
	 * initialise the SurfaceView using this method in OnCreateView of your
	 * fragment.
	 * 
	 * @param mVidSurfaceView
	 */
	public void setVideoTextureView(TextureView mVidTextureView) {
		this.mVidTextureView = mVidTextureView;
		mVidTextureView.setOnClickListener(this);
	}

	public String getmVideoUrl() {
		return mVideoUrl;
	}

	/**
	 * set Video url using this method
	 * 
	 * @param mVideoUrl
	 */
	public void setVideoUrl(String mVideoUrl) {
		Log.d("check", "setting url : " + mVideoUrl
				+ " to player with parent: " + mParentScreen);
		initializePlayer();
		this.mVideoUrl = mVideoUrl;

	}

	public boolean isAutoPlay() {
		return isAutoPlay;
	}

	/**
	 * Set true if start playing when prepared. Else false
	 * 
	 * @param isAutoPlay
	 */
	public void setAutoPlay(boolean isAutoPlay) {
		this.isAutoPlay = isAutoPlay;

	}

	/**
	 * returns true if player is playing else if paused returns false. Do not
	 * get confused between isPlaying and isPlayerActive.
	 * 
	 * @return
	 */
	public boolean isPlaying() {
		return isPlaying;
	}

	/**
	 * Use method to start playing if isAutoPlay is False
	 */
	public void playVideo() {
		Log.d("checks", "playing url : " + mVideoUrl
				+ " to player with parent: " + mParentScreen);
		initTime = System.currentTimeMillis();
		Log.d("test", "vid init time: " + initTime);
		mPlayer.prepareUrl(getActivity(), mVideoUrl, mVidTextureView,
				new Handler(getActivity().getMainLooper()), this, true, false);
		if (!mPlayer.isPrepared())
			getLoadingView(true).setVisibility(View.VISIBLE);
		mPlayer.setMaster(true);
		isPlaying = false;
		isPlayActive = true;
		mPlayer.setMaster(true);
		VideoplayInteruptionBroadcastReciever
				.setExternalInterruptListener(this);

		hideAllViewsExceptCoverImage();

	}

	private void hideAllViews() {
		if (initialView != null && initialView.getVisibility() == View.VISIBLE) {
			initialView.setVisibility(View.GONE);
		}
		if (pauseView != null && pauseView.getVisibility() == View.VISIBLE) {
			pauseView.setVisibility(View.GONE);
		}
		if (postPlayView != null
				&& postPlayView.getVisibility() == View.VISIBLE) {
			postPlayView.setVisibility(View.GONE);
		}

	}

	/**
	 * new alternate method for hiding views
	 */
	protected abstract void hideAllViewsExceptCoverImage();

	/***
	 * Use this method to stop video when screen going out of focus or else
	 * wise.
	 */
	public void stopVideo() {

		Log.d("check", "stopping url : " + mVideoUrl
				+ " to player with parent: " + mParentScreen);
		try {
			if (mPlayer != null) {
				mPlayer.setMaster(false);
				Log.d("check", "stop cmd reached at player for url: "
						+ mVideoUrl);
				if (isPlayActive) {

					mPlayer.stopVideo();
				}
			}
		} catch (Exception e) {
		}
		onPlayerCompleted();

		isPlayActive = false;
		isPlaying = false;
		setVideoUrl(mVideoUrl);
		getLoadingView(false).setVisibility(View.GONE);
		initialView.setVisibility(View.VISIBLE);
		Log.d("video", "showing cover image");
		if (getDefaultCoverImage() != null)
			getDefaultCoverImage().setVisibility(View.VISIBLE);

	}

	@Override
	public void onPlayerPrepared() {
		Log.d("check", "player prepared url: " + mVideoUrl);
		ImageUtil.setVideoBandWidth(initTime, System.currentTimeMillis());
		Log.d("test", "prepared time: " + System.currentTimeMillis()
				+ " latency: " + (System.currentTimeMillis() - initTime));
		if (mParentPlayerStateChangedListener != null)
			mParentPlayerStateChangedListener.onPlayerPrepared();
		startInitFadeAnim();
		hideAllViews();
		getLoadingView(false).setVisibility(View.GONE);
		isPlayActive = true;
		isPlaying = true;
		PrefUtils.setVideoPlayCount(mVideoUrl);
		Log.d("video play", "" + PrefUtils.getVideoPlayCount());
	}

	@Override
	public void onPlayerPaused() {
	}

	@Override
	public void onPlayerResumed() {
		Log.i("SdkVideo", "onPLayerResumed");
		startInitFadeAnim();
		hideAllViews();
	}

	@Override
	public void onPlayerPausedToBuffer() {
		getLoadingView(true).setVisibility(View.VISIBLE);
	}

	@Override
	public void onPlayerResumedAfterBuffer() {
		getLoadingView(false).setVisibility(View.GONE);

	}

	@Override
	public void onPlayerCompleted() {
		Log.d("test", "finish time: " + System.currentTimeMillis());
		if (postPlayView != null)
			postPlayView.setVisibility(View.VISIBLE);

		isPlaying = false;

		if (mParentPlayerStateChangedListener != null)
			mParentPlayerStateChangedListener.onPlayerCompleted();

		registerMixpanelEvent(MixpanelUtils.VIDEO_COMPLETED, 0, "");
		VideoplayInteruptionBroadcastReciever
				.removeExternalInterruptListener(this);

	}

	@Override
	public void onPlayerError() {
		getLoadingView(false).setVisibility(View.GONE);
		initializePlayer();
	}

	private void startInitFadeAnim() {
		if (initialView != null && initialView.getVisibility() == View.VISIBLE) {
			initialView.startAnimation(initFadeAnimation);
		}
		if (pauseView != null && pauseView.getVisibility() == View.VISIBLE) {
			pauseView.startAnimation(initFadeAnimation);
		}
		if (postPlayView != null
				&& postPlayView.getVisibility() == View.VISIBLE) {
			postPlayView.startAnimation(initFadeAnimation);
		}

	}

	AnimationListener fadeAnimListener = new AnimationListener() {

		@Override
		public void onAnimationStart(Animation animation) {
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		@Override
		public void onAnimationEnd(Animation animation) {

			if (getDefaultCoverImage() != null)
				getDefaultCoverImage().getHandler().postDelayed(new Runnable() {

					@Override
					public void run() {
						getDefaultCoverImage().setVisibility(View.GONE);

					}
				}, 300);

		}
	};

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.atomic_post_textureview:
			togglePlayState();
			break;

		}
	}

	/**
	 * use this method to pause/resume video
	 */
	protected void togglePlayState() {
		Log.d("check",
				"toggle play state called isPlayActive: " + isPlayActive
						+ " isplaying: " + isPlaying + " player prepared: "
						+ mPlayer.isPrepared());
		if (!isPlayActive) {
			playVideo();

			registerMixpanelEvent(MixpanelUtils.VIDEO_PLAYED, 0, "");
		} else if (mPlayer.isPrepared()) {
			if (isPlaying) {
				pauseVideo();
				registerMixpanelEvent(MixpanelUtils.VIDEO_PAUSED, 0, "");
			} else {
				resumeVideo();
				registerMixpanelEvent(MixpanelUtils.VIDEO_RESUMED, 0, "");
			}
		} else {
			hideAllViewsExceptCoverImage();
			if (mPlayer.isAutoPlay())
				setAutoPlayOff();
			else
				setAutoPlayOn();
		}
	}

	private void setAutoPlayOn() {
		getLoadingView(true).setVisibility(View.VISIBLE);
		if (mPlayer != null)
			mPlayer.mayBePlayIfPrepared(mVidTextureView, this);

	}

	private void setAutoPlayOff() {
		getLoadingView(false).setVisibility(View.GONE);
		if (mPlayer != null)
			mPlayer.pauseVideo();
		if (initialView != null) {
			initialView.setVisibility(View.VISIBLE);
		}
		if (mParentPlayerStateChangedListener != null)
			mParentPlayerStateChangedListener.onPlayerPaused();
	}

	/**
	 * Name says it all
	 */
	public void pauseVideo() {

		if (isPlaying || isPlayActive) {
			this.onPlayerPaused();
			if (mPlayer != null)
				mPlayer.pauseVideo();
			if (pauseView != null) {
				pauseView.setVisibility(View.VISIBLE);
			}
			isPlaying = false;
			if (mParentPlayerStateChangedListener != null)
				mParentPlayerStateChangedListener.onPlayerPaused();
			getLoadingView(false).setVisibility(View.GONE);
		}
		VideoplayInteruptionBroadcastReciever
				.removeExternalInterruptListener(this);

	}

	/**
	 * Name says it all
	 */
	public void resumeVideo() {
		if (isPlayActive) {
			this.onPlayerResumed();
			mPlayer.resumeVideo();
			isPlaying = true;
			if (pauseView != null)
				// pauseView.setVisibility(View.GONE);
				if (mParentPlayerStateChangedListener != null)
					mParentPlayerStateChangedListener.onPlayerResumed();
			VideoplayInteruptionBroadcastReciever
					.setExternalInterruptListener(this);
		} else
			playVideo();
	}

	/**
	 * Releases the player on destroy
	 */
	@Override
	public void onDestroyView() {
		Log.d("destroy", "destroying SDKVideoFragment");
		releaseAndDestroyPlayer();
		super.onDestroyView();
	}

	private void releaseAndDestroyPlayer() {
		if (mPlayer != null) {
			mPlayer.resetSelf();
			mPlayer = null;
		}

	}

	public void setParentPlayerListener(IPlayerStateChangedListener listener) {
		this.mParentPlayerStateChangedListener = listener;
	}

	public void setAdapterAndPos(FeedsPagerAdapter adapter, int pos) {
		this.mParentAdapter = adapter;
		this.mPos = pos;
	}

	@Override
	public void onStart() {
		/* if (mParentAdapter != null) */
		// mParentAdapter.addFragToList(this, mPos);
		super.onStart();
	}

	/**
	 * returns the loading view
	 * 
	 * @return
	 */
	protected abstract View getLoadingView(boolean show);

	public abstract View getDefaultCoverImage();

	protected abstract String[] getUserDetails();

	public void registerMixpanelEvent(String event_name, int share_type,
			String profile_role) {
		String username = "", usertype = "";
		try {
			username = getUserDetails()[0];
			usertype = getUserDetails()[1];
		} catch (Exception e) {
		}

		if (isAdded())
			MixpanelUtils.sendGenericEvent(mVideoUrl, username, usertype,
					mParentScreen, event_name, getActivity());
	}

	@Override
	public void onInCall() {
		pauseVideo();

	}

	@Override
	public void onOutCall() {
		pauseVideo();

	}

	@Override
	public void onHeadphoneDisconnected() {
		Log.d("check", "pausing video headphone disconnected");

		pauseVideo();

	}

	@Override
	public void onMusicPlayed() {
		pauseVideo();

	}
}
