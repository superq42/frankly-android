package me.frankly.view.fragment;

public interface StopRecordingListener {

	public void onTimeFinished();

}
