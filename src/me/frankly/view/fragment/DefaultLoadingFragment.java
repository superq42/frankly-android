package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.Utilities;
import me.frankly.view.ShareDialog;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.ProfileActivity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class DefaultLoadingFragment extends Fragment {
	private ImageView loadingView;
	private AnimationDrawable loaderAnimation;
	private View mRootView, emptyContainerView;
	private String mParentScreen;
	private BaseActivity mBaseActivity;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Log.d("LastCard", "Show EOF (in onCreateView");
		mRootView = inflater.inflate(R.layout.loading_fragment_layout, null);
		if (getActivity() instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) getActivity();
		}
		emptyContainerView = mRootView.findViewById(R.id.rl_empty_screen);
		loadingView = (ImageView) mRootView.findViewById(R.id.iv_loader);
		loadingView.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) loadingView.getDrawable();
		boolean isMoreDataAvailable = getArguments().getBoolean(
				"isMoreDataAvailable");
		boolean isEmpty = getArguments().getBoolean("isEmpty");
		mParentScreen = getArguments().getString("mParentScreen");
		String profileId = getArguments().getString("profileId");

		Log.d("EOF", "isMoreDataAvailable: " + isMoreDataAvailable);
		Log.d("EOF", "isEmpty: " + isEmpty);
		// Handling only two cases - Loading screen and EOF
		if (isMoreDataAvailable) {
			// Loading More Data - Does not need to be empty
			Utilities.removeEmptyScreen(emptyContainerView, mRootView);
			loadingView.setVisibility(View.VISIBLE);
			loaderAnimation.start();
		} else if (!isEmpty && !isMoreDataAvailable) {
			// EOF

			loadingView.setVisibility(View.GONE);
			loaderAnimation.stop();
			if (mParentScreen.equals(ScreenSpecs.SCREEN_CHANNEL)) {
				View iconView = mRootView.findViewById(R.id.iv_empty_jewel);
				RelativeLayout.LayoutParams p = (LayoutParams) iconView
						.getLayoutParams();
				Log.d("DLF", "empty screenadded");
				p.addRule(RelativeLayout.CENTER_IN_PARENT);

				Utilities.showEmptyScreen(mRootView, inviteFriendsListener,
						Constant.PLACEHOLDER_SCREENS.END_CHANNEL);
			} else if (mParentScreen.equals(ScreenSpecs.SCREEN_DISCOVER)) {
				Utilities.showEmptyScreen(mRootView, inviteFriendsListener,
						Constant.PLACEHOLDER_SCREENS.END_DISCOVER);

			} else if (mParentScreen.equals(ScreenSpecs.SCREEN_PROFILE)) {

				if (profileId != null
						&& PrefUtils.getCurrentUserAsObject().getId()
								.equals(profileId))
					Utilities.showEmptyScreen(mRootView, endOfProfileListener,
							Constant.PLACEHOLDER_SCREENS.END_MY_PROFILE);
				else {
					mRootView.setTag(R.string.user_full_name,
							((ProfileActivity) getActivity()).objectsContainers
									.get(0).getUser().getFull_name());
					Utilities.showEmptyScreen(mRootView, endOfProfileListener,
							Constant.PLACEHOLDER_SCREENS.END_PROFILE);
				}
			}
			registerMixpanelEvent(MixpanelUtils.EOF);
		} else if (isEmpty && !isMoreDataAvailable) {
			if (mParentScreen.equals(ScreenSpecs.SCREEN_CHANNEL)||mParentScreen.equals(ScreenSpecs.SCREEN_PROFILE)) {
				/*
				 * if
				 * (mRootView.findViewById(R.id.iv_empty_jewel).getVisibility()
				 * == View.VISIBLE)
				 * mRootView.findViewById(R.id.iv_empty_jewel).setVisibility(
				 * View.GONE);
				 */
				View iconView = mRootView.findViewById(R.id.iv_empty_jewel);
				RelativeLayout.LayoutParams p = (LayoutParams) iconView
						.getLayoutParams();
				p.addRule(RelativeLayout.CENTER_IN_PARENT);
				Log.d("DLF", "called");
				Utilities.removeEmptyScreen(emptyContainerView, mRootView);
				Utilities.showEmptyScreen(mRootView, inviteFriendsListener,
						Constant.PLACEHOLDER_SCREENS.END_CHANNEL);
				loaderAnimation.stop();
				loadingView.setVisibility(View.GONE);
				
			}

		} else {
			loaderAnimation.stop();
			loadingView.setVisibility(View.GONE);
		}
		return mRootView;
	}

	private OnClickListener inviteFriendsListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Log.d("HA", "Do nothing");
			// if (v.getTag().equals(ScreenSpecs.SCREEN_FEEDS)) {
			// if (getActivity() instanceof HomeActivity)
			// ((HomeActivity) getActivity()).showDiscover();
			// } else {
			// showInviteFriendsDialog();
			// }

		}
	};

	private OnClickListener endOfProfileListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (v.getTag().equals(Constant.PLACEHOLDER_SCREENS.END_PROFILE)) {
				ProfileActivity profile = (ProfileActivity) getActivity();
				mBaseActivity.askActivity(profile.objectsContainers.get(0)
						.getUser());

			} else {
				showInviteFriendsDialog();

			}

		}
	};

	protected void showInviteFriendsDialog() {
		Log.d("Invite", "show invite friends dialogue box");
		ShareDialog cdd = new ShareDialog((BaseActivity) getActivity(),
				R.layout.share_dialog_post_layout, null, 1, false,
				Constant.ScreenSpecs.SCREEN_DISCOVER, null);
		cdd.show();
	}

	public void registerMixpanelEvent(String event_type) {
		if (isAdded())
			MixpanelUtils.sendGenericEvent(null, null, null, mParentScreen,
					event_type, getActivity());
	}

}
