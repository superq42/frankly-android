package me.frankly.view.fragment;

import java.io.File;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.config.UrlResolver;
import me.frankly.listener.CompressionCompletedListener;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.servicereceiver.ProfileUploadingService;
import me.frankly.util.FileUtils;
import me.frankly.util.JsonUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.VideoCompressionTool;
import me.frankly.util.FileUtils.FileType;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.BaseActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class UploadVideoFragment extends Fragment implements
		View.OnClickListener {

	private ImageView iv_upload;
	private UniversalUser mUser;
	private EditableProfileObject editprofileObject;
	private static final int REQUEST_CODE_UPDATE_VIDEO = 3;
	private String video_path;
	private String thumb_path;
	private final String TAG = "UploadVideoFragment";
	UploadVideoFragment f;
	private CircularImageView cvProfilepic;
	private View mRootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mUser = PrefUtils.getCurrentUserAsObject();
		editprofileObject = new EditableProfileObject();
		f = this;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		if (mUser.getBio() != null)
			mRootView = inflater.inflate(R.layout.layout_upload_video, null);
		else
			mRootView = inflater.inflate(R.layout.layout_upload_video_nobio,
					null);
		iv_upload = (ImageView) mRootView.findViewById(R.id.iv_upload);
		iv_upload.setOnClickListener(this);
		Controller.registerGAEvent("CelebQuestionsFragment");
		TextView tv_Name = (TextView) mRootView
				.findViewById(R.id.frag_proilfe_name);
		TextView tv_Bio = (TextView) mRootView
				.findViewById(R.id.frag_proilfe_info);
		TextView tvRank = (TextView) mRootView
				.findViewById(R.id.frag_proilfe_rank);
		if (mUser.getBio() != null)
			tv_Bio.setText(mUser.getBio());
		if (mUser.getFull_name() != null)
			tv_Name.setText(mUser.getFull_name());
		if (mUser.getUser_title() != null)
			tvRank.setText(mUser.getFull_name());
		else
			tvRank.setVisibility(View.GONE);
		cvProfilepic = (CircularImageView) mRootView
				.findViewById(R.id.frag_profile_img);
		if (mUser.getProfile_picture() != null) {
			DisplayImageOptions options = CircularImageView
					.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
			cvProfilepic.setImageUrl(options, mUser.getProfile_picture());
		}
		return mRootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_upload:
			((BaseActivity) getActivity()).dispatchTakeVideoIntent(this, mUser,
					REQUEST_CODE_UPDATE_VIDEO);
			break;
		default:
			break;
		}
	}

	private void upLoadProfile() {
		Controller.removeCurrentUserFromCache();
		String profilepicpath;
		profilepicpath = mUser.getProfile_picture();

		Log.i("debug_201upload", " Upload Profile  pic uro  " + profilepicpath);
		Log.i("debug_201upload", " Upload Profile  pic  " + profilepicpath);
		Log.i("debug_201upload", " Upload Profile video  " + video_path);
		Log.i("debug_201upload", "Upload Profile Cover " + thumb_path);

		editprofileObject.setId(PrefUtils.getUserID());
		editprofileObject.setProfile_picture(profilepicpath);
		editprofileObject.setProfile_video(video_path);
		editprofileObject.setCover_picture(thumb_path);
		PrefUtils.setPendingEditProfileObject(editprofileObject);
		PrefUtils.setIsVideoSentPending(true);

		File compressedVideoFile = FileUtils.newFile(getActivity(),
				FileType.VIDEO_COMPRESSED, PrefUtils.getUserID());
		final String compressedVideoPath = compressedVideoFile
				.getAbsolutePath();

		if (Build.VERSION.SDK_INT >= 16 && video_path != null) {
			new VideoCompressionTool().processOpenVideo(video_path,
					compressedVideoPath, null, getActivity(),
					new CompressionCompletedListener() {

						@Override
						public void didWriteData(File file, Context context,
								boolean last, boolean error) {
							Log.e(TAG, "didwriteData error= " + error);
							if (error) {
								FileUtils.delete(compressedVideoPath);
							} else {
								editprofileObject
										.setCompressedVideoUrl(compressedVideoPath);
							}
							editprofileObject.setIsCompressionValid(!error);
							PrefUtils
									.setPendingEditProfileObject(editprofileObject);
							PrefUtils.setIsVideoSentPending(true);
							Intent intent = new Intent(context,
									ProfileUploadingService.class);
							context.startService(intent);
						}
					}, false);

		} else if (video_path != null) {
			Log.e("debug_201didwriteData", "uploadeing without compression ");
			editprofileObject.setIsCompressionValid(false);
			if (Controller.isNetworkConnectedWithMessage(getActivity())) {
				Intent intent = new Intent(getActivity(),
						ProfileUploadingService.class);
				getActivity().startService(intent);
			}
		}
		UniversalUser currentUserAsObject = PrefUtils.getCurrentUserAsObject();
		if (video_path != null && thumb_path != null) {
			currentUserAsObject.setProfile_video(video_path);
			currentUserAsObject.setCover_picture(getAbsoluteUrl(thumb_path));
		}
		PrefUtils.saveCurrentUserAsJson(JsonUtils.jsonify(currentUserAsObject));
		Controller.removeCache(UrlResolver.EndPoints.PROFILE_GET_BY_ID + "/"
				+ PrefUtils.getUserID());
	}

	private String getAbsoluteUrl(String url) {
		if (url != null && !url.startsWith("file") && !url.startsWith("http"))
			return "file://" + url;
		return url;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d("uploadvid", "" + resultCode + resultCode + data.toString());
		if (requestCode == REQUEST_CODE_UPDATE_VIDEO) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					video_path = data.getStringExtra("video_path");
					Log.i(TAG, "  on result " + video_path);
					thumb_path = data.getStringExtra("thumb_path");
					Log.d("debug301_onActivityresult", "video " + video_path
							+ " thumb:" + thumb_path);
					Controller.showToast(getActivity(), "Video Uploading");
					upLoadProfile();
					((ChannelFragment) getParentFragment())
							.removeCard();
				}
			}
		}
	}
}
