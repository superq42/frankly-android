package me.frankly.view.fragment;

import java.util.ArrayList;
import java.util.Iterator;

import me.frankly.R;
import me.frankly.R.color;
import me.frankly.adapter.ChannelListAdapter;
import me.frankly.adapter.SearchListAdapter;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.ChannelSelectedListener;
import me.frankly.listener.RequestListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.newmodel.ChannelIcon;
import me.frankly.model.newmodel.ChannelIconList;
import me.frankly.model.newmodel.ChannelListContainer;
import me.frankly.model.newmodel.ChannelObject;
import me.frankly.model.newmodel.SearchResults;
import me.frankly.model.response.SearchableObjectContainer;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.ShareUtils;
import me.frankly.view.BariolTextView;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.FranklyHomeActivity;
import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ChannelListFragment extends Fragment implements TextWatcher,
		OnScrollListener, OnClickListener, OnItemClickListener,
		RequestListener, ChannelSelectedListener, UserFollowListener {
	private static final String TAG = ChannelListFragment.class.getSimpleName();
	private static final int LIST_MODE_SEARCH = 1;
	private static final int LIST_MODE_CHANNEL = 2;
	private static final int LIST_MODE_NONE = -1;
	private static final int ERROR_MODE_EMPTY_RESULT = 1;
	private static final int ERROR_MODE_NO_INTERNET = 2;
	private static final int ERROR_MODE_NONE = -1;

	private static final int BANNER_ID = 0000;

	private BaseActivity mBaseActivity;
	private EditText searchEditText;
	private ListView searchListView;
	private ArrayList<SearchableObjectContainer> searchResult = new ArrayList<SearchableObjectContainer>();
	private SearchListAdapter searchAdapter;
	private ChannelListAdapter channelsAdapter;
	private ArrayList<ChannelObject> channels = new ArrayList<ChannelObject>();

	private View clearView, errorContainerView, searchBoxView, retryView;
	private TextView errorMessageTextView;
	private ImageView loaderView, errorIconImageView;
	private AnimationDrawable loaderDrawable;
	private int nextIndex = 0;
	private boolean isLoading = false;
	private volatile String mCurQueryString;
	private int mCurListMode = LIST_MODE_NONE, mCurErrorMode = ERROR_MODE_NONE;
	private boolean isTwitterInstalled;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("SA", "On create");
		super.onCreate(savedInstanceState);
		if (getActivity() instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) getActivity();
		}
		isTwitterInstalled = ShareUtils.isAppInstalled(ShareUtils.PKG_TWITTER,
				getActivity());
		searchAdapter = new SearchListAdapter(mBaseActivity, searchResult);
		channelsAdapter = new ChannelListAdapter(getActivity(), channels);
		channelsAdapter.setOnChannelSelectedListener(this);
		searchAdapter.setOnChannleSelectedListener(this);
		UniversalActionListenerCollection.getInstance()
				.addGeneralFollowListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Log.d(TAG, "On create view");
		View mRootView = inflater.inflate(R.layout.activity_search, null);
		searchEditText = (EditText) mRootView.findViewById(R.id.et_search);
		searchEditText.addTextChangedListener(this);
		searchBoxView = mRootView.findViewById(R.id.ll_search_tool);
		clearView = mRootView.findViewById(R.id.iv_search_clear);
		clearView.setOnClickListener(this);
		searchListView = (ListView) mRootView.findViewById(R.id.lv_search);
		loaderView = new ImageView(getActivity());
		loaderDrawable = (AnimationDrawable) getResources().getDrawable(
				R.drawable.loader_general);
		loaderView.setImageDrawable(loaderDrawable);
		searchListView.addFooterView(loaderView);
		hideLoaderView();
		searchListView.setOnScrollListener(this);

		searchListView.setOnItemClickListener(this);
		findErrorViews(mRootView);
		retryView.setOnClickListener(this);
		searchBoxView.setVisibility(View.GONE);
		getChannelList();
		displayChannelList();
		mRootView.setOnClickListener(this);
		return mRootView;
	}

	private void showLoaderView() {
		if (loaderView.getVisibility() != View.VISIBLE) {
			loaderView.setVisibility(View.VISIBLE);
			loaderDrawable.start();
		}

	}

	private void hideLoaderView() {
		if (loaderView.getVisibility() == View.VISIBLE) {
			loaderDrawable.stop();
			loaderView.setVisibility(View.GONE);
		}

	}

	private void findErrorViews(View mRootView) {

		errorContainerView = mRootView.findViewById(R.id.rl_error);
		errorIconImageView = (ImageView) mRootView
				.findViewById(R.id.iv_err_icon);
		errorMessageTextView = (TextView) mRootView
				.findViewById(R.id.tv_err_msg);
		retryView = mRootView.findViewById(R.id.tv_error_retry);
	}

	private void getChannelList() {
		if (Controller.isNetworkConnected(getActivity())) {
			showLoaderView();
			Controller.getChannelList("", "", channelsListener, getActivity());
		} else
			showNoInternetError();
	}

	private void displayChannelList() {
		if (Controller.isNetworkConnected(getActivity()))
			hideError();
		if (mCurListMode != LIST_MODE_CHANNEL) {
			// searchBoxView.setVisibility(View.GONE);
			searchListView.setAdapter(channelsAdapter);
			mCurListMode = LIST_MODE_CHANNEL;
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					searchEditText.requestFocus();
				}
			}, 200);
		}
	}

	private void displaySearch() {
		if (AppConfig.DEBUG) {
			Log.d("SA", "display search");
		}
		if (mCurListMode != LIST_MODE_SEARCH) {
			searchBoxView.setVisibility(View.VISIBLE);
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					searchEditText.requestFocus();
				}
			}, 200);
			searchListView.setAdapter(searchAdapter);
			mCurListMode = LIST_MODE_SEARCH;
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
		if (s.toString().trim().isEmpty()) {
			clearSearch();
			displayChannelList();
		} else {
			displaySearch();
			initSearch(s);
		}

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	private void initSearch(Editable arg0) {

		if (Controller.isNetworkConnected(getActivity())) {
			if (!searchResult.isEmpty()) {
				searchResult.clear();
				searchAdapter.notifyDataSetChanged();
			}
			isLoading = false;
			nextIndex = 0;
			mCurQueryString = arg0.toString().trim();
			search();
			hideError();
		} else
			showNoInternetError();
	}

	private void search() {

		if (!isLoading && isMoreDataAvailable() && mCurQueryString != null
				&& !mCurQueryString.isEmpty()) {
			isLoading = true;
			showLoaderView();
			Controller.searchElastic(getActivity(), mCurQueryString,
					Controller.DEFAULT_PAGE_SIZE, nextIndex, this);
		} else if (mCurQueryString != null && mCurQueryString.isEmpty())
			hideLoaderView();

	}

	private boolean isMoreDataAvailable() {
		return nextIndex >= 0;
	}

	private void showNoInternetError() {

		if (mCurErrorMode != ERROR_MODE_NO_INTERNET) {
			if (errorContainerView.getVisibility() != View.VISIBLE)
				errorContainerView.setVisibility(View.VISIBLE);
			if (retryView.getVisibility() != View.VISIBLE)
				retryView.setVisibility(View.VISIBLE);
			RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(
					android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT,
					android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);
			p.addRule(RelativeLayout.CENTER_IN_PARENT);
			p.addRule(RelativeLayout.CENTER_HORIZONTAL);
			errorContainerView.setLayoutParams(p);
			errorIconImageView.setImageResource(R.drawable.search_no_internet);
			errorMessageTextView
					.setText(Html
							.fromHtml("<B>Couldn't connect to the internet!</B><Br>Please check your internet & retry"));

			mCurErrorMode = ERROR_MODE_NO_INTERNET;
		}

	}

	private void showEmptyResultError() {
		if (mCurErrorMode != ERROR_MODE_EMPTY_RESULT) {
			if (errorContainerView.getVisibility() != View.VISIBLE)
				errorContainerView.setVisibility(View.VISIBLE);
			if (retryView.getVisibility() == View.VISIBLE)
				retryView.setVisibility(View.GONE);
			RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(
					android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT,
					android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);
			p.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			p.addRule(RelativeLayout.CENTER_HORIZONTAL);
			errorContainerView.setLayoutParams(p);
			errorIconImageView.setImageResource(R.drawable.no_result_cat);
			errorMessageTextView.setText(Html
					.fromHtml("<h3>Sorry!</h3><Br>No results found!"));

			mCurErrorMode = ERROR_MODE_EMPTY_RESULT;
		}

	}

	private void hideError() {
		if (mCurErrorMode != ERROR_MODE_NONE) {
			if (searchListView.getVisibility() != View.VISIBLE)
				searchListView.setVisibility(View.VISIBLE);
			errorContainerView.setVisibility(View.GONE);
			mCurErrorMode = ERROR_MODE_NONE;
		}
	}

	private boolean isNearEnd(int i, int totalItemCount) {
		// fileDebugLog(FILE_DEBUG_TAG + "isNearEnd",
		// "(totalItemCount - i < 3)? "
		// + (totalItemCount - i < 3));

		return totalItemCount - i < 3;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (isNearEnd(firstVisibleItem + visibleItemCount, totalItemCount))
			search();

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		MyUtilities.hideKeyboard(searchEditText, getActivity());

	}

	@Override
	public void onClick(View v) {
		if(AppConfig.DEBUG) {
			Log.d("SA", "onClick called");
		}
		switch (v.getId()) {
		case R.id.iv_search_clear:
			clearSearch();
			break;
		case BANNER_ID:
			String bannerIdString = (String) v.getTag();
			onChannelSelected(bannerIdString);
			break;
		case R.id.tv_error_retry:
			retry();
			break;
		
		}

	}

	private void retry() {
		getChannelList();
	}

	private void clearSearch() {

		if (!searchEditText.getText().toString().isEmpty()) {
			mCurQueryString = "";
			nextIndex = 0;
			searchEditText.setText("");
			searchResult.clear();
			searchAdapter.setData(searchResult);
			searchAdapter.notifyDataSetChanged();
			hideLoaderView();
			displayChannelList();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		final SearchResults results = (SearchResults) JsonUtils.objectify(
				(String) responseObject, SearchResults.class);
		if (results.getQ().equals(mCurQueryString)) {
			nextIndex = results.getNextIndex();
			if (isAdded())
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						hideLoaderView();
						isLoading = false;
						if (!isTwitterInstalled)
							results.setResults(removeInvitablesWithNoEmails(results
									.getResults()));
						searchResult = MyUtilities.appendUniqueItems(
								searchResult,
								removeUnknowns(results.getResults()),
								MyUtilities.MODE_DELETE_PREVIOUS);
						if (!searchResult.isEmpty()) {
							hideError();
							for (int i = 0; i < searchResult.size(); i = i + 1) {
								if (searchResult
										.get(i)
										.getType()
										.equals(SearchableObjectContainer.TYPE_INVITABLE_USER)
										&& searchResult.get(i).getInvitable()
												.isHas_invited()) {
									searchResult.remove(i);
								}
							}
							searchAdapter.setData(searchResult);
							searchAdapter.notifyDataSetChanged();
						} else
							showEmptyResultError();

					}
				});
		}

	}

	@Override
	public void onRequestError(int errorCode, String message) {

	}

	private ArrayList<SearchableObjectContainer> removeUnknowns(
			ArrayList<SearchableObjectContainer> stream) {
		Iterator<SearchableObjectContainer> i = stream.iterator();
		while (i.hasNext()) {
			SearchableObjectContainer temp = i.next();
			if (!SearchableObjectContainer.ACCEPTED_TYPE.contains(temp
					.getType()))
				i.remove();
		}
		return stream;

	}

	public ArrayList<SearchableObjectContainer> removeInvitablesWithNoEmails(
			ArrayList<SearchableObjectContainer> stream) {
		Iterator<SearchableObjectContainer> i = stream.listIterator(0);
		while (i.hasNext()) {
			SearchableObjectContainer temp = i.next();

			if (temp.getType().equals(
					SearchableObjectContainer.TYPE_INVITABLE_USER)
					&& (temp.getInvitable().getEmail() == null || temp
							.getInvitable().getEmail().isEmpty())) {
				i.remove();
			}
		}
		return stream;

	}

	private RequestListener channelsListener = new RequestListener() {

		@Override
		public void onRequestStarted() {

		}

		@Override
		public void onRequestError(int errorCode, String message) {

		}

		@Override
		public void onRequestCompleted(Object responseObject) {

			final ChannelListContainer container = (ChannelListContainer) JsonUtils
					.objectify((String) responseObject,
							ChannelListContainer.class);
			if (isAdded()) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (container != null
								&& container.getChannel_list() != null) {
							ChannelObject searchChannelObject = getSearchChannelObject(container
									.getChannel_list());
							hideError();
							channels.add(searchChannelObject);
							int bannerChannelCount = 0;
							for (ChannelObject tempChannelObject : container
									.getChannel_list()) {
								if (tempChannelObject.getType().equals(
										ChannelObject.TYPE_BANNER)) {
									searchListView
											.addHeaderView(getBannerHeaderView(tempChannelObject));

								}
							}
							searchListView.addHeaderView(getSearchHeaderView());
							Log.d("CLF", "notifyDSC called");
							channelsAdapter.notifyDataSetChanged();
							if (mCurListMode == LIST_MODE_CHANNEL)
								hideLoaderView();
						}

					}

				});
			}

		}
	};

	private ChannelObject getSearchChannelObject(
			ArrayList<ChannelObject> channel_list) {
		Iterator<ChannelObject> i = channel_list.iterator();
		while (i.hasNext()) {
			ChannelObject temp = i.next();
			if (temp.getType().equals(ChannelObject.TYPE_SEARCH)) {
				i.remove();
				return temp;
			}
		}
		return null;
	}

	private View getBannerHeaderView(ChannelObject tempChannelObject) {
		int bannerIconId;
		TextView channelTitleTextView = new BariolTextView(getActivity());
		channelTitleTextView.setGravity(Gravity.CENTER);
		channelTitleTextView.setTextSize(20);
		channelTitleTextView.setTextColor(getActivity().getResources()
				.getColor(color.white_pure));
		channelTitleTextView.setPadding(0, 15, 0, 15);
		LayoutParams p = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		channelTitleTextView.setLayoutParams(p);
		channelTitleTextView.setOnClickListener(this);
		channelTitleTextView.setId(BANNER_ID);

		channelTitleTextView.setTag(tempChannelObject.getChannel_id());
		channelTitleTextView.setText(tempChannelObject.getName());
		if (tempChannelObject.getChannel_id().equals(Constant.CHANNEL.FEED))
			bannerIconId = R.drawable.feeds_icon;
		else if (tempChannelObject.getChannel_id().equals(
				Constant.CHANNEL.DISCOVER))
			bannerIconId = R.drawable.discover_icon;
		else
			bannerIconId = -1;
		channelTitleTextView.setText(tempChannelObject.getName());
		/*
		 * if (bannerIconId > 0)
		 * channelTitleTextView.setCompoundDrawablesWithIntrinsicBounds(
		 * getResources().getDrawable(bannerIconId), null, null, null);
		 */
		return channelTitleTextView;
	}

	private View getSearchHeaderView() {

		searchBoxView = LayoutInflater.from(getActivity()).inflate(
				R.layout.search_channel, null);

		View searchView = searchBoxView.findViewById(R.id.rl_search);
		searchView.setOnClickListener(this);
		searchEditText = (EditText) searchBoxView.findViewById(R.id.et_search);
		searchEditText.addTextChangedListener(this);
		searchEditText.setFocusable(true);
		searchEditText.setFocusableInTouchMode(true);
		clearView = searchBoxView.findViewById(R.id.iv_search_clear);
		clearView.setOnClickListener(this);

		return searchBoxView;
	}

	@Override
	public void onChannelSelected(String channelId) {
		if (isAdded()) {
			MyUtilities.hideKeyboard(searchEditText, getActivity());
			((FranklyHomeActivity) getActivity()).showChannel(channelId);
		}
	}

	@Override
	public void onSearchSelected(final String queryString) {
		displaySearch();
		searchEditText.setText(queryString);
		searchEditText.setSelection(queryString.length());
	}

	@Override
	public void onDestroy() {
		UniversalActionListenerCollection.getInstance()
				.removeGeneralFollowListener(this);
		super.onDestroy();
	}

	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		syncSearchResults(userId, isFollowed);
		syncChannelList(userId, isFollowed);
	}

	private void syncChannelList(String userId, boolean isFollowed) {
		for (ChannelObject tempChannel : channels)
			if (tempChannel.getType().equals(ChannelObject.TYPE_SEARCH)) {
				for (ChannelIconList tempIconList : tempChannel.getViews()) {
					for (ChannelIcon tempIcon : tempIconList.getIcons()) {
						if (tempIcon.getType().equals(ChannelIcon.TYPE_USER)
								&& tempIcon.getUser().getId().equals(userId)) {
							tempIcon.getUser().setIs_following(isFollowed);
							/*
							 * if (channelsAdapter != null)
							 * channelsAdapter.notifyDataSetChanged();
							 */
							break;
						}
					}
				}
			}

	}

	private void syncSearchResults(String userId, boolean isFollowed) {
		for (SearchableObjectContainer tempObjectContainer : searchResult)
			if (tempObjectContainer.getType().equals(
					SearchableObjectContainer.TYPE_USER)
					&& tempObjectContainer.getUser().getId().equals(userId)) {
				tempObjectContainer.getUser().setIs_following(isFollowed);
				if (searchAdapter != null)
					searchAdapter.notifyDataSetChanged();
				break;
			}
	}

	public boolean resetState(Activity callerActivity) {
		if (isAdded() && callerActivity.equals(getActivity())
				&& mCurListMode == LIST_MODE_SEARCH) {
			clearSearch();
			return true;
		}
		return false;
	}

}
