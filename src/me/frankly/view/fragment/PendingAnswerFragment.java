package me.frankly.view.fragment;

import java.io.File;

import me.frankly.R;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.listener.PendingAnswersProgressListener;
import me.frankly.model.newmodel.PendingAnswer;
import me.frankly.servicereceiver.PendingAnswerProgressReceiver;
import me.frankly.servicereceiver.SingleAnswerUploadService;
import me.frankly.util.JsonUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.view.activity.ProfileActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This fragment show pending answers with upload progress or retry button
 */
public class PendingAnswerFragment extends Fragment implements
		PendingAnswersProgressListener, OnClickListener {

	private String LOG_TAG = PendingAnswerFragment.class.getSimpleName();

	private PendingAnswer pendingAnswer;
	private String questionId;
	private Bitmap answerBitmap;

	private ImageView ivAnswer;
	private ImageView ivCompressionProgress;
	private View llProgressPercentageAndStatus;
	private ProgressBar progressBar;
	private TextView tvProgressPercent;
	private TextView tvProgressState;
	private View llRetry;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Bundle args = getArguments();
		pendingAnswer = (PendingAnswer) JsonUtils.objectify(
				args.getString(Constant.Key.KEY_PENDING_ANSWER),
				PendingAnswer.class);
		questionId = pendingAnswer.getQuestionObject().getId();

		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "onCreateView(): " + questionId);
		}

		File imageFile = new File(pendingAnswer.getImagePath());
		if (imageFile.exists()) {
			answerBitmap = BitmapFactory
					.decodeFile(imageFile.getAbsolutePath());
		}

		View root = inflater
				.inflate(R.layout.fragment_pending_ans_upload, null);

		setQuestionViews(root);

		ivAnswer = (ImageView) root.findViewById(R.id.pending_answer_img);

		llProgressPercentageAndStatus = root
				.findViewById(R.id.ll_pending_answer_progress);
		progressBar = (ProgressBar) root.findViewById(R.id.pb_answerUpload);
		tvProgressPercent = (TextView) root
				.findViewById(R.id.tv_progressPrecent);
		tvProgressState = (TextView) root
				.findViewById(R.id.tv_pending_answer_state);
		llRetry = root.findViewById(R.id.ll_retry);
		llRetry.setOnClickListener(this);

		return root;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "onStart() " + questionId);
		}
		((ProfileActivity) getActivity()).addPendingAnsProgressListener(
				questionId, this);

		PendingAnswer savedPendingAnser = PrefUtils
				.getPendingAnswer(pendingAnswer.getClientId());
		if (savedPendingAnser != null) {
			pendingAnswer = savedPendingAnser;
		}

		updateUiByCurrentState();
	}

	@Override
	public void onStop() {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "onStop() " + questionId);
		}
		((ProfileActivity) getActivity())
				.delPendingAnsProgressListener(questionId);
		super.onStop();
	}

	/**
	 * @param view
	 */
	private void setQuestionViews(View view) {
		TextView tvQuestion = (TextView) view
				.findViewById(R.id.info_display_paused_video_question_text);
		tvQuestion.setText(pendingAnswer.getQuestionObject().getBody());

		TextView tvQuestionAuthor = (TextView) view
				.findViewById(R.id.info_display_paused_video_question_author);
		tvQuestionAuthor.setText(pendingAnswer.getQuestionObject()
				.getQuestion_author().getFirst_name());
		ivCompressionProgress = (ImageView) view
				.findViewById(R.id.compression_progress_image);

	}

	/**
	 * this method updates UI as per answer state
	 */
	private void updateUiByCurrentState() {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "updateUiByCurrentState(): " + pendingAnswer.getCurrentState());
		}
		switch (pendingAnswer.getCurrentState()) {
		case PendingAnswer.STATE_WAITING: {
			showWaitingState();
			break;
		}
		case PendingAnswer.STATE_PREPARING: {
			showPreparingState();
			break;
		}
		case PendingAnswer.STATE_UPLOADING: {
			showUploadingState();
			break;
		}
		case PendingAnswer.STATE_FAILED: {
			showFailureState();
			break;
		}
		}
	}

	/**
	 * it updates UI for {@link PendingAnswer#STATE_WAITING}
	 */
	private void showWaitingState() {
		progressBar.setProgress(0);
		tvProgressPercent.setText("0.0%");
		tvProgressState.setText("Waiting");

		llProgressPercentageAndStatus.setVisibility(View.VISIBLE);
		llRetry.setVisibility(View.GONE);

		updateAnswerImage();
		stopRotatingAnimation();
	}

	/**
	 * it updates UI for {@link PendingAnswer#STATE_PREPARING}
	 */
	private void showPreparingState() {
		progressBar.setProgress(0);
		tvProgressState.setText("Processing Video...");
		llProgressPercentageAndStatus.setVisibility(View.VISIBLE);
		llRetry.setVisibility(View.GONE);
		startRotatingAnimation();
		updateAnswerImage();
	}

	/**
	 * It updates UI for {@link PendingAnswer#STATE_UPLOADING}
	 */
	private void showUploadingState() {
		stopRotatingAnimation();
		float uploadPrecent = pendingAnswer.getPrecentUploaded();
		progressBar.setVisibility(View.VISIBLE);
		progressBar.setProgress((int) uploadPrecent);
		tvProgressPercent.setText(String.valueOf(uploadPrecent) + "%");
		tvProgressState.setText("Uploading Video...");

		llProgressPercentageAndStatus.setVisibility(View.VISIBLE);
		llRetry.setVisibility(View.GONE);

		updateAnswerImage();
	}

	/**
	 * this method updates UI as per {@link PendingAnswer#STATE_FAILURE}
	 */
	private void showFailureState() {
		stopRotatingAnimation();
		llProgressPercentageAndStatus.setVisibility(View.GONE);
		llRetry.setVisibility(View.VISIBLE);
		updateAnswerImage();
	}

	/**
	 * updates answer image as per uploadPercent
	 */
	private void updateAnswerImage() {
		if (answerBitmap == null) {
			return;
		}
		float uploadPrecent = pendingAnswer.getPrecentUploaded();

		if (uploadPrecent == 100) {
			ivAnswer.setImageBitmap(answerBitmap);
		} else {
			Bitmap gaussedAnswerBitmap = MyUtilities.fastblur(answerBitmap,
					10 - (int) (uploadPrecent / 10) + 1);
			ivAnswer.setImageBitmap(gaussedAnswerBitmap);
		}
	}

	@Override
	public void onProgressUpdate(Context ctx, Intent data) {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "onProgressUpdate() "
					+ (data == null ? "null" : data.getExtras()));
		}

		/**
		 * no need to check question id here. <br/>
		 * as {@link ProfileActivity} has checked it.
		 */
		if (data.hasExtra(PendingAnswerProgressReceiver.KEY_EXTRA_ERROR_CODE)) {
			Toast.makeText(getActivity(),
					"Some error happened. Please try again later.",
					Toast.LENGTH_SHORT).show();
			pendingAnswer.setPrecentUploaded(0);
			pendingAnswer.setCurrentState(PendingAnswer.STATE_WAITING);
			showFailureState();
			return;
		}

		if (data.hasExtra(PendingAnswerProgressReceiver.KEY_EXTRA_PROGRESS_STATE)) {
			int state = data.getIntExtra(
					PendingAnswerProgressReceiver.KEY_EXTRA_PROGRESS_STATE,
					PendingAnswer.STATE_WAITING);
			pendingAnswer.setCurrentState(state);
			if (state == PendingAnswer.STATE_UPLOADING
					&& data.hasExtra(PendingAnswerProgressReceiver.KEY_EXTRA_PROGRESS_PERCENT)) {
				float tempPercent = data
						.getFloatExtra(
								PendingAnswerProgressReceiver.KEY_EXTRA_PROGRESS_PERCENT,
								0.0f);
				if (pendingAnswer.getPrecentUploaded() < tempPercent) {
					pendingAnswer.setPrecentUploaded(tempPercent);
				}
			}
			updateUiByCurrentState();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.ll_retry: {
			retryPendingAnswerUpload();
			break;
		}
		}
	}

	/**
	 * checks network, starts service and updates UI
	 */
	private void retryPendingAnswerUpload() {
		if (!Controller.isNetworkConnectedWithMessage(getActivity())) {
			return;
		}
		pendingAnswer.setCurrentState(PendingAnswer.STATE_WAITING);
		updateUiByCurrentState();

		PrefUtils.updatePendingAnswer(pendingAnswer);

		// start service to upload this pending answer
		SingleAnswerUploadService.start(getActivity(), pendingAnswer);
	}

	/**
	 * starts the compression processing rotation animation of progressbar makes
	 * progressPercentage invisible
	 */
	private void startRotatingAnimation() {
		progressBar.setVisibility(View.INVISIBLE);
		ivCompressionProgress.setVisibility(View.VISIBLE);
		tvProgressPercent.setVisibility(View.INVISIBLE);
		Animation rotateAnimation = AnimationUtils.loadAnimation(getActivity(),
				R.anim.rotate_animation);
		ivCompressionProgress.startAnimation(rotateAnimation);
	}

	/**
	 * stops the compression processing rotation animation of progressbar makes
	 * progressPercentage visible
	 */
	private void stopRotatingAnimation() {
		tvProgressPercent.setVisibility(View.VISIBLE);
		ivCompressionProgress.clearAnimation();
		ivCompressionProgress.setVisibility(View.INVISIBLE);
	}
}
