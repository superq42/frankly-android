package me.frankly.view.fragment;

import java.util.regex.Pattern;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.RegexUtils;
import me.frankly.util.ShareUtils;
import me.frankly.view.activity.RegisterUserActivity;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class SignUpFragment extends Fragment implements OnClickListener,
/* RequestListener, */OnEditorActionListener {
	public static final String KEY_SHOW_PASSWORD_FEILD = "show_password_feild";
	public static final String KEY_FIRST_NAME = "first_name";
	public static final String KEY_LAST_NAME = "last_name";

	private boolean showPasswordFeild = false;
	private EditText emailEditText, passwordEditText, contactNumberEditText,
			fullNameEditText;
	private String newEmail = null;
	private String newPassword = null;
	private String newContactNumber = null;
	private String newFullName = null;
	private LinearLayout llTwitterPresent, llTwitterAbsent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null && args.containsKey(KEY_SHOW_PASSWORD_FEILD))
			showPasswordFeild = args.getBoolean(KEY_SHOW_PASSWORD_FEILD);

		setEmailFromAccounts();
		setFullNameFromContacts();
	}

	private void setFullNameFromContacts() {
		Cursor c = getActivity().getContentResolver().query(
				ContactsContract.Profile.CONTENT_URI, null, null, null, null);
		int count = c.getCount();
		String[] columnNames = c.getColumnNames();
		c.moveToFirst();
		int position = c.getPosition();
		if (count == 1 && position == 0) {
			for (int j = 0; j < columnNames.length; j++) {
				String columnName = columnNames[j];
				String columnValue = c.getString(c.getColumnIndex(columnName));
				Log.d("check", "key: " + columnName + " val: " + columnValue);
				if (columnName.equalsIgnoreCase("display_name")) {
					newFullName = columnValue;
					return;
				}

				// ...
				// consume the values here
			}
		}
		c.close();
	}

	private void setEmailFromAccounts() {
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
		Account[] accounts = AccountManager.get(getActivity()).getAccounts();
		for (Account account : accounts) {
			if (emailPattern.matcher(account.name).matches()) {
				newEmail = account.name;
				return;
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View mRootView = inflater.inflate(R.layout.layout_signup, null);

		emailEditText = (EditText) mRootView.findViewById(R.id.et_email);
		if (!showPasswordFeild) {
			emailEditText.setOnEditorActionListener(this);
		} else {
			passwordEditText = (EditText) mRootView
					.findViewById(R.id.et_password);
			passwordEditText.setOnEditorActionListener(this);
		}
		if (newEmail != null)
			emailEditText.setText(newEmail);
		contactNumberEditText = (EditText) mRootView
				.findViewById(R.id.et_phone);
		fullNameEditText = (EditText) mRootView.findViewById(R.id.et_fullname);
		ImageView btn_back = (ImageView) mRootView
				.findViewById(R.id.action_bar_back_btn);
		TextView title_text = (TextView) mRootView
				.findViewById(R.id.action_bar_title_text);
		title_text.setText("Sign Up");

		btn_back.setOnClickListener(this);

		fullNameEditText.setFocusable(true);

		if (newFullName != null) {
			fullNameEditText.setText(newFullName);
			fullNameEditText.setSelection(newFullName.length());
		}
		if (getArguments() != null
				&& getArguments().containsKey(KEY_FIRST_NAME))
			fullNameEditText.setHint(getArguments().getString(KEY_FIRST_NAME)
					+ getArguments().getString(KEY_LAST_NAME));
		fullNameEditText.setOnEditorActionListener(this);
		((RegisterUserActivity) getActivity()).clearUsername();

		mRootView.findViewById(R.id.tv_signup_button).setOnClickListener(this);
		((RegisterUserActivity) getActivity()).clearEmailAndPassword();

		mRootView.findViewById(R.id.iv_login_fb_stub).setOnClickListener(this);
		mRootView.findViewById(R.id.tv_policy).setOnClickListener(this);
		mRootView.findViewById(R.id.tv_terms).setOnClickListener(this);
		mRootView.findViewById(R.id.iv_login_twitter).setOnClickListener(this);
		llTwitterPresent = (LinearLayout) mRootView
				.findViewById(R.id.ll_non_fb_login);
		llTwitterAbsent = (LinearLayout) mRootView
				.findViewById(R.id.ll_fb_gplus_login);

		Controller.registerGAEvent("EmailAndPwdFragment");
		checkTwitterPresence(mRootView);

		return mRootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.action_bar_back_btn:
			MyUtilities.hideKeyboard(fullNameEditText, getActivity());
			getActivity().onBackPressed();
			break;

		case R.id.tv_signup_button:
			MyUtilities.hideKeyboard(fullNameEditText, getActivity());
			if (isAdded()
					&& Controller.isNetworkConnectedWithMessage(getActivity())) {
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(
						MixpanelUtils.SIGNUP_ATTEMPT,
						Constant.ScreenSpecs.SCREEN_SIGNUP);
				validateSignUp();
			}
			break;
		case R.id.iv_login_fb_new:
		case R.id.iv_login_fb_stub:
			if (isAdded()
					&& Controller.isNetworkConnectedWithMessage(getActivity())) {
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(
						MixpanelUtils.FB_LOGIN,
						Constant.ScreenSpecs.SCREEN_SIGNUP);
				Intent resultIntent = new Intent();

				resultIntent
						.putExtra(Constant.Key.LOGIN_FACEBOOK_FRANKLY, true);
				getActivity().setResult(Activity.RESULT_OK, resultIntent);
				getActivity().finish();

			}
			break;
		case R.id.iv_login_gplus_new:
		case R.id.iv_login_gplus:
			if (isAdded()
					&& Controller.isNetworkConnectedWithMessage(getActivity())) {
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(
						MixpanelUtils.GPLUS_LOGIN,
						Constant.ScreenSpecs.SCREEN_SIGNUP);
				Intent resultIntent = new Intent();

				resultIntent.putExtra(Constant.Key.LOGIN_GOOGLE_PLUS_FRANKLY,
						true);
				getActivity().setResult(Activity.RESULT_OK, resultIntent);
				getActivity().finish();

			}
			break;
		case R.id.iv_login_twitter:
			if (isAdded()
					&& Controller.isNetworkConnectedWithMessage(getActivity())) {
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(
						MixpanelUtils.TWITTER_LOGIN,
						Constant.ScreenSpecs.SCREEN_SIGNUP);
				Intent resultIntent = new Intent();

				resultIntent.putExtra(Constant.Key.LOGIN_TWITTER_FRANKLY, true);
				getActivity().setResult(Activity.RESULT_OK, resultIntent);
				getActivity().finish();

			}
			break;
		case R.id.tv_policy:
			openWebPage(getActivity().getString(R.string.frankly_me_privacy));
			break;
		case R.id.tv_terms:
			openWebPage(getActivity().getString(R.string.frankly_me_tos));
			break;
		}
	}

	private void validateSignUp() {

		newFullName = fullNameEditText.getText().toString().trim();
		newEmail = emailEditText.getText().toString().trim();
		newContactNumber = contactNumberEditText.getText().toString().trim();

		Log.d("Signup", "Just assigned all the variables");

		if (showPasswordFeild) {
			newPassword = passwordEditText.getText().toString().trim();
			Log.e("hemant", "showPassword true");
		}

		Log.d("Signup", newEmail + " " + newFullName + " " + newContactNumber
				+ " " + newPassword);

		Log.d("Signup", "Validated everything");
		// Validate the fields
		if (newFullName == null || newFullName.length() < 3)
			Controller.showToast(getActivity(),
					"Please provide your full name.");
		else if (!RegexUtils.isValidFullName(newFullName))
			Controller.showToast(getActivity(),
					"Use alphabets only.");
		else if (newEmail == null || !RegexUtils.isValidEmail(newEmail))
			Controller.showToast(getActivity(),
					"Please provide a valid email address.");
		else if (newContactNumber != null && newContactNumber.length() > 0
				&& newContactNumber.length() < 6)
			Controller.showToast(getActivity(),
					"Please provide a valid phone number.");
		else if ((newPassword == null || newPassword.length() < 6))
			Controller.showToast(getActivity(),
					"Password should be at least 6 characters long.");
		else {
			Log.e("hemant", "signing in");
			((RegisterUserActivity) getActivity()).onEmailAndPasswordSelected(
					newEmail, newPassword, newContactNumber, newFullName);
		}

	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_DONE
				|| actionId == EditorInfo.IME_ACTION_NEXT)
			validateSignUp();
		return false;
	}

	private void openWebPage(String url) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(browserIntent);
	}

	private void checkTwitterPresence(View view) {
		if (ShareUtils.findAppByPackageName(ShareUtils.PKG_TWITTER,
				getActivity()) != null) {
			llTwitterPresent.setVisibility(View.VISIBLE);
			llTwitterAbsent.setVisibility(View.GONE);
			view.findViewById(R.id.iv_login_gplus).setOnClickListener(this);
			view.findViewById(R.id.iv_login_fb_stub)
					.setVisibility(View.VISIBLE);
		} else {
			llTwitterAbsent.setVisibility(View.VISIBLE);
			llTwitterPresent.setVisibility(View.GONE);
			view.findViewById(R.id.iv_login_gplus_new).setOnClickListener(this);
			view.findViewById(R.id.iv_login_fb_new).setOnClickListener(this);
			view.findViewById(R.id.iv_login_fb_stub).setVisibility(View.GONE);
		}
	}
}
