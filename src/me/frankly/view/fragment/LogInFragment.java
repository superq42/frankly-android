package me.frankly.view.fragment;

import java.util.regex.Pattern;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.exception.ErrorCode;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.ShareUtils;
import me.frankly.view.activity.ForgotPasswordActivity;
import me.frankly.view.activity.RegisterUserActivity;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class LogInFragment extends Fragment implements OnClickListener,
		OnEditorActionListener {

	private EditText emailEditText, pwdEditText;
	private String nowUsername = null;
	private String nowPassword = null;
	private TextView emailErrorTextView, pwdErrorTextView;
	private LinearLayout llTwitterPresent, llTwitterAbsent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setEmailFromAccounts();
	}

	private void setEmailFromAccounts() {
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
		Account[] accounts = AccountManager.get(getActivity()).getAccounts();
		for (Account account : accounts) {
			if (emailPattern.matcher(account.name).matches()) {
				nowUsername = account.name;
				Log.d("SignInFragment", "username " + nowUsername);
				return;
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View mRootView = inflater.inflate(R.layout.login_screen, null);

		emailEditText = (EditText) mRootView.findViewById(R.id.et_email_phone);
		pwdEditText = (EditText) mRootView.findViewById(R.id.et_password);
		TextView forgotPasswordTextView = (TextView) mRootView
				.findViewById(R.id.tv_forgot_password);
		ImageView btn_back = (ImageView) mRootView
				.findViewById(R.id.action_bar_back_btn);
		TextView lofinButtonTextView = (TextView) mRootView
				.findViewById(R.id.tv_login_button);

		TextView title_text = (TextView) mRootView
				.findViewById(R.id.action_bar_title_text);
		title_text.setText("Login");
		emailEditText.setFocusable(true);
		pwdEditText.setOnEditorActionListener(this);
		if (nowUsername != null)
			emailEditText.setText(nowUsername);
		lofinButtonTextView.setOnClickListener(this);
		forgotPasswordTextView.setOnClickListener(this);
		btn_back.setOnClickListener(this);
		mRootView.findViewById(R.id.tv_open_signup).setOnClickListener(this);
		mRootView.setOnClickListener(this);
		((RegisterUserActivity) getActivity())
				.setLoginFailListener(mLoginFailListener);

		mRootView.findViewById(R.id.iv_login_fb_stub).setOnClickListener(this);
		// mRootView.findViewById(R.id.iv_login_gplus).setOnClickListener(this);
		mRootView.findViewById(R.id.iv_login_twitter).setOnClickListener(this);
		llTwitterPresent = (LinearLayout) mRootView
				.findViewById(R.id.ll_non_fb_login);
		llTwitterAbsent = (LinearLayout) mRootView
				.findViewById(R.id.ll_fb_gplus_login);

		Controller.registerGAEvent("SigninFragment");
		checkTwitterPresence(mRootView);

		return mRootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		// getKeyboardForcefully(emailEditText);
	}

	private LoginFailListener mLoginFailListener = new LoginFailListener() {

		@Override
		public void onLoginFailed(int errorCode, String errorMessage) {
			if (errorCode == ErrorCode.USER_NOT_FOUND.getErrorCode())
				Controller.setEditTextError(emailEditText, emailErrorTextView,
						errorMessage);
			else if (errorCode == ErrorCode.UNAUTHORISED_ACCESS.getErrorCode())
				Controller.setEditTextError(pwdEditText, pwdErrorTextView,
						errorMessage);
			else
				Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT)
						.show();

		}
	};

	@Override
	public void onClick(View v) {
		//Remove keyboard first
		
		removeKeyboardForcefully(emailEditText);
		
		switch (v.getId()) {
		case R.id.tv_login_button:
			if (isAdded() && Controller.isNetworkConnected(getActivity())){
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(
						MixpanelUtils.LOGIN_ATTEMPT, 
						Constant.ScreenSpecs.SCREEN_LOGIN) ; 
				mayBeGetAndSetFeilds();
		
			}
			break;
		case R.id.tv_forgot_password:
			if(isAdded()){
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(
						MixpanelUtils.NAVIGATION_FORGOT_PASSWORD, 
						Constant.ScreenSpecs.SCREEN_LOGIN) ; 
					Intent myIntent = new Intent(getActivity(),
							ForgotPasswordActivity.class);
					getActivity().startActivity(myIntent);
					
			     }
			break;
		case R.id.tv_open_signup:
			if(isAdded()){
					openSignUpScreen();
					((RegisterUserActivity) getActivity()).registerMixpanelEvent(
							MixpanelUtils.NAVIGATION_SIGNUP_SCREEN, 
							Constant.ScreenSpecs.SCREEN_LOGIN) ; 
			}
			break;
		case R.id.iv_login_fb_new:
		case R.id.iv_login_fb_stub:
			if (isAdded() && Controller.isNetworkConnectedWithMessage(getActivity())) {
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(MixpanelUtils.FB_LOGIN,
						Constant.ScreenSpecs.SCREEN_LOGIN) ;
				Intent resultIntent = new Intent();

				resultIntent
						.putExtra(Constant.Key.LOGIN_FACEBOOK_FRANKLY, true);
				getActivity().setResult(Activity.RESULT_OK, resultIntent);
				getActivity().finish();
				
			}
			break;
		case R.id.iv_login_gplus_new:
		case R.id.iv_login_gplus:
			if (isAdded() && Controller.isNetworkConnectedWithMessage(getActivity())) {
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(MixpanelUtils.GPLUS_LOGIN,
						Constant.ScreenSpecs.SCREEN_LOGIN) ;
				Intent resultIntent = new Intent();

				resultIntent.putExtra(Constant.Key.LOGIN_GOOGLE_PLUS_FRANKLY,
						true);
				getActivity().setResult(Activity.RESULT_OK, resultIntent);
				getActivity().finish();
				
			}
			break;
		case R.id.iv_login_twitter:
			if (isAdded() && Controller.isNetworkConnectedWithMessage(getActivity())) {
				((RegisterUserActivity) getActivity()).registerMixpanelEvent(MixpanelUtils.TWITTER_LOGIN,
						Constant.ScreenSpecs.SCREEN_LOGIN) ;
				Intent resultIntent = new Intent();

				resultIntent.putExtra(Constant.Key.LOGIN_TWITTER_FRANKLY, true);
				getActivity().setResult(Activity.RESULT_OK, resultIntent);
				getActivity().finish();
				
			}
			break;
		case R.id.action_bar_back_btn:
			getActivity().onBackPressed();
			break;
		}
	}

	public void getKeyboardForcefully(final View et) {
		et.requestFocus();
		et.postDelayed(new Runnable() {
			public void run() {
				InputMethodManager keyboard = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.showSoftInput(et, 10);
			}
		}, 50);
	}

	public void removeKeyboardForcefully(View et) {
		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(et.getWindowToken(), 0);

	}

	private void mayBeGetAndSetFeilds() {
		nowUsername = null;
		nowPassword = null;
		String str = emailEditText.getText().toString().trim();
		nowPassword = pwdEditText.getText().toString().trim();
		if (str == null || str.length() == 0) {
			Controller.showToast(getActivity(),
					"Please enter a valid username or email.");
			return;
		}
		nowUsername = str;

		if (nowPassword == null || nowPassword.length() == 0)
			Controller.showToast(getActivity(), "Please enter password");
		else if (nowPassword == null || nowPassword.length() < 6)
			Controller.showToast(getActivity(),
					"Password must be atleast 6 chars long");
		else {
			// TO DO class cast exception as getActivity is RegisterUserActivity
			/*
			 * ((NewLoginActivity) getActivity()).onUsernameAndPasswordSelected(
			 * nowUSername, nowPassword);
			 */

			Intent resultIntent = new Intent();
			resultIntent.putExtra("username", nowUsername);
			resultIntent.putExtra("password", nowPassword);

			resultIntent.putExtra(Constant.Key.LOGIN_EMAIL_FRANKLY, true);
			getActivity().setResult(Activity.RESULT_OK, resultIntent);
			getActivity().finish();

		}

	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_DONE
				|| actionId == EditorInfo.IME_ACTION_NEXT)
			mayBeGetAndSetFeilds();
		return false;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((RegisterUserActivity) getActivity()).removeLoginFailListener();
	}

	public static interface LoginFailListener {
		void onLoginFailed(int errorCode, String errorMessage);
	}

	protected void openSignUpScreen() {
		Fragment f = new SignUpFragment();
		Bundle args = new Bundle();
		args.putBoolean(SignUpFragment.KEY_SHOW_PASSWORD_FEILD, true);
		f.setArguments(args);
		getActivity()
				.getSupportFragmentManager()
				.beginTransaction()
				.setCustomAnimations(R.drawable.pull_up_from_bottom,
						R.drawable.push_out_to_bottom, R.drawable.pop_enter_up,

						R.drawable.pop_exit_up)
				.replace(R.id.realtabcontent, f)
				.addToBackStack(null).commit();
	}

	private void checkTwitterPresence(View view) {
		if (ShareUtils.findAppByPackageName(ShareUtils.PKG_TWITTER,
				getActivity()) != null) {
			llTwitterPresent.setVisibility(View.VISIBLE);
			llTwitterAbsent.setVisibility(View.GONE);
			view.findViewById(R.id.iv_login_gplus).setOnClickListener(this);
			view.findViewById(R.id.iv_login_fb_stub)
					.setVisibility(View.VISIBLE);
		} else {
			llTwitterAbsent.setVisibility(View.VISIBLE);
			llTwitterPresent.setVisibility(View.GONE);
			view.findViewById(R.id.iv_login_gplus_new).setOnClickListener(this);
			view.findViewById(R.id.iv_login_fb_new).setOnClickListener(this);
			view.findViewById(R.id.iv_login_fb_stub).setVisibility(View.GONE);
		}
	}

}
