package me.frankly.view.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UrlResolver;
import me.frankly.listener.CompressionCompletedListener;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.SingleUniversalUserContainer;
import me.frankly.model.newmodel.UniversalUser;
import me.frankly.servicereceiver.ProfileUploadingService;
import me.frankly.util.FileUtils;
import me.frankly.util.FileUtils.FileType;
import me.frankly.util.ImageUtil;
import me.frankly.util.JsonUtils;
import me.frankly.util.PrefUtils;
import me.frankly.util.ShareUtils;
import me.frankly.util.Utilities;
import me.frankly.util.VideoCompressionTool;
import me.frankly.view.CircularImageView;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.RecordAnswerActivity;
import twitter4j.JSONObject;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class EditProfileFragment extends SDKVideoFragment implements
		OnClickListener {

	private static String TAG = "EditProfileActivity";
	private static final int REQUEST_CODE_PROFILE_PIC = 0;
	private static final int REQUEST_CODE_UPDATE_TEXT = 1;
	private static final int REQUEST_CODE_UPDATE_VIDEO = 2;
	private static final int REQUEST_CODE_CROP_IMAGE = 3;
	private static final int REQUEST_CODE_UPDATE_INFO = 4;
	private EditableProfileObject editprofileObject;
	private UniversalUser mUser;
	private TextView tvName, tvRank;
	private TextView tvInfo;
	private View pauseView;
	private Uri profilePicUri = null;
	private String thumb_path = null;
	private ImageView ivProilepic;
	private ImageView ivCoverPic;
	private String video_path;
	private String profile_path;
	public boolean isDirty;
	private TextView getAskTxtView, ansQuesTxtView;
	private TextureView mTextureView;
	private ImageView loadingView, ivUpload;
	private AnimationDrawable loaderAnimation;
	private boolean isLoaderAnimating = false;
	private LinearLayout askBtnContainer;
	private ImageView iv_play_icon;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#skm EPF:";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + "onCreate", "Called");
		super.onCreate(savedInstanceState);
		mUser = PrefUtils.getCurrentUserAsObject();
		editprofileObject = new EditableProfileObject();
		/*
		 * if (PrefUtils.getisNewUserVisitedProfile())
		 * openProfileEditUserFragment();
		 */
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		fileDebugLog(FILE_DEBUG_TAG + "onCreateView", "Called");
		Log.i(TAG, "onCreate View ");
		View view = inflater.inflate(R.layout.fragment_editprofile_image,
				container, false);

		loadingView = (ImageView) view.findViewById(R.id.iv_loader);
		loadingView.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) loadingView.getDrawable();

		getAskTxtView = (TextView) view.findViewById(R.id.get_asked_txt);
		ansQuesTxtView = (TextView) view.findViewById(R.id.ans_ques_txt);

		getAskTxtView.setOnClickListener(this);
		ansQuesTxtView.setOnClickListener(this);

		mTextureView = (TextureView) view
				.findViewById(R.id.atomic_post_textureview);
		pauseView = view.findViewById(R.id.frag_edit_pauseview);

		view.findViewById(R.id.profile_edit).setOnClickListener(this);
		ivUpload = (ImageView) view.findViewById(R.id.profile_uploadvideo);
		ivUpload.setOnClickListener(this);
		ivCoverPic = (ImageView) view.findViewById(R.id.frag_editprof_coverpic);

		askBtnContainer = (LinearLayout) view
				.findViewById(R.id.edit_ques_container);

		ivProilepic = (ImageView) view.findViewById(R.id.profile_pic);
		ivProilepic.setOnClickListener(this);

		tvName = (TextView) view.findViewById(R.id.proilfe_name);
		tvInfo = (TextView) view.findViewById(R.id.proilfe_info);
		tvRank = (TextView) view.findViewById(R.id.proilfe_rank);
		iv_play_icon = (ImageView) view
				.findViewById(R.id.iv_intro_video_play_icon);
		// mUser = PrefUtils.getCurrentUserAsObject();
		dislpayProfile();
		Controller.registerGAEvent("EditProfileFragment");
		if (!PrefUtils.isVideoSentPending())
			Controller.requestProfile(getActivity(), PrefUtils.getUserID(),
					fetchUserListener);
		return view;
	}

	RequestListener fetchUserListener = new RequestListener() {

		@Override
		public void onRequestStarted() {
		}

		@Override
		public void onRequestError(int errorCode, String message) {

		}

		@Override
		public void onRequestCompleted(Object responseObject) {
			Log.i(TAG, " Fetch User onRequestCompleted in editProf");
			SingleUniversalUserContainer user = (SingleUniversalUserContainer) JsonUtils
					.objectify((String) responseObject,
							SingleUniversalUserContainer.class);
			if (user != null) {
				Log.d("profilevids", ""
						+ user.getUser().getProfile_videos().size());
				if (user.getUser()
						.getProfile_video()
						.equals(PrefUtils.getCurrentUserAsObject()
								.getProfile_video())
						&& !user.getUser()
								.getProfile_videos()
								.equals(PrefUtils.getCurrentUserAsObject()
										.getProfile_videos())) {
					UniversalUser updateUser = PrefUtils
							.getCurrentUserAsObject();
					updateUser.setProfile_videos(user.getUser()
							.getProfile_videos());
					mUser = updateUser;
					PrefUtils.saveCurrentUserAsJson(JsonUtils
							.jsonify(updateUser));
					setVideoview(mUser.getProfile_video() != null
							|| video_path != null);
				}
			}
		}
	};

	private void dislpayProfile() {
		fileDebugLog(FILE_DEBUG_TAG + "dislpayProfile", "Called");
		if (isAdded()) {
			tvName.setText(mUser.getFull_name());
			tvInfo.setText(mUser.getBio() != null ? mUser.getBio() : "");
			if (mUser.getUser_title() != null)
				tvRank.setText(mUser.getUser_title());
			else
				tvRank.setVisibility(View.GONE);
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		fileDebugLog(FILE_DEBUG_TAG + "updateProfile", "Called");
		updateProfile();
		EditableProfileObject pendingEditProfileObject = PrefUtils
				.getPendingEditProfileObject();
		if (pendingEditProfileObject != null
				&& pendingEditProfileObject.getProperVideoForUploading() != null) {
			video_path = pendingEditProfileObject.getProperVideoForUploading();
			thumb_path = pendingEditProfileObject.getCover_picture();
		}
		setVideoview(mUser.getProfile_video() != null || video_path != null);
	}

	private void updateProfile() {
		fileDebugLog(FILE_DEBUG_TAG + "updateProfile", "Called");
		Log.i(TAG, "updateProfile  PROFILE_PICURI  " + profilePicUri);
		Log.i(TAG, "updateProfile  PROFILE_PATH  " + profile_path);
		Log.i(TAG, "updateProfile  VIDEO_URL  " + video_path);
		Log.i(TAG,
				"updateProfile USER PROFILEPIC " + mUser.getProfile_picture());

		String cover_picture = null;
		String profile_picture = null;

		if (thumb_path != null)
			cover_picture = thumb_path;
		else
			cover_picture = mUser.getCover_picture();

		profile_picture = getAbsoluteUrl(mUser.getProfile_picture());

		if (cover_picture != null) {
			/*
			 * ImageLoader.getInstance().displayImage(
			 * getAbsoluteUrl(cover_picture), ivCoverPic);
			 */
			ivUpload.setVisibility(View.GONE);
			askBtnContainer.setVisibility(View.VISIBLE);
			ImageLoader.getInstance().loadImage(getAbsoluteUrl(cover_picture),
					new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String arg0, View arg1) {

						}

						@Override
						public void onLoadingFailed(String arg0, View arg1,
								FailReason arg2) {

						}

						@Override
						public void onLoadingComplete(String arg0, View arg1,
								Bitmap arg2) {
							if (isAdded()) {
								BitmapDrawable ob = new BitmapDrawable(
										getResources(), arg2);
								ivCoverPic.setImageBitmap(arg2);
								getView().findViewById(R.id.rl_edit_parent)
										.setBackgroundDrawable(ob);
							}
						}

						@Override
						public void onLoadingCancelled(String arg0, View arg1) {
						}
					});
		}
		if (profile_path != null) {
			profile_picture = getAbsoluteUrl(profile_path);
		}

		DisplayImageOptions option = CircularImageView
				.getDefaultDisplayOptions(R.drawable.placeholder_profile_person);
		ImageLoader.getInstance().displayImage(getAbsoluteUrl(profile_picture),
				ivProilepic, option);

	}

	private void setVideoview(boolean isVideo) {
		fileDebugLog(FILE_DEBUG_TAG + "setVideoview", "Called");
		if (isVideo) {
			setAutoPlay(false);
			String video_url;
			if (video_path != null)
				video_url = video_path;
			else {
				Log.e("videos list",
						new JSONObject(mUser.getProfile_videos()).toString());
				video_url = getAbsoluteUrl(ImageUtil
						.getNetworkBasedVideoUrl(mUser.getProfile_videos()));
				if (video_url == null) {
					Log.e("videos list", "NetworkBasedVideoUrl  null");
					video_url = mUser.getProfile_video();
				}
			}
			// Log.i(TAG, mUser.getFull_name() + "  Map url  " + video_url);
			setVideoUrl(video_url);
			setInitialView(pauseView);
			setPauseView(pauseView);
			setPostPlayView(pauseView);
			setVideoTextureView(mTextureView);

			mTextureView.setOnClickListener(this);
			mTextureView.setVisibility(View.VISIBLE);
			iv_play_icon.setVisibility(View.VISIBLE);
			pauseView.setOnClickListener(this);
			iv_play_icon.setOnClickListener(this);
		} else {
			mTextureView.setVisibility(View.GONE);
		}

	}

	private void openProfileEditUserFragment() {
		fileDebugLog(FILE_DEBUG_TAG + "openProfileTextEditFragment", "Called");

		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();

		FragmentTransaction transaction = fragmentManager.beginTransaction();
		Fragment fragment = new EditProfileNewUserFragment();

		Bundle bundle = new Bundle();

		bundle.putString(Constant.Key.KEY_USER_BIO, mUser.getBio());
		bundle.putString(Constant.Key.KEY_USER_NAME, mUser.getFull_name());

		fragment.setArguments(bundle);
		fragment.setTargetFragment(this, REQUEST_CODE_UPDATE_INFO);
		transaction.replace(R.id.act_editprofile_frame_l, fragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	@Override
	public void onClick(View view) {
		fileDebugLog(FILE_DEBUG_TAG + "onClick", "Called");
		switch (view.getId()) {
		case R.id.profile_edit:
			pauseVideo();
			openProfileEditUserFragment();
			break;
		case R.id.profile_pic:
			openImageIntent();
			// openCropImageFragment()
			break;

		case R.id.profile_uploadvideo:
			dispatchTakeVideoIntent();
			break;

		case R.id.ab_bb__back_btn:
			getActivity().finish();
			break;

		case R.id.atomic_post_textureview:
		case R.id.frag_edit_pauseview:

		case R.id.iv_intro_video_play_icon:
			togglePlayState();
			break;
		case R.id.get_asked_txt: {
			String text = ShareUtils.getTextToShare(mUser);
			((BaseActivity) getActivity()).shareActivity(this, text, null, null, mUser.getUser_type());
			break;
		}
		case R.id.ans_ques_txt: {
			((BaseActivity) getActivity()).openAnswerQuestionActivity();
			break;
		}
		}
	}

	@SuppressLint("NewApi")
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		fileDebugLog(FILE_DEBUG_TAG + "onActivityResult", "Called");

		Log.i(TAG, "onActivityResult" + requestCode);

		if (requestCode == REQUEST_CODE_UPDATE_TEXT
				&& resultCode == Activity.RESULT_OK) {
			if (data != null) {

				String userName = data
						.getStringExtra(Constant.Key.KEY_USER_NAME);
				String userBio = data.getStringExtra(Constant.Key.KEY_USER_BIO);
				mUser.setBio(userBio);
				mUser.setFirst_name(userName);
				dislpayProfile();
				// upLoadProfile();
			}

		} else if (requestCode == REQUEST_CODE_PROFILE_PIC) {
			Log.d("check", "in pic on activity result");
			if (resultCode == Activity.RESULT_OK) {
				Log.d("check", "result ok");
				isDirty = true;
				if (data != null && data.getData() != null) {
					profilePicUri = data.getData();
					Log.d("check", "URI not null: " + profilePicUri);
				}
				if (ivProilepic != null) {
					ivProilepic.setImageURI(profilePicUri);
				}
				// upLoadProfile();
				openCropImageFragment();
			} else
				profilePicUri = null;

		} else if (requestCode == REQUEST_CODE_UPDATE_VIDEO) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					isDirty = true;
					video_path = data.getStringExtra("video_path");
					Log.i(TAG, "  on result " + video_path);
					// video_path= getAbsoluteUrl(video_path);
					thumb_path = data.getStringExtra("thumb_path");
					Log.d("debug301_onActivityresult", "video " + video_path
							+ " thumb:" + thumb_path);
					Controller.showToast(getActivity(), "Video Uploading");
					upLoadProfile();

				}
			}
		} else if (requestCode == REQUEST_CODE_CROP_IMAGE) {
			Log.i(TAG, "onActivityResult Crop Image");
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					isDirty = true;
					profile_path = data
							.getStringExtra(Constant.Key.CROP_IMAGE_URL);
					profilePicUri = Uri.parse(profile_path);
					Log.i(TAG, "onActivityResult New cropped Uri "
							+ profile_path);
					updateProfile();
					upLoadProfile();

				}
			}

		} else if (requestCode == 4) {
			Log.d("img", "on result 4");
			if (data != null) {
				String userName = data
						.getStringExtra(Constant.Key.KEY_USER_NAME);
				String userBio = data.getStringExtra(Constant.Key.KEY_USER_BIO);
				video_path = data.getStringExtra("video_path");
				thumb_path = data.getStringExtra("tag:editprofilefragment");
				mUser.setBio(userBio);
				mUser.setFirst_name(userName);
				mUser = PrefUtils.getCurrentUserAsObject();
				dislpayProfile();
				updateProfile();
				setVideoview(mUser.getProfile_video() != null
						|| video_path != null);
			}
		}
	}

	private void openImageIntent() {
		fileDebugLog(FILE_DEBUG_TAG + "openImageIntent", "Called");
		final File sdImageMainDirectory = FileUtils.newFile(getActivity(),
				FileType.PROFILE_IMAGE, PrefUtils.getUserID());
		profilePicUri = Uri.fromFile(sdImageMainDirectory);

		// Camera.
		final List<Intent> cameraIntents = new ArrayList<Intent>();
		final Intent captureIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		final PackageManager packageManager = getActivity().getPackageManager();
		final List<ResolveInfo> listCam = packageManager.queryIntentActivities(
				captureIntent, 0);
		for (ResolveInfo res : listCam) {
			final String packageName = res.activityInfo.packageName;
			final Intent intent = new Intent(captureIntent);
			intent.setComponent(new ComponentName(res.activityInfo.packageName,
					res.activityInfo.name));
			intent.setPackage(packageName);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, profilePicUri);
			cameraIntents.add(intent);
		}

		final Intent galleryIntent = new Intent();
		galleryIntent.setType("image/*");
		galleryIntent.setAction(Intent.ACTION_PICK);

		final Intent chooserIntent = Intent.createChooser(galleryIntent,
				"Select Source");

		// Add the camera options.
		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
				cameraIntents.toArray(new Parcelable[] {}));

		startActivityForResult(chooserIntent, REQUEST_CODE_PROFILE_PIC);
	}

	private void dispatchTakeVideoIntent() {
		fileDebugLog(FILE_DEBUG_TAG + "dispatchTakeVideoIntent", "Called");
		Intent takeVideoIntent = new Intent(getActivity(),
				RecordAnswerActivity.class);
		Bundle args = new Bundle();
		args.putString(RecordAnswerActivity.KEY_EDIT_PROFILE, mUser.getId());
		takeVideoIntent.putExtras(args);
		startActivityForResult(takeVideoIntent, REQUEST_CODE_UPDATE_VIDEO);

	}

	private void upLoadProfile() {
		fileDebugLog(FILE_DEBUG_TAG + "upLoadProfile", "Called");
		Controller.removeCurrentUserFromCache();
		String profilepicpath = profile_path;
		if (profilepicpath == null)
			profilepicpath = Utilities.getPath(profilePicUri, getActivity());

		Log.i("debug_201upload", " Upload Profile  pic uro  " + profilePicUri);
		Log.i("debug_201upload", " Upload Profile  pic  " + profilepicpath);
		Log.i("debug_201upload", " Upload Profile video  " + video_path);
		Log.i("debug_201upload", "Upload Profile Cover " + thumb_path);

		editprofileObject.setId(PrefUtils.getUserID());
		editprofileObject.setProfile_picture(profilepicpath);
		editprofileObject.setProfile_video(video_path);
		editprofileObject.setCover_picture(thumb_path);
		PrefUtils.setPendingEditProfileObject(editprofileObject);
		PrefUtils.setIsVideoSentPending(true);

		File compressedVideoFile = FileUtils.newFile(getActivity(),
				FileType.VIDEO_COMPRESSED, PrefUtils.getUserID());
		final String compressedVideoPath = compressedVideoFile
				.getAbsolutePath();

		if (Build.VERSION.SDK_INT >= 16 && video_path != null) {
			Log.e("debug_201didwriteData", "uploadeing with compression ");
			new VideoCompressionTool().processOpenVideo(video_path,
					compressedVideoPath, null, getActivity(),
					new CompressionCompletedListener() {

						@Override
						public void didWriteData(File file, Context context,
								boolean last, boolean error) {
							Log.e(TAG, "didwriteData error= " + error);
							if (error) {
								FileUtils.delete(compressedVideoPath);
							} else {
								editprofileObject
										.setCompressedVideoUrl(compressedVideoPath);
							}
							editprofileObject.setIsCompressionValid(!error);
							PrefUtils
									.setPendingEditProfileObject(editprofileObject);
							PrefUtils.setIsVideoSentPending(true);
							Intent intent = new Intent(context,
									ProfileUploadingService.class);
							context.startService(intent);
						}
					}, false);

		} else if (video_path != null || profilePicUri != null) {
			Log.e("debug_201didwriteData", "uploadeing without compression ");
			editprofileObject.setIsCompressionValid(false);
			if (isAdded()
					&& Controller.isNetworkConnectedWithMessage(getActivity())) {
				Intent intent = new Intent(getActivity(),
						ProfileUploadingService.class);
				getActivity().startService(intent);
			}
		}
		UniversalUser currentUserAsObject = PrefUtils.getCurrentUserAsObject();
		if (video_path != null && thumb_path != null) {
			currentUserAsObject.setProfile_video(video_path);
			currentUserAsObject.setCover_picture(getAbsoluteUrl(thumb_path));
		}
		if (profilePicUri != null)
			currentUserAsObject
					.setProfile_picture(getAbsoluteUrl(profilepicpath));
		PrefUtils.saveCurrentUserAsJson(JsonUtils.jsonify(currentUserAsObject));
		Log.d("debug_201didwriteData", JsonUtils.jsonify(currentUserAsObject)
				.toString());
		// Controller.removeAllPagesInCache();
		Controller.removeCache(UrlResolver.EndPoints.PROFILE_GET_BY_ID + "/"
				+ PrefUtils.getUserID());
	}

	private String getAbsoluteUrl(String url) {
		fileDebugLog(FILE_DEBUG_TAG + "getAbsoluteUrl", "Called");
		if (url != null && !url.startsWith("file") && !url.startsWith("http"))
			return "file://" + url;

		return url;
	}

	@Override
	protected void hideAllViewsExceptCoverImage() {
		fileDebugLog(FILE_DEBUG_TAG + "hideAllViewsExceptCoverImage", "Called");
		pauseView.setVisibility(View.GONE);
		// getView().findViewById(R.id.rl_edit_parent).setBackgroundColor(
		// Color.BLACK);
	}

	@Override
	public void onPlayerPaused() {
		super.onPlayerPaused();
		ivCoverPic.setVisibility(View.GONE);
	}

	@Override
	protected View getLoadingView(boolean show) {
		fileDebugLog(FILE_DEBUG_TAG + "getLoadingView", "Called");
		if (show && !isLoaderAnimating) {
			loaderAnimation.start();
			isLoaderAnimating = true;
		} else if (!show && isLoaderAnimating) {
			loaderAnimation.stop();
			isLoaderAnimating = false;
		}
		return loadingView;
	}

	private void openCropImageFragment() {
		fileDebugLog(FILE_DEBUG_TAG + "openCropImageFragment", "Called");
		ImageCropFragment fragment = new ImageCropFragment();
		Bundle bundle = new Bundle();

		bundle.putString(Constant.Key.CROP_IMAGE_URL,
				Utilities.getPath(profilePicUri, getActivity()));
		fragment.setArguments(bundle);
		fragment.setTargetFragment(this, REQUEST_CODE_CROP_IMAGE);
		Log.d("check", "opening crop");
		getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.act_editprofile_frame_l, fragment)
				.addToBackStack(null).commit();

	}

	@Override
	public View getDefaultCoverImage() {
		fileDebugLog(FILE_DEBUG_TAG + "getDefaultCoverImage", "Called");
		return null;
	}

	@Override
	protected String[] getUserDetails() {
		fileDebugLog(FILE_DEBUG_TAG + "getUserDetails", "Called");
		return new String[] { mUser.getUsername(),
				String.valueOf(mUser.getUser_type()) };

	}

	private static void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		ivCoverPic.setVisibility(View.VISIBLE);
	}

}