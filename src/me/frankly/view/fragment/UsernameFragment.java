package me.frankly.view.fragment;

import me.frankly.R;
import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.CheckCredAvailabilityRequest;
import me.frankly.util.JsonUtils;
import me.frankly.view.activity.RegisterUserActivity;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class UsernameFragment extends Fragment implements OnClickListener,
		RequestListener, OnEditorActionListener {
	public static final String KEY_FIRST_NAME = "first_name";
	public static final String KEY_LAST_NAME = "last_name";
	private EditText usernameEditText;
	private TextView usernameErrorTextView;
	private String nowUsername = null;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		nowUsername = getTwitterIdFromAccounts();
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*
		 * Tracker t = MyApplication.getDefTracker();
		 * t.setScreenName("Register username screen: " +
		 * ((RegisterUserActivity) getActivity()).mAuthSourceString); t.send(new
		 * HitBuilders.AppViewBuilder().build());
		 */
		View mRootView = inflater.inflate(R.layout.add_info_signup, null);
		usernameEditText = (EditText) mRootView.findViewById(R.id.et_fullname);
		if (nowUsername != null)
			usernameEditText.setText(nowUsername);
		if (getArguments() != null
				&& getArguments().containsKey(KEY_FIRST_NAME))
			usernameEditText.setHint(getArguments().getString(KEY_FIRST_NAME)
					+ getArguments().getString(KEY_LAST_NAME));
		mRootView.findViewById(R.id.tv_next).setOnClickListener(this);
		usernameEditText.setOnEditorActionListener(this);
		((RegisterUserActivity) getActivity()).clearUsername();
		return mRootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_next:
			nowUsername = usernameEditText.getText().toString();
			((RegisterUserActivity) getActivity())
					.onUsernameSelected(nowUsername);
			break;
		}
	}

	@Override
	public void onRequestStarted() {
	}

	@Override
	public void onRequestCompleted(Object responseObject) {
		final CheckCredAvailabilityRequest response = (CheckCredAvailabilityRequest) JsonUtils
				.objectify((String) responseObject,
						CheckCredAvailabilityRequest.class);
		if (isAdded())
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					proceedProcess(response);
				}
			});

	}

	private void proceedProcess(CheckCredAvailabilityRequest response) {
		if (response.getUsername().equals(nowUsername)) {
			if (response.isExists())
				Controller.setEditTextError(usernameEditText,
						usernameErrorTextView,
						"This username has already been taken. Try some other");
			else
				((RegisterUserActivity) getActivity())
						.onUsernameSelected(nowUsername);
		}

	}

	@Override
	public void onRequestError(int errorCode, String message) {
		if (isAdded())
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {

					Toast.makeText(getActivity(), "Some error.. Try later",
							Toast.LENGTH_SHORT).show();
				}
			});

	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_DONE) {

		}
		// mayBeGetAndSetFeilds();
		return false;
	}

	private String getTwitterIdFromAccounts() {
		AccountManager am = AccountManager.get(getActivity()
				.getApplicationContext());
		Account[] accts = am
				.getAccountsByType("com.twitter.android.auth.login");
		if (accts.length > 0)
			return accts[0].name;
		return null;

	}

	
}
