package me.frankly.view.fragment;

import java.util.ArrayList;
import java.util.Collections;

import me.frankly.R;
import me.frankly.adapter.CommentsAdapter;
import me.frankly.config.Constant;
import me.frankly.config.Controller;
import me.frankly.config.UniversalActionListenerCollection;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.CommentObject;
import me.frankly.model.newmodel.CommentsList;
import me.frankly.model.newmodel.IdOnlyResponse;
import me.frankly.model.newmodel.PostObject;
import me.frankly.util.JsonUtils;
import me.frankly.util.MixpanelUtils;
import me.frankly.util.MyUtilities;
import me.frankly.util.PrefUtils;
import me.frankly.view.activity.BaseActivity;
import me.frankly.view.activity.CommentActivity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CommentFragment extends Fragment implements OnClickListener,
		RequestListener, OnScrollListener {

	private PostObject mPostObject = null;
	private View mRootView, btn_back;
	private EditText et_comment;
	private boolean isShown = false;
	private ArrayList<CommentObject> commentObjects;
	private ListView mListView;
	private CommentsAdapter commentsAdapter;
	private int index = 0;
	private ImageView comment_iv_loader, ivPostArrow, ivErrorOrNoComment;
	private boolean isDataLoading = false;
	private AnimationDrawable loaderAnimation;
	public int newCommentCount = 0; // variable to count the number of
									// newComments

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		commentObjects = new ArrayList<CommentObject>();
		commentsAdapter = new CommentsAdapter((BaseActivity)getActivity(), commentObjects);
		Bundle args = getArguments();
		if (args != null && args.containsKey(Constant.Key.KEY_POST_OBJECT)) {
			mPostObject = (PostObject) JsonUtils.objectify(
					args.getString(Constant.Key.KEY_POST_OBJECT),
					PostObject.class);
			getMoreComments();
		}
	}

	/**
	 * Gets next page of comments from Server
	 */
	private void getMoreComments() {
		isDataLoading = true;
		Controller.getComments(getActivity(), mPostObject.getId(),
				String.valueOf(index), this);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("comment_fragment", "onCreateView");
		mRootView = inflater.inflate(R.layout.comments_and_likes_fragment,
				container, false);
		mListView = (ListView) mRootView.findViewById(R.id.lv_comments);

		if (mPostObject == null)
			Toast.makeText(getActivity(),
					"Don't have the post id in arguments ", Toast.LENGTH_SHORT)
					.show();
		ivPostArrow = (ImageView) mRootView.findViewById(R.id.iv_post_arrow);
		ivPostArrow.setOnClickListener(this);

		ivErrorOrNoComment = (ImageView) mRootView
				.findViewById(R.id.iv_error_or_no_comment);

		et_comment = (EditText) mRootView.findViewById(R.id.et_write_comment);
		TextView tv_action_bar_title = (TextView) mRootView
				.findViewById(R.id.action_bar_title_text);
		tv_action_bar_title.setText("Comments");
		btn_back = mRootView.findViewById(R.id.action_bar_back_btn);

		comment_iv_loader = (ImageView) mRootView
				.findViewById(R.id.comment_iv_loader);
		comment_iv_loader.setImageResource(R.drawable.loader_general);
		loaderAnimation = (AnimationDrawable) comment_iv_loader.getDrawable();
		comment_iv_loader.setVisibility(View.VISIBLE);
		loaderAnimation.start();

		et_comment.setOnClickListener(this);
		btn_back.setOnClickListener(this);
		mListView.setOnScrollListener(this);
		mListView.setAdapter(commentsAdapter);

		Controller.registerGAEvent("CommentFragment");

		return mRootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		mRootView.setFocusableInTouchMode(true);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.et_write_comment:
			if (!isShown) {
				isShown = true;
				MyUtilities.showKeyboard(getActivity());
				et_comment.requestFocus();
				et_comment.setFocusableInTouchMode(true);
			} else {
				MyUtilities.hideKeyboard(et_comment, getActivity());
				isShown = false;
			}
			break;
		case R.id.iv_post_arrow:
			if (Controller.isNetworkConnected(getActivity())) {
				if (!et_comment.getText().toString().trim().isEmpty()
						&& et_comment.getText().toString().length() > 0) {
					Log.d("Post Comment", "postComment");
					ivPostArrow.setOnClickListener(null);
					postComment();
				} else {
					Controller.showToast(getActivity(),
							"Enter a comment to post!");
				}
			}
			break;
		case R.id.action_bar_back_btn:
			if (isShown) {
				hideKeyboardandShowListView();
			} else
				backPress();
			break;
		}

	}

	private void hideKeyboardandShowListView() {
		MyUtilities.hideKeyboard(et_comment, getActivity());
		isShown = false;
	}

	private void postComment() {
		final String commentBody = et_comment.getText().toString();
		comment_iv_loader.setVisibility(View.VISIBLE);
		loaderAnimation.start();
		if (isAdded())
			MixpanelUtils.sendGenericEvent(mPostObject.getId(), mPostObject
					.getAnswer_author().getUsername(), String
					.valueOf(mPostObject.getAnswer_author().getUser_type()),
					Constant.ScreenSpecs.SCREEN_COMMENT,
					MixpanelUtils.POST_COMMENT, getActivity());
		Controller.writeAComment(getActivity(), commentBody.toString(),
				mPostObject.getId(), new RequestListener() {

					@Override
					public void onRequestStarted() {
					}

					@Override
					public void onRequestError(int errorCode, String message) {
						if (isAdded())
							getActivity().runOnUiThread(new Runnable() {

								@Override
								public void run() {

									loaderAnimation.stop();
									comment_iv_loader.setVisibility(View.GONE);
									Controller
											.showToast(
													getActivity(),
													"Sorry an error occurred while posting your comment. \n"
															+ "Please try again after some time.");
									ivPostArrow.setOnClickListener(CommentFragment.this);
								}
							});

					}

					@Override
					public void onRequestCompleted(final Object responseObject) {

						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								IdOnlyResponse response = (IdOnlyResponse) JsonUtils
										.objectify((String) responseObject,
												IdOnlyResponse.class);
								newCommentCount = newCommentCount + 1; // increased
								// the
								// comment
								// count
								onCommentAdded(commentBody, response.getId());
								et_comment.setText("");
								loaderAnimation.stop();
								comment_iv_loader.setVisibility(View.GONE);
								hideKeyboardandShowListView();
								UniversalActionListenerCollection.getInstance()
										.onCommentDone(mPostObject.getId(),
												response.getId(),
												newCommentCount);
								ivPostArrow.setOnClickListener(CommentFragment.this);

							}
						});
						Log.d("Post Comment", "onRequestStarted");

					}
				}, null);

	}

	@Override
	public void onRequestStarted() {

	}

	@Override
	public void onRequestCompleted(Object responseObject) {

		if (isAdded()) {
			final CommentsList newPage = (CommentsList) JsonUtils.objectify(
					(String) responseObject, CommentsList.class);
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (newPage.getComments() != null
							&& newPage.getComments().size() > 0) {
						index = newPage.getNext_index();
						appendListWithNewPage(newPage.getComments());
					}

					isDataLoading = false;

					loaderAnimation.stop();
					comment_iv_loader.setVisibility(View.GONE);

					if (commentsAdapter.getCount() == 0) {
						ivErrorOrNoComment.setVisibility(View.VISIBLE);
						ivErrorOrNoComment
								.setImageResource(R.drawable.no_comments_icon);
					} else {
						mListView.setVisibility(View.VISIBLE);
						newCommentCount= commentsAdapter.getCount();
					}
				}
			});
		}

	}

	@Override
	public void onRequestError(final int errorCode, String message) {
		if (isAdded()) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					loaderAnimation.stop();
					comment_iv_loader.setVisibility(View.GONE);
					if (!Controller.isNetworkConnected(getActivity())) {
						ivErrorOrNoComment.setVisibility(View.VISIBLE);
						ivErrorOrNoComment
								.setImageResource(R.drawable.comments_no_internet_icon);
					}
					if (errorCode >= 500 && errorCode <= 505) {
						ivErrorOrNoComment.setVisibility(View.VISIBLE);
						ivErrorOrNoComment
								.setImageResource(R.drawable.comments_server_error_icon);
					}
					Log.e("onReqError", "onRequestError called->" + errorCode);
				}
			});
		}

	}

	protected void appendListWithNewPage(ArrayList<CommentObject> newComments) {
		commentObjects = MyUtilities.appendUniqueItems(commentObjects,
				newComments, MyUtilities.MODE_UPDATE_PREVIOUS);
		Collections.sort(commentObjects);
		commentsAdapter.setData(commentObjects);
		commentsAdapter.notifyDataSetChanged();
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (isNearEnd(firstVisibleItem + visibleItemCount, totalItemCount)
				&& !isDataLoading) {
			getMoreComments();
			Log.e("onscroll", "onscroll called");
		}

	}

	private boolean isNearEnd(int lastVisibleItemPos, int totalItemCount) {
		return totalItemCount - lastVisibleItemPos <= 1;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	private void onCommentAdded(String comment, String id) {
		CommentObject stubCommentObject = new CommentObject();
		stubCommentObject.setBody(comment);
		stubCommentObject.setComment_author(PrefUtils.getCurrentUserAsObject());
		stubCommentObject.setId(id);
		stubCommentObject.setTimestamp(System.currentTimeMillis());
		Log.e("commentcount", "--" + commentsAdapter.getCount());
		if (commentsAdapter.getCount() == 0) {
			ivErrorOrNoComment.setVisibility(View.GONE);

			mListView.setVisibility(View.VISIBLE);
		}
		commentObjects.add(stubCommentObject);
		commentsAdapter.notifyDataSetChanged();
		mListView.setSelection(commentsAdapter.getCount() - 1);

	}

	private void backPress() {
		/*
		 * Intent intent = new Intent(); intent.putExtra(Key.KEY_POST_ID,
		 * mPostObject.getId()); intent.putExtra(Constant.Key.COMMENT_COUNT,
		 * commentObjects.size());
		 */
		Log.i("CommentCount", "Comment count : " + newCommentCount);
		((CommentActivity) getActivity()).updateResult();
		 getActivity().onBackPressed();
	}

}