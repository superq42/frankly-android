package me.frankly.listener;

public interface FragementFocusChangeListener {

	public void onFocusChanged(boolean inFocus);
	public void onPageSelected(boolean inFocus);
}
