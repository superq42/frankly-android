package me.frankly.listener;

public interface RequestListener {

	public void onRequestStarted();

	public void onRequestCompleted(Object responseObject);

	public void onRequestError(final int errorCode, final String message);
}
