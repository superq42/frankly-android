package me.frankly.listener;

public interface QuestionPostCompletionListener {

	void onQuestionPosted(String questionBody, String quesId);
}
