package me.frankly.listener;

public interface ExternalInterruptListener {
	public void onMusicPlayed();

	public void onHeadphoneDisconnected();

	public void onInCall();

	public void onOutCall();
}
