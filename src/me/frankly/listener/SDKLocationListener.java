package me.frankly.listener;

import android.location.Location;

public interface SDKLocationListener {

	public void onLocationUpdated(Location location);

	public void onLocalityFound(String locality, String city, String country);
}
