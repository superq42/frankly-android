package me.frankly.listener;

public interface ChannelSelectedListener {
	public void onChannelSelected(String channelId);
	public void onSearchSelected(String queryString);

}
