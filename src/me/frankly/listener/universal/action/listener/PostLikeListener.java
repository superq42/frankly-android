package me.frankly.listener.universal.action.listener;

public interface PostLikeListener {
	public void onPostLikeChanged(String postId, boolean isLiked,
			int newLikeCount);
}
