package me.frankly.listener.universal.action.listener;

public interface UserFollowListener {
	public void onUserFollowChanged(String userId, boolean isFollowed);
}
