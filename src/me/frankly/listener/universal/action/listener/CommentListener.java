package me.frankly.listener.universal.action.listener;

public interface CommentListener{
	public void onCommentDone(String postId, String newCommentId,
			int newCommentCount);
}
