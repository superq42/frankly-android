package me.frankly.listener;

public interface SwipeAnimationDoneListener {
	public void onSwipeDone();
	public void onSwipeStarted();
}
