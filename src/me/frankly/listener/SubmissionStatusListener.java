package me.frankly.listener;

import android.os.Bundle;

public interface SubmissionStatusListener {

	public void onSubmitted(String postID, boolean success);

	public void onSubmitted(boolean success, String video_path, String thumb_path);

	public void onSubmissionCanceled(Bundle data, int type, boolean success);
}
