package me.frankly.listener;

import java.io.File;

import android.content.Context;

public interface CompressionCompletedListener {
	public void didWriteData(File file, Context context, boolean last, boolean error);

}
