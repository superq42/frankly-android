package me.frankly.listener;

import android.content.Context;
import android.content.Intent;

public interface PendingAnswersProgressListener {

	public void onProgressUpdate(Context ctx, Intent data);
}
