package me.frankly.listener;

public interface NetworkStateChangeListener {

	public void onNetworkStateChanged(int newNetworkState);

}
