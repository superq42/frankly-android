package me.frankly.util;

import me.frankly.R;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;

public class FranklyWallpaperService extends WallpaperService {

	@Override
	public Engine onCreateEngine() {
		return new WallEngine();
	}

	class WallEngine extends Engine {
		private final SurfaceHolder.Callback callback = new SurfaceHolder.Callback() {
			public void surfaceCreated(SurfaceHolder holder) {
				draw();
			}

			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
				draw();
			}

			public void surfaceDestroyed(SurfaceHolder holder) {
			}
		};

		@Override
		public void onCreate(SurfaceHolder surfaceHolder) {
			super.onCreate(surfaceHolder);
			draw();
			surfaceHolder.addCallback(callback);
		}

		@Override
		public void onDestroy() {
			this.getSurfaceHolder().removeCallback(callback);
			super.onDestroy();
		}

		@Override
		public void onVisibilityChanged(boolean visible) {
			if (visible) {
				draw();
			}
		}

		private void draw() {
			if (!isVisible())
				return;
			Canvas c = null;
			try {
				c = getSurfaceHolder().lockCanvas();
				if (c != null) {
					int portrait = c.getHeight() > c.getWidth() ? 0 : 1;
					Bitmap bm = BitmapFactory.decodeResource(getResources(), FILE_USE[portrait]);
					if (bm != null) {
						Matrix matrix = new Matrix();
						matrix.setRectToRect(
								new RectF(0, 0, bm.getWidth(), bm.getHeight()),
								new RectF(0, 0, c.getWidth(), c.getHeight()),
								Matrix.ScaleToFit.CENTER);
						c.drawBitmap(bm, matrix, null);
					}
				}
			} catch (Exception e) {
				Log.e("StaticLiveWallpaperEngine", "Error load bitmap");
			} finally {
				if (c != null)
					getSurfaceHolder().unlockCanvasAndPost(c);
			}
		}
	}

	static final int[] FILE_USE = { R.drawable.frankly_wallpaper_portrait, R.drawable.frankly_walpaper_landscape };
}
