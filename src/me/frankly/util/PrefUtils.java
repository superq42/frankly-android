package me.frankly.util;

import java.util.ArrayList;
import java.util.HashSet;

import me.frankly.config.Constant;
import me.frankly.model.Credentials;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.PendingAnswer;
import me.frankly.model.newmodel.PendingAnswersContainer;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.UniversalUser;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public final class PrefUtils {

	public static final int ID_RANDOM_ASK_FIRST_TIME = 501;
	public static final int ID_PROFILE_VIEW_FIRST_TIME = 502;
	public static final int ID_ASKQUESTION_FRAGMENT_FIRST_TIME = 503;
	public static final int ID_RANDOM_ANSWER_FIRST_TIME = 504;
	public static final int ID_MAIN_APP_FIRST_TIME = 505;
	public static final int ID_DISCOVER_FIRST_TIME = 506;
	public static final int ID_BASE_ASK_FIRST_TIME = 507;
	public static final int ID_BASE_ANSWER_FIRST_TIME = 508;

	private static final String KEY_SHARE_PACKAGE_DETAILS = "sharepackage";
	private static final String KEY_SHARE_VISIT_NUMBER = "shareindex";
	private static final String KEY_NOTICATION_COUNT = "notification_count";
	private static final String KEY_CURRENT_SHARE_INDEX = "currentshareindex";

	private static final String PENDING_UPLOADS = "pending_uploads";

	private static final String VIDEO_PLAY_COUNT = "video_play_count";
	// private static Context context;
	private static SharedPreferences pref;
	private static SharedPreferences prefRandomProfile, prefRandomQuestion;
	private static SharedPreferences prefUploadMediaService;

	private static Editor editor, mEditor, uploadMediaEditor, mQuestionEditor;
	private static String prefUploadMedia = "upload_media";
	private static String prefName = "user_pref";

	private static final String KEY_APP_ACCESS_TOKEN = "appAccessToken";
	public static final String DEFAULT_ACCESS_TOKEN = "NotAnAccessToken";

	private static final String KEY_FACEBOOK_ACCESS_TOKEN = "fbAccessToken";
	public static final String KEY_GOOGLE_PLUS_ACCESS_TOKEN = "gPlusAccessToken";
	private static final String KEY_TWITTER_ACCESS_TOKEN = "twitterAccessToken";
	private static final String KEY_TWITTER_ACCESS_TOKEN_SECRET = "twitterAccessTokenSecret";

	public static final String KEY_CURRENT_CITY = "current_city";
	public static final String KEY_CURRENT_COUNTRY = "current_country";
	public static final String KEY_USER_EMAIL = "userEmail";
	public static final String DEFAULT_EMAIL = "NotAnEmail";

	public static final String KEY_USER_ID = "user_id";
	public static final String DEFAULT_ID = "NotAnId";

	public static final String KEY_USER_NAME = "username";
	protected static final String DEFAULT_USER_NAME = "NotAName";

	public static final String KEY_GCM_ID = "gcm_id";
	public static final String DEFAULT_GCM_ID = "not_a_gcm_id";

	public static final String KEY_SETTING_NOTIF_FOLLOW = "notify_follow";
	public static final String KEY_SETTING_NOTIF_LIKE = "notify_like";
	public static final String KEY_SETTING_NOTIF_COMMENT = "notify_comment";
	public static final String KEY_SETTING_NOTIF_MENTION = "notify_mention";
	public static final String KEY_SETTING_NOTIF_ANSWER = "notify_answer";
	public static final String KEY_SETTING_NOTIF_ASK = "notify_ask";
	public static final String KEY_SETTING_NOTIF_REQUEST = "notify_request";
	public static final String KEY_SETTING_NOTIF_CHAT = "notify_chat";
	public static final String KEY_SETTING_NOTIF_VIBRATE = "notify_vibrate";
	public static final String KEY_CURRENT_USER_ASJSON = "current_user_as_json";

	public static final boolean DEFAULT_SETTING_NOTIF_VALUE = true;

	public static final String KEY_SETTING_IS_ANONYMOUS_QUESTION_ALLOWED = "is_anonymous_allowed";

	private static final String KEY_LOCATION_LAT = "locale_lat";
	private static final String KEY_LOCATION_LON = "locale_lon";

	private static final String KEY_POST_LOG = "post_log";
	private static final String KEY_RANDOM_PROFILE = "random_profile";
	private static final String KEY_RANDOM_QUESTION = "random_question";

	private static final String KEY_NOTIFICATION_TRACKER = "notif_tracker";
	private static final String KEY_FIRST_TIME = "first_time";
	private static final String KEY_APP_LAUNCHED_FIRST_TIME = "app_launched_first_time";

	private static final String KEY_QUICK_RETURN_HEIGHT = "quick_return_height";

	private static final String KEY_IS_GCM_UPDATED = "is_gcm_updated";
	private static final String KEY_EDITPROFILE_OBJECT = "edit_profile_object";
	private static final String KEY_PROFILE_UPLOAD_PENDING = "profile_upload_pending";
	private static final String KEY_PROFILE_SERVICE_STATUS = "profile_service_status";

	private static final String KEY_IS_FIRST_PROFILE_SWIPE_ANIM_SHOWN = "first_profile_swipe_anim_shown";
	private static final String KEY_IS_FIRST_SWIPE_ANIM_SHOWN = "first_swipe_anim_shown";
	private static final String KEY_IS_FIRST_TAP_ANIM_SHOWN = "first_tap_anim_shown";
	private static final String KEY_IS_FIRST_FOLLOW_ANIM_SHOWN = "first_follow_anim_shown";
	private static final String KEY_IS_FIRST_UPVOTE_ANIM_SHOWN = "first_upvote_anim_shown";
	private static final String KEY_IS_FIRST_PROFILE_DROP_ANIM_SHOWN = "first_profile_drop_anim_shown";
	private static final String KEY_IS_FIRST_NOTIFICATION_DROP_ANIM_SHOWN = "first_notification_drop_anim_shown";
	private static final String KEY_IS_FIRST_ANSWER_QUESTION_ANIM_SHOWN = "first_answer_question_anim_shown";
	private static final String KEY_IS_FIRST_RECORD_BUTTON_ANIM_SHOWN = "first_record_button_anim_shown";
	private static final String KEY_IS_FIRST_RECORD_UP_DROP_ANIM_SHOWN = "first_record_up_drop_anim_shown";
	private static final String KEY_IS_FIRST_ASK_ANIM_SHOWN = "first_ask_anim_shown";
	private static final String KEY_IS_FIRST_PROFILE_FOLLOW_ANIM_SHOWN = "first_profile_follow_anim_shown";
	private static final String KEY_IS_FIRST_PROFILE_ASK_ANIM_SHOWN = "first_profile_ask_anim_shown";
	private static final String KEY_IS_FIRST_SHARE_QUESTION_DIALOG_SHOWN = "first_share_question_dialog_shown";
	private static final String KEY_CELEBRITY = "user_celebrity_status";

	public static String KEY_VIDEO_STREAM = "Network_stream";
	public static String KEY_NETWORK_BANDWIDTH = "Network_bandwidth";

	public static final String KEY_REFERAR_USERNAME = "key_referar_username";

	public static final String KEY_IS_PEOPLE_FOLLOWED = "key_is_people_followed";
	public static final String KEY_IS_NEW_USER_VISIT_PROFILE = "key_is_new_user_visit_profile";

	public static final String KEY_APP_OPEN_COUNT = "key_app_open_count";
	public static final String KEY_SHOW_FEEDBACK_FORM = "key_feedback_form";

	public static final String KEY_APP_OPEN_TIME = "app_open_time_stamp";
	public static final String KEY_SHOW_SOFT_UPDATE = "show_soft_update";
	
	private static final String KEY_SHARE_SCREEN_SHOWN = "share_screen_shown_";
	
	/**
	 * Prefs that should never be cleared
	 */
	private static final String KEY_INSTALL_TRACKED = "install_tracked";

	private static String prevVidUrl = null;

	/**
	 * @param ctx
	 */
	public static void init(Context ctx) {
		pref = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE);
		editor = pref.edit();
		editor.commit();
		initUploadMediaPref(ctx);
	}

	/**
	 * @param mCtx
	 */
	public static void initUploadMediaPref(Context mCtx) {
		prefUploadMediaService = mCtx.getSharedPreferences(prefUploadMedia,
				Context.MODE_PRIVATE);
		uploadMediaEditor = prefUploadMediaService.edit();
		uploadMediaEditor.commit();
	}

	/**
	 * @param pInstallTracked
	 *            value for KEY_INSTALL_TRACKED
	 */
	public static void setInstallTracked(boolean pInstallTracked) {
		editor.putBoolean(KEY_INSTALL_TRACKED, pInstallTracked);
		editor.commit();
	}

	/**
	 * @return value of KEY_INSTALL_TRACKED
	 */
	public static boolean isInstallTracked() {
		return pref.getBoolean(KEY_INSTALL_TRACKED, false);
	}

	public static void savePendingUploads(PendingAnswersContainer pendingUploads) {
		String data = JsonUtils.jsonify(pendingUploads);
		uploadMediaEditor.putString(PENDING_UPLOADS, data);
		uploadMediaEditor.apply();
	}

	public static PendingAnswersContainer getPendingUploads() {
		String data = prefUploadMediaService.getString(PENDING_UPLOADS, null);
		if (data != null) {
			return (PendingAnswersContainer) JsonUtils.objectify(data,
					PendingAnswersContainer.class);
		} else {
			return null;
		}
	}

	public static void saveRandomProfile(Context context, UniversalUser mProfile) {
		mEditor.putString(KEY_RANDOM_PROFILE, JsonUtils.jsonify(mProfile));
		mEditor.commit();
	}

	public static String getRandomProfile() {
		return prefRandomProfile.getString(KEY_RANDOM_PROFILE, "");
	}

	public static void resetRandomProfile() {
		Log.i("pref_random", "reset random profile");
		mEditor.remove(KEY_RANDOM_PROFILE);
		mEditor.commit();
	}

	public static void saveRandomQuestion(Context context,
			QuestionObject mQuestion) {

		mQuestionEditor.putString(KEY_RANDOM_QUESTION,
				JsonUtils.jsonify(mQuestion));
		mQuestionEditor.commit();
	}

	public static String getRandomQuestion() {
		return prefRandomQuestion.getString(KEY_RANDOM_QUESTION, "");
	}

	public static void resetRandomQuestion() {
		mQuestionEditor.remove(KEY_RANDOM_QUESTION);
		mQuestionEditor.commit();
	}

	public static final String getAppAccessToken() {
		return pref.getString(KEY_APP_ACCESS_TOKEN, DEFAULT_ACCESS_TOKEN);
		// return "5c2feccf929d6406b8ac3a19b8379fa16f408819";

	}

	public static final void setAppAccessToken(String newAccessToken) {
		editor.putString(KEY_APP_ACCESS_TOKEN, newAccessToken);
		editor.commit();
	}

	public static final void clearAppAccessToken() {
		editor.remove(KEY_APP_ACCESS_TOKEN);
		editor.commit();
	}

	public static final String getFacebookAccessToken() {
		return pref.getString(KEY_FACEBOOK_ACCESS_TOKEN, DEFAULT_ACCESS_TOKEN);
	}

	public static final void setFacebookAccessToken(String newAccessToken) {
		editor.putString(KEY_FACEBOOK_ACCESS_TOKEN, newAccessToken);
		editor.commit();
	}

	public static final void clearFacebookAccessToken() {
		editor.remove(KEY_FACEBOOK_ACCESS_TOKEN);
		editor.commit();
	}

	public static final String getGooglePlusAccessToken() {
		return pref.getString(KEY_GOOGLE_PLUS_ACCESS_TOKEN,
				DEFAULT_ACCESS_TOKEN);
	}

	public static final void setGooglePlusAccessToken(String newAccessToken) {
		editor.putString(KEY_GOOGLE_PLUS_ACCESS_TOKEN, newAccessToken);
		editor.commit();
	}

	public static final void clearGooglePlusAccessToken() {
		editor.remove(KEY_GOOGLE_PLUS_ACCESS_TOKEN);
		editor.commit();
	}

	public static final String getUserEmail() {
		return pref.getString(KEY_USER_EMAIL, DEFAULT_EMAIL);
	}

	public static final void setUserEmail(String newEmail) {
		editor.putString(KEY_USER_EMAIL, newEmail);
		editor.commit();
	}

	public static final void saveCredentials(Credentials credentials) {
		if (credentials.getEmail() != null) {
			editor.putString(KEY_USER_EMAIL, credentials.getEmail());
		}
		editor.putString(KEY_APP_ACCESS_TOKEN, credentials.getAccess_token())
				.commit();
		if (credentials.getUsername() != null) {
			editor.putString(KEY_USER_NAME, credentials.getUsername());
		}
		if (credentials.getFacebook_access_token() != null)
			setFacebookAccessToken(credentials.getFacebook_access_token());
		if (credentials.getGoogle_plus_access_token() != null)
			setGooglePlusAccessToken(credentials.getGoogle_plus_access_token());
		// if(credentials.getTwitter_access_token()!=null)
		// set

		if (credentials.getId() != null)
			editor.putString(KEY_USER_ID, credentials.getId());
		editor.commit();
	}

	public static final String getGcmId() {
		return pref.getString(KEY_GCM_ID, DEFAULT_GCM_ID);
	}

	public static final String getUserID() {
		return pref.getString(KEY_USER_ID, DEFAULT_ID);
	}

	public static final void setGcmId(String newGcmId) {
		editor.putString(KEY_GCM_ID, newGcmId);
		editor.commit();

	}

	public static final String getUsername() {
		return pref.getString(KEY_USER_NAME, DEFAULT_USER_NAME);
	}

	public static final boolean GET_NOTIF_SETTING(String notifSettingKey) {
		return pref.getBoolean(notifSettingKey, DEFAULT_SETTING_NOTIF_VALUE);
	}

	public static final void SET_NOTIF_SETTING(String notifSettingKey,
			boolean notifSettingValue) {
		editor.putBoolean(notifSettingKey, notifSettingValue);
		editor.commit();
	}

	public static boolean isAnonymousQuestionAllowed() {
		return pref.getBoolean(KEY_SETTING_IS_ANONYMOUS_QUESTION_ALLOWED,
				DEFAULT_SETTING_NOTIF_VALUE);
	}

	public static void setAnonymousQuestionAllowed(boolean isAllowed) {
		editor.putBoolean(KEY_SETTING_IS_ANONYMOUS_QUESTION_ALLOWED, isAllowed);
		editor.commit();
	}

	public static void setWalkThrougSeen() {
		setWalkThroughSeen(true);
	}

	public static void setWalkThroughSeen(boolean paramBoolean) {
		editor.putBoolean("is_walkthrough_seen", paramBoolean);
		editor.commit();
	}

	public static boolean isWalkThroughSeen() {
		return pref.getBoolean("is_walkthrough_seen", false);
	}

	public static String getTwitterAccessToken() {
		return pref.getString(KEY_TWITTER_ACCESS_TOKEN, "");
	}

	public static String getTwitterAccessTokenSecret() {
		return pref.getString(KEY_TWITTER_ACCESS_TOKEN_SECRET, "");
	}

	public static void setTwitterAccessToken(String at) {
		editor.putString(KEY_TWITTER_ACCESS_TOKEN, at).commit();
	}

	public static void setTwitterAccessTokenSecret(String ats) {
		editor.putString(KEY_TWITTER_ACCESS_TOKEN_SECRET, ats).commit();
	}

	public static void saveCurrentUserAsJson(String jsonify) {
		editor.putString(KEY_CURRENT_USER_ASJSON, jsonify).commit();
	}

	public static UniversalUser getCurrentUserAsObject() {
		String userJson = pref.getString(KEY_CURRENT_USER_ASJSON, null);
		if (userJson != null) {
			return (UniversalUser) JsonUtils.objectify(userJson,
					UniversalUser.class);
		} else {
			return null;
		}
	}

	public static float[] getLocation() {
		float[] locale = new float[] { pref.getFloat(KEY_LOCATION_LON, 0.0f),
				pref.getFloat(KEY_LOCATION_LAT, 0.0f) };
		return locale;
	}

	public static void setLocation(float lon, float lat) {
		editor.putFloat(KEY_LOCATION_LON, lon).putFloat(KEY_LOCATION_LAT, lat)
				.commit();

	}

	public static void addPostToLog(String post_id) {
		Log.d("postlog", "adding to log: " + post_id);
		HashSet<String> postLogSet = getPostLog();
		if (postLogSet.add(post_id))
			editor.putStringSet(KEY_POST_LOG, postLogSet).commit();
	}

	public static void clearPostLog() {
		editor.remove(KEY_POST_LOG);
	}

	public static HashSet<String> getPostLog() {
		return (HashSet<String>) pref.getStringSet(KEY_POST_LOG,
				new HashSet<String>());
	}

	public static int getNotifId() {
		int notif = pref.getInt(KEY_NOTIFICATION_TRACKER, 1);
		editor.putInt(KEY_NOTIFICATION_TRACKER, notif + 1).commit();
		return notif;
	}

	public static void setIsFirstTime(boolean isFirstTime) {
		editor.putBoolean(KEY_FIRST_TIME, isFirstTime).commit();
	}

	public static boolean isComingFirstTime() {
		return pref.getBoolean(KEY_FIRST_TIME, true);
	}

	public static boolean isAppLaunchedFirstTime(int id) {
		return pref.getBoolean(KEY_APP_LAUNCHED_FIRST_TIME + id, true);
	}

	public static void setAppLaunchedFirstTime(int id, boolean status) {
		editor.putBoolean(KEY_APP_LAUNCHED_FIRST_TIME + id, status).commit();

	}

	public static void setCurrentCity(String city, String country) {
		// TODO Auto-generated method stub
		if (city != null && city.length() > 1) {
			editor.putString(KEY_CURRENT_CITY, city).commit();
		}
		if (country != null && country.length() > 1) {
			editor.putString(KEY_CURRENT_COUNTRY, country).commit();
		}

	}

	public static String getCurrentCity() {
		return pref.getString(KEY_CURRENT_CITY, "");
	}

	public static String getCurrentCountry() {
		return pref.getString(KEY_CURRENT_COUNTRY, "");
	}

	public static void setQuickReturnHeight(int h) {
		editor.putInt(KEY_QUICK_RETURN_HEIGHT, h).commit();
	}

	public static int getQuickReturnHeight() {
		return pref.getInt(KEY_QUICK_RETURN_HEIGHT, 0);
	}

	public static boolean isGcmUpdated() {
		return pref.getBoolean(KEY_IS_GCM_UPDATED, false);
	}

	public static void setGcmUpdated() {
		editor.putBoolean(KEY_IS_GCM_UPDATED, true).commit();
	}

	public static void clearPref() {
		boolean isInstallTracked = isInstallTracked();
		editor.clear().commit();
		setInstallTracked(isInstallTracked);
	}

	public static String getShareList(String key) {
		return pref.getString(KEY_SHARE_PACKAGE_DETAILS + key, null);
	}

	public static void setShareList(String listAsJson, String key) {
		editor.putString(KEY_SHARE_PACKAGE_DETAILS + key, listAsJson);
		editor.commit();
	}

	public static int getShareVisitNumber(String key) {
		return pref.getInt(KEY_SHARE_VISIT_NUMBER + key, -1);
	}

	public static void setShareVisitNumber(int share_index, String key) {
		editor.putInt(KEY_SHARE_VISIT_NUMBER + key, share_index);
		editor.commit();
	}

	public static int getCurrentShareIndex(String key) {
		return pref.getInt(KEY_CURRENT_SHARE_INDEX + key, 0);
	}

	public static void setNotificationCount(int count) {
		editor.putInt(KEY_NOTICATION_COUNT, count);
		editor.commit();
	}

	public static int getNotificationCount() {
		return pref.getInt(KEY_NOTICATION_COUNT, -1);
	}

	public static void setPendingEditProfileObject(
			EditableProfileObject editProfile) {
		if (editProfile == null) {
			editor.remove(KEY_EDITPROFILE_OBJECT);
		} else {
			editor.putString(KEY_EDITPROFILE_OBJECT,
					JsonUtils.jsonify(editProfile));
		}
		editor.commit();
	}

	public static EditableProfileObject getPendingEditProfileObject() {
		String editableProfileJson = pref.getString(KEY_EDITPROFILE_OBJECT,
				null);
		return (EditableProfileObject) JsonUtils.objectify(editableProfileJson,
				EditableProfileObject.class);
	}

	public static void setCurrentShareIndex(int share_index, String key) {
		editor.putInt(KEY_CURRENT_SHARE_INDEX + key, share_index);
		editor.commit();
	}

	public static void clearQuestionsPending() {
		uploadMediaEditor.clear().commit();
	}

	public static boolean isVideoSentPending() {

		return pref.getBoolean(KEY_PROFILE_UPLOAD_PENDING, false);
	}

	public static void setIsVideoSentPending(boolean isPending) {
		editor.putBoolean(KEY_PROFILE_UPLOAD_PENDING, isPending);
		editor.commit();
	}

	public static int getProfileServiceStatus() {
		return pref.getInt(KEY_PROFILE_SERVICE_STATUS,
				MyUtilities.SERVICE_STOPPED);
	}

	public static void setProfileServiceStatus(int value) {
		editor.putInt(KEY_PROFILE_SERVICE_STATUS, value).commit();
	}

	public static boolean getIsFirstSwipeAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_SWIPE_ANIM_SHOWN, false);
	}

	public static void setIsFirstSwipeAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_SWIPE_ANIM_SHOWN, value).commit();
	}

	public static boolean getIsFirstProfileSwipeAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_PROFILE_SWIPE_ANIM_SHOWN, false);
	}

	public static void setIsFirstProfileSwipeAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_PROFILE_SWIPE_ANIM_SHOWN, value)
				.commit();
	}

	public static boolean getIsFirstTapAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_TAP_ANIM_SHOWN, false);
	}

	public static void setIsFirstTapAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_TAP_ANIM_SHOWN, value).commit();
	}

	public static boolean getIsFirstFollowAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_FOLLOW_ANIM_SHOWN, false);
	}

	public static void setIsFirstFollowAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_FOLLOW_ANIM_SHOWN, value).commit();
	}

	public static boolean getIsFirstUpvoteAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_UPVOTE_ANIM_SHOWN, false);
	}

	public static void setIsFirstUpvoteAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_UPVOTE_ANIM_SHOWN, value).commit();
	}

	public static boolean getIsFirstProfileDropAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_PROFILE_DROP_ANIM_SHOWN, false);
	}

	public static void setIsFirstProfileDropAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_PROFILE_DROP_ANIM_SHOWN, value).commit();
	}

	public static boolean getIsFirstNotificationDropAnimShown() {
		return pref
				.getBoolean(KEY_IS_FIRST_NOTIFICATION_DROP_ANIM_SHOWN, false);
	}

	public static void setIsFirstNoificationDropAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_NOTIFICATION_DROP_ANIM_SHOWN, value)
				.commit();
	}

	public static boolean getIsFirstAnswerQuestionAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_ANSWER_QUESTION_ANIM_SHOWN, false);
	}

	public static void setIsFirstAnswerQuestionAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_ANSWER_QUESTION_ANIM_SHOWN, value)
				.commit();
	}

	public static boolean getIsFirstRecordButtonAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_RECORD_BUTTON_ANIM_SHOWN, false);
	}

	public static void setIsFirstRecordButtonAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_RECORD_BUTTON_ANIM_SHOWN, value)
				.commit();
	}

	public static boolean getIsFirstRecordUpDropAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_RECORD_UP_DROP_ANIM_SHOWN, false);
	}

	public static void setIsFirstRecordUpDropAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_RECORD_UP_DROP_ANIM_SHOWN, value)
				.commit();
	}

	public static boolean getIsFirstAskAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_ASK_ANIM_SHOWN, false);
	}

	public static void setIsFirstAskAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_ASK_ANIM_SHOWN, value).commit();
	}

	public static void setUserCelebrityStatus(int value) {
		editor.putInt(KEY_CELEBRITY, value).commit();
	}

	public static boolean isUserCelebrity() {
		return (pref.getInt(KEY_CELEBRITY, UniversalUser.TYPE_REGULAR) == UniversalUser.TYPE_CELEB);
	}

	public static boolean getIsFirstFollowProfileAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_PROFILE_FOLLOW_ANIM_SHOWN, false);
	}

	public static void setIsFirstFollowProfileAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_PROFILE_FOLLOW_ANIM_SHOWN, value)
				.commit();
	}

	public static boolean getIsFirstAskProfileAnimShown() {
		return pref.getBoolean(KEY_IS_FIRST_PROFILE_ASK_ANIM_SHOWN, false);
	}

	public static void setIsFirstAskProfileAnimShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_PROFILE_ASK_ANIM_SHOWN, value).commit();
	}

	public static int getVideoPlayCount() {
		return pref.getInt(VIDEO_PLAY_COUNT, 0);
	}

	public static void setVideoPlayCount(String currVidUrl) {
		if (prevVidUrl != null && currVidUrl != null
				&& !prevVidUrl.equals(currVidUrl)) {
			editor.putInt(VIDEO_PLAY_COUNT, getVideoPlayCount() + 1);
			editor.commit();
			prevVidUrl = currVidUrl;
		} else {
			prevVidUrl = currVidUrl;
		}
	}

	public static boolean getIsFirstShareQuestionDialogShown() {
		return pref.getBoolean(KEY_IS_FIRST_SHARE_QUESTION_DIALOG_SHOWN, false);
	}

	public static void setIsFirstShareQuestionDialogShown(boolean value) {
		editor.putBoolean(KEY_IS_FIRST_SHARE_QUESTION_DIALOG_SHOWN, value)
				.commit();
	}

	public static void setNetworkBandwidth(int value) {
		editor.putInt(KEY_NETWORK_BANDWIDTH, value).commit();
	}

	public static void setVideoStream(int value) {
		editor.putInt(KEY_VIDEO_STREAM, value).commit();
	}

	public static int getNetworkBandwidth() {
		return pref.getInt(KEY_NETWORK_BANDWIDTH,
				NetworkConnectivity.DEFAULT_BANDWIDTH);
	}

	public static int getVideoStream() {
		return pref.getInt(KEY_VIDEO_STREAM, Constant.CONNECTION.FINE_STREAM);
	}

	public static String getRefererUsername() {
		return pref.getString(KEY_REFERAR_USERNAME, null);
	}

	public static void setRefererUsername(String username) {
		Log.d("check", "setting referar username in pref: " + username);
		editor.putString(KEY_REFERAR_USERNAME, username).commit();
	}

	public static void removeRefererUsername() {
		editor.remove(KEY_REFERAR_USERNAME).commit();
	}

	public static boolean isPeopleFollowed() {
		return pref.getBoolean(KEY_IS_PEOPLE_FOLLOWED, false);
	}

	public static void setPeopleFollowed() {
		editor.putBoolean(KEY_IS_PEOPLE_FOLLOWED, true).commit();
	}

	public static boolean getisNewUserVisitedProfile() {
		return pref.getBoolean(KEY_IS_NEW_USER_VISIT_PROFILE, false);
	}

	public static void setisNewUserVisitedProfile(boolean value) {
		editor.putBoolean(KEY_IS_NEW_USER_VISIT_PROFILE, value).commit();
	}

	public static void setAppOpenCount(int count) {
		editor.putInt(KEY_APP_OPEN_COUNT, count).commit();
	}

	public static int getAppOpenCount() {
		return pref.getInt(KEY_APP_OPEN_COUNT, 0);
	}

	public static boolean isFeedbackShown() {
		return pref.getBoolean(KEY_SHOW_FEEDBACK_FORM, false);
	}

	public static void setFeedbackShown(boolean value) {
		editor.putBoolean(KEY_SHOW_FEEDBACK_FORM, value).commit();
	}

	public static void setLastUpdateCheckTime(long currentTime) {
		editor.putLong(KEY_APP_OPEN_TIME, currentTime).commit();
	}

	public static long getLastUpdateCheckTime() {
		return pref.getLong(KEY_APP_OPEN_TIME, -1);
	}

	public static void setSoftUpdate(boolean value) {
		editor.putBoolean(KEY_SHOW_SOFT_UPDATE, value).commit();
	}

	public static boolean getSoftUpdate() {
		return pref.getBoolean(KEY_SHOW_SOFT_UPDATE, false);
	}

	public static void setShareScreenShown(int pShareScreenMode, boolean pValue) {
		String keyScreenMode = KEY_SHARE_SCREEN_SHOWN + pShareScreenMode;
		editor.putBoolean(keyScreenMode, pValue).commit();
	}

	public static boolean isShareScreenShown(int pShareScreenMode) {
		String keyScreenMode = KEY_SHARE_SCREEN_SHOWN + pShareScreenMode;
		return pref.getBoolean(keyScreenMode, false);
	}
	
	/**
	 * @param clientId
	 * @return
	 */
	public static PendingAnswer getPendingAnswer(String clientId) {
		PendingAnswersContainer pendingAnswersContainer = getPendingUploads();
		if (pendingAnswersContainer == null
				|| pendingAnswersContainer.getPendingUploads() == null) {
			return null;
		}
		for (PendingAnswer pendingAnswer : pendingAnswersContainer
				.getPendingUploads()) {
			if (pendingAnswer.getClientId().equals(clientId)) {
				return pendingAnswer;
			}
		}
		return null;
	}

	/**
	 * @param pendingAnswer
	 */
	public static void updatePendingAnswer(PendingAnswer pendingAnswer) {
		PendingAnswersContainer pendingAnswersContainer = getPendingUploads();
		if (pendingAnswersContainer == null
				|| pendingAnswersContainer.getPendingUploads() == null) {
			return;
		}
		ArrayList<PendingAnswer> pendingAnswersList = pendingAnswersContainer
				.getPendingUploads();
		for (int i = 0; i < pendingAnswersList.size(); i++) {
			PendingAnswer tempPA = (PendingAnswer) pendingAnswersList.get(i);
			if (tempPA.getClientId().equals(pendingAnswer.getClientId())) {
				pendingAnswersList.remove(i);
				pendingAnswersList.add(i, pendingAnswer);
			}
		}
		savePendingUploads(pendingAnswersContainer);
	}

	/**
	 * @param pendingAnswer
	 */
	public static void removePendingAnswer(PendingAnswer pendingAnswer) {
		PendingAnswersContainer pendingAnswersContainer = getPendingUploads();
		if (pendingAnswersContainer == null
				|| pendingAnswersContainer.getPendingUploads() == null) {
			return;
		}
		ArrayList<PendingAnswer> pendingAnswersList = pendingAnswersContainer
				.getPendingUploads();
		for (int i = 0; i < pendingAnswersList.size(); i++) {
			PendingAnswer tempPA = (PendingAnswer) pendingAnswersList.get(i);
			if (tempPA.getClientId().equals(pendingAnswer.getClientId())) {
				pendingAnswersList.remove(i);
			}
		}
		savePendingUploads(pendingAnswersContainer);
	}

	/**
	 * @param pAnswerState
	 * @return
	 */
	public static PendingAnswer getNextPendingAnswer(int pAnswerState) {
		PendingAnswersContainer container = getPendingUploads();
		if (container == null || container.getPendingUploads() == null
				|| container.getPendingUploads().isEmpty()) {
			return null;
		}
		ArrayList<PendingAnswer> pendingAnswersList = container
				.getPendingUploads();
		for (PendingAnswer pendingAnswer : pendingAnswersList) {
			if (pendingAnswer.getCurrentState() == pAnswerState) {
				return pendingAnswer;
			}
		}
		return null;
	}

	/**
	 * change state of all pending answers to {@link PendingAnswer#STATE_FAILED}
	 */
	public static void updateAllPendingAnswers() {
		PendingAnswersContainer container = getPendingUploads();
		if (container == null || container.getPendingUploads() == null
				|| container.getPendingUploads().isEmpty()) {
			return;
		}
		ArrayList<PendingAnswer> pendingAnswersList = container
				.getPendingUploads();
		for (PendingAnswer pendingAnswer : pendingAnswersList) {
			pendingAnswer.setCurrentState(PendingAnswer.STATE_FAILED);
		}
		savePendingUploads(container);
	}
}