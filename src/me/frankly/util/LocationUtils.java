package me.frankly.util;

import me.frankly.listener.SDKLocationListener;
import me.frankly.model.newmodel.LocationObject;
import android.location.Location;
import android.util.Log;

public class LocationUtils {
	private static final String LOCATION_UTILS = "location_utils";
	public static final float FEETS_500_IN_METERS = 500.0f;
	public static final float KILOMETERS_3 = 3000f;
	public static final String ACROSS_THE_STREET = "Across the street";
	public static final String BUS_STOPS = "a few blocks away";
	public static final String BY_CAR = "15 mins by car";

	private static float getDistanceInMeters(double latA, double lngA,
			double latB, double lngB) {
		Location locationA = new Location("point A");

		locationA.setLatitude(latA);
		locationA.setLongitude(lngA);

		Location locationB = new Location("point B");

		locationB.setLatitude(latB);
		locationB.setLongitude(lngB);

		float distance = locationA.distanceTo(locationB);
		return distance;
	}

	public static String getProperLocationString(LocationObject location) {
		float[] coordinates = location.getCoordinate_point().getCoordinates();
		float[] location2 = PrefUtils.getLocation();
		String city = "Area 51";

		String location_name = location.getLocation_name();
		if (location_name != null && location_name.length() > 1) {
			int lastIndexOf = location_name.lastIndexOf(',');
			if (lastIndexOf > 0 && lastIndexOf < location_name.length())
				city = location_name.substring(0, lastIndexOf);
			else
				city = location_name;
		}

		Log.e(LOCATION_UTILS, "location  " + coordinates[1] + " "
				+ coordinates[0]);

		if (coordinates[1] == 0.0f && coordinates[1] == 0.0f
				|| (location2[0] == 0.0f && location2[1] == 0.0f)) {
			return city;
		} else {
			float distanceInMeters = getDistanceInMeters(location2[1],
					location2[0], coordinates[1], coordinates[0]);
			return fromMetersToProperStringMessage(distanceInMeters, city);
		}
	}

	private static String fromMetersToProperStringMessage(
			float distanceInMeters, String city) {

		float distanceInMiles = distanceInMeters / 1609.34f;
		if (distanceInMiles < 30) {
			if (distanceInMiles < 1) {
				return "1mi";
			} else {
				return (int) distanceInMiles + "mi";
			}
		} else {
			return city;
		}

	}

	public static void getAddressFromGeoLocation() {
		float[] location = PrefUtils.getLocation();
		Log.d(LOCATION_UTILS, "location to address " + location[0] + " long "
				+ location[1]);
		MyLocationHelper.getAddress(location[1] + "", location[0] + "",
				new SDKLocationListener() {

					@Override
					public void onLocationUpdated(Location location) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onLocalityFound(String locality, String city,
							String country) {
						// TODO Auto-generated method stub
						Log.e(LOCATION_UTILS, " locality " + locality
								+ " city " + city + " country " + country);
					}
				});
	}
}
