package me.frankly.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

public final class RegexUtils {
	
	private static final String REGEX_PATTERN_USERNAME = "[a-zA-Z0-9_]{6,30}";
	private static final String REGEX_PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String REGEX_PATTERN_FULLNAME = "^[\\p{L} .'-]+$";
	
	public static final String getEmailPattern() {
		return REGEX_PATTERN_EMAIL;
	}

	public static final String getUsernamePattern() {
		return REGEX_PATTERN_USERNAME;
	}

	public static final boolean isValidEmail(String inEmail) {
		Log.d("check", "Inemail:" + inEmail);
		return isAMatch(REGEX_PATTERN_EMAIL, inEmail);
	}

	public static final boolean isValidUsername(String inUsername) {
		return isAMatch(REGEX_PATTERN_USERNAME, inUsername);
	}
	
	public static final boolean isValidFullName(String fullName) {
		return isAMatch(REGEX_PATTERN_FULLNAME, fullName);
	}

	private static final boolean isAMatch(String pattern, String input) {

		Pattern tempPattern = Pattern.compile(pattern);
		Matcher tempMatcher = tempPattern.matcher(input);
		return tempMatcher.matches();
	}
}
