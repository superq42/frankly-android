package me.frankly.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
/**
 * Check device's network connectivity and speed
 * 
 * 
 */

public class NetworkConnectivity {

	/**
	 * Get the network info
	 * 
	 * @param context
	 * @return
	 */
	
	public static int ULTRALOW_BANDWIDTH = 0 ; 
	public static int DEFAULT_BANDWIDTH = 200 ; 
	public static int MID_BADWIDTH = 400 ; 
	public static int HIGH_BANDWIDTH = 900 ; 
	
	public static NetworkInfo getNetworkInfo(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo();
	}

	/**
	 * Check if there is any connectivity
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isConnected(Context context) {
		NetworkInfo info = getNetworkInfo(context);
		return (info != null && info.isConnected());
	}

	/**
	 * Check if there is any connectivity to a Wifi network
	 * 
	 * @param context
	 * @param type
	 * @return
	 */
	public static boolean isConnectedWifi(Context context) {
		NetworkInfo info = getNetworkInfo(context);
		return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
	}

	/**
	 * Check if there is any connectivity to a mobile network
	 * 
	 * @param context
	 * @param type
	 * @return
	 */
	public static boolean isConnectedMobile(Context context) {
		NetworkInfo info = getNetworkInfo(context);
		return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
	}

	/**
	 * Check if there is fast connectivity
	 * 
	 * @param context
	 * @return
	 */
	public static int getConnection(Context context) {
		NetworkInfo info = getNetworkInfo(context);
		if (!isConnected(context))
			return 0;
		return isConnectionFast(info.getType(), info.getSubtype());
	}

	/**
	 * Get the connectivity type
	 * 
	 * @param type
	 * @param subType
	 * @return
	 */
	public static int isConnectionFast(int type, int subType) {
		Log.i("MySpeed", "Type " + type + "  subType" + subType);
		if (type == ConnectivityManager.TYPE_WIFI) {
			return NetworkConnectivity.MID_BADWIDTH;
		} else if (type == ConnectivityManager.TYPE_MOBILE) {
			switch (subType) {
			
			// 50 kbps for 2G
			case TelephonyManager.NETWORK_TYPE_1xRTT:
			case TelephonyManager.NETWORK_TYPE_CDMA:
			case TelephonyManager.NETWORK_TYPE_EDGE:
			case TelephonyManager.NETWORK_TYPE_GPRS:
			case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                 return NetworkConnectivity.ULTRALOW_BANDWIDTH ;			
				// 250 kbps for 3G
			case TelephonyManager.NETWORK_TYPE_EVDO_0:
			case TelephonyManager.NETWORK_TYPE_EVDO_A:
			case TelephonyManager.NETWORK_TYPE_HSDPA:
			case TelephonyManager.NETWORK_TYPE_HSPA:
			case TelephonyManager.NETWORK_TYPE_HSUPA:
			case TelephonyManager.NETWORK_TYPE_UMTS:
			case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
			case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
			case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                 return NetworkConnectivity.DEFAULT_BANDWIDTH ; 
				// 450 kbps for 4G
			case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
				 return NetworkConnectivity.HIGH_BANDWIDTH ;  // ~ 10+ Mbps
				// Unknown
			case TelephonyManager.NETWORK_TYPE_UNKNOWN:
			default:
				return NetworkConnectivity.DEFAULT_BANDWIDTH;
			}
		} else {
			return NetworkConnectivity.DEFAULT_BANDWIDTH;
		}
	}

}
