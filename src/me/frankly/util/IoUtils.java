/**
 * 
 */
package me.frankly.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;

/**
 * this class is intended to have only java io related methods.
 */
public class IoUtils {

	/**
	 * @param pInputStream
	 * @return content of pInputStream as a string
	 */
	public static String getStringFromInputStream(InputStream pInputStream) {
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					pInputStream));
			String body = "";
			String content = "";
			while ((body = rd.readLine()) != null) {
				content += body + "\n";
			}
			return content.trim();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
