package me.frankly.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import me.frankly.config.AppConfig;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.coremedia.iso.boxes.Container;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;

public class VideoProcessingUtils {
	
	private static final String TAG = "VIDEO_PROCESSING";
	// TODO Only for testing
	private static final int MAX_GALLERY_VIDEO_SIZE = 100 * 1024 * 1024;
	// 100MB
	public static final int MAX_VIDEO_LENGTH = 91000;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#skm VPU:"; 

	/**
	 * @param pVideoFilePath
	 * @param pProgress
	 * @return
	 */
	public static Bitmap getThumbnail(String pVideoFilePath, int pProgress) {
		if (AppConfig.DEBUG) {
			Log.d(TAG, "getThumbnail(): " + pVideoFilePath);
		}
		Bitmap bmp = null;
		try {
			MediaMetadataRetriever mmd = new MediaMetadataRetriever();
			mmd.setDataSource(pVideoFilePath);
			if (pProgress == -1) {
				pProgress = 1;
			}
			bmp = mmd.getFrameAtTime(pProgress,
					MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
			if (bmp == null) {
				if (AppConfig.DEBUG) {
					Log.d(TAG, "getThumbnail(): null at progress " + pProgress);
				}
				bmp = mmd.getFrameAtTime(8,
						MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
			}
			if (bmp != null) {
				Bitmap bmp2 = getLowResolutionFromBitmap(bmp);
				if (!bmp2.equals(bmp)) {
					bmp.recycle();
				}
				bmp = null;
				bmp = bmp2;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bmp;
	}

	public static double getDuration(Track track) {
		fileDebugLog(FILE_DEBUG_TAG + "getDuration", "Called");
		double duration = 0;
		for (TimeToSampleBox.Entry entry : track.getDecodingTimeEntries()) {
			duration += entry.getCount() * entry.getDelta() / (double) track.getTrackMetaData().getTimescale();
		}
		return duration;
	}

	/**
	 * Function for trimming the movie tracks
	 * 
	 * @param movie
	 * @param startMs
	 * @param endMs
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */

	public static Movie trimUsingMp4Parser(Movie movie, int startMs, double endMs) 
							throws FileNotFoundException, IOException {
		fileDebugLog(FILE_DEBUG_TAG + "trimUsingMp4Parser", "Called");
		// remove all tracks we will create new tracks from the old
		List<Track> tracks = movie.getTracks();
		movie.setTracks(new LinkedList<Track>());
		double startTime = startMs;
		double endTime = endMs;

		for (Track track : tracks) {
			long currentSample = 0;
			double currentTime = 0;
			long startSample = -1;
			long endSample = -1;
			for (int i = 0; i < track.getDecodingTimeEntries().size(); i++) {
				TimeToSampleBox.Entry entry = track.getDecodingTimeEntries().get(i);
				for (int j = 0; j < entry.getCount(); j++) {
					// entry.getDelta() is the amount of time the current sample
					// covers.
					if (currentTime <= startTime) {
						// current sample is still before the new starttime
						startSample = currentSample;
					}
					if (currentTime <= endTime) {
						// current sample is after the new start time and still
						// before the new endtime
						endSample = currentSample;
					} else {
						// current sample is after the end o f the cropped video
						break;
					}
					currentTime += (double) entry.getDelta() / (double) track.getTrackMetaData().getTimescale();
					currentSample++;
				}
			}
			Log.e(TAG, "End Time" + endTime);
			Log.e(TAG, "Start Sample : " + startSample + "End Sample : " + endSample);

			movie.addTrack(new CroppedTrack(track, startSample, endSample));
		}
		return movie;
	}

	public static void mergeVideoSegments(Context context,
			ArrayList<String> videoSegmentNames, String mergedFilePath)
			throws IOException {
		if(AppConfig.DEBUG) {
			Log.d(TAG, "mergeVideoSegments(): " + videoSegmentNames.size());
		}
		/**
		 * Two array list created. inMovies stores the movie segments for
		 * trimming the tracks. The newInMovies stores the trimmed movie
		 * segments MovieCreator.build() is used for getting the segments and
		 * creating movies
		 */
		ArrayList<Movie> inMovies = new ArrayList<Movie>();
		ArrayList<Movie> newInMovies = new ArrayList<Movie>();
		// for (String vidSegName : videoSegmentNames)
		for (int i = 0; i < videoSegmentNames.size(); i++) {
			if (videoSegmentNames.get(i) != null) {
				if(AppConfig.DEBUG) {
					Log.d(TAG, "size: " + videoSegmentNames.size());
					Log.e(TAG, "names: " + videoSegmentNames);
					Log.e(TAG, "path: " + videoSegmentNames.get(i));
					
				}
				inMovies.add(MovieCreator.build(videoSegmentNames.get(i)));
			}
		}

		List<Track> videoTracks = new LinkedList<Track>();
		List<Track> audioTracks = new LinkedList<Track>();

		/**
		 * For trimming the movie tracks
		 */
		double cropTime = 0;

		for (Movie m : inMovies) {
			for (Track t : m.getTracks()) {
				if (t.getHandler().equals("vide")) {
					cropTime = VideoProcessingUtils.getDuration(t);
					newInMovies.add(VideoProcessingUtils.trimUsingMp4Parser(m, 0, cropTime));
				}
			}
		}

		/**
		 * For putting the trimmed tracks in respective audio and video track
		 * list
		 */
		for (Movie m : newInMovies) {
			for (Track t : m.getTracks()) {

				if (t.getHandler().equals("soun")) {
					Log.e(TAG, "audio time" + getDuration(t));
					audioTracks.add(t);
				}
				if (t.getHandler().equals("vide")) {
					Log.e(TAG, "vedio time" + getDuration(t));
					cropTime = (long) getDuration(t);
					videoTracks.add(t);
				}
			}
		}

		Movie result = new Movie();

		if (audioTracks.size() > 0) {
			result.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));
		}
		if (videoTracks.size() > 0) {
			result.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));
		}

		if (result.getTracks() != null && !result.getTracks().isEmpty()) {
			Container out = new DefaultMp4Builder().build(result);

			RandomAccessFile raf = new RandomAccessFile(String.format(mergedFilePath), "rw");
			FileChannel fc = raf.getChannel();
			out.writeContainer(fc);
			fc.close();
			raf.close();
		}
		if (AppConfig.DEBUG) {
			long returnTime = System.currentTimeMillis() / 1000;
			Log.d(TAG, "mergeVideoSegments() Return Time: " + returnTime);
		}
	}

	public static boolean isAValidVideo(Uri mediaURI, Context context) {
		fileDebugLog(FILE_DEBUG_TAG + "isAValidVideo", "Called");
		
		/**
		 * Using MediaDataRetriever we are checking if video is corrupted.
		 * We have no intentions of getting frames from video hare.
		 */
		String tmpVideoPath = getVideoPathFromURI(mediaURI, context);

		Bitmap tmpBmp = getThumbnail(tmpVideoPath, -1);
		if (tmpBmp == null) {
			return false;
		}
		
		try{
		Cursor cursor = MediaStore.Video.query(context.getContentResolver(), mediaURI, new String[] {
			MediaStore.Video.VideoColumns.DURATION,
			MediaStore.Video.VideoColumns.SIZE });
		cursor.moveToFirst();

		long duration = Long.valueOf(cursor.getString(cursor.getColumnIndex("duration")));
		long size = Long.valueOf(cursor.getString(cursor.getColumnIndex("_size")));
		if (duration > MAX_VIDEO_LENGTH) {
			Toast.makeText(context, "Invalid video. Length is more than 90 seconds", Toast.LENGTH_SHORT).show();
			return false;
		} else if (size > MAX_GALLERY_VIDEO_SIZE) {
			Toast.makeText(context, "Invalid video. Size is more than 100 MB", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;}
		catch (Exception e){
			Toast.makeText(context, "Invalid video. Please choose a valid video!", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	public static int getVideoLength(Context context, String filePath) throws FileNotFoundException {
		fileDebugLog(FILE_DEBUG_TAG + "getVideoLength", "Called");
		int videoLength = 0;
		MediaPlayer mp = MediaPlayer.create(context, Uri.parse(filePath));
		if (mp != null) {
			videoLength = mp.getDuration() / 1000;
			mp.reset();
			mp.release();
			return videoLength;
		} else
			throw new FileNotFoundException("File Not Found");
	}

	public static String getVideoPathFromURI(Uri contentUri, Context context) {
		fileDebugLog(FILE_DEBUG_TAG + "getVideoPathFromURI", "Called");
		String[] proj = { MediaStore.Video.Media.DATA };
		Cursor cursor = ((Activity) context).getContentResolver().query(contentUri, proj, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public static Bitmap getLowResolutionFromBitmap(Bitmap croppedImage) {
		fileDebugLog(FILE_DEBUG_TAG + "getLowResolutionFromBitmap", "Called");
		Bitmap finalBmp = null;
		try {
			int reqWidth = croppedImage.getWidth() / 3;
			int reqHeight = croppedImage.getHeight() / 3;
			Log.i(TAG, "reqwid:" + reqWidth);
			Log.i(TAG, "reqheight:" + reqHeight);
			int insampleSize = 1;
			int original_height = croppedImage.getHeight();
			int original_width = croppedImage.getWidth();
			if (original_height > reqHeight || original_width > reqWidth) {
				insampleSize = calculateInSampleSize(reqWidth, reqHeight, original_height, original_width);
			}
			int newheight = original_height / insampleSize;
			int newWidth = original_width / insampleSize;
			Log.i(TAG, "size old " + original_height + "x" + original_width + " new " + newheight + "x" + newWidth);
			finalBmp = Bitmap.createScaledBitmap(croppedImage, newWidth, newheight, false);
			Log.i(TAG, "finalbmp wid:" + finalBmp.getWidth() + "height: " + finalBmp.getHeight());

		} catch (Exception e) {
			Log.i("exception", "exception in decode bitmap");
			e.printStackTrace();
		}
		return finalBmp;
	}

	private static int calculateInSampleSize(int reqWidth, int reqHeight, int height, int width) {
		fileDebugLog(FILE_DEBUG_TAG + "calculateInSampleSize", "Called");
		// Raw height and width of image
		int inSampleSize;
		Log.i(TAG, "inside calcinsamplesize");

		if (width < height) {
			inSampleSize = Math.round((float) height / (float) reqHeight);
		} else {
			inSampleSize = Math.round((float) width / (float) reqWidth);
		}

		return inSampleSize;
	}

	private static void fileDebugLog(String logTag, String logMessage) {
		if(SHOW_FILE_DEBUG_LOGS == true) {
			Log.d(logTag, logMessage);
		}
	}

}
