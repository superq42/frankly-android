package me.frankly.util;

import me.frankly.config.FranklyCrashlytics;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

public final class JsonUtils {

	private static Gson M_GSON = new Gson();

	public static String jsonify(Object object) {
		return M_GSON.toJson(object);
	}

	public static Object objectify(String jsonString, Class<?> cls) {
		try {
			return M_GSON.fromJson(jsonString, cls);
		} catch (JsonSyntaxException e) {
			Log.d("search error"," Custom Json Objectify Error:   Json" 
					+ "  Class " + cls +e.toString());
			FranklyCrashlytics.log(" Custom Json Objectify Error:   Json" + jsonString
					+ "  Class " + cls + e.toString());
		}
		return null;
	}

	public static <cls> Object arrayObjectify(String jsonString, Class<?> cls) {
		return M_GSON.fromJson(jsonString, new TypeToken<cls>() {
		}.getType());
	}
}
