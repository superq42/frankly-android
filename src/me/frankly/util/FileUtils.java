package me.frankly.util;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import me.frankly.R;
import me.frankly.config.AppConfig;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

/**
 * This class has methods for file handling operations.
 * 
 * @note Only this class should be used through application.
 * 
 * @author sachin.gupta
 */
public class FileUtils {

	private final static String LOG_TAG = FileUtils.class.getSimpleName();

	public enum FileType {
		PROFILE_IMAGE, VIDEO_THUMBNAIL, VIDEO_SEGMENT, VIDEO_FINAL, VIDEO_COMPRESSED
	}

	private static final String VIDEO_PREFIX = "vid_";
	private static final String VIDEO_SUFFIX = ".mp4";
	private static final String IMG_PREFIX = "img_";
	private static final String IMG_SUFFIX = ".jpg";

	/**
	 * @return true if external storage is available
	 */
	public static boolean isExternalStorageAvailable() {
		// TODO also check write-able and size
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param context
	 * @param pOnlyExternal
	 * 
	 * @return
	 */
	public static File getFranklyDir(Context pContext, boolean pOnlyExternal) {
		String dirPath = File.separator + pContext.getString(R.string.app_name)
				+ File.separator;
		File root = null;
		if (isExternalStorageAvailable()) {
			root = new File(Environment.getExternalStorageDirectory() + dirPath);
		} else if (!pOnlyExternal) {
			root = new File(pContext.getFilesDir() + dirPath);
		}
		if (root != null) {
			root.mkdirs();
		} else {
			Log.e(LOG_TAG, "getFranklyDir(): " + pOnlyExternal);
		}
		return root;
	}

	/**
	 * @param pContext
	 * @param pFileType
	 * @param pFileName
	 * @return
	 */
	public static File newFile(Context pContext, FileType pFileType,
			String pFileName) {
		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "newFile(): " + pFileType + ", " + pFileName);
		}
		File fileRootDir;
		if (pFileType == FileType.PROFILE_IMAGE) {
			// only profile image can be stored in internal storage
			fileRootDir = getFranklyDir(pContext, false);
		} else {
			// only external storage
			fileRootDir = getFranklyDir(pContext, true);
		}

		if (fileRootDir == null) {
			return null;
		}
		// Create a media file name
		pFileName += System.currentTimeMillis();
		switch (pFileType) {
		case VIDEO_SEGMENT: {
			pFileName = VIDEO_PREFIX + pFileName + VIDEO_SUFFIX;
			break;
		}
		case VIDEO_COMPRESSED: {
			pFileName = VIDEO_PREFIX + "compressed_" + pFileName + VIDEO_SUFFIX;
			break;
		}
		case VIDEO_FINAL: {
			pFileName = VIDEO_PREFIX + "final_" + pFileName + VIDEO_SUFFIX;
			break;
		}
		case PROFILE_IMAGE:
		case VIDEO_THUMBNAIL:
		default: {
			pFileName = IMG_PREFIX + pFileName + IMG_SUFFIX;
			break;
		}
		}
		File file = new File(fileRootDir.getPath() + File.separator + pFileName);

		if (AppConfig.DEBUG) {
			Log.d(LOG_TAG, "newFile(): " + file.getAbsolutePath());
		}
		return file;
	}

	/**
	 * @param src
	 * @param dst
	 */
	public static void copyFile(String src, String dst) {
		FileInputStream inStream = null;
		FileOutputStream outStream = null;
		FileChannel inChannel = null;
		FileChannel outChannel = null;
		try {
			inStream = new FileInputStream(src);
			outStream = new FileOutputStream(dst);
			inChannel = inStream.getChannel();
			outChannel = outStream.getChannel();
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeSafely(inStream);
			closeSafely(outStream);
			closeSafely(inChannel);
			closeSafely(outChannel);
		}
	}

	/**
	 * @param pCloseable
	 */
	private static void closeSafely(Closeable pCloseable) {
		if (pCloseable != null) {
			try {
				pCloseable.close();
			} catch (Exception e) {
				// ignore
			}
		}
	}

	/**
	 * @param pBitmap
	 * @param pFilePath
	 * @return
	 */
	public static boolean createImageFile(Bitmap pBitmap, String pFilePath) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(pFilePath);
			pBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
			out.flush();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			closeSafely(out);
		}
	}
	
	/**
	 * @param pFilePath
	 */
	public static boolean delete(String pFilePath) {
		if (!TextUtils.isEmpty(pFilePath)) {
			return new File(pFilePath).delete();
		}
		return false;
	}
}
