package me.frankly.util;

import java.io.IOException;
import java.util.List;

import me.frankly.config.AppConfig;
import me.frankly.config.ExceptionalPhoneModels;
import me.frankly.config.FranklyCrashlytics;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.AudioEncoder;
import android.media.MediaRecorder.OutputFormat;
import android.media.MediaRecorder.VideoEncoder;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.widget.RelativeLayout;

/**
 * @author Bharat Verma
 * 
 *         This class helps RecordAnswerFramgment in video Recording. This class
 *         suppose to have all the work related to camera and mediarecorder that
 *         is used in RecordAnserFramgent. This class takes whole responsibility
 *         of camera and media-recorder interaction by record answer.
 * 
 */
public class CamcoderUtils implements SurfaceTextureListener {

	private static final String TAG = "camcoder_mine";

	private static final String GRAND_MODEL = "GT-I9082";

	private CameraType selectedCameraType;
	private FlashMode selectedFlashMode;

	public MediaRecorder mMediaRecorder;
	@SuppressWarnings("deprecation")
	public Camera mCamera;
	public String currentAction;
	private SparseIntArray cameraFacingAndIdsMap;
	
	private TextureView textureView;
	private Surface surface;
	private Context context;
	private CamcorderProfile bestProfilesPresent;
	private static final int PHOTO_HEIGHT_THRESHOLD = 900;
	
	private float ratio = 0.0f;
	private float screenRatio;
	private int screenWidth;
	private int screenHeight;
	private int videoHeight;
	private int videoWidth;


	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#skm CCU:"; 


	/**
	 * @param surfaceView1
	 *            : surfaceview that will be used for preview and recording
	 * @param context
	 * 
	 *            This method must be invoked before you can play with anything
	 *            related to camera or recording on RecordAnswerFragment
	 */
	public void init(TextureView textureView1, Context context) {
		fileDebugLog(FILE_DEBUG_TAG + "init", "Called");
		Log.d(TAG, "init called");
		if (mCamera != null) {
			Log.d(TAG, "mCamera INstance null in init()");
			releaseCamera();

		}
		textureView = textureView1;
		bestProfilesPresent = getBestProfilesPresent();

		perceptionFix(context);
		initializeCameraIds();
		if (textureView.getSurfaceTexture() != null)
			surface = new Surface(textureView.getSurfaceTexture());

		textureView.setSurfaceTextureListener(this);
		this.context = context;
	}

	public void perceptionFix(Context context) {
		fileDebugLog(FILE_DEBUG_TAG + "perceptionFix", "Called");
		DisplayMetrics metrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(metrics);
		screenWidth = metrics.widthPixels;
		screenHeight = metrics.heightPixels;

		screenRatio = ((float) metrics.heightPixels / (float) metrics.widthPixels);
		ratio = ((float) bestProfilesPresent.videoFrameWidth / (float) bestProfilesPresent.videoFrameHeight);
		Log.e(TAG, "camcoder  bestprofile "
				+ bestProfilesPresent.videoFrameWidth + "x"
				+ bestProfilesPresent.videoFrameHeight);
		videoHeight = bestProfilesPresent.videoFrameWidth;
		videoWidth = bestProfilesPresent.videoFrameHeight;
		Log.d(TAG, "screen states:" + screenHeight + "x" + screenWidth + "="
				+ screenRatio);
		float vidRatio = ((float) videoHeight / (float) videoWidth);
		boolean needPerceptionFix = ((Math.abs(vidRatio - screenRatio) > 0.01) ? true
				: false);
		Log.d(TAG, "video states:" + videoHeight + "x" + videoWidth + "="
				+ vidRatio + " diff:" + needPerceptionFix);

		if (needPerceptionFix) {
			if (vidRatio > screenRatio) {
				int heightNeeded = (int) (screenWidth * vidRatio);
				int diff = heightNeeded - screenHeight;
				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textureView
						.getLayoutParams();
				layoutParams.setMargins(0, -(diff / 2), 0, -(diff / 2));
				textureView.setLayoutParams(layoutParams);
				textureView.forceLayout();
				// image_thumb.setVisibility(View.VISIBLE);
				// RelativeLayout.LayoutParams layoutParams1 =
				// (RelativeLayout.LayoutParams) image_thumb.getLayoutParams();
				// layoutParams.setMargins(0, -(diff / 2), 0, -(diff / 2));
				// image_thumb.setLayoutParams(layoutParams1);
				// image_thumb.forceLayout();
				// image_thumb.setVisibility(View.GONE);

			} else {
				int widthNeeded = (int) ((float) screenHeight / vidRatio);
				int diff = widthNeeded - screenWidth;
				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textureView
						.getLayoutParams();
				layoutParams.setMargins(-(diff / 2), 0, -(diff / 2), 0);
				textureView.setLayoutParams(layoutParams);
				textureView.forceLayout();
				// image_thumb.setVisibility(View.VISIBLE);
				// RelativeLayout.LayoutParams layoutParams1 =
				// (RelativeLayout.LayoutParams) image_thumb.getLayoutParams();
				// layoutParams.setMargins(-(diff / 2), 0, -(diff / 2), 0);
				// image_thumb.setLayoutParams(layoutParams1);
				// image_thumb.forceLayout();
				// image_thumb.setVisibility(View.GONE);

			}

		}
	}

	public void releaseCamera() {
		fileDebugLog(FILE_DEBUG_TAG + "releaseCamera", "Called");
		// TODO Auto-generated method stub
		Log.d("cameraUtils", "in RecAnswer releaseCamera");
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
			Log.d("cameraUtils", "releaseCamera from RecordAns");
		}
	}

	@SuppressWarnings("deprecation")
	public Size setCameraPreviewSize(Camera camera) {
		fileDebugLog(FILE_DEBUG_TAG + "setCameraPreviewSize", "Called");
		Log.d(TAG, "setCameraPreview called");
		Camera.Parameters params = camera.getParameters();
		// float defaultCameraRatio = (float) params.getPreviewSize().width /
		// (float) params.getPreviewSize().height;
		Size res = null;
		List<Size> sizes = camera.getParameters().getSupportedPreviewSizes();
		// atleast one size will be returned so need need of null check here
		float ratioDiff = 999;
		for (Size s : sizes) {
			Log.i(TAG, "size " + s.width + "x" + s.height);
			float ratio = (float) s.width / (float) s.height;
			if (ratio == this.ratio && s.height <= PHOTO_HEIGHT_THRESHOLD) {
				res = s;
				break;
			}
		}
		// for (Size s : sizes) {
		// Log.i(TAG, "size " + s.width + "x" + s.height);
		// float ratio = (float) s.width / (float) s.height;
		// float diff = Math.abs((screenRatio - ratio));
		// if (diff < ratioDiff) {
		// res = s;
		// ratioDiff = diff;
		// }
		// // if (ratio == this.ra tio && s.height <= PHOTO_HEIGHT_THRESHOLD) {
		// // res = s;
		// // break;
		// // }
		// }
		if (res == null && sizes != null && !sizes.isEmpty()) {
			res = sizes.get(0);
		}
		Log.e(TAG, "final resoultion choosed " + res.height + "x" + res.width);
		params.setPreviewSize(res.width, res.height);
		camera.setParameters(params);
		return res;
	}

	@SuppressWarnings("deprecation")
	public static void setCameraDisplayOrientation(Context activity,
			int cameraId, android.hardware.Camera camera) {
		fileDebugLog(FILE_DEBUG_TAG + "setCameraDisplayOrientation", "Called");
		Log.d(TAG, "setCameraDisplayOrientation called");
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = ((Activity) activity).getWindowManager()
				.getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		camera.setDisplayOrientation(result);
	}

	public void releaseMediaRecorder() {
		fileDebugLog(FILE_DEBUG_TAG + "releaseMediaRecorder", "Called");
		Log.d(TAG, "releaseMediaRecorder called");
		if (mMediaRecorder != null) {
			try {
				mMediaRecorder.reset();
			} catch (Exception e) {
				fileDebugLog(FILE_DEBUG_TAG,
						"Failed to reset media recorder. Release it anyway.");
			}
			mMediaRecorder.release();
			mMediaRecorder = null;
		}
	}

	/**
	 * If device has N cameras, valid Camera Ids are from 0 to N-1
	 * 
	 * @see Camera#getCameraInfo(int, CameraInfo)
	 */
	private void initializeCameraIds() {
		cameraFacingAndIdsMap = new SparseIntArray();
		for (int cameraId = 0; cameraId < Camera.getNumberOfCameras(); cameraId++) {
			CameraInfo ci = new CameraInfo();
			Camera.getCameraInfo(cameraId, ci);
			if (AppConfig.DEBUG) {
				Log.d(TAG, "Camera Facing: " + ci.facing + ", Id: " + cameraId);
			}
			cameraFacingAndIdsMap.put(ci.facing, cameraId);
		}
	}

	/**
	 * @return
	 */
	public boolean hasFrontCamera() {
		return cameraFacingAndIdsMap.indexOfKey(CameraInfo.CAMERA_FACING_FRONT) > 0;
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public void setCamDefaults() {
		if (mCamera == null) {
			getCameraInstance();
		}
		Camera.Parameters mCamParameters = mCamera.getParameters();
		Log.i(TAG, "flash mode selected: " + selectedFlashMode);
		if (selectedCameraType == CameraType.BACK) {
			// set video focus mode
			mCamParameters.setRotation(90);
			mCamParameters
					.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
			mCamParameters.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
			// mCamParameters.setExposureCompensation(0);
			mCamParameters
					.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
			mCamParameters.setRecordingHint(true);
			if (selectedFlashMode == FlashMode.ON) {
				mCamParameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
			} else
				mCamParameters.setFlashMode(Parameters.FLASH_MODE_OFF);
		} else {
			mCamParameters
					.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
			// mCamParameters.setExposureCompensation(0);
			mCamParameters.setRotation(90);
			mCamParameters.setRecordingHint(true);
		}

		boolean isFaultyCam = false;
		String mModel = Build.MODEL;
		for (String faultyModel : ExceptionalPhoneModels
				.getCorruptedGreenVideoModels())
			if (mModel.equals(faultyModel)) {
				isFaultyCam = true;
				break;
			}

		if (isFaultyCam)
			mCamParameters.set("cam_mode", 1);
		try {
			mCamera.setParameters(mCamParameters);
		} catch (Exception e) {
			e.printStackTrace();
			Log.i("exception", "exception: set parameter failed");
		}
	}

	/*
	 * this method is important one. This method set profile and other params on
	 * camcoder/camera. This are done in an order that shouldn't be changed
	 * without expert knowledge about what you are gonna do
	 */

	@SuppressLint("NewApi")
	public boolean prepareMediaRecorder(String pFilePath) {
		if(AppConfig.DEBUG) {
			Log.d(TAG, "prepareMediaRecorder() " + pFilePath);
		}

		if (mCamera == null) {
			mCamera = getCameraInstance();
			setCamDefaults();
			Log.i(TAG, "inside if(mCamera == null)");
		}

		if (mMediaRecorder == null)
			initMediaRecorder();

		try {
			// Step 1: Unlock and set camera to MediaRecorder
			// mCamera.stopPreview();
			mCamera.unlock();
			mMediaRecorder.setCamera(mCamera);

			// Step 2: Set sources

			mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
			mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

			// Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
			if (Build.MODEL.equals(GRAND_MODEL)) {
				mCamera.stopPreview();
				mMediaRecorder.setVideoEncodingBitRate(2234000);
				mMediaRecorder.setAudioSamplingRate(48000);
				mMediaRecorder.setAudioEncodingBitRate(96000);
				mMediaRecorder
						.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
				mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
				mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
			} else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
				CamcorderProfile profile = null;
				if (Camera.getNumberOfCameras() > 0) {
					if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_720P)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_720P);
					} else if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_480P)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_480P);
					} else if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_HIGH)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_HIGH);
					} else if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_QVGA)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_QVGA);
					} else if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_CIF)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_CIF);
					} else {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_LOW);
					}
				} else {
					profile = CamcorderProfile.get(
							CameraInfo.CAMERA_FACING_BACK,
							CamcorderProfile.QUALITY_480P);
				}
				Log.d("record", "setting camcorder profile : "
						+ profile.videoFrameWidth + "x"
						+ profile.videoFrameHeight);

				mMediaRecorder.setProfile(profile);
			} else {
				CamcorderProfile profile = null;
				if (Camera.getNumberOfCameras() > 0) {
					if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_480P)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_480P);
					
					} else if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_720P)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_720P);
					} else if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_HIGH)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_HIGH);
					} else if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_QVGA)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_QVGA);
					} else if (CamcorderProfile.hasProfile(
							CameraInfo.CAMERA_FACING_FRONT,
							CamcorderProfile.QUALITY_CIF)) {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_CIF);
					} else {
						profile = CamcorderProfile.get(
								CameraInfo.CAMERA_FACING_FRONT,
								CamcorderProfile.QUALITY_LOW);
					}
				} else {
					profile = CamcorderProfile.get(
							CameraInfo.CAMERA_FACING_BACK,
							CamcorderProfile.QUALITY_480P);
				}
				Log.d("record", "setting camcorder profile : "
						+ profile.videoFrameWidth + "x"
						+ profile.videoFrameHeight);
				
				mMediaRecorder.setProfile(profile);
			}
			android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
			
			/**
			 * Orientation of video recording according to camera orientation.
			 */
			if (selectedCameraType == CameraType.FRONT) {
				int cameraId = cameraFacingAndIdsMap.get(CameraInfo.CAMERA_FACING_FRONT);
				android.hardware.Camera.getCameraInfo(cameraId, info);
				mMediaRecorder.setOrientationHint(info.orientation);
			} else
				mMediaRecorder.setOrientationHint(90);

			// Step 4: Set output file
			mMediaRecorder.setOutputFile(pFilePath);

			// Step 5: Set the preview output
			// mMediaRecorder.setPreviewDisplay(surface);

			// Step 6: Prepare configured MediaRecorder

			Log.i(TAG, "calling mediarecoder.prepare()");
			mMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			Log.d("TAG",
					"IllegalStateException preparing MediaRecorder: "
							+ e.getMessage());
			Log.i(TAG, "returning false");
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			Log.d("TAG",
					"IOException preparing MediaRecorder: " + e.getMessage());
			Log.i(TAG, "returning false");
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			Log.i(TAG, "Exception while preparing mediarecorder");
			Log.i(TAG, "returning false");
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public void prepareCamera() {
		fileDebugLog(FILE_DEBUG_TAG + "prepareCamera", "Called");
		// TODO Auto-generated method stub
		Log.i(TAG, "prepareCamera" + mCamera);
		if (mCamera != null) {
			try {
				Log.i(TAG, "setting preview display");
				mCamera.setPreviewTexture(textureView.getSurfaceTexture());
				// start camera failed issue
				if (surface != null) {
					mCamera.startPreview();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void showPreview() {
		fileDebugLog(FILE_DEBUG_TAG + "showPreview", "Called");
		// TODO Auto-generated method stub
		getCameraInstance();
		setCamDefaults();
		prepareCamera();

	}

	// public Camera getAppropriateCamera() {
	// int numberOfCameras = Camera.getNumberOfCameras();
	// if (numberOfCameras > 0) {
	// Log.d(TAG, "atleast one camera present NoOfCameras: " + numberOfCameras);
	// Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
	// boolean cameraFound = false;
	// for (int cameraIndex = 0; cameraIndex < numberOfCameras; cameraIndex++) {
	// Camera.getCameraInfo(cameraIndex, cameraInfo);
	// Log.d(TAG, "camera info facing " + cameraInfo.facing);
	// if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
	// Log.d(TAG, "front faceing camera found");
	// cameraFound = true;
	// try {
	//
	// Camera camera = Camera.open(cameraIndex);
	// setCameraDisplayOrientation(context, cameraIndex, camera);
	// setCameraPreviewSize(camera);
	// mCamera = camera;
	// return camera;
	// } catch (RuntimeException e) {
	// Log.e(TAG, "exception returning front camera " + e.getMessage());
	// e.printStackTrace();
	// }
	// } else {
	//
	// }
	// }
	// if (!cameraFound) {
	// Log.e(TAG, "front faceing camera NOT found");
	// try {
	// Camera camera = Camera.open(0);
	// setCameraDisplayOrientation(context, 0, camera);
	// setCameraPreviewSize(camera);
	// mCamera = camera;
	// return camera;
	// } catch (RuntimeException e) {
	// Log.e(TAG, "exception returning front camera " + e.getMessage());
	// e.printStackTrace();
	// }
	//
	// }
	//
	// } else
	// Log.e(TAG, "No camera found  ");
	//
	// return null;
	//
	// }

	@SuppressLint("NewApi")
	private Camera getCameraInstance() {
		fileDebugLog(FILE_DEBUG_TAG + "getCameraInstance", "Called");
		Log.d("cameraUtils- recans", "getCameraInstance called ");
		if (mCamera != null)
			releaseCamera();
		mCamera = null;
		try {
			int cameraId;
			if (selectedCameraType == CameraType.BACK) {
				cameraId = cameraFacingAndIdsMap.get(CameraInfo.CAMERA_FACING_BACK);
			} else {
				cameraId = cameraFacingAndIdsMap.get(CameraInfo.CAMERA_FACING_FRONT);
			}
			mCamera = Camera.open(cameraId); // attempt to get a Camera instance
			// Setting proper orientation of camera preview.
			setCameraDisplayOrientation(context, cameraId, mCamera);

		} catch (Exception e) {
			Log.i("TAG", "Camera not available");
			e.printStackTrace();
			// Camera is not available (in use or does not exist)
			/**
			 * Applying logs for debugging in crashlytics.
			 */
			if (selectedCameraType == CameraType.BACK) {
				FranklyCrashlytics.log("BACK Camera not available.");
			} else {
				FranklyCrashlytics.log("FRONT Camera not available.");
			}
		}
		if (mCamera != null && !(Build.MODEL.equals(GRAND_MODEL)))
			setCameraPreviewSize(mCamera);
		return mCamera; // returns null if camera is unavailable
	}

	/**
	 * @return bEST(based on Priority list) pROFILE PRESENT on that perticular
	 *         device
	 */
	public CamcorderProfile getBestProfilesPresent() {
		fileDebugLog(FILE_DEBUG_TAG + "getBestProfilesPresent", "Called");
		Log.d(TAG, "getAllProfilesPresent  called");
		CamcorderProfile profile = null;
		if (CamcorderProfile.hasProfile(CameraInfo.CAMERA_FACING_FRONT,
				CamcorderProfile.QUALITY_720P)) {
			profile = CamcorderProfile.get(CameraInfo.CAMERA_FACING_FRONT,
					CamcorderProfile.QUALITY_720P);
			Log.e("profile_found--Q_720P", getProfileData(profile));

		} else if (CamcorderProfile.hasProfile(CameraInfo.CAMERA_FACING_FRONT,
				CamcorderProfile.QUALITY_480P)) {
			profile = CamcorderProfile.get(CameraInfo.CAMERA_FACING_FRONT,
					CamcorderProfile.QUALITY_480P);
			Log.e("profile_found--Q_480P", getProfileData(profile));
		} else if (CamcorderProfile.hasProfile(CameraInfo.CAMERA_FACING_FRONT,
				CamcorderProfile.QUALITY_HIGH)) {
			profile = CamcorderProfile.get(CameraInfo.CAMERA_FACING_FRONT,
					CamcorderProfile.QUALITY_HIGH);
			Log.e("profile_found--Q_high", getProfileData(profile));

		} else if (CamcorderProfile.hasProfile(CameraInfo.CAMERA_FACING_FRONT,
				CamcorderProfile.QUALITY_QVGA)) {
			profile = CamcorderProfile.get(CameraInfo.CAMERA_FACING_FRONT,
					CamcorderProfile.QUALITY_QVGA);
			Log.e("profile_found--QVGA", getProfileData(profile));
		} else if (CamcorderProfile.hasProfile(CameraInfo.CAMERA_FACING_FRONT,
				CamcorderProfile.QUALITY_CIF)) {
			profile = CamcorderProfile.get(CameraInfo.CAMERA_FACING_FRONT,
					CamcorderProfile.QUALITY_CIF);
			Log.e("profile_found--CIF", getProfileData(profile));
		} else if (CamcorderProfile.hasProfile(CameraInfo.CAMERA_FACING_FRONT,
				CamcorderProfile.QUALITY_LOW)) {
			profile = CamcorderProfile.get(CameraInfo.CAMERA_FACING_FRONT,
					CamcorderProfile.QUALITY_LOW);
			Log.e("profile_found--LOW", getProfileData(profile));
		} else if (CamcorderProfile.hasProfile(CameraInfo.CAMERA_FACING_BACK,
				CamcorderProfile.QUALITY_HIGH)) {
			profile = CamcorderProfile.get(CameraInfo.CAMERA_FACING_BACK,
					CamcorderProfile.QUALITY_HIGH);
			Log.e("profile_found--CAMERA_FACING_BACK--HIGH",
					getProfileData(profile));
		} else {
			profile = CamcorderProfile.get(CameraInfo.CAMERA_FACING_BACK,
					CamcorderProfile.QUALITY_LOW);
			Log.e("profile_found--CAMERA_FACING_BACK--LOW",
					getProfileData(profile));
		}

		return profile;

	}

	public void getPreviewSizePresent(Camera camera) {
		fileDebugLog(FILE_DEBUG_TAG + "getPreviewSizePresent", "Called");
		Log.d(TAG, "getPreviewSizePresent  called");
		Camera.Parameters params = camera.getParameters();
		Size res = null;
		List<Size> sizes = camera.getParameters().getSupportedPreviewSizes();
		// atleast one size will be returned so need need of null check here

		for (Size s : sizes) {
			float ratio = ((float) s.width / (float) s.height);
			Log.i(TAG + "-PreviewSize", "size " + s.width + "x" + s.height
					+ " =" + ratio);

		}
	}

	/**
	 * @param camcorderProfile
	 * @return just for loggiing the data related to a profile.. can be avoided
	 */
	String getProfileData(CamcorderProfile camcorderProfile) {
		fileDebugLog(FILE_DEBUG_TAG + "getProfileData", "Called");
		if (camcorderProfile != null) {
			String profileInfo = "\n\n-----------------------------new profile --------------";
			profileInfo = " : \n" + camcorderProfile.toString() + "\n";
			profileInfo += "videoFrameWidth: "
					+ String.valueOf(camcorderProfile.videoFrameWidth)
					+ "x"
					+ String.valueOf(camcorderProfile.videoFrameHeight)
					+ " ="
					+ ((float) camcorderProfile.videoFrameWidth / (float) camcorderProfile.videoFrameHeight)
					+ "\n" + "audioBitRate: "
					+ String.valueOf(camcorderProfile.audioBitRate) + "\n"
					+ "audioChannels: "
					+ String.valueOf(camcorderProfile.audioChannels) + "\n"
					+ "audioCodec: "
					+ getAudioCodec(camcorderProfile.audioCodec) + "\n"
					+ "audioSampleRate: "
					+ String.valueOf(camcorderProfile.audioSampleRate) + "\n"
					+ "duration: " + String.valueOf(camcorderProfile.duration)
					+ "\n" + "fileFormat: "
					+ getFileFormat(camcorderProfile.fileFormat) + "\n"
					+ "quality: " + String.valueOf(camcorderProfile.quality)
					+ "\n" + "videoBitRate: "
					+ String.valueOf(camcorderProfile.videoBitRate) + "\n"
					+ "videoCodec: "
					+ getVideoCodec(camcorderProfile.videoCodec) + "\n"
					+ "videoFrameRate: "
					+ String.valueOf(camcorderProfile.videoFrameRate) + "\n";
			profileInfo += "\n\n\n--------------";
			// return profileInfo;
			return "videoFrameWidth: "
					+ String.valueOf(camcorderProfile.videoFrameWidth)
					+ "x"
					+ String.valueOf(camcorderProfile.videoFrameHeight)
					+ " ="
					+ ((float) camcorderProfile.videoFrameWidth / (float) camcorderProfile.videoFrameHeight);

		} else {
			return "profile is null ";
		}
	}

	/**
	 * @param audioCodec
	 * @return just for logging
	 */
	public String getAudioCodec(int audioCodec) {
		fileDebugLog(FILE_DEBUG_TAG + "getAudioCodec", "Called");
		switch (audioCodec) {
		case AudioEncoder.AMR_NB:
			return "AMR_NB";
		case AudioEncoder.DEFAULT:
			return "DEFAULT";
		default:
			return "unknown";
		}
	}

	/**
	 * @param fileFormat
	 * @return just for logging
	 */
	public String getFileFormat(int fileFormat) {
		fileDebugLog(FILE_DEBUG_TAG + "getFileFormat", "Called");
		switch (fileFormat) {
		case OutputFormat.MPEG_4:
			return "MPEG_4";
		case OutputFormat.RAW_AMR:
			return "RAW_AMR";
		case OutputFormat.THREE_GPP:
			return "THREE_GPP";
		case OutputFormat.DEFAULT:
			return "DEFAULT";
		default:
			return "unknown";
		}
	}

	/**
	 * @param videoCodec
	 * @return just for logging
	 */
	public String getVideoCodec(int videoCodec) {
		fileDebugLog(FILE_DEBUG_TAG + "getVideoCodec", "Called");
		switch (videoCodec) {
		case VideoEncoder.H263:
			return "H263";
		case VideoEncoder.H264:
			return "H264";
		case VideoEncoder.MPEG_4_SP:
			return "MPEG_4_SP";
		case VideoEncoder.DEFAULT:
			return "DEFAULT";
		default:
			return "unknown";
		}
	}

	public void initMediaRecorder() {
		fileDebugLog(FILE_DEBUG_TAG + "initMediaRecorder", "Called");
		Log.i(TAG, "inside init mediarecorder");
		if (mMediaRecorder == null) {
			mMediaRecorder = new MediaRecorder();
			Log.i(TAG, "mediarecorder: " + mMediaRecorder);
		}
	}

	public Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		fileDebugLog(FILE_DEBUG_TAG + "getOptimalPreviewSize", "Called");
		final double ASPECT_TOLERANCE = 0.2;
		double targetRatio = (double) w / h;
		if (sizes == null) {
			Log.d(TAG, "sizes are null");

			return null;
		}

		Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		// Try to find an size match aspect ratio and size
		for (Size size : sizes) {
			Log.d("Camera-size", "Checking size " + size.width + "w "
					+ size.height + "h");
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		// Cannot find the one match the aspect ratio, ignore the
		// requirement
		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		Log.d(TAG, "final size returning " + optimalSize.height + " + "
				+ optimalSize.width);
		return optimalSize;
	}

	public void setSurfaceView(TextureView textureView) {
		fileDebugLog(FILE_DEBUG_TAG + "setSurfaceView", "Called");
		this.textureView = textureView;
		if (textureView.getSurfaceTexture() != null)

			surface = new Surface(textureView.getSurfaceTexture());
		// svHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		textureView.setSurfaceTextureListener(this);
	}

	public void removeSurfaceHolderCallback(/* Callback callBack */) {
		fileDebugLog(FILE_DEBUG_TAG + "removeSurfaceHolderCallback", "Called");
		/*
		 * if (svHolder != null) svHolder.removeCallback(callBack);
		 */
		if (textureView != null)
			textureView.setSurfaceTextureListener(null);
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
			int height) {
		fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureAvailable", "Called");
		// TODO Auto-generated method stub
		Log.d(TAG, "surfaceCreated  called");
		this.surface = new Surface(surface);
		if (mCamera == null) {
			mCamera = getCameraInstance();
			setCamDefaults();
			getPreviewSizePresent(mCamera);
		}
		if (mCamera != null) {
			Log.d("cameraUtils- recans", "surfaceCreated camera not null");
			prepareCamera();
		} else
			Log.d("cameraUtils- recans", "surfaceCreated camera null");

	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureDestroyed", "Called");
		releaseCamera();
		return false;
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
			int height) {
		fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureSizeChanged", "Called");
		// TODO Auto-generated method stub

	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
		// TODO Auto-generated method stub
		// fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureUpdated", "Called");

	}

	private static void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS == true) {
			Log.d(logTag, logMessage);
		}
	}

	/**
	 * @param textureView
	 *            the textureView to set
	 */
	public void setTextureView(TextureView textureView) {
		this.textureView = textureView;
	}

	/**
	 * @return the cameraType
	 */
	public CameraType getCameraType() {
		return selectedCameraType;
	}

	/**
	 * @param cameraType
	 *            the cameraType to set
	 */
	public void setCameraType(CameraType cameraType) {
		this.selectedCameraType = cameraType;
	}

	/**
	 * @return the flashMode
	 */
	public FlashMode getFlashMode() {
		return selectedFlashMode;
	}

	/**
	 * @param flashMode
	 *            the flashMode to set
	 */
	public void setFlashMode(FlashMode flashMode) {
		this.selectedFlashMode = flashMode;
	}

	public enum CameraType {
		FRONT, BACK
	}

	public enum FlashMode {
		OFF, ON
	}
}