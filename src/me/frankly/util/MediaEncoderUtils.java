package me.frankly.util;

import android.annotation.TargetApi;
import android.media.MediaCodecInfo;
import android.media.MediaCodecInfo.CodecCapabilities;
import android.media.MediaCodecInfo.CodecProfileLevel;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.util.Pair;

import java.util.HashMap;

import com.google.android.exoplayer.util.MimeTypes;


/**
 * A utility class for querying the available codecs.
 */
@TargetApi(16)
public class MediaEncoderUtils {

  private static final HashMap<String, Pair<MediaCodecInfo, CodecCapabilities>> codecs =
      new HashMap<String, Pair<MediaCodecInfo, CodecCapabilities>>();
  
  public final static String MIME_TYPE = "video/avc";

  /**
   * Get information about the decoder that will be used for a given mime type. If no decoder
   * exists for the mime type then null is returned.
   *
   * @param mimeType The mime type.
   * @return name of the encoder that will be used, or null if no encoder exists.
   */
  public static String getEncoderInfo(String mimeType) {
    Pair<MediaCodecInfo, CodecCapabilities> info = getMediaCodecInfo(mimeType);
    if (info == null) {
      return null;
    }
    return info.first.getName();
  }

  /**
   * Returns the best encoder and its capabilities for the given mimeType. If there's no encoder
   * returns null.
   */
  private static synchronized Pair<MediaCodecInfo, CodecCapabilities> getMediaCodecInfo(
      String mimeType) {
    Pair<MediaCodecInfo, CodecCapabilities> result = codecs.get(mimeType);
    if (result != null) {
      return result;
    }
    int numberOfCodecs = MediaCodecList.getCodecCount();
    // Note: MediaCodecList is sorted by the framework such that the best decoders come first.
    for (int i = 0; i < numberOfCodecs; i++) {
      MediaCodecInfo info = MediaCodecList.getCodecInfoAt(i);
      String codecName = info.getName();
      if (info.isEncoder() && isOmxCodec(codecName)) {
        String[] supportedTypes = info.getSupportedTypes();
        for (int j = 0; j < supportedTypes.length; j++) {
          String supportedType = supportedTypes[j];
          if (supportedType.equalsIgnoreCase(mimeType)) {
            result = Pair.create(info, info.getCapabilitiesForType(supportedType));
            codecs.put(mimeType, result);
            return result;
          }
        }
      }
    }
    return null;
  }

  private static boolean isOmxCodec(String name) {
    return name.startsWith("OMX.");
  }

  /**
   * @param profile An AVC profile constant from {@link CodecProfileLevel}.
   * @param level An AVC profile level from {@link CodecProfileLevel}.
   * @return Whether the specified profile is supported at the specified level.
   */
  public static boolean isH264ProfileSupported(int profile, int level) {
    Pair<MediaCodecInfo, CodecCapabilities> info = getMediaCodecInfo(MimeTypes.VIDEO_H264);
    if (info == null) {
      return false;
    }

    CodecCapabilities capabilities = info.second;
    for (int i = 0; i < capabilities.profileLevels.length; i++) {
      CodecProfileLevel profileLevel = capabilities.profileLevels[i];
      if (profileLevel.profile == profile && profileLevel.level >= level) {
        return true;
      }
    }

    return false;
  }
  
  public static MediaFormat getOutputFromatForProfile(int profile,int level, int width, int height) {
	  MediaFormat outputFormat = null;
	  if(isH264ProfileSupported(profile,level)) {
		  outputFormat = MediaFormat.createVideoFormat(MIME_TYPE, width, height);
//		  outputFormat.setInteger(MediaFormat.KEY_PROFILE, new CodecProfileLevel(profile,level));
	  }
	  
	  return outputFormat;
  }

}
