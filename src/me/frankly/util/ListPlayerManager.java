package me.frankly.util;

import me.frankly.SDKPlayer.IPlayer;
import me.frankly.SDKPlayer.IPlayer.IPlayerStateChangedListener;
import me.frankly.adapter.FeedsListAdapter;
import me.frankly.adapter.FeedsListAdapter.VideoViewHolder;
import android.content.Context;
import android.os.Handler;
import android.view.View;

/**
 * A class that manages a single {@link IPlayer} for all of the Videos in
 * {@link FeedsListAdapter}
 * 
 * @author abhishek_inoxapps
 * 
 */
public class ListPlayerManager implements IPlayerStateChangedListener {

	// private String mScreen;
	private IPlayer player;
	private VideoViewHolder currentViewHolder;
	private Handler mHandler;
	private Context mCtx;
	private String currentUrl;
	private boolean isPlaying = false;
	private boolean isPrepared = false;

	public ListPlayerManager(Context ctx, IPlayer player, String screen,
			Handler handler) {
		this.player = player;
		// this.mScreen = screen;
		this.mHandler = handler;
		this.mCtx = ctx;
	}

	/**
	 * Use this method to get most out of the {@link ListPlayerManager}
	 * 
	 * @param url
	 * @param viewHolder
	 */
	public void togglePlayState(String url, VideoViewHolder viewHolder) {
		if (url.equals(currentUrl)) {
			if (isPrepared) {
				if (isPlaying)
					pauseVideo();
				else
					resumeVideo();
			}
		} else {
			stopCurrentVideo();
			playVideo(url, viewHolder);
		}
	}

	private void playVideo(String url, VideoViewHolder viewHolder) {
		currentUrl = url;
		currentViewHolder = viewHolder;
		isPlaying = false;
		isPrepared = false;
		player.prepareUrl(mCtx, currentUrl, viewHolder.videoTextureView,
				mHandler, this, true, true);
	}

	public void stopCurrentVideo() {
		resetPlayer();
		currentUrl = null;
		currentViewHolder = null;
		isPlaying = false;
		isPrepared = false;
	}

	private void resumeVideo() {
		player.resumeVideo();
		isPlaying = true;
		onPlayerResumed();
	}

	private void pauseVideo() {
		player.pauseVideo();
		isPlaying = false;
		onPlayerPaused();
	}

	private void resetPlayer() {
		if (player != null)
			player.resetSelf();
	}

	private void hideView(View v) {
		if (v != null && v.getVisibility() == View.VISIBLE)
			v.setVisibility(View.VISIBLE);
	}

	private void showView(View v) {
		if (v != null && v.getVisibility() != View.VISIBLE)
			v.setVisibility(View.VISIBLE);
	}

	@Override
	public void onPlayerPrepared() {
		isPrepared = true;
		isPlaying = true;
		hideView(currentViewHolder.loadingView);
		hideView(currentViewHolder.initView);

	}

	@Override
	public void onPlayerCompleted() {
		showView(currentViewHolder.postCompletionView);
		isPlaying = false;

	}

	@Override
	public void onPlayerPaused() {
		showView(currentViewHolder.pauseView);
	}

	@Override
	public void onPlayerResumed() {
		hideView(currentViewHolder.pauseView);

	}

	@Override
	public void onPlayerPausedToBuffer() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPlayerResumedAfterBuffer() {
		// TODO Auto-generated method stub

	}
	@Override
	public void onPlayerError() {
		// TODO Auto-generated method stub
		
	}

}
