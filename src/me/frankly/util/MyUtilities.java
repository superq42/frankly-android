package me.frankly.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.model.newmodel.UniqueEntity;

import org.apache.http.HttpResponse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.nostra13.universalimageloader.core.assist.ImageSize;

public class MyUtilities {

	private static final int ONE_HOUR = 3600;
	private static final int ONE_DAY = 3600 * 24;
	public static int TYPE_NOT_CONNECTED = 0;
	public static int SERVICE_STARTED = 1;
	public static int SERVICE_STOPPED = 0;

	public static int TYPE_MOBILE = 1;
	public static int TYPE_WIFI = 2;

	private static ArrayList<String> tagsOneLiners = new ArrayList<String>();
	private static final String TAG_CACHE = "my_cache";
	// updating previous object with this.
	public static final int MODE_UPDATE_PREVIOUS = 502;
	// delete previous object and add this object at latest location
	public static final int MODE_DELETE_PREVIOUS = 501;
	// ignore repeated objects in new list.
	public static final String PKG_TWITTER = "com.twitter.android";
	public static final String PKG_WHATSAPP = "com.whatsapp";
	public static final String PKG_FACEBOOK = "com.facebook.katana";

	private static final int MODE_IGNORE_NEW = 503;
	public static final int MODE_PULL_TO_REFRESH = 504;
	public static int SCREEN_WIDTH, SCREEN_HEIGHT;
	private static ImageSize coverImageSizeObject;

	static {
		tagsOneLiners.add("Likes everything worth like");
		tagsOneLiners.add("Enjoys all the good stuff");
		tagsOneLiners.add("My interests are a national secret");
		tagsOneLiners.add("Even the NSA is figuring out what I like");
		tagsOneLiners.add("Ask me what I like");
		tagsOneLiners.add("Askers get to know my interests");
		tagsOneLiners.add("Follow to know me better");
	}

	public static ArrayList<String> getTagOneLiners() {
		return tagsOneLiners;
	}

	public static String getRandomTagOneLiners() {

		Random r = new Random();
		int position = r.nextInt(tagsOneLiners.size() - 1);
		return tagsOneLiners.get(position);
	}

	public static void shareFacebook() {

	}

	public static void shareWhatsApp(Activity context, String data) {

		Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
		whatsappIntent.setType("text/plain");
		whatsappIntent.setPackage("com.whatsapp");
		whatsappIntent.putExtra(Intent.EXTRA_TEXT, data);
		try {
			context.startActivity(whatsappIntent);
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(context, "Whatsapp have not been installed",
					Toast.LENGTH_SHORT).show();
		}

	}

	public static void shareTwitter() {

	}

	public static void shareYouTube() {

	}

	public static void shareOthers() {

	}

	public static int getConnectivityStatus(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
				return TYPE_WIFI;

			if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
				return TYPE_MOBILE;
		}
		return TYPE_NOT_CONNECTED;
	}

	public static File lessResolution(String filePath, int width, int height) {
		int reqHeight = width;
		int reqWidth = height;
		BitmapFactory.Options options = new BitmapFactory.Options();

		// First decode with inJustDecodeBounds=true to check dimensions
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		// Calculate inSampleSize
		if (options.outHeight > reqHeight || options.outWidth > reqWidth) {
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);
		}

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		File f = new File(Environment.getExternalStorageDirectory()
				+ File.separator + "test.jpg");
		if (bitmap != null) {
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

			// you can create a new file name "test.jpg" in sdcard folder.
			FileOutputStream fo = null;
			try {
				f.createNewFile();
				fo = new FileOutputStream(f);
				fo.write(bytes.toByteArray());
				fo.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				bitmap.recycle();

			}
		} else
			Log.e("crop", "bitmap null");
		return f;
	}

	private static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {

		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}

	public static int pxToDp(Context mCtx, int px) {
		DisplayMetrics displayMetrics = mCtx.getResources().getDisplayMetrics();
		int dp = Math.round(px
				/ (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return dp;
	}

	public static int dpToPx(Context mCtx, int dp) {
		DisplayMetrics displayMetrics = mCtx.getResources().getDisplayMetrics();
		int px = Math.round(dp
				* (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	public static String toCamelCase(String inputString) {
		String result = "";
		if (inputString.length() == 0) {
			return result;
		}
		char firstChar = inputString.charAt(0);
		char firstCharToUpperCase = Character.toUpperCase(firstChar);
		result = result + firstCharToUpperCase;
		for (int i = 1; i < inputString.length(); i++) {
			char currentChar = inputString.charAt(i);
			char previousChar = inputString.charAt(i - 1);
			if (previousChar == ' ') {
				char currentCharToUpperCase = Character
						.toUpperCase(currentChar);
				result = result + currentCharToUpperCase;
			} else {
				char currentCharToLowerCase = Character
						.toLowerCase(currentChar);
				result = result + currentCharToLowerCase;
			}
		}
		return result;
	}

	private static HashMap<String, Typeface> typefaces = new HashMap<String, Typeface>();

	public static Typeface getTypeface(Context ctx, String typefaceName) {
		if (!typefaces.containsKey(typefaceName)) {
			Typeface tempTypeface = Typeface.createFromAsset(ctx.getAssets(),
					typefaceName + ".ttf");
			typefaces.put(typefaceName, tempTypeface);
		}
		return typefaces.get(typefaceName);

	}

	public static void shareText(String textToShare, String subject,
			Context context) {
		Intent i = new Intent(android.content.Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
		i.putExtra(android.content.Intent.EXTRA_TEXT, textToShare);
		context.startActivity(Intent.createChooser(i, "Share via"));
	}

	public static String getPostLink(String post_id) {
		return "http://frankly.me/post/" + post_id;
	}

	public static String getImagePathFromURI(Context context, Uri contentUri) {

		String[] proj = { MediaStore.Images.Media.DATA };

		Cursor cursor = ((Activity) context).managedQuery(contentUri, proj,
				null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public static String getRealPathFromURINew(Uri filetypeuri, Context context) {
		if (filetypeuri != null) {
			String path = filetypeuri.toString();
			if (path.toLowerCase().startsWith("file://")) {
				// Selected file/directory path is below
				path = (new File(URI.create(path))).getAbsolutePath();
			}
			return path;
		} else {
			return null;
		}

	}

	public static String getRealPathFromContentURI(Context context,
			Uri contentUri) {
		Cursor cursor = null;
		try {
			Log.e("getRealPathFromUri", "try");
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null,
					null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			String path = cursor.getString(column_index);
			Log.e("getRealPathFromUri", "try " + path);
			return path;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public static String createThumbUrl(String media_url) {

		int last = media_url.lastIndexOf(".");
		String extension = media_url.substring(last);
		String url = media_url.substring(0, last) + "_thumb" + extension;
		Log.d("thumb", "url calculated " + url);
		return url;

	}

	public static Bitmap decodemyBitmap(Context context, String imgPath,
			int reqWidth, int reqHeight) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		try {
			// BitmapFactory.decodeStream(context.getContentResolver()
			// .openInputStream(imgUri), null, options);

			BitmapFactory.decodeFile(imgPath, options);

			Log.i("decode_bitmap", "outheight:" + options.outHeight);
			Log.i("decode_bitmap", "outWidth:" + options.outWidth);
			Log.i("decode_bitmap", "reqwid:" + reqWidth);
			Log.i("decode_bitmap", "reqheight:" + reqHeight);

			if (options.outHeight > reqHeight || options.outWidth > reqWidth) {
				options.inSampleSize = calculateInSampleSize(reqWidth,
						reqHeight, options);
			}
			options.inJustDecodeBounds = false;

			Bitmap resizedBmp = BitmapFactory.decodeFile(imgPath, options);

			Log.i("decode_bitmap", "resizedbmp wid:" + resizedBmp.getWidth()
					+ "height: " + resizedBmp.getHeight());
			Bitmap finalBmp = Bitmap.createScaledBitmap(resizedBmp, reqWidth,
					reqHeight, false);

			Log.i("decode_bitmap", "finalbmp wid:" + finalBmp.getWidth()
					+ "height: " + finalBmp.getHeight());

			return finalBmp;

		} catch (Exception e) {
			Log.i("exception", "exception in decode bitmap");
			e.printStackTrace();
		}
		return null;
	}

	private static int calculateInSampleSize(int reqWidth, int reqHeight,
			BitmapFactory.Options options) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize;
		Log.i("decode_bitmap", "inside calcinsamplesize");

		if (width < height) {
			inSampleSize = Math.round((float) height / (float) reqHeight);
		} else {
			inSampleSize = Math.round((float) width / (float) reqWidth);
		}

		return inSampleSize;
	}

	private static <T extends UniqueEntity> ArrayList<T> appendItems(
			ArrayList<T> firstList, ArrayList<T> secondList, int mode) {
		Log.e(TAG_CACHE, "appendItems");
		HashSet<String> idList = new HashSet<String>(
				firstList.size() > 8 ? firstList.size() : 8);
		ArrayList<T> finalList = new ArrayList<T>(firstList);
		for (UniqueEntity entity : firstList) {
			idList.add(entity.getId());
		}
		Log.e(TAG_CACHE, "ids in first " + idList);
		for (UniqueEntity entity : secondList) {
			if (!idList.contains(entity.getId())) {
				finalList.add((T) entity);
				idList.add(entity.getId());
			} else {

				Log.e(TAG_CACHE, "item already Found " + entity.getId());
				if (mode == MODE_IGNORE_NEW) {
					Log.e(TAG_CACHE, "do nothing mode ");
				} else if (mode == MODE_DELETE_PREVIOUS) {
					Log.e(TAG_CACHE, "delete old object mode ");

					for (UniqueEntity oldEntity : finalList) {
						if (oldEntity.getId().equals(entity.getId())) {

							finalList.remove(oldEntity);
							break;
						}
					}
					finalList.add((T) entity);
					idList.add(entity.getId());
				} else if (mode == MODE_UPDATE_PREVIOUS) {
					Log.e(TAG_CACHE, "delete old object mode ");

					for (UniqueEntity oldEntity : finalList) {
						if (oldEntity.getId().equals(entity.getId())) {
							int indexOf = finalList.indexOf((T) oldEntity);
							finalList.set(indexOf, (T) entity);
							break;
						}
					}
					idList.add(entity.getId());
				}
			}
		}
		return finalList;

	}

	/*
	 * Accepts two list as parameters and returns a list with unique elements
	 * from both of them
	 */

	public static <T extends UniqueEntity> ArrayList<T> appendUniqueItems(
			ArrayList<T> firstList, ArrayList<T> secondList, int mode) {

		if (firstList == null && secondList == null) {
			return null;
		} else if (firstList == null) {

			return appendItems(new ArrayList<T>(), secondList, mode);
		} else if (secondList == null) {

			return appendItems(new ArrayList<T>(), firstList, mode);
		} else {

			firstList = appendItems(new ArrayList<T>(), firstList, mode);
			secondList = appendItems(new ArrayList<T>(), secondList, mode);
			if (mode == MODE_PULL_TO_REFRESH) {
				return appendItems(secondList, firstList, MODE_IGNORE_NEW);
			} else {
				return appendItems(firstList, secondList, mode);
			}

		}
	}

	public static void navigateToProfile(String username, Context context) {
		Log.d("utils", "navigating to profile " + username);
		// if (username != null) {
		// ProfileFragmentNew profileFragment = new ProfileFragmentNew();
		// Bundle args = new Bundle();
		// args.putString(ProfileFragmentNew.KEY_USER_ID, username);
		// profileFragment.setArguments(args);
		// ((FragmentActivity)
		// context).getSupportFragmentManager().beginTransaction().replace(R.id.realtabcontent,
		// profileFragment).addToBackStack(null).commit();
		//
		// }
	}

	public static String getProperTimeString(long timeInSeconds) {
		return getProperTimeString(System.currentTimeMillis() / 1000,
				timeInSeconds);
	}
	
	public static String getProperTimeString(long currentSecs, long timeInSeconds) {
		Log.e("date", "times in sec recieved " + timeInSeconds);
		long diff = currentSecs - timeInSeconds;
		Log.e("date", "times in sec diff " + diff);
		if (diff < 0) {
			return "just now";
		} else if (0 <= diff && diff < ONE_HOUR) {
			// less then an hour

			int min = (int) diff / 60;
			return min + " minutes ago";

		} else if (ONE_HOUR <= diff && diff < ONE_DAY) {
			int hour = (int) diff / 3600;
			return hour + " hours ago";
		} else if (ONE_DAY <= diff && diff < ONE_DAY * 7) {
			int d = (int) diff / ONE_DAY;
			return d + " days ago";
		} else {
			Log.e("date", "else case with diff " + diff);

			Calendar cal = Calendar.getInstance(Locale.US);
			cal.setTimeInMillis(timeInSeconds * 1000);
			Calendar calCurrent = Calendar.getInstance(Locale.US);
			String month = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT,
					Locale.US);
			String dateSting = cal.getDisplayName(Calendar.DATE,
					Calendar.SHORT, Locale.US);
			dateSting = cal.getTime().getDate() + "";
			String yr = cal.getDisplayName(Calendar.YEAR, Calendar.SHORT,
					Locale.US);
			yr = cal.getTime().getYear() + "";
			String yrnow = calCurrent.getDisplayName(Calendar.DATE,
					Calendar.SHORT, Locale.US);
			yrnow = calCurrent.getTime().getYear() + "";
			String finaldate = dateSting + " " + month;

			if (!yrnow.equals(yr)) {
				finaldate += " " + yr;
			}
			return finaldate;

		}
	}

	public static void shareLinkOnFacebookUsingIntent(String urlToShare,
			Context context, String packageName) {

		Intent facebookIntent = findAppByPackageName(packageName, context);
		if (facebookIntent != null) {
			facebookIntent.putExtra(Intent.EXTRA_TEXT, urlToShare);
			context.startActivity(facebookIntent);
		}

	}

	public static Intent findAppByPackageName(String packageName,
			Context context) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		List<ResolveInfo> matches = context.getPackageManager()
				.queryIntentActivities(intent, 0);
		for (ResolveInfo info : matches) {
			if (info.activityInfo.packageName.toLowerCase().startsWith(
					packageName)) {
				intent.setPackage(info.activityInfo.packageName);
				return intent;
			}
		}
		return null;
	}

	public static boolean appInstalledOrNot(String uri, Context context) {
		PackageManager pm = context.getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	public static void likeAPostFacebookOperation(final String post_id,
			Context context) {

		if (checkPermissions()) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					Log.d("likeonfacebook", "permission granted");
					Bundle params = new Bundle();
					// params.putString("type", "object");
					params.putString("object", getPostLink(post_id));
					// params.putString("title", "this is title");
					// params.putString("description",
					// "this is sample description");

					Request request = new Request(Session.getActiveSession(),
							"me/og.likes", params, HttpMethod.POST);
					Response response = request.executeAndWait();
					Log.d("likeonfacebook", response + "");

				}
			}).start();
		} else {
			Log.e("likeonfacebook", "permission notgranted");
			requestPermissions(context);
		}
	}

	public static void unLikeAPostFacebookOperation(String post_id) {
		Request request = new Request(Session.getActiveSession(),
				"{id_from_create_call}", null, HttpMethod.DELETE);

		Response response = request.executeAndWait();
		// handle the response
	}

	public static boolean checkPermissions() {
		Session s = Session.getActiveSession();
		if (s != null) {

			return s.getPermissions().contains("publish_actions");
		} else
			return false;
	}

	public static void requestPermissions(Context context) {
		Log.e("likeonfacebook", "requesting permission ");
		Session s = Session.getActiveSession();
		List<String> PERMISSIONS = new ArrayList<String>();
		PERMISSIONS.add("publish_actions");
		if (s != null)
			s.requestNewPublishPermissions(new Session.NewPermissionsRequest(
					(Activity) context, PERMISSIONS));
		else
			Log.e("likeonfacebook", "session null");
	}

	public static void copyFile(File src, File dst) throws IOException {
		FileInputStream inStream = new FileInputStream(src);
		FileOutputStream outStream = new FileOutputStream(dst);
		FileChannel inChannel = inStream.getChannel();
		FileChannel outChannel = outStream.getChannel();
		inChannel.transferTo(0, inChannel.size(), outChannel);
		inStream.close();
		outStream.close();
	}

	public static String getClientGeneratedPostLink(String client_id) {
		return "http://frankly.me/p/" + client_id;
	}

	public static boolean checkForPackage(Context context, String packageName) {
		PackageManager pm = context.getPackageManager();

		try {
			if (packageName != null) {
				PackageInfo info = pm.getPackageInfo(packageName,
						PackageManager.GET_META_DATA);
				return true;
			} else {
				return false;
			}

		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return false;
		}

	}

	public static Bitmap fastblur(Bitmap sentBitmap, int radius) {

		// Stack Blur v1.0 from
		// http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
		//
		// Java Author: Mario Klingemann <mario at quasimondo.com>
		// http://incubator.quasimondo.com
		// created Feburary 29, 2004
		// Android port : Yahel Bouaziz <yahel at kayenko.com>
		// http://www.kayenko.com
		// ported april 5th, 2012

		// This is a compromise between Gaussian Blur and Box blur
		// It creates much better looking blurs than Box Blur, but is
		// 7x faster than my Gaussian Blur implementation.
		//
		// I called it Stack Blur because this describes best how this
		// filter works internally: it creates a kind of moving stack
		// of colors whilst scanning through the image. Thereby it
		// just has to add one new block of color to the right side
		// of the stack and remove the leftmost color. The remaining
		// colors on the topmost layer of the stack are either added on
		// or reduced by one, depending on if they are on the right or
		// on the left side of the stack.
		//
		// If you are using this algorithm in your code please add
		// the following line:
		//
		// Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>
		Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

		if (radius < 1) {
			return (null);
		}

		int w = bitmap.getWidth();
		int h = bitmap.getHeight();

		int[] pix = new int[w * h];
		Log.e("pix", w + " " + h + " " + pix.length);
		bitmap.getPixels(pix, 0, w, 0, 0, w, h);

		int wm = w - 1;
		int hm = h - 1;
		int wh = w * h;
		int div = radius + radius + 1;

		int r[] = new int[wh];
		int g[] = new int[wh];
		int b[] = new int[wh];
		int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
		int vmin[] = new int[Math.max(w, h)];

		int divsum = (div + 1) >> 1;
		divsum *= divsum;
		int dv[] = new int[256 * divsum];
		for (i = 0; i < 256 * divsum; i++) {
			dv[i] = (i / divsum);
		}

		yw = yi = 0;

		int[][] stack = new int[div][3];
		int stackpointer;
		int stackstart;
		int[] sir;
		int rbs;
		int r1 = radius + 1;
		int routsum, goutsum, boutsum;
		int rinsum, ginsum, binsum;

		for (y = 0; y < h; y++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			for (i = -radius; i <= radius; i++) {
				p = pix[yi + Math.min(wm, Math.max(i, 0))];
				sir = stack[i + radius];
				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);
				rbs = r1 - Math.abs(i);
				rsum += sir[0] * rbs;
				gsum += sir[1] * rbs;
				bsum += sir[2] * rbs;
				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}
			}
			stackpointer = radius;

			for (x = 0; x < w; x++) {

				r[yi] = dv[rsum];
				g[yi] = dv[gsum];
				b[yi] = dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (y == 0) {
					vmin[x] = Math.min(x + radius + 1, wm);
				}
				p = pix[yw + vmin[x]];

				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[(stackpointer) % div];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi++;
			}
			yw += w;
		}
		for (x = 0; x < w; x++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			yp = -radius * w;
			for (i = -radius; i <= radius; i++) {
				yi = Math.max(0, yp) + x;

				sir = stack[i + radius];

				sir[0] = r[yi];
				sir[1] = g[yi];
				sir[2] = b[yi];

				rbs = r1 - Math.abs(i);

				rsum += r[yi] * rbs;
				gsum += g[yi] * rbs;
				bsum += b[yi] * rbs;

				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}

				if (i < hm) {
					yp += w;
				}
			}
			yi = x;
			stackpointer = radius;
			for (y = 0; y < h; y++) {
				// Preserve alpha channel: ( 0xff000000 & pix[yi] )
				pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16)
						| (dv[gsum] << 8) | dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (x == 0) {
					vmin[y] = Math.min(y + r1, hm) * w;
				}
				p = x + vmin[y];

				sir[0] = r[p];
				sir[1] = g[p];
				sir[2] = b[p];

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[stackpointer];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi += w;
			}
		}

		Log.e("pix", w + " " + h + " " + pix.length);
		bitmap.setPixels(pix, 0, w, 0, 0, w, h);

		return (bitmap);
	}

	/**
	 * @deprecated use {@link MyUtilities#showKeyboard(Activity, View)} instead
	 * 
	 * @param context
	 */
	@Deprecated
	public static void showKeyboard(Context context) {
		((InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE))
				.toggleSoftInput(InputMethodManager.SHOW_FORCED,
						InputMethodManager.HIDE_IMPLICIT_ONLY);
	}

	/**
	 * @param pActivity
	 * @param pView
	 *            or null
	 */
	public static void showKeyboard(Activity pActivity, View pView) {
		if (pView == null) {
			pView = pActivity.getWindow().getCurrentFocus();
		} else {
			/**
			 * For {@link EditText}, a call to {@link View#requestFocus()} will
			 * open the keyboard as per inputType set for {@link EditText}
			 */
			pView.requestFocus();
		}
		if (pView != null) {
			InputMethodManager imm = (InputMethodManager) pActivity
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (imm != null) {
				imm.showSoftInput(pView, InputMethodManager.SHOW_FORCED);
			}
		}
	}
	
	/**
	 * @param pView
	 * @param pActivity
	 */
	public static void hideKeyboard(View pView, Activity pActivity) {
		if (pView == null) {
			pView = pActivity.getWindow().getCurrentFocus();
		}
		if (pView != null) {
			InputMethodManager imm = (InputMethodManager) pActivity
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (imm != null) {
				imm.hideSoftInputFromWindow(pView.getWindowToken(), 0);
			}
		}
	}

	public static String getContent(HttpResponse response) {
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String body = "";

			String content = "";

			while ((body = rd.readLine()) != null) {
				content += body + "\n";
			}
			return content.trim();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Bitmap scaleDownToScreenSize(Activity ctx,
			Bitmap originalBitmap) {
		if (SCREEN_HEIGHT == 0 || SCREEN_WIDTH == 0) {
			DisplayMetrics displaymetrics = new DisplayMetrics();
			ctx.getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);
			SCREEN_WIDTH = displaymetrics.widthPixels;
			SCREEN_HEIGHT = displaymetrics.heightPixels;
		}
		originalBitmap = Bitmap.createScaledBitmap(originalBitmap,
				SCREEN_WIDTH, SCREEN_HEIGHT, true);
		return originalBitmap;
	}

	public static int getScreenWidth() {
		return SCREEN_WIDTH;
	}

	public static int getScreenHeight() {
		return SCREEN_HEIGHT;
	}

	public static void calculateScreenSize(Activity ctx) {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		ctx.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		SCREEN_WIDTH = displaymetrics.widthPixels;
		SCREEN_HEIGHT = displaymetrics.heightPixels;
	}

	public static void setCoverImageSize() {
		coverImageSizeObject = new ImageSize(Constant.SCREEN.WIDTH,
				Constant.SCREEN.HEIGHT);
	}

	public static ImageSize getCoverImageSize() {
		if (coverImageSizeObject == null)
			setCoverImageSize();
		return coverImageSizeObject;
	}
	
	public static void openPlayStore(Activity activity) {
		try {
			Intent viewIntent = new Intent("android.intent.action.VIEW",
					Uri.parse("https://play.google.com/store/apps/details?id="
							+ activity.getPackageName()));
			activity.startActivity(viewIntent);
		} catch (Exception e) {
			Toast.makeText(
					activity,
					"Unable to connect to the playstore, please try again after some time.",
					Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	
	public static void sendEmail(Activity activity) {
		Intent email = new Intent(Intent.ACTION_SEND);
		email.putExtra(Intent.EXTRA_EMAIL,
				new String[] { activity.getResources().getString(R.string.ask_frankly_email) });
		email.putExtra(Intent.EXTRA_SUBJECT,
				activity.getResources().getString(R.string.feedback_subject));
		email.setType("message/rfc822");
		activity.startActivity(Intent.createChooser(email, "Choose an Email client :"));
	}

}
