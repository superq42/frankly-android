/*
 * This is the source code of Telegram for Android v. 1.3.2. It is licensed
 * under GNU GPL v. 2 or later. You should have received a copy of the license
 * in this archive (see LICENSE).
 * 
 * Copyright Nikolai Kudashov, 2013.
 */

package me.frankly.util;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.Random;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.config.Controller;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Utilities {

	public native static int convertVideoFrame(ByteBuffer src, ByteBuffer dest,
			int destFormat, int width, int height, int padding, int swap);

	public static void addMediaToGallery(String fromPath, Context context) {
		if (fromPath == null) {
			return;
		}
		File f = new File(fromPath);
		Uri contentUri = Uri.fromFile(f);
		addMediaToGallery(contentUri, context);
	}

	public static void addMediaToGallery(Uri uri, Context context) {
		if (uri == null) {
			return;
		}
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(uri);
		context.sendBroadcast(mediaScanIntent);
	}

	@SuppressLint("NewApi")
	public static String getPath(final Uri uri, Context context) {
		try {
			if ("content".equalsIgnoreCase(uri.getScheme())) {
				return getDataColumn(context, uri, null, null);
			} else if ("file".equalsIgnoreCase(uri.getScheme())) {
				return uri.getPath();
			}
		} catch (Exception e) {
			Log.e("debug_bharat", e + "");
		}
		return null;
	}

	public static String getDataColumn(Context context, Uri uri,
			String selection, String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = { column };

		try {
			cursor = context.getContentResolver().query(uri, projection,
					selection, selectionArgs, null);
			if (cursor != null && cursor.moveToFirst()) {
				final int column_index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(column_index);
			}
		} catch (Exception e) {
			Log.e("debug_bharat", e + "");
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return null;
	}

	public static void showEmptyScreen(View mRootView,
			OnClickListener listener, int type) {
		RelativeLayout rl_empty_screen = (RelativeLayout) mRootView
				.findViewById(R.id.rl_empty_screen);
		ImageView emptyJewelImageView = (ImageView) mRootView
				.findViewById(R.id.iv_empty_jewel);
		TextView emptyTitleTextView = (TextView) mRootView
				.findViewById(R.id.tv_empty_title);
		TextView emptyBodyTextView = (TextView) mRootView
				.findViewById(R.id.tv_empty_body);

		TextView emptyButtonTextView;

		emptyButtonTextView = (TextView) mRootView
				.findViewById(R.id.tv_empty_button);

		if (type != Constant.PLACEHOLDER_SCREENS.EMPTY_NOTIFICATIONS) {
			mRootView.setBackgroundResource(0);
			mRootView.setBackgroundResource(R.drawable.backdrop_follow);
		}

		rl_empty_screen.setVisibility(View.VISIBLE);
		int jewelId = R.drawable.pussy;
		String title = "";
		String body = "";
		String button_label = "";

		switch (type) {
		case Constant.PLACEHOLDER_SCREENS.EMPTY_FEEDS:
			Random rndm = new Random();
			int showActionDiscover = rndm.nextInt(11211);
			title = "Your Feeds are empty!";
			if (showActionDiscover % 2 == 1) {
				body = "Find and follow interesting people from discover \n to get their answers in your feeds.";
				button_label = "Discover People";
				emptyButtonTextView.setTag(ScreenSpecs.SCREEN_DISCOVER);
			} else {
				body = "Invite a few friends to get more stories in your feeds";
				button_label = "Invite Friends";
				emptyButtonTextView.setTag(ScreenSpecs.SCREEN_FEEDS);
			}
			break;
		case Constant.PLACEHOLDER_SCREENS.END_CHANNEL:
			jewelId = R.drawable.no_content_kitty;
			emptyTitleTextView.setVisibility(View.GONE);
			emptyBodyTextView.setVisibility(View.GONE);
			emptyButtonTextView.setVisibility(View.GONE);
			break;
		case Constant.PLACEHOLDER_SCREENS.EMPTY_NOTIFICATIONS:
			title = "You don't have any notifications yet!";
			body = "Invite a few friends to get more stories \n in your feeds";
			button_label = "Invite Friends";
			break;
		case Constant.PLACEHOLDER_SCREENS.API_ERROR:
			jewelId = R.drawable.ic_apifailure;
			title = "Oops!";
			body = "Server error! Please try again after some time";
			button_label = "Retry";
			Log.d("Empty", "Here in api fail");
			// Retry button
			break;
		case Constant.PLACEHOLDER_SCREENS.NO_INTERNET:
			title = "Network error!";
			body = "There seems to be a problem with your internet connection."
					+ " \n Please check that your internet is switched on.";
			button_label = "Retry";
			jewelId = R.drawable.ic_nointernet;
			emptyTitleTextView.setVisibility(View.VISIBLE);
			emptyBodyTextView.setVisibility(View.VISIBLE);
			emptyButtonTextView.setVisibility(View.VISIBLE);
			break;
		case Constant.PLACEHOLDER_SCREENS.BAD_INTERNET:
			title = "Your internet is too slow!";
			body = "The internet connection you are on is too slow."
					+ " \n Please switch to a different connection.";
			button_label = "Retry";
			jewelId = R.drawable.ic_nointernet;
			emptyTitleTextView.setVisibility(View.VISIBLE);
			emptyBodyTextView.setVisibility(View.VISIBLE);
			emptyButtonTextView.setVisibility(View.VISIBLE);
			break;
		case Constant.PLACEHOLDER_SCREENS.EMPTY_QUESTION_LIST:
			title = "Answer Questions!";
			body = "Questions asked to you will show up here. \n Invite friends to get asked.";
			button_label = "Invite Friends";
			break;
		case Constant.PLACEHOLDER_SCREENS.END_FEEDS:
			title = "You've consumed all your feeds";
			body = "Discover new people and their stories by following them.";
			button_label = "Discover";
			emptyButtonTextView.setTag(ScreenSpecs.SCREEN_FEEDS);
			break;
		case Constant.PLACEHOLDER_SCREENS.END_DISCOVER:
			title = "No more celebrities for today!";
			body = "Invite a few friends and get to know their stories on video.";
			button_label = "Invite Friends";
			emptyButtonTextView.setTag(ScreenSpecs.SCREEN_DISCOVER);
			break;
		case Constant.PLACEHOLDER_SCREENS.END_PROFILE:
			title = "That's all!";
			body = "Ask to get more stories from "
					+ mRootView.getTag(R.string.user_full_name).toString()
					+ ".";
			button_label = "Ask";
			emptyButtonTextView
					.setTag(Constant.PLACEHOLDER_SCREENS.END_PROFILE);
			break;
		case Constant.PLACEHOLDER_SCREENS.END_MY_PROFILE:
			title = "That's all!";
			body = "Invite friends to get asked.";
			button_label = "Invite Friends";
			emptyButtonTextView
					.setTag(Constant.PLACEHOLDER_SCREENS.END_MY_PROFILE);
			break;
		case Constant.PLACEHOLDER_SCREENS.NO_FEED:
			jewelId = R.drawable.pussy;
			title = "No Feeds";
			body = "Follow some interesting \n people to get feeds";
			button_label = "Discover";
			emptyTitleTextView.setVisibility(View.VISIBLE);
			emptyBodyTextView.setVisibility(View.VISIBLE);
			emptyButtonTextView.setVisibility(View.VISIBLE);
			emptyButtonTextView
					.setTag(Constant.PLACEHOLDER_SCREENS.NO_FEED);
			break;	
			
		}

		emptyJewelImageView.setBackgroundResource(jewelId);
		emptyTitleTextView.setText(title);
		emptyBodyTextView.setText(body);
		emptyButtonTextView.setText(button_label);
		emptyJewelImageView.setVisibility(View.VISIBLE);
		if (type != Constant.PLACEHOLDER_SCREENS.END_CHANNEL)
			emptyButtonTextView.setVisibility(View.VISIBLE);
		emptyButtonTextView.setOnClickListener(listener);

	}

	public static void removeEmptyScreen(View emptyContainerView, View mRootView) {
		if (emptyContainerView.getVisibility() == View.VISIBLE)
			emptyContainerView.setVisibility(View.GONE);
		mRootView.setBackgroundResource(R.color.default_background);
	}

	public static File getFileName(Context context, String type) {
		File root;
		if (isExternalStorageAvailable()) {
			root = new File(Environment.getExternalStorageDirectory()
					+ File.separator + "Frankly" + File.separator);

		} else {
			root = new File(context.getFilesDir() + File.separator + "Frankly"
					+ File.separator);
		}
		root.mkdirs();
		final String fname = System.currentTimeMillis() + type;
		return new File(root, fname);
	}

	public static String getReferrerParameter(String url) {
		try {
			return url.split("utm_content=")[1].split("&")[0];
		} catch (Exception e) {
			try {
				return url.split("utm_term=")[1].split("&")[0];
			} catch (Exception f) {
				try {
					return url.split("username=")[1].split("&")[0];
				} catch (Exception g) {
					Controller.registerGAEvent("NO_REFERRER_USERNAME_FOUND");
					return null;
				}
			}
		}
	}

	private static boolean isExternalStorageAvailable() {
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else {
			return false;
		}
	}
}
