package me.frankly.util;

import java.util.ArrayList;

public class PermissionUtils {
	public static final int READ_PERMISSIONS = 0;
	public static final int WRITE_PERMISSIONS = 1;

	public static final ArrayList<String> getFacebookPermission(int what)
			throws IllegalArgumentException {
		if (what == READ_PERMISSIONS)
			return getFacebookReadPermissions();
		else if (what == WRITE_PERMISSIONS)
			return getFacebookWritePermissions();
		else
			throw new IllegalArgumentException("Sorry, but what?");
	}

	private static final ArrayList<String> getFacebookReadPermissions() {
		ArrayList<String> permissionsList = new ArrayList<String>();
		permissionsList.add("basic_info");
		permissionsList.add("email");
		permissionsList.add("user_birthday");
		permissionsList.add("user_about_me");
		permissionsList.add("user_likes");
		permissionsList.add("user_interests");

		return permissionsList;
	}

	private static final ArrayList<String> getFacebookWritePermissions() {
		ArrayList<String> permissionsList = new ArrayList<String>();
		permissionsList.add("publish_actions");
		permissionsList.add("publish_stream");
		return permissionsList;
	}
}
