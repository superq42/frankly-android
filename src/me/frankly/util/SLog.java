package me.frankly.util;

import android.util.Log;



public  class SLog {
	public static boolean debug=true;
	public static final String KEY="frankly";
	
	public static  void i(String message)
	{
		Log.i(KEY, message);
	}
	public  static void i(String key,String message)
	{
		Log.i(key, message);
	}
	
	public static  void e(String message)
	{
		Log.e(KEY, message);
	}
	public  static void e(String key,String message)
	{
		Log.e(key, message);
	}
	

}
