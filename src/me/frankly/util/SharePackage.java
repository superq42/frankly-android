package me.frankly.util;


import android.util.Log;

public class SharePackage implements Comparable<SharePackage> {

	private static final String SHARE_PACKAGE = "share_package";
	public String packageName;
	public int successCount;
	public int viewCount;
	public int icon;// resource id
	public String appname;

	public SharePackage(String packagename, int resID,int successCount, int viewCount) {
		this.packageName = packagename;
		this.successCount = successCount;
		this.viewCount = viewCount;
		this.icon = resID;
	}



	@Override
	public int compareTo(SharePackage another) {
		float thisRatio = 0.0f, anotherRatio = 0.0f;
		if (viewCount != 0)
			thisRatio = (float) successCount / (float) viewCount;

		if (another.viewCount != 0)
			anotherRatio = (float) another.successCount / (float) another.viewCount;
		Log.e(SHARE_PACKAGE + "_compare_", "thisRatio " + thisRatio + " anotherRatio: " + anotherRatio + " diff " + (thisRatio - anotherRatio));

		if (anotherRatio > thisRatio)
			return -1;
		else if (anotherRatio < thisRatio)
			return 1;
		else
			return this.viewCount - another.viewCount;

	}

	@Override
	public boolean equals(Object o) {
		SharePackage another = (SharePackage) o;

		return this.packageName.equals(another.packageName);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.packageName + "--" + successCount + "--" + viewCount;
	}

}
