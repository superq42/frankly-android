package me.frankly.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import me.frankly.R;
import me.frankly.config.Constant;
import me.frankly.model.newmodel.NotificationModel;
import me.frankly.view.activity.LauncherActivity;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

public class MyNotificationUtils {

	public static void createSimpleNotification(Context context,
			NotificationModel nObject) {
		NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(
				context);
		nBuilder.setContentTitle(nObject.getHeading());
		nBuilder.setContentText(nObject.getText());
		// nBuilder.setContentInfo(nObject.info);
		nBuilder.setSmallIcon(R.drawable.ic_notification);
		nBuilder.setAutoCancel(true);
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(nObject.getNotif_id(), nBuilder.build());

	}

	public static void CreateBigNotification(final Context context,
			final NotificationModel nObject) {
		new Thread(new Runnable() {

			@SuppressLint("NewApi")
			@Override
			public void run() {
				NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(
						context);

				nBuilder.setContentTitle(nObject.getHeading());
				nBuilder.setContentText(nObject.getText());

				String icon_url = nObject.getIcon_url();
				setLargeIconUrl(context, nBuilder, icon_url);
				
				nBuilder.setColor(context.getResources().getColor(R.color.bg_notification_icon));
				nBuilder.setSmallIcon(R.drawable.ic_notification);
				
				nBuilder.setAutoCancel(true);

				Intent aIntent = new Intent(context, LauncherActivity.class);
				aIntent.setAction(Constant.Notification.ACTION_FROM_NOTIFICATION);
				aIntent.putExtra(Constant.Notification.KEY_NOTIF_OBJECT,
						JsonUtils.jsonify(nObject));
				int nextInt = nObject.getNotif_id();
				PendingIntent pIntent = PendingIntent.getActivity(context,
						nextInt, aIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				nBuilder.setContentIntent(pIntent);

				Notification notification = null;
				
				Log.d("debug_push", "checking type " + nObject.getNotif_View_Type());
				
				if (nObject.getNotif_View_Type() == NotificationModel.TYPE_INBOX) {
					NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
					inboxStyle.setBigContentTitle(nObject.getHeading());
					ArrayList<String> linesList = nObject.getLinesList();
					if (linesList != null && !linesList.isEmpty()) {
						for (Iterator<String> iterator = linesList.iterator(); iterator
								.hasNext();) {
							String nextLine = (String) iterator.next();
							inboxStyle.addLine(nextLine);
						}
					}
					nBuilder.setStyle(inboxStyle);
					notification = nBuilder.build();
				} else if (nObject.getNotif_View_Type() == NotificationModel.TYPE_BIGPICTURE) {
					String image_url = nObject.getImage_url();
					NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
					Bitmap bitmap = null;
					if (image_url != null && !image_url.isEmpty()) {
						bitmap = getScaledBitmapFromURL(image_url, 256, 256);
						if (bitmap != null)
							bigPicStyle.bigPicture(bitmap);
						else
							bitmap = BitmapFactory.decodeResource(
									context.getResources(), R.drawable.icon);
						bigPicStyle.setSummaryText(nObject.getText());
					} else {
						bitmap = BitmapFactory.decodeResource(
								context.getResources(), R.drawable.icon);
					}
					bigPicStyle.bigPicture(bitmap);
					bigPicStyle.setBigContentTitle(nObject.getHeading());
					nBuilder.setStyle(bigPicStyle);
					notification = nBuilder.build();
				} else if (nObject.getNotif_View_Type() == NotificationModel.TYPE_BIGTEXT) {
					NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
					bigTextStyle.setBigContentTitle(nObject.getHeading());
					bigTextStyle.bigText(nObject.getText());
					bigTextStyle.setSummaryText("Frankly");
					nBuilder.setStyle(bigTextStyle);
					notification = nBuilder.build();
				} else if (nObject.getNotif_View_Type() == NotificationModel.TYPE_CUSTOM) {
					RemoteViews bigView = new RemoteViews(context
							.getPackageName(),
							R.layout.item_customnotification_item);
					String image_url = nObject.getImage_url();
					if (image_url != null && !image_url.equals("icon")) {
						Bitmap bitmap2 = getScaledBitmapFromURL(image_url, 256,
								256);
						bigView.setImageViewBitmap(
								R.id.image_customnotification_big, bitmap2);
					} else {
						bigView.setImageViewResource(
								R.id.image_customnotification_big,
								R.drawable.icon);
					}
					bigView.setTextViewText(R.id.tv_customnotification_message,
							nObject.getText());
					bigView.apply(context, null);
					notification = nBuilder.build();
					if (Build.VERSION.SDK_INT >= 16) {
						notification.bigContentView = bigView;
					}
				}
				
				setSoundVibrationUtilities(notification);
				notification.flags |= Notification.FLAG_AUTO_CANCEL;

				NotificationManager mNotificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.notify(nextInt, notification);
			}
		}).start();

	}

	public static Bitmap getScaledBitmapFromURL(String strURL, int height,
			int width) {
		try {
			URL url = new URL(strURL);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			if (myBitmap != null) {
				Bitmap createScaledBitmap = getResizedBitmap(myBitmap, height,
						width);
				if (myBitmap.equals(createScaledBitmap))
					return myBitmap;
				else {
					myBitmap.recycle();
					myBitmap = null;
					return createScaledBitmap;
				}
			} else
				return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
		// return bm;
		if (bm != null) {
			int width = bm.getWidth();
			int height = bm.getHeight();
			float scaleWidth = ((float) newWidth) / width;
			float scaleHeight = ((float) newHeight) / height;
			// CREATE A MATRIX FOR THE MANIPULATION
			Matrix matrix = new Matrix();
			// RESIZE THE BIT MAP
			matrix.postScale(scaleWidth, scaleHeight);

			// "RECREATE" THE NEW BITMAP
			Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
					matrix, false);
			return resizedBitmap;

		} else
			return bm;
	}

	public static void setSoundVibrationUtilities(Notification notification) {

		// ----------------------
		// Add Sound
		// ----------------------
		// a. Default sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// b. Custom sound from SD card
		// notification.sound =
		// Uri.parse("file:///sdcard/notification/ringer.mp3");

		// ----------------------
		// Add Vibration
		// ----------------------
		// a. Default vibration
		notification.defaults |= Notification.DEFAULT_VIBRATE;

		// b. Custom vibration
		// long[] vibrate = {
		// 0,
		// 100,
		// 200,
		// 300 };
		// notification.vibrate = vibrate;

		// ------------------------
		// Add Flashing Lights
		// ------------------------
		// a. Default lights
		// notification.defaults |= Notification.DEFAULT_LIGHTS;

		// b. Custom lights
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;
		notification.ledARGB = 0xff4183d7;
		notification.ledOnMS = 300;
		notification.ledOffMS = 1000;

	}

	public static void setLargeIconUrl(final Context context,
			NotificationCompat.Builder nBuilder, String icon_url) {
		if (icon_url != null && !icon_url.equals("icon")) {
			Bitmap bitmap = getScaledBitmapFromURL(icon_url, 128, 128);
			if (bitmap != null) {
				nBuilder.setLargeIcon(bitmap);
			} else {
				nBuilder.setLargeIcon(((BitmapDrawable) context.getResources()
						.getDrawable(R.drawable.icon)).getBitmap());

			}
			bitmap = null;

		} else
			nBuilder.setLargeIcon(((BitmapDrawable) context.getResources()
					.getDrawable(R.drawable.icon)).getBitmap());
	}

}
