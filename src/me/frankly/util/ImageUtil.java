package me.frankly.util;

import java.util.HashMap;
import java.util.Iterator;

import me.frankly.config.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.BitmapFactory;
import android.util.Log;

public class ImageUtil {

	public static String getThumbUrl(String str) {
		if (str != null && str.startsWith("http")) {
			int index = str.lastIndexOf('.');
			String type = str.substring(index);
			String url = str.substring(0, index);
			return url + "_thumb" + type;
		}
		return str;

	}

	public static String getLargeUrl(String str) {
		if (str != null) {
			int index = str.lastIndexOf('.');
			String type = str.substring(index);
			String url = str.substring(0, index);
			return url + "_large" + type;
		}
		return null;
	}

	public static BitmapFactory.Options getOptions() {
		return getOptions(Constant.SCREEN.WIDTH, Constant.SCREEN.HEIGHT);
	}

	public static BitmapFactory.Options getOptions(int reqWidth, int reqHeight) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;
			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}
		options.inSampleSize = inSampleSize;
		return options;
	}

	/*
	 * public static String getVideoUrl(ProfileVideos profileVideos) {
	 * 
	 * if (profileVideos != null) { boolean highConnecton =
	 * FranklyApplication.isfastConnection; if (highConnecton) return
	 * profileVideos.getOpt(); else { if (profileVideos.getLow() != null) return
	 * profileVideos.getLow(); else if (profileVideos.getMed() != null) return
	 * profileVideos.getMed(); else return profileVideos.getOpt(); } } return
	 * null; }
	 */

	public static String getNetworkBasedVideoUrl(HashMap<String, String> map) {

		String original = null;
		if (map != null) {
			String videoJson = JsonUtils.jsonify(map);
			int bandwidth = PrefUtils.getNetworkBandwidth();
			int videoStream = PrefUtils.getVideoStream();
			Log.d("NetworkSpeed", "My bandwidth is: " + bandwidth);
			int lowKey = -1;
			int myKey = 0;
			int highkey = 10000;
			int streamKey = 0;
			boolean hasMultipleQualities = false;
			try {
				JSONObject jsonObject = new JSONObject(videoJson);
				original = jsonObject.getString("original");
				Iterator<String> keys = jsonObject.keys();
				while (keys.hasNext()) {
					try {
						Log.d("NetworkSpeed", "Before extracting the key");
						int key = Integer.valueOf(keys.next());
						// Check if key is less than current bandwidth
						Log.d("NetworkSpeed", "Key from server is: " + key);
						Log.d("NetworkSpeed", "Result of check: "
								+ (key <= bandwidth));
						if (key <= bandwidth) {
							// Check if key is greater than currently set key
							// If so then you can set the mykey to a higher
							// value
							// because the bandwidth is good
							if (key >= myKey)
								myKey = key;
							else
								lowKey = key;
							hasMultipleQualities = true;
						} else if (key > bandwidth && key < highkey)
							highkey = key;
					} catch (Exception e) {
					}
				}

				/*
				 * Log.i("Speed",
				 * "----------------------------------------------------- ");
				 * Log.i("Speed", "My Bandwidth  " + bandwith); Log.i("Speed",
				 * "My Stream     " + videoStream); Log.i("Speed",
				 * "My key        " + myKey); Log.i("Speed", "Low key       " +
				 * lowKey); Log.i("Speed", "High Key      " + highkey);
				 */
				if (hasMultipleQualities) {
					if (videoStream == Constant.CONNECTION.FINE_STREAM)
						streamKey = myKey != -1 ? myKey : lowKey;
					else if (videoStream == Constant.CONNECTION.DOWN_STREAM)
						streamKey = lowKey != -1 ? lowKey : myKey;
					else if (videoStream == Constant.CONNECTION.UP_STREAM)
						streamKey = highkey != 10000 ? highkey : myKey;
					PrefUtils.setNetworkBandwidth(streamKey);
					PrefUtils.setVideoStream(Constant.CONNECTION.FINE_STREAM);
				} else {
					streamKey = 0;
				}
				Log.i("Speed", "Stream Key    " + streamKey);

				Log.i("NetworkSpeed", "Stream key is: " + streamKey);
				return jsonObject.getString(String.valueOf(streamKey));

			} catch (JSONException e) {
				Log.i("Speed", "getNetworkBasedVideoUrl " + e);
				e.printStackTrace();
			}
		}
		return original;
	}

	public static void setVideoBandWidth(long startTime, long endTime) {
		long loadTime = (endTime - startTime);
		Log.i("NetworkSpeed", "Load Time is: " + loadTime);
		if (loadTime < 3100) {
			PrefUtils.setVideoStream(Constant.CONNECTION.UP_STREAM);
			Log.d("NetworkSpeed", "Notch it up");
		} else if (loadTime > 5900) {
			PrefUtils.setVideoStream(Constant.CONNECTION.DOWN_STREAM);
			Log.d("NetworkSpeed", "Backup tiger");
		}

	}

}
