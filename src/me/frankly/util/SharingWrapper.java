package me.frankly.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Context;
import android.util.Log;

public class SharingWrapper {

	public ArrayList<SharePackage> pkgData;

	public SharingWrapper(ArrayList<SharePackage> shareList) {
		this.pkgData = shareList;
	}

	public ArrayList<SharePackage> getPkgData() {

		return pkgData;
	}

	public static ArrayList<SharePackage> getSharePackageList(String key) {
		String shareList = PrefUtils.getShareList(key);
		SharingWrapper wrapper = (SharingWrapper) JsonUtils.objectify(
				shareList, SharePackage.class);
		return wrapper.pkgData;
	}

	public static ArrayList<SharePackage> getDefaultSharePackageList(
			Context context, LinkedHashMap<String, Integer> hm, String screen) {
		ArrayList<SharePackage> sharePackageList = new ArrayList<SharePackage>();
		int length = hm.size();

		for (String key : hm.keySet()) {
			String[] str = key.split("#");
			// Log.e(str[1],screen);
			if (ShareUtils.isAppInstalled(str[0], context)
					&& str[1].equals(screen)) {

				sharePackageList
						.add(new SharePackage(str[0], hm.get(key), 0, 0));
			}

		}

		return sharePackageList;
	}

}
