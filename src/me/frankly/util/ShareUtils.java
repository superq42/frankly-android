package me.frankly.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import me.frankly.R;
import me.frankly.config.AppConfig;
import me.frankly.config.Constant.ScreenSpecs;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.UniversalUser;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class ShareUtils {
	// ignore repeated objects in new list.
	public static final String PKG_TWITTER = "com.twitter.android";
	public static final String PKG_WHATSAPP = "com.whatsapp";
	public static final String PKG_FACEBOOK = "com.facebook.katana";
	public static final String PKG_KIK = "kik.android";
	public static final String PKG_LINE = "jp.naver.line.android";
	public static final String PKG_FB_MESSANGER = "com.facebook.orca";
	public static final String PKG_TUMBLR = "com.tumblr";
	public static final String PKG_WECHAT = "com.tencent.mm";
	public static final String PKG_HIKE = "com.bsb.hike";
	public static final String PKG_YOUTUBE = "com.google.android.youtube";
	public static final String PKG_INSTAGRAM = "com.instagram.android";
	public static final String PKG_HANGOUTS = "com.google.android.talk";
	public static final String PKG_GOOGLE_PLUS = "com.google.android.apps.plus";
	public static final String PKG_VIBER = "com.viber.voip";
	public static final String POST = ScreenSpecs.SCREEN_POST;
	public static final String FEEDS = ScreenSpecs.SCREEN_FEEDS;
	public static final String PROFILE = ScreenSpecs.SCREEN_PROFILE;
	public static final String DISCOVER = ScreenSpecs.SCREEN_DISCOVER;

	public static class ShareType {
		public static final String ANSWER = "Answer/Post";
		public static final String APP_INVITE = "Invited to app";
		public static final String MY_ANSWER = "Own answer/post";
		public static final String PROFILE = "Profile";
	}

	public static ArrayList<SharePackage> postPackageList = null;
	public static ArrayList<SharePackage> profilePagePackageList = null;

	public static final int repitition_number_answerscreen = 3;
	public static final int visible_items_answerscreen = 3;
	public static final int repitition_number_feeds = 10;
	public static final int visible_items_feeds = 4;

	public static final int repitition_number_placeHolder = 3;
	public static final int visible_items_placeholder = 1;

	public static LinkedHashMap<String, Integer> mp = new LinkedHashMap<String, Integer>();

	public static void CreateHashMap() {
		Log.d("HASHMAP", "HASHMAP_CALLED");
		if (mp.size() > 0)
			mp.clear();

		mp.put(PKG_WHATSAPP + "#" + FEEDS, R.drawable.ic_share_whatsapp_new);
		mp.put(PKG_WHATSAPP + "#" + DISCOVER, R.drawable.ic_share_watsapp1);
		mp.put(PKG_WHATSAPP + "#" + POST, R.drawable.ic_share_whatsapp_new);
		mp.put(PKG_WHATSAPP + "#" + PROFILE, R.drawable.ic_share_watsapp1);
		mp.put(PKG_HIKE + "#" + FEEDS, R.drawable.ic_share_hike1);
		mp.put(PKG_HIKE + "#" + DISCOVER, R.drawable.ic_share_hike1);
		mp.put(PKG_HIKE + "#" + POST, R.drawable.ic_share_hike1);
		mp.put(PKG_HIKE + "#" + PROFILE, R.drawable.ic_share_hike1);
		mp.put(PKG_FB_MESSANGER + "#" + FEEDS, R.drawable.ic_share_fbmsngr1);
		mp.put(PKG_FB_MESSANGER + "#" + DISCOVER, R.drawable.ic_share_fbmsngr1);
		mp.put(PKG_FB_MESSANGER + "#" + POST, R.drawable.ic_share_fbmsngr1);
		mp.put(PKG_FB_MESSANGER + "#" + PROFILE, R.drawable.ic_share_fbmsngr1);
		mp.put(PKG_LINE + "#" + FEEDS, R.drawable.ic_share_line1);
		mp.put(PKG_LINE + "#" + DISCOVER, R.drawable.ic_share_line1);
		mp.put(PKG_LINE + "#" + POST, R.drawable.ic_share_line1);
		mp.put(PKG_LINE + "#" + PROFILE, R.drawable.ic_share_line1);
		mp.put(PKG_TUMBLR + "#" + FEEDS, R.drawable.ic_share_tumblr1);
		mp.put(PKG_TUMBLR + "#" + DISCOVER, R.drawable.ic_share_tumblr1);
		mp.put(PKG_TUMBLR + "#" + POST, R.drawable.ic_share_tumblr1);
		mp.put(PKG_TUMBLR + "#" + PROFILE, R.drawable.ic_share_tumblr1);
		mp.put(PKG_KIK + "#" + FEEDS, R.drawable.ic_share_kik1);
		mp.put(PKG_KIK + "#" + DISCOVER, R.drawable.ic_share_kik1);
		mp.put(PKG_KIK + "#" + POST, R.drawable.ic_share_kik1);
		mp.put(PKG_KIK + "#" + PROFILE, R.drawable.ic_share_kik1);
		mp.put(PKG_HANGOUTS + "#" + FEEDS, R.drawable.ic_share_hangout1);
		mp.put(PKG_HANGOUTS + "#" + DISCOVER, R.drawable.ic_share_hangout1);
		mp.put(PKG_HANGOUTS + "#" + POST, R.drawable.ic_share_hangout1);
		mp.put(PKG_HANGOUTS + "#" + PROFILE, R.drawable.ic_share_hangout1);
		mp.put(PKG_VIBER + "#" + FEEDS, R.drawable.ic_share_viber1);
		mp.put(PKG_VIBER + "#" + DISCOVER, R.drawable.ic_share_viber1);
		mp.put(PKG_VIBER + "#" + POST, R.drawable.ic_share_viber1);
		mp.put(PKG_VIBER + "#" + PROFILE, R.drawable.ic_share_viber1);
		mp.put(PKG_WECHAT + "#" + FEEDS, R.drawable.ic_share_wechat1);
		mp.put(PKG_WECHAT + "#" + DISCOVER, R.drawable.ic_share_wechat1);
		mp.put(PKG_WECHAT + "#" + POST, R.drawable.ic_share_wechat1);
		mp.put(PKG_WECHAT + "#" + PROFILE, R.drawable.ic_share_wechat);
		mp.put(PKG_FACEBOOK + "#" + FEEDS, R.drawable.ic_share_fb1);
		mp.put(PKG_FACEBOOK + "#" + DISCOVER, R.drawable.ic_share_fb1);
		mp.put(PKG_FACEBOOK + "#" + POST, R.drawable.ic_share_fb1);
		mp.put(PKG_FACEBOOK + "#" + PROFILE, R.drawable.ic_share_fb);
		mp.put(PKG_TWITTER + "#" + FEEDS, R.drawable.ic_share_twitter1);
		mp.put(PKG_TWITTER + "#" + DISCOVER, R.drawable.ic_share_twitter1);
		mp.put(PKG_TWITTER + "#" + POST, R.drawable.ic_share_twitter1);
		mp.put(PKG_TWITTER + "#" + PROFILE, R.drawable.ic_share_twitter1);
		mp.put(PKG_GOOGLE_PLUS + "#" + FEEDS, R.drawable.ic_share_googleplus1);
		mp.put(PKG_GOOGLE_PLUS + "#" + DISCOVER,
				R.drawable.ic_share_googleplus1);
		mp.put(PKG_GOOGLE_PLUS + "#" + POST, R.drawable.ic_share_googleplus1);
		mp.put(PKG_GOOGLE_PLUS + "#" + PROFILE, R.drawable.ic_share_googleplus1);

	}

	public static void shareLinkUsingIntent(String textToShare,
			Context context, String packageName, int request_code) {

		Intent shareIntent = findAppByPackageName(packageName, context);

		if (shareIntent != null) {
			shareIntent.setType("text/plain");
			shareIntent.putExtra(Intent.EXTRA_TEXT, textToShare);
			((Activity) context).startActivityForResult(shareIntent,
					request_code);

		}

	}

	public static Intent findAppByPackageName(String packageName,
			Context context) {
		if (packageName != null) {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			List<ResolveInfo> matches = context.getPackageManager()
					.queryIntentActivities(intent, 0);
			for (ResolveInfo info : matches) {
				if (info.activityInfo.packageName.toLowerCase(Locale.ENGLISH)
						.startsWith(packageName)) {
					intent.setPackage(info.activityInfo.packageName);
					return intent;
				}
			}
		}
		return null;
	}

	public static boolean isAppInstalled(String uri, Context context) {
		PackageManager pm = context.getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	public static ArrayList<SharePackage> getSharingPackages(Context context,
			String key) {
		if (key.equals(PROFILE)) {
			if (profilePagePackageList == null) {
				profilePagePackageList = SharingWrapper
						.getDefaultSharePackageList(context, mp, PROFILE);

			}
			return profilePagePackageList;
		} else {
			if (postPackageList == null) {
				postPackageList = SharingWrapper.getDefaultSharePackageList(
						context, mp, FEEDS);
			}
			if (!key.equals(POST) && AppConfig.DEBUG) {
				Log.e("ShareUtils",
						"screen name key did not match any pre-known keys. Returning postPackagelist");
			}
			return postPackageList;
		}

	}

	public static int getProperIconFromPackageNameRecScreen(
			String package_name, String screen) {
		return mp.get(package_name + "#" + screen);
	}

	public static void shareLinkUsingIntent(String message_to_share,
			Context context, String packageName) {
		Intent facebookIntent = findAppByPackageName(packageName, context);

		if (facebookIntent != null) {
			facebookIntent.putExtra(Intent.EXTRA_TEXT, message_to_share);
			facebookIntent.putExtra(Intent.EXTRA_SUBJECT, "Frankly.me");
			((Activity) context).startActivityForResult(facebookIntent, 504);
		}
	}

	public static void setupSharePackage(SharePackage share,
			ImageView shareImage, OnClickListener listener) {

		if (share != null && share.packageName != null) {

			shareImage.setImageResource(share.icon);
			shareImage.setTag(share.packageName);
			shareImage.setOnClickListener(listener);
		} else
			shareImage.setVisibility(View.GONE);
	}

	/**
	 * @param pUser
	 * @return
	 */
	public static String getTextToShare(UniversalUser pUser) {
		return new StringBuilder()
				.append("Hey, I am answering questions through video selfies on Frankly.me. Checkout ")
				.append(pUser.getWeb_link())
				.append(" just a notification away - ")
				.append(pUser.getFirst_name()).toString();
	}

	/**
	 * @param pAnswerClientId
	 * @return
	 */
	public static String getTextToShare(String pAnswerClientId) {
		return new StringBuilder()
				.append("Hey, I just posted a video answer on frankly. Do check it out, and ask me something too ")
				.append(MyUtilities.getClientGeneratedPostLink(pAnswerClientId))
				.toString();
	}

	/**
	 * @param pQuestion
	 * @return
	 */
	public static String getTextToShare(QuestionObject pQuestion) {
		StringBuilder sb = new StringBuilder();
		sb.append("Check out my question to ")
				.append(pQuestion.getQuestion_to().getFirst_name())
				.append(" on ").append(pQuestion.getWeb_link()).toString();
		if (AppConfig.DEBUG) {
			Log.d("ACF", "sb : " + sb.toString());
		}
		return sb.toString();
	}
}
