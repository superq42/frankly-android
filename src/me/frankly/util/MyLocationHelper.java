package me.frankly.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import me.frankly.listener.SDKLocationListener;
import me.frankly.model.newmodel.Component;
import me.frankly.model.newmodel.GetAddressGsonResponse;
import me.frankly.model.newmodel.Result;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class MyLocationHelper {

	private static boolean firstRequest = false;
	public static final String DEFAULT_LOCATION = "Rajeev Chowk(Default Location)";
	private static String formattedAddress;
	public static float default_latitude = 28.63290f, default_longitude = 77.21968f;
	public static final int REQ_OPEN_GPS = 101;
	public static final int REQ_OPEN_ANY = 102;
	private static final long ONE_MINUTE = 60 * 1000;
	public static boolean gpsexpected, nwexpected, gpsrecieved, nwrecieved;
	private static Location gpsLocation = null;
	private static Location nwLocation = null;
	private static Location lastLocation = null;
	private static LocationManager locationManager;
	private static MyListener gpsListener;
	private static MyListener nwListener;

	public static boolean checkAndOpenProviders(Context context, SDKLocationListener handler) {
		Log.i("locale", "checkandOpen");
		boolean gpsEnabled = isGPSEnabled();
		boolean networkEnabled = isNetworkEnabled();
		Log.i("locale", "gps status " + isGPSEnabled() + " gpsenabled : " + gpsEnabled);
		if (gpsEnabled && !networkEnabled) {
			createAndShowAlert("Network Location Services Not Enabled. Enable Them For Retter Results", REQ_OPEN_ANY, context, handler);
			return false;
		} else if (!gpsEnabled && networkEnabled) {
			Log.i("locale", "if gps is disabled and network location services are enabled");
			createAndShowAlert("GPS is not Enabled. Enable GPS For Better Results", REQ_OPEN_GPS, context, handler);
			return false;

		} else if (!gpsEnabled && !networkEnabled) {
			Log.i("locale", "if gps is disabled and network location services are disabled");
			createAndShowAlert("No Location Services Enabled. Enable Them For Retter Results", REQ_OPEN_ANY, context, handler);
			return false;
		} else {
			Log.i("locale", "both are enabled");
			return true;
		}
	}

	public static boolean isGPSEnabled() {
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	public static boolean isNetworkEnabled() {
		return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}

	private static boolean createAndShowAlert(String message, final int reqCode, final Context context, final SDKLocationListener handler) {
		return true;
	}

	public static void manageLocation(Context context, final SDKLocationListener handler) {
		if (locationManager == null)
			locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		// boolean status = checkAndOpenProviders(context, handler);
		if (true) {
			Log.i("locale", "manageLocationContinue");
			continueLocationFetchingTask(handler);
		}
	}

	protected static boolean isBetterLocation(Location location, Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > ONE_MINUTE;
		boolean isSignificantlyOlder = timeDelta < -ONE_MINUTE;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate) {
			return true;
		}
		return false;
	}

	public static Location getBestLastLocation() {
		// Log.i("locale", "getBestLastLocationCalled");
		Location gpsLastLocation = null, nwLastLocation = null;
		if (isGPSEnabled()) {
			gpsLastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		}
		if (isNetworkEnabled()) {
			nwLastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		if (gpsLastLocation != null) {
			// Log.i("locale", "gps last location not null it is "
			// + gpsLastLocation.getLatitude());
			boolean gpsBetterLocation = isBetterLocation(gpsLastLocation, nwLastLocation);
			if (gpsBetterLocation) {
				return gpsLastLocation;
			}
		} else {
			// Log.i("locale", "gps last location null " + gpsLastLocation);
		}
		return nwLastLocation;
	}

	public static void continueLocationFetchingTask(SDKLocationListener handler) {
		lastLocation = getBestLastLocation();
		if ((!isGPSEnabled()) && (!isNetworkEnabled())) {
			handler.onLocationUpdated(lastLocation);
		}

		gpsListener = new MyListener(LocationManager.GPS_PROVIDER, handler);
		nwListener = new MyListener(LocationManager.NETWORK_PROVIDER, handler);
		if (lastLocation != null) {
			Log.i("locale", "best location " + lastLocation.getLatitude() + " : " + lastLocation.getLongitude() + "/" + lastLocation.getAccuracy() + "/" + lastLocation.getTime());
		}
		if (isGPSEnabled()) {
			gpsexpected = true;
			Log.i("locale", "gps enabled .. requesting  location");
			locationManager.requestLocationUpdates("gps", 1000 * 60, (float) 1000.4, gpsListener);
		}
		if (isNetworkEnabled()) {
			nwexpected = true;
			Log.i("locale", "network enabled .. requesting  location");
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 60, (float) 1000.4, nwListener);
		}
	}

	public static Location getBestLocationAvailable() {
		Log.i("locale", "getBestLocation available");

		if (nwLocation != null) {

			boolean isnwBest = isBetterLocation(nwLocation, gpsLocation);
			if (isnwBest) {
				Log.i("locale", "nw is best");
				return nwLocation;
			} else {
				if (gpsLocation != null) {
					Log.i("locale", "gps is best over nw");
					return gpsLocation;
				}
			}
		} else if (gpsLocation != null) {
			Log.i("locale", "gps is best");
			return gpsLocation;
		}
		// both new location are null
		Log.i("locale", "last is best");
		return lastLocation;

	}

	private static class MyListener implements LocationListener {
		String locationProvider;
		SDKLocationListener handler;

		MyListener(String locationProviderName, SDKLocationListener handler) {
			locationProvider = locationProviderName;
			this.handler = handler;
		}

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
			Log.i("locale", "status changed " + arg0 + arg1);

		}

		@Override
		public void onProviderEnabled(String arg0) {
			Log.i("locale", "provider enabled" + arg0);

		}

		@Override
		public void onProviderDisabled(String arg0) {
			Log.i("locale", "provider disabled" + arg0);

		}

		@Override
		public void onLocationChanged(Location location) {
			if (locationProvider.equals(LocationManager.GPS_PROVIDER)) {
				Log.i("locale", "location changed GPS: Latitude " + location.getLatitude() + " longitude " + location.getLongitude() + " accuracy " + location.getAccuracy()
						+ "provider " + location.getProvider());
				gpsLocation = location;
				gpsrecieved = true;
			} else {
				Log.i("locale", "location changed NTWRK: Latitude " + location.getLatitude() + " longitude " + location.getLongitude() + " accuracy " + location.getAccuracy()
						+ "provider " + location.getProvider());
				nwLocation = location;
				nwrecieved = true;
			}
			removeUpdates();
			handler.onLocationUpdated(location);

		}
	}

	public static void removeUpdates() {
		// Log.i("locale", "gps updated to false");
		gpsexpected = false;
		nwexpected = false;
		locationManager.removeUpdates(gpsListener);
		locationManager.removeUpdates(nwListener);
	}

	public static void getAddress(String latitude, String longitude, final SDKLocationListener handler) {

		if (!firstRequest) {
			firstRequest = true;
			new GetLocale(latitude, longitude, handler).execute();
		}
		// asyncHttpClient.setTimeout(20000);

	}

	public static void setDefaultLocation(String locality, long latitude, long longitude) {
		formattedAddress = DEFAULT_LOCATION;
		default_latitude = latitude;
		default_longitude = longitude;

	}

	public static class GetLocale extends AsyncTask<Void, Void, Void> {
		String lat, lon;

		SDKLocationListener handler;

		public GetLocale(String lat, String lon, SDKLocationListener handler) {
			this.lat = lat;
			this.lon = lon;
			this.handler = handler;
		}

		@Override
		protected Void doInBackground(Void... params) {
			String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&sensor=true";
			Log.i("locale", "url is " + url);
			try {
				HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
				conn.connect();
				InputStream is = conn.getInputStream();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
				String content = "";
				String tempStr;
				while ((tempStr = bufferedReader.readLine()) != null)
					content += tempStr;
				GetAddressGsonResponse response = (GetAddressGsonResponse) JsonUtils.objectify(content, GetAddressGsonResponse.class);
				if (response != null) {
					String city = "";
					String country = "";
					String route = "";
					String sublocality = "";
					String locality = "";
					int count = 0;
					for (Result result : response.results) {
						count = 0;
						for (Component component : result.address_components) {
							if (component.types.contains("locality")) {
								count++;
								Log.i("locale", "city found: " + component.short_name);
								if (component.short_name.length() < city.length())
									city = component.short_name;
								// city = component.short_name;
							}
							if (component.types.contains("country")) {
								count++;
								Log.i("locale", "country found: " + component.long_name);
								country = component.long_name;
								route = component.short_name;
							}
							if (component.types.contains("administrative_area_level_2")) {
								count++;
								if (component.short_name.length() < locality.length())
									locality = component.short_name;
							}

						}
						if (count >= 2) {
							formattedAddress = route + " " + sublocality + ", " + locality;
							break;
						}
					}
					if (city.length() <= 1)
						city = locality;
					handler.onLocalityFound(formattedAddress, city, country);
				}

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
}
