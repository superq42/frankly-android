package me.frankly.util;

import java.text.SimpleDateFormat;

import me.frankly.config.Controller;
import me.frankly.listener.RequestListener;
import me.frankly.model.newmodel.MixPanelCommand;
import me.frankly.model.newmodel.UniversalUser;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

@SuppressLint("SimpleDateFormat")
public class MixpanelUtils {
	private static final String TAG = "MixPanel-Integration";
	public static final String MIXPANEL_TOKEN = "cb253b293f726a87c1341b9524328563";
	public static final String USER_ID = "user_id";
	public static final String POST_ID = "post_id";
	public static final String TIMESTAMP = "timestamp";

	public static final String APP_INSTALLED = "Frankly installed";
	public static final String APP_LAUNCHED = "Frankly launched";

	/**
	 * Sign up and log in related events
	 */
	public static final String SIGNUP_ATTEMPT = "Email signup attempt";
	public static final String LOGIN_ATTEMPT = "Email login attempt";
	public static final String FB_LOGIN = "Facebook login attempt";
	public static final String GPLUS_LOGIN = "Gplus login attempt";
	public static final String TWITTER_LOGIN = "Twitter login attempt";

	/**
	 * Navigation actions to various screens, fragments or dialogues
	 */
	public static final String NAVIGATION_SEARCH = "Navigated to search screen from top mast";
	public static final String NAVIGATION_MY_PROFILE = "Navigated to my profile from top mast";
	public static final String NAVIGATION_ANSWER_QUESTIONS = "Navigated to question list screen from top mast";
	public static final String NAVIGATION_SIGNUP_SCREEN = "Navigated to sign up screen";
	public static final String NAVIGATION_LOGIN_SCREEN = "Navigated to login screen";
	public static final String NAVIGATION_FORGOT_PASSWORD = "Navigated to forgot password";
	public static final String NAVIGATION_PROFILE = "Navigated to profile";
	public static final String NAVIGATION_COMMENTS = "Navigated to comment screen";
	public static final String NAVIGATION_NOTIFICATIONS = "Navigated to notifications screen";
	public static final String NAVIGATION_SETTINGS = "Navigated to settings screen";
	public static final String CARD_FLIPPED = "Card flipped";
	public static final String EOF = "Navigated to end of card stack";

	/**
	 * User actions on various screens
	 */
	public static final String WALKTHROUGH_PAGE_SCROLLED = "Walkthrough_page_scrolled";

	/*
	 * Video interactions
	 */
	public static final String VIDEO_PLAYED = "Video played";
	public static final String VIDEO_PAUSED = "Video paused";
	public static final String VIDEO_RESUMED = "Video resumed after pause";
	public static final String VIDEO_COMPLETED = "Completed video";

	/*
	 * Share related interactions
	 */
	public static final String SHARE_DIALOG = "Custom share dialog clicked";
	public static final String SHARE_JEWEL = "Share jewel clicked";

	public static final String POST_LIKED = "Post liked";
	public static final String POST_UNLIKED = "Post unliked";
	public static final String USER_UNFOLLOWED = "User unfollowed";
	public static final String USER_FOLLOWED = "User followed";
	public static final String ASK_ME = "Ask me button/icon clicked";

	/*
	 * Question ask and upvoted related events
	 */
	public static final String ADD_QUESTION = "New question asked";
	public static final String QUESTION_UPVOTED = "Question upvoted";
	public static final String QUESTION_DOWNVOTED = "Question downvoted";

	/*
	 * Comment Posted
	 */
	public static final String POST_COMMENT = "Comment posted";

	// TODO - Answer Recording
	public static final String ANSWER_QUESTION = "Answer my question";
	public static final String START_RECORD_VIDEO = "start recording video ";
	public static final String STOP_RECORD_VIDEO = "Stop recording video ";
	public static final String BACK_PRESSED_RECORD_ANSWER = " back pressed record answer ";
	public static final String FORWARD_PRESSED_RECORD_ANSWER = "forward button pressed record video";
	public static final String DELETE_SEGMENT = "deleted a segment";

	public static final String BACK_PRESSED_SHARE_RECORD_ANSWER = " back pressed share record answer ";

	
	private static void sendMixpanelEvent(JSONObject json, Context appContext,
			String eventName) {
		Log.e(TAG,
				"sending MixPanel Event " + eventName + " props :"
						+ json.toString());
		try {
			MixpanelAPI mixpanel = MixpanelAPI.getInstance(appContext,
					MIXPANEL_TOKEN);

			mixpanel.track(eventName, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setMixpanelIdentity(String userid, Context appContext,
			boolean isAlias) {
		Log.e(TAG, "set MixPanelIdentity " + userid);
		String userId = PrefUtils.getCurrentUserAsObject().getId();
		setSuperProperties(appContext, userId);
	}

	public static void setSuperProperties(Context appContext, String userId) {
		JSONObject json = new JSONObject();
		try {
			if (userId != null && userId.length() > 0) {
				json.put(USER_ID, userId);
			}
			json.put(TIMESTAMP, new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
					.format(new java.util.Date()));
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e(TAG, "Error JSON parsing ");
		}
	}

	public static void setSuperPropertiesForCampains(Context context) {
		JSONObject json = new JSONObject();
		try {

			json.put("utm_source", "Custom");
			json.put("utm_term", "Custom");
			json.put("utm_medium", "Custom");
			json.put("utm_content", "Custom");
			json.put("utm_campaign", "Custom");

		} catch (JSONException e) {
			e.printStackTrace();
			Log.e(TAG, "Error JSON parsing ");
		}

	}

	public static void sendInstallEvent(String referrerUsername, String url,
			String event_name, Context context) {
		JSONObject json = new JSONObject();
		try {
			if (referrerUsername != null)
				json.put("referred_by", referrerUsername);
			if (url != null)
				json.put("url_used", url);

		} catch (Exception e) {

		}
		MixpanelUtils.sendMixpanelEvent(json, context, event_name);
	}
	
	public static void sendCompressionFailEvent(String eventName, int originalVideoLength,
			int compressedVideoLength, Context context ){
		JSONObject json = new JSONObject();
		try {
				json.put("original_video_length", originalVideoLength);
				json.put("compressed_video_length", compressedVideoLength);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		MixpanelUtils.sendMixpanelEvent(json, context, eventName);
	}

	public static void sendGenericEvent(String entity_id, String user_name,
			String user_role, String source, String event_name, Context context) {

		JSONObject json = new JSONObject();
		try {
			if (entity_id != null)
				json.put("entity_id", entity_id);
			if (source != null)
				json.put("screen_type", source);
			if (user_role != null)
				json.put("user_role", user_role);
			if (user_name != null)
				json.put("user_name", user_name);
			// Current user
			if (PrefUtils.getUserID() != null)
				json.put("actor_user_id", PrefUtils.getUserID());

		} catch (JSONException e) {
			e.printStackTrace();
		}
		MixpanelUtils.sendMixpanelEvent(json, context, event_name);
	}

	public static void sendLoginAttemptEvent(String type, Context ctx) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("Type", type);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sendMixpanelEvent(obj, ctx, "Login Attempt");
	}

	public static void sendLoginSuccessEvent(String type, boolean isNewUser,
			String userId, Context ctx) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("Type:", type).put("Is New User", isNewUser)
					.put("User Id", userId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sendMixpanelEvent(obj, ctx, "Login Success");
	}

	public static void sendProfileNavigationEvent(String entity_id,
			String user_name, String profile_role, String source,
			String event_name, Context context) {

		JSONObject json = new JSONObject();
		try {
			if (entity_id != null)
				json.put("entity_id", entity_id);
			if (source != null)
				json.put("screen_type", source);
			if (profile_role != null)
				json.put("profile_role", profile_role);
			if (user_name != null)
				json.put("user_name", user_name);
			// Current user
			if (PrefUtils.getUserID() != null)
				json.put("actor_user_id", PrefUtils.getUserID());

		} catch (JSONException e) {
			e.printStackTrace();
		}
		MixpanelUtils.sendMixpanelEvent(json, context, event_name);
	}

	public static void sendSlideshowEvent(int card_number, Context context) {
		JSONObject json = new JSONObject();

		try {
			json.put("card_numbed", card_number);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		MixpanelUtils.sendMixpanelEvent(json, context,
				MixpanelUtils.WALKTHROUGH_PAGE_SCROLLED);
	}

	public static void sendShareJewelEvent(String package_name,
			String user_name, String user_role, String source, Context context) {
		JSONObject json = new JSONObject();
		try {
			if (package_name != null)
				json.put("app_shared_to", package_name);
			if (user_name != null)
				json.put("user_name", user_name);
			if (user_role != null)
				json.put("user_role", user_role);
			if (source != null)
				json.put("screen_type", source);
			// Current user
			if (PrefUtils.getUserID() != null)
				json.put("actor_user_id", PrefUtils.getUserID());
		} catch (Exception e) {
			e.fillInStackTrace();
		}

		MixpanelUtils.sendMixpanelEvent(json, context,
				MixpanelUtils.SHARE_JEWEL);
	}

	public static void sendMixpanelProfile(final Context appContext) {

		if (Controller.isNetworkConnected(appContext)) {
			Controller.getMixPanelCommand(appContext, new RequestListener() {

				@Override
				public void onRequestStarted() {

				}

				@Override
				public void onRequestError(int errorCode, String message) {
					Log.e(TAG, "onResponseError mixpanel " + message);
				}

				@Override
				public void onRequestCompleted(Object responseObject) {
					Log.e(TAG, "onResponse mixpanel " + responseObject);
					MixPanelCommand command = (MixPanelCommand) JsonUtils
							.objectify(responseObject + "",
									MixPanelCommand.class);

					if (command.track)
						setMixpanelPeopleDetails(appContext);

					/*
					 * PrefUtils.setAppLaunchedFirstTime(PrefUtils.
					 * ID_MIXPANEL_FIRST_TIME, false);
					 */
				}
			});
		}

		//
	}

	public static void setMixpanelPeopleDetails(Context appContext) {

		MixpanelAPI mixpanel = MixpanelAPI.getInstance(appContext,
				MIXPANEL_TOKEN);
		try {
			mixpanel.getPeople().identify(
					PrefUtils.getCurrentUserAsObject().getId());

			UniversalUser currentUser = PrefUtils.getCurrentUserAsObject();
			if (currentUser != null) {
				mixpanel.getPeople().set("$full_name",
						currentUser.getFull_name());
				mixpanel.getPeople().set("gender", currentUser.getGender());
				mixpanel.getPeople().set("username", currentUser.getUsername());
				mixpanel.getPeople().set("user_id", currentUser.getId());
				mixpanel.getPeople().set(
						"location",
						PrefUtils.getLocation()[0] + ","
								+ PrefUtils.getLocation()[1]);
				mixpanel.getPeople().set("gcm_id", PrefUtils.getGcmId());
				mixpanel.getPeople().set(
						"$created",
						new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
								.format(new java.util.Date()));
			}
		} catch (Exception e) {

		}

	}

}
