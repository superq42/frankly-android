package me.frankly.util;

import java.io.File;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.util.Log;

public class MyVideoUtilities {

	public static int pxToDp(Context mCtx, int px) {
		DisplayMetrics displayMetrics = mCtx.getResources().getDisplayMetrics();
		int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return dp;
	}

	public static int dpToPx(Context mCtx, int dp) {
		DisplayMetrics displayMetrics = mCtx.getResources().getDisplayMetrics();
		int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	public static String toCamelCase(String inputString) {
		String result = "";
		if (inputString.length() == 0) {
			return result;
		}
		char firstChar = inputString.charAt(0);
		char firstCharToUpperCase = Character.toUpperCase(firstChar);
		result = result + firstCharToUpperCase;
		for (int i = 1; i < inputString.length(); i++) {
			char currentChar = inputString.charAt(i);
			char previousChar = inputString.charAt(i - 1);
			if (previousChar == ' ') {
				char currentCharToUpperCase = Character.toUpperCase(currentChar);
				result = result + currentCharToUpperCase;
			} else {
				char currentCharToLowerCase = Character.toLowerCase(currentChar);
				result = result + currentCharToLowerCase;
			}
		}
		return result;
	}

	private static HashMap<String, Typeface> typefaces = new HashMap<String, Typeface>();

	public static Typeface getTypeface(Context ctx, String typefaceName) {
		if (!typefaces.containsKey(typefaceName)) {
			Typeface tempTypeface = Typeface
					.createFromAsset(ctx.getAssets(), typefaceName + ".ttf");
			typefaces.put(typefaceName, tempTypeface);
		}
		return typefaces.get(typefaceName);

	}

	public static void shareText(String textToShare, String subject, Context context) {
		Intent i = new Intent(android.content.Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
		i.putExtra(android.content.Intent.EXTRA_TEXT, textToShare);
		context.startActivity(Intent.createChooser(i, "Share via"));
	}

	public static String getPostLink(String post_id) {
		return "link for post id " + post_id;
	}

	/* Checks if external storage is available for read and write */
	private static boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}

	/* Checks if external storage is available to at least read */
	public static boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)
				|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}

	public static boolean isMemoryCardAvailable() {
		return isExternalStorageReadable() && isExternalStorageWritable();
	}

	public static long getUsableSizeInMB() {
		StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
		long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
		long megAvailable = bytesAvailable / (1024 * 1024);
		Log.e("bharat", "Available MB : " + megAvailable);
		return megAvailable;
	}

	public static long isMediaWritable() {
		if (isExternalStorageReadable() && isExternalStorageWritable()) {
			return getUsableSizeInMB();
		} else
			return -1;
	}

	public static File getAlbumStorageDir(String albumName) {
		// Get the directory for the user's public pictures directory.
		File file = new File(Environment.getExternalStorageDirectory(), albumName);
		if (!file.mkdirs()) {
			Log.e("FILES", "Directory existance assured");
		}
		return file;
	}

	public static String getVideoIdFromUrl(String url) {
		return url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("."));
	}
}
