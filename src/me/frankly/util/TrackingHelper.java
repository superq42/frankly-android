package me.frankly.util;

import me.frankly.config.FranklyApplication;
import android.app.Activity;
import android.provider.Settings.Secure;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.mobileapptracker.MobileAppTracker;

/**
 * @note As of now these methods are called only from Launcher activity
 * @note Later these can also be called from base of all activities
 */
public class TrackingHelper {

	/**
	 * @param pActivity
	 */
	public static void onActivityCreate(final Activity pActivity) {

		FranklyApplication application = (FranklyApplication) pActivity
				.getApplication();
		final MobileAppTracker tracker = application.getMobileAppTracker();

		// If your app already has a pre-existing user base before you implement
		// the MAT SDK, then
		// identify the pre-existing users with this code snippet.
		// Otherwise, MAT counts your pre-existing users as new installs the
		// first time they run your app.
		// Omit this section if you're upgrading to a newer version of the MAT
		// SDK.
		// This section only applies to NEW implementations of the MAT SDK.
		// boolean isExistingUser = ...
		// if (isExistingUser) {
		// mobileAppTracker.setExistingUser(true);
		// }

		new Thread() {
			public void run() {
				try {
					Info adInfo = AdvertisingIdClient
							.getAdvertisingIdInfo(pActivity
									.getApplicationContext());
					tracker.setGoogleAdvertisingId(adInfo.getId(),
							adInfo.isLimitAdTrackingEnabled());
				} catch (Exception e) {
					// Encountered an error getting Google Advertising Id,
					// using ANDROID_ID
					tracker.setAndroidId(Secure.getString(
							pActivity.getContentResolver(), Secure.ANDROID_ID));
				}
			}
		}.start();

		// Check if deferred deeplink can be opened
		// Uncomment this line if your MAT account has enabled deferred
		// deeplinks
		// mobileAppTracker.checkForDeferredDeeplink(750);

	}

	/**
	 * @param pActivity
	 */
	public static void onActivityResume(Activity pActivity) {
		FranklyApplication application = (FranklyApplication) pActivity
				.getApplication();
		MobileAppTracker tracker = application.getMobileAppTracker();
		// Get source of open for app re-engagement
		tracker.setReferralSources(pActivity);
		// MAT will not function unless the measureSession call is included
		tracker.measureSession();
	}
}
