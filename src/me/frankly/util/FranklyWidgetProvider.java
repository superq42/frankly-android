package me.frankly.util;

import me.frankly.R;
import me.frankly.view.activity.StubLauncherActivity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class FranklyWidgetProvider extends AppWidgetProvider{

	@Override 
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
	        super.onUpdate(context, appWidgetManager, appWidgetIds);
	        final int N = appWidgetIds.length;
	        // Perform this loop procedure for each App Widget that belongs to this provider 
	        for (int i=0; i<N; i++) {
	            int appWidgetId = appWidgetIds[i];
	            Intent intent = new Intent(context, StubLauncherActivity.class);
	            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
	            RemoteViews franklyIcon = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
	            franklyIcon.setOnClickPendingIntent(R.id.btn_widget, pendingIntent);
	            appWidgetManager.updateAppWidget(appWidgetId, franklyIcon);
	        } 
	    } 
}
