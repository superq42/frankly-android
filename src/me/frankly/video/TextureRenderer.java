/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.frankly.video;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import me.frankly.view.fragment.RecordAnswerFragment;
import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.Matrix;

@TargetApi(16)
public class TextureRenderer {

	private static final int FLOAT_SIZE_BYTES = 4;
	private static final int TRIANGLE_VERTICES_DATA_STRIDE_BYTES = 5 * FLOAT_SIZE_BYTES;
	private static final int TRIANGLE_VERTICES_DATA_POS_OFFSET = 0;
	private static final int TRIANGLE_VERTICES_DATA_UV_OFFSET = 3;
	
	private static final float[] mTriangleVerticesData = { -1.0f, -1.0f, 0,
			0.f, 0.f, 1.0f, -1.0f, 0, 1.f, 0.f, -1.0f, 1.0f, 0, 0.f, 1.f, 1.0f,
			1.0f, 0, 1.f, 1.f, };
	
	private static final float sepiaColorMatrix[] = { 0.3588f, 0.7044f,
			0.1368f, 0f, 0.2990f, 0.5870f, 0.1140f, 0f, 0.2392f, 0.4696f,
			0.0912f, 0f, 0f, 0f, 0f, 0f };

//	private static final float sepiaColorMatrix1[] = { 1f,1f,
//		1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f,
//		1f, 1f, 1f, 1f, 1f, 1f };
	
	private static final String VERTEX_SHADER = "uniform mat4 uMVPMatrix;\n"
			+ "uniform mat4 uSTMatrix;\n" + "attribute vec4 aPosition;\n"
			+ "attribute vec4 aTextureCoord;\n"
			+ "varying vec2 vTextureCoord;\n" + "void main() {\n"
			+ "  gl_Position = uMVPMatrix * aPosition;\n"
			+ "  vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n" + "}\n";
	
//	"attribute vec4 vPosition;"
//	+ "attribute vec4 vTexCoordinate;"
//	+ "uniform mat4 textureTransform;" + "uniform mat4 colorMatrix;"
//	+ "varying mat4 outputColorMatrix;"
//	+ "varying vec2 v_TexCoordinate;" + "void main() {"
//	+ "   v_TexCoordinate = (textureTransform * vTexCoordinate).xy;"
//	+ "   outputColorMatrix = colorMatrix;"
//	+ "   gl_Position = vPosition;" + "}";

	/*
	 * private static final String FRAGMENT_SHADER =
	 * "#extension GL_OES_EGL_image_external : require\n" +
	 * "precision mediump float;\n" + "varying vec2 vTextureCoord;\n" +
	 * "uniform samplerExternalOES sTexture;\n" + "void main() {\n" +
	 * "  vec4 color = texture2D(sTexture, vTextureCoord);\n" +
	 * "  float grey = (color.r + color.g + color.b) / 3.0;\n" +
	 * "  gl_FragColor = vec4(grey, grey, grey, 1);\n" + "}";
	 */

	private static final String vertexShaderCode = "attribute vec4 aPosition;"
			+ "attribute vec4 aTexCoordinate;" + "varying vec4 v_Color;"
			+ "uniform mat4 uSTMatrix;" + "uniform mat4 uMVPMatrix;"
			+ "uniform mat4 colorMatrix;" + "varying mat4 outputColorMatrix;"
			+ "varying vec2 v_TexCoordinate;" + "void main() {"
			+ "   v_TexCoordinate = (uSTMatrix * aTexCoordinate).xy;"
			+ "   outputColorMatrix = colorMatrix;"
			+ "   gl_Position = uMVPMatrix*aPosition;" + "}";

	private static final String fragmentShaderCode0 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "  vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  gl_FragColor = color;" + "}";

	private static final String fragmentShaderCode1 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "    vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  vec4 fColor = vec4(0.95, 0.047, 0.047, 1);;\n"
			+ "  gl_FragColor = (0.9 * color) + ((1.0 - 0.9) * fColor);" + "}";

	private static final String fragmentShaderCode2 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "    vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  vec4 fColor = vec4(0.196, 0.949, 0.047, 1);\n"
			+ "  gl_FragColor = (0.9 * color) + ((1.0 - 0.9) * fColor);" + "}";

	private static final String fragmentShaderCode3 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "    vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  vec4 fColor = vec4(0.048, 0.2, 0.949, 1);;\n"
			+ "  gl_FragColor = (0.9 * color) + ((1.0 - 0.9) * fColor);" + "}";

	private static final String fragmentShaderCode4 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main() {\n"
			+ "  vec4 color =  texture2D(texture, v_TexCoordinate);\n"
			+ "  float grey = (color.r + color.g + color.b) / 3.0;\n"
			+ "  gl_FragColor = vec4(grey, grey, grey, 0);\n" + "}";

	private static final String fragmentShaderCode5 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "varying mat4 outputColorMatrix;"
			+ "void main () {"
			+ "     vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "     vec4 outputColor = color * outputColorMatrix;"
			+ "     gl_FragColor = (0.1 * color)+(0.9 * outputColor);" + "}";
	
	private static final String fragmentShaderCode6 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "    vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  vec4 fColor = vec4(0.65, 0.65, 0.95, 1);\n"
			+ "  gl_FragColor = (0.9 * color) + (0.1 * fColor);" + "}";
	
	private static final String fragmentShaderCode7 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main() {\n"
			+ "  vec4 textureColor = texture2D(texture, v_TexCoordinate);"
			+ "  vec3 W = vec3(0.2125, 0.7154, 0.0721);\n"
			+ "  vec2 stp0 = vec2(1.0 / 480.0, 0.0);"
		    +"   vec2 st0p = vec2(0.0, 1.0 / 360.0);"
		    +"   vec2 stpp = vec2(1.0 / 480.0, 1.0 / 360.0);"
		    +"   vec2 stpm = vec2(1.0 / 480.0, -1.0 / 360.0);"
		    +"   float im1m1 = dot( texture2D(texture, v_TexCoordinate - stpp).rgb, W);"
		    +"   float ip1p1 = dot( texture2D(texture, v_TexCoordinate + stpp).rgb, W);"
		    +"   float im1p1 = dot( texture2D(texture, v_TexCoordinate - stpm).rgb, W);"
		    +"   float ip1m1 = dot( texture2D(texture, v_TexCoordinate + stpm).rgb, W);"
		    +"   float im10 = dot( texture2D(texture, v_TexCoordinate - stp0).rgb, W);"
		    +"   float ip10 = dot( texture2D(texture, v_TexCoordinate + stp0).rgb, W);"
		    +"   float i0m1 = dot( texture2D(texture, v_TexCoordinate - st0p).rgb, W);"
		    +"   float i0p1 = dot( texture2D(texture, v_TexCoordinate + st0p).rgb, W);"
		    +"   float h = -im1p1 - 2.0 * i0p1 - ip1p1 + im1m1 + 2.0 * i0m1 + ip1m1;"
		    +"   float v = -im1m1 - 2.0 * im10 - im1p1 + ip1m1 + 2.0 * ip10 + ip1p1;"
		    +"   float mag = 1.0 - length(vec2(h, v));"
		    +"   vec3 target = vec3(mag);"
		    +"   gl_FragColor = vec4(mix(textureColor.rgb, target, 0.2), 1.0);" 
//		    +"    gl_FragColor = textureColor;"
		    +"}";

	
	/**
	 * @note it will be set in {@link RecordAnswerFragment}
	 * @note it will be used in {@link OutputSurface}
	 */
	public static int selectedFragmentShader = 0;
	
	private float[] mMVPMatrix = new float[16];
	private float[] mSTMatrix = new float[16];
	private int mProgram;
	private int mTextureID = -12345;
	private int muMVPMatrixHandle;
	private int mColorHandle;
	private int muSTMatrixHandle;
	private int maPositionHandle;
	private int maTextureHandle;
	private int mRotationAngle = 0;
	private FloatBuffer mTriangleVertices;
	

	public TextureRenderer() {

	}

	public TextureRenderer(int rotation) {
		mRotationAngle = rotation;
		mTriangleVertices = ByteBuffer
				.allocateDirect(mTriangleVerticesData.length * FLOAT_SIZE_BYTES)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTriangleVertices.put(mTriangleVerticesData).position(0);
		Matrix.setIdentityM(mSTMatrix, 0);
	}

	public int getTextureId() {
		return mTextureID;
	}

	public void drawFrame(SurfaceTexture st, boolean invert) {
		checkGlError("onDrawFrame start");
		st.getTransformMatrix(mSTMatrix);

		if (invert) {
			mSTMatrix[5] = -mSTMatrix[5];
			mSTMatrix[13] = 1.0f - mSTMatrix[13];
		}

		GLES20.glUseProgram(mProgram);
		checkGlError("glUseProgram");
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID);
		mTriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
		GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT,
				false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
		checkGlError("glVertexAttribPointer maPosition");
		GLES20.glEnableVertexAttribArray(maPositionHandle);
		checkGlError("glEnableVertexAttribArray maPositionHandle");
		mTriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
		GLES20.glVertexAttribPointer(maTextureHandle, 2, GLES20.GL_FLOAT,
				false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
		checkGlError("glVertexAttribPointer maTextureHandle");
		GLES20.glEnableVertexAttribArray(maTextureHandle);
		checkGlError("glEnableVertexAttribArray maTextureHandle");
		GLES20.glUniformMatrix4fv(muSTMatrixHandle, 1, false, mSTMatrix, 0);
		GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mMVPMatrix, 0);
		GLES20.glUniformMatrix4fv(mColorHandle, 1, false, sepiaColorMatrix, 0);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
		checkGlError("glDrawArrays");
		GLES20.glFinish();
	}

	public void surfaceCreated() {
		switch (selectedFragmentShader) {
		case 0:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode0);
			break;
		case 1:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode1);
			break;
		case 2:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode2);
			break;
		case 3:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode3);
			break;
		case 4:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode4);
			break;
		case 5:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode5);
			break;
		case 6:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode6);
			break;
		case 7:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode7);
			break;
		default:
			mProgram = createProgram(vertexShaderCode, fragmentShaderCode0);
		}
		// mProgram = createProgram(VERTEX_SHADER, FRAGMENT_SHADER);
		if (mProgram == 0) {
			throw new RuntimeException("failed creating program");
		}
		maPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
		checkGlError("glGetAttribLocation aPosition");
		if (maPositionHandle == -1) {
			throw new RuntimeException(
					"Could not get attrib location for aPosition");
		}
		maTextureHandle = GLES20
				.glGetAttribLocation(mProgram, "aTexCoordinate");
		checkGlError("glGetAttribLocation aTextureCoord");
		if (maTextureHandle == -1) {
			throw new RuntimeException(
					"Could not get attrib location for aTextureCoord");
		}
		muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
		checkGlError("glGetUniformLocation uMVPMatrix");
		if (muMVPMatrixHandle == -1) {
			throw new RuntimeException(
					"Could not get attrib location for uMVPMatrix");
		}
		muSTMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uSTMatrix");
		checkGlError("glGetUniformLocation uSTMatrix");
		if (muSTMatrixHandle == -1) {
			throw new RuntimeException(
					"Could not get attrib location for uSTMatrix");
		}
		mColorHandle = GLES20.glGetUniformLocation(mProgram, "colorMatrix");
		checkGlError("glGetUniformLocation colorMatrix");
		if (mColorHandle == -1) {
			throw new RuntimeException(
					"Could not get attrib location for colorMatrix");
		}
		int[] textures = new int[1];
		GLES20.glGenTextures(1, textures, 0);
		mTextureID = textures[0];
		GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID);
		checkGlError("glBindTexture mTextureID");
		GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
		checkGlError("glTexParameter");

		Matrix.setIdentityM(mMVPMatrix, 0);
		if (mRotationAngle != 0) {
			Matrix.rotateM(mMVPMatrix, 0, mRotationAngle, 0, 0, 1);
		}
	}

	public void changeFragmentShader(String fragmentShader) {
		GLES20.glDeleteProgram(mProgram);
		mProgram = createProgram(VERTEX_SHADER, fragmentShader);
		if (mProgram == 0) {
			throw new RuntimeException("failed creating program");
		}
	}

	private int loadShader(int shaderType, String source) {
		int shader = GLES20.glCreateShader(shaderType);
		checkGlError("glCreateShader type=" + shaderType);
		GLES20.glShaderSource(shader, source);
		GLES20.glCompileShader(shader);
		int[] compiled = new int[1];
		GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
		if (compiled[0] == 0) {
			GLES20.glDeleteShader(shader);
			shader = 0;
		}
		return shader;
	}

	private int createProgram(String vertexSource, String fragmentSource) {
		int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
		if (vertexShader == 0) {
			return 0;
		}
		int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
		if (pixelShader == 0) {
			return 0;
		}
		int program = GLES20.glCreateProgram();
		checkGlError("glCreateProgram");
		if (program == 0) {
			return 0;
		}
		GLES20.glAttachShader(program, vertexShader);
		checkGlError("glAttachShader");
		GLES20.glAttachShader(program, pixelShader);
		checkGlError("glAttachShader");
		GLES20.glLinkProgram(program);
		int[] linkStatus = new int[1];
		GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
		if (linkStatus[0] != GLES20.GL_TRUE) {
			GLES20.glDeleteProgram(program);
			program = 0;
		}
		return program;
	}

	public void checkGlError(String op) {
		int error;
		if ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
			throw new RuntimeException(op + ": glError " + error);
		}
	}
}