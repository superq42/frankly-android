package me.frankly.video;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

public class VideoTextureRenderer extends TextureSurfaceRenderer implements
		SurfaceTexture.OnFrameAvailableListener {
	private static final boolean SHOW_FILE_DEBUG_LOGS = true;
	private static final String FILE_DEBUG_TAG = "#skm VTR:";

	private int mShader = 0;
	private int oldShader = 0;
	private static final float sepiaColorMatrix[] = { 0.3588f, 0.7044f,
			0.1368f, 0f, 0.2990f, 0.5870f, 0.1140f, 0f, 0.2392f, 0.4696f,
			0.0912f, 0f, 0f, 0f, 0f, 0f };

	private static final String vertexShaderCode = "attribute vec4 vPosition;"
			+ "attribute vec4 vTexCoordinate;"
			+ "uniform mat4 textureTransform;" + "uniform mat4 colorMatrix;"
			+ "varying mat4 outputColorMatrix;"
			+ "varying vec2 v_TexCoordinate;" + "void main() {"
			+ "   v_TexCoordinate = (textureTransform * vTexCoordinate).xy;"
			+ "   outputColorMatrix = colorMatrix;"
			+ "   gl_Position = vPosition;" + "}";

	private static final String fragmentShaderCode0 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "  vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  gl_FragColor = color;" + "}";

	private static final String fragmentShaderCode1 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "    vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  vec4 fColor = vec4(0.95, 0.047, 0.047, 1);\n"
			+ "  gl_FragColor = (0.9 * color) + ((1.0 - 0.9) * fColor);" + "}";

	private static final String fragmentShaderCode2 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "    vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  vec4 fColor = vec4(0.196, 0.949, 0.047, 1);\n"
			+ "  gl_FragColor = (0.9 * color) + ((1.0 - 0.9) * fColor);" + "}";

	private static final String fragmentShaderCode3 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "    vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  vec4 fColor = vec4(0.048, 0.2, 0.949, 1);\n"
			+ "  gl_FragColor = (0.9 * color) + ((1.0 - 0.9) * fColor);" + "}";

	private static final String fragmentShaderCode4 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main() {\n"
			+ "  vec4 color =  texture2D(texture, v_TexCoordinate);\n"
			+ "  float grey = (color.r + color.g + color.b) / 3.0;\n"
			+ "  gl_FragColor = vec4(grey, grey, grey, 0);\n" + "}";

	private static final String fragmentShaderCode5 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision lowp float;"
			+ "varying vec2 v_TexCoordinate;"
			+ "varying mat4 outputColorMatrix;"
			+ "uniform samplerExternalOES texture;"
			+ "void main()\n {"
			+ "     vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "     vec4 outputColor = color*outputColorMatrix;"
			+ "     gl_FragColor =  (0.1 * color) + ((0.9) * outputColor);"
			+ "}";
	
	private static final String fragmentShaderCode6 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main () {"
			+ "    vec4 color = texture2D(texture, v_TexCoordinate);"
			+ "  vec4 fColor = vec4(0.65, 0.65, 0.95, 1);\n"
			+ "  gl_FragColor = (0.9 * color) + (0.1* fColor);" + "}";
	
	private static final String fragmentShaderCode7 = "#extension GL_OES_EGL_image_external : require\n"
			+ "precision mediump float;"
			+ "uniform samplerExternalOES texture;"
			+ "varying vec2 v_TexCoordinate;"
			+ "void main() {\n"
			+ "  vec4 textureColor = texture2D(texture, v_TexCoordinate);"
			+ "  vec3 W = vec3(0.2125, 0.7154, 0.0721);\n"
			+ "  vec2 stp0 = vec2(1.0 / 480.0, 0.0);"
		    +"   vec2 st0p = vec2(0.0, 1.0 / 360.0);"
		    +"   vec2 stpp = vec2(1.0 / 480.0, 1.0 / 360.0);"
		    +"   vec2 stpm = vec2(1.0 / 480.0, -1.0 / 360.0);"
		    +"   float im1m1 = dot( texture2D(texture, v_TexCoordinate - stpp).rgb, W);"
		    +"   float ip1p1 = dot( texture2D(texture, v_TexCoordinate + stpp).rgb, W);"
		    +"   float im1p1 = dot( texture2D(texture, v_TexCoordinate - stpm).rgb, W);"
		    +"   float ip1m1 = dot( texture2D(texture, v_TexCoordinate + stpm).rgb, W);"
		    +"   float im10 = dot( texture2D(texture, v_TexCoordinate - stp0).rgb, W);"
		    +"   float ip10 = dot( texture2D(texture, v_TexCoordinate + stp0).rgb, W);"
		    +"   float i0m1 = dot( texture2D(texture, v_TexCoordinate - st0p).rgb, W);"
		    +"   float i0p1 = dot( texture2D(texture, v_TexCoordinate + st0p).rgb, W);"
		    +"   float h = -im1p1 - 2.0 * i0p1 - ip1p1 + im1m1 + 2.0 * i0m1 + ip1m1;"
		    +"   float v = -im1m1 - 2.0 * im10 - im1p1 + ip1m1 + 2.0 * ip10 + ip1p1;"
		    +"   float mag = 1.0 - length(vec2(h, v));"
		    +"   vec3 target = vec3(mag);"
		    +"   gl_FragColor = vec4(mix(textureColor.rgb, target, 0.2), 1.0);" 
//		    +"    gl_FragColor = textureColor;"
		    +"}";


	private static float squareSize = 1.0f;
	private static float squareCoords[] = { -squareSize, squareSize, 0.0f, // top
																			// left
			-squareSize, -squareSize, 0.0f, // bottom left
			squareSize, -squareSize, 0.0f, // bottom right
			squareSize, squareSize, 0.0f }; // top right

	private static short drawOrder[] = { 0, 1, 2, 0, 2, 3 };

	private Context ctx;

	// Texture to be shown in backgrund
	private FloatBuffer textureBuffer;
	private float textureCoords[] = { 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
			1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f };
	private int[] textures = new int[1];

	private int vertexShaderHandle;
	private int fragmentShaderHandle;
	private int shaderProgram;
	private FloatBuffer vertexBuffer;
	private ShortBuffer drawListBuffer;

	private SurfaceTexture videoTexture;
	private float[] videoTextureTransform;
	private boolean frameAvailable = false;

	private int videoWidth;
	private int videoHeight;
	private boolean adjustViewport = false;

	public VideoTextureRenderer(Context context, SurfaceTexture texture,
			int width, int height) {
		super(texture, width, height);
		fileDebugLog(FILE_DEBUG_TAG + "VideoTextureRenderer", "Called");
		this.ctx = context;
		videoTextureTransform = new float[16];
	}

	public void loadShaders(int fragment) {
		fileDebugLog(FILE_DEBUG_TAG + "loadshaders", "Called");
		vertexShaderHandle = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
		GLES20.glShaderSource(vertexShaderHandle, vertexShaderCode);
		GLES20.glCompileShader(vertexShaderHandle);
		checkGlError("Vertex shader compile");

		fragmentShaderHandle = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);

		switch (fragment) {
		case 0:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode0);
			break;
		case 1:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode1);
			break;
		case 2:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode2);
			break;
		case 3:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode3);
			break;
		case 4:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode4);
			break;
		case 5:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode5);
			break;
		case 6:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode6);
			break;
		case 7:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode7);
			break;
		default:
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode0);
		}

		GLES20.glCompileShader(fragmentShaderHandle);
		checkGlError("Pixel shader compile");

		shaderProgram = GLES20.glCreateProgram();
		GLES20.glAttachShader(shaderProgram, vertexShaderHandle);
		GLES20.glAttachShader(shaderProgram, fragmentShaderHandle);
		GLES20.glLinkProgram(shaderProgram);
		checkGlError("Shader program compile");

		int[] status = new int[1];
		GLES20.glGetProgramiv(shaderProgram, GLES20.GL_LINK_STATUS, status, 0);
		if (status[0] != GLES20.GL_TRUE) {
			String error = GLES20.glGetProgramInfoLog(shaderProgram);
			Log.e("SurfaceTest", "Error while linking program:\n" + error);
		}

	}

	private void setupVertexBuffer() {
		fileDebugLog(FILE_DEBUG_TAG + "setupVertexBuffer", "Called");
		// Draw list buffer
		ByteBuffer dlb = ByteBuffer.allocateDirect(drawOrder.length * 2);
		dlb.order(ByteOrder.nativeOrder());
		drawListBuffer = dlb.asShortBuffer();
		drawListBuffer.put(drawOrder);
		drawListBuffer.position(0);

		// Initialize the texture holder
		ByteBuffer bb = ByteBuffer.allocateDirect(squareCoords.length * 4);
		bb.order(ByteOrder.nativeOrder());

		vertexBuffer = bb.asFloatBuffer();
		vertexBuffer.put(squareCoords);
		vertexBuffer.position(0);

	}

	private void setupTexture(Context context) {
		// fileDebugLog(FILE_DEBUG_TAG + "setupTexture", "Called");

		ByteBuffer texturebb = ByteBuffer
				.allocateDirect(textureCoords.length * 4);
		texturebb.order(ByteOrder.nativeOrder());

		textureBuffer = texturebb.asFloatBuffer();
		textureBuffer.put(textureCoords);
		textureBuffer.position(0);

		// Generate the actual texture
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glGenTextures(1, textures, 0);
		checkGlError("Texture generate");

		GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textures[0]);
		checkGlError("Texture bind");

		videoTexture = new SurfaceTexture(textures[0]);
		videoTexture.setOnFrameAvailableListener(this);
	}

	public void stopRendering() {
		super.stopRendering();
	}

	@Override
	protected boolean draw() {
		// fileDebugLog(FILE_DEBUG_TAG + "draw", "Called");
		synchronized (this) {
			if (frameAvailable) {
				Log.d("GKM", "shader selected" + mShader);
				if (oldShader != mShader) {
					oldShader = mShader;
					loadShaders(mShader);
				}
				videoTexture.updateTexImage();
				videoTexture.getTransformMatrix(videoTextureTransform);
				frameAvailable = false;
			} else {
				return false;
			}

		}
		if (adjustViewport)
			adjustViewport();

		GLES20.glClearColor(1.0f, 0.0f, 0.0f, 0.0f);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

		// Draw texture
		GLES20.glUseProgram(shaderProgram);
		int textureParamHandle = GLES20.glGetUniformLocation(shaderProgram,
				"texture");
		int textureCoordinateHandle = GLES20.glGetAttribLocation(shaderProgram,
				"vTexCoordinate");
		int positionHandle = GLES20.glGetAttribLocation(shaderProgram,
				"vPosition");
		int textureTranformHandle = GLES20.glGetUniformLocation(shaderProgram,
				"textureTransform");
		int colorHandle = GLES20.glGetUniformLocation(shaderProgram,
				"colorMatrix");

		GLES20.glEnableVertexAttribArray(positionHandle);
		GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false,
				4 * 3, vertexBuffer);

		GLES20.glBindTexture(GLES20.GL_TEXTURE0, textures[0]);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glUniform1i(textureParamHandle, 0);

		GLES20.glEnableVertexAttribArray(textureCoordinateHandle);
		GLES20.glVertexAttribPointer(textureCoordinateHandle, 4,
				GLES20.GL_FLOAT, false, 0, textureBuffer);

		GLES20.glUniformMatrix4fv(textureTranformHandle, 1, false,
				videoTextureTransform, 0);
		GLES20.glUniformMatrix4fv(colorHandle, 1, false, sepiaColorMatrix, 0);

		GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
				GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
		GLES20.glDisableVertexAttribArray(positionHandle);
		GLES20.glDisableVertexAttribArray(textureCoordinateHandle);
		GLES20.glDisableVertexAttribArray(colorHandle);

		return true;
	}

	private void adjustViewport() {
		fileDebugLog(FILE_DEBUG_TAG + "adjustViewport", "Called");
		float surfaceAspect = height / (float) width;
		float videoAspect = videoHeight / (float) videoWidth;

		if (surfaceAspect > videoAspect) {
			float heightRatio = height / (float) videoHeight;
			int newWidth = (int) (width * heightRatio);
			int xOffset = (newWidth - width) / 2;
			GLES20.glViewport(-xOffset, 0, newWidth, height);
		} else {
			float widthRatio = width / (float) videoWidth;
			int newHeight = (int) (height * widthRatio);
			int yOffset = (newHeight - height) / 2;
			GLES20.glViewport(0, -yOffset, width, newHeight);
		}

		adjustViewport = false;
	}

	@Override
	protected void initGLComponents() {
		fileDebugLog(FILE_DEBUG_TAG + "initGLComponents", "Called");
		setupVertexBuffer();
		setupTexture(ctx);
		loadShaders(this.mShader);
	}

	@Override
	protected void deinitGLComponents() {
		fileDebugLog(FILE_DEBUG_TAG + "deinitGLComponents", "Called");
		GLES20.glDeleteTextures(1, textures, 0);
		GLES20.glDeleteProgram(shaderProgram);
		videoTexture.release();
		videoTexture.setOnFrameAvailableListener(null);
	}

	public void setVideoSize(int width, int height) {
		fileDebugLog(FILE_DEBUG_TAG + "setVideoSize", "Called");
		this.videoWidth = width;
		this.videoHeight = height;
		adjustViewport = true;
	}

	public void checkGlError(String op) {
		fileDebugLog(FILE_DEBUG_TAG + "checkGLError", "Called");
		int error;
		while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
			Log.e("SurfaceTest",
					op + ": glError " + GLUtils.getEGLErrorString(error));
		}
	}

	public SurfaceTexture getVideoTexture() {
		// fileDebugLog(FILE_DEBUG_TAG + "getVideoTexture", "Called");
		return videoTexture;
	}

	public int getmShader() {
		return mShader;
	}

	public void setmShader(int mShader) {
		this.mShader = mShader;
		// loadShaders(mShader);
	}

	@Override
	public void onFrameAvailable(SurfaceTexture surfaceTexture) {

		 synchronized (this)
		{
			fileDebugLog(FILE_DEBUG_TAG + "onframeAvailable", "Called");
			frameAvailable = true;
		}
	}

	private static void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}
}