package me.frankly.SDKPlayer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaCodec;
import android.media.MediaCodec.CryptoException;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;

import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.ExoPlayer.Listener;
import com.google.android.exoplayer.FrameworkSampleSource;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecTrackRenderer.DecoderInitializationException;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer.EventListener;
import com.google.android.exoplayer.util.PlayerControl;

public class PlayerExoImpl implements IPlayer, EventListener, Listener,
		SurfaceTextureListener {
	private ExoPlayer mExoPlayer;
	private PlayerControl mControl;
	private MediaCodecVideoTrackRenderer videoTrackRenderer;
	private MediaCodecAudioTrackRenderer audioTrackRenderer;
	private FrameworkSampleSource sampleSource;
	private String mUrl;
	private boolean autoPlay;
	private TextureView mTextureView;
	private Handler mHandler;
	private boolean isPrepared = false;
	private boolean playingOpt = true;
	private IPlayerStateChangedListener stateChangedListener;
	private Context mCtx;

	protected PlayerExoImpl() {
		mExoPlayer = ExoPlayer.Factory.newInstance(2);
		mControl = new PlayerControl(mExoPlayer);
		mExoPlayer.addListener(this);
		autoPlay = false;
		isPrepared = false;
	}

	@Override
	public void prepareUrl(Context ctx, String url, Handler handler,
			boolean playOpt) {
		prepareUrl(ctx, url, null, handler, null, false, playOpt);
	}

	@Override
	public String getUrl() {
		return mUrl;
	}

	@Override
	public void pauseVideo() {
		if (mControl.canPause())
			mControl.pause();
	}

	@Override
	public void resumeVideo() {
		mControl.start();

	}

	@Override
	public void stopVideo() {
		Log.d("check", "stopping from exo: " + mUrl);
		mExoPlayer.setPlayWhenReady(false);
		// mExoPlayer.stop();
	}

	@Override
	public void resetSelf() {

		// if (mExoPlayer.getPlaybackLooper().getThread().isAlive())
		// mExoPlayer.getPlaybackLooper().getThread().interrupt();
		// // if (mExoPlayer.getPlaybackLooper().)
		// mExoPlayer.getPlaybackLooper().quit();
		mExoPlayer.removeListener(this);
		mExoPlayer.release();
		if (mTextureView != null)
			mTextureView.setSurfaceTextureListener(null);
		mUrl = null;
		sampleSource = null;
		audioTrackRenderer = null;
		videoTrackRenderer = null;
		autoPlay = false;
		mHandler = null;
		isPrepared = false;
		stateChangedListener = null;
		playingOpt = true;

	}

	@Override
	public boolean isMasterPlayer() {
		return false;

	}

	@Override
	public void prepareUrl(Context ctx, String url, TextureView textureView,
			Handler handler, IPlayerStateChangedListener stateChangedListener,
			boolean autoPlay, boolean playOpt) {
		if (mUrl != null && url.equals(mUrl)) {
			mayBePlayIfPrepared(mTextureView, stateChangedListener);
		} else {

			// resetSelf();
			mExoPlayer = ExoPlayer.Factory.newInstance(2);
			mControl = new PlayerControl(mExoPlayer);
			mExoPlayer.addListener(this);
			mUrl = url;
			mTextureView = textureView;
			mHandler = handler;
			this.mCtx = ctx;
			this.stateChangedListener = stateChangedListener;
			this.autoPlay = autoPlay;
			this.isPrepared = false;
			prepareUrlIntl(playOpt);
		}

	}

	@SuppressLint("InlinedApi")
	private void prepareUrlIntl(boolean playOpt) {
		String tempUrl = mUrl;
		if (playOpt) {
			playingOpt = true;
			tempUrl = getOptimizedVideoUrl(mUrl);
		} else {
			playingOpt = false;
		}
		sampleSource = new FrameworkSampleSource(mCtx, Uri.parse(tempUrl),
				null, 2);
		audioTrackRenderer = new MediaCodecAudioTrackRenderer(sampleSource);
		videoTrackRenderer = new MediaCodecVideoTrackRenderer(sampleSource,
				MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT, 1000, mHandler,
				this, 12);
		mExoPlayer.prepare(videoTrackRenderer, audioTrackRenderer);
		if (autoPlay) {
			mTextureView.setSurfaceTextureListener(this);
			mExoPlayer.sendMessage(videoTrackRenderer,
					MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, new Surface(
							mTextureView.getSurfaceTexture()));
			Log.d("exo", "playing");
			mExoPlayer.setPlayWhenReady(true);
		}
	}

	@Override
	public void mayBePlayIfPrepared(TextureView textureView,
			IPlayerStateChangedListener stateChangedListener) {
		this.mTextureView = textureView;
		mTextureView.setSurfaceTextureListener(this);
		mExoPlayer.sendMessage(videoTrackRenderer,
				MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, new Surface(
						mTextureView.getSurfaceTexture()));
		if (isPrepared()) {
			stateChangedListener.onPlayerPrepared();
			mExoPlayer.seekTo(0);
			mControl.start();
		} else
			mExoPlayer.setPlayWhenReady(true);

	}

	@Override
	public boolean isPrepared() {
		return isPrepared;

	}

	@Override
	public void onDecoderInitializationError(DecoderInitializationException e) {

	}

	@Override
	public void onCryptoError(CryptoException e) {

	}

	@Override
	public void onDroppedFrames(int count, long elapsed) {

	}

	@Override
	public void onVideoSizeChanged(int width, int height) {
		Log.i("MyPlayer", "width " + width);
		Log.i("MyPlayer", "Height " + height);
	}

	@Override
	public void onDrawnToSurface(Surface surface) {

	}

	@Override
	public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
		if (playbackState == ExoPlayer.STATE_READY) {
			isPrepared = true;
			if (stateChangedListener != null)
				stateChangedListener.onPlayerPrepared();

		} else if (playbackState == ExoPlayer.STATE_ENDED) {
			if (stateChangedListener != null)
				stateChangedListener.onPlayerCompleted();
		}

	}

	@Override
	public void onPlayWhenReadyCommitted() {

	}

	@Override
	public void onPlayerError(ExoPlaybackException error) {
		Log.e("exoimpl", "exoplayer onError  extra");
		error.printStackTrace();
		if (playingOpt) {
			Log.e("exoimpl", "error playing optimized url. now playing raw url");
			if (mUrl != null) {
				prepareUrlIntl(false);
			}
			Log.e("exoimpl", "currentplaying url is null forwarding error");
		}

	}

	private String getOptimizedVideoUrl(String url) {
		StringBuilder optBuilder = new StringBuilder(url);
		optBuilder.insert(url.lastIndexOf("."), "_opt");
		return optBuilder.toString();
	}

	@Override
	public void setMaster(boolean isMaster) {
		if (isMaster)
			mExoPlayer.getPlaybackLooper().getThread()
					.setPriority(Thread.NORM_PRIORITY + 3);
		else
			mExoPlayer.getPlaybackLooper().getThread()
					.setPriority(Thread.MIN_PRIORITY);

	}

	@Override
	public boolean isAutoPlay() {
		return autoPlay;
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
			int height) {
		mExoPlayer.sendMessage(videoTrackRenderer,
				MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, new Surface(
						surface));

	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {

		return false;
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
			int height) {

	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {

		/*mExoPlayer.sendMessage(videoTrackRenderer,
				MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, new Surface(
						surface));*/
	}

}
