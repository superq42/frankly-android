package me.frankly.SDKPlayer;

import me.frankly.SDKPlayer.IPlayer.IPlayerStateChangedListener;
import android.content.Context;
import android.os.Handler;
import android.view.TextureView;

/**
 * A group of {@link IPlayer} instances that move together for buffering and
 * playing
 * 
 * @author abhishek_inoxapps
 * 
 */
public class IPlayerCluster {
	private static final int DEFAULT_PLAYER_COUNT = 3;
	private IPlayer[] players;
	private IPlayer masterPlayer;
	private String owner;
	private int mPlayerCount;

	public IPlayerCluster(String owner) {
		this(owner, DEFAULT_PLAYER_COUNT);
	}

	public IPlayerCluster(String owner, int playerCount) {
		this.owner = owner;
		this.mPlayerCount = playerCount;
		buildArmy();
	}

	/**
	 * Builds an army of {@link IPlayer} to fight. 'coz that's what armies are
	 * built for.
	 */
	private void buildArmy() {
		players = new IPlayer[mPlayerCount];
		for (int i = 0; i < mPlayerCount; i++)
			players[i] = IPlayerFactory.getInstance().newPlayer();
	}

	/**
	 * Resets the cluster and all its players
	 */
	public void resetCluster() {
		for (IPlayer player : players)
			player.resetSelf();
		masterPlayer = null;
	}

	/**
	 * Assigns an {@link IPlayer} to URL for being played in near future
	 * 
	 * @param url
	 *            to assign for
	 * @param handler
	 *            to handle player messages
	 */
	public boolean assignPlayer(Context ctx, String url, Handler handler) {
		IPlayer playerToAssign = null;
		for (IPlayer player : players) {
			if (player.getUrl() == null)
				playerToAssign = player;
			else if (player.getUrl().equals(url))
				return true; // Already assigned
		}
		if (playerToAssign == null)
			playerToAssign = getPlayerToAsign();
		playerToAssign.resetSelf();
		playerToAssign.prepareUrl(ctx, url, handler, true);
		return true;
	}

	private IPlayer getPlayerToAsign() {
		// TODO Auto-generated method stub
		for (IPlayer player : players)
			if (player != masterPlayer)
				return player;
		return null;
	}

	/**
	 * Assigns an {@link IPlayer} to URL and plays it instantly unless
	 * interrupted
	 * 
	 * @param url
	 * @param handler
	 * @param surfaceView
	 */
	public void assignPlayer(Context ctx, String url, Handler handler,
			TextureView textureView,
			IPlayerStateChangedListener stateChangedListener) {
		if (mayBePlayAssigned(url, textureView, stateChangedListener))
			return;
		if (masterPlayer == null)
			setMasterPlayer(players[0]);
		masterPlayer.resetSelf();
		masterPlayer.prepareUrl(ctx, url, textureView, handler,
				stateChangedListener, true, true);

	}

	/**
	 * Commands the player assigned to this URL to start playing when prepared
	 * 
	 * @param url
	 * @param surfaceView
	 * @return true if command successfully delivered, false if no player
	 *         assigned to this URL
	 */
	public boolean mayBePlayAssigned(String url, TextureView textureView,
			IPlayerStateChangedListener stateChangedListener) {
		if (masterPlayer != null && masterPlayer.getUrl().equals(url))
			return true; // Already playing
		else
			stopCurrentVideo();

		for (IPlayer player : players)
			if (player.getUrl() != null && player.getUrl().equals(url)) {
				player.mayBePlayIfPrepared(textureView, stateChangedListener);
				setMasterPlayer(player);
				return true; // Message successfully passed
			}

		return false; // No player assigned for this url;
	}

	public void stopCurrentVideo() {
		// TODO Auto-generated method stub
		if (masterPlayer != null)
			masterPlayer.resetSelf();

	}

	private void setMasterPlayer(IPlayer player) {
		masterPlayer = player;
	}

	public IPlayer getMasterPlayer() {
		return masterPlayer;
	}

	public String getOwner() {
		return owner;
	}

	public boolean isPlayerAssigned(String url) {

		for (IPlayer player : players)
			if (player.getUrl().equals(url))
				return true;
		return false;

	}
}
