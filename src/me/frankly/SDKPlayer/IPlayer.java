package me.frankly.SDKPlayer;

import android.content.Context;
import android.os.Handler;
import android.view.TextureView;

/**
 * An interface that every Video Player must implement to communicate with
 * client or Manager {@link IPlayerCluster}
 * 
 * @author abhishek_inoxapps
 * 
 */
public interface IPlayer {

	public void prepareUrl(Context ctx, String url, Handler handler,
			boolean playOpt);

	public void prepareUrl(Context ctx, String url, TextureView textureView,
			Handler handler, IPlayerStateChangedListener stateChangedListener,
			boolean autoPlay, boolean playOpt);

	public void mayBePlayIfPrepared(TextureView surfaceView,
			IPlayerStateChangedListener stateChangedListener);

	public String getUrl();

	public void pauseVideo();

	public void resumeVideo();

	public void stopVideo();

	public void resetSelf();

	public boolean isMasterPlayer();

	public boolean isPrepared();

	public boolean isAutoPlay();

	public void setMaster(boolean isMaster);

	/**
	 * Listens to Player state changes
	 * 
	 * @author abhishek_inoxapps
	 * 
	 */
	public interface IPlayerStateChangedListener {
		/**
		 * Player started playing
		 */
		public void onPlayerPrepared();

		/**
		 * player completed
		 */
		public void onPlayerCompleted();

		/**
		 * player paused
		 */
		public void onPlayerPaused();

		/**
		 * player resumed
		 */
		public void onPlayerResumed();


		public void onPlayerPausedToBuffer();

		public void onPlayerResumedAfterBuffer();
		
		public void onPlayerError();
	}

}
