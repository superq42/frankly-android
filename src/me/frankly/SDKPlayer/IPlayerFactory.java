package me.frankly.SDKPlayer;

public abstract class IPlayerFactory {

	public static final boolean isExo =  false/*android.os.Build.VERSION.SDK_INT >= 16*/ ; 


	private static IPlayerFactory instance;

	static {
		if (isExo)
			instance = new ExoPlayerFactory();
		else
			instance = new MediaPlayerFactory();
	}

	public static IPlayerFactory getInstance() {
		return instance;
	}

	private IPlayerFactory() {
	}

	public abstract IPlayer newPlayer();

	private static class ExoPlayerFactory extends IPlayerFactory {
		@Override
		public IPlayer newPlayer() {
			return new PlayerExoImpl();
		}
	}

	private static class MediaPlayerFactory extends IPlayerFactory {
		@Override
		public IPlayer newPlayer() {
			return new PlayerMediaImpl();
		}
	}

}
