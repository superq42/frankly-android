package me.frankly.SDKPlayer;

import java.io.IOException;

import me.frankly.config.Constant;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class PlayerMediaImpl implements IPlayer, OnBufferingUpdateListener,
		OnCompletionListener, OnPreparedListener, OnErrorListener,
		OnInfoListener, SurfaceTextureListener {

	private MediaPlayer mPlayer;
	private String mUrl;
	private boolean autoPlay;
	private TextureView mTextureView;
	private boolean playingOpt = true;
	private IPlayerStateChangedListener stateChangedListener;
	private boolean isPrepared = false;
	private Handler mHandler;
	private Context mCtx;

	private static final boolean SHOW_FILE_DEBUG_LOGS = false;
	private static final String FILE_DEBUG_TAG = "#skm PMI:";

	protected PlayerMediaImpl() {
		fileDebugLog(FILE_DEBUG_TAG + "PlayerMediaImpl", "Called");
		Log.d("check", "media player created");
		mPlayer = new MediaPlayer();
		autoPlay = false;
	}

	@Override
	public void prepareUrl(Context ctx, String url, Handler handler,
			boolean playOpt) {
		fileDebugLog(FILE_DEBUG_TAG + "prepareUrl", "Called");
		Log.d("prepare url","player preparing 2");
		mHandler = handler;
		mCtx = ctx;
		prepareUrl(ctx, url, null, handler, null, false, playOpt);
	}

	@Override
	public void prepareUrl(Context ctx, String url, TextureView textureView,
			Handler handler, IPlayerStateChangedListener stateChangedListener,
			boolean autoPlay, boolean playOpt) {
		Log.d("prepare url","player preparing 1");
		fileDebugLog(FILE_DEBUG_TAG + "prepareUrl", "Called");
		mHandler = handler;
		mCtx = ctx;
		if (mUrl != null && url.equals(mUrl)) {
			mayBePlayIfPrepared(mTextureView, stateChangedListener);
		} else {

			// resetSelf();
			mPlayer = new MediaPlayer();
			mPlayer.setOnBufferingUpdateListener(this);
			mPlayer.setOnCompletionListener(this);
			mPlayer.setOnPreparedListener(this);
			mPlayer.setOnErrorListener(this);
			mPlayer.setOnInfoListener(this);
			mUrl = url;
			mTextureView = textureView;
			if (mTextureView != null) {
				mTextureView.setSurfaceTextureListener(this);
				if (mTextureView.getSurfaceTexture() != null)
					mPlayer.setSurface(new Surface(mTextureView
							.getSurfaceTexture()));
			}
			this.stateChangedListener = stateChangedListener;
			this.autoPlay = autoPlay;
			this.isPrepared = false;
			prepareUrlIntl(false);
		}

	}

	private void prepareUrlIntl(boolean playOpt) {
		Log.d("prepare url init","player prepare init");
		fileDebugLog(FILE_DEBUG_TAG + "prepareUrlIntl", "Called");
		String tempUrl = getHttpUrl(mUrl);
		if (playOpt) {
			playingOpt = true;
			tempUrl = getOptimizedVideoUrl(mUrl);
		} else {
			playingOpt = false;
		}

		try {
			mPlayer.setDataSource(tempUrl);
			mPlayer.prepareAsync();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void mayBePlayIfPrepared(TextureView textureView,
			IPlayerStateChangedListener stateChangedListener) {
		Log.d("may be play pepared","player reset");
		fileDebugLog(FILE_DEBUG_TAG + "mayBePlayIfPrepared", "Called");
		this.mTextureView = textureView;
		this.stateChangedListener = stateChangedListener;
		mTextureView.setSurfaceTextureListener(this);
		if (mTextureView.getSurfaceTexture() != null)
			mPlayer.setSurface(new Surface(mTextureView.getSurfaceTexture()));
		mPlayer.setSurface(new Surface(mTextureView.getSurfaceTexture()));
		if (isPrepared()) {
			stateChangedListener.onPlayerPrepared();
			mPlayer.seekTo(0);
			mPlayer.start();
		} else
			autoPlay = true;

	}

	@Override
	public String getUrl() {
		fileDebugLog(FILE_DEBUG_TAG + "getUrl", "Called");
		return mUrl;
	}

	@Override
	public void pauseVideo() {
		// ((Activity)mCtx).getWindow().setLayout(newWidth, screenHeight);
		fileDebugLog(FILE_DEBUG_TAG + "pauseVideo", "Called");
		if (mPlayer.isPlaying())
			mPlayer.pause();
		autoPlay = false;
	}

	@Override
	public void resumeVideo() {
		fileDebugLog(FILE_DEBUG_TAG + "resumeVideo", "Called");
		if (!mPlayer.isPlaying())
			mPlayer.start();

	}

	@Override
	public void stopVideo() {
		fileDebugLog(FILE_DEBUG_TAG + "stopVideo", "Called");
		if (mPlayer != null && mPlayer.isPlaying()) {
			fileDebugLog(FILE_DEBUG_TAG + "stopVideo", "Playing video stopped.");
			mPlayer.stop();
		}
		autoPlay = false;
	}

	@Override
	public void resetSelf() {
		fileDebugLog(FILE_DEBUG_TAG + "resetSelf", "Called");
		Log.d("reset self","player reset");
		if (mPlayer != null) {
			mPlayer.reset();
			mPlayer.release();
			mPlayer = null;
			mUrl = null;
			autoPlay = false;
			if (mTextureView != null)
				mTextureView.setSurfaceTextureListener(null);
			mTextureView = null;
			playingOpt = true;
			stateChangedListener = null;
			isPrepared = false;
		}

	}

	@Override
	public boolean isMasterPlayer() {
		fileDebugLog(FILE_DEBUG_TAG + "isMasterPlayer", "Called");
		return false;

	}

	@Override
	public boolean isPrepared() {
		fileDebugLog(FILE_DEBUG_TAG + "isPrepared", "Called");
		return isPrepared;

	}

	@Override
	public void setMaster(boolean isMaster) {
		fileDebugLog(FILE_DEBUG_TAG + "setMaster", "Called");
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		fileDebugLog(FILE_DEBUG_TAG + "onBufferingUpdate", "Called");
		Log.d("check", "mp buffered: " + percent);

	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		Log.d("onError","player play error");
		fileDebugLog(FILE_DEBUG_TAG + "onError", "What:" + what);
		if (playingOpt) {
			Log.e("exoimpl", "error playing optimized url. now playing raw url");
			if (mUrl != null) {
				prepareUrlIntl(false);
				return true;
			}
			Log.e("exoimpl", "currentplaying url is null forwarding error");
		}
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				Toast.makeText(mCtx, "Error Playing This Video",
						Toast.LENGTH_SHORT).show();
			}
		});
		if (stateChangedListener != null) {
			stateChangedListener.onPlayerError();
			return true;
		}
		return false;
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		Log.d("prepared","player prepared");
		fileDebugLog(FILE_DEBUG_TAG + "onPrepared", "Called");
		int mVideoWidth;
		int mVideoHeight;
		int mWidthMargin = 0;
		int mHeightMargin = 0;

		int screenWidth = Constant.SCREEN.WIDTH;
		int screenHeight = Constant.SCREEN.HEIGHT;
		int videoWidth = mp.getVideoWidth();
		int videoHeight = mp.getVideoHeight();
		float videoProportion = (float) videoWidth / (float) videoHeight;
		float screenProportion = (float) screenWidth / (float) screenHeight;
		Log.i("MyPlayer", " Video Url " + mUrl);
		Log.i("MyPlayer", " Player width: " + videoWidth + " ,  Height:"
				+ videoHeight);
		Log.i("MyPlayer", " Screen width: " + Constant.SCREEN.WIDTH
				+ " ,  Height: " + Constant.SCREEN.HEIGHT);
		Log.i("MyPlayer", "Video Proportion " + videoProportion);
		Log.i("MyPlayer", "Screen Proportion " + screenProportion);

		if (videoProportion > screenProportion) {
			mVideoWidth = (int) (screenHeight * videoProportion);
			mVideoHeight = screenHeight;
			if (mVideoWidth > screenWidth)
				mWidthMargin = (mVideoWidth - screenWidth) / 2;
		} else {
			mVideoWidth = screenWidth;
			mVideoHeight = (int) (screenWidth / videoProportion);
			if (mVideoHeight > screenHeight)
				mHeightMargin = (mVideoHeight - screenHeight) / 2;
		}

		Log.i("MyPlayer", "New Width  " + mVideoWidth);
		Log.i("MyPlayer", "New height  " + mVideoHeight);
		Log.i("MyPlayer", "Margin height  " + mHeightMargin);
		Log.i("MyPlayer", "Margin width  " + mWidthMargin);

		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				mVideoWidth, mVideoHeight);
		mTextureView.setLayoutParams(lp);
		lp.setMargins(-mWidthMargin, -mHeightMargin, -mWidthMargin,
				-mHeightMargin);
		isPrepared = true;
		if (autoPlay) {
			Log.v("myplayer", "autoplay on");
			if (stateChangedListener != null)
				stateChangedListener.onPlayerPrepared();
			mp.start();
		} else
			Log.v("myplayer", "autoplay off");
	}

	@Override
	public boolean onInfo(MediaPlayer mp, int what, int extra) {
		fileDebugLog(FILE_DEBUG_TAG + "onInfo", "Called");
		switch (what) {
		case MediaPlayer.MEDIA_INFO_BUFFERING_START:
			if (stateChangedListener != null)
				stateChangedListener.onPlayerPausedToBuffer();
			Log.d("player paused to buffer","player paused to buffer");
			break;
		case MediaPlayer.MEDIA_INFO_BUFFERING_END:
			if (stateChangedListener != null)
				stateChangedListener.onPlayerResumedAfterBuffer();
			Log.d("player resumed after buffer","pllayer resumed after buffer");
			break;
		}
		return false;
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		fileDebugLog(FILE_DEBUG_TAG + "onCompletion", "Called");
		if (stateChangedListener != null)
			stateChangedListener.onPlayerCompleted();

	}

	private String getOptimizedVideoUrl(String url) {
		Log.d("get optimised videoo url","getting opptimised video url");
		fileDebugLog(FILE_DEBUG_TAG + "getOptimizedVideoUrl", "Called");
		StringBuilder optBuilder = new StringBuilder(url);
		optBuilder.insert(url.lastIndexOf("."), "_opt");
		return optBuilder.toString();
	}

	private String getHttpUrl(String url) {
		fileDebugLog(FILE_DEBUG_TAG + "getHttpUrl", "Called");
		if (url.contains("https")) {
			StringBuilder builder = new StringBuilder(url);
			builder.deleteCharAt(4);
			return builder.toString();
		} else
			return url;
	}

	@Override
	public boolean isAutoPlay() {
		fileDebugLog(FILE_DEBUG_TAG + "isAutoPlay", "Called");
		return autoPlay;
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
			int height) {
		fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureAvailable", "Called");
		mPlayer.setSurface(new Surface(surface));
		Log.d("st", "on surface texture available");

	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureDestroyed", "Called");
		Log.d("st", "on surface texture destroyed");
		return false;
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
			int height) {
		fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureSizeChanged", "Called");
		Log.d("st", "onSurfaceTextureSizeChanged");

	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
		// fileDebugLog(FILE_DEBUG_TAG + "onSurfaceTextureUpdated", "Called");
//		mPlayer.setSurface(new Surface(surface));
		// Log.d("st", "onSurfaceTextureUpdated");

	}

	private static void fileDebugLog(String logTag, String logMessage) {
		if (SHOW_FILE_DEBUG_LOGS) {
			Log.d(logTag, logMessage);
		}
	}
}
