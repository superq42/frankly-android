package me.frankly.config;


public class Constant {

	public static class ScreenSpecs {
		public static final String SCREEN_SPLASH = "screen_splash";
		public static final String SCREEN_LOGIN = "screen_login";
		public static final String SCREEN_SIGNUP = "screen_signup";
		public static final String SCREEN_HOME = "screen_home";
		public static final String SCREEN_FEEDS = "feed";
		public static final String SCREEN_DISCOVER = "discover";
		public static final String SCREEN_PROFILE = "screen_profile";
		public static final String SCREEN_DISCOVER_PROFILE = "screen_discover_profile";
		public static final String SCREEN_ASK = "screen_ask";
		public static final String SCREEN_QUESTION_LIST = "screen_public_question_list";
		public static final String SCREEN_MY_QUESTIONS = "Answer my questions";
		public static final String SCREEN_SHARE = "screen_share";
		public static final String SCREEN_SEARCH = "screen_search";
		public static final String SCREEN_COMMENT = "Comment Screen";
		public static final String NOTIFICATION_ME = "me";
		public static final String NOTIFICATION_NEWS = "news";
		public static final String SCREEN_TYPE = "screen_type";
		public static final String SCREEN_POST = "post";
		public static final String SETTINGS = "settings";
		public static final String SCREEN_CHANNEL = "screen_channel";
	}

	public static class AuthSource {
		public static final String SOURCE_GOOGLE = "google";
		public static final String SOURCE_TWITTER = "twitter";
		public static final String SOURCE_EMAIL = "email";
		public static final String SOURCE_FACEBOOK = "facebook";
		public static final String SOURCE_USERNAME = SOURCE_EMAIL;
	}

	protected static class VolleyTtl {
		protected static final int HARD_TTL_1 = 3 * 24 * 60 * 60 * 1000;
		protected static final int SOFT_TTL_1 = 3 * 60 * 1000;
		protected static final int SOFT_TTL_ASK_SUGGESTIONS = 24 * 60 * 60 * 1000;
		protected static final int HARD_TTL_ASK_SUGGESTIONS = 24 * 60 * 60 * 1000;
		protected static final int HARD_TTL_2 = 3 * 24 * 60 * 60 * 1000;
		protected static final int SOFT_TTL_2 = 1 * 1000;
	}

	public static class Key {

		public static final String KEY_SCREEN = "key_Screen";
		public static final String KEY_POST_OBJECT = "post_obj";
		public static final String KEY_USER_ID = "user_id";
		public static final String KEY_USER_TYPE = "user_type";
		public static final String KEY_USER_OBJECT = "user_obj";
		public static final String KEY_USER_NAME = "user_name";
		public static final String KEY_USER_BG_PIC = "user_bg_pic";
		public static final String KEY_USER_BIO = "user_bio";
		public static final String KEY_FROM_DEEPLINK = "from_deeplink";
		public static final String KEY_IS_AUTOPLAY = "is_autoplay";
		public static final String KEY_CELEB_QUES_JSON = "celeb_ques_json";
		public static final String KEY_PARENT_SCREEN_NAME = "prnt_scrn_nm";
		public static final String KEY_EMAIL_SIGN_IN = "email_sign_in";
		public static final String LOGIN_FACEBOOK_FRANKLY = "frankly_login_fb";
		public static final String LOGIN_GOOGLE_PLUS_FRANKLY = "frankly_login_google_plus";
		public static final String LOGIN_TWITTER_FRANKLY = "frankly_login_twitter";
		public static final String LOGIN_EMAIL_FRANKLY = "frankly_login_email";
		public static final String CROP_IMAGE_URL = "crop_image_url";
		public static final String COMMENT_COUNT = "comment_count";
		public static final String COMMENT_ID = "comment_id";
		public static final String KEY_SHORT_QUESTION = "question_short_id";
		public static final String KEY_PENDING_ANSWER = "pending_answer";
		public static final String KEY_CHANNEL_ID = "channel_id";
		public static final String TEXT_TO_SHARE = "text_to_share";
		public static final String SCREEN_MODE = "screen_mode";
		public static final String KEY_QUESTION_OBJECT = "question_obj";
		public static final String KEY_ANSWER_QUESTION = "ans_question_obj";
	}

	public static class DeepLinks {
		public static final String URL_SCHEME_FRANKLY = "frankly";
		public static final String URL_SCHEME_HTTP = "http";

		public static final String DISCOVER = "discover";
		public static final String FEED = "feed";
		public static final String ANSWER_QUESTIONS = "answer";
		public static final String POST_ID = "p";
		public static final String USER_ID = "u";
		public static final String QUESTION_ID = "q";
		public static final String SHARE = "share";
		public static final String COMMENTS = "comments";
		public static final String COMMENT = "comment";
		public static final String ASK = "ask";
		
		// not implemented yet
		public static final String USERNAME = "user_name";
		public static final String NOTIFICATIONS = "notifications";
		public static final String ASK_ID = "idask";
		public static final String UPDATE = "update";
		public static final String PROFILE_ID = "pid";
		public static final String QUESTION = "question";

	}

	public static class Notification {
		public static final String ACTION_FROM_NOTIFICATION = "com.example.frankly.from.notification";
		public static final String KEY_NOTIF_OBJECT = "notif_object";
		public static final String TYPE_NOTIFICATION_COUNT = "notification_count";
	}

	public static class USER {
		public static final int CELEBRITY = 2;
		public static final int USER = 0;
	}

	public static class USER_ROLE {
		public static final String QUESTION_AUTHOR = "Question author";
		public static final String ANSWER_AUTHOR = "Answer author";
	}

	public static class SCREEN {
		public static int WIDTH;
		public static int HEIGHT;
	}

	public static class CONNECTION {
		public static int UP_STREAM = 0;
		public static int FINE_STREAM = 1;
		public static int DOWN_STREAM = 2;
	}

	public static class PLACEHOLDER_SCREENS {
		public static final int EMPTY_FEEDS = 0;
		public static final int EMPTY_QUESTION_LIST = 1;
		public static final int EMPTY_NOTIFICATIONS = 2;
		public static final int API_ERROR = 3;
		public static final int NO_INTERNET = 4;
		public static final int END_FEEDS = 5;
		public static final int END_DISCOVER = 6;
		public static final int END_MY_PROFILE = 7;
		public static final int END_PROFILE = 8;
		public static final int BAD_INTERNET = 9;
		public static final int END_CHANNEL = 10;
		public static final int NO_FEED = 11;
	}

	public static class ERROR_CODES {
		public static final int NETWORK_NOT_AVAILABLE = 1011;
		public static final int SHIT_HAPPENED = 1022;
		public static final int BAD_INTERNET = 502;
		public static final int UNAUTHORIZED_ACCESS = 403;
		public static final int ANSWER_FILES_MISSING = 1023;
		public static final int ALREADY_ANSWERED = 404;
	}

	public static class CHANNEL {
		public static final String FEED = "feed";
		public static final String DISCOVER = "discover";

	}
}
