package me.frankly.config;

import java.util.ArrayList;
import java.util.HashMap;

import me.frankly.adapter.FeedsPagerAdapter;
import me.frankly.listener.universal.action.listener.CommentListener;
import me.frankly.listener.universal.action.listener.PostLikeListener;
import me.frankly.listener.universal.action.listener.UserFollowListener;
import me.frankly.model.newmodel.PostObject;
import me.frankly.view.activity.ProfileActivity;
import android.support.v4.app.Fragment;

/**
 * A collection of Listeners that listens to User Action and updates the
 * associated Page/Data Structure.
 * 
 * @author abhishek_inoxapps
 * 
 */
public class UniversalActionListenerCollection implements CommentListener,
		UserFollowListener, PostLikeListener {

	private static UniversalActionListenerCollection instance;
	private static final String KEY_DEFAULT = "default";

	public static UniversalActionListenerCollection getInstance() {
		if (instance == null)
			instance = new UniversalActionListenerCollection();
		return instance;
	}

	private HashMap<String, ArrayList<CommentListener>> commentListeners;
	private HashMap<String, ArrayList<UserFollowListener>> userFollowListeners;
	private HashMap<String, ArrayList<PostLikeListener>> likeListeners;

	private UniversalActionListenerCollection() {
		commentListeners = new HashMap<String, ArrayList<CommentListener>>();
		userFollowListeners = new HashMap<String, ArrayList<UserFollowListener>>();
		likeListeners = new HashMap<String, ArrayList<PostLikeListener>>();
	}

	/**
	 * Use this method to add your {@link CommentListener} for a specific
	 * {@link PostObject}. Typically, this will be a {@link Fragment} that shows
	 * the very {@link PostObject}. And should be called in
	 * {@link Fragment#onCreate(android.os.Bundle)}
	 * 
	 * @param newListener
	 * @param postId
	 */
	public void addPostCommentListener(CommentListener newListener,
			String postId) {
		ArrayList<CommentListener> postListenerBucket;
		if (commentListeners.containsKey(postId))
			postListenerBucket = commentListeners.get(postId);
		else {
			postListenerBucket = new ArrayList<CommentListener>();
			commentListeners.put(postId, postListenerBucket);
		}
		postListenerBucket.add(newListener);

	}

	/**
	 * Use this method to remove your {@link CommentListener} that you've
	 * previously added using
	 * {@link UniversalActionListenerCollection#addPostCommentListener(CommentListener, String)}
	 * 
	 * @param listener
	 * @param postId
	 * @return <code>true</code> if was added and removed. <code>false</code>
	 *         otherwise.
	 */
	public boolean removePostCommentListener(CommentListener listener,
			String postId) {
		if (commentListeners.containsKey(postId)) {
			ArrayList<CommentListener> bucket = commentListeners.get(postId);
			boolean isRemoved = bucket.remove(listener);
			if (bucket.isEmpty())
				commentListeners.remove(postId);
			return isRemoved;
		}
		return false;
	}

	/**
	 * Use this method to add a {@link CommentListener} for every
	 * {@link PostObject}. Typically this will be a collection of
	 * {@link PostObject}. For example, {@link VerticalViewPagerFragment},
	 * {@link FeedsPagerAdapter}, {@link ProfileActivity}
	 * 
	 * @param newListener
	 */
	public void addGeneralCommentListener(CommentListener newListener) {
		addPostCommentListener(newListener, KEY_DEFAULT);
	}

	/**
	 * Use this method to remove your {@link CommentListener} that you've
	 * previously added using
	 * {@link UniversalActionListenerCollection#addGeneralCommentListener(CommentListener, String)}
	 * 
	 * @param listener
	 * @return <code>true</code> if was added and removed. <code>false</code>
	 *         otherwise.
	 */
	public boolean removeGeneralCommentListener(CommentListener listener) {
		return removePostCommentListener(listener, KEY_DEFAULT);
	}

	@Override
	public void onCommentDone(String postId, String newCommentId,
			int newCommentCount) {
		if (commentListeners.containsKey(KEY_DEFAULT))
			notifyCommentDone(commentListeners.get(KEY_DEFAULT), postId,
					newCommentId, newCommentCount);
		if (commentListeners.containsKey(postId))
			notifyCommentDone(commentListeners.get(postId), postId,
					newCommentId, newCommentCount);
	}

	private void notifyCommentDone(ArrayList<CommentListener> commentListeners,
			String postId, String newCommentId, int newCommentCount) {
		for (CommentListener thisCommentListener : commentListeners)
			thisCommentListener.onCommentDone(postId, newCommentId,
					newCommentCount);

	}

	public void addUserFollowListener(UserFollowListener newListener,
			String userId) {
		ArrayList<UserFollowListener> followListenerBucket;
		if (userFollowListeners.containsKey(userId))
			followListenerBucket = userFollowListeners.get(userId);
		else {
			followListenerBucket = new ArrayList<UserFollowListener>();
			userFollowListeners.put(userId, followListenerBucket);
		}
		followListenerBucket.add(newListener);
	}

	public boolean removeUserFollowListener(UserFollowListener listener,
			String userId) {
		if (userFollowListeners.containsKey(userId)) {
			ArrayList<UserFollowListener> bucket = userFollowListeners
					.get(userId);
			boolean isRemoved = bucket.remove(listener);
			if (bucket.isEmpty())
				userFollowListeners.remove(userId);
			return isRemoved;
		}
		return false;
	}

	public void addGeneralFollowListener(UserFollowListener newListener) {
		addUserFollowListener(newListener, KEY_DEFAULT);
	}

	public boolean removeGeneralFollowListener(UserFollowListener listener) {
		return removeUserFollowListener(listener, KEY_DEFAULT);
	}

	@Override
	public void onUserFollowChanged(String userId, boolean isFollowed) {
		if (userFollowListeners.containsKey(KEY_DEFAULT))
			notifyUserFollowed(userFollowListeners.get(KEY_DEFAULT), userId,
					isFollowed);
		if (userFollowListeners.containsKey(userId))
			notifyUserFollowed(userFollowListeners.get(userId), userId,
					isFollowed);
	}

	private void notifyUserFollowed(ArrayList<UserFollowListener> listeners,
			String userId, boolean isFollowed) {
		for (UserFollowListener thisFollowListener : listeners)
			thisFollowListener.onUserFollowChanged(userId, isFollowed);
	}

	public void addPostLikeListener(PostLikeListener newListener, String postId) {
		ArrayList<PostLikeListener> likeListenerBucket;
		if (likeListeners.containsKey(postId))
			likeListenerBucket = likeListeners.get(postId);
		else {
			likeListenerBucket = new ArrayList<PostLikeListener>();
			likeListeners.put(postId, likeListenerBucket);
		}
		likeListenerBucket.add(newListener);
	}

	public void addGeneralLikeListener(PostLikeListener newListener) {
		addPostLikeListener(newListener, KEY_DEFAULT);
	}

	public boolean removePostLikeListener(PostLikeListener listener,
			String postId) {
		if (likeListeners.containsKey(postId)) {
			ArrayList<PostLikeListener> bucket = likeListeners.get(postId);
			boolean isRemoved = bucket.remove(listener);
			if (bucket.isEmpty())
				likeListeners.remove(postId);
			return isRemoved;
		}
		return false;
	}

	public boolean removeGeneralLikeListener(PostLikeListener listener) {
		return removePostLikeListener(listener, KEY_DEFAULT);
	}

	@Override
	public void onPostLikeChanged(String postId, boolean isLiked,
			int newLikeCount) {
		if (likeListeners.containsKey(KEY_DEFAULT))
			notifyPostLiked(likeListeners.get(KEY_DEFAULT), postId, isLiked,
					newLikeCount);
		if (likeListeners.containsKey(postId))
			notifyPostLiked(likeListeners.get(postId), postId, isLiked,
					newLikeCount);

	}

	private void notifyPostLiked(ArrayList<PostLikeListener> listeners,
			String postId, boolean isLiked, int newLikeCount) {
		for (PostLikeListener listener : listeners)
			listener.onPostLikeChanged(postId, isLiked, newLikeCount);
	}
}
