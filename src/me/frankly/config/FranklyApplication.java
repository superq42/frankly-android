package me.frankly.config;

import me.frankly.BuildConfig;
import me.frankly.R;
import me.frankly.servicereceiver.SingleAnswerUploadService;
import me.frankly.util.NetworkConnectivity;
import me.frankly.util.PrefUtils;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mobileapptracker.MobileAppTracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * Created by Sumit on 4/11/14.
 */
public class FranklyApplication extends Application {

	private final String analytics_appId = "UA-53424885-4";
	public static final String APPS_FLYER_KEY = "jHjD7y6c5xE6yfBfnNFCK";
	private static GoogleAnalytics googleAnalytics;
	private static Tracker DEF_TRACKER;

	// 40 as default because that is when we started sending version code to
	// server
	public static String mVersionCode = "44";

	/**
	 * non-static singleton
	 */
	private MobileAppTracker mMobileAppTracker;

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d("InstallTracking", "Going to register event through apps flyer");

		// Set version code of app
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			mVersionCode = String.valueOf(info.versionCode);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		// Setup Crashalytics
		FranklyCrashlytics.enableCrashlytics(this);

		Log.e("Build.DEBUG", "" + BuildConfig.DEBUG);
		// Init UIL - Set default options
		initUIL();

		// Init Analytics - GA & Mixpanel
		initGoogleAnalytics();

		Controller.init(getApplicationContext());
		// initMixpanel();

		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		Constant.SCREEN.WIDTH = size.x;
		Constant.SCREEN.HEIGHT = size.y;

		// Update the network bandwidth in pref utils in case
		// it is lower than the last connection
		int current_network_bandwidth = NetworkConnectivity.getConnection(this);
		if (current_network_bandwidth > PrefUtils.getNetworkBandwidth())
			PrefUtils.setNetworkBandwidth(current_network_bandwidth);

		PrefUtils.setVideoStream(Constant.CONNECTION.FINE_STREAM);

		SingleAnswerUploadService.start(this, null);
	}

	private void initUIL() {

		@SuppressWarnings("deprecation")
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.cacheInMemory(true).cacheOnDisc(true)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				getBaseContext())
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.denyCacheImageMultipleSizesInMemory()
				.defaultDisplayImageOptions(options).writeDebugLogs().build();

		ImageLoader.getInstance().init(config);
	}

	private void initGoogleAnalytics() {
		googleAnalytics = GoogleAnalytics.getInstance(this);

		DEF_TRACKER = googleAnalytics.newTracker(analytics_appId);

	}

	public static Tracker getDefTracker() {
		return DEF_TRACKER;
	}

	/**
	 * @return
	 */
	public MobileAppTracker getMobileAppTracker() {
		if (mMobileAppTracker == null) {
			MobileAppTracker.init(this, getString(R.string.mat_advertiser_id),
					getString(R.string.mat_conversion_key));
			mMobileAppTracker = MobileAppTracker.getInstance();
		}
		return mMobileAppTracker;
	}
}
