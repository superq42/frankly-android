package me.frankly.config;

import me.frankly.BuildConfig;

/**
 * @note all flags that should be set on/off for production build
 */
public class AppConfig {

	/**
	 * @note no need to set it manually
	 * @warning till BuildConfig.DEBUG is working fine
	 */
	public static final boolean DEBUG = BuildConfig.DEBUG;
}
