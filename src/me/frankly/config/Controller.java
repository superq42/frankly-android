package me.frankly.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.frankly.config.UrlResolver.EndPoints;
import me.frankly.exception.DataNotFoundException;
import me.frankly.exception.ErrorCode;
import me.frankly.listener.NetworkStateChangeListener;
import me.frankly.listener.RequestListener;
import me.frankly.listener.SDKLocationListener;
import me.frankly.model.CheckCredAvailabilityRequest;
import me.frankly.model.Credentials;
import me.frankly.model.newmodel.DeviceInfo;
import me.frankly.model.newmodel.EditableProfileObject;
import me.frankly.model.newmodel.FeedbackPostRequest;
import me.frankly.model.newmodel.InstallReferralData;
import me.frankly.model.newmodel.InviteCelebrityRequest;
import me.frankly.model.newmodel.PriviledgedUser;
import me.frankly.model.newmodel.QuestionId;
import me.frankly.model.newmodel.QuestionObject;
import me.frankly.model.newmodel.SharePostObject;
import me.frankly.model.newmodel.SinglePriviledgedUserContainer;
import me.frankly.model.newmodel.TagObject;
import me.frankly.model.newmodel.postrequest.ChangePassword;
import me.frankly.model.newmodel.postrequest.CoordinatePoint;
import me.frankly.model.newmodel.postrequest.DeleteAnswerRequest;
import me.frankly.model.newmodel.postrequest.LoginRequest;
import me.frankly.model.newmodel.postrequest.LogoutRequest;
import me.frankly.model.newmodel.postrequest.MergeAccountRequest;
import me.frankly.model.newmodel.postrequest.PostCommentrequest;
import me.frankly.model.newmodel.postrequest.PostIdOnlyRequest;
import me.frankly.model.newmodel.postrequest.PostQuestionRequest;
import me.frankly.model.newmodel.postrequest.RegisterRequest;
import me.frankly.model.newmodel.postrequest.UpdateGCMRequest;
import me.frankly.model.newmodel.postrequest.UserIdOnlyRequest;
import me.frankly.model.newmodel.postrequest.UsernameOnlyRequest;
import me.frankly.servicereceiver.GCMIntentService;
import me.frankly.util.JsonUtils;
import me.frankly.util.LocationUtils;
import me.frankly.util.MyLocationHelper;
import me.frankly.util.MyUtilities;
import me.frankly.util.NetworkUtils;
import me.frankly.util.PrefUtils;
import me.frankly.view.activity.LauncherActivity;
import me.frankly.view.activity.SettingsActivity;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.RequestAsyncTask;
import com.facebook.Session;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.HitBuilders.EventBuilder;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.Plus;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;

public final class Controller {

	private static DeviceInfo mDeviceInfo = null;
	private static RequestQueue mRequestQueue;
	private static NetworkStateChangeListener notifier;
	private static NetworkResponse defNoInternetNetworkResponse = new NetworkResponse(
			ErrorCode.NO_INTERNET_CONNECTION.getErrorCode(),
			"Network Not Available".getBytes(), null, false);

	private static final String TAG_SEARCH_QUERY = "search";

	private static final RetryPolicy DEFAULT_RETRY_POLICY = new RetryPolicy() {

		@Override
		public void retry(VolleyError error) throws VolleyError {
		}

		@Override
		public int getCurrentTimeout() {
			return 0;
		}

		@Override
		public int getCurrentRetryCount() {
			return 3;
		}
	};

	public static final int DEFAULT_PAGE_SIZE = 10;

	public static final void init(Context context) {

		PrefUtils.init(context);
		/*
		 * if (!NetworkUtils.isNetworkConnected(context))
		 * Toast.makeText(context, "No internet connection",
		 * Toast.LENGTH_SHORT).show();
		 */

		mRequestQueue = Volley.newRequestQueue(context);
		Log.d("controller", PrefUtils.getGcmId());
		Log.d("controller", PrefUtils.getAppAccessToken());

		if (PrefUtils.getGcmId().equals(PrefUtils.DEFAULT_GCM_ID)) {
			Log.e("gcm_id", "we have default id " + PrefUtils.getGcmId());
			if (NetworkUtils.isNetworkConnected(context))
				registerOnGCM(context);
		} else if (!PrefUtils.isGcmUpdated()) {
			Log.e("gcm_id", "init else case" + PrefUtils.getGcmId());
			updateGcmToServer(context);
		}

	}

	public static void sendInstallReferalData(final Context context,
			String device_id, String url_playstore) {
		InstallReferralData data = new InstallReferralData(device_id,
				url_playstore);
		Log.e("install_ref", JsonUtils.jsonify(data));
		RequestListener listener = new RequestListener() {

			@Override
			public void onRequestStarted() {

			}

			@Override
			public void onRequestError(int errorCode, String message) {
				Log.e("install_ref", "failed with error " + errorCode + " "
						+ message);
			}

			@Override
			public void onRequestCompleted(Object responseObject) {
				Log.e("install_ref", "success " + responseObject);

			}
		};
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.INSTALL_REF);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.POST, data, url, listener);
		listener.onRequestStarted();
		volleyTypeRequest.setTag(listener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void getInitProfile(Context context) {

		Log.e("Ashish", "getInitProfile");
		requestProfile(context, PrefUtils.getUserID(), initProfileListener);

	}

	public static void sendFeedBackText(Context context, String text,
			RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.RECORD_FEEDBACK_TEXT);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.GET, null, url, mListener);
		volleyTypeRequest.setTag(mListener);
		mListener.onRequestStarted();
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);

	}

	private static RequestListener initProfileListener = new RequestListener() {

		@Override
		public void onRequestStarted() {

		}

		@Override
		public void onRequestError(int errorCode, String message) {

		}

		@Override
		public void onRequestCompleted(Object responseObject) {

			Log.e("Ashish", "onRequestCompleted" + responseObject);
			SinglePriviledgedUserContainer container = (SinglePriviledgedUserContainer) JsonUtils
					.objectify((String) responseObject,
							SinglePriviledgedUserContainer.class);
			PriviledgedUser user = container.getUser();
			FranklyCrashlytics.setUserName(user.getUsername());
			if (PrefUtils.isVideoSentPending()) {
				EditableProfileObject pendingEditProfileObject = PrefUtils
						.getPendingEditProfileObject();
				if (pendingEditProfileObject != null) {
					String profile_video = pendingEditProfileObject
							.getProfile_video();
					String cover_image = pendingEditProfileObject
							.getCover_picture();
					if (profile_video != null && cover_image != null) {

						File file_video = new File(profile_video);
						File file_thumb = new File(cover_image);

						if (file_video.exists() && file_thumb.exists()) {
							user.setProfile_video(profile_video);
							user.setCover_picture(cover_image);
						} else {
							PrefUtils.setPendingEditProfileObject(null);
							PrefUtils.setIsVideoSentPending(false);
						}
					}

				}
				// String videopath =
			}
			PrefUtils.saveCurrentUserAsJson(JsonUtils.jsonify(user));
		}
	};

	public static void getLocaleFromGPS(final Context context) {
		final SDKLocationListener initLocationHandler2 = new SDKLocationListener() {

			@Override
			public void onLocationUpdated(Location location) {

				if (location != null) {
					/*
					 * Log.d("locale", "locale update on init: " +
					 * location.getLongitude() + " " + location.getLatitude());
					 */

					PrefUtils.setLocation((float) location.getLongitude(),
							(float) location.getLatitude());
					if (isUserLoggedIn())
						sendLocation(context);
					Log.d("location_utils", "asking geolocation ");
					LocationUtils.getAddressFromGeoLocation();

					float[] locationArray = PrefUtils.getLocation();
					if (locationArray != null) {

						if (locationArray[1] != 0 && locationArray[0] != 0) {
							MyLocationHelper.getAddress(locationArray[1] + "",
									locationArray[0] + "", null);
						}

					}
				}
			}

			@Override
			public void onLocalityFound(String locality, String city,
					String country) {

				PrefUtils.setCurrentCity(city, country);

			}
		};
		final SDKLocationListener initLocationHandler1 = new SDKLocationListener() {

			@Override
			public void onLocationUpdated(Location location) {

				if (location != null) {
					/*
					 * Log.d("locale", "locale update on init: " +
					 * location.getLongitude() + " " + location.getLatitude());
					 */

					PrefUtils.setLocation((float) location.getLongitude(),
							(float) location.getLatitude());
					if (isUserLoggedIn())
						sendLocation(context);

					// Un-comment below lines if you also want to get address of
					// location;

					/*
					 * Log.d("location_utils", "asking geolocation ");
					 * LocationUtils.getAddressFromGeoLocation();
					 * 
					 * float[] locationArray = PrefUtils.getLocation(); if
					 * (locationArray != null) {
					 * 
					 * if (locationArray[1] != 0 && locationArray[0] != 0) {
					 * MyLocationHelper .getAddress(locationArray[1] + "",
					 * locationArray[0] + "", initLocationHandler2); }
					 * 
					 * }
					 */
				}
			}

			@Override
			public void onLocalityFound(String locality, String city,
					String country) {

				PrefUtils.setCurrentCity(city, country);

			}
		};

		MyLocationHelper.manageLocation(context, initLocationHandler1);
	}

	private static void sendLocation(Context context) {
		String url = UrlResolver.withAppendedPath(EndPoints.UPDATE_LOCATION);
		CoordinatePoint ob = new CoordinatePoint();
		ob.setCoordinate_point(PrefUtils.getLocation());

		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, ob, url, new RequestListener() {

					@Override
					public void onRequestStarted() {

					}

					@Override
					public void onRequestError(int errorCode, String message) {
						// Log.d("locale", " $$$ Send Locations Error" +
						// message);
					}

					@Override
					public void onRequestCompleted(Object responseObject) {
						// Log.d("locale", " $$$ Send Locations completed"
						// + responseObject.toString());

					}
				});
		// Log.d("locale", "$$$ Sending Locations ");
		dispatchToQueue(vtr, context);
	}

	public static final boolean isUserLoggedIn() {
		// Log.d("debug_201Controller", "isUserLoggedIn=" +
		// (!PrefUtils.getAppAccessToken().equals(PrefUtils.DEFAULT_ACCESS_TOKEN)));
		return !PrefUtils.getAppAccessToken().equals(
				PrefUtils.DEFAULT_ACCESS_TOKEN);

	}

	public static void requestUpdateProfile(final Context context,
			final EditableProfileObject profileObject,
			final RequestListener mListener) {
		Log.e("Update",
				"requestUpdateProfile start: "
						+ JsonUtils.jsonify(profileObject));
		mListener.onRequestStarted();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					HttpResponse updateResponse = updateProfile(context,
							profileObject);
					if (updateResponse.getStatusLine().getStatusCode() == 200) {
						Log.e("Update", "inside if");
						String responseBody = MyUtilities
								.getContent(updateResponse);
						mListener.onRequestCompleted(responseBody);
						if (!PrefUtils.isVideoSentPending())
							PrefUtils.saveCurrentUserAsJson(responseBody);
						Log.e("Update", "response body" + responseBody);
					}
				} catch (Exception e) {
					Log.e("Update", "inside exception" + e);
					e.printStackTrace();
					mListener.onRequestError(
							ErrorCode.SHIT_HAPPENED.getErrorCode(), "Error: "
									+ e.getMessage());

				}

			}
		}).start();

	}

	public static final void requestRegistration(Context context,
			String source, Credentials mCredentials, RequestListener mListener) {
		RegisterRequest mRequest;
		if (PrefUtils.getGcmId().equals(PrefUtils.DEFAULT_GCM_ID)) {
			Log.d("auth", "gcm id unavailable");
			mRequest = new RegisterRequest(getDeviceInfoObject(context)
					.getDevice_id(), mCredentials.getFullName(),
					mCredentials.getEmail(), mCredentials.getPassword(),
					getDeviceInfoObject(context));
		} else {
			Log.d("auth", "gcm id " + PrefUtils.getGcmId());
			Log.e("gcm_id",
					"gcm id here in requestRegistration "
							+ PrefUtils.getGcmId());
			mRequest = new RegisterRequest(getDeviceInfoObject(context)
					.getDevice_id(), mCredentials.getFullName(),
					mCredentials.getEmail(), mCredentials.getPassword(),
					getDeviceInfoObject(context), PrefUtils.getGcmId());
			PrefUtils.setGcmUpdated();
		}
		if (mCredentials.getPhone_num() != null)
			mRequest.setPhone_num(mCredentials.getPhone_num());
		if (mCredentials.getFacebook_access_token() != null) {
			mRequest.setExternal_access_token(mCredentials
					.getFacebook_access_token());
		}
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.REGISTER_NEW_USER);
		if (source == Constant.AuthSource.SOURCE_FACEBOOK) {

			mRequest.setSocial_user_id(mCredentials.getFb_user_id());
		}
		if (source == Constant.AuthSource.SOURCE_GOOGLE) {

			mRequest.setExternal_access_token(mCredentials
					.getGoogle_plus_access_token());
			mRequest.setSocial_user_id(mCredentials.getGplus_user_id());
		}
		if (source == Constant.AuthSource.SOURCE_TWITTER) {
			mRequest.setExternal_access_token(mCredentials
					.getTwitter_access_token());
			mRequest.setToken_secret(mCredentials.getTwitter_token_secret());
			mRequest.setSocial_user_id(mCredentials.getTwitter_user_id());
		}
		url += "/" + source;

		Log.d("reg", "req obj: " + JsonUtils.jsonify(mRequest));
		Request<String> volleyComplaintRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.POST, mRequest, url, mListener);
		volleyComplaintRequest.setShouldCache(false);
		dispatchToQueue(volleyComplaintRequest, context);
		mListener.onRequestStarted();

	}

	public static void requestUnAnsQuesCount(Context context,
			RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_UNANS_QUES_COUNT);
		Request<String> volleyTypeReq = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, url, mListener);
		volleyTypeReq.setShouldCache(false);
		volleyTypeReq.setTag(mListener);
		dispatchToQueue(volleyTypeReq, context);
		mListener.onRequestStarted();
	}

	public static void getAppUpdateInfo(Context context, String versionCode,
			RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_APP_UPDATE_INFO)
				+ "?device_type=android&device_version_code=" + versionCode;
		Log.d("SLA", "url : " + url);
		Request<String> volleyTypeReq = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, url, mListener);
		volleyTypeReq.setShouldCache(false);
		volleyTypeReq.setTag(mListener);
		dispatchToQueue(volleyTypeReq, context);
		mListener.onRequestStarted();
	}

	public static void requestQuestionsAskedToMe(Context context, String next,
			RequestListener mListener) {
		String paginatedUrl = UrlResolver
				.withAppendedPath(EndPoints.GET_QUESTIONS)
				+ "?since="
				+ next
				+ "&limit=" + DEFAULT_PAGE_SIZE;
		Request<String> volleyTypeRequest = bundleToVolleyRequestWithSoftTtl(
				context, Method.GET, null, paginatedUrl, mListener);
		Log.i("controller", "url:" + paginatedUrl);
		volleyTypeRequest.setShouldCache(false);
		volleyTypeRequest.setTag(mListener);
		dispatchToQueue(volleyTypeRequest, context);
		mListener.onRequestStarted();
	}

	public static void requestfollow(Context context, String userid,
			RequestListener mListener) {
		UserIdOnlyRequest mRequest = new UserIdOnlyRequest();
		mRequest.setUserId(userid);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.POST, mRequest,
				UrlResolver.withAppendedPath(EndPoints.FOLLOW_USER), mListener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);
		mListener.onRequestStarted();
	}

	public static void requestUnfollow(Context context, String userid,
			RequestListener mListener) {
		UserIdOnlyRequest mRequest = new UserIdOnlyRequest();
		mRequest.setUserId(userid);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.POST, mRequest,
				UrlResolver.withAppendedPath(EndPoints.UNFOLLOW_USER),
				mListener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);
		mListener.onRequestStarted();
	}

	public static void getSinglePost(Context context, String post_id,
			RequestListener mListener) {
		String url = UrlResolver.withAppendedPath(EndPoints.GET_SINGLE_POST);
		url += post_id;
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, url, mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
		mListener.onRequestStarted();
	}

	public static void basicPaginatedGetRequest(Context context, int EndPoint,
			String since, String username, RequestListener mListener,
			boolean shouldCache) {
		String paginatedUrl = UrlResolver.withAppendedPath(EndPoint);
		if (username != null)
			paginatedUrl += "/" + username;
		float[] loc = PrefUtils.getLocation();
		paginatedUrl += "?since=" + since + "&limit=" + DEFAULT_PAGE_SIZE
				+ "&lon=" + String.valueOf(loc[0]) + "&lat=" + loc[1];
		Log.e("bharat_url", "url :" + paginatedUrl);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.GET, null, paginatedUrl, mListener);
		volleyTypeRequest.setTag(mListener);
		volleyTypeRequest.setShouldCache(shouldCache);
		mListener.onRequestStarted();
		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void basicPaginatedGetRequestQuestions(Context context,
			int EndPoint, String since, String username,
			RequestListener mListener) {
		String paginatedUrl = UrlResolver.withAppendedPath(EndPoint);
		if (username != null)
			paginatedUrl += "?" + "username=" + username;
		float[] loc = PrefUtils.getLocation();
		paginatedUrl += "&since=" + since + "&limit=" + DEFAULT_PAGE_SIZE
				+ "&lon=" + String.valueOf(loc[0]) + "&lat=" + loc[1];
		Log.e("bharat_url", "url :" + paginatedUrl);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.GET, null, paginatedUrl, mListener);
		volleyTypeRequest.setTag(mListener);
		volleyTypeRequest.setShouldCache(false);
		mListener.onRequestStarted();
		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void basicPaginatedGetRequestFollowers(Context context,
			int EndPoint, String offset, String un, RequestListener mListener,
			boolean shouldCache) {
		String paginatedUrl = UrlResolver.withAppendedPath(EndPoint);
		if (un != null)
			paginatedUrl += "/" + un;
		float[] loc = PrefUtils.getLocation();
		paginatedUrl += "?offset=" + offset + "&limit=" + DEFAULT_PAGE_SIZE
				+ "&lon=" + String.valueOf(loc[0]) + "&lat=" + loc[1];
		Log.e("bharat_url", "url :" + paginatedUrl);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoSoftTtl(
				context, Method.GET, null, paginatedUrl, mListener);
		volleyTypeRequest.setTag(mListener);
		volleyTypeRequest.setShouldCache(true);
		mListener.onRequestStarted();
		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void getNotification(Context context, int since, String type,
			int pagesize, RequestListener mListener) {
		String url = UrlResolver.withAppendedPath(EndPoints.GET_NOTIFICATIONS);
		url += "?offset=" + since + "&limit=" + pagesize + "&type=" + type;
		Log.e("bharat_notification", "notificatino call " + url);
		Log.e("notification_url ", url);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, url, mListener);
		vtr.setShouldCache(false);
		mListener.onRequestStarted();
		dispatchToQueue(vtr, context);
	}

	public static void getPeoplePictures(Context context, int count,
			RequestListener mListener) {
		String url = UrlResolver.withAppendedPath(EndPoints.GET_PEOPLE_PIC);
		url = url + "?count=" + count;
		Log.d("FB", "url : " + url);

		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, url, mListener);
		vtr.setShouldCache(false);
		mListener.onRequestStarted();
		dispatchToQueue(vtr, context);

	}

	public static void recordResponse(Context context, String word,
			String medium, String message, RequestListener mListener) {
		String url = UrlResolver.withAppendedPath(EndPoints.RECORD_RESPONSE);
		// url = url + "?count=" + word;
		FeedbackPostRequest fbPostReq = new FeedbackPostRequest();
		fbPostReq.setCount(word);
		fbPostReq.setMedium(medium);
		fbPostReq.setMessage(message);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, fbPostReq, url, mListener);
		vtr.setShouldCache(false);
		mListener.onRequestStarted();
		dispatchToQueue(vtr, context);

	}

	public static void searchElastic(Context context, String query,
			int userCount, int userSkip, RequestListener mListener) {
		mRequestQueue.cancelAll(TAG_SEARCH_QUERY);
		StringBuilder urlBuilder = new StringBuilder(
				UrlResolver.withAppendedPath(EndPoints.ELASTIC_SEARCH));
		urlBuilder.append("?q=").append(query.replaceAll(" ", "%20"))
				.append("&offset=").append(userSkip).append("&limit=")
				.append(userCount);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, urlBuilder.toString(), mListener);
		vtr.setShouldCache(false);
		vtr.setTag(TAG_SEARCH_QUERY);
		dispatchToQueue(vtr, context);
	}

	public static void getSearchSuggestions(Context context, String query,
			RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_SEARCH_SUGGESTIONS);
		url += "?q=" + query.replaceAll(" ", "%20");
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, url, mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static void writeAComment(Context context, String commentBody,
			String post_id, RequestListener mListener, ArrayList<TagObject> tags) {
		Log.e("date", "writing time " + System.currentTimeMillis() / 1000 + "");
		PostCommentrequest commentRequest = new PostCommentrequest();
		commentRequest.setBody(commentBody);
		commentRequest.setPost_id(post_id);
		if (tags != null)
			commentRequest.setTags(tags);
		commentRequest.setCoordinate_point(PrefUtils.getLocation());
		String url = UrlResolver.withAppendedPath(EndPoints.POST_COMMENT);
		Request<String> mRequest = bundleToVolleyRequestNoCaching(context,
				Method.POST, commentRequest, url, mListener);
		mRequest.setShouldCache(false);
		dispatchToQueue(mRequest, context);
	}

	public static void getLikesOnPost(Context context, String post_id,
			int next_index, RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_USERS_LIST_WHO_LIKED_THIS_POST);
		url += "?post_id=" + post_id + "&offset=" + next_index + "&limit="
				+ DEFAULT_PAGE_SIZE;
		Log.e("bharat_like", "url for likes req " + url);
		Request<String> vtr = bundleToVolleyRequestNoSoftTtl(context,
				Method.GET, null, url, mListener);
		vtr.setShouldCache(true);
		dispatchToQueue(vtr, context);
	}

	public static void likeThisPost(Context context, String post_id,
			RequestListener mListener) {
		PostIdOnlyRequest newRequest = new PostIdOnlyRequest();
		newRequest.setPost_id(post_id);
		Log.d("Upvote", post_id);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, newRequest,
				UrlResolver.withAppendedPath(EndPoints.LIKE_A_POST), mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static void reshareThisPost(Context context, String post_id,
			RequestListener mListener) {
		PostIdOnlyRequest newRequest = new PostIdOnlyRequest();
		newRequest.setPost_id(post_id);
		Log.d("Reshare", post_id);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, newRequest,
				UrlResolver.withAppendedPath(EndPoints.RESHARE_POST), mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static void unlikeThisPost(Context context, String post_id,
			RequestListener mListener) {
		PostIdOnlyRequest newRequest = new PostIdOnlyRequest();
		newRequest.setPost_id(post_id);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, newRequest,
				UrlResolver.withAppendedPath(EndPoints.UNLIKE_A_POST),
				mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static void platformSharePost(Context context, String post_id,
			String platform, RequestListener mListener) {
		SharePostObject newRequest = new SharePostObject();
		newRequest.setPostId(post_id);
		newRequest.setPlatform(platform);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, newRequest,
				UrlResolver.withAppendedPath(EndPoints.PLATFORM_SHARE),
				mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static void postAQuestion(Context context, String type,
			String media_url, String body, boolean is_anonymous,
			String question_to, ArrayList<String> tags,
			String recommendedQuestionId, RequestListener mListener,
			String requestID) {
		PostQuestionRequest mRequest = new PostQuestionRequest();
		mRequest.setBody(body);
		mRequest.setQuestion_type(type);
		mRequest.setMedia_url(media_url);
		mRequest.setCoordinate_points(PrefUtils.getLocation());
		mRequest.setIs_anonymous(is_anonymous);
		mRequest.setQuestion_to(question_to);
		mRequest.setRecommended_question_id(recommendedQuestionId);
		mRequest.setTags(tags);
		if (requestID != null)
			mRequest.setRequest_id(requestID);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.POST, mRequest,
				UrlResolver.withAppendedPath(EndPoints.ASK_QUESTION), mListener);
		Log.i("ask", "question object json: " + JsonUtils.jsonify(mRequest));
		mListener.onRequestStarted();
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void getComments(Context context, String post_id,
			String next_index, RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_COMMENTS_ON_THIS_POST);
		url += "?post_id=" + post_id + "&since=" + next_index + "&limit="
				+ DEFAULT_PAGE_SIZE;
		Log.e("comment_url", url);
		Request<String> mRequest = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, url, mListener);
		mRequest.setShouldCache(false);
		mRequest.setTag(mListener);
		dispatchToQueue(mRequest, context);
	}

	public static void getAskedQuestions(Context context, String since,
			int limit, RequestListener listener) {

		String paginatedUrl = UrlResolver
				.withAppendedPath(EndPoints.GET_QUESTIONS)
				+ "?since="
				+ since
				+ "&limit=" + limit;

		Log.d("SuperDuper", "Requesting: " + paginatedUrl);
		Request<String> xRequest = Controller.bundleToVolleyRequestWithSoftTtl(
				context, Method.GET, null, paginatedUrl, listener);
		xRequest.setShouldCache(true);

		xRequest.setTag(listener);
		Controller.dispatchToQueue(xRequest, context);

		xRequest.setRetryPolicy(new DefaultRetryPolicy(15000000, 5,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		listener.onRequestStarted();

	}

	public static void getSingleQuestion(Context context, String quesId,
			RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_SINGLE_QUESTION);
		url += "?question_id=" + quesId;
		Request<String> vtr = bundleToVolleyRequestWithSoftTtl(context,
				Method.GET, null, url, mListener);
		vtr.setShouldCache(true);
		vtr.setTag(mListener);
		dispatchToQueue(vtr, context);
	}

	private static Request<String> bundleToVolleyRequestNoSoftTtl(
			final Context context, int what, Object newRequest, String url,
			final RequestListener mListener) {
		StringBuffer buffer = new StringBuffer(url);
		buffer.replace(0, UrlResolver.BASE_URL.length() - 1, "response--");
		final String url_recieved = buffer.toString();
		Request<String> tempRequest = new JsonRequest<String>(what, url,
				JsonUtils.jsonify(newRequest), new Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d(url_recieved, "Line 767 here is my response: "
								+ response);
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(url_recieved, "in error");
						if (error != null) {
							if (error.networkResponse != null) {
								mListener.onRequestError(
										error.networkResponse.statusCode,
										new String(error.networkResponse.data));
								int statusCode = error.networkResponse.statusCode;

								if (statusCode == ErrorCode.UNAUTHORISED_ACCESS
										.getErrorCode()
										|| statusCode == ErrorCode.TOKEN_EXPIRED
												.getErrorCode()) {

									logoutUserImplicit(context);

								} else {
									mListener
											.onRequestError(
													error.networkResponse.statusCode,
													new String(
															error.networkResponse.data));
									logApiErrorOnGA(
											"CATEGORY_ERROR",
											url_recieved,
											new String(
													error.networkResponse.data),
											error.networkResponse.statusCode);
									Log.e(url_recieved, new String(
											error.networkResponse.data));
								}
							} else {
								Log.e(url_recieved,
										"Error networkresponse is null in volley "
												+ error.getMessage());
								mListener.onRequestError(
										ErrorCode.SHIT_HAPPENED.getErrorCode(),
										"CUSTOM_UNKNOWN_ERROR");
								logApiErrorOnGA("CATEGORY_ERROR", url_recieved,
										"CUSTOM_UNKNOWN_ERROR",
										ErrorCode.SHIT_HAPPENED.getErrorCode());
							}
						} else {
							Log.e(url_recieved, "Error is null in volley");
							mListener.onRequestError(
									ErrorCode.SHIT_HAPPENED.getErrorCode(),
									"CUSTOM_UNKNOWN_ERROR");
							logApiErrorOnGA("CATEGORY_ERROR", url_recieved,
									"CUSTOM_UNKNOWN_ERROR",
									ErrorCode.SHIT_HAPPENED.getErrorCode());
						}
					}
				}) {
			@Override
			protected Response<String> parseNetworkResponse(
					NetworkResponse response) {
				Response<String> mResponse;
				Log.d("parsevolley", "status code:" + response.statusCode);
				if (response.statusCode == 200) {
					String responseBody = new String(response.data);
					if (mListener != null)
						mListener.onRequestCompleted(responseBody);
					mResponse = Response.success(
							responseBody,
							parseIgnoreCacheHeaders(response, this.getUrl(),

							Constant.VolleyTtl.SOFT_TTL_2,
									Constant.VolleyTtl.HARD_TTL_2));
				} else {
					parseNetworkError(new VolleyError(response));
					mResponse = Response.error(new VolleyError(response));
				}
				return mResponse;
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headersMap = new HashMap<String, String>();
				headersMap.put("X-DeviceId", getDeviceInfoObject(context)
						.getDevice_id());
				Log.d("check",
						"AppAccessToken: " + PrefUtils.getAppAccessToken());
				headersMap.put("X-Token", PrefUtils.getAppAccessToken());

				headersMap.put("X-Version-Code",
						FranklyApplication.mVersionCode);
				Log.d("headers", headersMap + "");
				return headersMap;
			}

			@Override
			public String getBodyContentType() {
				return "application/json";
			}
		};
		return tempRequest;
	}

	public static void sendNonVolleyLoginRequest(final String url,
			final Object json, final RequestListener listener) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				String jsonString = JsonUtils.jsonify(json);
				HttpClient client = new DefaultHttpClient();
				HttpResponse response;
				try {
					HttpPost post = new HttpPost(url);
					post.setEntity(new StringEntity(jsonString));
					post.setHeader("Accept", "application/json");
					post.setHeader("Content-type", "application/json");
					response = client.execute(post);
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(response.getEntity()
									.getContent()));
					String responseBody = "", tempStr = null;
					int statusCode = response.getStatusLine().getStatusCode();
					while ((tempStr = reader.readLine()) != null)
						responseBody += tempStr;
					if (statusCode == 200)
						listener.onRequestCompleted(responseBody);
					else
						listener.onRequestError(statusCode, responseBody);

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();

	}

	public static void unblockUser(Context context, String username,
			RequestListener mListener) {
		UsernameOnlyRequest mRequest = new UsernameOnlyRequest();
		mRequest.setUsername(username);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, mRequest,
				UrlResolver.withAppendedPath(EndPoints.UNBLOCK_USER), mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static void blockThisUser(Context context, String username,
			RequestListener mListener) {
		UsernameOnlyRequest mRequest = new UsernameOnlyRequest();
		mRequest.setUsername(username);
		String url = UrlResolver.withAppendedPath(EndPoints.BLOCK_USER);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, mRequest, url, mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static void forgotPassword(Context context, String id,
			RequestListener mListener, boolean isUsername) {
		String url = UrlResolver.withAppendedPath(EndPoints.FORGOT_PASSWORD);
		CheckCredAvailabilityRequest mRequest = new CheckCredAvailabilityRequest();
		if (isUsername)
			mRequest.setUsername(id);
		else
			mRequest.setEmail(id);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, mRequest, url, mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static void requestLogin(Context context, Credentials credentials,
			String source, RequestListener mListener)
			throws DataNotFoundException, IllegalStateException {

		Log.i("ashish", JsonUtils.jsonify(credentials));

		LoginRequest mRequest;
		if (isUserLoggedIn())
			throw new IllegalStateException("Already logged In");
		if (PrefUtils.getGcmId().equals(PrefUtils.DEFAULT_GCM_ID)) {
			Log.d("auth", "gcm id unavailable");
			mRequest = new LoginRequest(source, getDeviceInfoObject(context)
					.getDevice_id(), getDeviceInfoObject(context));
		} else {
			Log.d("auth", "gcm id " + PrefUtils.getGcmId());
			Log.e("gcm_id",
					"gcm id here in requestRegistration "
							+ PrefUtils.getGcmId());
			mRequest = new LoginRequest(source, getDeviceInfoObject(context)
					.getDevice_id(), getDeviceInfoObject(context),
					PrefUtils.getGcmId());
			PrefUtils.setGcmUpdated();
		}
		String mUrl = UrlResolver.withAppendedPath(EndPoints.LOGIN);
		if (source == Constant.AuthSource.SOURCE_EMAIL
				|| source == Constant.AuthSource.SOURCE_USERNAME)
			mUrl += "/email";
		else
			mUrl += "/social/" + source;
		if (source == Constant.AuthSource.SOURCE_EMAIL
				|| source == Constant.AuthSource.SOURCE_USERNAME) {
			mRequest = populateSeperateLoginRequest(mRequest, credentials);
		} else if (source == Constant.AuthSource.SOURCE_FACEBOOK) {
			mRequest = populateFacebookLoginRequest(mRequest, credentials);
		} else if (source == Constant.AuthSource.SOURCE_GOOGLE) {
			mRequest = populateGoogleLoginRequest(mRequest, credentials);
		} else if (source == Constant.AuthSource.SOURCE_TWITTER) {
			mRequest = populateTwitterLoginRequest(mRequest, credentials);
		}
		Log.d("Login_check",
				"mRequestLogin data at controller: "
						+ JsonUtils.jsonify(mRequest));
		sendNonVolleyLoginRequest(mUrl, mRequest, mListener);
	}

	static LoginRequest populateSeperateLoginRequest(LoginRequest request,
			Credentials credentials) throws DataNotFoundException {
		if (credentials.getUsername() != null)
			request.setUsername(credentials.getUsername());
		if (credentials.getEmail() != null)
			request.setEmail(credentials.getEmail());
		if (credentials.getPassword() == null)
			throw new DataNotFoundException("Password required",
					Constant.AuthSource.SOURCE_EMAIL);
		request.setPassword(credentials.getPassword());
		return request;
	}

	static LoginRequest populateFacebookLoginRequest(LoginRequest mRequest,
			Credentials credentials) {
		mRequest.setExternal_access_token(credentials
				.getFacebook_access_token());
		mRequest.setSocial_user_id(credentials.getFb_user_id());
		mRequest.setEmail(credentials.getEmail());
		if (!PrefUtils.getGcmId().equals(PrefUtils.DEFAULT_GCM_ID))
			credentials.setGcm_id(PrefUtils.getGcmId());
		return mRequest;
	}

	static LoginRequest populateGoogleLoginRequest(LoginRequest mRequest,
			Credentials credentials) {
		mRequest.setExternal_access_token(credentials
				.getGoogle_plus_access_token());
		mRequest.setSocial_user_id(credentials.getGplus_user_id());
		mRequest.setEmail(credentials.getEmail());
		return mRequest;
	}

	static LoginRequest populateTwitterLoginRequest(LoginRequest mRequest,
			Credentials credentials) {
		mRequest.setExternal_access_token(credentials.getTwitter_access_token());
		mRequest.setSocial_user_id(credentials.getTwitter_user_id());
		mRequest.setToken_secret(credentials.getTwitter_token_secret());
		return mRequest;
	}

	public static void requestFeeds(Context context, int endPoint,
			RequestListener mListener, String referrerUsername, String since) {
		String paginatedUrl = UrlResolver.withAppendedPath(endPoint)
				+ "?offset=" + since + "&limit=" + DEFAULT_PAGE_SIZE
				+ "&lat=0.0&lon=0.0";
		if (referrerUsername != null) {
			paginatedUrl += "&append_top=" + referrerUsername;
		}
		Log.d("LastCard", "Hitting: " + paginatedUrl);
		Log.d("Controller", "requesting feeds");
		Request<String> xRequest = bundleToVolleyRequestWithSoftTtl(context,
				Method.GET, null, paginatedUrl, mListener);
		xRequest.setShouldCache(true);
		xRequest.setTag(mListener);
		dispatchToQueue(xRequest, context);
		xRequest.setRetryPolicy(new DefaultRetryPolicy(15000000, 5,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		mListener.onRequestStarted();
	}

	public static void getNotificationsCount(Context context,
			RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_NOTIFICATION_COUNT);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, url, mListener);
		dispatchToQueue(vtr, context);
	}

	public static void requestAnswersByUser(Context context, String username,
			String since, RequestListener mListener) {
		String paginatedUrl = UrlResolver
				.withAppendedPath(EndPoints.USER_TIMELINE)
				+ "/"
				+ username
				+ "?since=" + since + "&limit=" + DEFAULT_PAGE_SIZE;
		Request<String> volleyTypeRequest = bundleToVolleyRequestWithSoftTtl(
				context, Method.GET, null, paginatedUrl, mListener);

		volleyTypeRequest.setShouldCache(false);

		volleyTypeRequest.setTag(mListener);
		dispatchToQueue(volleyTypeRequest, context);
		mListener.onRequestStarted();

	}

	public static void requestTypedAnswersByUser(Context context,
			String username, String since, RequestListener mListener) {
		String paginatedUrl = UrlResolver
				.withAppendedPath(EndPoints.USER_TIMELINE)
				+ "/"
				+ username
				+ "/multitype?offset=" + since + "&limit=" + DEFAULT_PAGE_SIZE;
		Request<String> volleyTypeRequest = bundleToVolleyRequestWithSoftTtl(
				context, Method.GET, null, paginatedUrl, mListener);

		volleyTypeRequest.setShouldCache(false);

		volleyTypeRequest.setTag(mListener);
		dispatchToQueue(volleyTypeRequest, context);
		mListener.onRequestStarted();
	}

	public static void requestLikedAnswersByUser(Context context,
			String username, String since, RequestListener mListener) {
		String paginatedUrl = UrlResolver
				.withAppendedPath(EndPoints.USER_TIMELINE_LIKED)
				+ "/"
				+ username + "?offset=" + since + "&limit=" + DEFAULT_PAGE_SIZE;
		Request<String> volleyTypeRequest = bundleToVolleyRequestWithSoftTtl(
				context, Method.GET, null, paginatedUrl, mListener);
		volleyTypeRequest.setShouldCache(false);
		volleyTypeRequest.setTag(mListener);
		dispatchToQueue(volleyTypeRequest, context);
		mListener.onRequestStarted();
	}

	public static HttpResponse updateProfile(Context context,
			EditableProfileObject profileObject) throws Exception {
		String urlString = UrlResolver
				.withAppendedPath(EndPoints.PROFILE_UPDATE)
				+ PrefUtils.getUserID();
		Log.i("Update profile url", urlString);

		HttpClient client = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(urlString);
		httppost.addHeader("X-Token", PrefUtils.getAppAccessToken());
		httppost.addHeader("X-DeviceId", Controller
				.getDeviceInfoObject(context).getDevice_id());

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

		nameValuePairs.add(new BasicNameValuePair("first_name", profileObject
				.getFirst_name()));
		nameValuePairs
				.add(new BasicNameValuePair("bio", profileObject.getBio()));

		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpResponse response = client.execute(httppost);
		Log.d("Update", "" + response);
		return response;
	}

	public static void requestFollowersByUser(Context context, String username,
			long next_index, RequestListener mListener) {
		String paginatedUrl = UrlResolver
				.withAppendedPath(EndPoints.USER_FOLLOWERS)
				+ "/"
				+ username
				+ "?offset=" + next_index + "&limit=" + DEFAULT_PAGE_SIZE;
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoSoftTtl(
				context, Method.GET, null, paginatedUrl, mListener);
		volleyTypeRequest.setShouldCache(true);
		volleyTypeRequest.setTag(mListener);
		dispatchToQueue(volleyTypeRequest, context);
		mListener.onRequestStarted();
	}

	/*
	 * public static void checkUsernameAvailability(String username,
	 * RequestListener mListener) { CheckUsernameRequest mRequest = new
	 * CheckUsernameRequest( getDeviceInfoObject().getDevice_id());
	 * Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
	 * Request.Method.POST, mRequest, UrlResolver
	 * .withAppendedPath(UrlResolver.EndPoints.USERNAME_AVAILABILITY),
	 * mListener); volleyTypeRequest.setShouldCache(false);
	 * dispatchToQueue(volleyTypeRequest); mListener.onRequestStarted(); }
	 */

	public static void requestProfile(Context context, String userId,
			RequestListener mListener) {

		Log.e("Ashish", "requestProfile");
		String userUrl = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.PROFILE_GET_BY_ID)
				+ "/" + userId;
		Request<String> volleyTypeRequest = bundleToVolleyRequestWithSoftTtl(
				context, Request.Method.GET, null, userUrl, mListener);
		mListener.onRequestStarted();
		volleyTypeRequest.setTag(mListener);
		volleyTypeRequest.setShouldCache(!userId.equals(PrefUtils.getUserID()));

		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void requestProfileByName(String username,
			final Context mContext, RequestListener mListener) {

		Log.e("Ashish", "requestProfile");
		String userUrl = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.PROFILE_GET_BY_USERNAME)
				+ "/" + username;
		Request<String> volleyTypeRequest = bundleToVolleyRequestWithSoftTtl(
				mContext, Request.Method.GET, null, userUrl, mListener);
		mListener.onRequestStarted();
		volleyTypeRequest.setTag(mListener);
		volleyTypeRequest.setShouldCache(true);
		dispatchToQueue(volleyTypeRequest, mContext);
	}

	public static void requestQuestionByShortId(String shortId,
			final Context mContext, RequestListener mListener) {

		String questionUrl = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.GET_SHARED_QUESTION)
				+ "/" + shortId;
		Request<String> volleyTypeRequest = bundleToVolleyRequestWithSoftTtl(
				mContext, Request.Method.GET, null, questionUrl, mListener);
		mListener.onRequestStarted();
		volleyTypeRequest.setTag(mListener);
		volleyTypeRequest.setShouldCache(true);
		dispatchToQueue(volleyTypeRequest, mContext);
	}

	public static DeviceInfo getDeviceInfoObject(Context mContext) {
		if (mDeviceInfo == null) {
			mDeviceInfo = new DeviceInfo(mContext);
		}
		return mDeviceInfo;
	}

	private static void dispatchToQueue(Request<String> request,
			Context mContext) {
		if (!NetworkUtils.isNetworkConnected(mContext)) {
			VolleyError error = new VolleyError(defNoInternetNetworkResponse);
			if (request.getMethod() != Request.Method.GET) {
				Log.i("nointernet", "its a post request..");
				request.deliverError(error);
			} else {
				Log.i("nointernet", "its a get request..");
				request.setRetryPolicy(DEFAULT_RETRY_POLICY);
				mRequestQueue.add(request);
			}
		} else {
			request.setRetryPolicy(DEFAULT_RETRY_POLICY);
			mRequestQueue.add(request);
		}
	}

	private static Request<String> bundleToVolleyRequestNoCaching(
			final Context context, int request_method_type,
			final Object newRequest, String url, final RequestListener mListener) {
		StringBuffer buffer = new StringBuffer(url);
		buffer.replace(0, UrlResolver.BASE_URL.length() - 1, "response--");
		final String url_recieved = buffer.toString();

		Request<String> tempRequest = new JsonRequest<String>(
				request_method_type, url, JsonUtils.jsonify(newRequest),

				new Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d(url_recieved, "Line 1205 here is my response: "
								+ response);

					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(url_recieved, "in error");
						if (error != null) {
							if (error.networkResponse != null) {
								int statusCode = error.networkResponse.statusCode;
								Log.d("Logout", "Status code: " + statusCode);
								Log.d("Logout",
										"Error Code: "
												+ ErrorCode.UNAUTHORISED_ACCESS
														.getErrorCode());
								if (statusCode == ErrorCode.UNAUTHORISED_ACCESS
										.getErrorCode()
										|| statusCode == ErrorCode.TOKEN_EXPIRED
												.getErrorCode()) {
									logoutUserImplicit(context);
									return;
								}
								mListener.onRequestError(
										error.networkResponse.statusCode,
										new String(error.networkResponse.data));
								Log.e(url_recieved, new String(
										error.networkResponse.data));
								logApiErrorOnGA("CATEGORY_ERROR", url_recieved,
										new String(error.networkResponse.data),
										error.networkResponse.statusCode);
							} else {
								Log.e(url_recieved,
										"Error networkresponse is null in volley "
												+ error.getMessage());
								mListener.onRequestError(
										ErrorCode.SHIT_HAPPENED.getErrorCode(),
										"CUSTOM_UNKNOWN_ERROR");
								logApiErrorOnGA("CATEGORY_ERROR", url_recieved,
										"CUSTOM_UNKNOWN_ERROR",
										ErrorCode.SHIT_HAPPENED.getErrorCode());
							}
						} else {
							Log.e(url_recieved, "Error is null in volley");
							mListener.onRequestError(
									ErrorCode.SHIT_HAPPENED.getErrorCode(),
									"CUSTOM_UNKNOWN_ERROR");
							logApiErrorOnGA("CATEGORY_ERROR", url_recieved,
									"CUSTOM_UNKNOWN_ERROR",
									ErrorCode.SHIT_HAPPENED.getErrorCode());
						}
					}
				}) {

			@Override
			protected Response<String> parseNetworkResponse(
					NetworkResponse response) {
				Response<String> mResponse;
				Log.d("Logout", "This is network response status code: "
						+ response.statusCode);
				if (response.statusCode == 200) {
					String responseBody = new String(response.data);
					if (mListener != null)
						mListener.onRequestCompleted(responseBody);
					mResponse = Response.success(responseBody,
							HttpHeaderParser.parseCacheHeaders(response));
				} else {
					parseNetworkError(new VolleyError(response));
					mResponse = Response.error(new VolleyError(response));
					Log.d("Logout", "in the second loop");
				}
				return mResponse;
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Log.d("Logout", "Here in get Headers");
				HashMap<String, String> headersMap = new HashMap<String, String>();
				headersMap.put("X-DeviceId", getDeviceInfoObject(context)
						.getDevice_id());
				Log.d("check",
						"AppAccessToken: " + PrefUtils.getAppAccessToken());
				headersMap.put("X-Token", PrefUtils.getAppAccessToken());

				headersMap.put("X-Version-Code",
						FranklyApplication.mVersionCode);
				Log.d("headers", headersMap + "");
				return headersMap;
			}

			@Override
			public String getBodyContentType() {
				return "application/json";
			}
		};

		return tempRequest;
	}

	private static Request<String> bundleToVolleyRequestWithSoftTtl(
			final Context context, int what, final Object newRequest,
			String url, final RequestListener mListener) {
		StringBuffer buffer = new StringBuffer(url);

		final String dev_id = getDeviceInfoObject(context).getDevice_id();
		Log.d("SuperDuper", PrefUtils.getAppAccessToken());
		Log.d("SuperDuper", dev_id);
		buffer.replace(0, UrlResolver.BASE_URL.length() - 1, "response--");
		final String url_recieved = buffer.toString();
		Request<String> tempRequest = new JsonRequest<String>(what, url,
				JsonUtils.jsonify(newRequest), new Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d(url_recieved, "Line 1305 here is my response: "
								+ response);
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(url_recieved, "in error");
						if (error != null) {
							if (error.networkResponse != null) {
								/*
								 * mListener.onRequestError(
								 * error.networkResponse.statusCode, new
								 * String(error.networkResponse.data) +
								 * "device id:" + dev_id);
								 */
								int statusCode = error.networkResponse.statusCode;
								if (statusCode == ErrorCode.UNAUTHORISED_ACCESS
										.getErrorCode()
										|| statusCode == ErrorCode.TOKEN_EXPIRED
												.getErrorCode()) {

									Log.d("SuperDuper", "Unauthorized access");
									logoutUserImplicit(context);
								} else {
									mListener
											.onRequestError(
													error.networkResponse.statusCode,
													new String(
															error.networkResponse.data));
									logApiErrorOnGA(
											"CATEGORY_ERROR",
											url_recieved,
											new String(
													error.networkResponse.data),
											error.networkResponse.statusCode);
									Log.e(url_recieved, new String(
											error.networkResponse.data)
											+ "device id:" + dev_id);
								}
							} else {
								Log.e(url_recieved,
										"Error networkresponse is null in volley");
								mListener.onRequestError(
										ErrorCode.SHIT_HAPPENED.getErrorCode(),
										"CUSTOM_UNKNOWN_ERROR");
								logApiErrorOnGA("CATEGORY_ERROR", url_recieved,
										"CUSTOM_UNKNOWN_ERROR",
										ErrorCode.SHIT_HAPPENED.getErrorCode());
							}
						} else {
							Log.e(url_recieved, "Error is null in volley");
							mListener.onRequestError(
									ErrorCode.SHIT_HAPPENED.getErrorCode(),
									"CUSTOM_UNKNOWN_ERROR");
							logApiErrorOnGA("CATEGORY_ERROR", url_recieved,
									"CUSTOM_UNKNOWN_ERROR",
									ErrorCode.SHIT_HAPPENED.getErrorCode());
						}
					}
				}) {

			@Override
			protected Response<String> parseNetworkResponse(
					NetworkResponse response) {
				Response<String> mResponse;
				if (response.statusCode == 200) {

					String responseBody = new String(response.data);
					Log.d("Controller", "Line 1360 response body is "
							+ responseBody);
					if (mListener != null)
						mListener.onRequestCompleted(responseBody);
					mResponse = Response.success(
							responseBody,
							parseIgnoreCacheHeaders(response, this.getUrl(),
									Constant.VolleyTtl.SOFT_TTL_1,
									Constant.VolleyTtl.HARD_TTL_1));

				} else {
					parseNetworkError(new VolleyError(response));
					mResponse = Response.error(new VolleyError(response));

				}
				return mResponse;
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {

				HashMap<String, String> headersMap = new HashMap<String, String>();
				headersMap.put("X-DeviceId", getDeviceInfoObject(context)
						.getDevice_id());
				Log.d("check",
						"AppAccessToken: " + PrefUtils.getAppAccessToken());
				headersMap.put("X-Token", PrefUtils.getAppAccessToken());

				headersMap.put("X-Version-Code",
						FranklyApplication.mVersionCode);
				Log.d("headers", headersMap + "");
				return headersMap;
			}

			@Override
			public String getBodyContentType() {
				return "application/json";
			}
		};

		return tempRequest;
	}

	public static boolean saveCredentials(Context context,
			Credentials credentials) {
		PrefUtils.saveCredentials(credentials);
		// MpUtils.setMixpanelIdentity(credentials.getId(), mContext,
		// isRegistered);
		Log.e("Ashish", "shallRedirectTOMainActivity");
		getInitProfile(context);
		return true;
	}

	public static void registerOnGCM(Context context) {
		Log.e("gcm_id", " register on GCM called");
		if (PrefUtils.getGcmId() == PrefUtils.DEFAULT_GCM_ID) {
			registerOnGCMInBackground(context);
		} else {
			Log.e("gcm_id", "gcm id is not default");
		}
	}

	private static void registerOnGCMInBackground(final Context context) {
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				Log.e("gcm_id", "do in background");
				GoogleCloudMessaging gcm = GoogleCloudMessaging
						.getInstance(context);
				try {
					String regId = gcm.register(GCMIntentService.GCM_SENDER_ID);
					Log.d("gcm_id", "Gcm_reg_id" + regId);
					PrefUtils.setGcmId(regId);
					getDeviceInfoObject(context).setGcm_id(regId);
					if (isUserLoggedIn())
						updateGcmToServer(context);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(Void result) {
			};
		}.execute(null, null, null);
	}

	/**
	 * Extracts a {@link Cache.Entry} from a {@link NetworkResponse}.
	 * Cache-control headers are ignored. SoftTtl == 3 mins, ttl == 24 hours.
	 * 
	 * @param response
	 *            The network response to parse headers from
	 * @return a cache entry for the given response, or null if the response is
	 *         not cacheable.
	 */
	public static Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response,
			String url, long softttl, long hardttl) {
		long now = System.currentTimeMillis();

		Map<String, String> headers = response.headers;
		long serverDate = 0;
		String serverEtag = null;
		String headerValue;
		headerValue = headers.get("Date");
		if (headerValue != null) {
			serverDate = parseDateAsEpoch(headerValue);
		}

		serverEtag = headers.get("ETag");

		// entry expires
		// completely
		final long softExpire = now + softttl;
		final long ttl = now + hardttl;

		Cache.Entry entry = new Cache.Entry();
		entry.data = response.data;
		entry.etag = serverEtag;
		entry.softTtl = softExpire;
		entry.ttl = ttl;
		entry.serverDate = serverDate;
		entry.responseHeaders = headers;

		return entry;
	}

	/**
	 * Parse date in RFC1123 format, and return its value as epoch
	 */
	public static long parseDateAsEpoch(String dateStr) {
		try {
			// Parse date in RFC1123 format if this header contains one
			return DateUtils.parseDate(dateStr).getTime();
		} catch (DateParseException e) {
			// Date in invalid format, fallback to 0
			return 0;
		}
	}

	public static void logoutUserImplicit(final Context context) {
		Log.d("logout", "logging out implicit");
		Handler h = new Handler(context.getMainLooper());
		h.post(new Runnable() {

			@Override
			public void run() {
				logout(context);
			}
		});

	}

	public static void logoutUserExplicit(final Context context) {

		RequestListener logoutRequestListener = new RequestListener() {

			@Override
			public void onRequestStarted() {

			}

			@Override
			public void onRequestError(int errorCode, String message) {
				((Activity) context).runOnUiThread(new Runnable() {

					@Override
					public void run() {
						logout(context);

					}
				});

			}

			@Override
			public void onRequestCompleted(Object responseObject) {

				((Activity) context).runOnUiThread(new Runnable() {

					@Override
					public void run() {
						logout(context);

					}
				});

			}
		};

		LogoutRequest mRequest = new LogoutRequest(getDeviceInfoObject(context)
				.getDevice_id());
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.POST, mRequest,
				UrlResolver.withAppendedPath(EndPoints.LOGOUT),
				logoutRequestListener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);

	}

	private static void logout(Context context) {
		Log.d("Logout", "Starting activity");

		clearApplicationData(context);
		PrefUtils.clearAppAccessToken();
		logoutGPlus(context);
		logoutFB();
		Intent logoutIntent = new Intent(context, LauncherActivity.class);

		logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		context.startActivity(logoutIntent);

		mRequestQueue.cancelAll(SettingsActivity.class);
		Controller.init(context.getApplicationContext());
	}

	private static void logoutGPlus(Context context) {
		final GoogleApiClient googleApiClient = new GoogleApiClient.Builder(
				context).addApi(Plus.API, Plus.PlusOptions.builder().build())
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();

		googleApiClient.registerConnectionCallbacks(new ConnectionCallbacks() {

			@Override
			public void onConnectionSuspended(int arg0) {

			}

			@Override
			public void onConnected(Bundle arg0) {

				Plus.AccountApi.clearDefaultAccount(googleApiClient);
			}
		});

		googleApiClient.connect();

	}

	private static void logoutFB() {

		Session fbSession = Session.getActiveSession();
		if (fbSession != null)
			fbSession.closeAndClearTokenInformation();

	}

	private static void clearApplicationData(Context context) {
		PrefUtils.clearPref();
		PrefUtils.clearQuestionsPending();
		File cache = context.getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String s : children) {
				if (!s.equals("lib")) {
					deleteDir(new File(appDir, s));
					Log.i("TAG",
							"**************** File /data/data/APP_PACKAGE/" + s
									+ " DELETED *******************");
				}
			}
		}
	}

	private static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		return dir.delete();
	}

	public static void cancelAllMyRequests(RequestListener tag) {
		mRequestQueue.cancelAll(tag);
	}

	public static void mergeSocialAccount(Context context, int type,
			String access_token, String social_id, String email,
			RequestListener mListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.MERGE_SOCIAL_ACCOUNT);
		url += type;
		MergeAccountRequest newRequest = new MergeAccountRequest();
		newRequest.setEmail(email);
		newRequest.setExternal_access_token(access_token);
		newRequest.setSocial_user_id(social_id);
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.POST, newRequest, url, mListener);
		vtr.setShouldCache(false);
		dispatchToQueue(vtr, context);
	}

	public static File createTempFile(InputStream stream, String suffix) {
		File downloadingMediaFile = null;
		Log.d("cache", "creating temp file:");
		try {
			downloadingMediaFile = File.createTempFile("Mmedia", suffix);
			byte[] b = new byte[1024];
			FileOutputStream out = new FileOutputStream(downloadingMediaFile);
			do {

				int numread = stream.read(b);
				if (numread <= 0) {

					// Nothing left to read so quit
					break;

				} else {
					// Log.d("cache", "writing to temp file:  " + new
					// String(b));
					out.write(b, 0, numread);

				}

			} while (true);
			out.close();
			Log.d("cache",
					"creating temp file done: "
							+ downloadingMediaFile.getAbsolutePath()
							+ " file length: ");

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();

		}
		return downloadingMediaFile;

	}

	public static void getFBCoverUrl(final Context context, String username,
			final RequestListener mListener) {
		String fbCoverJsonUrl = "https://graph.facebook.com/" + username
				+ "?fields=cover";
		RequestListener getCoverJSonListener = new RequestListener() {

			@Override
			public void onRequestStarted() {

			}

			@Override
			public void onRequestError(int errorCode, String message) {

			}

			@Override
			public void onRequestCompleted(Object responseObject) {

				Log.d("fbcoverjson", "cover json: " + (String) responseObject);
				try {
					JSONObject coverJSon = new JSONObject(
							(String) responseObject);
					String coverUrl = coverJSon.getJSONObject("cover")
							.getString("source");
					mListener.onRequestCompleted(coverUrl);

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}
		};
		Request<String> vtr = bundleToVolleyRequestNoCaching(context,
				Method.GET, null, fbCoverJsonUrl, getCoverJSonListener);
		dispatchToQueue(vtr, context);
	}

	public static String getFbProfilePicUrl(String username) {
		return "https://graph.facebook.com/" + username
				+ "/picture?width=300&height=300";
	}

	public static void getMixPanelCommand(Context context,
			RequestListener requestListener) {
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.MIXPANEL_COMMAND);

		Request<String> volleyTypeRequest = bundleToVolleyRequestWithSoftTtl(
				context, Request.Method.GET, null, url, requestListener);

		requestListener.onRequestStarted();
		volleyTypeRequest.setTag(requestListener);
		volleyTypeRequest.setShouldCache(true);
		dispatchToQueue(volleyTypeRequest, context);

	}

	public static String getValidAboutMe(String param) {
		String aboutMe = param;
		if (aboutMe.length() <= 280)
			return aboutMe;
		aboutMe = aboutMe.substring(0, 280);
		if (aboutMe.lastIndexOf(".") != -1)
			aboutMe = aboutMe.substring(0, aboutMe.lastIndexOf("."));
		Log.d("check", "Returning about me: " + aboutMe);
		return aboutMe;
	}

	public static void changeUserName(Context context, String userName,
			RequestListener listener) {

		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.CHANGE_USER);
		UsernameOnlyRequest data = new UsernameOnlyRequest();
		data.setUsername(userName);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.POST, data, url, listener);

		listener.onRequestStarted();
		volleyTypeRequest.setTag(listener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);

	}

	public static void changePassword(Context context, String password,
			RequestListener listener) {
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.CHANGE_PASSWORD);
		ChangePassword data = new ChangePassword();
		data.setNewPassword(password);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.POST, data, url, listener);

		listener.onRequestStarted();
		volleyTypeRequest.setTag(listener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);

	}

	/*
	 * invite the celebrity
	 */

	public static void inviteCelebrity(Context context, String id,
			RequestListener listener) {
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.CELEBRITY_INVITE);
		InviteCelebrityRequest invite = new InviteCelebrityRequest();
		invite.setInvitableId(id);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.POST, invite, url, listener);

		listener.onRequestStarted();
		volleyTypeRequest.setTag(listener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);

	}

	public static void getCelebrityQuestions(Context context, String userid,
			String next, RequestListener requestListener) {
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.GET_CELEBRITY_QUESTIONS)
				+ "/"
				+ userid
				+ "?since="
				+ next
				+ "&limit="
				+ DEFAULT_PAGE_SIZE;
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.GET, null, url, requestListener);
		requestListener.onRequestStarted();
		volleyTypeRequest.setTag(requestListener);
		volleyTypeRequest.setShouldCache(true);
		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void getCategorisedCelebs(Context context,
			RequestListener requestListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_CATEGORISED_CELEBS);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.GET, null, url, requestListener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void getPeopleToFollow(Context context,
			RequestListener requestListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_PEOPLE_TO_FOLLOW);
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Method.GET, null, url, requestListener);
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, context);
	}

	public static void upvoteQuestion(Context context, String questionID,
			RequestListener requestListener) {
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.UPVOTE_QUESTION)
				+ "/"
				+ questionID;
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.POST, null, url, requestListener);
		requestListener.onRequestStarted();
		volleyTypeRequest.setTag(requestListener);
		volleyTypeRequest.setShouldCache(true);
		dispatchToQueue(volleyTypeRequest, context);

	}

	public static void downvoteQuestion(Context context, String questionID,
			RequestListener requestListener) {
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.DOWNVOTE_QUESTION)
				+ "/" + questionID;
		Log.i("sumit", "Downvote url " + url);

		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.POST, null, url, requestListener);
		requestListener.onRequestStarted();

		volleyTypeRequest.setTag(requestListener);
		volleyTypeRequest.setShouldCache(true);
		dispatchToQueue(volleyTypeRequest, context);

	}

	public static void ignoreAnswer(Context context, QuestionObject question,
			RequestListener requestListener) {
		String url = UrlResolver
				.withAppendedPath(UrlResolver.EndPoints.IGNORE_ANSWER);
		QuestionId qId = new QuestionId();
		qId.setId(question.getId());
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				context, Request.Method.POST, qId, url, requestListener);
		requestListener.onRequestStarted();

		volleyTypeRequest.setTag(requestListener);
		volleyTypeRequest.setShouldCache(true);
		dispatchToQueue(volleyTypeRequest, context);

	}

	public static void promptForInternet(final Context ctx) {
		OnClickListener clickListener = new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				Toast.makeText(ctx, "Navigate to net settings ",
						Toast.LENGTH_SHORT).show();
			}
		};
		AlertDialog internetDialog = new AlertDialog.Builder(ctx)
				.setMessage("No internet connection")
				.setPositiveButton("Ok", clickListener).setTitle("Frankly")
				.create();
		internetDialog.show();
	}

	public static void invalidateAllPagesInCache(int endPoint) {
		String baseUrl = UrlResolver.withAppendedPath(endPoint);
		Set<String> keySet = mRequestQueue.getCache().getAllKeys();
		Iterator<String> i = keySet.iterator();
		ArrayList<String> filteredKeys = new ArrayList<String>();
		String key;
		while (i.hasNext()) {
			key = i.next();
			Log.d("keyset", "new key: " + key);
			if (key.contains(baseUrl))
				filteredKeys.add(key);
		}
		try {
			for (String k : filteredKeys)
				invalidateCache(k, true);
		} catch (ConcurrentModificationException e) {
			e.printStackTrace();
		}
	}

	public static void invalidateCache(String key, boolean fullExpire) {
		mRequestQueue.getCache().invalidate(key, fullExpire);
	}

	public static void removeAllPagesInCache(int endPoint) {
		Log.d("cache", "cleared "+endPoint);
		String baseUrl = UrlResolver.withAppendedPath(endPoint);
		Set<String> keySet = mRequestQueue.getCache().getAllKeys();
		Iterator<String> i = keySet.iterator();
		ArrayList<String> filteredKeys = new ArrayList<String>();
		String key;
		while (i.hasNext()) {
			key = i.next();
			Log.d("keyset", "new key: " + key);
			if (key.contains(baseUrl))
				filteredKeys.add(key);
		}
		try {
			for (String k : filteredKeys)
				removeCache(k);
		} catch (ConcurrentModificationException e) {
			e.printStackTrace();
		}
	}

	public static void removeCache(String key) {

		Log.d("cache", "removing key: " + key);
		mRequestQueue.getCache().remove(key);

	}

	public static void removeCurrentUserFromCache() {
		String cacheKey = UrlResolver.withAppendedPath(EndPoints.PROFILE_GET)
				+ "/" + PrefUtils.getUsername();
		removeCache(cacheKey);
	}

	public static Session getActiveFBSession(Activity activity) {
		Session s = Session.getActiveSession();
		if (s != null) {
			Log.d("fb",
					"active session:" + s.toString() + " at : "
							+ s.getAccessToken());
			return validateSession(s);

		} else {
			Log.d("fb", "session not active");
			s = Session.openActiveSessionFromCache(activity);
			if (s != null) {
				Log.d("fb", "active session from cache:" + s.toString()
						+ " at : " + s.getAccessToken());
				return validateSession(s);
			} else {
				Log.d("fb", "session not active");
				return null;
			}
		}

	}

	private static Session validateSession(Session s) {
		if (s.isClosed())
			Log.d("fb", "session closed");
		else {
			Log.d("fb", "session open");
			// publishStory(s);
		}
		return s;
	}

	public static void publishStory(final Context context, Session session) {

		if (session != null) {
			Bundle postParams = new Bundle();
			postParams.putString("name", "Facebook SDK for Android");
			postParams.putString("caption",
					"Build great social apps and get more installs.");
			postParams
					.putString(
							"description",
							"The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
			postParams.putString("link",
					"https://developers.facebook.com/android");
			postParams
					.putString("picture",
							"https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

			com.facebook.Request.Callback callback = new com.facebook.Request.Callback() {
				public void onCompleted(com.facebook.Response response) {
					JSONObject graphResponse = response.getGraphObject()
							.getInnerJSONObject();
					String postId = null;
					try {
						postId = graphResponse.getString("id");
					} catch (JSONException e) {
						Log.i("fb", "JSON error " + e.getMessage());
					}
					FacebookRequestError error = response.getError();
					if (error != null) {
						Toast.makeText(context, error.getErrorMessage(),
								Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(context, postId, Toast.LENGTH_LONG)
								.show();
					}
				}
			};

			com.facebook.Request request = new com.facebook.Request(session,
					"me/feed", postParams, HttpMethod.POST, callback);

			RequestAsyncTask task = new RequestAsyncTask(request);
			task.execute();
		}
	}

	private static void updateGcmToServer(Context context) {
		if (isUserLoggedIn()) {
			Log.e("gcm_id", "user logged in and sending gcm finally");
			RequestListener updateGcmRequestListener = new RequestListener() {

				@Override
				public void onRequestStarted() {

				}

				@Override
				public void onRequestError(int errorCode, String message) {

					Log.e("gcm_id", " error" + message);
				}

				@Override
				public void onRequestCompleted(Object responseObject) {

					PrefUtils.setGcmUpdated();
					Log.d("gcm_id", " " + responseObject);

				}
			};
			String url = UrlResolver.withAppendedPath(EndPoints.UPDATE_GCM);
			UpdateGCMRequest mRequest = new UpdateGCMRequest();
			mRequest.setDevice_id(getDeviceInfoObject(context).getDevice_id());
			mRequest.setGcm_id(PrefUtils.getGcmId());
			Request<String> vtr = bundleToVolleyRequestNoCaching(context,
					Method.POST, mRequest, url, updateGcmRequestListener);
			dispatchToQueue(vtr, context);
		} else {
			Log.e("gcm_id", " user not logged in ");
		}
	}

	public static void onNetworkStateChaged(int networkState) {

		if (notifier != null)
			notifier.onNetworkStateChanged(networkState);
	}

	public static void registerNetworkStateListener(
			NetworkStateChangeListener reNotifier) {
		notifier = reNotifier;

	}

	/**
	 * @param pActivity
	 * @param pResId
	 */
	public static void showToast(final Activity pActivity, final int pResId) {
		pActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				String msg = pActivity.getString(pResId);
				if (!TextUtils.isEmpty(msg)) {
					Toast.makeText(pActivity, msg, Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	/**
	 * @deprecated use {@link Controller#showToast(Activity, int)} instead
	 * @param ctx
	 * @param message
	 */
	@Deprecated
	public static void showToast(final Context ctx, final String message) {
		try {
			((Activity) ctx).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
				}
			});
		} catch (Exception e) {
			// 
		}
	}

	public static void setEditTextError(EditText editText,
			TextView errorTextView, String errorMessage) {

		/*
		 * editText.setCompoundDrawablesWithIntrinsicBounds(null, null,
		 * editTextErrorDrawable, null); errorTextView.setText(errorMessage);
		 */
	}

	public static void removeError(EditText editText, TextView errorTextView) {
		editText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
		errorTextView.setText("");
	}

	public static boolean isNetworkConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		return ni != null;
	}

	public static boolean isNetworkConnectedWithMessage(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			Toast.makeText(context,
					"No internet! Please check your network connection.",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	public static ToolTipView addToolTipView(
			ToolTipRelativeLayout toolTipRelativeLayout, View view,
			Context context, String text, boolean below) {
		ToolTip toolTip = new ToolTip().withText(text).withColor(Color.WHITE)
				.withShadow().withAnimationType(ToolTip.AnimationType.NONE);
		toolTip.withTypeface(MyUtilities.getTypeface(context, "bariol"));
		toolTip.withTextColor(Color.BLACK);
		ToolTipView toolTipView = null;
		if (toolTipRelativeLayout != null) {
			toolTipView = toolTipRelativeLayout.showToolTipForView(toolTip,
					view, below);
			Log.d("check", "Tool tip relative layout not null");
		} else
			Log.d("check", "Tool tip relative layout null");
		// mRedToolTipView.setOnToolTipViewClickedListener(this);
		return toolTipView;
	}

	public static void registerGAEvent(String screenName) {
		Tracker t = FranklyApplication.getDefTracker();
		t.setScreenName(screenName);
		t.send(new HitBuilders.AppViewBuilder().build());
	}

	public static void getChannelDown(String channelId, String downId,
			int limit, RequestListener mListener, Context ctx) {
		StringBuilder urlBuilder = new StringBuilder(
				UrlResolver.withAppendedPath(EndPoints.GET_CHANNEL));
		urlBuilder.append(channelId).append("?offset=").append(downId)
				.append("&limit=").append(limit);
		String url = urlBuilder.toString();
		Request<String> vTR = bundleToVolleyRequestWithSoftTtl(ctx, Method.GET,
				null, url, mListener);
		dispatchToQueue(vTR, ctx);
	}

	public static void getChannelUp(String channelId, String upId, int limit,
			RequestListener mListener, Context ctx) {
		StringBuilder urlBuilder = new StringBuilder(
				UrlResolver.withAppendedPath(EndPoints.GET_CHANNEL));
		urlBuilder.append(channelId).append("?up_id").append(upId)
				.append("&limit=").append(limit);
		String url = urlBuilder.toString();
		Request<String> vTR = bundleToVolleyRequestWithSoftTtl(ctx, Method.GET,
				null, url, mListener);
		dispatchToQueue(vTR, ctx);
	}

	public static void getChannelList(String upId, String downId,
			RequestListener mListener, Context ctx) {
		String url = UrlResolver.withAppendedPath(EndPoints.GET_CHANNEL_LIST);
		Request<String> vTR = bundleToVolleyRequestNoCaching(ctx, Method.GET,
				null, url, mListener);
		dispatchToQueue(vTR, ctx);
	}

	public static void getChannelNewDataCount(String channelId, String upId,
			RequestListener mListener) {
		StringBuilder urlBuilder = new StringBuilder(
				UrlResolver
						.withAppendedPath(EndPoints.GET_CHANNEL_NEW_DATA_COUNT));
	}

	private static void logApiErrorOnGA(String category, String action,
			String label, int value) {
		try {
			EventBuilder b = new HitBuilders.EventBuilder();
			b.setCategory(category);
			b.setAction(action);
			b.setLabel(label);
			b.setValue(value);
			FranklyApplication.getDefTracker().send(b.build());
		} catch (Exception e) {

		}
	}

	/**
	 * fire delete answer request to server
	 * 
	 * @param activity
	 * @param post_id
	 * @param mListener
	 */
	public static void deleteAnswer(Activity activity, String post_id,
			RequestListener mListener) {
			DeleteAnswerRequest deleteAnswerRequest = new DeleteAnswerRequest();
			deleteAnswerRequest.setPost_id(post_id);
			String url = UrlResolver.withAppendedPath(EndPoints.DELETE_ANSWER);
			Request<String> mRequest = bundleToVolleyRequestNoCaching(
					activity.getApplicationContext(), Method.POST,
					deleteAnswerRequest, url, mListener);
			mRequest.setShouldCache(false);
			dispatchToQueue(mRequest, activity.getApplicationContext());
	}

	public static void requestSlugDetails(Context pContext, String pUserName, String pSlug,
			 RequestListener pListener) {
		String url = UrlResolver
				.withAppendedPath(EndPoints.GET_SLUG_DETAILS);
		url += "/" + pUserName + "/" + pSlug;
		Request<String> volleyTypeRequest = bundleToVolleyRequestNoCaching(
				pContext, Method.GET, null, url, pListener);
		volleyTypeRequest.setTag(pListener);
		pListener.onRequestStarted();
		volleyTypeRequest.setShouldCache(false);
		dispatchToQueue(volleyTypeRequest, pContext);
	}

}
