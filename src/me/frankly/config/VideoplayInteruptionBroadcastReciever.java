package me.frankly.config;

import me.frankly.listener.ExternalInterruptListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class VideoplayInteruptionBroadcastReciever extends BroadcastReceiver {

	private String TAG = getClass().getSimpleName();

	private static ExternalInterruptListener sExternalInterruptListener;

	public static void setExternalInterruptListener(
			ExternalInterruptListener newExternalInterruptListener) {
		sExternalInterruptListener = newExternalInterruptListener;
	}

	public static boolean removeExternalInterruptListener(
			ExternalInterruptListener interruptListener) {
		if (sExternalInterruptListener != null
				&& sExternalInterruptListener.equals(interruptListener)) {
			sExternalInterruptListener = null;
			Log.d("check", "removed");
			return true;
		}
		Log.d("check", "Failed to remove");
		return false;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, intent.getAction());
		if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
			int state = intent.getIntExtra("state", -1);
			switch (state) {
			case 0:
				Log.d(TAG, "Headset is unplugged");
				if (sExternalInterruptListener != null)
					sExternalInterruptListener.onHeadphoneDisconnected();
				break;
			case 1:
				Log.d(TAG, "Headset is plugged");
				break;
			default:
				Log.d(TAG, "Default state");
			}
			return;
		} else if (intent.getAction().equals(
				TelephonyManager.ACTION_PHONE_STATE_CHANGED)) {
			String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
			if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
				Log.d(TAG, "Phone Is Ringing");
				if (sExternalInterruptListener != null)
					sExternalInterruptListener.onInCall();
			}

			if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
				Log.d(TAG, "Call Recieved Or Outgoing call in progress");
				if (sExternalInterruptListener != null)
					sExternalInterruptListener.onOutCall();
			}

			if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
				Log.d(TAG, "Phone Is Idle");
			}
			return;
		} else if (intent.getAction().equals(
				"android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION")) {
			Log.i(TAG, "Music Playing ");
			/*
			 * if (sExternalInterruptListener != null)
			 * sExternalInterruptListener.onMusicPlayed();
			 */

			return;
		}
	}

}