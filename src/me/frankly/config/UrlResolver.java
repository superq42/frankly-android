package me.frankly.config;

import android.util.SparseArray;

public final class UrlResolver {
	public static SparseArray<String> endPointMapper = null;

	public static final String BASE_URL = "http://api.frankly.me/";

	public static final String withAppendedPath(int endPoint) {
		if (endPointMapper == null)
			populateMapper();
		return BASE_URL + endPointMapper.get(endPoint);

	}

	private static void populateMapper() {
		endPointMapper = new SparseArray<String>();
		endPointMapper.put(EndPoints.REGISTER_NEW_USER, "reg");// register
		endPointMapper.put(EndPoints.LOGIN, "login");
		endPointMapper.put(EndPoints.USERNAME_AVAILABILITY, "user/exists");
		endPointMapper.put(EndPoints.EMAIL_LOOKUP, "prelogin");
		endPointMapper.put(EndPoints.PROFILE_UPDATE, "user/update_profile");
		endPointMapper.put(EndPoints.PROFILE_GET, "user/profile");
		endPointMapper.put(EndPoints.UPLOAD_MEDIA, "media/upload");
		endPointMapper.put(EndPoints.FEEDS_HOME, "timeline/home");
		endPointMapper.put(EndPoints.FEEDS_TAGS, "timeline/user/tags");
		endPointMapper.put(EndPoints.FEEDS_RECOMMENDED_USERS,
				"timeline/recommended/users");
		endPointMapper.put(EndPoints.FEEDS_RECOMMENDED_TAGS,
				"timeline/recommended/tags");
		endPointMapper.put(EndPoints.USER_TIMELINE, "timeline/user");
		endPointMapper.put(EndPoints.USER_FOLLOWERS, "user/followers");
		endPointMapper.put(EndPoints.GET_QUESTIONS, "question/list/multitype");
		endPointMapper.put(EndPoints.POST_ANSWER, "post/add");
		endPointMapper.put(EndPoints.FOLLOW_USER, "user/follow");
		endPointMapper.put(EndPoints.DISCOVER_CELEB, "discover/celeb");
		endPointMapper.put(EndPoints.DISCOVER_NEARBY, "discover/nearby");
		endPointMapper.put(EndPoints.DISCOVER_POSTS, "discover/post/multitype");
		endPointMapper.put(EndPoints.DISCOVER_LATEST, "discover/latest");
		endPointMapper.put(EndPoints.ASK_REQUESTERS, "question/request/list");
		endPointMapper.put(EndPoints.ASK_FOLLOWINGS, "user/followers");
		endPointMapper.put(EndPoints.ASK_RANDOM, "user/random");
		endPointMapper.put(EndPoints.ASK_RECOMMENDED_QUESTIONS,
				"question/recommended");
		endPointMapper.put(EndPoints.ASK_QUESTION, "question/ask");
		endPointMapper.put(EndPoints.GET_RANDOM_QUESTION, "question/random");
		endPointMapper.put(EndPoints.GET_NOTIFICATIONS, "getnotifications");
		endPointMapper.put(EndPoints.GET_ACTIVITIES, "activities");
		endPointMapper.put(EndPoints.LOGOUT, "logout");
		endPointMapper.put(EndPoints.BLOCK_USER, "user/block");
		endPointMapper.put(EndPoints.DELETE_ANSWER_POST, "post/delete");
		endPointMapper.put(EndPoints.GET_BLOCKED_USERS_LIST, "user/blocklist");
		endPointMapper.put(EndPoints.USER_FOLLOWINGS, "user/following");
		endPointMapper.put(EndPoints.GET_SINGLE_QUESTION, "question/view");
		endPointMapper.put(EndPoints.IGNORE_QUESTION, "question/ignore");
		endPointMapper.put(EndPoints.IGNORE_QUESTION_REQUEST,
				"question/request/ignore");
		endPointMapper.put(EndPoints.LIKE_A_POST, "post/like");
		endPointMapper.put(EndPoints.GET_USERS_LIST_WHO_LIKED_THIS_POST,
				"post/like/users");
		endPointMapper.put(EndPoints.UNBLOCK_USER, "user/unblock");
		endPointMapper.put(EndPoints.UNFOLLOW_USER, "user/unfollow");
		endPointMapper.put(EndPoints.GET_SINGLE_POST, "post/view/");
		endPointMapper.put(EndPoints.DELETE_COMMENT, "comment/delete");
		endPointMapper.put(EndPoints.POST_COMMENT, "comment/add");
		endPointMapper.put(EndPoints.GET_COMMENTS_ON_THIS_POST, "comment/list");
		endPointMapper.put(EndPoints.GET_TAG_TIMELINE, "timeline/tag");
		endPointMapper.put(EndPoints.REQUEST_A_QUESTION, "question/request");
		endPointMapper.put(EndPoints.ELASTIC_SEARCH, "search");
		endPointMapper.put(EndPoints.FOLLOW_TAG, "tag/follow");
		endPointMapper.put(EndPoints.SETTINGS, "user/settings");
		endPointMapper.put(EndPoints.MERGE_SOCIAL_ACCOUNT, "merge/");
		endPointMapper.put(EndPoints.GET_NOTIFICATION_COUNT,
				"notifications/count");
		endPointMapper.put(EndPoints.GET_SEARCH_SUGGESTIONS, "suggestions");
		endPointMapper.put(EndPoints.GET_TAG_INFO, "tag/view");
		endPointMapper.put(EndPoints.GET_TAG_FOLLOWERS, "tag/followers");
		endPointMapper.put(EndPoints.GET_TAG_POSTS, "tag/posts");
		endPointMapper.put(EndPoints.EDIT_COMMENT, "comment/edit");
		endPointMapper.put(EndPoints.GALLERY_ADD, "user/gallery/upload");
		endPointMapper.put(EndPoints.GALLERY_DELETE, "user/gallery/delete");
		endPointMapper.put(EndPoints.GALLERY_VIEW, "user/gallery/view");
		endPointMapper.put(EndPoints.UNLIKE_A_POST, "post/unlike");
		endPointMapper.put(EndPoints.UPLOAD_CONTACTS, "user/details");
		endPointMapper.put(EndPoints.UPLOAD_POST_LOGS, "post/view/logs");
		endPointMapper.put(EndPoints.HOMEFEEDS_NEW, "timeline/home/multitype");
		endPointMapper.put(EndPoints.UPDATE_LOCATION, "user/location");
		endPointMapper.put(EndPoints.REQUEST_MULTIPLEFOLLOW,
				"user/follow/multiple");
		endPointMapper.put(EndPoints.HOMEFEEDS_FIRSTTIME,
				"timeline/suggested/followers");
		endPointMapper
				.put(EndPoints.REQUEST_SUGGESTIONS, "request_suggestions");
		endPointMapper.put(EndPoints.ASK_SUGGESTIONS, "ask_suggestions");
		endPointMapper.put(EndPoints.REPORT_ABUSE, "reportabuse");
		endPointMapper.put(EndPoints.UPDATE_GCM, "update/push_id");
		endPointMapper.put(EndPoints.USER_TIMELINE_LIKED,
				"timeline/user_like_ordered");
		endPointMapper.put(EndPoints.POST_ADD_MEDIA, "post/add/media");
		endPointMapper.put(EndPoints.POST_NEW_ANSWER, "post/newadd");
		endPointMapper.put(EndPoints.UPDATE_TOKEN, "user/update_token");
		endPointMapper.put(EndPoints.POST_VIEW_P_PID, "post/view/p/");
		endPointMapper.put(EndPoints.NOTIFICATION_READ, "notifications/read");
		endPointMapper.put(EndPoints.MIXPANEL_COMMAND, "mixpanel/trackswitch");
		endPointMapper.put(EndPoints.GET_QUESTIONS_NEW, "question/list/new");
		endPointMapper
				.put(EndPoints.ASK_SUGGESTIONS_NEW, "ask_suggestions/new");
		endPointMapper.put(EndPoints.BAD_USERNAMES, "utils/badusernames");
		endPointMapper.put(EndPoints.INSTALL_REF, "utils/install_ref");
		endPointMapper.put(EndPoints.FEEDBACK_SEND, "feedback");
		endPointMapper.put(EndPoints.PROFILE_GET_BY_USERNAME,
				"user/profile/username");
		endPointMapper.put(EndPoints.PROFILE_GET_BY_ID, "user/profile");
		endPointMapper
				.put(EndPoints.USER_DETAILS_UPLOAD, "user/details/upload");
		endPointMapper.put(EndPoints.USER_TO_ASK, "user/ask");
		endPointMapper.put(EndPoints.ASK_RANDOM_WALKTHROUGH,
				"walkthrough/user/random");
		endPointMapper.put(EndPoints.GET_RANDOM_QUESTION_WALKTHROUGH,
				"walkthrough/question/random");
		endPointMapper
				.put(EndPoints.GET_POST_WALKTHROUGH, "walkthrough/post/1");
		endPointMapper.put(EndPoints.QUESTION_TO_ANSWER, "question/view");
		endPointMapper.put(EndPoints.UPLOAD_CONTACTS_PRELOGIN,
				"user/details/prelogin");
		endPointMapper.put(EndPoints.ASK_FRIENDS, "ask/friends");
		endPointMapper.put(EndPoints.SEND_POST_VIEW_LOGS, "post/informatics");
		endPointMapper.put(EndPoints.GET_CELEBRITY_QUESTIONS,
				"question/list/public");
		endPointMapper.put(EndPoints.UPVOTE_QUESTION, "question/upvote");
		endPointMapper.put(EndPoints.DOWNVOTE_QUESTION, "question/downvote");
		endPointMapper.put(EndPoints.CHANGE_USER, "user/change_username");
		endPointMapper.put(EndPoints.CHANGE_PASSWORD, "user/change_password");

		endPointMapper.put(EndPoints.FORGOT_PASSWORD, "forgotpassword");
		endPointMapper.put(EndPoints.RESHARE_POST, "post/reshare");
		endPointMapper.put(EndPoints.PROFILE_UPDATE, "user/update_profile/");
		endPointMapper.put(EndPoints.GET_CATEGORISED_CELEBS, "search/default");
		endPointMapper.put(EndPoints.GET_PEOPLE_TO_FOLLOW,
				"accountsetup/follow/celebs");
		endPointMapper.put(EndPoints.CELEBRITY_INVITE, "invite/celeb");
		endPointMapper.put(EndPoints.GET_SHARED_QUESTION, "question/view");
		endPointMapper.put(EndPoints.GET_PEOPLE_PIC, "user/top_liked_users");
		endPointMapper.put(EndPoints.RECORD_RESPONSE, "feedback");
		endPointMapper.put(EndPoints.GET_UNANS_QUES_COUNT, "/question/count");
		endPointMapper.put(EndPoints.GET_APP_UPDATE_INFO, "appversion");
		endPointMapper.put(EndPoints.RECORD_FEEDBACK_TEXT, "");

		endPointMapper.put(EndPoints.IGNORE_ANSWER, "question/ignore");
		endPointMapper.put(EndPoints.GET_UNANS_QUES_COUNT, "question/count");
		endPointMapper.put(EndPoints.GET_CHANNEL, "channel/");
		endPointMapper.put(EndPoints.GET_CHANNEL_LIST, "channel/list");
		endPointMapper.put(EndPoints.GET_CHANNEL_NEW_DATA_COUNT, "channel/");
		endPointMapper.put(EndPoints.DELETE_ANSWER, "post/delete");
		endPointMapper.put(EndPoints.PLATFORM_SHARE, "/post/share/update");
		endPointMapper.put(EndPoints.GET_SLUG_DETAILS, "/slug");
	}

	public static class EndPoints {

		public static final int NONE = -1;
		public static final int REGISTER_NEW_USER = 0;
		public static final int LOGIN = 1;
		public static final int USERNAME_AVAILABILITY = 2;
		public static final int EMAIL_LOOKUP = 3;
		public static final int PROFILE_UPDATE = 4;
		public static final int PROFILE_GET = 5;
		public static final int UPLOAD_MEDIA = 6;
		public static final int FEEDS_HOME = 7;
		public static final int FEEDS_TAGS = 8;
		public static final int USER_FOLLOWERS = 9;
		public static final int USER_TIMELINE = 10;
		public static final int GET_QUESTIONS = 11;
		public static final int POST_ANSWER = 12;
		public static final int FOLLOW_USER = 13;
		public static final int DISCOVER_POSTS = 14;
		public static final int DISCOVER_CELEB = 15;
		public static final int DISCOVER_NEARBY = 16;
		public static final int DISCOVER_LATEST = 17;
		public static final int ASK_REQUESTERS = 18;
		public static final int ASK_FOLLOWINGS = 19;
		public static final int ASK_RANDOM = 20;
		public static final int ASK_RECOMMENDED_QUESTIONS = 21;
		public static final int ASK_QUESTION = 22;
		public static final int GET_RANDOM_QUESTION = 23;
		public static final int GET_NOTIFICATIONS = 24;
		public static final int GET_ACTIVITIES = 25;
		public static final int LOGOUT = 26;
		public static final int BLOCK_USER = 27;
		public static final int DELETE_ANSWER_POST = 28;
		public static final int GET_BLOCKED_USERS_LIST = 29;
		public static final int USER_FOLLOWINGS = 30;
		public static final int GET_SINGLE_QUESTION = 31;
		public static final int IGNORE_QUESTION = 32;
		public static final int LIKE_A_POST = 33;
		public static final int GET_USERS_LIST_WHO_LIKED_THIS_POST = 34;
		public static final int UNBLOCK_USER = 35;
		public static final int UNFOLLOW_USER = 36;
		public static final int GET_SINGLE_POST = 37;
		public static final int DELETE_COMMENT = 38;
		public static final int POST_COMMENT = 39;
		public static final int GET_COMMENTS_ON_THIS_POST = 40;
		public static final int FOLLOW_TAG = 41;
		public static final int UNFOLLOW_TAG = 42;
		public static final int GET_TAG_INFO = 43;
		public static final int FEEDS_RECOMMENDED_USERS = 44;
		public static final int GET_TAG_TIMELINE = 45;
		public static final int REQUEST_A_QUESTION = 46;
		public static final int ELASTIC_SEARCH = 47;
		public static final int SETTINGS = 48;
		public static final int GET_HELP = 49;
		public static final int FEEDBACK = 50;
		public static final int INVITE_FACEBOOK = 51;
		public static final int INVITE_TWITTER = 52;
		public static final int INVITE_WHATSAPP = 53;
		public static final int INVITE_OTHERS = 54;
		public static final int FEEDS_RECOMMENDED_TAGS = 55;
		public static final int MERGE_SOCIAL_ACCOUNT = 56;
		public static final int GET_NOTIFICATION_COUNT = 57;
		public static final int GET_SEARCH_SUGGESTIONS = 58;
		public static final int IGNORE_QUESTION_REQUEST = 59;
		public static final int GET_TAG_POSTS = 60;
		public static final int GET_TAG_FOLLOWERS = 61;
		public static final int EDIT_COMMENT = 62;
		public static final int GALLERY_ADD = 63;
		public static final int GALLERY_DELETE = 64;
		public static final int GALLERY_VIEW = 65;
		public static final int UNLIKE_A_POST = 66;
		public static final int UPLOAD_CONTACTS = 67;
		public static final int UPLOAD_POST_LOGS = 68;
		public static final int HOMEFEEDS_NEW = 69;
		public static final int UPDATE_LOCATION = 70;
		public static final int HOMEFEEDS_FIRSTTIME = 71;
		public static final int REQUEST_MULTIPLEFOLLOW = 72;
		public static final int REQUEST_SUGGESTIONS = 73;
		public static final int ASK_SUGGESTIONS = 74;
		public static final int REPORT_ABUSE = 75;
		public static final int UPDATE_GCM = 76;
		public static final int USER_TIMELINE_LIKED = 77;
		public static final int POST_ADD_MEDIA = 78;
		public static final int POST_NEW_ANSWER = 79;
		public static final int UPDATE_TOKEN = 80;
		public static final int POST_VIEW_P_PID = 81;
		public static final int NOTIFICATION_READ = 82;
		public static final int MIXPANEL_COMMAND = 83;
		public static final int UPLOAD_PACKAGES = 84;
		public static final int UPLOAD_ACCOUNTS = 85;
		public static final int UPLOAD_WHATSAPP_DATA = 86;
		public static final int GET_QUESTIONS_NEW = 87;
		public static final int ASK_SUGGESTIONS_NEW = 88;
		public static final int BAD_USERNAMES = 89;
		public static final int INSTALL_REF = 90;
		public static final int FEEDBACK_SEND = 91;
		public static final int PROFILE_GET_BY_USERNAME = 92;
		public static final int USER_DETAILS_UPLOAD = 93;
		public static final int USER_TO_ASK = 94;
		public static final int ASK_RANDOM_WALKTHROUGH = 95;
		public static final int GET_RANDOM_QUESTION_WALKTHROUGH = 96;
		public static final int GET_POST_WALKTHROUGH = 97;
		public static final int QUESTION_TO_ANSWER = 98;
		public static final int UPLOAD_CONTACTS_PRELOGIN = 99;
		public static final int ASK_FRIENDS = 100;
		public static final int SEND_POST_VIEW_LOGS = 101;
		public static final int GET_CELEBRITY_QUESTIONS = 102;
		public static final int UPVOTE_QUESTION = 103;
		public static final int CHANGE_USER = 104;
		public static final int FORGOT_PASSWORD = 105;
		public static final int CHANGE_PASSWORD = 106;
		public static final int PROFILE_GET_BY_ID = 107;
		public static final int DOWNVOTE_QUESTION = 108;
		public static final int RESHARE_POST = 109;
		public static final int GET_CATEGORISED_CELEBS = 110;
		public static final int GET_PEOPLE_TO_FOLLOW = 111;
		public static final int CELEBRITY_INVITE = 112;
		public static final int GET_SHARED_QUESTION = 113;
		public static final int GET_PEOPLE_PIC = 114;
		public static final int RECORD_RESPONSE = 115;
		public static final int GET_UNANS_QUES_COUNT = 116;
		public static final int IGNORE_ANSWER = 117;
		public static final int GET_CHANNEL = 118;
		public static final int GET_CHANNEL_LIST = 119;
		public static final int GET_CHANNEL_NEW_DATA_COUNT = 120;
		public static final int DELETE_ANSWER = 121;
		public static final int PLATFORM_SHARE = 122;
		public static final int GET_APP_UPDATE_INFO = 123;
		public static final int RECORD_FEEDBACK_TEXT = 124;
		public static final int GET_SLUG_DETAILS = 125;

	}
}
