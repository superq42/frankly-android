package me.frankly.config;

import io.fabric.sdk.android.Fabric;
import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

/**
 * Wrapper class of Crashlytics to disable Crashlytics in development mode
 * 
 * @author Bedprakash
 */
public class FranklyCrashlytics {

	private static boolean IS_IN_PRODUCTION = !AppConfig.DEBUG;

	private static String TAG = "FranklyCrashlytics";

	/**
	 * @param applicaitonInstance
	 */
	public static void enableCrashlytics(Application applicaitonInstance) {
		if (IS_IN_PRODUCTION) {
			Fabric.with(applicaitonInstance, new Crashlytics());

		} else {
			Log.i(TAG, "Crash Monitoring is off for Debug mode builds.");
		}
	}

	/**
	 * @param string
	 */
	public static void log(String string) {
		if (IS_IN_PRODUCTION) {
			Crashlytics.log(string);
		} else {
			Log.i(TAG, "Crash Monitoring is off for Debug mode builds.");
		}
	}

	/**
	 * @param userName
	 */
	public static void setUserName(String userName) {
		if (IS_IN_PRODUCTION) {
			Crashlytics.setUserName(userName);
		} else {
			Log.i(TAG, "Crash Monitoring is off for Debug mode builds.");
		}
	}
}
